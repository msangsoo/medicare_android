package kr.co.hi.medicare.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import kr.co.hi.medicare.net.hwNet.BaseData;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import kr.co.hi.medicare.Defined;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.community.data.ReceiveDataCode;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_DY002;
import kr.co.hi.medicare.util.AlertDialogUtil;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.JsonUtil;
import kr.co.hi.medicare.util.Util;

/**
 * Created by suwun on 2016-06-20
 * 재활운동 운동후 느낌 팝업
 * @since 0, 1
 */
public class CustomVideoPopupActivity extends BaseActivityMedicare {


    private TextView[] tVids ={null,null,null,null,null,null,null,null,null};
    private TextView mCodeTv1 , mCodeTv2 , mCodeTv3 , mCodeTv4 , mCodeTv5 , mCodeTv6 , mCodeTv7 , mCodeTv8 , mCodeTv9;
    private Intent intent;
    private String mMotionCode , mMLSEQ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_video_popup);
        init();
    }

    /**
     * 초기화
     */
    public void init(){


        intent = getIntent();
        if (intent != null){
            mMotionCode = intent.getStringExtra(EXTRA_MOTION_CODE);
            mMLSEQ      = intent.getStringExtra(EXTRA_ML_SEQ);
        }

        mCodeTv1 = (TextView) findViewById(R.id.code_tv1);
        mCodeTv2 = (TextView) findViewById(R.id.code_tv2);
        mCodeTv3 = (TextView) findViewById(R.id.code_tv3);
        mCodeTv4 = (TextView) findViewById(R.id.code_tv4);
        mCodeTv5 = (TextView) findViewById(R.id.code_tv5);
        mCodeTv6 = (TextView) findViewById(R.id.code_tv6);
        mCodeTv7 = (TextView) findViewById(R.id.code_tv7);
        mCodeTv8 = (TextView) findViewById(R.id.code_tv8);
        mCodeTv9 = (TextView) findViewById(R.id.code_tv9);
        tVids[0]=mCodeTv1;
        tVids[1]=mCodeTv2;
        tVids[2]=mCodeTv3;
        tVids[3]=mCodeTv4;
        tVids[4]=mCodeTv5;
        tVids[5]=mCodeTv6;
        tVids[6]=mCodeTv7;
        tVids[7]=mCodeTv8;
        tVids[8]=mCodeTv9;

        if (mMotionCode.equals("A")){
            setView(0);
        }else if (mMotionCode.equals("B")){
            setView(1);
        }else if (mMotionCode.equals("C")){
            setView(2);
        }else if (mMotionCode.equals("D")){
            setView(3);
        }else if (mMotionCode.equals("E")){
            setView(4);
        }else if (mMotionCode.equals("F")){
            setView(5);
        }else if (mMotionCode.equals("G")){
            setView(6);
        }else if (mMotionCode.equals("H")){
            setView(7);
        }else if (mMotionCode.equals("I")){
            setView(8);
        }

    }

    public void click_event(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.cancel_btn:  //  뒤로가기
                finish();
                break;
            case R.id.confirm_btn:
//                requestFeelApi(mMotionCode);
                doUpdateInfo(mMotionCode); //context 문제
                break;
            case R.id.code_tv1:
                mMotionCode = "A";
                setBackGroundColor(mMotionCode);
                break;
            case R.id.code_tv2:
                mMotionCode = "B";
                setBackGroundColor(mMotionCode);
                break;
            case R.id.code_tv3:
                mMotionCode = "C";
                setBackGroundColor(mMotionCode);
                break;
            case R.id.code_tv4:
                mMotionCode = "D";
                setBackGroundColor(mMotionCode);
                break;
            case R.id.code_tv5:
                mMotionCode = "E";
                setBackGroundColor(mMotionCode);
                break;
            case R.id.code_tv6:
                mMotionCode = "F";
                setBackGroundColor(mMotionCode);
                break;
            case R.id.code_tv7:
                mMotionCode = "G";
                setBackGroundColor(mMotionCode);
                break;
            case R.id.code_tv8:
                mMotionCode = "H";
                setBackGroundColor(mMotionCode);
                break;
            case R.id.code_tv9:
                mMotionCode = "I";
                setBackGroundColor(mMotionCode);
                break;
        }
    }








    public void doUpdateInfo(String mtype){
        final Tr_DY002.RequestData requestData = new Tr_DY002.RequestData();

        requestData.ML_SEQ = mMLSEQ;
        requestData.MOTION_CODE = mtype;
        requestData.SEQ = UserInfo.getLoginInfo().seq;

        MediNewNetworkModule.doApi(CustomVideoPopupActivity.this, new Tr_DY002(), requestData,true, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_DY002) {
                    Tr_DY002 data = (Tr_DY002)responseData;
                    try {
                        switch (data.RESULT_CODE){
                            case ReceiveDataCode.DY002_SUCCESS:
                                UiThread();
                                break;
                            case ReceiveDataCode.DY002_ERROR_NOWRITER:
                                errorMessage = getResources().getString(R.string.msg_fail_input);
                                break;
                            case ReceiveDataCode.DY002_ERROR_NOMEMBER:
                                errorMessage = getResources().getString(R.string.comm_error_db011_6666);
                                break;
                            case ReceiveDataCode.DY002_ERROR_ETC:
                                errorMessage = getResources().getString(R.string.comm_error_db011_9999);
                                break;
                            default:
                                errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:2";
                                break;
                        }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                        errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:3";
                    }
                }

                if(!errorMessage.equals("")){
                    CDialog.showDlg(CustomVideoPopupActivity.this, errorMessage);
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CLog.e(response);
            }
        });
    }











    /**
     * API 호출
     */

    public void requestFeelApi(String mtype) {


        CLog.i("mtype --> " + mtype);

        JSONObject jObject = new JSONObject();

        try {
            jObject.put("MOTION_CODE", mtype);   // 모션코드
            jObject.put("ML_SEQ", mMLSEQ);     // 현재페이지
            jObject.put("SEQ", UserInfo.getLoginInfo().seq);      // 회원일렬번호


        } catch (JSONException e) {
            e.printStackTrace();
        }

        //old
//        request(EServerAPI.API_POST, RequestUtil.getJSONParameter(jObject, "DY002"));
        //new
        try {
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            client.setTimeout(10000);
            client.addHeader("Accept-Charset", "UTF-8");
            client.addHeader("User-Agent", new WebView(getBaseContext()).getSettings().getUserAgentString());
            jObject.put("DOCNO","DY002");
            params.put("strJson", jObject.toString());
            CLog.i("url = " + Defined.BASE_HTTPS_URL +"?" +params.toString());
            client.post(Defined.BASE_HTTPS_URL, params, mAsyncHttpHandler);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private AsyncHttpResponseHandler mAsyncHttpHandler = new AsyncHttpResponseHandler() {
        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            CLog.i("onSuccess");

            try {

                int count = headers.length;

                for (int i = 0; i < count; i++) {
                    CLog.v(headers[i].getName() + ": " + headers[i].getValue());
                }

                String response = new String(responseBody, "UTF-8");
                CLog.i("response = " +response);
                JSONObject resultData = null;

                if(response.startsWith("<")){   // api result 문자열 시작값이 "<" 로 시작하는 경우는 xml 폼
                    try {
                        response =  Util.parseXml(response);
                    }catch(Exception e){
                        CLog.e(e.toString());
                    }

                    resultData = new JSONObject(response);

                }else{
                    resultData = new JSONObject(response);
                }

                String resultCode = resultData.getString("RESULT_CODE");
                switch(resultCode){
                    case "0000":
                        UiThread();
                        break;
                    case "4444":
                        UIThread(CustomVideoPopupActivity.this, getString(R.string.confirm), getString(R.string.msg_fail_input));
                        break;
                    case "6666":
                        UIThread(CustomVideoPopupActivity.this, getString(R.string.confirm), getString(R.string.no_user));
                        break;
                    case "9999":
                        UIThread(CustomVideoPopupActivity.this, getString(R.string.confirm), getString(R.string.msg_error));
                        break;
                }


            } catch (Exception e) {
                CLog.e(e.toString());
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            CLog.i("onFailure");
            Handler mHandler = new Handler(Looper.getMainLooper());
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    UIThread(CustomVideoPopupActivity.this, getString(R.string.confirm), getString(R.string.networkexception));
                }
            }, 0);
        }
    };

    @Override
    public void request(EServerAPI eServerAPI, Object obj) {
        Record record = new Record();
        record.setEServerAPI(eServerAPI);
        switch (eServerAPI) {
            case API_GET:
                record.setRequestData(obj);
                break;
            case API_POST:
                record.setRequestData(obj);
                break;
        }
        sendApi(record);
    }


    @Override
    public void response(Record record) throws UnsupportedEncodingException {
        String json = new String(record.getData());
        json = json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1);
        Map<String, Object> result = JsonUtil.getJSONParserData(record.getData());
        CLog.e(json);
        if (result.containsKey("RESULT_CODE")) {
            try {
                switch ((String) result.get("RESULT_CODE")) {
                    case "0000":
                        UiThread();
                        break;
                    case "4444":
                        UIThread(CustomVideoPopupActivity.this, getString(R.string.confirm), getString(R.string.msg_fail_input));
                        break;
                    case "6666":
                        UIThread(CustomVideoPopupActivity.this, getString(R.string.confirm), getString(R.string.no_user));
                        break;
                    case "9999":
                        UIThread(CustomVideoPopupActivity.this, getString(R.string.confirm), getString(R.string.msg_error));
                        break;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void networkException(Record record) {
        CLog.e("ERROR STATE : " + record.stateCode);
        UIThread(CustomVideoPopupActivity.this, getString(R.string.confirm), getString(R.string.networkexception));
    }

    private void UIThread(final Activity activity, final String confirm, final String message) {
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertDialogUtil.onAlertDialog(activity, confirm, message);
            }
        }, 0);
    }

    private void UiThread() {
        final Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                intent.putExtra(EXTRA_MOTION_CODE , mMotionCode);
                setResult(RESULT_OK , intent);
                finish();

            }

        }, 0);
    }

    public void setBackGroundColor(String code){

        if (code.equals("A")){
            setView(0);
        }else if (code.equals("B")){
            setView(1);
        }else if (code.equals("C")){
            setView(2);
        }else if (code.equals("D")){
            setView(3);
        }else if (code.equals("E")){
            setView(4);
        }else if (code.equals("F")){
            setView(5);
        }else if (code.equals("G")){
            setView(6);
        }else if (code.equals("H")){
            setView(7);
        }else if (code.equals("I")){
            setView(8);
        }
    }

    private void setView(int position){

        for(int i=0; i<tVids.length; i++){
            if(i==position){
                tVids[i].setBackgroundResource(R.color.x143_144_158);
                tVids[i].setTextColor(getResources().getColor(R.color.colorWhite));
            }else{
                tVids[i].setBackgroundResource(R.color.colorWhite);
                tVids[i].setTextColor(getResources().getColor(R.color.x12_13_44));
            }
        }



    }
}
