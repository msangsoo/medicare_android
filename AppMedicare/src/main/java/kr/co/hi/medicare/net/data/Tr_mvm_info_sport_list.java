package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * input값
 * insures_code : 회사코드
 * app_code : 기기명
 * mber_sn : 회원키값
 * misson_goal_alert_typ : 1 :체중등록 // 2: 혈압등록 3: 식사일지 등록 4:혈당등록
 *
 * output 값
 * api_code : 기기명
 * insures_code : 회사코드
 * misson_goal_alert_typ" : 1 :체중등록 // 2: 혈압등록 3: 식사일지 등록 4:혈당등록
 * misson_goal_txt : 내용
 * misson_goal_point : 포인트
 * data_yn : Y / N
 */

public class Tr_mvm_info_sport_list extends BaseData {
    private final String TAG = Tr_mvm_info_sport_list.class.getSimpleName();

    public static class RequestData {
        public String mber_sn;
        public String misson_goal_alert_typ;
    }

    public Tr_mvm_info_sport_list() {
    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {
            JSONObject body = getBaseJsonObj();

            RequestData data = (RequestData) obj;
            body.put("api_code", getApiCode(TAG));
            body.put("insures_code", INSURES_CODE);
            body.put("mber_sn", data.mber_sn);
            body.put("misson_goal_alert_typ", data.misson_goal_alert_typ);


            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/
    @SerializedName("api_code")
    public String api_code; //
    @SerializedName("pageNumber")
    public String pageNumber; //
    @SerializedName("maxpageNumber")
    public String maxpageNumber; //
    @SerializedName("active_list")
    public List<active_list> active_list = new ArrayList<>(); //

    public class active_list {
        @SerializedName("active_seq")
        public String active_seq;
        @SerializedName("active_typ")
        public String active_typ;
        @SerializedName("active_cate")
        public String active_cate;
        @SerializedName("active_nm")
        public String active_nm;
        @SerializedName("mets")
        public String mets;
    }

}
