package kr.co.hi.medicare.fragment.mypage;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;


/**
 * Created by MrsWin on 2017-02-16.
 */

public class MyPointMainFragment extends BaseFragmentMedi {
    private final String TAG = getClass().getSimpleName();

    private RadioButton[] mFragmentRadio;
    private RadioGroup mFragmentGroup;
    private Fragment[] mFragments;
    private View tabview;

    public static Fragment newInstance() {
        MyPointMainFragment fragment = new MyPointMainFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_2tab_toolbar_gray, container, false);
        return view;
    }

//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        //super.loadActionbar(actionBar);
//        actionBar.setWhiteTheme();
//        actionBar.setActionBarTitle(getString(R.string.my_point));
//
//    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabview = view;
        mFragmentGroup = view.findViewById(R.id.tab_radiogroup);

        final String[] mFragmentNames = new String[]{
                getString(R.string.my_point_menu1),
                getString(R.string.my_point_menu2),
        };

        mFragments = new Fragment[]{
                MyPointInFoFragment.newInstance(),
                MyPointHowToUseFragment.newInstance(),
        };

        mFragmentRadio = new RadioButton[]{
                view.findViewById(R.id.tab1),
                view.findViewById(R.id.tab2),
        };

        mFragmentRadio[0].setText(mFragmentNames[0]);
        mFragmentRadio[1].setText(mFragmentNames[1]);


        mFragmentGroup.setOnCheckedChangeListener(mCheckedChangeListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(null, view, getContext());


        //마이페이지
        view.findViewById(R.id.tab1).setOnTouchListener(ClickListener);
        view.findViewById(R.id.tab2).setOnTouchListener(ClickListener);

        //코드부여
        view.findViewById(R.id.tab1).setContentDescription(getString(R.string.pointlist));
        view.findViewById(R.id.tab2).setContentDescription(getString(R.string.pointhowto));



        setChildFragment(mFragments[0]);
    }

    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            int pos=group.indexOfChild(tabview.findViewById(checkedId));
            setChildFragment(mFragments[pos]);

        }
    };

    private void setChildFragment(Fragment child) {
        FragmentTransaction childFt = getChildFragmentManager().beginTransaction();

        if (!child.isAdded()) {
            childFt.replace(R.id.child_fragment_layout, child);
            childFt.addToBackStack(null);
            childFt.commit();
        }
    }


}