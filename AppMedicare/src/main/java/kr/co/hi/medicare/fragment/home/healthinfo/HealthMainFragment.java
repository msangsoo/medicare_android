package kr.co.hi.medicare.fragment.home.healthinfo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.sample.SampleFragmentMedi;



/**
 * Created by MrsWin on 2017-02-16.
 */

public class HealthMainFragment extends BaseFragmentMedi {
    private final String TAG = getClass().getSimpleName();
    public static String HEALTH_MAIN_KEY_MENU_IDX = "BUNDLE_KEY_MENU_IDX";
    public static int HEALTH_MENU_1 = 0;
    public static int HEALTH_MENU_2 = 1;
    public static int HEALTH_MENU_3 = 2;
    public static int HEALTH_MENU_4 = 3;
    public static int HEALTH_MENU_5 = 4;

    private FragmentPagerAdapter mPagerAdapter;
    private ViewPager mViewPager;
    private RadioButton[] mFragmentRadio;
    private RadioGroup mFragmentGroup;
    private Fragment[] mFragments;
    private View tabview;

    private LinearLayout[] mContentLayouts;
    private LinearLayout mContentLayout1;
    private LinearLayout mContentLayout2;
    private LinearLayout mContentLayout3;


    public static BaseFragmentMedi newInstance() {
        HealthMainFragment fragment = new HealthMainFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_3tab_health_info, container, false);
        return view;
    }

//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        super.loadActionbar(actionBar);
//        actionBar.setWhiteTheme();
//        actionBar.setActionBarTitle("건강정보");
//    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // XXX 백으로 데이터 보내기
        Bundle bundle = new Bundle();
        bundle.putString(SampleFragmentMedi.SAMPLE_BACK_DATA, TAG + " BackData!!!");
        setBackData(bundle);

        tabview = view;

        mFragmentGroup = view.findViewById(R.id.tab_radiogroup);

        mContentLayout1 = view.findViewById(R.id.child_fragment_layout);
        mContentLayout2 = view.findViewById(R.id.child_fragment_layout2);
        mContentLayout3 = view.findViewById(R.id.child_fragment_layout3);
        mContentLayouts = new LinearLayout[]{mContentLayout1 , mContentLayout2 , mContentLayout3};

        final String[] mFragmentNames = new String[]{
                getString(R.string.health_info_menu1),
                getString(R.string.health_info_menu2),
                getString(R.string.health_info_menu3),
        };

        mFragmentRadio = new RadioButton[]{
                 view.findViewById(R.id.tab1),
                 view.findViewById(R.id.tab2),
                 view.findViewById(R.id.tab3),
        };

        mFragments = new Fragment[] {

                HealthColumnFragment.newInstance(),
                HealthMealFragment.newInstance(),
                DiseaseMainFragment.newInstance(),
        };


        int i = 0;
        for (String name : mFragmentNames) {
            mFragmentRadio[i].setText(name);
            i++;
        }
        mFragmentGroup.setOnCheckedChangeListener(mCheckedChangeListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(null, view, getContext());


        //홈
        view.findViewById(R.id.tab1).setOnTouchListener(ClickListener);
        view.findViewById(R.id.tab2).setOnTouchListener(ClickListener);
        view.findViewById(R.id.tab3).setOnTouchListener(ClickListener);

        //코드부여
        view.findViewById(R.id.tab1).setContentDescription(getString(R.string.h_calum));
        view.findViewById(R.id.tab2).setContentDescription(getString(R.string.h_meal));
        view.findViewById(R.id.tab3).setContentDescription(getString(R.string.h_disease));


        setChildFragment(0);


    }

    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            int pos = group.indexOfChild(tabview.findViewById(checkedId));
            setChildFragment(pos);

        }
    };

    private void setChildFragment(int idx) {
        Fragment fragment = mFragments[idx];
        FragmentTransaction childFt = getChildFragmentManager().beginTransaction();

        for (int i = 0; i < mContentLayouts.length; i++) {
            mContentLayouts[i].setVisibility(i == idx ? View.VISIBLE : View.GONE);
        }

        if (mContentLayouts[idx].getChildCount() == 0) {
            childFt.replace(mContentLayouts[idx].getId(), fragment);
            childFt.addToBackStack(null);
            childFt.commit();
        }

//        if (!fragment.isAdded()) {
//            childFt.replace(mContentLayouts[idx].getId(), fragment);
//            childFt.addToBackStack(null);
//            childFt.commit();
//        }
    }


}