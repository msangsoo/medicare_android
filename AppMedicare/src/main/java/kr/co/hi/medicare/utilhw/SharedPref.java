package kr.co.hi.medicare.utilhw;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {
    private final String TAG = SharedPref.class.getSimpleName();
	
	private static SharedPref instance;
	private static Context mContext;
	
	public final String PREF_NAME = "login";

	public static final String LOGIN_JSON = "login_json";   // 로그인시 받은 json 데이터 저장용

    public static String SAVED_LOGIN_ID = "ID";         // 저장된 아이디
    public static String SAVED_LOGIN_PWD = "PW";       // 저장된 패스워드
    public static String IS_AUTO_LOGIN = "AUTO_LOGIN";           // 자동 로그인 여부

    public static String MBER_SN = "mber_sn";         // 저장된 아이디

    public static String _NOTIPOPUP_DATE = "notipopup_date";         // 노티팝업 날짜
    public static String _IS_STEP_SAVE_INFO = "is_step_save_info";         // 걸음 저장여부
    public static String INTRO_DUMMY_POP = "intro_dummypop";     // 인트로 소개 더미팝업

//    public static String IS_SAVED_LOGIN_ID = "is_saved_login_id";   // 아이디 저장 여부

    public static String IS_LOGIN_SUCEESS = "is_login_suceess";     // 로그인 성공 여부

    public static String MAIN_CARD_LIST = "main_card_list";         // 메인화면 카드 리스트
    public static String STEP_DATA_SOURCE_TYPE = "step_data_source_type"; // 걸음 데이터소스(구글, 밴드)
    public static String IS_SAVED_HEALTH_MESSAGE_DB = "is_saved_health_message_db"; // 3개월치 건강메시지

    public static String GOOGLE_FITNESS_UPLOAD_DATE2 = "GOOGLE_FITNESS_UPLOAD_DATE2"; // 구글 피트니스 걸음 데이터

    public static String IS_SAVED_MEAL_DB = "is_saved_meal_db";           // 3개월치 식사 데이터
    public static String IS_SAVED_FOOD_DB = "is_saved_food_db"; // 3개월치 음식 데이터
    public static String LOAD_MAIN_DATA = "load_main_data";     // 3개월치 데이터

    public static String INTRO_FOOD_DB = "intro_food_db";     // 기본 음식 데이터

    public static String SPLASH_PERMISSION_CHECK = "splash_permission_check";     // 기본 음식 데이터
    public static String SPLASH_BUILD_GOOGLE_ACCOUNT = "splash_build_google_account_2";     // 구글 인증 여부
    public static String IS_REGIST_GOOGLE_FITNESS = "is_regist_google_fitness_2";     // 구글 피트니스 인증 여부

    public static String ALARM_NEW_CHECK = "ALARM_NEW_CHECK"; //알리미 msg New
    public static String ALARM_NEW_INDEX = "ALARM_NEW_INDEX"; //알리미 N 표시 및 idx값
    public static String ALARM_GUBUN = "ALARM_GUBUN"; //알리미 구분
    public static String NOTI_TAB_SAVE = "NOTI_TAB_SAVE"; //노티 탭 저장

    public static String PRESURE_LAST_SAVE_TIME = "presure_last_save_time";
    public static String PRESURE_AFTER_COUNT = "presure_after_count";   // 고혈압 전단계 1기 카운트
    public static String PRESURE_AFTER_TIME = "presure_after_time";     // 고혈압 전단계 1기 시간
    public static String PRESURE_ONESTEP_COUNT = "presure_onestep_count";   // 고혈압 1단계 카운트
    public static String PRESURE_ONESTEP_TIME = "presure_onestep_time";     // 고혈압 1단계 타임
    public static String PRESURE_TWOSTEP_COUNT = "presure_twostep_count";   // 고혈압 2단계 카운트
    public static String PRESURE_TWOSTEP_TIME = "presure_twostep_time";     // 고혈압 2단계 타임

    public static String HEALTH_MESSAGE_HEALTH = "health_message_health"; // 건강메시지
    public static String HEALTH_MESSAGE_SUGAR = "health_message_sugar"; // 건강메시지
    public static String HEALTH_MESSAGE_CONFIRIM = "health_message_confirm"; // 건강메시지
    public static String HEALTH_MESSAGE = "health_message"; // 건강메시지
    public static String ALIMI_MESSAGE = "ALIMI_MESSAGE"; // 알리미 메시지
    public static String PRESURE_DAY_FIRST = "presure_day_first";     //  혈압 최초 입력 여부

    public static String SUGAR_OVER_CHECK = "sugar_over_check";
    public static String SUAGR_OVER_TIME = "sugar_over_time";

    public static String STEP_TOTAL_COUNT = "step_total_count";               // 걸음수 총카운트
    public static String STEP_TOTAL_SAVEDATE = "step_total_savedate";          // 날짜를 초기화를 위한값
    public static String STEP_DISTANCE_TOTAL = "step_distance_total";
    public static String STEP_CALRORI_TOTAL = "step_calrori_total";

    public static String IS_DRUG_REQUEST = "is_drug_request"; // 검사 신청 여부

    public static String SAVE_LOAD_TODAY = "save_load_today";

    public static String HEALTH_INFO_NEW = "health_info_new"; //건강정보 서버 업데이트 날짜


    public static String PUSH_MOVE_INDEX = "PUSH_MOVE_INDEX"; // 푸시 이동 저장
    public static String PUSH_MOVE_INDEX_HIST = "PUSH_MOVE_INDEX_HIST"; // 푸시 이동 로우인덱스

    public static String CITY_STATION = "CITY_STATION"; // 지역 저장

    public static String LOGIN_DATE = "LOGIN_DATE"; // 로그인 날자

    public static String FINGERN_CHANNELID = "FINGERN_CHANNELID"; // 채널아이디 저장

    public static String HEALTH_DANGER_POP = "health_danger_pop";     // 오늘하루안보기 팝업(건강메시지)
    public static String HEALTH_MISSION_POP = "HEALTH_MISSION_POP";     // 건강포인트 적립

    public static String HEALTH_DANGER_MESSAGE = "HEALTH_DANGER_MESSAGE"; // 건강메시지
    public static String SEND_LOG_DATE = "SEND_LOG_DATE"; //클릭 로그 날짜
    public static String CERTI_DATE = "CERTI_DATE"; //인증 날짜


	public static SharedPref getInstance() {
		if (instance == null) {
			instance = new SharedPref();
		}
		return instance;
	}

	public void initContext(Context context) {
        mContext = context;
    }
	
	// 값 불러오기
    public String getPreferences(String key){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString(key, "");
    }
    
 // 값 불러오기
    public int getPreferences(String key, int defValue){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return pref.getInt(key, defValue);
    }

    // 값 불러오기
    public float getPreferences(String key, float defValue){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return pref.getFloat(key, defValue);
    }
 // 값 불러오기
    public boolean getPreferences(String key, boolean defValue){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return pref.getBoolean(key, defValue);
    }
    
    // 값 저장하기
    public void savePreferences(String key, String val){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, val);
        editor.commit();
    }
    
 // 값 저장하기
    public void savePreferences(String key, int val){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, val);
        editor.commit();
    }

    // 값 저장하기
    public void savePreferences(String key, float val){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat(key, val);
        editor.commit();
    }

    // 값 저장하기
    public void savePreferences(String key, boolean val){
        if (mContext == null) {
            Logger.e(TAG, "mContext is null");
        } else {
            SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(key, val);
            editor.commit();
        }
    }
     
    // 값(Key Data) 삭제하기
    public void removePreferences(String key){
        if (mContext == null) {
            Logger.e(TAG, "mContext is null");
        } else {
            SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.remove(key);
            editor.commit();
        }
    }
     
    // 값(ALL Data) 삭제하기
    public void removeAllPreferences(){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }
}
