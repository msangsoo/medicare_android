package kr.co.hi.medicare.googleFitness;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperWeight;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.net.hwdata.Tr_mvm_info_input_data;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.value.model.BandModel;

import static java.text.DateFormat.getDateTimeInstance;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Created by godaewon on 2017. 9. 13..
 */

public class SplashUploadGoogleFitData {
    public static final String TAG = SplashUploadGoogleFitData.class.getSimpleName();
    private SimpleDateFormat sdf;
    private DBHelper helper;

    public void uploadGoogleStepData(Context context, final MediNewNetworkHandler handler) {
        String lastUploadDate = SharedPref.getInstance().getPreferences(SharedPref.GOOGLE_FITNESS_UPLOAD_DATE2);// 마지막 저장된 날자

        Calendar yesterDayCal = (Calendar) Calendar.getInstance().clone();  // 어제 날자
        yesterDayCal.add(Calendar.DATE, -1);    // 어제

        sdf = new SimpleDateFormat("yyyyMMdd");

        String yesterDay = CDateUtil.getForamtyyyyMMdd(new Date(yesterDayCal.getTimeInMillis())); // 어제 날자

        if (TextUtils.isEmpty(lastUploadDate)) {
            // 기존에 저장된 날자가 없으면 오늘 날자로 저장
            SharedPref.getInstance().savePreferences(SharedPref.GOOGLE_FITNESS_UPLOAD_DATE2, sdf.format(new Date()));
            handler.onSuccess(null);
            return;
        }
        // 어제 기준 1개월 날자
        Calendar Month3Cal = Calendar.getInstance();//  1개월 날자
        Month3Cal.add(Calendar.MONTH, -1);
        Month3Cal.add(Calendar.DATE, -1);

        String Month3Day = CDateUtil.getForamtyyyyMMdd(new Date(Month3Cal.getTimeInMillis()));



        // 마지막 저장된 날자
        //DEBUG
//        lastUploadDate = "20190522";
        Calendar lastUploadCal = (Calendar) CDateUtil.getCalendar_yyyyMMdd(lastUploadDate).clone(); // 마지막 저장된 날자
        String lastUploadDay = CDateUtil.getForamtyyyyMMdd(new Date(lastUploadCal.getTimeInMillis()));


        if(lastUploadDay.compareTo(Month3Day) < 0){
            Log.i(TAG, "구글피트니스 걸음 데이터 1개월만 데이터 받기"+lastUploadDay);
            lastUploadCal.set(Calendar.YEAR, Month3Cal.get(Calendar.YEAR));
            lastUploadCal.set(Calendar.MONTH, Month3Cal.get(Calendar.MONTH));
            lastUploadCal.set(Calendar.DATE, Month3Cal.get(Calendar.DATE));
            lastUploadDay = CDateUtil.getForamtyyyyMMdd(new Date(lastUploadCal.getTimeInMillis()));
        }

        if (lastUploadDay.compareTo(yesterDay) > 0) {
            Log.i(TAG, "구글피트니스 걸음 데이터 어제 날자까지 저장되어 있음;"+lastUploadDay);
            handler.onSuccess(null);
            return;
        }

        yesterDay = sdf.format(new Date(lastUploadCal.getTimeInMillis()));
        lastUploadDay = sdf.format(new Date(yesterDayCal.getTimeInMillis()));
        Log.i(TAG, "yesterDay="+yesterDay+", lastUploadDay="+lastUploadDay+ ", "+yesterDay.compareTo(lastUploadDay));

        readData(context, lastUploadCal,yesterDayCal, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                SharedPref.getInstance().savePreferences(SharedPref.GOOGLE_FITNESS_UPLOAD_DATE2, sdf.format(new Date()));
//                    if (finalYesterDay.equals(finalLastUploadDay1)) {
                    handler.onSuccess(responseData);
//                    }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                handler.onFailure(statusCode, response, error);
            }
        });

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void readData(final Context context, Calendar startcal,Calendar endcal, final MediNewNetworkHandler handler) {
        final DataReadRequest req = queryFitnessDay(startcal,endcal);

        Fitness.getHistoryClient(context,
                GoogleSignIn.getLastSignedInAccount(context))
                .readData(req)
                .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
                    @Override
                    public void onSuccess(DataReadResponse response) {

                        Calendar cal = Calendar.getInstance();
                        long temp_idx = StringUtil.getLongVal(CDateUtil.getForamtyyMMddHHmmss(new Date(cal.getTimeInMillis())));

                        if (response.getBuckets().size() > 0) {
                            DateFormat dateFormat = getDateTimeInstance();

                            List<BandModel> list = new ArrayList<>();
                            Log.i(TAG, "BucketSize=" + response.getBuckets().size());
                            for (Bucket bucket : response.getBuckets()) {
                                List<DataSet> dataSets = bucket.getDataSets();
                                for (DataSet dataSet : dataSets) {

                                    for (DataPoint dp : dataSet.getDataPoints()) {
                                        Log.i(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(MILLISECONDS)));
                                        Log.i(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(MILLISECONDS)));
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.setTimeInMillis(dp.getStartTime(MILLISECONDS));
                                        calendar.set(Calendar.MINUTE, 0);
                                        calendar.set(Calendar.SECOND, 0);
                                        calendar.set(Calendar.MILLISECOND, 0);
                                        ++temp_idx;

                                        BandModel model = new BandModel();
                                        model.setIdx(String.valueOf(temp_idx));
                                        model.setRegDate(CDateUtil.getForamtyyyyMMddHHmm(new Date(calendar.getTimeInMillis())));
                                        for (Field field : dp.getDataType().getFields()) {

                                            int value = dp.getValue(field).asInt();
                                            Log.i(TAG, "readData.value="+value);

                                            // 녹십자 헬스케어의 계산식을 사용한 칼로리계산
                                            int calorie = StringUtil.getIntVal(getCalroriCalulator(value));
                                            float distance = getDistanceCalulator(value);


                                            model.setStep(value);
                                            model.setCalories(calorie);
                                            model.setDistance(distance);
                                            list.add(model);
                                        }
                                    }
                                }
                            }

                            if (list.size() > 0) {
                                uploadGoogleFitStepData(context, list, handler);
                            } else {
                                handler.onSuccess(null);
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "onFailure 구글 피트니스 업로드 오류");
                        handler.onFailure(0, null, null);
                    }
        });

    }

    /**
     * 걸음수 조회 하기
     */
    public DataReadRequest queryFitnessDay(Calendar startcal,Calendar endcal) {
        DataType dataType1 = DataType.TYPE_STEP_COUNT_DELTA;
        DataType dataType2 = DataType.AGGREGATE_STEP_COUNT_DELTA;
//        Calendar cal = (Calendar) Calendar.getInstance().clone();
//        cal.add(Calendar.DATE, day);
        startcal.set(Calendar.HOUR_OF_DAY, startcal.getMinimum(Calendar.HOUR_OF_DAY));
        startcal.set(Calendar.MINUTE, startcal.getMinimum(Calendar.MINUTE));
        startcal.set(Calendar.SECOND, startcal.getMinimum(Calendar.SECOND));
        startcal.set(Calendar.MILLISECOND, startcal.getMinimum(Calendar.MILLISECOND));
        long startTime = startcal.getTimeInMillis();
        String dateStr = CDateUtil.getForamtyyyyMMdd(new Date(startTime));
        Log.i(TAG, "queryFitnessDay.dateStr="+dateStr);


        endcal.set(Calendar.HOUR_OF_DAY, endcal.getMaximum(Calendar.HOUR_OF_DAY));
        endcal.set(Calendar.MINUTE, endcal.getMaximum(Calendar.MINUTE));
        endcal.set(Calendar.SECOND, endcal.getMaximum(Calendar.SECOND));
        endcal.set(Calendar.MILLISECOND, endcal.getMaximum(Calendar.MILLISECOND));
        long endTime = endcal.getTimeInMillis();
        String enddateStr = CDateUtil.getForamtyyyyMMdd(new Date(endTime));
        Log.i(TAG, "queryFitnessDay.dateStr="+enddateStr);

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(dataType1, dataType2)
                .bucketByTime(1, TimeUnit.HOURS)
//                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, MILLISECONDS)
                .build();


          Log.i(TAG, "GoogleStepSave startDate:"+startTime + " endTime:" + endTime  +  " readRequest:"+readRequest);

        return readRequest;
    }


    /**
     * 구글 걸음 데이터를 서버에 전송 및 Sqlite에 저장하기
     * 일별 조회 일때만 저장하기
     */
    private void uploadGoogleFitStepData(Context context, List<BandModel> dataModel, MediNewNetworkHandler handler) {
        Tr_mvm_info_input_data inputData                = new Tr_mvm_info_input_data();
        Tr_login login                                  = UserInfo.getLoginInfo();

        Tr_mvm_info_input_data.RequestData requestData  = new Tr_mvm_info_input_data.RequestData();
        requestData.mber_sn     = login.mber_sn;
        requestData.ast_mass    = inputData.getArray(dataModel, "D");
        MediNewNetworkModule.doApi(context, Tr_mvm_info_input_data.class, requestData, handler);
    }

    /**
     * 칼로리 계산
     */
    public String getCalroriCalulator(int step) {
        Tr_login login = UserInfo.getLoginInfo();
        int sex = StringUtil.getIntVal(login.mber_sex);
        float height = StringUtil.getFloat(login.mber_height);
        float weight = StringUtil.getFloatVal(login.mber_bdwgh);

        Log.i(TAG, "getCalroriTargetCalulator.sex=" + sex + ", height=" + height + ", weight=" + weight + ", step=" + step);
        double calroriTarget = 0.0f;
        String rtnValue;
        if (sex == 1) {
            calroriTarget = (step * (((3.5 + 0.1 * (height / 100 / (-0.5625 * 3.5 + 4.2294) * 3.5 / 2 * 60)) * weight) / 1000 * 5)) / (((((height / 100 / (-0.5625 * 3.5 + 4.2294)) * 3.5) / 2) * 60) / (height / 100 / (-0.5625 * 3.5 + 4.2294)));
        } else {
            calroriTarget = (step * (((3.5 + 0.1 * (height / 100 / (-0.5133 * 3.5 + 4.1147) * 3.5 / 2 * 60)) * weight) / 1000 * 5)) / (((((height / 100 / (-0.5133 * 3.5 + 4.1147)) * 3.5) / 2) * 60) / (height / 100 / (-0.5133 * 3.5 + 4.1147)));
        }
        rtnValue = String.format("%.0f", calroriTarget);

        if (height == 0 || weight == 0) {
            return "0";
        } else {
            return rtnValue;
        }
    }


    /**
     * 거리계산
     */
    public float getDistanceCalulator(int step) {
        Tr_login login = UserInfo.getLoginInfo();
        float avgStepDistance = (StringUtil.getFloat(login.mber_height) - 100f); // 평균보폭
        return StringUtil.getFloatVal(String.format("%.1f",(avgStepDistance * step) * 0.1f));

    }



}
