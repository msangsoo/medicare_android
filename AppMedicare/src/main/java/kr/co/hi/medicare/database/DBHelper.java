package kr.co.hi.medicare.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.utilhw.Logger;

/**
 * Created by mrsohn on 2017. 3. 20..
 */

public class DBHelper extends SQLiteOpenHelper {
    private final String TAG                    = DBHelper.class.getSimpleName();

    // + ------------------------------------------------------------
    // 15 : 7/12 - 음식테이블레 unit 추가. (14->15업그래이드시 오류발생)
    // 16 : 7/13 - 음식테이블 DB_VERSION이 다르면 테이블 Drop하고 새로 만들도록 수정.
    // + ------------------------------------------------------------
    private static int DB_VERSION               = 32; //2019-03-04 park update 21
    public boolean isNewFood                    = false;        //새로운 음식디비 있음.
    public static String DB_NAME                = "greencare_db";
    public static final String DB_PATH = "/data/data/" + BuildConfig.APPLICATION_ID + "/databases/";

    public String MAIN_ITEM_TABLE               = "_item_table";
    public String MAIN_ITEM_COLUMN_IDX          = "_idx";
    public String MAIN_ITEM_COLUMN_ITEM         = "_item";
    public String MAIN_ITEM_COLUMN_VISIBLE      = "_visible";

    private DBHelperSugar mSugarDb              = new DBHelperSugar(DBHelper.this);
    private DBHelperPresure mPresureDb          = new DBHelperPresure(DBHelper.this);
    private DBHelperStep mStepDb                = new DBHelperStep(DBHelper.this);
    private DBHelperStepRealtime mStepRtimeDb   = new DBHelperStepRealtime(DBHelper.this);
    private DBHelperWater mWaterDb              = new DBHelperWater(DBHelper.this);
    private DBHelperWeight mWeightDb            = new DBHelperWeight(DBHelper.this);
    private DBHelperBasic mBasicDb              = new DBHelperBasic(DBHelper.this);
    private DBHelperMessage mMessageDb          = new DBHelperMessage(DBHelper.this);
    private DBHelperPPG mPpgDb                  = new DBHelperPPG(DBHelper.this);

    private DBHelperFoodMain mFoodMainDb        = new DBHelperFoodMain(DBHelper.this);
    private DBHelperFoodDetail mFoodDetailDb    = new DBHelperFoodDetail(DBHelper.this);

    private DBHelperFoodCalorie mFoodCalorieDb  = new DBHelperFoodCalorie(DBHelper.this);

    //2019-02-25 park add
    private DBHelperCommunitySearches mCommunitySearches  = new DBHelperCommunitySearches(DBHelper.this);
    private DBHelperCommunityNotice mCommunityNotice = new DBHelperCommunityNotice(DBHelper.this);
    private DBHelperAlramHealth mDbHelperAlramHealth = new DBHelperAlramHealth(DBHelper.this);

    //2019-03-04 음식 히스토리
    private DBHelperFoodCalorieSearchHis mFoodCalorieSearchHisDb  = new DBHelperFoodCalorieSearchHis(DBHelper.this);

    //2019-03-23 운동 히스토리
    private DBHelperCalorie mCaloriesDb  = new DBHelperCalorie(DBHelper.this);

    //2019-04-03 클릭로그
    private DBHelperLog mLogDb    = new DBHelperLog(DBHelper.this);


    // DBHelper 생성자로 관리할 DB 이름과 버전 정보를 받음
    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    // DB를 새로 생성할 때 호출되는 함수
    @Override
    public void onCreate(SQLiteDatabase db) {
        // 새로운 테이블 생성
        Logger.i(TAG, "DBHelper:onCreate");
        String sql = "CREATE TABLE if not exists "  + MAIN_ITEM_TABLE + " ("
                                                    + MAIN_ITEM_COLUMN_IDX + " INTEGER, "
                                                    + MAIN_ITEM_COLUMN_ITEM + " TEXT, "
                                                    + MAIN_ITEM_COLUMN_VISIBLE + " BOOLEAN);";
        db.execSQL(sql);
        Logger.i(TAG, "onCreate.sql=" + sql);
        db.execSQL(mSugarDb.createDb());
        db.execSQL(mPresureDb.createDb());
        db.execSQL(mStepDb.createDb());
        db.execSQL(mStepRtimeDb.createDb());
        db.execSQL(mWaterDb.createDb());
        db.execSQL(mWeightDb.createDb());
        db.execSQL(mBasicDb.createDb());
        db.execSQL(mMessageDb.createDb());
        db.execSQL(mPpgDb.createDb());

        db.execSQL(mFoodMainDb.createDb());
        db.execSQL(mFoodDetailDb.createDb());
        db.execSQL(mFoodCalorieDb.createDb());

        db.execSQL(mCommunitySearches.createDb());//2019-02-25 park add
        db.execSQL(mCommunityNotice.createDb());//2019-02-25 park add
        db.execSQL(mFoodCalorieSearchHisDb.createDb());//2019-03-04 음식 히스토리
        db.execSQL(mDbHelperAlramHealth.createDb());//2019-03-12 건강기록알람
        db.execSQL(mCaloriesDb.createDb());//2019-03-23 운동 히스토리
        db.execSQL(mLogDb.createDb()); // 클릭로
    }


    // DB 업그레이드를 위해 버전이 변경될 때 호출되는 함수
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Logger.i(TAG, "DBHelper:onUpgrade");

        if (oldVersion != newVersion) {
            Logger.i(TAG, "DBHelper:onUpgrade.oldVersion=" + oldVersion + ", newVersion=" + newVersion);

            isNewFood = true;
            // 지우고 테이블 다시 만듦.
//            db.execSQL(mFoodCalorieDb.dropTable()); //음식데이터로 업그레이드 시 삭제되지 않게 주석 처리 20190327
            db.execSQL(mCommunitySearches.dropTable());
            db.execSQL(mCommunityNotice.dropTable());
            db.execSQL(mDbHelperAlramHealth.dropTable());
            db.execSQL(mFoodCalorieSearchHisDb.dropTable());
            db.execSQL(mLogDb.dropTable());

            onCreate(db);
        }
    }

    public void insert(int idx, String _item, boolean _visible) {
        // DB에 입력한 값으로 행 추가
        String sql = "INSERT INTO " + MAIN_ITEM_TABLE
                                    + " VALUES(" + idx + ", '"
                                    + _item + "', '"
                                    + _visible + "');";
        Logger.i(TAG, "insert.sql=" + sql);
        transactionExcute(sql);
    }

    public void updateIdx(int idx, String item) {
        String sql = "UPDATE "  + MAIN_ITEM_TABLE + " SET "
                                + MAIN_ITEM_COLUMN_IDX + "=" + idx
                                + " WHERE " + MAIN_ITEM_COLUMN_ITEM + "='" + item + "';";
        Logger.i(TAG, "updateIdx.sql=" + sql);
        transactionExcute(sql);
    }

    public void updateVisible(boolean _visible, String item) {
        String sql = "UPDATE "  + MAIN_ITEM_TABLE + " SET "
                                + MAIN_ITEM_COLUMN_VISIBLE + "='" + _visible
                                + "' WHERE " + MAIN_ITEM_COLUMN_ITEM + "='" + item + "';";
        Logger.i(TAG, "updateVisible.sql=" + sql);
        transactionExcute(sql);

    }

    public void deleteAll() {
        SQLiteDatabase db = getWritableDatabase();

        String sql = "DELETE FROM " + DBHelperSugar.TB_DATA_SUGAR;
        Logger.i(TAG, "delete.sql=" + sql);
        db.execSQL(sql);
        Logger.i(TAG, sql);

        String sql2 = "DELETE FROM " + DBHelperWeight.TB_DATA_WEIGHT;
        Logger.i(TAG, "delete.sql=" + sql2);
        db.execSQL(sql2);
        Logger.i(TAG, sql2);

        String sql3 = "DELETE FROM " + DBHelperFoodDetail.TB_DATA_FOOD_DETAIL;
        Logger.i(TAG, "delete.sql=" + sql3);
        db.execSQL(sql3);
        Logger.i(TAG, sql3);

        String sql4 = "DELETE FROM " + DBHelperFoodMain.TB_DATA_FOOD_MAIN;
        Logger.i(TAG, "delete.sql=" + sql4);
        db.execSQL(sql4);
        Logger.i(TAG, sql4);

        String sql5 = "DELETE FROM " + DBHelperMessage.TB_DATA_MESSAGE;
        Logger.i(TAG, "delete.sql=" + sql5);
        db.execSQL(sql5);
        Logger.i(TAG, sql5);

        String sql6 = "DELETE FROM " + DBHelperPresure.TB_DATA_PRESSURE;
        Logger.i(TAG, "delete.sql=" + sql6);
        db.execSQL(sql6);
        Logger.i(TAG, sql6);

        String sql7 = "DELETE FROM " + DBHelperStep.TB_DATA_STEP;
        Logger.i(TAG, "delete.sql=" + sql7);
        db.execSQL(sql7);
        Logger.i(TAG, sql7);

        String sql8 = "DELETE FROM " + DBHelperWater.TB_DATA_WATER;
        Logger.i(TAG, "delete.sql=" + sql8);
        db.execSQL(sql8);
        Logger.i(TAG, sql8);

        String sql9 = "DELETE FROM " + DBHelperPPG.TB_DATA_PPG;
        Logger.i(TAG, "delete.sql=" + sql9);
        db.execSQL(sql9);
        Logger.i(TAG, sql9);

        String sql10 = "DELETE FROM " + DBHelperStepRealtime.TB_DATA_STEP_REALTIME;
        Logger.i(TAG, "delete.sql=" + sql10);
        db.execSQL(sql10);
        Logger.i(TAG, sql10);


        String sql11 = "DELETE FROM " + DBHelperCommunitySearches.TB_DATA_COMM_SEARCHES;
        Logger.i(TAG, "delete.sql=" + sql11);
        db.execSQL(sql11);
        Logger.i(TAG, sql11);

        String sql12 = "DELETE FROM " + DBHelperFoodCalorieSearchHis.Field.TB_DATA_FOOD_CALORIE_SEARCH_HIS;
        Logger.i(TAG, "delete.sql=" + sql10);
        db.execSQL(sql12);
        Logger.i(TAG, sql12);


        String sql13 = "DELETE FROM " + DBHelperCommunityNotice.TB_DATA_COMM_NOTICE;
        Logger.i(TAG, "delete.sql=" + sql13);
        db.execSQL(sql13);
        Logger.i(TAG, sql13);

        String sql14 = "DELETE FROM " + DBHelperCalorie.TB_DATA_CALROIE;
        Logger.i(TAG, "delete.sql=" + sql14);
        db.execSQL(sql14);
        Logger.i(TAG, sql14);

        String sql15 = "DELETE FROM " + DBHelperLog.Field.TB_LOG;
        Logger.i(TAG, "delete.sql=" + sql15);
        db.execSQL(sql15);
        Logger.i(TAG, sql15);


        db.close();
    }


    public void delete(String _item) {
        SQLiteDatabase db = getWritableDatabase();
        // 입력한 항목과 일치하는 행 삭제
        String sql = "DELETE FROM " + MAIN_ITEM_TABLE + " WHERE " + MAIN_ITEM_COLUMN_ITEM + "='" + _item + "';";
        Logger.i(TAG, "delete.sql=" + sql);
        db.execSQL(sql);
        Logger.i(TAG, sql);
        db.close();
    }

    public void transactionExcute(String sql) {
        SQLiteDatabase db = null;

        try {
            db = getWritableDatabase();
            db.beginTransaction();
            db.execSQL(sql);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (db != null)
                db.endTransaction();
        }
        if (db != null)
            db.close();
    }

    public boolean transactionExcuteB(String sql) {
        boolean isSuccess=false;

        SQLiteDatabase db = getWritableDatabase();

        db.beginTransaction();
        try {
            db.execSQL(sql);
            db.setTransactionSuccessful();
            isSuccess=true;
        } catch (SQLException e) {
            e.printStackTrace();
            isSuccess=false;
        } finally {
            db.endTransaction();
        }

        db.close();

        return isSuccess;
    }


    //2019-02-25 PARK ADD sql 만큼 쿼리 수행
    public boolean transactionExcute(List<String> sql) {

        boolean isSuccess=false;

        if(sql==null||sql.size()<1)
            return isSuccess;

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {

            for(int i=0;i<sql.size();i++) {
                db.execSQL(sql.get(i));
            }

            db.setTransactionSuccessful();
            isSuccess=true;
        } catch (SQLException e) {
            e.printStackTrace();
            isSuccess=false;
        } finally {
            db.endTransaction();
        }

        db.close();
        return isSuccess;
    }


    public DBHelperSugar getSugarDb() {
        return mSugarDb;
    }

    public DBHelperPresure getPresureDb() {
        if (mPresureDb == null)
            mPresureDb = new DBHelperPresure(DBHelper.this);
        return mPresureDb;
    }

    public DBHelperStep getStepDb() {
        if (mStepDb == null)
            mStepDb = new DBHelperStep(DBHelper.this);
        return mStepDb;
    }

    public DBHelperStepRealtime getmStepRtimeDb() {
        if (mStepRtimeDb == null)
            mStepRtimeDb = new DBHelperStepRealtime(DBHelper.this);
        return mStepRtimeDb;
    }

    public DBHelperPPG getPPGDb() {
        if (mPpgDb == null)
            mPpgDb = new DBHelperPPG(DBHelper.this);
        return mPpgDb;
    }

    public DBHelperWater getWaterDb() {
        if (mWeightDb == null)
            mWeightDb = new DBHelperWeight(DBHelper.this);
        return mWaterDb;
    }

    public DBHelperWeight getWeightDb() {
        if (mWeightDb == null)
            mWeightDb = new DBHelperWeight(DBHelper.this);
        return mWeightDb;
    }

    public DBHelperBasic getBasicDb() {
        if (mBasicDb == null)
            mBasicDb = new DBHelperBasic(DBHelper.this);
        return mBasicDb;
    }

    public DBHelperMessage getMessageDb() {
        if (mMessageDb == null)
            mMessageDb = new DBHelperMessage(DBHelper.this);
        return mMessageDb;
    }

    public DBHelperFoodMain getFoodMainDb() {
        if (mFoodMainDb == null)
            mFoodMainDb = new DBHelperFoodMain(DBHelper.this);
        return mFoodMainDb;
    }

    public DBHelperFoodDetail getFoodDetailDb() {
        if (mFoodDetailDb == null)
            mFoodDetailDb = new DBHelperFoodDetail(DBHelper.this);
        return mFoodDetailDb;
    }

    public DBHelperFoodCalorie getFoodCalorieDb() {
        if (mFoodCalorieDb == null)
            mFoodCalorieDb = new DBHelperFoodCalorie(DBHelper.this);
        return mFoodCalorieDb;
    }
    //2019-02-25 park add
    public DBHelperCommunitySearches getmCommunitySearches(){
        if (mCommunitySearches ==null)
            mCommunitySearches = new DBHelperCommunitySearches(DBHelper.this);
        return mCommunitySearches;
    }

    public DBHelperCommunityNotice getmCommunityNotice(){
        if (mCommunityNotice ==null)
            mCommunityNotice = new DBHelperCommunityNotice(DBHelper.this);
        return mCommunityNotice;
    }
    public DBHelperAlramHealth getAlramHealth(){
        if (mDbHelperAlramHealth ==null)
            mDbHelperAlramHealth = new DBHelperAlramHealth(DBHelper.this);
        return mDbHelperAlramHealth;
    }


    public DBHelperFoodCalorieSearchHis getFoodCalorieSearchHisDb() {
        if (mFoodCalorieSearchHisDb ==null)
            mFoodCalorieSearchHisDb = new DBHelperFoodCalorieSearchHis(DBHelper.this);
        return mFoodCalorieSearchHisDb;
    }

    //2019-03-23
    public DBHelperCalorie getCalorieDb() {
        if (mCaloriesDb ==null)
            mCaloriesDb = new DBHelperCalorie(DBHelper.this);
        return mCaloriesDb;
    }

    public DBHelperLog getLogDb() {
        if (mLogDb ==null)
            mLogDb = new DBHelperLog(DBHelper.this);
        return mLogDb;
    }

}