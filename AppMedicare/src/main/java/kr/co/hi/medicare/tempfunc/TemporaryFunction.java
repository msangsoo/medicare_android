package kr.co.hi.medicare.tempfunc;

import android.app.Activity;
import android.content.Intent;

import java.text.SimpleDateFormat;
import java.util.Date;

import kr.co.hi.medicare.R;

import kr.co.hi.medicare.activity.MainActivityMedicare;

/**
 * 2019-02-23 park
 * 기존 메디 소스 페이지로 이동시 임시로 사용
 */
public class TemporaryFunction {
    public static final int SEC = 60;
    public static final int MIN = 60;
    public static final int HOUR = 24;
    public static final int DAY = 30;
    public static final int MONTH = 12;



    public static void MoveToActivity(Activity activity, Class cls){

        Intent intent = new Intent(activity,cls);
        if(intent!=null&&activity!=null) {
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
            if(activity.getClass()!=MainActivityMedicare.class)
                activity.finish();

        }

    }

    public static String getDateFormat(String date_s) {

        if(date_s==null||date_s.equals(""))
            return "";

//        SimpleDateFormat new_format = new SimpleDateFormat("yy년MM월dd일 a hh시mm분");

        SimpleDateFormat original_format = new SimpleDateFormat("yyyy-MM-dd a hh:mm:ss");
        Date date = null;

        try{
           date = original_format.parse(date_s);
        }catch (Exception e){
            e.printStackTrace();
        }

        long curTime = System.currentTimeMillis();
        long regTime = date.getTime();
        long diffTime = (curTime - regTime) / 1000;

        String msg = null;

        if(diffTime<0)
            diffTime = 0;

//        //해당시간으로부터 10일이상 지났으면
//        if(diffTime/SEC/MIN/HOUR>9){
//            msg = new_format.format(date);
//
//        }else{
            if(diffTime < SEC) {
                // sec
//                msg = diffTime + "초전";
                msg = "1분전";
            } else if ((diffTime /= SEC) < MIN) {
                // min
                System.out.println(diffTime);
                msg = diffTime + "분전";
            } else if ((diffTime /= MIN) < HOUR) {
                // hour
                msg = (diffTime ) + "시간전";
            } else{
                msg = (diffTime/=HOUR) +"일전";
            }
//            else if ((diffTime /= HOUR) < DAY) {
//                // day
//                msg = (diffTime ) + "일전";
//            }

//            else if ((diffTime /= DAY) < MONTH) {
//                // day
//                msg = (diffTime ) + "달전";
//            } else {
//                msg = (diffTime/=MONTH) + "년전";
//            }


//        }


        return msg;
    }



}
