package kr.co.hi.medicare.fragment.community.holder;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.community.data.CommunityCommentData;
import kr.co.hi.medicare.fragment.community.data.CommunityCommentInfo;
import kr.co.hi.medicare.tempfunc.TemporaryFunction;

public class CommunityCommentListViewHolder extends RecyclerView.ViewHolder{

    public ImageView comment_profile,comment_profile_teduri; // 게시자 사진
    public TextView comment_nick; // 닉네임
    public TextView comment_date; // 날짜
    public TextView comment; // 댓글내용
    public ImageView btn_delete; // 삭제
    private String SEQ;

    public CommunityCommentListViewHolder(View view,String SEQ, Context context){
        super(view);
        comment_profile_teduri = view.findViewById(R.id.comment_profile_teduri);
        comment_profile = view.findViewById(R.id.comment_profile);
        comment_nick = view.findViewById(R.id.comment_nick);
        comment_date = view.findViewById(R.id.comment_date);
        btn_delete = view.findViewById(R.id.btn_delete);
        comment = view.findViewById(R.id.comment);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(null, view, context);


        //커뮤니티
        btn_delete.setOnTouchListener(ClickListener);


        //코드부여
        btn_delete.setContentDescription(context.getString(R.string.comunity_comand_del));

        this.SEQ = SEQ;
    }

    public void SetView(CommunityCommentData data, Context context){
        comment_nick.setText(data.NICK);
        comment_date.setText(TemporaryFunction.getDateFormat(data.REGDATE)); //파싱필요

        if(data.CM_CONTENT.indexOf("@")==0&&data.CM_CONTENT.contains(" ")){
            setMessageBold(comment, data.CM_CONTENT.replaceFirst("@",""),0, data.CM_CONTENT.replaceFirst("@","").indexOf(" ") );
        }else{

            int T_MBER_SN = 0;
            try{
                T_MBER_SN = Integer.parseInt(data.T_MBER_SN);
            }catch (Exception e){
                T_MBER_SN = 0;
            }

            if(T_MBER_SN>0&&data.CM_CONTENT.indexOf("@")==0) {
                comment.setText(data.CM_CONTENT.replaceFirst("@", ""));
            }else {
                comment.setText(data.CM_CONTENT);
            }
        }

        CommonFunction.setProfile(context, data.PROFILE_PIC, comment_profile);
        CommonFunction.setProfileTeduri(context, data.MBER_GRAD, comment_profile_teduri);

        if(data.OSEQ.equals(SEQ)){
            btn_delete.setVisibility(View.VISIBLE);
        }else{
            btn_delete.setVisibility(View.INVISIBLE);
        }


    }

    private void setMessageBold(TextView view,String message, int start, int end){
        SpannableStringBuilder sp = new SpannableStringBuilder(message);
        sp.setSpan( new StyleSpan(Typeface.BOLD),start,end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        view.setText(sp);
    }

}

