package kr.co.hi.medicare.fragment.login.join;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.iid.FirebaseInstanceId;
import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.common.DummyWebviewFragment;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_mber_sms_check;
import kr.co.hi.medicare.net.data.Tr_mber_sms_proc;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 * Created by MrsWin on 2017-02-16.
 */

public class JoinStepSimpleFragment extends BaseFragmentMedi {
    private final String TAG = getClass().getSimpleName();
    private CheckBox mCheckBoxAll;
    private TextView mContractTvAll;
    private CheckBox mCheckBox1;
    private TextView mContractTv1;
    private CheckBox mCheckBox2;
    private TextView mContractTv2;
    private CheckBox mCheckBox3;
    private TextView mContractTv3;
    private LinearLayout joinstepsimple1agreegroup;
    private LinearLayout mSmsAuthLayout;
    private TextView joinstepsimple1contracterrortextview;
    private EditText joinstepsimple1phonenumedittext;
    private ImageView joinstepsimple1phonenumimageview;
    private Button mReqSmsBtn;
    private TextView mPhoneNumEt;
    private EditText mCodeEditEt;
    private ImageView joinstepsimple1codeconfirmbtn;
    private Button mAuthSmsBtn;
    private Button mReqReSmsBtn;
    private InputMethodManager mImm;

    public static Fragment newInstance() {
        JoinStepSimpleFragment fragment = new JoinStepSimpleFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(this.getClass().getSimpleName(), "onCreate()");
        super.onCreate(savedInstanceState);

//        Bundle bundle = new Bundle();
//        replaceFragment(JoinStep2_2Fragment.newInstance(), bundle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.join_step_simple_fragment, container, false);

        mImm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        this.mCodeEditEt = (EditText) view.findViewById(R.id.join_step_simple_code_edittext);
        this.mPhoneNumEt = (TextView) view.findViewById(R.id.join_step_simple1_phone_num_error_textview);

        this.joinstepsimple1codeconfirmbtn = (ImageView) view.findViewById(R.id.join_step_simple1_code_confirm_btn);


        this.joinstepsimple1phonenumimageview = (ImageView) view.findViewById(R.id.join_step_simple1_phone_num_imageview);
        this.joinstepsimple1contracterrortextview = (TextView) view.findViewById(R.id.join_step_simple1_contract_error_textview);
        this.joinstepsimple1agreegroup = (LinearLayout) view.findViewById(R.id.join_step_simple1_agreegroup);


        this.mSmsAuthLayout = view.findViewById(R.id.join_step_simple_sms_auth_layout);

        this.mReqSmsBtn = (Button) view.findViewById(R.id.join_step_simple_req_phonenum_btn);
        this.mAuthSmsBtn = (Button) view.findViewById(R.id.join_step_simple_auth_btn);
        this.mReqReSmsBtn = (Button) view.findViewById(R.id.join_step_simple_retry_sms_code);

        this.mContractTvAll = (TextView) view.findViewById(R.id.join_step_all_check_textview);
        this.mContractTv1 = (TextView) view.findViewById(R.id.join_step_simple_contract_textview_1);
        this.mContractTv2 = (TextView) view.findViewById(R.id.join_step_simple_contract_textview_2);
        this.mContractTv3 = (TextView) view.findViewById(R.id.join_step_simple_contract_textview_3);

        this.mCheckBoxAll = (CheckBox) view.findViewById(R.id.join_step_simple1_all_checkbox);
        this.mCheckBox1 = (CheckBox) view.findViewById(R.id.join_step_simple1_checkbox1);
        this.mCheckBox2 = (CheckBox) view.findViewById(R.id.join_step_simple1_checkbox2);
        this.mCheckBox3 = (CheckBox) view.findViewById(R.id.join_step_simple1_checkbox3);

        this.mPhoneNumEt = (EditText) view.findViewById(R.id.join_step_simple1_phone_num_edittext);

        mSmsAuthLayout.setVisibility(View.GONE);

        mPhoneNumEt.addTextChangedListener(mPhoneNumWatcher);
        mCodeEditEt.addTextChangedListener(mSmsNumWatcher);

        mReqSmsBtn.setOnClickListener(mClickListener);
        mAuthSmsBtn.setOnClickListener(mClickListener);
        mReqReSmsBtn.setOnClickListener(mClickListener);

        mCheckBoxAll.setOnClickListener(mClickListener);
        mContractTvAll.setOnClickListener(mClickListener);

        mContractTv1.setOnClickListener(mClickListener);
        mContractTv2.setOnClickListener(mClickListener);
        mContractTv3.setOnClickListener(mClickListener);

        mCheckBox1.setOnCheckedChangeListener(mCheckedChangeListener);
        mCheckBox2.setOnCheckedChangeListener(mCheckedChangeListener);
        mCheckBox3.setOnCheckedChangeListener(mCheckedChangeListener);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    TextWatcher mPhoneNumWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(mCheckBox1.isChecked() || mCheckBox2.isChecked() || mCheckBox3.isChecked()) {
                mReqSmsBtn.setEnabled(StringUtil.isValidPhoneNumber(s.toString()));
            }
        }
        @Override
        public void afterTextChanged(Editable s) { }
    };

    TextWatcher mSmsNumWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            Log.i(TAG, "mSmsNumWatcher.count="+count);
            int len = s.toString().length();
            mAuthSmsBtn.setEnabled((len >= 5));
        }
        @Override
        public void afterTextChanged(Editable s) { }
    };


    /**
     * 휴대폰 번호 인증
     */
    private void mberSmsProc() {
        String phoneNum = mPhoneNumEt.getText().toString();
        if (validCheck() == false) {
            return;
        }

        final Tr_mber_sms_proc.RequestData requestData = new Tr_mber_sms_proc.RequestData();
        boolean isShowProgress=true;

        requestData.token =  FirebaseInstanceId.getInstance().getToken();
        requestData.mber_hp = phoneNum;

        MediNewNetworkModule.doApi(getContext(), new Tr_mber_sms_proc(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_mber_sms_proc) {
                    Tr_mber_sms_proc data = (Tr_mber_sms_proc)responseData;
                    mReqSmsBtn.setEnabled(false);
                    // result_code: 결과코드(0000 :문자발송 성공 / 4444: 번호 없음 / 5555: 번호 2개 이상)
                    switch(data.result_code) {
                        case "0000":     // 0000: 정회원이지만 회원가입을 안한경우(가입성공)
                            mSmsAuthLayout.setVisibility(View.VISIBLE);

                            mCheckBoxAll.setEnabled(false);
                            mCheckBox1.setEnabled(false);
                            mCheckBox2.setEnabled(false);
                            mCheckBox3.setEnabled(false);
                            mPhoneNumEt.setEnabled(false);
                            mPhoneNumEt.setFocusable(false);
                            mPhoneNumEt.setFocusableInTouchMode(false);
                            mPhoneNumEt.setInputType(InputType.TYPE_NULL);
                            mReqSmsBtn.setEnabled(false);

                            mCodeEditEt.setText("");
                            mCodeEditEt.requestFocus();
                            mCodeEditEt.setSelection(0);
                            break;
                        case "4444":    //  4444: 아이디중복
                            mSmsAuthLayout.setVisibility(View.GONE);
                            CDialog.showDlg(getContext(), getString(R.string.join_simple_auth_check_message))
                                    .setIconView(R.drawable.login_m_ico_05);
                            break;
                        case "5555":
                            // 비밀번호가 틀렸습니다.
//                            CDialog.showDlg(getContext(), "번호가 2개 이상인 고객입니다.");
                            mSmsAuthLayout.setVisibility(View.GONE);
                            CDialog.showDlg(getContext(), getString(R.string.join_simple_auth_check_message1))
                                    .setIconView(R.drawable.login_m_ico_05);
                            break;
                        case "6666":
                            CDialog.showDlg(getContext(), getString(R.string.already_regist_member))
                                    .setIconView(R.drawable.join2_img_02);
                            break;
                        case "9999":    // 9999: 처리실패(준회원이지만 회원가입이 된경우 / 기타오류)
                            // 기타오류
                            mSmsAuthLayout.setVisibility(View.GONE);
                            CDialog.showDlg(getContext(), getString(R.string.comm_error_db002));
                            break;
                    }
                } else {
                    CDialog.showDlg(getContext(), getString(R.string.comm_error_db002));
                    mReqSmsBtn.setEnabled(true);
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                Log.e(TAG, response);
            }
        });
    }

    private boolean validCheck() {
        String phoneNum = mPhoneNumEt.getText().toString();
        if(mCheckBox1.isChecked()  == false  || mCheckBox2.isChecked() == false || mCheckBox3.isChecked() == false) {
            CDialog.showDlg(getContext(), "약관 동의후 해주세요.");
            return false;
        }

        if (StringUtil.isValidPhoneNumber(phoneNum) == false) {
            CDialog.showDlg(getContext(), "휴대폰 번호를 확인해주세요.");
            return false;
        }

        return true;
    }

    /**
     * 휴대폰 번호 인증
     */
    public JoinDataVo dataVo;
    private void smsCheck() {
        if (validCheck() == false)
            return;

        dataVo = new JoinDataVo();
        final Tr_mber_sms_check.RequestData requestData = new Tr_mber_sms_check.RequestData();
        boolean isShowProgress = true;

        requestData.mber_hp = mPhoneNumEt.getText().toString();
        requestData.token = FirebaseInstanceId.getInstance().getToken();
        requestData.result_key = mCodeEditEt.getText().toString();

        MediNewNetworkModule.doApi(getContext(), new Tr_mber_sms_check(), requestData, isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage = "";

                if (responseData instanceof Tr_mber_sms_check) {
                    Tr_mber_sms_check data = (Tr_mber_sms_check) responseData;
                    // result_code: 결과코드(0000: 일치 / 4444: 불일치 / 6666: 가입된 정보가 있음)
                    if ("0000".equals(data.result_code)) {

                        dataVo.setName(data.m_name);
                        dataVo.setSex(data.m_sex);
                        dataVo.setPhoneNum(data.mber_hp);
                        dataVo.setBirth(data.m_birth);
                        dataVo.setMberGrad("10");

                        Bundle bundle = new Bundle();
                        bundle.putString(JoinStep2_2FragmentMedi.BUNDLE_KEY_MBER_NM, data.m_name);
                        bundle.putBinder(JoinStep1_2Fragment.JOIN_DATA, dataVo);

                        replaceFragment(JoinStep2_2FragmentMedi.newInstance(), bundle);
                    } else if("6666".equals(data.result_code)) {
                        CDialog.showDlg(getContext(), getString(R.string.already_regist_member))
                                .setIconView(R.drawable.join2_img_02);
                    } else {
                        CDialog.showDlg(getContext(), "인증번호가 일치하지 않습니다.\n인증번호를 확인해주세요.")
                                .setIconView(R.drawable.login_m_ico_05);
                    }
                } else {
                    errorMessage = getResources().getString(R.string.networkexception);
                    CDialog.showDlg(getContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                Log.e(TAG, response);
                CDialog.showDlg(getContext(), getString(R.string.comm_error_db002));
            }
        });
    }



    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Bundle bundle = new Bundle();
            switch (v.getId()) {
                case R.id.join_step_simple1_all_checkbox :
                    boolean isChecked = mCheckBoxAll.isChecked();
                    mCheckBox1.setChecked(isChecked);
                    mCheckBox2.setChecked(isChecked);
                    mCheckBox3.setChecked(isChecked);
                    break;
                case R.id.join_step_all_check_textview :
                    boolean isCheck = !mCheckBoxAll.isChecked();
                    mCheckBoxAll.setChecked(isCheck);
                    mCheckBox1.setChecked(isCheck);
                    mCheckBox2.setChecked(isCheck);
                    mCheckBox3.setChecked(isCheck);
                    break;
                case R.id.join_step_simple1_checkbox1 :
                    mCheckBox1.setChecked(!mCheckBox1.isChecked());
                    break;
                case R.id.join_step_simple1_checkbox2 :
                    mCheckBox2.setChecked(!mCheckBox2.isChecked());
                    break;
                case R.id.join_step_simple1_checkbox3 :
                    mCheckBox3.setChecked(!mCheckBox3.isChecked());
                    break;
                case R.id.join_step_simple_req_phonenum_btn:
                    v.setEnabled(false);
                    mberSmsProc();
                    break;
                case R.id.join_step_simple_auth_btn:
                    v.setEnabled(false);
//                    mReqReSmsBtn.setEnabled(true);
                    smsCheck();
                    break;
                case R.id.join_step_simple_retry_sms_code:
                    String message = "인증문자를 받지 못하셨나요?\n5초 후에도 수신이 안되신다면\n재발송 버튼을 눌러주세요.";
                    CDialog.showDlg(getContext(), message)
                            .setNoButton("취소", null)
                            .setOkButton("재발송", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mberSmsProc();
                                }
                            });
                    break;
                case R.id.join_step_simple_contract_textview_1:
                    bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract1_title_1));
                    bundle.putString(DummyWebviewFragment.URL, getString(R.string.personal_terms_1_url));
                    NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                    break;
                case R.id.join_step_simple_contract_textview_2:
                    bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract2_title_1));
                    bundle.putString(DummyWebviewFragment.URL, getString(R.string.personal_terms_2_url));
                    NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                    break;
                case R.id.join_step_simple_contract_textview_3:
                    bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract3_title_1));
                    bundle.putString(DummyWebviewFragment.URL, getString(R.string.personal_terms_3_url));
                    NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                    break;
            }
        }
    };


    CompoundButton.OnCheckedChangeListener mCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.join_step_simple1_checkbox1 :
                case R.id.join_step_simple1_checkbox2 :
                case R.id.join_step_simple1_checkbox3 :
                    if(mCheckBox1.isChecked()  == false  || mCheckBox2.isChecked() == false || mCheckBox3.isChecked() == false) {
                        mCheckBoxAll.setChecked(false);
                        mReqSmsBtn.setEnabled(false);
                    } else if(mCheckBox1.isChecked() && mCheckBox2.isChecked() && mCheckBox3.isChecked()) {
                        mCheckBoxAll.setChecked(true);
                        String phoneNum = mPhoneNumEt.getText().toString();
                        mReqSmsBtn.setEnabled(StringUtil.isValidPhoneNumber(phoneNum));
                    }

                    break;


            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        mImm.hideSoftInputFromWindow(mPhoneNumEt.getWindowToken(), 0);
        mImm.hideSoftInputFromWindow(mCodeEditEt.getWindowToken(), 0);
    }
}
