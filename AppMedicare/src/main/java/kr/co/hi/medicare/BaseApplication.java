package kr.co.hi.medicare;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.os.Build;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import kr.co.hi.medicare.util.CLog;

/**
 * Created by zoo1 on 2016-02-04.
 */
public class BaseApplication extends Application
{
	private static BaseApplication mInstance = null;

	/**
	 * Called when the application is starting, before any activity, service,
	 * or receiver objects (excluding content providers) have been created.
	 * Implementations should be as quick as possible (for example using
	 * lazy initialization of state) since the time spent in this function
	 * directly impacts the performance of starting the first activity,
	 * service, or receiver in a process.
	 * If you override this method, be sure to call super.onCreate().
	 */
	@Override
	public void onCreate()
	{
		mInstance = this;
		super.onCreate();
		initImageLoader(getApplicationContext());                   // 이미지로더 Context 설정
	}

	public static BaseApplication getInstance()
	{
		return mInstance;
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
	}


	/**
	 * 이미지 로더 init
	 * @param context
	 */
	public void initImageLoader(Context context) {

		int memoryCacheSize;

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
			int memClass = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
			memoryCacheSize = (memClass / 8) * 1024 * 1024; // 1/8 of app memory limit
		} else {
			memoryCacheSize = 2 * 1024 * 1024;
		}

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
				.threadPriority(Thread.NORM_PRIORITY - 2)
				.memoryCacheSize(memoryCacheSize)
						//.denyCacheImageMultipleSizesInMemory()
				.discCacheFileNameGenerator(new Md5FileNameGenerator())
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				.threadPoolSize(5)
						//.imageDownloader(secureLoader)
						//.enableLogging() // Not necessary in common
				.build();

		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
		CLog.i("initImageLoader complete");
	}

}
