package kr.co.hi.medicare.common.swipeListview;


/**
 * 
 * @author baoyz
 * @date 2014-8-24
 *
 */
public interface SwipeMenuCreator {

	void create(SwipeMenu menu);
}
