package kr.co.hi.medicare.fragment.login;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.net.hwNet.ApiData;
import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.GuideActivity;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.net.hwdata.Tr_login_add_aft;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by MrsWin on 2017-02-16.
 *
 * Modifyed by park on 2019-03-15
 * 1. BaseFragment -> BaseFragmentMendi로 변경
 * 2. 마이페이지 - 회원 정보 추가 수정에서 동일한 UI를 사용하고 있어서 해당 페이지를 상속 후 일부 함수만 override하고 재사용
 * 3. join_step2_2virus_etc_edittext 추가 (질환 기타 선택시 입력 가능한 edittext)
 * 4. 디자인 가이드내용 일부 반영 -> R.layout.login_first_info_2_2_fragment의 구분선 제거 / 항목당 간격 조정 / 백그라운드 색상 화이트로 변경 / 완료 버튼 색상 및 크기 변경
 *
 */

public class LoginFirstInfoFragment2_2 extends BaseFragmentMedi {
    private final String TAG = LoginFirstInfoFragment2_2.class.getSimpleName();

    public static final String BUNDLE_KEY_HEIGHT = "bundle_key_height";
    public static final String BUNDLE_KEY_WEIGHT = "bundle_key_weight";
    public static final String BUNDLE_KEY_WEIGHT_GOAL = "bundle_key_weight_goal";

    public LinearLayout[] mActiveLayoutArr;
    public TextView[] mActiveTitleArr;
    public TextView[] mActiveContentArr;
    public CheckBox[] mDeseaseCheckboxs;

    public View[] mActiveLines;

    public EditText mDeseaseEditText;
    public RadioGroup mJobRadioGroup;
    public RadioGroup mSmokeRadioGroup;

    public View mActiveLayout;
    public int mActiveIdx = 0;

    boolean mIsRegist = false;       // 가입후 바로 들어왔는지 여부

    public Tr_login_add_aft.RequestData requestData = new Tr_login_add_aft.RequestData();

    public static Fragment newInstance() {
        LoginFirstInfoFragment2_2 fragment = new LoginFirstInfoFragment2_2();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_first_info_2_2_fragment, container, false);
        mActiveLayoutArr = new LinearLayout[]{
                view.findViewById(R.id.join_step2_2active_type1_layout)
                ,view.findViewById(R.id.join_step2_2active_type2_layout)
                ,view.findViewById(R.id.join_step2_2active_type3_layout)
        };

        mActiveTitleArr = new TextView[]{
                view.findViewById(R.id.join_step2_2active_type1_title)
                ,view.findViewById(R.id.join_step2_2active_type2_title)
                ,view.findViewById(R.id.join_step2_2active_type3_title)
        };

        mActiveContentArr = new TextView[]{
                view.findViewById(R.id.join_step2_2active_type1_content)
                ,view.findViewById(R.id.join_step2_2active_type2_content)
                ,view.findViewById(R.id.join_step2_2active_type3_content)
        };

        mActiveLines = new View[]{
                view.findViewById(R.id.join_step2_2active_type1_line_1)
                ,view.findViewById(R.id.join_step2_2active_type1_line_2)
                ,view.findViewById(R.id.join_step2_2active_type1_line_3)
        };

        mDeseaseCheckboxs = new CheckBox[]{
                (CheckBox)view.findViewById(R.id.join_step2_2virus_type1_checkbox)
                , (CheckBox)view.findViewById(R.id.join_step2_2virus_type2_checkbox)
                , (CheckBox)view.findViewById(R.id.join_step2_2virus_type3_checkbox)
                , (CheckBox)view.findViewById(R.id.join_step2_2virus_type4_checkbox)
                , (CheckBox)view.findViewById(R.id.join_step2_2virus_type5_checkbox)
                , (CheckBox)view.findViewById(R.id.join_step2_2virus_type6_checkbox)
        };

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            String height = bundle.getString(BUNDLE_KEY_HEIGHT);
            String weight = bundle.getString(BUNDLE_KEY_WEIGHT);
            String gweight = bundle.getString(BUNDLE_KEY_WEIGHT_GOAL);

            mIsRegist = getArguments().getBoolean(LoginFirstInfoFragment1_2.BUNDLE_IS_REGIST);

            UserInfo user = new UserInfo(getContext());

            requestData.mber_height = height;
            requestData.mber_bdwgh = weight;
            requestData.mber_bdwgh_goal = gweight;
            requestData.mber_actqy = "1";
            requestData.disease_nm = "5";
            requestData.disease_txt = "";
            requestData.job_yn = "Y";
            requestData.smkng_yn = "N";
            requestData.mbar_id = user.getSaveId();
        }



        for (LinearLayout ll : mActiveLayoutArr) {
            ll.setOnClickListener(mLLClickListener);
        }

        for (CheckBox checkBox : mDeseaseCheckboxs) {
            checkBox.setOnClickListener(mDiseaseCheckBoxClickListener);
        }

        mDeseaseEditText = view.findViewById(R.id.join_step2_2virus_etc_edittext);
        mDeseaseEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                v.clearFocus();
                InputMethodManager imm2 = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm2.hideSoftInputFromWindow (v.getWindowToken(),0);
                return false;
            }
        });
        ((CommonToolBar)view.findViewById(R.id.toolbar)).setLeftBtnOnclickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm2 = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm2.hideSoftInputFromWindow (mDeseaseEditText.getWindowToken(),0);
                onBackPressed();
            }
        });

        mJobRadioGroup = view.findViewById(R.id.join_step2_2job_group);
        mJobRadioGroup.setOnCheckedChangeListener(mJobCheckedChangeListener);
        mSmokeRadioGroup = view.findViewById(R.id.join_step2_2smoking_radio_group);
        mSmokeRadioGroup.setOnCheckedChangeListener(mSmokeCheckedChangeListener);

        view.findViewById(R.id.complete_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validCheck()) {
                    doUpdateInfo();
                }
            }
        });
    }


    /**
     * 활동 선택
     */
    View.OnClickListener mLLClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mActiveLayout == v) {
                mActiveLayout.setBackgroundColor(Color.TRANSPARENT);
                mActiveLayout = null;
            } else {
                for (LinearLayout ll : mActiveLayoutArr) {
                    ll.setBackgroundColor(Color.TRANSPARENT);
                }


                mActiveLayout = v;
                mActiveLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.x105_129_236));
            }
            String activeTab = mActiveLayout.getTag().toString();
            mActiveIdx = StringUtil.getIntVal(activeTab)-1;
            for (int i = 0; i < mActiveTitleArr.length; i++) {
                if (mActiveIdx == i) {
                    mActiveTitleArr[i].setTextColor(ContextCompat.getColor(getContext(), R.color.colorWhite));
                    mActiveContentArr[i].setTextColor(ContextCompat.getColor(getContext(), R.color.colorWhite));
                    mActiveLines[i].setBackgroundColor(Color.TRANSPARENT);
                } else {
                    mActiveTitleArr[i].setTextColor(ContextCompat.getColor(getContext(), R.color.x12_13_44));
                    mActiveContentArr[i].setTextColor(ContextCompat.getColor(getContext(), R.color.x143_144_158));
                    mActiveLines[i].setBackgroundColor(ContextCompat.getColor(getContext(), R.color.x_237_237_241));
                }
            }


            requestData.mber_actqy = activeTab;

//            requestData.smkng_yn = "";
//            requestData.sugar_typ = "";
//            requestData.sugar_occur_de = "";
//            requestData.mbar_id = "";
        }
    };

    /**
     * 질환체크
     */
    View.OnClickListener mDiseaseCheckBoxClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            for (CheckBox checkBox : mDeseaseCheckboxs) {
                if (v != checkBox) {
                    checkBox.setChecked(false);
                }
                else{
                    if(!checkBox.isChecked())
                        checkBox.setChecked(true);
                }
            }
            requestData.disease_nm = v.getTag().toString();

            if(v == mDeseaseCheckboxs[5] && mDeseaseCheckboxs[5].isChecked()){
                mDeseaseEditText.setVisibility(View.VISIBLE);
            }else{
                mDeseaseEditText.setVisibility(View.GONE);
            }

        }
    };

    public void getDiseaseETC(){
        if(mDeseaseCheckboxs[5].isChecked()){
            requestData.disease_txt = mDeseaseEditText.getText().toString();
        }
    }

    /**
     * 직업 선택 여부
     */
    RadioGroup.OnCheckedChangeListener mJobCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton rb = group.findViewById(checkedId);
            requestData.job_yn = rb.getTag().toString();
        }
    };

    /**
     * 흡연 선택 여부
     */
    RadioGroup.OnCheckedChangeListener mSmokeCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton rb = group.findViewById(checkedId);
            requestData.smkng_yn = rb.getTag().toString();
        }
    };

    /**
     * 직업 선택 라디오 그룹
     * @return
     */
    private int getJob() {
        return mJobRadioGroup.getCheckedRadioButtonId();
    }

    /**
     * 직업 선택 라디오 그룹
     * @return
     */
    private int getSmoke() {
        return mSmokeRadioGroup.getCheckedRadioButtonId();
    }

    private boolean validCheck() {
        if (TextUtils.isEmpty(requestData.mber_actqy)) {
            CDialog.showDlg(getContext(), "활동량을 선택해 주세요.");
            return false;
        }
        getDiseaseETC();
        if (TextUtils.isEmpty(requestData.disease_nm)) {
            CDialog.showDlg(getContext(), "보유질환을 선택해 주세요.");
            return false;
        }

        if( (requestData.disease_nm.equals("6")&&requestData.disease_txt.trim().equals(""))&&mDeseaseCheckboxs[5].isChecked()){
            CDialog.showDlg(getContext(), "보유질환을 입력해 주세요.");
            return false;
        }


        if (TextUtils.isEmpty(requestData.job_yn)) {
            CDialog.showDlg(getContext(), "직업보유 여부를 선택해 주세요.");
            return false;
        }

        return true;
    }

    //2019-03-15 park 아래 함수를 재선언 하기 위해 접근제어자 변경 private -> public
    public void doUpdateInfo() {

        getData(getContext(), Tr_login_add_aft.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_login_add_aft) {
                    Tr_login_add_aft data = (Tr_login_add_aft) obj;
                    if ("Y".equals(data.reg_yn)) {
                        //        UserInfo user = new UserInfo(getContext());

                        UserInfo user = new UserInfo(getContext());
                        final String mber_id = user.getSaveId();
                        final String mber_pwd = user.getSavedPw();
                        final boolean guide = SharedPref.getInstance().getPreferences(GuideActivity.PREF_KEY_GUIDE, true);

                        // 최초 정보 입력 후 기존 데이터 삭제 처리
                        DBHelper helper = new DBHelper(getContext());
                        helper.deleteAll();
                        SharedPref.getInstance().removeAllPreferences();    // sharedprefrence 초기화

                        //2019-04-05 park add 회원정보가 아에 초기화 되어서 is/pw 정보를 다시 넣어줌
                        user.setSavedId(mber_id);
                        user.setSavePwd(mber_pwd);
                        SharedPref.getInstance().savePreferences(GuideActivity.PREF_KEY_GUIDE, guide);

                        // 로그인 정보 갱신 재로그인
                        if (mIsRegist) {
                            // 회원가입후 들어온 경우 자동 로그인 설정
                            user.setIsAutoLogin(true);   // 자동 로그인 설정
                        }
                        // Sqlash 음식 초기데이터 true
                        SharedPref.getInstance().savePreferences(SharedPref.INTRO_FOOD_DB, true);
                        //건강메시지 On/Off
                        SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MESSAGE_CONFIRIM, false);
                        // 로그인 정보 Json 다시 저장
//                        SharedPref.getInstance().savePreferences(SharedPref.LOGIN_JSON, responseData.getJsonString());

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                doMediLoginNew(mber_id, mber_pwd);
                            }
                        }, 100);

                        // 로그인 싱글턴 정보 갱신 하기.
//                        Tr_login login = UserInfo.getLoginInfo();
//                        login.mber_height = requestData.mber_height;
//                        login.mber_bdwgh = requestData.mber_bdwgh;
//                        login.mber_bdwgh_goal = requestData.mber_bdwgh_goal;
//                        login.sugar_typ = requestData.sugar_typ;
//                        login.sugar_occur_de = requestData.sugar_occur_de;

//                        Define.getInstance().setLoginInfo(login);
//
//                        if("N".equals(data.point_alert_yn)) {
////                            replaceFragment(MainFragment.newInstance());
////                            stratGoogleClient(MainFragment.newInstance());
//                            Intent intent = new Intent(getActivity(), MainActivityMedicare.class);
//                            startActivity(intent);
//                            getActivity().finish();
//                        }
//                        else{
////                            CDialog.showDlg(getContext(),getString(R.string.first_loigin_point, StringUtil.getIntVal(data.point_alert_amt)));
//
//                            CDialog.showDlg(getContext(), getString(R.string.first_loigin_point, StringUtil.getIntVal(data.point_alert_amt)), new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
////                                    replaceFragment(MainFragment.newInstance());
//                                    Intent intent = new Intent(getActivity(), MainActivityMedicare.class);
//                                    startActivity(intent);
//                                    getActivity().finish();
//                                }
//                            });
//                        }

                        /*CDialog.showDlg(getContext(), "정보가 수정 되었습니다.", new CDialog.DismissListener() {
                            @Override
                            public void onDissmiss() {
                                onBackPressed();
                            }
                        });*/

                    } else {
                        CDialog.showDlg(getContext(), "사용자 정보 등록에 실패 하였습니다.");
                    }
                }
            }
        });
    }

    /**
     * 신규 로그인 전문
     */
    private void doMediLoginNew(String userId, String userPwd) {
        final Tr_login.RequestData requestData = new Tr_login.RequestData();

        requestData.mber_id = userId;
        requestData.mber_pwd = userPwd;

        MediNewNetworkModule.doApi(getContext(), Tr_login.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_login) {
                    Tr_login data = (Tr_login)responseData;
                    try {

                        String resultCode = data.log_yn;
                        if ("Y".equals(resultCode)) {
                            CDialog.showDlg(getContext(), "메디케어서비스 회원이 되신것을\n환영합니다!!", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
//                                    replaceFragment(MainFragment.newInstance());
                                    SharedPref.getInstance().savePreferences(GuideActivity.PREF_KEY_GUIDE, false);    // 가이드 보지 않기
                                    UserInfo user = new UserInfo(getContext());
                                    user.setIsAutoLogin(true); // 오토로그인
                                    Intent intent = new Intent(getActivity(), MainActivityMedicare.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    startActivity(intent);
                                    getActivity().finish();
                                    getActivity().overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
                                }
                            });
                        } else {
                            // 존재하지 않는 사용자.
//                            user.setIsAutoLogin(false);
//                            UIThread(LoginActivityMedicare.this, getString(R.string.confirm), getString(R.string.login_wrong_id));
                            CDialog.showDlg(getContext(), "사용자 정보 등록에 실패 하였습니다.");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
//                        CLog.e(e.toString());
                        CDialog.showDlg(getContext(), "사용자 정보 등록에 실패 하였습니다.");
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                Log.e(TAG, "onFailure.statusCode="+statusCode+", response="+response);
                CDialog.showDlg(getContext(), "사용자 정보 등록에 실패 하였습니다.");
            }
        });
    }

    @Override
    public void onResume() {
        Logger.d(this.getClass().getSimpleName(), "onResume()");
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}