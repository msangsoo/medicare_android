package kr.co.hi.medicare.fragment.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.data.Tr_mber_home_keyword_list;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.utilhw.IntentUtil;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 *
 **/
public class KeywordContentFragment extends BaseFragmentMedi {
    private final String TAG = getClass().getSimpleName();

    private static final String BUNDLE_KEY_SEX = "BUNDLE_KEY_SEX";

    private boolean mIsMan = false; // 1:남성, 2:여성

    private ViewPager mViewPager;
    private PagerAdapter mAdapter;
    private TextView mCntTv;
    private TextView mSexTitleTv ;
    private String mSex;
    private String sexText;
    private String sexText10, sexText60;

    private TextView mTopKeywordTv;
    private ImageView mCaractorIv;

    private int mManImages[] = {
            R.drawable.keywords_man1
            , R.drawable.keywords_man2
            , R.drawable.keywords_man3
            , R.drawable.keywords_man4
            , R.drawable.keywords_man5
            , R.drawable.keywords_man6
    };

    private int mWomanImages[] = {
            R.drawable.keywords_woman1
            , R.drawable.keywords_woman2
            , R.drawable.keywords_woman3
            , R.drawable.keywords_woman4
            , R.drawable.keywords_woman5
            , R.drawable.keywords_woman6
    };

    private ImageView[] mIndicators;

    public static Fragment newInstance(String sex) {
        KeywordContentFragment fragment = new KeywordContentFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_KEY_SEX, sex);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frament_keyword_content_layout, container, false);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        user = new UserInfo(getContext());
//        String sex = getar
        Bundle bundle = getArguments();
        mSex = bundle.getString(BUNDLE_KEY_SEX);
        mIsMan = "1".equals(mSex);

        String sexStr = ("1".equals(mSex) ? getString(R.string.text_mail) : getString(R.string.text_femail));
//        ((TextView)view.findViewById(R.id.keyword_sex_tv)).setText(sexStr);
        mSexTitleTv = view.findViewById(R.id.keyword_sex_tv);
        sexText = "대 "+sexStr+"은"; // 30대 남성은
        sexText10 = "대 이하 자녀를 둔 "+sexStr+"은"; // 30대 남성은
        sexText60 = "대 이상 "+sexStr+"은";

        mViewPager = view.findViewById(R.id.keyword_viewpager);
        mCntTv = view.findViewById(R.id.keyword_count_tv);

        mIndicators = new ImageView[]{
                view.findViewById(R.id.keyword_indicator_1)
                , view.findViewById(R.id.keyword_indicator_2)
                , view.findViewById(R.id.keyword_indicator_3)
                , view.findViewById(R.id.keyword_indicator_4)
                , view.findViewById(R.id.keyword_indicator_5)
                , view.findViewById(R.id.keyword_indicator_6)
        };

        mTopKeywordTv = view.findViewById(R.id.keyword_top_keyword_tv);
        mCaractorIv = view.findViewById(R.id.keyword_caractor_iv);
        // 건강상담받기전화
        view.findViewById(R.id.keyword_call_health_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tr_login userInfo = UserInfo.getLoginInfo();
                if ("10".equals(userInfo.mber_grad)) {
                    IntentUtil.requestPhoneDialActivity(getContext(), getString(R.string.health_call_num));
                } else {
                    CDialog dlg = CDialog.showDlg(getContext(), "현대해상 메디케어서비스 미 가입자는\n상담하기를 이용할 수 없습니다.\n\n서비스 가입 및 문의는\n1588-5656에서 가능합니다.")
                            .setIconView(R.drawable.login_icon3);
                }

            }
        });
        // 건강상담받기
        view.findViewById(R.id.keyword_go_medicare_service_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(MainActivityMedicare.INTENT_KEY_MOVE_MENU, MainActivityMedicare.HOME_MENU_4);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
            }
        });


        //click 저장
        OnClickListener ClickListener = new OnClickListener(null, view, getContext());


        //홈
        view.findViewById(R.id.keyword_call_health_btn).setOnTouchListener(ClickListener);
        view.findViewById(R.id.keyword_go_medicare_service_btn).setOnTouchListener(ClickListener);

        //코드부여
        view.findViewById(R.id.keyword_call_health_btn).setContentDescription(getString(R.string.keyword_call_health_btn));
        view.findViewById(R.id.keyword_go_medicare_service_btn).setContentDescription(getString(R.string.keyword_go_medicare_service_btn));




        for (ImageView iv : mIndicators) {
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int idx = StringUtil.getIntVal(v.getTag().toString());
                    mViewPager.setCurrentItem(idx);
                }
            });
        }
        mAdapter = new PagerAdapter();
        mViewPager.setAdapter(mAdapter);
        initPager();
//        getKeywordData();
    }

    private void setMessageColor(String message, int start, int end, int start1, int end1){
        SpannableStringBuilder sp = new SpannableStringBuilder(message);
        sp.setSpan( new ForegroundColorSpan(getResources().getColor(R.color.x105_129_236)),start,end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sp.setSpan( new StyleSpan(android.graphics.Typeface.BOLD), start, end, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

        SpannableStringBuilder sp1 = new SpannableStringBuilder(sp);
        sp1.setSpan( new ForegroundColorSpan(getResources().getColor(R.color.x105_129_236)),start1,end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sp1.setSpan( new StyleSpan(android.graphics.Typeface.BOLD), start1, end1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        mCntTv.setText(sp1);
    }

    public void initPager() {

        if (getParentFragment() instanceof KeywordMainFragment) {
            Tr_mber_home_keyword_list tr = ((KeywordMainFragment)getParentFragment()).getTr();
            if (tr != null)
                mCntTv.setText(String.format(getString(R.string.keyword_bottom_text),StringUtil.getFormatPrice(tr.counsel_cnt),StringUtil.getFormatPrice(tr.tot_counsel_cnt)));
            setMessageColor(mCntTv.getText().toString(),mCntTv.getText().toString().indexOf("는 ")+1, mCntTv.getText().toString().indexOf("명"),
                    mCntTv.getText().toString().indexOf("신 ")+1,mCntTv.getText().toString().indexOf("건"));
        }

        mAdapter = new PagerAdapter();
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

            @Override
            public void onPageSelected(int position) {
                setViewpagerChange(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) { }
        });


        int ageIdx = getMyData() <=0?getMberAgeGroupServerAge():getMyData();
//        int ageIdx = getMberAgeGroupServerAge();
        Log.i(TAG, "getMberAgeGroup="+ageIdx);
        setViewpagerChange(ageIdx);
        mViewPager.setCurrentItem(ageIdx, false);
        mIndicators[ageIdx].setImageResource(R.drawable.draw_circle_x_93_122_232);
        mAdapter.notifyDataSetChanged();
    }

    private void setViewpagerChange(int position) {
        setIndicatorPosition(position);
        if ("1".equals(mSex)) {
            mCaractorIv.setImageResource(mManImages[position]);
        } else {
            mCaractorIv.setImageResource(mWomanImages[position]);
        }

        String ageGrp = (""+(position+1) *10);
        if(ageGrp.equals("10")){
            mSexTitleTv.setText(ageGrp + sexText10);
        } else if(ageGrp.equals("60")){
            mSexTitleTv.setText(ageGrp + sexText60);
        } else{
            mSexTitleTv.setText(ageGrp + sexText);
        }

        if ("1".equals(mSex)) {
            List<Tr_mber_home_keyword_list.Keyword_info_man_list> list = getParentManData(ageGrp);
            if (list != null && list.size() > 0) {
                Tr_mber_home_keyword_list.Keyword_info_man_list data = list.get(0);
                mTopKeywordTv.setText(data.keyword_nm);
            }

        } else {
            List<Tr_mber_home_keyword_list.Keyword_info_woman_list> list = getParentWomanData(ageGrp);
            if (list != null && list.size() > 0) {
                Tr_mber_home_keyword_list.Keyword_info_woman_list data = list.get(0);
                mTopKeywordTv.setText(data.keyword_nm);
            }
        }
    }

    private int getMyData() {
        if (getParentFragment() instanceof KeywordMainFragment) {
            return ((KeywordMainFragment)getParentFragment()).getMyList();
        }
        return 0;
    }


    private List<Tr_mber_home_keyword_list.Keyword_info_man_list> getParentManData(String ageKey) {
        if (getParentFragment() instanceof KeywordMainFragment) {
            return ((KeywordMainFragment)getParentFragment()).getManList(ageKey);
        }
        return null;
    }

    private List<Tr_mber_home_keyword_list.Keyword_info_woman_list> getParentWomanData(String ageKey) {
        if (getParentFragment() instanceof KeywordMainFragment) {
            return ((KeywordMainFragment)getParentFragment()).getWomanList(ageKey);
        }
        return null;
    }

    class PagerAdapter extends androidx.viewpager.widget.PagerAdapter {

        @Override
        public int getCount() {
            return mIndicators.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view.equals(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container,final int position) {

//            View view = inflater.inflate(R.layout.img_viewpager, null);
            View view = LayoutInflater.from(getContext()).inflate(R.layout.viewpager_keyword_content_layout, null, false);
            view.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            Log.i(TAG, "instantiateItem.position="+position);

            TextView[] noTvs = new TextView[] { view.findViewById(R.id.keyword_pager_item_no_1)
                                                , view.findViewById(R.id.keyword_pager_item_no_2)
                                                , view.findViewById(R.id.keyword_pager_item_no_3)
                                                , view.findViewById(R.id.keyword_pager_item_no_4)
                                                , view.findViewById(R.id.keyword_pager_item_no_5)};
            TextView[] keywordTvs = new TextView[] { view.findViewById(R.id.keyword_pager_item_keyword_1)
                                                ,view.findViewById(R.id.keyword_pager_item_keyword_2)
                                                ,view.findViewById(R.id.keyword_pager_item_keyword_3)
                                                ,view.findViewById(R.id.keyword_pager_item_keyword_4)
                                                ,view.findViewById(R.id.keyword_pager_item_keyword_5) };

            String ageGrp = (""+(position+1) *10);

            if ("1".equals(mSex)) {
                List<Tr_mber_home_keyword_list.Keyword_info_man_list> list = getParentManData(ageGrp);
                if (list != null)
                    for (int i = 0; i < list.size(); i++) {
                        Tr_mber_home_keyword_list.Keyword_info_man_list data = list.get(i);
                        noTvs[i].setText(data.view_order+"위");
                        keywordTvs[i].setText(data.keyword_nm);
                    }

            } else {
                List<Tr_mber_home_keyword_list.Keyword_info_woman_list> list = getParentWomanData(ageGrp);
                if (list != null)
                    for (int i = 0; i < list.size(); i++) {
                        Tr_mber_home_keyword_list.Keyword_info_woman_list data = list.get(i);
                        noTvs[i].setText(data.view_order+"위");
                        keywordTvs[i].setText(data.keyword_nm);
                    }
            }


            ((ViewPager) container).addView(view, 0);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public void startUpdate(ViewGroup container) {
            super.startUpdate(container);
        }
    }

    private void setIndicatorPosition(int position) {
        for (int i = 0; i < mIndicators.length ; i++) {
            ImageView iv = mIndicators[i];
            iv.setImageResource(i == position ? R.drawable.draw_circle_x_93_122_232 : R.drawable.draw_circle_44444444);
        }
    }

    /**
     * 연령대 계산
     * @return
     */
    private int getMberAgeGroup() {
        try {
            String birthStr = UserInfo.getLoginInfo().mber_lifyea;

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.KOREAN);
            Date birthDay = sdf.parse(birthStr);

            GregorianCalendar today = new GregorianCalendar();
            GregorianCalendar birth = new GregorianCalendar();
            birth.setTime(birthDay);

            int factor = 0;
            if(today.get(Calendar.DAY_OF_YEAR) < birth.get(Calendar.DAY_OF_YEAR)) {
                factor = -1;
            }

            int ageGrp = ((today.get(Calendar.YEAR) - birth.get(Calendar.YEAR)) + factor) / 10;
            ageGrp = ageGrp-1;
            if (ageGrp < 0)
                ageGrp = 0;
            Log.i(TAG, "getMberAgeGroup.birthStr="+birthStr+", ageGrp="+ageGrp);
            return ageGrp;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * 연령대 계산
     * @return
     */
    private int getMberAgeGroupServerAge() {
        try {
            String birthStr = UserInfo.getLoginInfo().age;
            int ageGrp;

            if(birthStr.length()==1){
                ageGrp=0;
                return ageGrp;
            } else if (birthStr.length()==2) {
                ageGrp = StringUtil.getIntVal(birthStr.substring(0,1))-1;
                if(ageGrp > 5){
                    ageGrp = 5;
                }
                return ageGrp;
            } else {
                ageGrp = 5;
                return ageGrp;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
