package kr.co.hi.medicare.fragment.login;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.net.hwNet.ApiData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.BaseActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.hwdata.Tr_login_id;
import kr.co.hi.medicare.utilhw.EditTextUtil;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.utilhw.ViewUtil;

/**
 * Created by MrsWin on 2017-02-16.
 * 아이디 찾기
 */

public class FindIdFragment extends BaseFragmentMedi implements NewActivity.onKeyBackPressedListener {

    private EditText mPhoneNumTv;
    private EditText mPhoneNumTv2;
    private EditText mPhoneNumTv3;

    private TextView mEmailTv;
    private TextView mEmailErrTv;
    private TextView mSerchText;


    private TextView mResultTv;
    private TextView find_pwd_button;
    private TextView mFindFailTv;

    private Button mConfrimbtn;

    private Button typeButton;
    private Boolean isPhoneCheck;

    private String mSuccesPhoneNum = "";
    private LinearLayout mIDFindSuccessLayout;

    public static Fragment newInstance() {
        FindIdFragment fragment = new FindIdFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.find_id_fragment, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        EditTextUtil.hideKeyboard(getContext(), mPhoneNumTv);
        /*EditTextUtil.hideKeyboard(getContext(), mPhoneNumTv1);
        EditTextUtil.hideKeyboard(getContext(), mPhoneNumTv2);
        EditTextUtil.hideKeyboard(getContext(), mPhoneNumTv3);*/
    }

//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        super.loadActionbar(actionBar);
//        actionBar.setActionBarTitle(getString(R.string.find_id));       // 액션바 타이틀
//    }

    private void initView(View view) {
        mPhoneNumTv = (EditText)view.findViewById(R.id.find_id_phone_num_edittext);
        /*mPhoneNumTv1 = (EditText)view.findViewById(R.id.find_id_phone_num1_edittext);
        mPhoneNumTv2 = (EditText)view.findViewById(R.id.find_id_phone_num2_edittext);
        mPhoneNumTv3 = (EditText)view.findViewById(R.id.find_id_phone_num3_edittext);*/

        mIDFindSuccessLayout = view.findViewById(R.id.id_find_success_layout);
        mFindFailTv = view.findViewById(R.id.join_step1_id_error_textview);

        mIDFindSuccessLayout.setVisibility(View.GONE);
        mFindFailTv.setVisibility(View.GONE);

        mEmailTv = (TextView)view.findViewById(R.id.find_id_edittext);
        mEmailErrTv = (TextView)view.findViewById(R.id.join_step1_id_error_textview);
        mSerchText = (TextView)view.findViewById(R.id.searched_userid);

        mResultTv = (TextView)view.findViewById(R.id.find_id_edittext);

        mPhoneNumTv.addTextChangedListener(textWatcherInput);
        mConfrimbtn = view.findViewById(R.id.find_id_phone_num_confrim_button);

        view.findViewById(R.id.find_id_phone_num_confrim_button).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.find_id_complete_btn).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.find_pwd_button).setOnClickListener(mOnClickListener);

        /** font Typeface 적용 */
        typeButton = (Button)view.findViewById(R.id.find_id_complete_btn);
        ViewUtil.setTypefacenotosanskr_bold(getContext(), typeButton);

        find_pwd_button = (TextView)view.findViewById(R.id.find_pwd_button);
        ViewUtil.setTypefacenotosanskr_bold(getContext(), typeButton);
        isPhoneCheck = false;
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            String email;

            if(getString(R.string.find_id_dont_regist).equals(mEmailTv.getText().toString())){
                email = "";
            }
            else{
                email = mEmailTv.getText().toString();
            }

            Bundle bundle = new Bundle();
            bundle.putString(FindPwdFragment.FIND_PWD_PHONE_NUM, mSuccesPhoneNum);
            bundle.putString(FindPwdFragment.FIND_PWD_EMAIL, email);

            if (R.id.find_id_complete_btn == vId) {
//                replaceFragment(_LoginFragment.newInstance(), bundle);
                getActivity().finish();
            } else if (R.id.find_id_phone_num_confrim_button == vId) {
                validPhoneNumCheck();
                if(isPhoneCheck){
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mPhoneNumTv.getWindowToken(), 0);
                    doLoginId(mPhoneNumTv.getText().toString());
                }
            } else if (R.id.find_pwd_button == vId) {
                if (getActivity() instanceof BaseActivityMedicare) {
                    ((BaseActivityMedicare)getActivity()).replaceFragment(FindPwdFragment.newInstance(), true, true, bundle);
                } else {
                    NewActivity.startActivity(getActivity(), FindPwdFragment.class, bundle);
                    getActivity().finish();
                }

            }
        }
    };

    TextWatcher textWatcherInput = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            validPhoneNumCheck();
            BtnEnableCheck();
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            Log.i("beforeTextChanged", s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {
            validPhoneNumCheck();
            BtnEnableCheck();
        }
    };

    private void BtnEnableCheck(){
        if(isPhoneCheck == true) {
            mConfrimbtn.setEnabled(true);
        }else{
            mConfrimbtn.setEnabled(false);
        }
    }

    /**
     * 전화번호 체크
     * @return
     */
    private void validPhoneNumCheck() {
        String phoneNum = "";
        phoneNum = mPhoneNumTv.getText().toString();
        if (StringUtil.isValidPhoneNumber(phoneNum) == false) {
            isPhoneCheck = false;
        }
        else{
            isPhoneCheck = true;
        }
        BtnEnableCheck();
    }
    /*private void validPhoneNumCheck() {
        mEmailErrTv.setVisibility(View.INVISIBLE);


        String phoneNum = "";
        phoneNum = mPhoneNumTv.getText().toString();
        /*phoneNum += mPhoneNumTv1.getText().toString();
        phoneNum += mPhoneNumTv2.getText().toString();
        phoneNum += mPhoneNumTv3.getText().toString();*/

        /*final String numVal = phoneNum;
        if (StringUtil.isValidPhoneNumber(phoneNum) == false) {
            CDialog.showDlg(getContext(), getString(R.string.join_step1_phone_num_error));
            return;
        } else {
            Tr_mber_reg_check_hp.RequestData requestData = new Tr_mber_reg_check_hp.RequestData();
            requestData.mber_hp = phoneNum;
            getData(getContext(), Tr_mber_reg_check_hp.class, requestData, new ApiData.IStep() {
                @Override
                public void next(Object obj) {
                    if (obj instanceof Tr_mber_reg_check_hp) {
                        Tr_mber_reg_check_hp data = (Tr_mber_reg_check_hp)obj;
                        mResultTv.setVisibility(View.VISIBLE);
                        find_pwd_button.setVisibility(View.VISIBLE);
                        typeButton.setVisibility(View.VISIBLE);
                        if ("N".equals(data.mber_hp_yn)) {
                            //mEmailErrTv.setVisibility(View.VISIBLE);
                            mEmailTv.setText(getString(R.string.find_id_dont_regist));
                            mEmailTv.setTextColor(Color.parseColor("#555555"));
                        } else {
                            doLoginId(numVal);
                        }
                    }
                }
            });
        }
    }*/

    /**
     * 아이디 찾기
     * @param phoneNum
     */
    private void doLoginId(final String phoneNum) {

        Tr_login_id.RequestData requestData = new Tr_login_id.RequestData();
        requestData.mber_hp = phoneNum;
        getData(getContext(), Tr_login_id.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_login_id) {
                    Tr_login_id data = (Tr_login_id)obj;
//                    mResultTv.setVisibility(View.VISIBLE);
//                    find_pwd_button.setVisibility(View.VISIBLE);
//                    typeButton.setVisibility(View.VISIBLE);
                    if("".equals(data.mber_id)){
//                        mEmailTv.setText(getString(R.string.find_id_dont_regist));
//                        mEmailTv.setTextColor(Color.parseColor("#555555"));
//                        mSerchText.setVisibility(View.GONE);
                        mFindFailTv.setVisibility(View.VISIBLE);
                        mIDFindSuccessLayout.setVisibility(View.GONE);
                    }
                    else{
                        mSuccesPhoneNum = phoneNum;
                        mSerchText.setVisibility(View.VISIBLE);
//                        mSerchText.setText("아이디를 찾았습니다.");
                        mEmailTv.setText(data.mber_id);

                        mConfrimbtn.setEnabled(false);

                        mFindFailTv.setVisibility(View.GONE);
                        mIDFindSuccessLayout.setVisibility(View.VISIBLE);
                    }

                }

            }
        });
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (getActivity() instanceof NewActivity)
            ((NewActivity)context).setOnKeyBackPressedListener(this);
    }

    @Override
    public void onBack() {
        getActivity().finish();
    }

}
