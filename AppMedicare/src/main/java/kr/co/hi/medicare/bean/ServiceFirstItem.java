package kr.co.hi.medicare.bean;

/**
 * Created by suwun on 2016-06-21.
 * 서비스 이력 면역세포 / 면역력,세포검사 / PET-CT
 * @since 0, 1
 */
public class ServiceFirstItem {

    private String mSEQ;
    private String mREQDATE;
    private String mUSEDATE;
    private String mSO_NAME;
    private String mUSE_MEMO;
    private String mOSEQ;
    private String mRESULT_CODE;

    public ServiceFirstItem(String seq,
                            String reqdate,
                            String usedate,
                            String sq_name,
                            String use_memo,
                            String oseq,
                            String result_code){
        this.mSEQ =   seq;
        this.mREQDATE  =   reqdate;
        this.mUSEDATE =   usedate;
        this.mSO_NAME   =   sq_name;
        this.mUSE_MEMO   =   use_memo;
        this.mOSEQ   =   oseq;
        this.mRESULT_CODE   =   result_code;
    }

    public String getmSEQ() {
        return mSEQ;
    }

    public void setmSEQ(String mSEQ) {
        this.mSEQ = mSEQ;
    }

    public String getmREQDATE() {
        return mREQDATE;
    }

    public void setmREQDATE(String mREQDATE) {
        this.mREQDATE = mREQDATE;
    }

    public String getmUSEDATE() {
        return mUSEDATE;
    }

    public void setmUSEDATE(String mUSEDATE) {
        this.mUSEDATE = mUSEDATE;
    }

    public String getmSO_NAME() {
        return mSO_NAME;
    }

    public void setmSO_NAME(String mSO_NAME) {
        this.mSO_NAME = mSO_NAME;
    }

    public String getmUSE_MEMO() {
        return mUSE_MEMO;
    }

    public void setmUSE_MEMO(String mUSE_MEMO) {
        this.mUSE_MEMO = mUSE_MEMO;
    }

    public String getmOSEQ() {
        return mOSEQ;
    }

    public void setmOSEQ(String mOSEQ) {
        this.mOSEQ = mOSEQ;
    }

    public String getmRESULT_CODE() {
        return mRESULT_CODE;
    }

    public void setmRESULT_CODE(String mRESULT_CODE) {
        this.mRESULT_CODE = mRESULT_CODE;
    }

}
