package kr.co.hi.medicare.fragment.health.step;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.component.CDialogNoScroll;
import kr.co.hi.medicare.net.data.Tr_mvm_info_sport_list;

/**
 * Created by MrsWin on 2017-04-23.
 */

public class CalorieListDialog {
    private final String TAG = CalorieListDialog.class.getSimpleName();

    private Context mContext;

    private CalorieInputFragment mBaseFragment;
    private Button dlgOkBtn;
    private RecyclerView mRecyclerView;
    private CalroieAdapter mCalroieAdapter;//ok

    private LinearLayoutManager mLayoutManager;
    private CDialogNoScroll dlg;



    /**
     * 운동리스트 Dialog
     *
     *
     */
    public CalorieListDialog(CalorieInputFragment BaseFragment, final List<Tr_mvm_info_sport_list.active_list> list , final View.OnClickListener onClickListener) {

        mBaseFragment = BaseFragment;
        View view = LayoutInflater.from(mBaseFragment.getContext()).inflate(R.layout.dialog_calorie_list, null);
        mContext = mBaseFragment.getContext();

        mRecyclerView = view.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mRecyclerView.setHasFixedSize(true);

        mCalroieAdapter = new CalroieAdapter();
        mRecyclerView.setAdapter(mCalroieAdapter);

        mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mCalroieAdapter.setData(list);


        dlgOkBtn = (Button) view.findViewById(R.id.calroie_dlg_cancle);

        dlg = CDialogNoScroll.showDlg(mBaseFragment.getContext(), view);
        dlg.setTitle("");
        dlgOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dlg.dismiss();

                if (onClickListener != null)
                    onClickListener.onClick(v);
            }
        });
    }

    class CalroieAdapter extends RecyclerView.Adapter<CalroieAdapter.ViewHolder> {
        List<Tr_mvm_info_sport_list.active_list> mList = new ArrayList<>();

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.calorie_list_item, parent, false);
            return new ViewHolder(itemView);
        }

        public void setData(List<Tr_mvm_info_sport_list.active_list> dataList) {
            mList.clear();

            mList.addAll(dataList);
            notifyDataSetChanged();
        }


        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            final Tr_mvm_info_sport_list.active_list data = mList.get(position);

            holder.exercise_name.setText(data.active_nm);
            holder.exercise_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mBaseFragment.saveExercize(data.active_seq,data.mets,data.active_nm);
                    dlg.dismiss();
                }
            });


        }

        @Override
        public int getItemCount() {
            return mList == null ? 0 : mList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView exercise_name;

            public ViewHolder(View itemView) {
                super(itemView);

                exercise_name = itemView.findViewById(R.id.exercise_name);
            }

        }

    }

}
