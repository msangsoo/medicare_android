package kr.co.hi.medicare.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.util.AlramUtil;

public class GCAlarmService extends BroadcastReceiver {
    private final String TAG = getClass().getSimpleName();

    public static int ALRAM_CODE_DAILY_MISSION = 543;

    @Override
    public void onReceive(Context context, Intent intent) {
        AlramUtil.setAlarmDailyMission(context);
        Log.i(TAG, "intent.getAction()="+intent.getAction());
        if (intent.getAction() != Intent.ACTION_BOOT_COMPLETED) {
            showPushMessage(context);
        }
    }

    /**
     * 푸시 메시지를 전달 받으면 상태표시바에 표시함
     */
    public void showPushMessage(Context context) {
        NotiDummyActivity.showPushMessage(context, ALRAM_CODE_DAILY_MISSION, context.getString(R.string.login_logo), "어제 획득한 포인트를 확인해보세요.", "5","0");
    }
}