package kr.co.hi.medicare.fragment.community;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.io.File;
import java.util.HashMap;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.BaseActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.PictureBaseFragment;
import kr.co.hi.medicare.fragment.community.adapter.CommunityProfileAdapter;
import kr.co.hi.medicare.fragment.community.adapter.LoadMoreListener;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.community.data.CommunityProfileData;
import kr.co.hi.medicare.fragment.community.data.CommunityUserData;
import kr.co.hi.medicare.fragment.community.data.ReceiveDataCode;
import kr.co.hi.medicare.fragment.community.dialog.DialogCommon;
import kr.co.hi.medicare.fragment.health.food.HttpAsyncFileTask33;
import kr.co.hi.medicare.fragment.health.food.HttpAsyncTaskInterface;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_DB012;
import kr.co.hi.medicare.net.data.Tr_mber_profile_send;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.Util;
import kr.co.hi.medicare.utilhw.cameraUtil.ProviderUtil;

import static android.app.Activity.RESULT_OK;


public class CommunityProfileFragment extends PictureBaseFragment implements LoadMoreListener, SwipeRefreshLayout.OnRefreshListener, DialogCommon.UpdateProfile{
    private final String TAG = CommunityProfileFragment.class.getSimpleName();

    public static final String KEY_PROFILE_INFO = "KEY_PROFILE_INFO";
    public static final String KEY_PRE_PAGE = "KEY_PRE_PAGE";


    private int firstVisibleItem, visibleItemCount, totalItemCount;
    private int visibleThreshold = 1;
    private CommunityProfileAdapter mAdapter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefresh;
    private LinearLayoutManager mLayoutManager;
    private int TotalPage = 1;
    private String pre_page="";

    private CommunityProfileData profileData;
    private CommunityUserData userData;
    private Tr_login myInfo;
    private LinearLayout btn_back;
    private TextView text_pre_page,level,nick,info,point,count;
    private ImageView profile, profile_teduri,btn_picture,btn_modify;
    public static Fragment newInstance() {
        CommunityProfileFragment fragment = new CommunityProfileFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_community_profile, container, false);

        myInfo = UserInfo.getLoginInfo();
        btn_back = view.findViewById(R.id.btn_back);
        text_pre_page = view.findViewById(R.id.text_pre_page);
        level = view.findViewById(R.id.level);
        nick = view.findViewById(R.id.nick);
        info = view.findViewById(R.id.info);
        point = view.findViewById(R.id.point);
        count = view.findViewById(R.id.count);
        profile = view.findViewById(R.id.profile);
        super.ivPreview = profile;
        profile_teduri = view.findViewById(R.id.profile_teduri);
        btn_picture = view.findViewById(R.id.btn_picture);
        btn_modify = view.findViewById(R.id.btn_modify);

        btn_back.setOnClickListener(mOnClickListener);
        btn_picture.setOnClickListener(mOnClickListener);
        btn_modify.setOnClickListener(mOnClickListener);

        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        recyclerView = view.findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new CommunityProfileAdapter(getContext(),mOnClickListener);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager llManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = llManager.getItemCount();
                firstVisibleItem = llManager.findFirstVisibleItemPosition();

                if (dy>0&&!mAdapter.getMore() && (totalItemCount - visibleItemCount)<= (firstVisibleItem + visibleThreshold)) {
                    onLoadMore();
                }
            }
        });

        swipeRefresh.setOnRefreshListener(this);

        Bundle bundle = getArguments();
        if (bundle != null) {
            userData = (CommunityUserData)bundle.get(KEY_PROFILE_INFO);
            pre_page = bundle.getString(KEY_PRE_PAGE);
        }


        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //글쓰기 완료 후 다시 커뮤니티 메인 페이지로 이동 시 리스트뷰 갱신
        if(resultCode==RESULT_OK) {
            if(requestCode == CommunityCommentFragment.REQUEST_CODE_COMMENTINFO){
                CommunityListViewData comm_data =(CommunityListViewData)data.getSerializableExtra(CommunityCommentFragment.REQUEST_CODE_COMMDATA);

                boolean isDel = false;
                try{
                    isDel = data.getBooleanExtra(CommunityCommentFragment.REQUEST_CODE_ISDEL,false);
                }catch (Exception e){
                    e.printStackTrace();
                }

                if(comm_data!=null) {

                    if(!isDel)
                        mAdapter.updateData(comm_data);
                    else
                        mAdapter.deleteItem(comm_data.CM_SEQ);
                }
            }

            if (requestCode == REQUEST_IMAGE_CROP) {

                if(outputFileUri!=null){
                    updateProfileToServer();
                }


            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setPrePage();
        if(userData!=null&&userData.MBER_SN!=null&&!userData.MBER_SN.trim().equals("")){
            getListFromServer(PROGRESSBAR_TYPE_CENTER, "1", userData.MBER_SN, myInfo.mber_sn);
        }
    }

    private void setCount(int value){
        if(value<1)
            count.setText(getString(R.string.comm_none));
        else
            count.setText(getString(R.string.comm_count)+ " "+ value+getString(R.string.comm_unit));
    }

    private void setPrePage(){
        if(pre_page!=null){
            text_pre_page.setText(pre_page);

        }
    }

    private void setProfileInfo(){
        if(profileData!=null){
            level.setText("Lv."+ profileData.LV);
            nick.setText(profileData.NICKNAME);
            info.setText( (profileData.MBER_SEX.equals("1") ? getResources().getString(R.string.man2) : getResources().getString(R.string.women2)) + (getDisease().equals("비공개")  ? "" : " / "+getDisease()));
            int point_i = 0;
            try{
                point_i = Integer.parseInt(profileData.TOT_POINT);
            }catch (Exception e){

            }
            point.setText(getString(R.string.comm_point)+" "+Util.makeStringComma(point_i)+" P");
            CommonFunction.setProfile(getContext(), profileData.PROFILE_PIC, profile);
            CommonFunction.setProfileTeduri(getContext(), profileData.MBER_GRAD, profile_teduri);

            if(profileData.MBER_SN.equals(myInfo.mber_sn)){
                btn_modify.setVisibility(View.VISIBLE);
                btn_picture.setVisibility(View.VISIBLE);
            }else{
                btn_modify.setVisibility(View.INVISIBLE);
                btn_picture.setVisibility(View.INVISIBLE);
            }
        }
    }

    private String getDisease(){

        String DISEASE_NM="";

        if(profileData.DISEASE_OPEN.equals("Y")){

            if(profileData.DISEASE_NM.equals("")){
                DISEASE_NM = getString(R.string.comm_reg_disease_none);
            }else{
                DISEASE_NM = profileData.DISEASE_NM;
            }

        }else{
            DISEASE_NM = getString(R.string.comm_reg_disease_no);
        }

        return DISEASE_NM;
    }


    @Override
    public void onRefresh() {
        getListFromServer(PROGRESSBAR_TYPE_TOP, "1", userData.MBER_SN, myInfo.mber_sn  );
    }

    @Override
    public void onLoadMore() {
        mAdapter.setProgressMore(true);
        mAdapter.setMore(true);
        getListFromServer(PROGRESSBAR_TYPE_BOTTOM, Integer.toString(TotalPage+1), userData.MBER_SN, myInfo.mber_sn  );
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int viewId = v.getId();
            switch (viewId){
                case R.id.btn_back:
                    onBackPressed();
                    break;
                case R.id.text:
                    int position = Integer.parseInt(v.getTag(R.id.comm_main_text).toString());
                    NewActivity.moveToCommentPage(CommunityProfileFragment.this,mAdapter.getItem(position, profileData.MBER_SN),false,getString(R.string.comm_profile),mAdapter.getItem(position,profileData.MBER_SN).CM_GUBUN);
                    break;
                case R.id.image:
                    int position2 = Integer.parseInt(v.getTag(R.id.comm_main_image).toString());
                    CommunityListViewData data = mAdapter.getItem(position2);
                    if(data!=null){
                        NewActivity.moveToImagePage(CommunityProfileFragment.this, data.CM_IMG1,data.NICK);
                    }

                    break;

                case R.id.btn_picture:
                    showSelectGalleryCamera();
                    break;
                case R.id.btn_modify:
                    openModifyProfile();
                    break;
            }

        }
    };

    /**
     * 서버에 게시글 리스트 데이터 요청 DB012
     * @param loadingType 프로그레스바 로딩 타입
     * @param PG 요청 페이지
     * @param SEQ
     * @param ME_SEQ
     */
    private void getListFromServer(final String loadingType,final String PG,String SEQ,String ME_SEQ) {
        final Tr_DB012.RequestData requestData = new Tr_DB012.RequestData();
        boolean isShowProgress=false;

        requestData.PG_SIZE = PAGE_SIZE;
        requestData.PG = PG;
        requestData.SEQ = SEQ;
        requestData.ME_SEQ = ME_SEQ;

        if(loadingType.equals(PROGRESSBAR_TYPE_CENTER))
            isShowProgress=true;

        MediNewNetworkModule.doApi(getContext(), new Tr_DB012(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                //하단 프로그레스바일 경우 프로그레스바 종료
                if(loadingType.equals(PROGRESSBAR_TYPE_BOTTOM))
                    mAdapter.setProgressMore(false);

                if (responseData instanceof Tr_DB012) {
                    Tr_DB012 data = (Tr_DB012)responseData;
                    try {

                        switch (data.RESULT_CODE){
                            case ReceiveDataCode.DB012_SUCCESS:
                                if(data.DATA!=null&&data.DATA.size()>0){
                                    int requestPage = Integer.parseInt(PG);
                                    int receivePage = Integer.parseInt(data.TOT_PAGE);
                                    if(requestPage<=receivePage){
                                        TotalPage = requestPage;
                                        if(TotalPage<2){//서버로부터 받은 페이지가 2보다 작으면 클린 후 추가
                                            mAdapter.addAllItem(data.DATA);
                                        }else{ //
                                            mAdapter.addItemMore(data.DATA);
                                        }

                                    }
                                }

                                profileData  = new CommunityProfileData();
                                profileData.NICKNAME = userData.NICKNAME;
                                profileData.MBER_SN = data.MBER_SN;
                                profileData.PROFILE_PIC = data.PROFILE_PIC;
                                profileData.LV = data.LV;
                                profileData.MBER_SEX = data.MBER_SEX;
                                profileData.DISEASE_NM = data.DISEASE_NM;
                                profileData.TOT_POINT = data.TOT_POINT;
                                profileData.DISEASE_OPEN = data.DISEASE_OPEN;
                                profileData.TOT_CNT = data.TOT_CNT;
                                profileData.MBER_NM = data.MBER_NM;
                                profileData.MBER_GRAD = data.MBER_GRAD;
//                              profileData.IS_MEMBER





                                setProfileInfo();
                                setCount(Integer.parseInt(data.TOT_CNT));
                                break;

                            default:
                                errorMessage = getResources().getString(R.string.comm_error_common_9999);
                                break;
                        }



                    } catch (Exception e) {
                        CLog.e(e.toString());
                        errorMessage = getResources().getString(R.string.comm_error_common_9999)+"\ncode:2";
                    }
                }else{
                    errorMessage = getResources().getString(R.string.comm_error_common_9999)+"\ncode:3";

                }
                switch (loadingType){
                    case PROGRESSBAR_TYPE_CENTER:

                        break;
                    case PROGRESSBAR_TYPE_BOTTOM:
                        mAdapter.setMore(false);
                        break;
                    case PROGRESSBAR_TYPE_TOP:
                        swipeRefresh.setRefreshing(false);
                        break;
                }

                if(!errorMessage.equals("")){
                    CDialog.showDlg(getContext(), errorMessage);
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                Log.e(TAG, response);
                switch (loadingType){
                    case PROGRESSBAR_TYPE_CENTER:
                        break;
                    case PROGRESSBAR_TYPE_BOTTOM:
                        mAdapter.setProgressMore(false);
                        mAdapter.setMore(false);
                        break;
                    case PROGRESSBAR_TYPE_TOP:
                        swipeRefresh.setRefreshing(false);
                        break;
                }
            }
        });
    }


    @Override
    public void updateProfile(String NICK, String DISEASE_OPEN,String DISEASE_NM) {

        if(!DISEASE_OPEN.equals("")) {

            userData.NICKNAME = NICK;
            profileData.NICKNAME = NICK;
            profileData.DISEASE_OPEN = DISEASE_OPEN;
            profileData.DISEASE_NM = DISEASE_NM;
//            switch (profileData.DISEASE_OPEN) {
//                case "Y":
//                    profileData.DISEASE_NM = userInfo.disease_nm;
//                    break;
//                case "N":
//                    profileData.DISEASE_NM = getResources().getString(R.string.comm_reg_disease_no);
//                    break;
//            }
            setProfileInfo();

        }else{
            openModifyProfile();
        }
    }

    private void openModifyProfile(){

        Tr_login user = UserInfo.getLoginInfo();

        DialogCommon.showDialog(
                getContext(),
                userData.NICKNAME,
                profileData.DISEASE_OPEN,
                CommonFunction.getDiseaseNM(user.disease_nm,user.disease_txt),
                userData.MBER_SN,
                this);

    }


    private void updateProfileToServer() {

        HashMap<String,String> params = new HashMap<>();
        params.put("mber_sn",myInfo.mber_sn);
        params.put("cmpny_code", "304");

        HttpAsyncFileTask33 rssTask = new HttpAsyncFileTask33(getString(R.string.profile_upload_url), new HttpAsyncTaskInterface() {
            @Override
            public void onPreExecute() {
                ((BaseActivityMedicare)getContext()).showProgress();
            }
            @Override
            public void onPostExecute(String data) {
            }
            @Override
            public void onError() {
                ((BaseActivityMedicare)getContext()).hideProgress();
                CDialog.showDlg(getContext(), getString(R.string.text_network_data_send_fail));
            }
            @Override
            public void onFileUploaded(String result) {

                String errorMessage="";

                try {
                    BaseData resObj = new Gson().fromJson(result, Tr_mber_profile_send.class);
                    if (resObj instanceof Tr_mber_profile_send) {
                        try {
                            final Tr_mber_profile_send resultData = (Tr_mber_profile_send) resObj;

                            switch (resultData.data_yn) {
                                case "Y":
                                    profileData.PROFILE_PIC = resultData.file_url;
//                                    CommonFunction.setProfile(getContext(), myPageData.PROFILE_PIC, profile);
//                                    CommonFunction.setUpdateLoginInfo(getContext());
                                    break;
                                default:
                                    errorMessage = "이미지가 변경되지 않았습니다.\n잠시 후 다시 시도해 주세요.";
                                    break;
                            }

                        }catch (Exception e){
                            errorMessage = getResources().getString(R.string.comm_error_db002)+"\ncode:2";
                        }
                    } else {
                        errorMessage = getResources().getString(R.string.comm_error_db002)+"\ncode:3";
                    }
                }catch (Exception e){
                    errorMessage = getResources().getString(R.string.comm_error_db002)+"\ncode:4";
                }

                if(!errorMessage.equals("")){
                    CDialog.showDlg(getContext(), errorMessage);
                }

                ((BaseActivityMedicare)getContext()).hideProgress();
            }
        });

        rssTask.setParam(new File(ProviderUtil.getOutputFilePath(outputFileUri)),"img_file",params);
        rssTask.execute();
    }


}
