package kr.co.hi.medicare.fragment.community.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.community.data.CommunityUserData;
import kr.co.hi.medicare.fragment.community.holder.ProgressViewHolder;


public class CommunityUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private View.OnClickListener onClickListener;
    private Context context;
    private ArrayList<CommunityUserData> data;

    private boolean isMoreLoading = false;

    private static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView profile,profile_teduri;
        TextView nick;

        public ViewHolder(View itemView) {
            super(itemView);
            profile = itemView.findViewById(R.id.profile);
            profile_teduri = itemView.findViewById(R.id.profile_teduri);//no
            nick = itemView.findViewById(R.id.nick);
        }

        public void SetView(CommunityUserData data,Context context){
            nick.setText(data.NICKNAME);
            CommonFunction.setProfile(context, data.PROFILE_PIC, profile);
            CommonFunction.setProfileTeduri(context,data.MBER_GRAD,profile_teduri);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position) != null ? LoadMoreListener.VIEW_ITEM : LoadMoreListener.VIEW_PROG;
    }


    public CommunityUserAdapter(Context context, View.OnClickListener onClickListener) {
        this.context = context;
        this.onClickListener = onClickListener;
        data = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType == LoadMoreListener.VIEW_ITEM) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_community_user, parent, false));
        }else{
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_progress, parent, false));
        }

    }

    public void setMore(boolean isMore) {
        this.isMoreLoading = isMore;
    }

    public boolean getMore() {
        return isMoreLoading;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ViewHolder){
            if (data.size() > 0) {
                if (position < data.size()) {
                    //뷰홀더의 자원을 초기화//
                    final ViewHolder commViewHolder = (ViewHolder) holder;
                    commViewHolder.SetView(data.get(position),context);

                    commViewHolder.nick.setTag(R.id.comm_user,position);
                    commViewHolder.profile.setTag(R.id.comm_user,position);
                    commViewHolder.nick.setOnClickListener(onClickListener);
                    commViewHolder.profile.setOnClickListener(onClickListener);
                    return;
                }

            }
        }
    }

    @Override
    public int getItemCount() {
        if (data==null) {
            return 0;
        }
        return data.size();
    }

    public CommunityUserData getData(int position){

        return data.get(position);
    }


    public void delItemAll() {
        if(data!=null){
            this.data.clear();
            notifyDataSetChanged();
        }
    }

    public void addAllItem(List<CommunityUserData> data) {
        if(this.data==null)
            this.data = new ArrayList<>();

        if(data!=null){
            this.data.clear();
            this.data.addAll(data);
            notifyDataSetChanged();
        }
    }

    public void addItemMore(List<CommunityUserData> data){
        int sizeInit = this.data.size();
        this.data.addAll(data);
        notifyItemRangeChanged(sizeInit, this.data.size());
    }


    public void addItem(CommunityUserData data) {
        if(this.data==null)
            this.data = new ArrayList<>();

        if(data!=null){
            ArrayList<CommunityUserData> datas = new ArrayList<>();
            datas.add(data);
            datas.addAll(this.data);
            this.data.clear();
            this.data = datas;
            notifyDataSetChanged();
        }
    }

    public void setProgressMore(final boolean isProgress) {
        if (isProgress) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    data.add(null);
                    notifyItemInserted(data.size() - 1);
                }
            });
        } else {

            if(data.size()>0) {
                data.remove(data.size() - 1);
                notifyItemRemoved(data.size());
            }
        }
    }


}
