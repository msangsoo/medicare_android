package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.hi.medicare.utilhw.Logger;

/**
 *
 다이어트 프로그램 신청

 Input 값 api_code : api 코드명 insures_code : 회사코드(108) mber_sn : 기존회원키값
 Output 값 api_code : api 코드명 insures_code : 회원사 코드 data_yn : 저장완료
 */


public class Tr_mvm_asstb_diet_program_req extends BaseData {
    private final String TAG = Tr_mvm_asstb_diet_program_req.class.getSimpleName();

    public static class RequestData {

        public String mber_sn; //12121212",
    }

    public Tr_mvm_asstb_diet_program_req() throws JSONException {

    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof Tr_mvm_asstb_diet_program_req.RequestData) {
            JSONObject body = new JSONObject();
            Tr_mvm_asstb_diet_program_req.RequestData data = (Tr_mvm_asstb_diet_program_req.RequestData) obj;
            body.put("api_code", getApiCode(TAG));
            body.put("insures_code", INSURES_CODE);
            body.put("mber_sn", data.mber_sn);

            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/


    @SerializedName("api_code")        //		api 코드명 string
    public String api_code;        //		api 코드명 string
    @SerializedName("insures_code")//		회원사 코드
    public String insures_code;    //		회원사 코드
    @SerializedName("mber_sn")
    public String mber_sn;
    @SerializedName("reg_yn")  // 저장 여부
    public String reg_yn;

}
