package kr.co.hi.medicare.fragment.medicareService;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.utilhw.Logger;

/**
 *
 **/
public class MedicareServiceFragment extends BaseFragmentMedi {
    private UserInfo user;
    private WebView mWebview;

    public static Fragment newInstance() {
        MedicareServiceFragment fragment = new MedicareServiceFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frament_medicare_service, container, false);
        return view;


    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CommonToolBar toolBar = getToolBar(view);
        toolBar.setVisibility(View.VISIBLE);
        toolBar.setTitle("메디케어서비스");
        toolBar.getBackBtn().setVisibility(View.GONE);


        user = new UserInfo(getContext());
        init(view);
    }

    /**
     *
     * @param view
     */
    private void init(View view) {

        mWebview = (WebView) view.findViewById(R.id.web_view);

        WebSettings settings = mWebview.getSettings();
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.setWebViewClient(new WebViewClient());
        settings.setDefaultTextEncodingName("utf-8");
        settings.setBuiltInZoomControls(true); // 줌 허용
        settings.setSupportZoom(true);
        settings.setDisplayZoomControls(false);
        settings.setUseWideViewPort(true);

        settings.setLoadWithOverviewMode(true);
//        mWebview.addJavascriptInterface(new AndroidBridge(), "HybridApp");
        mWebview.setWebViewClient(new WebViewClientClass());
        mWebview.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                new AlertDialog.Builder(getContext())
                        .setTitle("알림")
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok,
                                new AlertDialog.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        result.confirm();
                                    }
                                })
                        .setCancelable(false)
                        .create()
                        .show();
                return true;
            }
        });
        mWebview.loadUrl("https://m.shealthcare.co.kr/HL_MED/info/m_info_and.asp");
    }

    private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if( url.startsWith("http:") || url.startsWith("https:") )
            {
                return false;
            }
            // tel일경우 아래와 같이 처리해준다.
            else if (url.startsWith("tel:"))
            {
                Intent tel = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(tel);

//                IntentUtil.requestPhoneDialActivity(getContext(), tel);
                return true;
            } else if (url != null && url.startsWith("intent://")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    Intent existPackage = getContext().getPackageManager().getLaunchIntentForPackage(intent.getPackage());
                    if (existPackage != null) {
                        startActivity(intent);
                    }
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Logger.i(TAG, "onPageFinished.hideprogress");
            hideProgress();
            // progress 가 해지 안 될 경우 처리
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (getActivity() != null)
                        hideProgress();
                }
            }, 1 * 500);

            setTitle(mWebview.getTitle());
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Logger.i(TAG, "onPageStarted.showProgress");
            showProgress();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            Logger.i(TAG, "onReceivedError.hideprogress");
            hideProgress();
            // progress 가 해지 안 될 경우 처리
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (getActivity() != null)
                        hideProgress();
                }
            }, 1 * 500);
        }

        @Override
        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            super.onReceivedHttpError(view, request, errorResponse);
            Logger.i(TAG, "onReceivedHttpError.hideprogress");
            hideProgress();
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            Logger.i(TAG, "onReceivedSslError.hideprogress");
            hideProgress();
        }
    }

    @Override
    public void onBackPressed() {
        if (mWebview.canGoBack()) {
            mWebview.goBack();
        } else {
            finishStep();
        }

    }


    @Override
    public void onResume() {
        super.onResume();
    }
}
