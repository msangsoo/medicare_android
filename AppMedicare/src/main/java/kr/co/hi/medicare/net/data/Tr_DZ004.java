package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 서비스이력보기 > 맞춤형 운동 / 영양관리 서비스 이력보기

 AST_LENGTH : 배열의 원소 개수 ADDR_MASS : 배열 HN_SEQ : 운동/영양관리 일련번호 SGUBUN : 서비스구분 (A:맞춤형 운동영양서비스,B: 항암약선요리강습,C:재활운동기기 제공(워키디커피) ) REQDATE : 신청일자 ENDDATE : 종료일자 (A: 맞춤형서비스 일때만 종료일자가 나옵니다.) USEDATE : 이용일자 (A: 맞춤형서비스 일때는 이용일자가 없습니다.) SO_NAME : 서비스(제휴)기관명 USE_MEMO : 서비스 이용내역(메모) OSEQ : 회원일련번호 RESULT_CODE : 결과코드
 0000 : 조회성공 4444 : 등록된 이력이 없습니다. 6666 : 회원이 존재하지 않음 9999 : 기타오류
 */

public class Tr_DZ004 extends BaseData {

    public static class RequestData {
//        strJson={"DOCNO":"DZ002","SEQ":"0001013000015"}
        public String DOCNO;
        public String SEQ;

    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {
            JSONObject body = getBaseJsonObj();
            RequestData data = (RequestData) obj;

            body.put("DOCNO", "DZ002");
            body.put("SEQ", data.SEQ);


            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("AST_LENGTH")
    public String ast_length;

}