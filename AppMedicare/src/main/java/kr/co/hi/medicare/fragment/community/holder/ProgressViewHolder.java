package kr.co.hi.medicare.fragment.community.holder;

import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import kr.co.hi.medicare.R;

public class ProgressViewHolder extends RecyclerView.ViewHolder {
    public ProgressBar pBar;
    public ProgressViewHolder(View v) {
        super(v);
        pBar = (ProgressBar) v.findViewById(R.id.pBar);
    }
}
