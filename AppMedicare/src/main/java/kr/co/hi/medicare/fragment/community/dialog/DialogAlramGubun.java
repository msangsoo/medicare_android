package kr.co.hi.medicare.fragment.community.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import kr.co.hi.medicare.R;

public class DialogAlramGubun extends Dialog {

    private static DialogAlramGubun instance;
    private Context context;
    private TextView btn_food,btn_bloodsugar,btn_weight,btn_bp;
    private String GUBUN="";


    public static DialogAlramGubun getInstance(Context context) {
        if (instance == null||instance.getContext()!=context) {
            instance = new DialogAlramGubun(context);
        }

        instance.show();
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND|WindowManager.LayoutParams.FLAG_FULLSCREEN;
        lpWindow.dimAmount = 0.8f;
//        lpWindow.windowAnimations = R.style.PauseDialogAnimation;
        getWindow().setAttributes(lpWindow);



        setContentView(R.layout.dialog_mypage_alram_gubun);

        btn_food = findViewById(R.id.btn_food);
        btn_bloodsugar = findViewById(R.id.btn_bloodsugar);
        btn_weight = findViewById(R.id.btn_weight);
        btn_bp = findViewById(R.id.btn_bp);

        findViewById(R.id.btn_cancel).setOnClickListener(onClickListener);
        findViewById(R.id.btn_food).setOnClickListener(onClickListener);
        findViewById(R.id.btn_bloodsugar).setOnClickListener(onClickListener);
        findViewById(R.id.btn_weight).setOnClickListener(onClickListener);
        findViewById(R.id.btn_bp).setOnClickListener(onClickListener);

    }


    public static DialogAlramGubun showDialog(Context context,String GUBUN){
        DialogAlramGubun dialog = getInstance(context);
        dialog.GUBUN = GUBUN;
        return dialog;
    }


    public DialogAlramGubun(Context context) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        this.context = context;
    }


    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()){
                case R .id.btn_food:
                case R .id.btn_bloodsugar:
                case R .id.btn_weight:
                case R .id.btn_bp:
                    GUBUN = ((TextView)v).getText().toString();
                    dismiss();
                    break;

                case R.id.btn_cancel:
                    dismiss();
                    break;
            }
        }
    };

    public String getGUBUN(){
        return GUBUN;
    }

}

