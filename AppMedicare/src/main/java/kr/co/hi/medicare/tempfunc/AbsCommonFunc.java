package kr.co.hi.medicare.tempfunc;


import java.util.List;

public interface AbsCommonFunc {
    void resultCallback(Object o);
    Object data();
    void showDialog();
    void closeDialog();
}
