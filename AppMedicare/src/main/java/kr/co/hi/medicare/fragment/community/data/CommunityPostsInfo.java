package kr.co.hi.medicare.fragment.community.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommunityPostsInfo {

    @Expose
    @SerializedName("DATA")
    private List<CommunityPostsData> DATA;
    @Expose
    @SerializedName("DATA_LENGTH")
    private String DATA_LENGTH;
    @Expose
    @SerializedName("DOCNO")
    private String DOCNO;

}
