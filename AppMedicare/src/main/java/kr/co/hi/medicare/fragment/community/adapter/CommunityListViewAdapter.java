package kr.co.hi.medicare.fragment.community.adapter;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import co.lujun.androidtagview.TagView;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.fragment.community.data.CommunityDataInfo;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.community.data.UpdateDataFunction;
import kr.co.hi.medicare.fragment.community.dialog.DialogCommon;
import kr.co.hi.medicare.fragment.community.holder.CommunityListViewHolder;
import kr.co.hi.medicare.fragment.community.holder.EmptyViewHolder;
import kr.co.hi.medicare.fragment.community.holder.ProgressViewHolder;

import static kr.co.hi.medicare.fragment.community.adapter.LoadMoreListener.VIEW_EMPTY;
import static kr.co.hi.medicare.fragment.community.adapter.LoadMoreListener.VIEW_ITEM;
import static kr.co.hi.medicare.fragment.community.adapter.LoadMoreListener.VIEW_PROG;


public class CommunityListViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements UpdateDataFunction {
    private View.OnClickListener onClickListener;
    private CommunityDataInfo data;
    private Context context;
    private DialogCommon dialog;
    private TagView.OnTagClickListener onTagClickListener;
    private String SEQ;
    private String CM_GUBUN;
    private Fragment fragment;
    private boolean isMoreLoading = false;
    private int prePosition=-1;

    public CommunityListViewAdapter(Context context,View.OnClickListener onClickListener,TagView.OnTagClickListener onTagClickListener,String SEQ,String CM_GUBUN,Fragment fragment) {
        this.fragment = fragment;
        this.context = context;
        this.onClickListener = onClickListener;
        this.onTagClickListener = onTagClickListener;
        data = new CommunityDataInfo();
        this.SEQ = SEQ;
        this.CM_GUBUN = CM_GUBUN;
    }

    @Override
    public int getItemViewType(int position) {
        if(data.getSize()==0){
            return VIEW_EMPTY;
        }else{
            return data.getADDR_MASS().get(position) != null ? VIEW_ITEM : VIEW_PROG;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //리스트에 나타낼 뷰를 생성//

        switch (viewType){
            case VIEW_ITEM:
                return new CommunityListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_community, parent, false), this, SEQ, context, CM_GUBUN, fragment, new CommunityListViewHolder.setAction() {
                    @Override
                    public void setActionAfterDelete(String CM_SEQ) {
                        deleteItem(CM_SEQ);
                    }
                });
            case VIEW_EMPTY:
                return new EmptyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_emptyview, parent, false));
            default:
                return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_progress, parent, false));
        }
    }

    public void setMore(boolean isMore) {
        this.isMoreLoading = isMore;
    }

    public boolean getMore() {
       return isMoreLoading;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof CommunityListViewHolder){
            if (data.getSize() > 0) {
                if (position < data.getSize()) {

                    //뷰홀더의 자원을 초기화//
                    final CommunityListViewHolder commViewHolder = (CommunityListViewHolder) holder;
                    Log.v("positiontest","positiontest:"+position);
                    commViewHolder.btn_detail.setTag(position);
                    commViewHolder.btn_comment.setTag(position);
                    commViewHolder.comment_all.setTag(R.id.comm_main_text,position);
                    commViewHolder.text.setTag(R.id.comm_main_text,position);
                    commViewHolder.layout_whole.setTag(R.id.comm_main_text,position);


                    commViewHolder.SetView(data.getADDR_MASS().get(position),CommunityListViewHolder.TYPE_LIST,position);
                    commViewHolder.profile.setTag(R.id.comm_main_profile,position);
                    commViewHolder.profile.setOnClickListener(onClickListener);


                    commViewHolder.image.setTag(R.id.comm_main_image,data.getADDR_MASS().get(position));
                    commViewHolder.btn_comment.setOnClickListener(onClickListener);
                    commViewHolder.text.setOnClickListener(onClickListener);
                    commViewHolder.layout_whole.setOnClickListener(onClickListener);
                    commViewHolder.comment_all.setOnClickListener(onClickListener);
                    commViewHolder.image.setOnClickListener(onClickListener);
                    commViewHolder.tag.setOnTagClickListener(onTagClickListener);
                    commViewHolder.btn_detail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int pos = Integer.parseInt(view.getTag().toString());
                            data.getADDR_MASS().get(pos).ISDETAIL = true;
                            notifyDataSetChanged();
                        }
                    });

//                    if (data.getADDR_MASS().get(position).MBER_SN.equals(SEQ)) {
//                        commViewHolder.editor.setVisibility(View.VISIBLE);
//                    } else {
//                        commViewHolder.editor.setVisibility(View.GONE);
//                    }

//                    commViewHolder.editor.setTag(R.id.comm_main_editor,position);
//                    commViewHolder.editor.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//
//                            final int position = Integer.parseInt(v.getTag(R.id.comm_main_editor).toString());
//
//                            dialog = DialogCommon.showDialogEditor(context,new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    dialog.dismiss(); //게시글 수정
//
//
//                                   final CommunityListViewData data = getItem(position);
//
//
//                                   if(data.CM_IMG1!=null&&!data.CM_IMG1.equals("")) {
//                                       NewActivity.moveToWritePage(fragment, data, data.CM_IMG1);
//                                   }else{
//                                       NewActivity.moveToWritePage(fragment, data, "");
//                                   }
//                                }
//                            },new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    dialog.dismiss(); // 게시글 삭제
//                                    CDialog.showDlg(context, context.getResources().getString(R.string.comm_dialog_message_4))
//                                            .setOkButton(context.getResources().getString(R.string.comm_delete), new View.OnClickListener() {
//                                                @Override
//                                                public void onClick(View v) {
//                                                    sendDeleteWriter(context,getItem(position).CM_SEQ);
//                                                }
//                                            })
//                                            .setNoButton(context.getResources().getString(R.string.comm_cancel), null);
//
//                                }
//                            });
//
//                        }
//                    });



                    return;
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if (data == null) {
            return 0;
        }

        //현재 리스트에 등록된 개수만큼 반환//
        return data.getSize();
    }


    public void addAllData(List<CommunityListViewData> data) {
        if(this.data.getADDR_MASS()==null)
            this.data.initData();

        if(data!=null){
            this.data.getADDR_MASS().clear();
            this.data.getADDR_MASS().addAll(data);
            notifyDataSetChanged();
        }
    }

    public CommunityListViewData getItem(int position){


        return data.getADDR_MASS().get(position);
    }

    public void addItemMore(List<CommunityListViewData> data){
        int sizeInit = this.data.getSize();
        this.data.getADDR_MASS().addAll(data);
        notifyItemRangeChanged(sizeInit, this.data.getSize());
    }

    public void setProgressMore(final boolean isProgress) {
        if (isProgress) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    data.getADDR_MASS().add(null);
                    notifyItemInserted(data.getSize() - 1);
                }
            });
        } else {

            if(data.getSize()>0) {
                data.getADDR_MASS().remove(data.getSize() - 1);
                notifyItemRemoved(data.getSize());
            }
        }
    }

    public void clearAllItem() {

        if (data.getSize() > 0){
            data.getADDR_MASS().clear();
            notifyDataSetChanged();
        }

    }



    @Override
    public void setDataHeart(int position,String HCNT, String MYHEART) {

        if(data.getSize()>=position){
            data.getADDR_MASS().get(position).MYHEART=MYHEART;
            data.getADDR_MASS().get(position).HCNT = HCNT;
        }

    }

    @Override
    public void setDataComment(int position, String RCNT) {

    }

    public void updateData(CommunityListViewData comm_data) {

        if(comm_data!=null){
            Log.v("requestcode","requestcode"+comm_data.CM_SEQ+";"+comm_data.NICK);
            for(int i=0; i< data.getSize();i++){
                if(data.getADDR_MASS().get(i).CM_SEQ.equals(comm_data.CM_SEQ)){
                    data.getADDR_MASS().get(i).RNUM = comm_data.RNUM;
                    data.getADDR_MASS().get(i).RCNT = comm_data.RCNT;
                    data.getADDR_MASS().get(i).HCNT = comm_data.HCNT;
                    data.getADDR_MASS().get(i).REGDATE = comm_data.REGDATE;
                    data.getADDR_MASS().get(i).CM_TITLE = comm_data.CM_TITLE;
                    data.getADDR_MASS().get(i).NICK = comm_data.NICK;
                    data.getADDR_MASS().get(i).CM_SEQ = comm_data.CM_SEQ;
                    data.getADDR_MASS().get(i).TPAGE = comm_data.TPAGE;
                    data.getADDR_MASS().get(i).CM_IMG1 = comm_data.CM_IMG1;
                    data.getADDR_MASS().get(i).PROFILE_PIC = comm_data.PROFILE_PIC;
                    data.getADDR_MASS().get(i).CM_CONTENT = comm_data.CM_CONTENT;
                    data.getADDR_MASS().get(i).CM_TAG = comm_data.CM_TAG;
                    data.getADDR_MASS().get(i).MYHEART = comm_data.MYHEART;
                    data.getADDR_MASS().get(i).ISDETAIL = comm_data.ISDETAIL;
                    data.getADDR_MASS().get(i).MBER_SN = comm_data.MBER_SN;
                    data.getADDR_MASS().get(i).CM_GUBUN = comm_data.CM_GUBUN;
                    data.getADDR_MASS().get(i).MBER_GRAD = comm_data.MBER_GRAD;
                    data.getADDR_MASS().get(i).CM_MEAL = comm_data.CM_MEAL;
                    notifyItemChanged(i);
                    break;
                }
            }
        }
    }



    public void deleteItem(String CM_SEQ) {
        for(int i=0; i<data.getSize();i++){
            if(data.getADDR_MASS().get(i).CM_SEQ.equals(CM_SEQ)){
                data.getADDR_MASS().remove(i);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i,data.getSize());
                break;
            }
        }
    }


}
