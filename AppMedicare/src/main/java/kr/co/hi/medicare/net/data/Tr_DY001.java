package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 맞춤동영상 > 월별 맞춤동영상 보기

 1 ML_SEQ : 플레이리스트 일련번호(운동지각도 입력/수정할때 필요합니다) 2 ML_MONTH : 동영상게시 월 3 MV_GUBUN : 동영상 구분(E:재활운동 ,R: 특별레시피) 4 MOTION_CODE : 운동지각도 코드(운동지각코드 탭을 참조하세요) 5 MOTION_TITLE : 운동지각도 코드 설명 6 MV_CATE_TITLE : 동영상 카테고리 제목 7 MV_TITLE : 동영상타이틀 8 MV_NOTIFY : 동영상 주의(권장)사항 9 MV_CONTENT : 동영상 내용 10 MV_DESCRIPTION : 동영상 설명 11 MV_URL : 동영상 URL 12 MV_THUMBNAIL : 동영상 썸네일 URL 5 LMONTH : 게시월 기준 이전컨텐츠가있는 마지막 월 6 NMONTH : 게시월 기준 다음컨텐츠가 있는 월 13 OSEQ : 회원일련번호 14 RESULT_CODE : 결과코드
 0000 : 조회성공 4444 : 등록된 맞춤동영상이 없습니다. 6666 : 회원이 존재하지 않음 9999 : 기타오류
 */

public class Tr_DY001 extends BaseData {

    public static class RequestData {
//        strJson={"DOCNO":"DY001","SEQ":"0001013000015","MMONTH":"201901","MV_GUBUN":"E"}
        public String MMONTH;
        public String MV_GUBUN;
        public String SEQ;
        public String DOCNO;
    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {
            JSONObject body = getBaseJsonObj();

            RequestData data = (RequestData) obj;

            body.put("DOCNO", "DY001");
            body.put("SEQ", data.SEQ);
            body.put("MV_GUBUN", data.MV_GUBUN);
            body.put("MMONTH", data.MMONTH);

            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("OSEQ")
    public String OSEQ;
	@SerializedName("RESULT_CODE")
    public String RESULT_CODE;

}