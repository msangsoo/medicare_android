/**
 * Program Name : StringUtil.java
 * Description : Samsung Wallet
 * 
 * @author : 개발자이름
 *         ***************************************************************
 *         P R O G R A M H I S T O R Y
 *         ***************************************************************
 *         DATE : PROGRAMMER : CONTENT
 *         2013.04.09 : 개발자이름 : 수정내용
 */
package kr.co.hi.medicare.util;

import java.net.URLEncoder;

/**
 * @author KimDS
 */
public class StringUtil
{

	/**
	 * <PRE>
	 * src가 null이 아니면 src Return
	 * src가 null이면 replace Return
	 * </PRE>
	 * 
	 * jmkim9 2012. 3. 22. 오후 5:21:49
	 * 
	 * @param src
	 *            원본
	 * @param replace
	 *            replace String
	 * @return String 결과
	 */
	public static String nvl(String src, String replace)
	{
		return (src != null) ? src : replace;
	}

	// static public String NVL(String exp, String replace_with )
	// {
	// if (exp == null)
	// return replace_with;
	// else
	// return exp;
	//
	// }

	// junit
	// /**
	// * <PRE>
	// * url String 을 클릭할 수 있는 SpannableString으로 변환
	// * </PRE>
	// *
	// * chiyo 2011. 11. 3. 오후 7:36:10
	// *
	// * @param sUrl
	// * @return SpannableString
	// */
	// public static SpannableString parseURL(String sUrl)
	// {
	// SpannableString sUrlSpan = new SpannableString(sUrl);
	// sUrlSpan.setSpan(new URLSpan(sUrl), 0, sUrl.length(),
	// Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
	//
	// return sUrlSpan;
	// }

	// junit
	// /**
	// * <PRE>
	// * src의 문자열이 comp에 속하는지 Check 주로 파일 확장자 비교용으로 사용
	// * </PRE>
	// *
	// * jmkim9 2012. 3. 19. 오후 3:40:23
	// *
	// * @return boolean
	// * @param src
	// * @param comp
	// * 비교용 문자열('|'로 구분됨)
	// * @param token
	// */
	// public static boolean stringIN(String src, String comp, String token)
	// {
	// boolean ret = false;
	//
	// StringTokenizer st = new StringTokenizer(comp, token); // token으로 스트링을
	// // 자른다
	// while(st.hasMoreTokens()) // 토큰이 더 있을동안
	// {
	// if(src.toLowerCase().equals(st.nextToken()))
	// // 자른 스트링과 비교
	// ret = true;
	// }
	//
	// return ret;
	// }

	/**
	 * <PRE>
	 * String의 문자열 길이 Return (한글은 2Byte로 계산)
	 * </PRE>
	 * 
	 * jmkim 2012. 11. 22. 오후 6:14:22
	 * 
	 * @return int 결과
	 * @param str
	 *            문자열
	 */
	public static int getStringLength(String str)
	{
		int len = str.length();
		int cnt = 0;

		for(int i = 0; i < len; i++)
		{
			if(str.charAt(i) < 256) // 1바이트 문자라면...
			{
				cnt++; // 길이 1 증가
			}
			else
			{
				// 2바이트 문자라면...
				cnt += 2; // 길이 2 증가
			}
		}

		return cnt;
	}

	/**
	 * <PRE>
	 * str 문자열의 왼쪽에 size 갯수만큼 c를 추가
	 * </PRE>
	 * 
	 * jmkim 2012. 11. 22. 오후 6:16:08
	 * 
	 * @return String 결과
	 * @param str
	 *            문자열
	 * @param size
	 *            사이즈
	 * @param c
	 *            char
	 */
	public static String lefPad(String str, int size, char c)
	{
		StringBuilder builder = new StringBuilder();

		for(int i = 0; i < size; i++)
		{
			builder.append(c);
		}

		builder.append(str);

		return builder.toString();
	}

	/**
	 * <PRE>
	 * str 문자열의 오른쪽에 size 갯수만큼 c를 추가
	 * </PRE>
	 * 
	 * jmkim 2012. 11. 22. 오후 6:16:39
	 * 
	 * @return String 결과
	 * @param str
	 *            문자열
	 * @param size
	 *            사이즈
	 * @param c
	 *            char
	 */
	public static String rightPad(String str, int size, char c)
	{
		StringBuilder builder = new StringBuilder();

		builder.append(str);

		for(int i = 0; i < size; i++)
		{
			builder.append(c);
		}

		return builder.toString();
	}

	public static String encodingUrl(String a_sUrl)
	{
		String str = a_sUrl;
		String result = "";
		try
		{
			int count = 0;
			while(str.contains("/"))
			{
				if(count < 3)
				{
					count++;
					int n = str.indexOf("/");
					result = result + str.substring(0, n + 1);
					str = str.substring(n + 1);
				}
				else
				{
					int n = str.indexOf("/");

					result = result + URLEncoder.encode(str.substring(0, n), "UTF-8") + "/";
					str = str.substring(n + 1);
				}
			}
			result = result + URLEncoder.encode(str, "UTF-8");

			result = result.replaceAll("\\+", "%20");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		CLog.e("result url : " + result);
		return result;
	}
}
