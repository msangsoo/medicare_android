package kr.co.hi.medicare.fragment.community;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.net.hwNet.BaseData;

import co.lujun.androidtagview.TagView;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.adapter.CommunityTagAdapter;
import kr.co.hi.medicare.fragment.community.adapter.LoadMoreListener;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.community.data.CommunityUserData;
import kr.co.hi.medicare.fragment.community.data.SendDataCode;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_DB001;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;

import static android.app.Activity.RESULT_OK;

/**
 * Created by mrsohn on 2017. 3. 14..
 * 혈당관리
 */

public class CommunityTagFragment extends BaseFragmentMedi implements LoadMoreListener, SwipeRefreshLayout.OnRefreshListener{
    private final String TAG = CommunityTagFragment.class.getSimpleName();

    public static final String TAG_TITLE = "TAG_TITLE";
    public static final String PRE_PAGE = "PRE_PAGE";

    private int TotalPage = 1;
    private TextView title,text_pre_page;
    private LinearLayout btn_back;
    private SwipeRefreshLayout swipeRefresh;
    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private CommunityTagAdapter communityTagAdapter;
    private int firstVisibleItem, visibleItemCount, totalItemCount;
    private int visibleThreshold = 1;
    private String titleName="";
    private String prePage="";
    Tr_login user = null;

    public static Fragment newInstance() {
        CommunityTagFragment fragment = new CommunityTagFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_community_tag, container, false);

        user = UserInfo.getLoginInfo();
        text_pre_page = view.findViewById(R.id.text_pre_page);
        title = view.findViewById(R.id.title);
        Bundle bundle = getArguments();
        if (bundle != null){
            titleName = bundle.getString(TAG_TITLE);
            if(titleName==null||titleName.equals("")){
                titleName="태그";
            }
            this.title.setText(titleName.replaceAll("#",""));

            prePage = bundle.getString(PRE_PAGE);
            if(prePage==null||prePage.equals("")){
                prePage="";
            }

            this.text_pre_page.setText(prePage);

        }

        TagView.OnTagClickListener onTagClickListener = new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {
//                NewActivity.moveToTagPage(getActivity(), text, titleName);
                titleName = text;
                title.setText(titleName.replaceAll("#",""));
                communityTagAdapter.setBoldTag(titleName);
                getListFromServer(PROGRESSBAR_TYPE_CENTER,"1","",titleName);
            }

            @Override
            public void onTagLongClick(int position, String text) {

            }

            @Override
            public void onSelectedTagDrag(int position, String text) {

            }

            @Override
            public void onTagCrossClick(int position) {

            }
        };


        btn_back = view.findViewById(R.id.btn_back);
        btn_back.setOnClickListener(mOnClickListener);
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(this);
        recyclerView = view.findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        communityTagAdapter = new CommunityTagAdapter(getContext(),mOnClickListener,titleName,onTagClickListener);
        recyclerView.setAdapter(communityTagAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager llManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = llManager.getItemCount();
                firstVisibleItem = llManager.findFirstVisibleItemPosition();

                if (dy>0&&!communityTagAdapter.getMore() && (totalItemCount - visibleItemCount)<= (firstVisibleItem + visibleThreshold)) {
                    onLoadMore();
                }
            }
        });


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getListFromServer(PROGRESSBAR_TYPE_CENTER,"1","",titleName);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //글쓰기 완료 후 다시 커뮤니티 메인 페이지로 이동 시 리스트뷰 갱신
        if(resultCode==RESULT_OK) {
            if(requestCode == CommunityCommentFragment.REQUEST_CODE_COMMENTINFO){
                CommunityListViewData comm_data =(CommunityListViewData)data.getSerializableExtra(CommunityCommentFragment.REQUEST_CODE_COMMDATA);
                if(communityTagAdapter!=null){

                    boolean isDel = false;
                    try{
                        isDel = data.getBooleanExtra(CommunityCommentFragment.REQUEST_CODE_ISDEL,false);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    if(!isDel)
                        communityTagAdapter.updateData(comm_data);
                    else
                        communityTagAdapter.deleteItem(comm_data.CM_SEQ);
                }
            }
        }
    }


    @Override
    public void onRefresh()
    {
        getListFromServer(PROGRESSBAR_TYPE_TOP,"1","",titleName);
    }

    @Override
    public void onLoadMore() {
        communityTagAdapter.setProgressMore(true);
        communityTagAdapter.setMore(true);
        getListFromServer(PROGRESSBAR_TYPE_BOTTOM,Integer.toString(TotalPage+1),"",titleName);
    }


    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int viewId = v.getId();
            Bundle bundle = new Bundle();
            switch (viewId){
                case R.id.btn_back:
                    onBackPressed();
                    break;
                case R.id.profile:
                    int position = Integer.parseInt(v.getTag(R.id.comm_user).toString());
                    CommunityUserData userData = new CommunityUserData();
                    userData.MBER_SN = communityTagAdapter.getData(position).MBER_SN;
                    userData.NICKNAME = communityTagAdapter.getData(position).NICK;
                    NewActivity.moveToProfilePage(getString(R.string.comm_search_tag), userData,getActivity());
                    break;
                case R.id.text:
                    NewActivity.moveToCommentPage(CommunityTagFragment.this,communityTagAdapter.getData(Integer.parseInt(v.getTag(R.id.comm_main_text).toString())),false,getString(R.string.comm_search_tag),SendDataCode.CM_ALL);
                    break;
            }

        }
    };


    /**
     * 서버에 게시글 리스트 데이터 요청 DB002
     * @param loadingType 프로그레스바 로딩 타입
     * @param PG 요청 페이지
     * @param sword 키워드
     * @param tag 태그
     */
    private void getListFromServer(final String loadingType,final String PG,String sword,String tag) {
        final Tr_DB001.RequestData requestData = new Tr_DB001.RequestData();
        boolean isShowProgress=false;

        requestData.CMGUBUN = SendDataCode.CM_ALL;
        requestData.PG_SIZE = PAGE_SIZE;
        requestData.PG = PG;
        requestData.SEQ = user.mber_sn;
        requestData.SWORD = sword;
        requestData.CM_TAG = tag;

        if(loadingType.equals(PROGRESSBAR_TYPE_CENTER))
            isShowProgress=true;

        MediNewNetworkModule.doApi(getContext(), new Tr_DB001(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                //하단 프로그레스바일 경우 프로그레스바 종료
                if(loadingType.equals(PROGRESSBAR_TYPE_BOTTOM))
                    communityTagAdapter.setProgressMore(false);

                if (responseData instanceof Tr_DB001) {
                    Tr_DB001 data = (Tr_DB001)responseData;
                    try {
                        if(data.DATA!=null&&data.DATA.size()>0){

                            int requestPage = Integer.parseInt(PG);
                            int receivePage = Integer.parseInt(data.DATA.get(0).TPAGE);

                            if(requestPage<=receivePage){
                                TotalPage = requestPage;
                                if(TotalPage<2){//서버로부터 받은 페이지가 2보다 작으면 클린 후 추가
                                    communityTagAdapter.addAllItem(data.DATA);
                                }else{ //
                                    communityTagAdapter.addItemMore(data.DATA);
                                }
                            }
                        }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
                switch (loadingType){
                    case PROGRESSBAR_TYPE_CENTER:

                        break;
                    case PROGRESSBAR_TYPE_BOTTOM:
                        communityTagAdapter.setMore(false);
                        break;
                    case PROGRESSBAR_TYPE_TOP:
                        swipeRefresh.setRefreshing(false);
                        break;
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                Log.e(TAG, response);
                switch (loadingType){
                    case PROGRESSBAR_TYPE_CENTER:
                        break;
                    case PROGRESSBAR_TYPE_BOTTOM:
                        communityTagAdapter.setProgressMore(false);
                        communityTagAdapter.setMore(false);
                        break;
                    case PROGRESSBAR_TYPE_TOP:
                        swipeRefresh.setRefreshing(false);
                        break;
                }
            }
        });
    }


}
