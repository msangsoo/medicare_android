package kr.co.hi.medicare.fragment.health.food;

public interface HttpAsyncTaskInterface {
	
	public void onPreExecute();
	public void onPostExecute(String data);
	public void onError();
	public void onFileUploaded(String result);
}
