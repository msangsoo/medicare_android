package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;
import kr.co.hi.medicare.utilhw.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.net.hwdata.Tr_login;

/**
 푸시셋팅

 input 값
 insures_code : 회사코드
 mber_sn : 회원키
 notice_yn : 공지알림
 health_yn : 건강알림
 heart_yn : 좋아요알림
 reply_yn : 답글알림
 daily_yn : 일일미션알림
 cm_yn : 커뮤니티알림

 output값
 api_code : 호출코드명
 insures_code : 회사코드
 result_code : 코드

 0000 성공 / 4444 실패
 */


public class Tr_mber_push_set extends BaseData {
    private final String TAG = Tr_mber_push_set.class.getSimpleName();

    public Tr_mber_push_set() {
        super.conn_url = "http://m.shealthcare.co.kr/HL_MED/WebService/MED_MOBILE_CALL.asmx/MED_mobile_Call";
    }

    public static class RequestData {
        public String notice_yn; // Y"
        public String health_yn; // Y"
        public String heart_yn; // Y"
        public String reply_yn; // Y"
        public String daily_yn; // Y"
        public String cm_yn; // Y"
    }




    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof Tr_mber_push_set.RequestData) {
            JSONObject body = getBaseJsonObj(getApiCode(TAG));
            Tr_mber_push_set.RequestData data = (Tr_mber_push_set.RequestData) obj;
            Tr_login user = UserInfo.getLoginInfo();
            body.put("mber_sn", user.mber_sn);
            body.put("notice_yn", data.notice_yn);
            body.put("health_yn", data.health_yn);
            body.put("heart_yn", data.heart_yn);
            body.put("reply_yn", data.reply_yn);
            body.put("daily_yn", data.daily_yn);
            body.put("cm_yn", data.cm_yn);
            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("api_code")
    public String api_code; //
    @SerializedName("insures_code")
    public String insures_code; //
    @SerializedName("result_code")
    public String result_code; //

}
