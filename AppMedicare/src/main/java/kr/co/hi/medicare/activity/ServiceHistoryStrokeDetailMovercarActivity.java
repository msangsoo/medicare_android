package kr.co.hi.medicare.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.Defined;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.adapter.HistoryStrokeMovercarListView_Adapter;
import kr.co.hi.medicare.bean.ServiceCarMoverItem;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.AlertDialogUtil;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.Util;

/**
 * Created by suwun on 2017-02-24.
 * 서비스 이력보기 (이송차량 지원)
 * @since 0, 1
 */
public class ServiceHistoryStrokeDetailMovercarActivity extends BaseActivityMedicare {

    private UserInfo user;
    private TextView result_tv_all1 , result_tv_used2, result_tv_remain3;
    private ListView mListView;
    private int mType; // 0 이송차량 지원
    private Intent intent;

    int totalMoney=0, usedMoney=0;

    private ArrayList<ServiceCarMoverItem> mItem = new ArrayList<ServiceCarMoverItem>();
    private HistoryStrokeMovercarListView_Adapter mAdapter;

    private boolean mDataFlag = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_history_stroke_carmover);
        user = new UserInfo(this);

        init();
    }

    private void init() {

        intent = getIntent();
        if (intent != null){
            mType = intent.getIntExtra(EXTRA_SERVICETYPE , 0);
        }

        CLog.i("mType --> " + mType);


        result_tv_all1         = (TextView) findViewById(R.id.result_tv_all1);
        result_tv_used2         = (TextView) findViewById(R.id.result_tv_used2);
        result_tv_remain3         = (TextView) findViewById(R.id.result_tv_remain3);
        mListView       = (ListView) findViewById(R.id.data_list);


        requestHistoryApi(mType);

    }


    /**
     * api 호출
     * @param mtype
     */

    public void requestHistoryApi(int mtype) {

        CLog.i("mtype --> " + mtype);

        try {
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            client.setTimeout(10000);
            client.addHeader("Accept-Charset", "UTF-8");
            client.addHeader("User-Agent", new WebView(getBaseContext()).getSettings().getUserAgentString());
            Tr_login login = UserInfo.getLoginInfo();

            JSONObject jObject = new JSONObject();
            if(BuildConfig.DEBUG){
                jObject.put("SEQ", "0001013000015");
            }else{
                jObject.put("SEQ", login.seq);
            }
//            jObject.put("SEQ", "0001013000015");
            jObject.put("DOCNO", "DZ008");

            params.put("strJson", jObject.toString());
            CLog.i("url = " + Defined.COMMUNITY_HTTPS_URL + "?" + params.toString());
            client.post(Defined.COMMUNITY_HTTPS_URL, params, mAsyncHttpHandler);
        } catch (Exception e) {
            CLog.e(e.toString());
        }

    }

    private AsyncHttpResponseHandler mAsyncHttpHandler = new AsyncHttpResponseHandler() {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            CLog.i("onSuccess");

            try {

                int count = headers.length;

                for (int i = 0; i < count; i++) {
                    CLog.v(headers[i].getName() + ": " + headers[i].getValue());
                }

                String response = new String(responseBody, "UTF-8");
                CLog.i("response = " +response);
                JSONObject resultData = null;

                if(response.startsWith("<")){   // api result 문자열 시작값이 "<" 로 시작하는 경우는 xml 폼
                    try {
                        response =  Util.parseXml(response);
                    }catch(Exception e){
                        CLog.e(e.toString());
                    }

                    resultData = new JSONObject(response);

                }else{
                    resultData = new JSONObject(response);
                }
                totalMoney=0; usedMoney=0;

                String resultCode = resultData.getString("DOCNO");
                switch(resultCode){

                    case "DZ008": //
                        try {
                            String resultLength = (String) resultData.get("DATA_LENGTH");
                            totalMoney = Integer.valueOf((String) resultData.get("TOTAL_MONEY"));

                            if (!resultLength.equals("0")) {
                                JSONArray jsonArray = new JSONArray(resultData.getString("DATA"));
                                JSONObject object1 = jsonArray.getJSONObject(0);

                                switch (object1.getString("RESULT_CODE")) {
                                    case "0000":
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject object = jsonArray.getJSONObject(i);
                                            ServiceCarMoverItem item = new ServiceCarMoverItem(
                                                    object.getString("TV_SEQ"),
                                                    object.getString("REQDATE"),
                                                    object.getString("HOPEDATE"),
                                                    object.getString("USEDATE"),
                                                    object.getString("SO_NAME"),
                                                    object.getString("ODIVISION"),
                                                    object.getString("DEPARTADDRES"),
                                                    object.getString("ARRIVALADDRES"),
                                                    object.getString("DISTANCE"),
//                                                    object.getString("USE_MEMO"),
                                                    object.getString("STATE_NAME"),
                                                    object.getString("UCOST"),
                                                    String.valueOf(i+1), // 카운트
                                                    object.getString("OSEQ"),
                                                    object.getString("RESULT_CODE"));
                                            mItem.add(item);
                                            usedMoney+= Integer.valueOf(object.getString("UCOST"));
                                            mDataFlag = true;
                                        }
                                        break;
                                    case "4444":
                                        mListView.setVisibility(View.GONE);
                                        result_tv_all1.setText(Util.Comma_won(totalMoney)+" 원");
                                        result_tv_used2.setText("0 원");
                                        result_tv_remain3.setText(Util.Comma_won(totalMoney)+" 원");
                                        UIThread(ServiceHistoryStrokeDetailMovercarActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record), false);
                                        break;
                                    case "6666":
                                        UIThread(ServiceHistoryStrokeDetailMovercarActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "9999":
                                        UIThread(ServiceHistoryStrokeDetailMovercarActivity.this, getString(R.string.confirm), getString(R.string.no_user) , true);
                                        break;

                                }
                                UiThread();
                                break;
                            } else {
                                UIThread(ServiceHistoryStrokeDetailMovercarActivity.this, getString(R.string.confirm), getString(R.string.meal_tok_notdata)  , true);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                }


            } catch (Exception e) {
                CLog.e(e.toString());
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            CLog.i("onFailure");
            UIThread(ServiceHistoryStrokeDetailMovercarActivity.this, getString(R.string.confirm), getString(R.string.networkexception) , true);

        }
    };


    public void click_event(View v) {

        switch (v.getId()) {
            // 뒤로
            case R.id.activity_actrecord_ImageView_back:
                finish();
                break;
        }
    }

    @Override
    public void request(EServerAPI eServerAPI, Object obj) {}

    @Override
    public void response(Record record) throws UnsupportedEncodingException {}

    @Override
    public void networkException(Record record) {}

    private void UIThread(final Activity activity, final String confirm, final String message , final boolean aa) {
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertDialogUtil.onAlertDialog(activity, confirm, message, aa);
            }
        }, 0);
    }

    private void UiThread() {
        final Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                CLog.i("UiThread ---> ");

                if (mDataFlag) {


                    result_tv_all1.setText(Util.Comma_won(totalMoney)+" 원");
                    result_tv_used2.setText(Util.Comma_won(usedMoney)+" 원");
                    result_tv_remain3.setText(Util.Comma_won(totalMoney-usedMoney)+" 원");

                    mAdapter = new HistoryStrokeMovercarListView_Adapter(ServiceHistoryStrokeDetailMovercarActivity.this, mItem);
                    mListView.setAdapter(mAdapter);

                }
            }

        }, 0);


    }
}
