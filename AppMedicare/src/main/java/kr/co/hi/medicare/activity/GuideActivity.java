package kr.co.hi.medicare.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.adapter.GuideViewPager_Adapter;
import kr.co.hi.medicare.fragment.login.guide.GuidePage1Fragment;
import kr.co.hi.medicare.fragment.login.guide.GuidePage2Fragment;
import kr.co.hi.medicare.fragment.login.guide.GuidePage3Fragment;
import kr.co.hi.medicare.utilhw.DisplayUtil;
import kr.co.hi.medicare.utilhw.SharedPref;

/**
 * 최초 어플 실행시 어플 사용 가이드.
 */
public class GuideActivity extends AppCompatActivity {
    private ImageView activity_guide_indicator_1, activity_guide_indicator_2, activity_guide_indicator_3;
    public static final String PREF_KEY_GUIDE = "guide";    // false면 보지 않기
//    public static final String PREF_KEY_FLAG = "flag";  //

    private LinearLayout mGuideBtnLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);

        init();
    }

    private void init() {
        // 가이드 최초후 다시 안보이기.
//        SharedPreferences sp = this.getSharedPreferences(GuideActivity.PREF_KEY_GUIDE, this.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sp.edit();
//        editor.putBoolean(GuideActivity.PREF_KEY_FLAG, false);
//        editor.commit();

        ViewPager activity_guide_ViewPager_guidepager = (ViewPager) findViewById(R.id.activity_guide_ViewPager_guidepager);
        activity_guide_indicator_1 = (ImageView) findViewById(R.id.activity_guide_ImageView_page1);
        activity_guide_indicator_2 = (ImageView) findViewById(R.id.activity_guide_ImageView_page2);
        activity_guide_indicator_3 = (ImageView) findViewById(R.id.activity_guide_ImageView_page3);

        mGuideBtnLayout = findViewById(R.id.guide_button_layout);

        setupViewPager(activity_guide_ViewPager_guidepager);
        setIndicator(activity_guide_ViewPager_guidepager);

        Button guidStartBtn = (Button) findViewById(R.id.guide_start_btn);
        final CheckBox dontShowCheckBox = (CheckBox) findViewById(R.id.guide_dont_show_btn);


        guidStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPref.getInstance().savePreferences(GuideActivity.PREF_KEY_GUIDE, !dontShowCheckBox.isChecked());

                Intent intent = new Intent(GuideActivity.this, LoginActivityMedicare.class);
                startActivity(intent);
                finish();
            }
        });
    }

    /**
     * ViewPager 페이지(Fragment) 정의
     **/
    private void setupViewPager(ViewPager viewPager) {
        GuideViewPager_Adapter pageAdapter = new GuideViewPager_Adapter(getSupportFragmentManager());
        pageAdapter.addFragment(new GuidePage1Fragment());
        pageAdapter.addFragment(new GuidePage2Fragment());
        pageAdapter.addFragment(new GuidePage3Fragment());
        viewPager.setAdapter(pageAdapter);
    }

    /**
     * Indicator 리스너.
     **/
    private void setIndicator(ViewPager viewPager) {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mGuideBtnLayout.setVisibility(View.INVISIBLE);
//                activity_guide_indicator_1.setImageResource(R.drawable.draw_circle_44444444);
//                activity_guide_indicator_2.setImageResource(R.drawable.draw_circle_44444444);
//                activity_guide_indicator_3.setImageResource(R.drawable.draw_circle_44444444);
                setIndicatioWidth(activity_guide_indicator_1, false);
                setIndicatioWidth(activity_guide_indicator_2, false);
                setIndicatioWidth(activity_guide_indicator_3, false);
                switch (position) {
                    case 0:
//                        activity_guide_indicator_1.setImageResource(R.drawable.draw_app_guide_indicator);
                        setIndicatioWidth(activity_guide_indicator_1, true);
                        break;
                    case 1:
//                        activity_guide_indicator_2.setImageResource(R.drawable.draw_app_guide_indicator);
                        setIndicatioWidth(activity_guide_indicator_2, true);
                        break;
                    case 2:
//                        activity_guide_indicator_3.setImageResource(R.drawable.draw_app_guide_indicator);
                        setIndicatioWidth(activity_guide_indicator_3, true);
                        mGuideBtnLayout.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setIndicatioWidth(ImageView iv, boolean isEnable) {
        ViewGroup.LayoutParams params = iv.getLayoutParams();
        int width = DisplayUtil.getDpToPix(GuideActivity.this,isEnable ? 20 : 9);
        params.width = width;
        iv.setLayoutParams(params);
        iv.setImageResource(isEnable ? R.drawable.draw_app_guide_indicator : R.drawable.draw_circle_44444444);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPref.getInstance().initContext(this);
    }
}
