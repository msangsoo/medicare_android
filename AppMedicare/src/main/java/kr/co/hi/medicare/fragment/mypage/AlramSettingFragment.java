package kr.co.hi.medicare.fragment.mypage;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.value.Define;
import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.net.data.Tr_mber_push_set;
import kr.co.hi.medicare.util.CLog;

/**
 * Created by MrsWin on 2017-02-16.
 * 아이디 찾기
 */

public class AlramSettingFragment extends BaseFragmentMedi {
    public static final String ALRAM_DATA = "ALRAM_DATA";

    private CheckBox noticealramcheckbox;
    private CheckBox healthalramcheckbox;
    private CheckBox likealramcheckbox;
    private CheckBox commentalramcheckbox;
    private CheckBox missionalramcheckbox;

    private String notice_checked_value;
    private String health_checked_value;
    private String like_checked_value;
    private String comment_checked_value;
    private String mission_checked_value;

    private MyPageData myPageData = new MyPageData();

    public static Fragment newInstance() {
        AlramSettingFragment fragment = new AlramSettingFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mypage_alram_setting, container, false);

        Bundle bundle = getArguments();
        if(bundle!=null){
            myPageData = (MyPageData)bundle.getSerializable(ALRAM_DATA);
        }


        initView(view);


        AlramSetting();

        return view;
    }


    private void initView(View view) {

        noticealramcheckbox = (CheckBox) view.findViewById(R.id.notice_alram_checkbox);
        healthalramcheckbox = (CheckBox) view.findViewById(R.id.health_alram_checkbox);
        likealramcheckbox = (CheckBox) view.findViewById(R.id.like_alram_checkbox);
        commentalramcheckbox = (CheckBox) view.findViewById(R.id.comment_alram_checkbox);
        missionalramcheckbox = (CheckBox) view.findViewById(R.id.mission_alram_checkbox);

        noticealramcheckbox.setOnClickListener(mOnClickListener);
        healthalramcheckbox.setOnClickListener(mOnClickListener);
        likealramcheckbox.setOnClickListener(mOnClickListener);
        commentalramcheckbox.setOnClickListener(mOnClickListener);
        missionalramcheckbox.setOnClickListener(mOnClickListener);
    }

    /**
     * 알람설정 셋팅
     */
    public void AlramSetting() {
        notice_checked_value = myPageData.notice_yn;
        health_checked_value = myPageData.health_yn;
        like_checked_value = myPageData.heart_yn;
        comment_checked_value = myPageData.reply_yn;
        mission_checked_value = myPageData.daily_yn;
        CheckedSetting();
    }

    public void CheckedSetting() {

        if("Y".equals(notice_checked_value)){
            noticealramcheckbox.setChecked(true);
        }
        else{
            noticealramcheckbox.setChecked(false);
        }

        if("Y".equals(health_checked_value)){
            healthalramcheckbox.setChecked(true);
        }
        else{
            healthalramcheckbox.setChecked(false);
        }

        if("Y".equals(like_checked_value)){
            likealramcheckbox.setChecked(true);
        }
        else{
            likealramcheckbox.setChecked(false);
        }

        if("Y".equals(comment_checked_value)){
            commentalramcheckbox.setChecked(true);
        }
        else{
            commentalramcheckbox.setChecked(false);
        }

        if("Y".equals(mission_checked_value)){
            missionalramcheckbox.setChecked(true);
        }
        else{
            missionalramcheckbox.setChecked(false);
        }
    }


    /**
     * 알람설정 수정
     */
    public void AlramSetting_Chagne() {
        Tr_mber_push_set.RequestData requestData = new Tr_mber_push_set.RequestData();
        requestData.notice_yn = notice_checked_value;
        requestData.health_yn = health_checked_value;
        requestData.heart_yn = like_checked_value;
        requestData.reply_yn = comment_checked_value;
        requestData.daily_yn = mission_checked_value;
        requestData.cm_yn = "N"; //스토리 보드우선(스토리 보드 추가 시 레이아웃 추가해야함)


        MediNewNetworkModule.doApi(getContext(), Tr_mber_push_set.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_mber_push_set) {
                    Tr_mber_push_set data = (Tr_mber_push_set) responseData;
                    try {
                        if("0000".equals(data.result_code)){
                            Tr_login login = UserInfo.getLoginInfo();
                            login.notice_alert_yn = notice_checked_value;
                            login.health_alert_yn = health_checked_value;
                            login.like_alert_yn = like_checked_value;
                            login.comment_alert_yn = comment_checked_value;
                            login.mission_alert_yn = mission_checked_value;
                            Define.getInstance().setLoginInfo(login);

                            myPageData.reply_yn = comment_checked_value;
                            myPageData.daily_yn = mission_checked_value;
                            myPageData.notice_yn = notice_checked_value;
                            myPageData.health_yn = health_checked_value;
                            myPageData.heart_yn = like_checked_value;
                        }else{
                            AlramSetting(); //초기화
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                AlramSetting();
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
            }
        });

    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            switch (vId) {
                case R.id.notice_alram_checkbox :
                    if (noticealramcheckbox.isChecked()) {
                        notice_checked_value = "Y";
                    } else {
                        notice_checked_value = "N";
                    }
                    AlramSetting_Chagne();
                    break;
                case R.id.health_alram_checkbox :
                    if (healthalramcheckbox.isChecked()) {
                        health_checked_value = "Y";
                    } else {
                        health_checked_value = "N";
                    }
                    AlramSetting_Chagne();
                    break;
                case R.id.like_alram_checkbox :
                    if (likealramcheckbox.isChecked()) {
                        like_checked_value = "Y";
                    } else {
                        like_checked_value = "N";
                    }
                    AlramSetting_Chagne();
                    break;
                case R.id.comment_alram_checkbox :
                    if (commentalramcheckbox.isChecked()) {
                        comment_checked_value = "Y";
                    } else {
                        comment_checked_value = "N";
                    }
                    AlramSetting_Chagne();
                    break;
                case R.id.mission_alram_checkbox :
                    if (missionalramcheckbox.isChecked()) {
                        mission_checked_value = "Y";
                    } else {
                        mission_checked_value = "N";
                    }
                    AlramSetting_Chagne();
                    break;
            }
        }
    };

}
