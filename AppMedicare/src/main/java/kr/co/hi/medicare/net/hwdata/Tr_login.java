package kr.co.hi.medicare.net.hwdata;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.hi.medicare.utilhw.SharedPref;

/**
 *
 FUNCTION NAME	login	로그인

 input값
 insures_code : 회사코드
 token : 토큰값
 mber_id : ID값
 mber_pwd : PW
 app_code : 휴대폰버전
 phone_model : 폰모델
 pushk : 푸쉬 타입(아이폰때문 에 아이폰에만 존재 (ET:기업테스트,ER:기업배포,AT:앱스토어테스트,AR:앱스토어배포
 app_ver : 앱버전

 output 값
 api_code : 호출코드명
 insures_code : 회사코드
 mber_sn : 회원번호
 tot_basic_goal :
 day_basic_goal :
 default_basic_goal :
 log_yn :
 mber_hp_newyn :
 mber_sex : 성별
 mber_lifyea : 생년월일
 mber_nm : 회원명
 mber_hp : 회원번호
 mber_sex : 회원성별
 mber_height : 회원 신장
 mber_bdwgh : 회원 키
 mber_bdwgh_app :
 mber_bdwgh_goal : 목표체중
 mber_actqy : 활동량
 mber_grad : 회원등급
 disease_nm : 질병코드
 disease_txt : 질병명
 medicine_yn :
 smkng_yn : 흡연여부
 mber_zone :
 goal_mvm_calory :
 goal_mvm_stepcnt :
 goal_water_ntkqy :
 sugar_typ :
 sugar_occur_de :
 mber_grad :
 day_health_amt :
 accml_sum_amt : 가용포인트
 point_total_amt : 누적포인트
 sugar_alert_yn :
 health_alert_yn :
 mission_alert_yn :
 mission_walk_start_de :
 mission_walk_end_de :
 notice_yn : 공지알림
 health_yn : 건강알림
 heart_yn : 좋아요알림
 reply_yn : 답글알림
 daily_yn : 일일미션알림
 cm_yn : 커뮤니티알림
 pm_yn : 프리미엄회원여부
 st_yn : 프리미엄회원여부(뇌졸증)
 seq : 대체키(juminnum)
 nickname : 커뮤니티 닉네임
 age : 나이
 gclife_id : 임직원몰 연동된상태면 아이디출력 - 연동이안되어있으면 null
 add_reg_yn : 추가정보 입력 여부y/n

 */

public class Tr_login extends BaseData {
    private final String TAG = getClass().getSimpleName();

    public static class RequestData {
        public String mber_id;//":"moon@theinsystems.com"
        public String mber_pwd;//":"1111"
    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof Tr_login.RequestData) {
            JSONObject body = getBaseJsonObj();

            Tr_login.RequestData data = (Tr_login.RequestData) obj;

            String refreshedToken = FirebaseInstanceId.getInstance().getToken();    // 토큰값.

            body.put("api_code", "login");
            body.put("app_code", super.APP_CODE);
            body.put("insures_code", super.INSURES_CODE);
            body.put("app_ver", getAppVersion(getContext()));
            body.put("phone_model", Build.MODEL);
            body.put("token", refreshedToken);
            body.put("pushk", "");


            body.put("mber_id", data.mber_id);
            body.put("mber_pwd", data.mber_pwd);

//            if (getContext() != null) {
//
//                if (TextUtils.isEmpty(data.mber_id) == false && TextUtils.isEmpty(data.mber_pwd) == false) {
//                    SharedPref.getInstance().initContext(getContext());
//                    Log.i(TAG, "로그인 아이디 저장 ID="+data.mber_id);
//                    Log.i(TAG, "로그인 패스워드 저장 ID="+data.mber_pwd);
//                    SharedPref.getInstance().savePreferences(SharedPref.SAVED_LOGIN_ID, data.mber_id);
//                    SharedPref.getInstance().savePreferences(SharedPref.SAVED_LOGIN_PWD, data.mber_pwd);
//                }
//            }

            return body;
        }
        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    /**
     * 리시브 받은 json string 를 사용할 필요가 있을때 사용
     * @param json
     */
    @Override
    public void setJsonString(Context context, String json) {
        super.setJsonString(context, json);
        Log.i(getClass().getSimpleName(), "로그인 정보 저장");
        SharedPref.getInstance().savePreferences(SharedPref.LOGIN_JSON, json);
    }

    // 20190222 추가
    @SerializedName("st_yn")
    public String st_yn;
    @SerializedName("pm_yn")
    public String pm_yn;
    @SerializedName("cm_yn")
    public String cm_yn;
    @SerializedName("reply_yn")
    public String reply_yn;
    @SerializedName("daily_yn")
    public String daily_yn;
    @SerializedName("heart_yn")
    public String heart_yn;
    @SerializedName("health_yn")
    public String health_yn;
    @SerializedName("notice_yn")
    public String notice_yn;
    @SerializedName("add_reg_yn")
    public String add_reg_yn;
    @SerializedName("mission_walk_end_de")
    public String mission_walk_end_de;
    @SerializedName("mission_walk_start_de")
    public String mission_walk_start_de;
    @SerializedName("mission_alert_yn")
    public String mission_alert_yn;
    @SerializedName("health_alert_yn")
    public String health_alert_yn;
    @SerializedName("notice_alert_yn")
    public String notice_alert_yn;
    @SerializedName("like_alert_yn")
    public String like_alert_yn;
    @SerializedName("comment_alert_yn")
    public String comment_alert_yn;
    @SerializedName("sugar_alert_yn")
    public String sugar_alert_yn;
    @SerializedName("user_point_amt")
    public String user_point_amt;
    @SerializedName("day_health_amt")
    public String day_health_amt;
    @SerializedName("sugar_occur_de")
    public String sugar_occur_de;
    @SerializedName("sugar_typ")
    public String sugar_typ;
    @SerializedName("goal_water_ntkqy")
    public String goal_water_ntkqy;
    @SerializedName("goal_mvm_stepcnt")
    public String goal_mvm_stepcnt;
    @SerializedName("goal_mvm_calory")
    public String goal_mvm_calory;
    @SerializedName("mber_zone")
    public String mber_zone;
    @SerializedName("smkng_yn")
    public String smkng_yn;
    @SerializedName("medicine_yn")
    public String medicine_yn;
    @SerializedName("disease_nm")
    public String disease_nm;
    @SerializedName("seq")
    public String seq;
    @SerializedName("mber_grad")
    public String mber_grad;
    @SerializedName("mber_actqy")
    public String mber_actqy;
    @SerializedName("mber_bdwgh_goal")
    public String mber_bdwgh_goal;
    @SerializedName("mber_bdwgh_app")
    public String mber_bdwgh_app;
    @SerializedName("mber_bdwgh")
    public String mber_bdwgh;
    @SerializedName("mber_height")
    public String mber_height;
    @SerializedName("mber_hp")
    public String mber_hp;
    @SerializedName("mber_nm")
    public String mber_nm;
    @SerializedName("mber_lifyea")
    public String mber_lifyea;
    @SerializedName("mber_sex")
    public String mber_sex;
    @SerializedName("mber_hp_newyn")
    public String mber_hp_newyn;
    @SerializedName("log_yn")
    public String log_yn;
    @SerializedName("default_basic_goal")
    public String default_basic_goal;
    @SerializedName("day_basic_goal")
    public String day_basic_goal;
    @SerializedName("tot_basic_goal")
    public String tot_basic_goal;
    @SerializedName("mber_sn")
    public String mber_sn;
    @SerializedName("insures_code")
    public String insures_code;
    @SerializedName("api_code")
    public String api_code;
    // 로그인시 아이디 저장하기 위한 변수
    public String mber_id;

    //2019-03-19 park add
    @SerializedName("age")
    public String age;
    @SerializedName("nickname")
    public String nickname;
    @SerializedName("point_total_amt")
    public String point_total_amt;
    @SerializedName("accml_sum_amt")
    public String accml_sum_amt;
    @SerializedName("disease_txt")
    public String disease_txt;
    @SerializedName("gclife_id")
    public String gclife_id;

    //2019-04-10
    @SerializedName("offerinfo_yn") //동의 여부
    public String offerinfo_yn;


}
