package kr.co.hi.medicare.fragment.mypage;

import java.io.Serializable;

public class MyPageData implements Serializable {
    public String mber_sn;
    public String notice_yn;//공지알림
    public String health_yn;//건강알림
    public String heart_yn;//좋아요알림
    public String reply_yn;//답글알림
    public String daily_yn;//일일미션알림
    public String cm_yn;//커뮤니티알림

    public String MBER_NM;
    public String MBER_SEX;
    public String NICKNAME;
    public String MBER_LIFYEA;
    public String age;
    public String MBER_DE;
    public String PROFILE_PIC;
    public String DISEASE_OPEN;
    public String DISEASE_NM;
    public String accml_sum_amt;
    public String POINT_TOTAL_AMT;
    public String mber_height;
    public String mber_bdwgh;
    public String mber_bdwgh_goal;
    public String actqy;
    public String smoking_yn;
    public String job_yn;
    public String mber_grad;
    public String app_ver;
    public String mber_hp;

    public String DISEASE_TXT;
}
