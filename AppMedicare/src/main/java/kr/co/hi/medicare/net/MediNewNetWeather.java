package kr.co.hi.medicare.net;

import android.content.Context;
import android.util.Log;
import android.webkit.WebView;

import com.google.gson.Gson;
import kr.co.hi.medicare.net.hwNet.BaseData;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.lang.reflect.Constructor;

import cz.msebera.android.httpclient.Header;
import kr.co.hi.medicare.Defined;
import kr.co.hi.medicare.activity.BaseActivityMedicare;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_getMsrstnAcctoRltmMesureDnsty;
import kr.co.hi.medicare.net.data.Tr_getNearbyMsrstnList;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.JsonLogPrint;


/**
 * 날씨 API용 통신 모듈
 */
public class MediNewNetWeather {
    private final static String TAG = MediNewNetWeather.class.getSimpleName();

    public static void doApi(Context context, final Class<? extends BaseData> trCls, Object requestData, final MediNewNetworkHandler handler ) {
        doApi(context, trCls, requestData, true, handler);
    }

    public static void doApi(Context context, final Class<? extends BaseData> trCls, Object requestData, boolean isShowProgress, final MediNewNetworkHandler handler ) {
        BaseData tr = createTrClass(trCls, context);
        doApi(context, tr, requestData, isShowProgress, handler);
    }

    public static void doApi(final Context context, final BaseData tr, Object requestData, final MediNewNetworkHandler handler ) {
        doApi(context, tr, requestData, true, handler);
    }

    public static void doApi(final Context context, final BaseData tr, Object requestData, boolean isShowProgress, final MediNewNetworkHandler handler ) {
        if (isShowProgress) {
            if (context instanceof BaseActivityMedicare) {
                Log.i(TAG, "doApi progress showProgress");
                ((BaseActivityMedicare)context).showProgress();
            } else {
                Log.e(TAG, "doApi progress Context="+context);
            }
        }

        try {
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            client.setTimeout(10000);
            client.addHeader("Accept-Charset", "UTF-8");
            client.addHeader("User-Agent", new WebView(context).getSettings().getUserAgentString());

            if (requestData != null) {
                JSONObject reqObj = tr.makeJson(requestData);
                if (reqObj != null) {
                    Log.e(TAG, "request data is null");
                    params.put("strJson", reqObj.toString());
                }
            }

            String url = Defined.BASE_HTTPS_URL;
            if (tr instanceof Tr_getNearbyMsrstnList) {
                // 가까운 측정소 정보 가져오기
                Tr_getNearbyMsrstnList tr2 = (Tr_getNearbyMsrstnList)tr;
                url = tr2.getConnUrl();
            } else if (tr instanceof Tr_getMsrstnAcctoRltmMesureDnsty) {
                // 가까운 측정소 정보 가져오기
                Tr_getMsrstnAcctoRltmMesureDnsty tr2 = (Tr_getMsrstnAcctoRltmMesureDnsty) tr;
                url = tr2.getConnUrl();
            }

            CLog.i("url = " + url);
            final String trName = tr.getClass().getSimpleName();
            client.post(url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    if (context instanceof BaseActivityMedicare) {
                        ((BaseActivityMedicare)context).hideProgress();
                    }

                    String response = null;
                    try {
                        response = new String(responseBody);

                        // json 불필요 내용 제거
                        int idx = response.indexOf("{");
                        if (idx != -1)
                            response = response.substring(idx);
                        response = response.replaceAll("</string>", "");

//                        Log.i(TAG, "response\ng"+response);
                        JsonLogPrint.printJson(trName, response);
                        Gson gson = new Gson();
                        BaseData responseData = gson.fromJson(response, tr.getClass());
                        handler.onSuccess(responseData);
                    } catch (Exception e) {
                        e.printStackTrace();
                        handler.onFailure(statusCode, response, e);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    if (context instanceof BaseActivityMedicare) {
                        ((BaseActivityMedicare)context).hideProgress();
                    }

                    Log.e(TAG, error.getMessage() +"::"+  tr.getClass().getName());
                    error.printStackTrace();

                    String response = "";
                    if (responseBody != null) {
                        response = new String(responseBody);
                    }

                    handler.onFailure(statusCode, response, error);
                }
            });

        }catch(Exception e){
            CLog.e(e.toString());
        }
    }

    private static BaseData createTrClass(Class<? extends BaseData> cls, Context context) {
        BaseData trClass = null;
        try {
            Constructor<? extends BaseData> co = cls.getConstructor();
            trClass = co.newInstance();
        } catch (Exception e) {
            try {
                Constructor<? extends BaseData> co = cls.getConstructor(Context.class);
                trClass = co.newInstance(context);
            } catch (Exception e2) {
                Log.e(TAG, "createTrClass", e2);
            }
        }

        return trClass;
    }
}
