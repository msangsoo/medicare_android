package kr.co.hi.medicare.fragment.health;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.home.HomeFragmentMedicare;
import kr.co.hi.medicare.googleFitness.HomeTodayGoogleFitData;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_mvm_misson_goal_alert;
import kr.co.hi.medicare.net.hwNet.ApiData;
import kr.co.hi.medicare.net.hwNet.BaseData;
import kr.co.hi.medicare.sample.SampleFragmentMedi;
import kr.co.hi.medicare.fragment.IBaseFragment;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.health.food.FoodManageFragment;
import kr.co.hi.medicare.fragment.health.message.DlgHealthMessaeView;
import kr.co.hi.medicare.fragment.health.message.HealthMessageFragment;
import kr.co.hi.medicare.fragment.health.pressure.PressureManageFragment;
import kr.co.hi.medicare.fragment.health.question.QuestionnaireFragment;
import kr.co.hi.medicare.fragment.health.step.StepManageFragment;
import kr.co.hi.medicare.fragment.health.sugar.SugarManageFragment;
import kr.co.hi.medicare.fragment.health.weight.WeightManageFragment;

import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;


/**
 * Created by MrsWin on 2017-02-16.
 */

public class HealthFragment extends BaseFragmentMedi implements IBaseFragment {
    private final String TAG = getClass().getSimpleName();

//    private FragmentPagerAdapter mPagerAdapter;
//    private ViewPager mViewPager;
    private RadioButton[] mFragmentRadio;
    private LinearLayout[] mChildLayout;
    private RadioGroup mFragmentGroup;
    private View tabview;
    private DlgHealthMessaeView mHealthMsgDlgView;

    private Toolbar mToolBar;

    public static int pos = 0;


    private final Fragment[] mFragments = new Fragment[] {
            FoodManageFragment.newInstance(),
            StepManageFragment.newInstance(),
            WeightManageFragment.newInstance(),
            PressureManageFragment.newInstance(),
            SugarManageFragment.newInstance(),
    };

    public static Fragment newInstance() {
        HealthFragment fragment = new HealthFragment();
        return fragment;
    }

    public HealthFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab5_fullcolor, container, false);
        return view;
    }

    private TextView  new_message;
    private RelativeLayout message_lv;
    private View close_btn;
    private ImageView mHealthMessage;


    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // XXX 백으로 데이터 보내기
        Bundle bundle = new Bundle();
        bundle.putString(SampleFragmentMedi.SAMPLE_BACK_DATA, TAG+" BackData!!!");
        setBackData(bundle);

        tabview = view;
        mToolBar = view.findViewById(R.id.medi_main_health_toolbar);

        new_message = view.findViewById(R.id.new_message);
        message_lv = view.findViewById(R.id.message_lv);
        close_btn = view.findViewById(R.id.close_btn);
        new_message.setOnClickListener(mOnClickListener);
        close_btn.setOnClickListener(mOnClickListener);
        mHealthMessage = view.findViewById(R.id.common_toolbar_back_btn);
        mHealthMessage.setOnClickListener(mOnClickListener);
        mFragmentGroup = view.findViewById(R.id.tab_radiogroup);

        final String[] mFragmentNames = new String[] {
                getString(R.string.health_menu_2),
                getString(R.string.health_menu_1),
                getString(R.string.health_menu_3),
                getString(R.string.health_menu_4),
                getString(R.string.main_menu_2),
        };

        mFragmentRadio = new RadioButton[]{
                view.findViewById(R.id.tab1),
                view.findViewById(R.id.tab2),
                view.findViewById(R.id.tab3),
                view.findViewById(R.id.tab4),
                view.findViewById(R.id.tab5),
        };

        mChildLayout = new LinearLayout[] {
                view.findViewById(R.id.child_fragment_layout)
                , view.findViewById(R.id.child_fragment_layout2)
                , view.findViewById(R.id.child_fragment_layout3)
                , view.findViewById(R.id.child_fragment_layout4)
                , view.findViewById(R.id.child_fragment_layout5)
        };

        int i = 0;
        for (String name : mFragmentNames) {
            mFragmentRadio[i].setText(name);
            i++;
        }

        mFragmentGroup.setOnCheckedChangeListener(mCheckedChangeListener);
        view.findViewById(R.id.common_toolbar_right_btn).setOnClickListener(mOnClickListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mOnClickListener, view, getContext());


        //건강관리
        mHealthMessage.setOnTouchListener(ClickListener);

        //코드부여
        mHealthMessage.setContentDescription(getString(R.string.mHealthMessage));

        setChildFragment(pos);
        mFragmentRadio[pos].setChecked(true);

    }

    @Override
    public void onResume() {
        super.onResume();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setChildFragment(pos);
                    mFragmentRadio[pos].setChecked(true);
                }
            }, 100);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getGoogleFitDayStep();
            }
        }, 100);



    }

    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            pos = group.indexOfChild(tabview.findViewById(checkedId));
            setChildFragment(pos);
        }
    };

    private Fragment mCurrentFrament;
    private void setChildFragment(int pos) {
        Log.i(TAG,"HEALTH_POS : " + pos);

        mCurrentFrament = mFragments[pos];
        if (!mCurrentFrament.isAdded()) {
            FragmentTransaction childFt = getChildFragmentManager().beginTransaction();
            childFt.replace(mChildLayout[pos].getId(), mCurrentFrament);
            childFt.addToBackStack(null);
            childFt.commit();
        } else {
            mCurrentFrament.onResume();

        }

        for (int i = 0; i < mChildLayout.length; i++) {
            mChildLayout[i].setVisibility(i == pos ? View.VISIBLE : View.GONE);
        }
    }

    public int getCurrentFragment() {
        return pos;
    }

    //클릭 이벤트 정의
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int viewId = v.getId();
            Bundle bundle = new Bundle();
            switch (viewId){
                //탭버튼
                case R.id.common_toolbar_back_btn:
                    NewActivity.startActivity(getActivity(), HealthMessageFragment.class,null);
                    break;
                case R.id.common_toolbar_right_btn:
                    NewActivity.startActivity(getActivity(), QuestionnaireFragment.class,null);
                    break;
                case R.id.close_btn:
                    SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MESSAGE, false);
//                    mHealthMessage.setImageResource(R.drawable.health_m_btn_01);
                    message_lv.setVisibility(View.GONE);
                    break;
                case R.id.new_message:
                    NewActivity.startActivity(getActivity(), HealthMessageFragment.class,null);
                    break;
            }
        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.i(TAG, "onConfigurationChanged="+newConfig.orientation);
        int visible = Configuration.ORIENTATION_LANDSCAPE == newConfig.orientation ? View.GONE : View.VISIBLE;
        mToolBar.setVisibility(visible);
        mFragmentGroup.setVisibility(visible);
        if (SharedPref.getInstance().getPreferences(SharedPref.HEALTH_MESSAGE, false)) {
            message_lv.setVisibility(visible);
        } else {
            message_lv.setVisibility(View.GONE);
        }

    }

    /**
     * 오늘 걸음 미션 확인
     */
    private int mTodayStepTotal = 0;
    private void getGoogleFitDayStep() {
        new HomeTodayGoogleFitData().getDayStepCount(getActivity(), 0,new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof String) {
                    int todayTotalStep = StringUtil.getIntVal(obj.toString());
                    Log.i(TAG, "오늘걸음수="+todayTotalStep);
                    mTodayStepTotal = todayTotalStep;

//
//               // 구글 피트니스 걸음 센서 등록
                    todayStepMissionAlert();
                }
            }
        });
    }

    boolean isShowAlert = false;
    private void todayStepMissionAlert() {
        if (mTodayStepTotal >= 10000) {

            if (isShowAlert)
                return;

            final String today = CDateUtil.getToday_yyyy_MM_dd();
            String prefToday = SharedPref.getInstance().getPreferences(HomeFragmentMedicare.PREF_KEY_STEP_MISSION_COMPLETE);

            if (today.equals(prefToday) == false) {
                isShowAlert = true;
                Tr_mvm_misson_goal_alert.RequestData requestData = new Tr_mvm_misson_goal_alert.RequestData();
                requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;
                requestData.misson_work_typ = "1";

                MediNewNetworkModule.doApi(getContext(), Tr_mvm_misson_goal_alert.class, requestData, new MediNewNetworkHandler() {
                    @Override
                    public void onSuccess(BaseData responseData) {
                        if (responseData instanceof Tr_mvm_misson_goal_alert) {
                            Tr_mvm_misson_goal_alert data = (Tr_mvm_misson_goal_alert)responseData;
                            try {

                                if(data.data_yn.equals("Y")) {
                                    SharedPref.getInstance().savePreferences(HomeFragmentMedicare.PREF_KEY_STEP_MISSION_COMPLETE, today);
                                    CDialog.showDlg(getContext(),data.misson_goal_txt)
                                            .setPointTvView(data.misson_goal_point+"P")
                                            .setOkButton(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {

                                                }
                                            });

                                }


                            } catch (Exception e) {
                                CLog.e(e.toString());
                            }
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, String response, Throwable error) {

                    }
                });
            }
        }
    }

}