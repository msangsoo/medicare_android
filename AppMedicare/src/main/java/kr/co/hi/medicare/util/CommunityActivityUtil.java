package kr.co.hi.medicare.util;

public enum CommunityActivityUtil {
    SEARCH_TYPE_DEFAULT("말머리 선택", 1),

    SEARCH_TYPE_AFTER("암 진단 후", 2),

    SEARCH_TYPE_METHOD("암 치료방법", 3),

    SEARCH_TYPE_REVIEW("암 극복후기", 4),

    SEARCH_TYPE_ETC("기타", 5);

    private String name = null;
    private int idx = 0;

    CommunityActivityUtil(String name, int index) {
        this.name = name;
        this.idx = index;
    }

    public String getName() {
        return name;
    }

    public int getValue()
    {
        return this.ordinal() + 1;
    }

    public String getName(int code)
    {
        for(CommunityActivityUtil communityActivityUtil : CommunityActivityUtil.values())
        {
            if(code == communityActivityUtil.getValue())
            {
//                CLog.i("Spinner --> " + communityActivityUtil.getValue());
                return communityActivityUtil.getName();
            }
        }
        return "";
    }

}
