package kr.co.hi.medicare.fragment.health.food;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import kr.co.hi.medicare.value.Define;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperFoodCalorie;
import kr.co.hi.medicare.database.DBHelperFoodDetail;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.net.hwdata.Tr_get_meal_input_data;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 * Created by MrsWin on 2017-04-23.
 */

public class FoodShareDialog {
    private final String TAG = FoodShareDialog.class.getSimpleName();

    private TextView foodShare_btn1,foodShare_btn2,foodShare_btn3,foodShare_btn4,foodShare_btn5, foodShare_btn6;
    private Button dlgCancelBtn;
    private Button dlgOkBtn;
    private Context mContext;
    private Tr_get_meal_input_data.ReceiveDatas A;
    private Tr_get_meal_input_data.ReceiveDatas B;
    private Tr_get_meal_input_data.ReceiveDatas C;
    private Tr_get_meal_input_data.ReceiveDatas D;
    private Tr_get_meal_input_data.ReceiveDatas E;
    private Tr_get_meal_input_data.ReceiveDatas F;
    private List<DBHelperFoodCalorie.Data> foodList = new ArrayList<>();
    private CommunityListViewData sharelist;

    private String Path = "";
    private Bitmap Bit = null;

    private BaseFragmentMedi mBaseFragment;


    /**
     * 음식 공유 Dialog
     *
     *
     */
    public FoodShareDialog(BaseFragmentMedi BaseFragment, final Tr_get_meal_input_data.ReceiveDatas mMealDataA, final Tr_get_meal_input_data.ReceiveDatas mMealDataB,
                           final Tr_get_meal_input_data.ReceiveDatas mMealDataC, final Tr_get_meal_input_data.ReceiveDatas mMealDataD, final Tr_get_meal_input_data.ReceiveDatas mMealDataE,
                           final Tr_get_meal_input_data.ReceiveDatas mMealDataF, final View.OnClickListener onClickListener) {

        mBaseFragment = BaseFragment;
        View view = LayoutInflater.from(mBaseFragment.getContext()).inflate(R.layout.dialog_foodshare, null);
        mContext = mBaseFragment.getContext();

        /**
         * A:아침,B:점심,C:저녁,D:아침간식,E:점심간식,F:저녁간식
         */
        A  = mMealDataA;
        B  = mMealDataB;
        C  = mMealDataC;
        D  = mMealDataD;
        E  = mMealDataE;
        F  = mMealDataF;

        dlgCancelBtn = (Button) view.findViewById(R.id.food_dlg_cancle);
        dlgOkBtn = (Button) view.findViewById(R.id.food_dlg_confirm);

        foodShare_btn1 = view.findViewById(R.id.foodShare_btn1);
        foodShare_btn2 = view.findViewById(R.id.foodShare_btn2);
        foodShare_btn3 = view.findViewById(R.id.foodShare_btn3);
        foodShare_btn4 = view.findViewById(R.id.foodShare_btn4);
        foodShare_btn5 = view.findViewById(R.id.foodShare_btn5);
        foodShare_btn6 = view.findViewById(R.id.foodShare_btn6);

        foodShare_btn1.setOnClickListener(mClickListener);
        foodShare_btn2.setOnClickListener(mClickListener);
        foodShare_btn3.setOnClickListener(mClickListener);
        foodShare_btn4.setOnClickListener(mClickListener);
        foodShare_btn5.setOnClickListener(mClickListener);
        foodShare_btn6.setOnClickListener(mClickListener);

        final CDialog dlg = CDialog.showDlg(mBaseFragment.getContext(), view,true);
        dlg.setTitle("커뮤니티에 공유하실\n식사를 선택하세요.",true);
        dlgOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dlg.dismiss();
                NewActivity.moveToWritePage(mBaseFragment, sharelist, "");

                if (onClickListener != null)
                    onClickListener.onClick(v);
            }
        });
        dlgCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });
    }

    /**
     * 데이터 가져오기 (음식)
     */
    private void getFoodListData(String idx,String name) {
        DBHelper helper = new DBHelper(mContext);
        DBHelperFoodDetail db = helper.getFoodDetailDb();
        foodList = db.getFoodList(idx);

        sharelist = new CommunityListViewData();
        sharelist.ISSHARE=true;
        sharelist.CM_CONTENT = "";
        sharelist.CM_MEAL_LIST = new ArrayList<>();
        String temp_foodname ="";
        Float temp_calorie = 0.f;

        for(int i=0; i<foodList.size(); i++) {
            if(foodList.size()-1 == i){
                temp_foodname += foodList.get(i).food_name + " / ";
            }else {
                temp_foodname += foodList.get(i).food_name + ", ";
            }

            temp_calorie = temp_calorie + StringUtil.getFloatVal(foodList.get(i).food_calorie) * StringUtil.getFloatVal(foodList.get(i).forpeople);
        }
        sharelist.CM_MEAL_LIST.add(name + " / " + temp_foodname + StringUtil.getNoneZeroString(temp_calorie)+" kcal");

    }

    public Bitmap getIndexToImageData(final String idx) {

        try {
            String path = Define.getFoodPhotoPath(idx);
//            Uri uri = ProviderUtil.getOutputMediaFileUri(getContext(), new File(path));
//            Bitmap bitmapImage =
//                    MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);

            if (TextUtils.isEmpty(idx)) {
                Logger.e(TAG, "getIndexToImageData idx is null");
                return null;
            }

            // 로컬에서 받아오기
            Bitmap bitmap;
            Logger.i(TAG, "getIndexToImageData.path="+path);
            File imgFile = new File(path);
            if (imgFile.exists()) {
                bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                return bitmap;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int vId = v.getId();
            if (vId == R.id.foodShare_btn1) {
                foodShare_btn1.setSelected(true);
                foodShare_btn2.setSelected(false);
                foodShare_btn3.setSelected(false);
                foodShare_btn4.setSelected(false);
                foodShare_btn5.setSelected(false);
                foodShare_btn6.setSelected(false);

                foodShare_btn1.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_bold));
                foodShare_btn2.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn3.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn4.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn5.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn6.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));

                if(A != null) {

//                    if (TextUtils.isEmpty(A.picture) == false) {
//                        Path = A.picture;
//                    } else {
//                        Bit = getIndexToImageData(A.idx);
//                    }
                    getFoodListData(A.idx,"아침식사");

                }


            } else if (vId == R.id.foodShare_btn2) {
                foodShare_btn1.setSelected(false);
                foodShare_btn2.setSelected(true);
                foodShare_btn3.setSelected(false);
                foodShare_btn4.setSelected(false);
                foodShare_btn5.setSelected(false);
                foodShare_btn6.setSelected(false);

                foodShare_btn1.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn2.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_bold));
                foodShare_btn3.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn4.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn5.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn6.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));

                if(B != null) {
//                    if (TextUtils.isEmpty(B.picture) == false) {
//                        Path = B.picture;
//                    } else {
//                        Bit = getIndexToImageData(B.idx);
//                    }
                    getFoodListData(B.idx,"점심식사");
                }




            } else if (vId == R.id.foodShare_btn3) {
                foodShare_btn1.setSelected(false);
                foodShare_btn2.setSelected(false);
                foodShare_btn3.setSelected(true);
                foodShare_btn4.setSelected(false);
                foodShare_btn5.setSelected(false);
                foodShare_btn6.setSelected(false);

                foodShare_btn1.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn2.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn3.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_bold));
                foodShare_btn4.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn5.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn6.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));

                if(C != null) {
//                    if (TextUtils.isEmpty(C.picture) == false) {
//                        Path = C.picture;
//                    } else {
//                        Bit = getIndexToImageData(C.idx);
//                    }
                    getFoodListData(C.idx,"저녁식사");
                }



            } else if (vId == R.id.foodShare_btn4) {

                foodShare_btn1.setSelected(false);
                foodShare_btn2.setSelected(false);
                foodShare_btn3.setSelected(false);
                foodShare_btn4.setSelected(true);
                foodShare_btn5.setSelected(false);
                foodShare_btn6.setSelected(false);

                foodShare_btn1.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn2.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn3.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn4.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_bold));
                foodShare_btn5.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn6.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));

                if(D != null) {
//                    if (TextUtils.isEmpty(D.picture) == false) {
//                        Path = D.picture;
//                    } else {
//                        Bit = getIndexToImageData(D.idx);
//                    }

                    getFoodListData(D.idx,"아침간식");
                }



            } else if (vId == R.id.foodShare_btn5) {

                foodShare_btn1.setSelected(false);
                foodShare_btn2.setSelected(false);
                foodShare_btn3.setSelected(false);
                foodShare_btn4.setSelected(false);
                foodShare_btn5.setSelected(true);
                foodShare_btn6.setSelected(false);

                foodShare_btn1.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn2.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn3.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn4.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn5.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_bold));
                foodShare_btn6.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));

                if(E != null) {
//                    if (TextUtils.isEmpty(E.picture) == false) {
//                        Path = E.picture;
//                    } else {
//                        Bit = getIndexToImageData(E.idx);
//                    }
                    getFoodListData(E.idx, "점심간식");
                }

            } else if (vId == R.id.foodShare_btn6) {

                foodShare_btn1.setSelected(false);
                foodShare_btn2.setSelected(false);
                foodShare_btn3.setSelected(false);
                foodShare_btn4.setSelected(false);
                foodShare_btn5.setSelected(false);
                foodShare_btn6.setSelected(true);

                foodShare_btn1.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn2.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn3.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn4.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn5.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_light));
                foodShare_btn6.setTypeface(ResourcesCompat.getFont(mContext, R.font.notosanskr_bold));

                if(F != null) {
//                    if (TextUtils.isEmpty(F.picture) == false) {
//                        Path = F.picture;
//                    } else {
//                        Bit = getIndexToImageData(F.idx);
//                    }

                    getFoodListData(F.idx, "저녁간식");
                }

            }
        }
    };


}
