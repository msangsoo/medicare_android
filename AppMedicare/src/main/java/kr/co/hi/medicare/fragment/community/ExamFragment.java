package kr.co.hi.medicare.fragment.community;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.R;

import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;

/**
 * Created by mrsohn on 2017. 3. 14..
 * 혈당관리
 */

public class ExamFragment extends BaseFragmentMedi {
    private final String TAG = ExamFragment.class.getSimpleName();

    public static String EXAM_DATA = "EXAM_DATA";

    public static Fragment newInstance() {
        ExamFragment fragment = new ExamFragment();
        return fragment;
    }

//    /**
//     * 액션바 세팅
//     */
//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        super.loadActionbar(actionBar);
//        actionBar.setWhiteTheme();
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exam, container, false);
        EditText editText = view.findViewById(R.id.exam_edittext);

        Bundle bundle = getArguments();
        if (bundle != null)
            editText.setText(bundle.getString(EXAM_DATA));

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        view.findViewById(R.id.exam_move_fragment_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString(EXAM_DATA, "XXXM 플래그먼트 이동");
                replaceFragment(ExamFragment.newInstance(), bundle);   // 플레먼트 화면 이동 처리

//                Bundle bundle = new Bundle();   // 전달할 데이터가 있을 경우 Bundle로 데이터 전달
//                bundle.putString(ExamFragment.EXAM_DATA, "전달 데이터");
                NewActivity.startActivity(getActivity(), ExamFragment.class, bundle);
            }
        });
    }

}
