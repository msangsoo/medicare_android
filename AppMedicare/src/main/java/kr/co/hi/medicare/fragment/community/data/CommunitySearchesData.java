package kr.co.hi.medicare.fragment.community.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommunitySearchesData {
    public int idx;
    public String regdate;
    public String word;

}
