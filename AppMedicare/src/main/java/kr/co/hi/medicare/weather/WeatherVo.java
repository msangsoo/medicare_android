package kr.co.hi.medicare.weather;

public class WeatherVo {
    public String observationpoint = ""; // 지역
    public String date = "";
    public String temp = "";
    public String weather = "";
    public String dust = "";
    public String cityName = "";
}
