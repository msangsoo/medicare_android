package kr.co.hi.medicare.net.hwdata;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.value.model.StepModel;

/**
 FUNCTION NAME	미션 걸음수 전송
 API: "login":
 mission_walk_start_de  : 걸음수 시작일

 mission_walk_end_de    :걸음수 끝나는 일






 ​2. 구글피트니스 또는 애플건강에서 해당하는 날짜로 조회해서

 아래와 같이 호출합니다.​

 API : mvm_info_input_data_mission





 {   ""api_code"": ""mvm_info_input_data_mission"",   ""insures_code"": ""303"",  ""mber_sn"": ""1344""
 ,""ast_mass"":[{""step"":""580"",""regdate"":""20180427""   }  ] }





 */

public class Tr_mvm_info_input_data_mission extends BaseData {
    private final String TAG = Tr_mvm_info_input_data_mission.class.getSimpleName();

    public static class RequestData {
        public String mber_sn; // 1000
        public JSONArray ast_mass; //
    }


    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof Tr_mvm_info_input_data_mission.RequestData) {
            JSONObject body = new JSONObject();

            Tr_mvm_info_input_data_mission.RequestData data = (Tr_mvm_info_input_data_mission.RequestData) obj;
            body.put("api_code", getApiCode(TAG)); //
            body.put("insures_code", INSURES_CODE); // 300
            body.put("mber_sn", data.mber_sn); //  1000
            body.put("ast_mass", data.ast_mass); //
            return body;
        }

        return super.makeJson(obj);
    }

    public static class RequestArrData {
        public String idx; // 1
        public String amount; // 100
        public String regtype; // D
        public String regdate; // 201703301420
    }

    public JSONArray getArray(List<StepModel> dataModel) {
        JSONArray array = new JSONArray();
        if (dataModel.size() > 0) {
            for (StepModel model : dataModel) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put("step" , model.step );
                    obj.put("regdate" , model.regDate);
                    array.put(obj);
                } catch (JSONException e) {
                    Logger.e(e);
                }
            }
        }

        return array;
    }


    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("api_code")
    public String api_code; //
    @SerializedName("insures_code")
    public String insures_code; //
    @SerializedName("reg_yn")
    public String reg_yn; //

}
