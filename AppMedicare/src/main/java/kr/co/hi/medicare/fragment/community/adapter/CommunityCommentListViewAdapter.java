package kr.co.hi.medicare.fragment.community.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.fragment.community.data.CommunityCommentData;
import kr.co.hi.medicare.fragment.community.data.CommunityCommentInfo;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.community.data.UpdateDataFunction;
import kr.co.hi.medicare.fragment.community.holder.CommunityCommentListViewHolder;
import kr.co.hi.medicare.fragment.community.holder.CommunityListViewHolder;
import kr.co.hi.medicare.fragment.community.holder.ProgressViewHolder;

import static android.app.Activity.RESULT_OK;
import static kr.co.hi.medicare.fragment.community.CommunityCommentFragment.REQUEST_CODE_COMMDATA;
import static kr.co.hi.medicare.fragment.community.CommunityCommentFragment.REQUEST_CODE_ISDEL;
import static kr.co.hi.medicare.fragment.community.adapter.LoadMoreListener.VIEW_HEADER;
import static kr.co.hi.medicare.fragment.community.adapter.LoadMoreListener.VIEW_ITEM;
import static kr.co.hi.medicare.fragment.community.adapter.LoadMoreListener.VIEW_PROG;


public class CommunityCommentListViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements UpdateDataFunction {
    private View.OnClickListener onClickListener;
    private CommunityCommentInfo data;
    private Context context;
    private String SEQ;
    private boolean isMoreLoading = false;
    private CommunityListViewData communityListViewData;
    private Fragment fragment;
    private int VIEW_TYPE=CommunityListViewHolder.TYPE_VIEW;

    public CommunityCommentListViewAdapter(Context context, View.OnClickListener onClickListener, String SEQ, CommunityListViewData communityListViewData,Fragment fragment) {
        this.context = context;
        this.onClickListener = onClickListener;
        data = new CommunityCommentInfo();
        this.SEQ = SEQ;
        this.communityListViewData = communityListViewData;
        this.fragment = fragment;
    }

    public void setViewType(int VIEW_TYPE){
        this.VIEW_TYPE = VIEW_TYPE;
    }

    @Override
    public int getItemViewType(int position) {
        if(position==0){
            return VIEW_HEADER;
        }else{
            return data.ADDR_MASS.get(position-1) != null ? VIEW_ITEM : VIEW_PROG;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case VIEW_ITEM:
                return new CommunityCommentListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_community_comment, parent, false),SEQ, context);
            case VIEW_HEADER:
                return new CommunityListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_community, parent, false), this, SEQ, context, communityListViewData.CM_GUBUN, fragment, new CommunityListViewHolder.setAction() {
                    @Override
                    public void setActionAfterDelete(String CM_SEQ) {
                        Intent comm_info = new Intent();
                        comm_info.putExtra(REQUEST_CODE_COMMDATA, getCommunityListViewData());
                        comm_info.putExtra(REQUEST_CODE_ISDEL,true);
                        fragment.getActivity().setResult(RESULT_OK, comm_info);
                        fragment.getActivity().finish();
                    }
                });
            default:
                return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_progress, parent, false));
        }
    }

    public void setMore(boolean isMore) {
        this.isMoreLoading = isMore;
    }

    public boolean getMore() {
        return isMoreLoading;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof CommunityCommentListViewHolder){
            if (data.ADDR_MASS.size() > 0) {
                    final CommunityCommentListViewHolder commViewHolder = (CommunityCommentListViewHolder) holder;
                    commViewHolder.SetView(data.ADDR_MASS.get(position-1),context);
                    commViewHolder.btn_delete.setTag(R.id.comm_comment_delete, position-1);
                    commViewHolder.comment_nick.setTag(R.id.comm_comment_nick,position-1);
                    commViewHolder.btn_delete.setOnClickListener(onClickListener);
                    commViewHolder.comment_nick.setOnClickListener(onClickListener);

                    commViewHolder.comment_profile.setTag(R.id.comm_comment_profile,position-1);
                    commViewHolder.comment_profile.setOnClickListener(onClickListener);
            }
        }else if(holder instanceof CommunityListViewHolder){
            final CommunityListViewHolder commViewHolder = (CommunityListViewHolder) holder;
            commViewHolder.SetView(communityListViewData,VIEW_TYPE,0);
            commViewHolder.profile.setTag(R.id.comm_main_profile,position);
            commViewHolder.profile.setOnClickListener(onClickListener);
        }
    }

    @Override
    public int getItemCount() {
        if (data == null||data.ADDR_MASS==null) {
            return 1; //헤더가 무조건있기때문에 데이터가 없어도 1
        }
        //현재 리스트에 등록된 개수만큼 반환//
        return data.ADDR_MASS.size()+1;
    }


    public void addAllData(List<CommunityCommentData> data) {
        if(this.data.ADDR_MASS==null)
            this.data.ADDR_MASS = new ArrayList<>();

        if(data!=null){
            this.data.ADDR_MASS.clear();
            this.data.ADDR_MASS.addAll(data);
            notifyDataSetChanged();
//            setDataComment(0,Integer.toString(getItemCount()-1) );
        }
    }

    public void addItem(CommunityCommentData data) {
        if(this.data.ADDR_MASS==null)
            this.data.ADDR_MASS = new ArrayList<>();

        if(data!=null){
            ArrayList<CommunityCommentData> datas = new ArrayList<>();
            datas.add(data);
            datas.addAll(this.data.ADDR_MASS);
            this.data.ADDR_MASS.clear();
            this.data.ADDR_MASS = datas;
            notifyDataSetChanged();
//            setDataComment(0,Integer.toString(getItemCount()-1));
        }
    }


    public void addItemMore(List<CommunityCommentData> data){
        int sizeInit = getItemCount();
        this.data.ADDR_MASS.addAll(data);
        notifyItemRangeChanged(sizeInit, getItemCount());
//        setDataComment(0,Integer.toString(getItemCount()-1));
    }

    public void setProgressMore(final boolean isProgress) {
        if (isProgress) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    data.ADDR_MASS.add(null);
//                    notifyItemInserted(data.ADDR_MASS.size() - 1);
                    notifyItemInserted(data.ADDR_MASS.size());
                }
            });
        } else {

            if(data.ADDR_MASS.size()>0) {
//                data.ADDR_MASS.remove(data.ADDR_MASS.size() - 1);
                data.ADDR_MASS.remove(data.ADDR_MASS.size()-1);
//                notifyItemRemoved(data.ADDR_MASS.size());
                notifyItemRemoved(getItemCount());
            }
        }
    }

    public CommunityCommentData getItem(int position){
        return data.ADDR_MASS.get(position);
    }


    public void deleteItem(String cc_seq) {
        for(int i=0; i<data.ADDR_MASS.size();i++){
            if(data.ADDR_MASS.get(i).CC_SEQ.equals(cc_seq)){
                data.ADDR_MASS.remove(i);
                notifyItemRemoved(i+1);
                notifyItemRangeChanged(i+1,getItemCount());
                setDataComment(0,"-1");
//                setDataComment(0,Integer.toString(getItemCount()-1));
                break;
            }
        }
    }


    @Override
    public void setDataHeart(int position,String HCNT, String MYHEART) {
        communityListViewData.MYHEART=MYHEART;
        communityListViewData.HCNT = HCNT;
    }

    @Override
    public void setDataComment(int position, String RCNT) {
        int RCNT_I = Integer.parseInt(communityListViewData.RCNT);
        RCNT_I += Integer.parseInt(RCNT);
        communityListViewData.RCNT = Integer.toString(RCNT_I);
        notifyItemChanged(position);
    }

    public CommunityListViewData getCommunityListViewData(){

        return communityListViewData;
    }

    public void setCommunityListViewData(CommunityListViewData data){
        communityListViewData = data;
        notifyItemChanged(0);
    }

}
