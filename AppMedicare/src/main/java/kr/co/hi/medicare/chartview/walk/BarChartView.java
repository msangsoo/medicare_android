
package kr.co.hi.medicare.chartview.walk;

import android.content.Context;
import android.graphics.Color;
import android.graphics.RectF;
import android.util.Log;
import android.view.View;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.charting.charts.BarChart;
import kr.co.hi.medicare.charting.components.Legend;
import kr.co.hi.medicare.charting.components.XAxis;
import kr.co.hi.medicare.charting.components.YAxis;
import kr.co.hi.medicare.charting.data.BarData;
import kr.co.hi.medicare.charting.data.BarDataSet;
import kr.co.hi.medicare.charting.data.BarEntry;
import kr.co.hi.medicare.charting.data.CEntry;
import kr.co.hi.medicare.charting.formatter.IAxisValueFormatter;
import kr.co.hi.medicare.charting.highlight.Highlight;
import kr.co.hi.medicare.charting.interfaces.datasets.IBarDataSet;
import kr.co.hi.medicare.charting.listener.OnChartValueSelectedListener;
import kr.co.hi.medicare.charting.utils.MPPointF;
import kr.co.hi.medicare.chartview.valueFormat.AxisValueFormatter;
import kr.co.hi.medicare.chartview.valueFormat.BarDataFormatter;
import kr.co.hi.medicare.chartview.valueFormat.MyAxisValueFormatter;
import kr.co.hi.medicare.chartview.valueFormat.XYMarkerView;
import kr.co.hi.medicare.utilhw.ChartTimeUtil;
import kr.co.hi.medicare.value.TypeDataSet;


public class BarChartView implements OnChartValueSelectedListener {
    private final String TAG = getClass().getSimpleName();

    protected BarChart mChart;
    protected Context mContext;

//    protected Typeface mTfRegular;
//    protected Typeface mTfLight;

    public BarChartView(Context context, View v) {
        mContext = context;

//        mTfRegular = ResourcesCompat.getFont(context, R.font.nanum_barun_gothic);
//        mTfLight = ResourcesCompat.getFont(context, R.font.nanum_barun_gothic_light);

        mChart = (BarChart) v.findViewById(R.id.chart1);
        mChart.setOnChartValueSelectedListener(this);

        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(false); // 상단 값 표시
        mChart.setTouchEnabled(true);       // 마커 표시 하려면 true

        mChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(false);
        mChart.setDoubleTapToZoomEnabled(false);
        mChart.setScaleEnabled(false);

        mChart.setDrawGridBackground(false);

        AxisValueFormatter xAxisFormatter = new AxisValueFormatter(TypeDataSet.Period.PERIOD_DAY);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(15);
        xAxis.setValueFormatter(xAxisFormatter);

        IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = mChart.getAxisLeft();
//        leftAxis.setTypeface(mTfLight);
        leftAxis.setLabelCount(8, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);

        // 하단 설명 문구 (The Year 2017)
        Legend l = mChart.getLegend();
        l.setEnabled(false);

        mChart.setExtraTopOffset(30);   // 차트 상단 여백
        mChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChart.setDrawMarkers(true);    // 마커 표시 활성화 시키기
            }
        });

        initMarkerView(xAxisFormatter);
    }

    public void initMarkerView(IAxisValueFormatter xAxisFormatter) {
        // 차트 클릭시 나오는 마커
        XYMarkerView mv = new XYMarkerView(mContext, xAxisFormatter);
        mv.setChartView(mChart);
        mChart.setMarker(mv);
        mv.setEnabled(true);
    }

    public BarChart getBarChart() {
        return mChart;
    }

    public void setXValueFormat(IAxisValueFormatter f) {
        XAxis xAxis = mChart.getXAxis();
        xAxis.setValueFormatter(f);
    }

    public IAxisValueFormatter getXValueFormat() {
        XAxis xAxis = mChart.getXAxis();
        return xAxis.getValueFormatter();
    }

    public void setUnitStr(String unit) {
        XAxis xAxis = mChart.getXAxis();
        AxisValueFormatter formatter = ((AxisValueFormatter)xAxis.getValueFormatter());
        formatter.setUnitStr(unit);
        initMarkerView(formatter);
    }

    public void invalidate() {
        if (mChart != null)
            mChart.invalidate();
    }

    public void animateXY() {
        mChart.animateXY(500, 500);
    }

    public void animateY() {
        mChart.animateY(5);
    }

    public void setDefaultDummyData(ChartTimeUtil timeClass) {
        List<BarEntry> yVals1 = new ArrayList<>();
        for (int i = 0; i <= 23; i++) {
            yVals1.add(new BarEntry(i, 0));
        }
        setData(yVals1, timeClass);
    }

    public void setData(List<BarEntry> yVals1, ChartTimeUtil timeClass) {
        BarDataSet set1;


//        Log.i(TAG, "setData.getTimeClass="+mChart.getTimeClass()+", timeClass.getTimeUnit()="+timeClass.getTimeUnit()+", "+(mChart.getTimeClass().getTimeUnit() != timeClass.getTimeUnit()));
//        if (mChart.getTimeClass() != null && mChart.getTimeClass().getTimeUnit() != timeClass.getTimeUnit()) {
//            mChart.clear();
//        }

        mChart.setTimeClass(timeClass);


//        if (mChart.getData() != null && mChart.getData().getDataSetCount() > 0) {
//            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
//            set1.setValues(yVals1);
//            set1.setHighLightColor(Color.TRANSPARENT);  // 차트 클릭시 하이라이트 색상
//            set1.setHighLightAlpha(Color.TRANSPARENT);
//
//            data = mChart.getData();
//            data.setDrawValues(false);  // 바차트 상단에 값을 표시할지 여부
//
//            mChart.setData(data, timeClass);
//            mChart.getData().notifyDataChanged();
//            mChart.notifyDataSetChanged();
//        } else {

            // 2칸 나오는 차트로 세팅
            set1 = new BarDataSet(yVals1, "The year 2017");

            set1.setDrawIcons(false);
            set1.setValues(yVals1);

            // 그래프 색상 설정
//            set1.setColors(ColorTemplate.MATERIAL_COLORS);
//            set1.setColor(ContextCompat.getColor(mContext, R.color.x_91_132_230));
            set1.setColors(getColors());
            set1.setHighLightColor(Color.TRANSPARENT);
            set1.setHighLightAlpha(Color.TRANSPARENT);

            List<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            // 그래프 두께 및 상단 값 세팅
            BarData  data = new BarData(dataSets);

            data.setValueTextSize(10f);
            data.setValueTextColor(Color.BLACK);
//            data.setValueTypeface(mTfLight);
            data.setDrawValues(false);
            data.setBarWidth(0.9f);
            data.setValueFormatter(new BarDataFormatter());

//        }

        mChart.setData(data, timeClass);
    }


//    public void setTestData(int count, float range) {
//
//        float start = 0f;   // 시작 위치
//
//        List<BarEntry> yVals1 = new ArrayList<BarEntry>();
//
//        for (int i = (int) start; i < start + count; i++) {
//            float mult = (range + 1);
//            float val = (float) (Math.random() * mult);
//
//            if (Math.random() * 100 < 25) {
//                yVals1.add(new BarEntry(i, val, ContextCompat.getDrawable(mContext, android.R.drawable.btn_star)));
//            } else {
//                yVals1.add(new BarEntry(i, val));
//            }
//        }
//
//        BarDataSet set1;
//
//        if (mChart.getData() != null && mChart.getData().getDataSetCount() > 0) {
//            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
//            set1.setValues(yVals1);
//            mChart.getData().notifyDataChanged();
//            mChart.notifyDataSetChanged();
//        } else {
//            set1 = new BarDataSet(yVals1, "The year 2017");
//            set1.setDrawIcons(false);
//
//            // 그래프 색상 설정
//            set1.setColor(ContextCompat.getColor(mContext, R.color.color_E1147F));
//
//            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
//            dataSets.add(set1);
//
//            // 그래프 두께 및 상단 값 세팅
//            BarData data = new BarData(dataSets);
//            data.setValueTextSize(10f);
//            data.setValueTextColor(Color.RED);
////            data.setValueTypeface(mTfLight);
//            data.setBarWidth(0.9f);
//            data.setValueFormatter(new BarDataFormatter());
//
//            mChart.setData(data, mChart.getTimeClass());
//        }
//    }

    protected RectF mOnValueSelectedRectF = new RectF();

    @Override
    public void onValueSelected(CEntry e, Highlight h) {

        if (e == null)
            return;

        RectF bounds = mOnValueSelectedRectF;
        mChart.getBarBounds((BarEntry) e, bounds);
        MPPointF position = mChart.getPosition(e, YAxis.AxisDependency.LEFT);

        Log.i("bounds", bounds.toString());
        Log.i("position", position.toString());

        Log.i("x-index",
                "low: " + mChart.getLowestVisibleX() + ", high: "
                        + mChart.getHighestVisibleX());

        MPPointF.recycleInstance(position);
    }

    /**
     * 컬러 지정
     * @return
     */
    private int[] getColors() {
        int[] colors = new int[2];
        int[] MATERIAL_COLORS2 = {ContextCompat.getColor(mContext, R.color.colorMain), ContextCompat.getColor(mContext, R.color.x_255_157_48)};
        System.arraycopy(MATERIAL_COLORS2, 0, colors, 0, 2);
        return colors;
    }

    public int rgb(String hex) {
        int color = (int) Long.parseLong(hex.replace("#", ""), 16);
        int r = (color >> 16) & 0xFF;
        int g = (color >> 8) & 0xFF;
        int b = (color >> 0) & 0xFF;
        return Color.rgb(r, g, b);
    }


    @Override
    public void onNothingSelected() { }
}
