package kr.co.hi.medicare.fragment.health.weight;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.net.bluetooth.manager.DeviceDataUtil;
import kr.co.hi.medicare.value.model.WeightModel;
import kr.co.hi.medicare.net.bluetooth.BluetoothManager;
import kr.co.hi.medicare.net.hwNet.ApiData;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.component.CDatePicker;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperWeight;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.health.step.StepInputFragment;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_bdwgh_goal_input;
import kr.co.hi.medicare.net.data.Tr_get_hedctdata;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.net.hwdata.Tr_mber_user_call;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.utilhw.TextWatcherFloatUtil;
import kr.co.hi.medicare.value.Define;
import kr.co.hi.medicare.fragment.IBaseFragment;

import static kr.co.hi.medicare.utilhw.CDateUtil.getForamtyyMMddHHmmssSS;
import static kr.co.hi.medicare.utilhw.CDateUtil.getForamtyyyyMMddHHmm;

/**
 * Created by insystemscompany on 2017. 2. 28..
 */

public class WeightInputFragment extends BaseFragmentMedi implements IBaseFragment {

    private static final String TAG = WeightInputFragment.class.getSimpleName();
    private TextView mDateTv, mTimeTv;

    private EditText mWeightEt;
    private EditText mWeightTargetEt;
    private EditText mWeightBodyRateEt;
    private EditText mWeightinnerVolumeEt;
    private EditText mWeightWaterVolumeEt;
    private EditText mWeightMuscleVolumeEt;
    private int cal_year;
    private int cal_month;
    private int cal_day;
    private int cal_hour;
    private int cal_min;
    private CommonToolBar commonActionBar;
    private LinearLayout edit_lv, dialog_btn_layout;
    private TextView saveBtn;

    private InputMethodManager imm;

    public static Fragment newInstance() {
        StepInputFragment fragment = new StepInputFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_weight_input, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imm = (InputMethodManager) getContext().getSystemService(getContext().INPUT_METHOD_SERVICE);

        /**
         * 상단 바 셋팅
         */
        edit_lv = view.findViewById(R.id.edit_lv);
        edit_lv.setVisibility(View.GONE);

        commonActionBar = view.findViewById(R.id.common_bar);
        commonActionBar.setVisibility(View.VISIBLE);
        saveBtn = view.findViewById(R.id.common_toolbar_right_btn);
        saveBtn.setText(getString(R.string.text_save));
        saveBtn.setOnClickListener(mClickListener);

        dialog_btn_layout = view.findViewById(R.id.dialog_btn_layout);
        dialog_btn_layout.setVisibility(View.GONE);

        mDateTv = (TextView) view.findViewById(R.id.weight_input_date_textview);
        mTimeTv = (TextView) view.findViewById(R.id.weight_input_time_textview);

        mWeightEt = view.findViewById(R.id.etWeightWg);
        mWeightTargetEt = view.findViewById(R.id.etWeightTargetWg);
        mWeightBodyRateEt = view.findViewById(R.id.etWeightBodyRate);
        mWeightinnerVolumeEt = view.findViewById(R.id.etWeightInnerVolume);
        mWeightWaterVolumeEt = view.findViewById(R.id.etWeightWaterVolume);
        mWeightMuscleVolumeEt = view.findViewById(R.id.etWeightMuscleVolume);


        String now_time = getForamtyyyyMMddHHmm(new Date(System.currentTimeMillis()));
        java.util.Calendar cal = CDateUtil.getCalendar_yyyy_MM_dd_HH_mm(now_time);
        cal_year = cal.get(Calendar.YEAR);
        cal_month = cal.get(Calendar.MONTH);
        cal_day = cal.get(Calendar.DAY_OF_MONTH);
        cal_hour = cal.get(Calendar.HOUR_OF_DAY);
        cal_min = cal.get(Calendar.MINUTE);

        mDateTvSet(cal_year, cal_month, cal_day);
        mTimeTvSet(cal_hour, cal_min);

        DBHelper helper = new DBHelper(getContext());
        DBHelperWeight WeightDb = helper.getWeightDb();
        DBHelperWeight.WeightStaticData bottomData = WeightDb.getResultStatic();

        if(bottomData.getWeight().isEmpty()){
            mWeightEt.setHint("0");
        }else{
            mWeightEt.setHint(bottomData.getWeight());
        }

        Tr_login login = UserInfo.getLoginInfo();
        mWeightTargetEt.setEnabled(true);


        mDateTv.setOnTouchListener(mTouchListener);
        mTimeTv.setOnTouchListener(mTouchListener);


        new TextWatcherFloatUtil().setTextWatcher(mWeightEt, 300, 2);
        new TextWatcherFloatUtil().setTextWatcher(mWeightTargetEt, 300, 2);

        getMyInfo();
    }

    @Override
    public void onResume() {
        super.onResume();
        saveBtn.setEnabled(true);
    }

    /**
     * 개인정보 불러오기
     */
    private void getMyInfo() {

        Tr_mber_user_call.RequestData requestData = new Tr_mber_user_call.RequestData();
        Tr_login login = UserInfo.getLoginInfo();
        requestData.mber_sn = login.mber_sn;

        getData(getContext(), Tr_mber_user_call.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_mber_user_call) {
                    Tr_mber_user_call data = (Tr_mber_user_call) obj;

                    if ("Y".equals(data.data_yn)) {
                        mWeightTargetEt.setHint(data.mber_bdwgh_goal);
                    }
                }
            }
        });
    }

    private void valueInit(){

        mDateTv.setText("");
        mDateTv.setTag("");
        mTimeTv.setText("");
        mTimeTv.setTag("");
        mWeightEt.setText("");
        mWeightTargetEt.setText("");
        mWeightBodyRateEt.setText("");
        mWeightinnerVolumeEt.setText("");
        mWeightWaterVolumeEt.setText("");
        mWeightMuscleVolumeEt.setText("");

    }

    private void seveGoalWeight() {
        SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MISSION_POP,true);
        if(mWeightTargetEt.getText().toString().length()==0){
            valueInit();
            onBackPressed();
        }else {

            Tr_bdwgh_goal_input.RequestData requestData = new Tr_bdwgh_goal_input.RequestData();
            Tr_login login = UserInfo.getLoginInfo();
            requestData.mber_sn = login.mber_sn;
            requestData.mber_bdwgh_goal = mWeightTargetEt.getText().toString();

            getData(getContext(), Tr_bdwgh_goal_input.class, requestData, new ApiData.IStep() {
                @Override
                public void next(Object obj) {
                    if (obj instanceof Tr_bdwgh_goal_input) {
                        Tr_bdwgh_goal_input data = (Tr_bdwgh_goal_input) obj;

                        if ("Y".equals(data.reg_yn)) {
                            MediNewNetworkModule.doReLoginMediCare(getContext(), new MediNewNetworkHandler() {
                                @Override
                                public void onSuccess(BaseData responseData) {
                                    valueInit();
                                    onBackPressed();
                                }

                                @Override
                                public void onFailure(int statusCode, String response, Throwable error) {
                                    valueInit();
                                    onBackPressed();
                                }
                            });





                        }
                    }
                }
            });
        }
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            if (vId == R.id.common_toolbar_right_btn) {
                if(!TextUtils.isEmpty(mWeightEt.getText().toString())) {
                    //미래시간 입력방지
                    if (StringUtil.getLongVal(mDateTv.getTag().toString() + mTimeTv.getTag().toString()) > StringUtil.getLongVal(CDateUtil.getToday_yyyy_MM_dd_HH_mm())) {
                        CDialog.showDlg(getContext(), getString(R.string.message_nowtime_over));
                        return;
                    }
                }
                validInputCheck();
            }
        }
    };

    View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                int vId = v.getId();
                if (vId == R.id.weight_input_date_textview) {
                    showDatePicker(v);
                } else if (vId == R.id.weight_input_time_textview) {
                    showTimePicker();
                }
            }
            return false;
        }
    };

    private boolean DateTimeCheck(String type, int pram1, int pram2, int pram3){
        Calendar cal = Calendar.getInstance();

        if(type.equals("D")){
            cal.set(Calendar.YEAR, pram1);
            cal.set(Calendar.MONTH, pram2);
            cal.set(Calendar.DAY_OF_MONTH, pram3);
            cal.set(Calendar.HOUR_OF_DAY, cal_hour);
            cal.set(Calendar.MINUTE, cal_min);

            if(cal.getTimeInMillis() > System.currentTimeMillis()){
                CDialog.showDlg(getContext(), getString(R.string.message_nowtime_over), new CDialog.DismissListener() {
                    @Override
                    public void onDissmiss() {

                    }
                });
                return false;
            }else{
                return true;
            }
        }else{
            cal.set(Calendar.YEAR, cal_year);
            cal.set(Calendar.MONTH, cal_month);
            cal.set(Calendar.DAY_OF_MONTH, cal_day);
            cal.set(Calendar.HOUR_OF_DAY, pram1);
            cal.set(Calendar.MINUTE, pram2);

            if(cal.getTimeInMillis() > System.currentTimeMillis()){
                CDialog.showDlg(getContext(), getString(R.string.message_nowtime_over), new CDialog.DismissListener() {
                    @Override
                    public void onDissmiss() {

                    }
                });

                return false;
            }else{
                return true;
            }
        }
    }

    private void showDatePicker(View v) {
        GregorianCalendar calendar = new GregorianCalendar();

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        String date = mDateTv.getTag().toString();
        if (TextUtils.isEmpty(date) == false) {
            year = StringUtil.getIntVal(date.substring(0 , 4));
            month = StringUtil.getIntVal(date.substring(4 , 6))-1;
            day = StringUtil.getIntVal(date.substring(6 , 8));
        }

        new CDatePicker(getContext(), dateSetListener, year, month, day, false).show();
    }

    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            if(DateTimeCheck("D",year, monthOfYear, dayOfMonth)) {
                cal_year = year;
                cal_month = monthOfYear;
                cal_day = dayOfMonth;
                mDateTvSet(year, monthOfYear, dayOfMonth);
            }
        }

    };

    private void mDateTvSet(int year, int monthOfYear, int dayOfMonth){
        String msg = String.format("%d.%d.%d", year, monthOfYear + 1, dayOfMonth);
        String tagMsg = String.format("%d%02d%02d", year, monthOfYear + 1, dayOfMonth);
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear + 1);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        //미래시간 입력방지
        if(StringUtil.getLongVal(tagMsg) > StringUtil.getLongVal(CDateUtil.getToday_yyyy_MM_dd())){
            return;
        }

        mDateTv.setText(msg+" "+ CDateUtil.getDateToWeek(tagMsg)+"요일");
        mDateTv.setTag(tagMsg);
    }

    private void showTimePicker() {
        Calendar cal = Calendar.getInstance(Locale.KOREA);
        Date now = new Date();
        cal.setTime(now);

        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        String time = mTimeTv.getTag().toString();
        if (TextUtils.isEmpty(time) == false) {
            hour = StringUtil.getIntVal(time.substring(0, 2));
            minute = StringUtil.getIntVal(time.substring(2 , 4));

            Logger.i(TAG, "hour="+hour+", minute="+minute);
        }

        TimePickerDialog dialog = new TimePickerDialog(getContext(), listener, hour, minute, false);
        dialog.show();
    }

    /**
     * 시간 피커 완료
     */
    private TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            if(DateTimeCheck("S",hourOfDay, minute, 0)) {
                cal_hour = hourOfDay;
                cal_min = minute;
                mTimeTvSet(hourOfDay, minute);
            }
        }
    };

    private void mTimeTvSet(int hourOfDay, int minute){
        // 설정버튼 눌렀을 때
        String amPm = getString(R.string.text_morning);
        int hour = hourOfDay;
        if (hourOfDay >= 12) {
            amPm = getString(R.string.text_afternoon);
            if (hourOfDay >= 13)
                hour -= 12;
        }
        String tagMsg = String.format("%02d%02d", hourOfDay, minute);
        String timeStr = String.format("%02d:%02d", hour, minute);
        mTimeTv.setText(amPm + " " + timeStr);
        mTimeTv.setTag(tagMsg);
    }

    private void validInputCheck() {
        Tr_get_hedctdata.DataList data = new Tr_get_hedctdata.DataList();


        final String regDate = mDateTv.getTag().toString();
        if (TextUtils.isEmpty(regDate)) {
            CDialog.showDlg(getContext(), getString(R.string.date_input_error));
            return;
        }

        final String timeStr = mTimeTv.getTag().toString();
        if (TextUtils.isEmpty(timeStr)) {
            CDialog.showDlg(getContext(), getString(R.string.time_input_error));
            return;
        }

        final String mWeight = mWeightEt.getText().toString();
        if(!TextUtils.isEmpty(mWeight)){
            if(StringUtil.getFloat(mWeight) > 300.0f || StringUtil.getFloat(mWeight) < 20.0f){
                CDialog.showDlg(getContext(), getString(R.string.weight_range));
                return;
            }
        }

        if(TextUtils.isEmpty(mWeight)){
            CDialog.showDlg(getContext(), getString(R.string.warning_weight));
            return;
        }

        final String mWeightBodyRate = mWeightBodyRateEt.getText().toString();
        if(!TextUtils.isEmpty(mWeightBodyRate)){
            if(StringUtil.getFloat(mWeightBodyRate) > 50.0f){
                CDialog.showDlg(getContext(), getString(R.string.weight_fat_range));
                return;
            }
        }

        final String mWeightTarget = mWeightTargetEt.getText().toString();

        final String mWeightinner = mWeightinnerVolumeEt.getText().toString();

        final String mWeightWater = mWeightWaterVolumeEt.getText().toString();

        final String mWeightMuscle = mWeightMuscleVolumeEt.getText().toString();


        doInputWeight(regDate, timeStr, mWeight, mWeightTarget, mWeightBodyRate, mWeightinner, mWeightWater, mWeightMuscle);
        saveBtn.setEnabled(false);

//        CDialog.showDlg(getContext(), getString(R.string.weight_regist), new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//
//            }
//        }, null);
    }

    private void doInputWeight(String regDate, String timeStr, final String mWeight, final String mWeightTarget, final String mWeightBodyRate, String mWeightinner, String mWeightWater, String mWeightMuscle ) {

        regDate += timeStr;

        String weightTarget = mWeightTarget;
        if(TextUtils.isEmpty(weightTarget)){
            Tr_login login = UserInfo.getLoginInfo();
            weightTarget = login.mber_bdwgh_goal;
        }

        SparseArray<WeightModel> array = new SparseArray<>();
        WeightModel dataModel = new WeightModel();
        dataModel.setIdx(getForamtyyMMddHHmmssSS(new Date(System.currentTimeMillis())));
        dataModel.setWeight(StringUtil.getFloatVal(mWeight));                 // 체중
        dataModel.setFat(StringUtil.getFloatVal(mWeightBodyRate));      // 체지방률
        dataModel.setObesity(StringUtil.getFloatVal(mWeightinner));     // 내장지방
        dataModel.setBodyWater(StringUtil.getFloatVal(mWeightWater));   // 수분
        dataModel.setMuscle(StringUtil.getFloatVal(mWeightMuscle));     // 근육
        dataModel.setBdwgh_goal(StringUtil.getFloatVal(weightTarget));     // 근육
        dataModel.setBmr(0);
        dataModel.setBone(0);
        dataModel.setHeartRate(0);
        dataModel.setRegType("U");
        dataModel.setRegDate(regDate);

        array.append(0, dataModel);

        if(StringUtil.getFloatVal(mWeight) > 0.f){
            new DeviceDataUtil().uploadWeight(WeightInputFragment.this, dataModel, new BluetoothManager.IBluetoothResult() {
                @Override
                public void onResult(boolean isSuccess) {
                    if (isSuccess) {
                        Tr_login login = UserInfo.getLoginInfo();
                        if(!mWeightTargetEt.getText().toString().isEmpty()){
                            login.mber_bdwgh_goal = mWeightTargetEt.getText().toString();
                            Define.getInstance().setLoginInfo(login);
                        }

                        DBHelper helper = new DBHelper(getContext());
                        DBHelperWeight weightDb = helper.getWeightDb();
                        DBHelperWeight.WeightStaticData bottomData = weightDb.getResultStatic();
                        if(!bottomData.getWeight().isEmpty()){
                            login.mber_bdwgh_app = bottomData.getWeight();

                            int sex             = StringUtil.getIntVal(login.mber_sex);
                            float weight        = StringUtil.getFloatVal(login.mber_bdwgh_app);
                            float height        = StringUtil.getFloatVal(login.mber_height);
                            int calory          = StringUtil.getIntVal(login.goal_mvm_calory);
                            int step = StringUtil.getIntVal(getStepTargetCalulator(sex, height, weight, calory));
//                                    setStepTarget(0, step);

                            Define.getInstance().setLoginInfo(login);
                        }

                        seveGoalWeight();


//                        CDialog.showDlg(getContext(), getString(R.string.regist_success), new CDialog.DismissListener() {
//                            @Override
//                            public void onDissmiss() {
//
//
//                            }
//                        });
                    } else {
                        CDialog.showDlg(getContext(), getString(R.string.text_regist_fail));
                        saveBtn.setEnabled(true);
                    }
                }
            });
        }else{
            new DeviceDataUtil().uploadTargetWeight(WeightInputFragment.this, dataModel, new BluetoothManager.IBluetoothResult() {
                @Override
                public void onResult(boolean isSuccess) {
                    if (isSuccess) {
                        MediNewNetworkModule.doReLoginMediCare(getContext(), new MediNewNetworkHandler() {
                            @Override
                            public void onSuccess(BaseData responseData) {
                                Tr_login login = UserInfo.getLoginInfo();

                                if(!mWeightTargetEt.getText().toString().isEmpty()){
                                    login.mber_bdwgh_goal = mWeightTargetEt.getText().toString();
                                    Define.getInstance().setLoginInfo(login);
                                }

                                seveGoalWeight();
                            }

                            @Override
                            public void onFailure(int statusCode, String response, Throwable error) {
                                Tr_login login = UserInfo.getLoginInfo();

                                if(!mWeightTargetEt.getText().toString().isEmpty()){
                                    login.mber_bdwgh_goal = mWeightTargetEt.getText().toString();
                                    Define.getInstance().setLoginInfo(login);
                                }

                                seveGoalWeight();
                                saveBtn.setEnabled(true);
                            }
                        });


//                        CDialog.showDlg(getContext(), getString(R.string.regist_success), new CDialog.DismissListener() {
//                            @Override
//                            public void onDissmiss() {
//
//                            }
//                        });
                    } else {
                        CDialog.showDlg(getContext(), getString(R.string.text_regist_fail));
                        saveBtn.setEnabled(true);
                    }
                }
            });
        }


    }


    private boolean isValidWeightTargetEt() {
        String text = mWeightTargetEt.getText().toString();
        return TextUtils.isEmpty(text) == false;
    }

    private boolean isValidWeightEt() {
        String text = mWeightEt.getText().toString();
        return TextUtils.isEmpty(text) == false;
    }

    private boolean isValidDate() {
        String text = mDateTv.getText().toString();
        return TextUtils.isEmpty(text) == false;
    }

    private boolean isValidTime() {
        String text = mTimeTv.getText().toString();
        return TextUtils.isEmpty(text) == false;
    }

    @Override
    public void onPause() {
        super.onPause();
        imm.hideSoftInputFromWindow(mWeightEt.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(mWeightTargetEt.getWindowToken(), 0);

    }
}