package kr.co.hi.medicare.fragment.community.holder;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import kr.co.hi.medicare.R;

public class EmptyViewHolder extends RecyclerView.ViewHolder {
    public TextView emptyview;
    public EmptyViewHolder(View v) {
        super(v);
        emptyview = (TextView) v.findViewById(R.id.emptyview);
    }
}
