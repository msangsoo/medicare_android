package kr.co.hi.medicare.fragment.mypage;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import kr.co.hi.medicare.activity.BaseActivityMedicare;
import kr.co.hi.medicare.component.OnClickListener;

import kr.co.hi.medicare.net.hwNet.BaseData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_mber_hospital_booking;
import kr.co.hi.medicare.net.data.Tr_serviceCounsel;
import kr.co.hi.medicare.net.data.Tr_serviceCounselContents;
import kr.co.hi.medicare.net.data.Tr_serviceReserve;
import kr.co.hi.medicare.net.data.Tr_serviceReserve_visit;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 * Created by insystemscompany on 2017. 2. 28..
 */

public class MyServiceFragment extends BaseFragmentMedi {
    private final String TAG = MyServiceFragment.class.getSimpleName();



    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerViewDetail;
    private CounselAdapter mCounselAdapter;//ok
    private ServiceReserveAdapter mReserveAdapter;
    private VisitAdapter mVisitAdapter;
    private ServiceCheckAdapter mCheckAdapter;//ok
    private TextView mTxt;
    private NestedScrollView mDetailscrollview;
    private String maxpage;
    private boolean mIsLast = false;
    private int mPageNo = 1;


    private RadioButton tvServiceCall;
    private RadioButton tvServiceVisit;
    private RadioButton tvServiceReserv;
    private RadioButton tvServiceCheck;

    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 2;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private LinearLayoutManager mLayoutManager;

    private String seq;

//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
////        super.loadActionbar(actionBar);
////        actionBar.setActionBarTitle(getString(R.string.text_setting));
//    }

    public static Fragment newInstance() {
        MyServiceFragment fragment = new MyServiceFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mypage_service, container, false);
        mRecyclerView = view.findViewById(R.id.serviceinfo_recycler_view);
        mRecyclerViewDetail = view.findViewById(R.id.serviceinfo_recycler_detail_view);

        mDetailscrollview = (NestedScrollView) view.findViewById(R.id.detail_scrollview);

        tvServiceCall = view.findViewById(R.id.tvServiceCall);
        tvServiceVisit = view.findViewById(R.id.tvServiceVisit);
        tvServiceReserv = view.findViewById(R.id.tvServiceReserv);
        tvServiceCheck = view.findViewById(R.id.tvServiceCheck);
        mTxt = view.findViewById(R.id.no_contents);

        tvServiceCall.setOnClickListener(mClickListener);
        tvServiceVisit.setOnClickListener(mClickListener);
        tvServiceReserv.setOnClickListener(mClickListener);
        tvServiceCheck.setOnClickListener(mClickListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, getContext());


        //마이페이지
        tvServiceCall.setOnTouchListener(ClickListener);
        tvServiceReserv.setOnTouchListener(ClickListener);
        tvServiceCheck.setOnTouchListener(ClickListener);
        tvServiceVisit.setOnTouchListener(ClickListener);



        //코드부여
        tvServiceCall.setContentDescription(getString(R.string.tvServiceCall));
        tvServiceReserv.setContentDescription(getString(R.string.tvServiceReserv));
        tvServiceCheck.setContentDescription(getString(R.string.tvServiceCheck));
        tvServiceVisit.setContentDescription(getString(R.string.tvServiceVisit));

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setHasFixedSize(true);

        mRecyclerViewDetail.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerViewDetail.setItemAnimator(new DefaultItemAnimator());
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerViewDetail.setHasFixedSize(true);

        mCounselAdapter = new CounselAdapter();
        mReserveAdapter = new ServiceReserveAdapter();
        mVisitAdapter = new VisitAdapter();
        mCheckAdapter = new ServiceCheckAdapter();

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);


        mPageNo = 1;
        mRecyclerView.setVisibility(View.VISIBLE);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = mRecyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                        Log.i("Yaeye!", "end called" + loading + visibleItemCount+"/" + totalItemCount+"/" + firstVisibleItem);
                    }
                }else if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    // End has been reached

                    Log.i("Yaeye!", "end called" + loading + visibleItemCount+"/" + totalItemCount+"/" + firstVisibleItem);

                    if(tvServiceCall.isChecked()==true){
                        getServiceCounsel();
                    }
                    else if(tvServiceVisit.isChecked()==true){
                        getReserve_visit();
                    }
                    else if(tvServiceReserv.isChecked()==true){
                        getServiceReserve();
                    }
                    else if(tvServiceCheck.isChecked()==true){
                        getServiceCheck();
                    }
                    loading = true;
                }
            }
        });

        return view;
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvServiceCall:
                    mPageNo = 1;
                    mIsLast = false;
                    mRecyclerView.setAdapter(mCounselAdapter);
                    getServiceCounsel();
                    reset(0, true);
                    break;
                case R.id.tvServiceVisit:
                    mPageNo = 1;
                    mIsLast = false;
                    mRecyclerView.setAdapter(mVisitAdapter);
                    getReserve_visit();
                    reset(0, true);
                    break;
                case R.id.tvServiceReserv:
                    mPageNo = 1;
                    mIsLast = false;
                    mRecyclerView.setAdapter(mReserveAdapter);
                    getServiceReserve();
                    reset(0, true);
                    break;
                case R.id.tvServiceCheck:
                    mPageNo = 1;
                    mIsLast = false;
                    mRecyclerView.setAdapter(mCheckAdapter);
                    getServiceCheck();
                    reset(0, true);
                    break;
            }
        }
    };

    /**
     * 서비스 신청현황 ( 상담내역 일반상담, 건강상담 )
     */
    public void getServiceCounsel() {
        if (mIsLast)
            return;

        mRecyclerView.setVisibility(View.VISIBLE);
        mTxt.setVisibility(View.GONE);
        mDetailscrollview.setVisibility(View.GONE);

        Tr_serviceCounsel.RequestData requestData = new Tr_serviceCounsel.RequestData();
        Tr_login login = UserInfo.getLoginInfo();


        requestData.mber_sn = login.mber_sn;
        requestData.pageNumber = "" + (mPageNo);
        requestData.mber_grad = login.mber_grad;


        MediNewNetworkModule.doApi(getContext(), Tr_serviceCounsel.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_serviceCounsel) {
                    Tr_serviceCounsel data = (Tr_serviceCounsel)responseData;
                    try {
                        List<Tr_serviceCounsel.ServiceCounsel> list = data.serviceCounsel;

                        mCounselAdapter.setData(list);

                        maxpage = data.maxpageNumber;

                        if ("0".equals(data.maxpageNumber) || StringUtil.getIntVal(data.maxpageNumber) < mPageNo) {
                            mPageNo = 1;
                            mIsLast = true;
                            mRecyclerView.setVisibility(View.GONE);
                            mTxt.setVisibility(View.VISIBLE);
                        }
                        else {

                            mPageNo++;
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
            }
        });
    }

    class CounselAdapter extends RecyclerView.Adapter<CounselAdapter.ViewHolder> {
        List<Tr_serviceCounsel.ServiceCounsel> mList = new ArrayList<>();

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_service_call, parent, false);
            return new ViewHolder(itemView);
        }

        public void setData(List<Tr_serviceCounsel.ServiceCounsel> dataList) {
            if (mPageNo == 1)
                mList.clear();


            mList.addAll(dataList);
            notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            final Tr_serviceCounsel.ServiceCounsel data = mList.get(position);

            holder.title_date_1.setText(StringUtil.getConvertedDateFormatkr(data.time));
            holder.message.setText(data.title);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getCounselContents(data.seq);
                }
            });

        }

        @Override
        public int getItemCount() {
            return mList == null ? 0 : mList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView title_date_1,message;
            ImageView arrow;

            public ViewHolder(View itemView) {
                super(itemView);
                title_date_1 = itemView.findViewById(R.id.title_date_1);
                message = itemView.findViewById(R.id.message);
                arrow = itemView.findViewById(R.id.arrow);
            }
        }
    }


    /**
     * 질문 답변 불러오기
     */
    private void getCounselContents(String seq) {
//        mRecyclerView.setVisibility(View.GONE);
//        mDetailscrollview.setVisibility(View.VISIBLE);

        Tr_serviceCounselContents.RequestData requestData = new Tr_serviceCounselContents.RequestData();
        Tr_login login = UserInfo.getLoginInfo();

        requestData.mber_sn = login.mber_sn;
        requestData.seq = seq;


        MediNewNetworkModule.doApi(getContext(), Tr_serviceCounselContents.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_serviceCounselContents) {
                    Tr_serviceCounselContents data = (Tr_serviceCounselContents)responseData;
                    try {
                        List<Tr_serviceCounselContents.ServiceCounselContents> list = data.serviceCounselContents;

                        if(list.size()>0){
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(MyServiceCallListFragment.CALL_LIST_DATA , (Serializable) list);
                            NewActivity.startActivity(MyServiceFragment.this, MyServiceCallListFragment.class,  bundle);
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
            }
        });
    }

    /**
     * 병원간호내역
     */
    public void getReserve_visit() {
        if (mIsLast)
            return;

        mRecyclerView.setVisibility(View.VISIBLE);
        mDetailscrollview.setVisibility(View.GONE);
        mTxt.setVisibility(View.GONE);

        Tr_serviceReserve_visit.RequestData requestData = new Tr_serviceReserve_visit.RequestData();
        Tr_login login = UserInfo.getLoginInfo();
        requestData.mber_sn = login.mber_sn;
        requestData.pageNumber = "" + (mPageNo);

        MediNewNetworkModule.doApi(getContext(), Tr_serviceReserve_visit.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_serviceReserve_visit) {
                    Tr_serviceReserve_visit data = (Tr_serviceReserve_visit)responseData;
                    try {
                        List<Tr_serviceReserve_visit.serviceReserve_visit> list = data.serviceReserve_visit;

                        mVisitAdapter.setData(list);



                        maxpage = data.maxpageNumber;

                        if ("0".equals(data.maxpageNumber) || StringUtil.getIntVal(data.maxpageNumber) < mPageNo) {
                            mPageNo = 1;
                            mIsLast = true;
                            mRecyclerView.setVisibility(View.GONE);
                            mTxt.setVisibility(View.VISIBLE);
                        } else {
                            mPageNo++;
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
            }
        });

    }

    class VisitAdapter extends RecyclerView.Adapter<VisitAdapter.ViewHolder> {
        List<Tr_serviceReserve_visit.serviceReserve_visit> mList = new ArrayList<>();

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_service, parent, false);
            return new ViewHolder(itemView);
        }

        public void setData(List<Tr_serviceReserve_visit.serviceReserve_visit> dataList) {
            if (mPageNo == 1)
                mList.clear();


            mList.addAll(dataList);
            notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            Tr_serviceReserve_visit.serviceReserve_visit data = mList.get(position);
            holder.date_1.setText(StringUtil.getConvertedDateFormatkr(data.regdate));
            holder.date_2.setText(StringUtil.getConvertedDateFormatkr(data.visit_day));
            holder.title_date_2.setText(getResources().getString(R.string.mypage_service_visitdate));

            holder.hospital.setText("");
            if(data.hospital!=null&&!data.hospital.equals(""))
                holder.hospital.append(data.hospital);

            if(data.branch!=null&&!data.branch.equals(""))
                holder.hospital.append(" / "+data.branch);

            if(data.jindan!=null&&!data.jindan.equals(""))
                holder.hospital.append(" / "+data.jindan);

            if(data.visit_count!=null&&!data.visit_count.equals(""))
                holder.hospital.append(" / "+data.visit_count+"회차");


//            holder.hospital.setText(data.hospital+" / "+data.branch+" / "+data.jindan+" / "+data.visit_count+"회차");
            holder.confirm.setText(data.charge);
        }

        @Override
        public int getItemCount() {
            return mList == null ? 0 : mList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView date_1,date_2,title_date_1,title_date_2,confirm;
            Button hospital;


            public ViewHolder(View itemView) {
                super(itemView);
                date_1 = itemView.findViewById(R.id.date_1);
                date_2 = itemView.findViewById(R.id.date_2);
                title_date_1 = itemView.findViewById(R.id.title_date_1);
                title_date_2 = itemView.findViewById(R.id.title_date_2);
                confirm = itemView.findViewById(R.id.confirm);
                hospital = itemView.findViewById(R.id.hospital);

            }
        }
    }



    /**
     * 진료예약, 검진예약(고유키값) 나중에 회원쪽 cmpny_code HB으로 바꿔야 한다 20180313
     */
    public void getServiceReserve() {
        if (mIsLast)
            return;

        mRecyclerView.setVisibility(View.VISIBLE);
        mDetailscrollview.setVisibility(View.GONE);
        mTxt.setVisibility(View.GONE);

        Tr_serviceReserve.RequestData requestData = new Tr_serviceReserve.RequestData();
        Tr_login login = UserInfo.getLoginInfo();
        requestData.mber_sn = login.mber_sn;
        requestData.pageNumber = "" + (mPageNo);


        MediNewNetworkModule.doApi(getContext(), Tr_serviceReserve.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_serviceReserve) {
                    Tr_serviceReserve data = (Tr_serviceReserve) responseData;
                    try {
                        List<Tr_serviceReserve.Reserve> list = data.serviceReserve;

                        mReserveAdapter.setData(list);


                        maxpage = data.maxpageNumber;

                        if ("0".equals(data.maxpageNumber) || StringUtil.getIntVal(data.maxpageNumber) < mPageNo) {
                            mPageNo = 1;
                            mIsLast = true;
                            mRecyclerView.setVisibility(View.GONE);
                            mTxt.setVisibility(View.VISIBLE);
                        } else {
                            mPageNo++;
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
            }
        });
    }


    class ServiceReserveAdapter extends RecyclerView.Adapter<ServiceReserveAdapter.ViewHolder> {
        List<Tr_serviceReserve.Reserve> mList = new ArrayList<>();

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_service, parent, false);
            return new ViewHolder(itemView);
        }

        public void setData(List<Tr_serviceReserve.Reserve> dataList) {
            if (mPageNo == 1)
                mList.clear();


            mList.addAll(dataList);
            notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            Tr_serviceReserve.Reserve data = mList.get(position);

            holder.date_1.setText(StringUtil.getConvertedDateFormatkr(data.regdate));
            holder.date_2.setText(StringUtil.getConvertedDateFormatkr(data.resrv_de));
            holder.confirm.setText(data.resrv_step);
            holder.hospital.setText(data.contents + " / " + data.hospitalName);
        }

        @Override
        public int getItemCount() {
            return mList == null ? 0 : mList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView date_1,date_2,title_date_1,title_date_2,confirm;
            Button hospital;


            public ViewHolder(View itemView) {
                super(itemView);
                date_1 = itemView.findViewById(R.id.date_1);
                date_2 = itemView.findViewById(R.id.date_2);
                title_date_1 = itemView.findViewById(R.id.title_date_1);
                title_date_2 = itemView.findViewById(R.id.title_date_2);
                confirm = itemView.findViewById(R.id.confirm);
                hospital = itemView.findViewById(R.id.hospital);

            }
        }
    }



    /**
     * 검진예약
     */
    public void getServiceCheck() {
        if (mIsLast)
            return;

        mRecyclerView.setVisibility(View.VISIBLE);
        mDetailscrollview.setVisibility(View.GONE);
        mTxt.setVisibility(View.GONE);

        Tr_mber_hospital_booking.RequestData requestData = new Tr_mber_hospital_booking.RequestData();
        Tr_login login = UserInfo.getLoginInfo();
        requestData.mber_sn = login.mber_sn;
        requestData.pageNumber = "" + (mPageNo);


        MediNewNetworkModule.doApi(getContext(), Tr_mber_hospital_booking.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_mber_hospital_booking) {
                    Tr_mber_hospital_booking data = (Tr_mber_hospital_booking) responseData;
                    try {
                        List<Tr_mber_hospital_booking.Booking_arr> list = data.booking_arr;

                        mCheckAdapter.setData(list);


                        maxpage = data.maxpageNumber;
                        if ("0".equals(data.maxpageNumber) || StringUtil.getIntVal(data.maxpageNumber) < mPageNo||list==null||list.size()<1) {
                            mPageNo = 1;
                            mIsLast = true;
                            mRecyclerView.setVisibility(View.GONE);
                            mTxt.setVisibility(View.VISIBLE);
                        } else {
                            mPageNo++;
                        }

//                        if(list.size()<1){
//                            mPageNo = 1;
//                            mIsLast = true;
//                            mRecyclerView.setVisibility(View.GONE);
//                            mTxt.setVisibility(View.VISIBLE);
//                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                        mPageNo = 1;
                        mIsLast = true;
                        mRecyclerView.setVisibility(View.GONE);
                        mTxt.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {


            }
        });
    }


    class ServiceCheckAdapter extends RecyclerView.Adapter<ServiceCheckAdapter.ViewHolder> {
        List<Tr_mber_hospital_booking.Booking_arr> mList = new ArrayList<>();

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_service, parent, false);
            return new ViewHolder(itemView);
        }

        public void setData(List<Tr_mber_hospital_booking.Booking_arr> dataList) {
            if (mPageNo == 1)
                mList.clear();

            mList.addAll(dataList);
            notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            Tr_mber_hospital_booking.Booking_arr data = mList.get(position);

            holder.date_1.setText(StringUtil.getConvertedDateFormatkr(data.reg_date));
            holder.date_2.setText("1차:"+StringUtil.getConvertedDateFormat(data.first_date)+" / 2차:"+StringUtil.getConvertedDateFormat(data.second_date));
            holder.confirm.setText(data.step);
            holder.hospital.setText(data.h_name);
        }

        @Override
        public int getItemCount() {
            return mList == null ? 0 : mList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView date_1,date_2,title_date_1,title_date_2,confirm;
            Button hospital;


            public ViewHolder(View itemView) {
                super(itemView);
                date_1 = itemView.findViewById(R.id.date_1);
                date_2 = itemView.findViewById(R.id.date_2);
                title_date_1 = itemView.findViewById(R.id.title_date_1);
                title_date_2 = itemView.findViewById(R.id.title_date_2);
                confirm = itemView.findViewById(R.id.confirm);
                hospital = itemView.findViewById(R.id.hospital);

            }
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mPageNo = 1;
    }

    @Override
    public void onBackPressed() {

        if (mRecyclerViewDetail != null) {
            mDetailscrollview.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            if (getActivity() instanceof BaseActivityMedicare) {
                ((BaseActivityMedicare)getActivity()).finishStep();
            }
//            super.onBackPressed();
        }
    }

    public void reset(int previousTotal, boolean loading) {
        this.previousTotal = previousTotal;
        this.loading = loading;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getCurrentFragment() == MainActivityMedicare.HOME_MENU_5) {
            tvServiceCall.performClick();
            tvServiceCall.setChecked(true);
            tvServiceReserv.setChecked(false);
            tvServiceCheck.setChecked(false);
            tvServiceVisit.setChecked(false);
            reset(0, true);
        }

    }
}
