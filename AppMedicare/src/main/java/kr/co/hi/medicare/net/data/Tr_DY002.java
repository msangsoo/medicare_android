package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 맞춤동영상 > 운동지각도 입력 / 수정

 OSEQ : 회원일련번호 RESULT_CODE : 결과코드
 0000 : 모션 등록성공 4444 : 필수 입력값이 없습니다.(등록실패) 6666 : 회원이 존재하지 않음 9999 : 기타오류
 */

public class Tr_DY002 extends BaseData {

    public static class RequestData {
//        strJson={"DOCNO":"DY002","SEQ":"0001013000015","MOTION_CODE":"E","ML_SEQ":"1"}
        public String DOCNO;
        public String SEQ;
        public String MOTION_CODE;
        public String ML_SEQ;


    }

    public Tr_DY002(){
        super.conn_url = "https://m.shealthcare.co.kr/HL_MED_COMMUNITY/ws.asmx/getJson";
        super.json_obj_name = "strJson";
    }


    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {
            JSONObject body = getBaseJsonObj();

            RequestData data = (RequestData) obj;

            body.put("DOCNO", "DY002");
            body.put("SEQ", data.SEQ);
            body.put("MOTION_CODE", data.MOTION_CODE);
            body.put("ML_SEQ", data.ML_SEQ);

            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("OSEQ")
    public String OSEQ;
	@SerializedName("RESULT_CODE")
    public String RESULT_CODE;

}