package kr.co.hi.medicare.util;

import java.text.DecimalFormat;

public enum PhysicalActivityUtil {
    /**
     * 걷기(약 5km/h의 보통 속도)
     */
    WALKING(3.3f, "걷기(약 5km/h의 보통 속도)", 1),
    /**
     * 달리기(약 8km/h의 일반적인 조깅)
     */
    RUNNING(8.0f, "달리기(약 8km/h의 일반적인 조깅)", 2),
    /**
     * 자전거(레저활동)
     */
    BICYCLE(4.0f, "자전거(레저활동)", 3),
    /**
     * 계단 오르기
     */
    CLIMBING_STAIRS(8.0f, "계단 오르기", 4),
    /**
     * 줄넘기
     */
    JUMP_ROPE(10.0f, "줄넘기", 5),
    /**
     * 에어로빅
     */
    AEROBIC(6.5f, "에어로빅", 6),
    /**
     * 스트레칭
     */
    STRETCHING(2.5f, "스트레칭", 7),
    /**
     * 근력 운동(일반적인 강도)
     */
    STRENGTH_TRAINING(5.5f, "근력 운동(일반적인 강도)", 8),
    /**
     * 수영(자유형)
     */
    SWIMMING_FREESTYLE(8.0f, "수영(자유형)", 9),
    /**
     * 수영(배영)
     */
    SWIMMING_BACKSTROKE(7.0f, "수영(배영)",10),
    /**
     * 수영(평영)
     */
    SWIMMING_BREASTSTROKE(10.0f, "수영(평영)",11),
    /**
     * 수영(접영)
     */
    SWIMMING_BUTTERFLY(11.0f, "수영(접영)",12),
    /**
     * 아쿠아로빅
     */
    AQUAROBICS(4.0f, "아쿠아로빅",13),
    /**
     * 요가
     */
    YOGA(2.5f, "요가",14),
    /**
     * 골프
     */
    GOLF(4.5f, "골프",15),
    /**
     * 배드민턴
     */
    BADMINTON(4.5f, "배드민턴",16),
    /**
     * 탁구
     */
    TABLE_TENNIS(4.0f, "탁구",17),
    /**
     * 테니스(단식)
     */
    TENNIS_SINGLE(7.0f, "테니스(단식)",18),
    /**
     * 테니스(복식)
     */
    TENNIS_DOUBLE(5.0f, "테니스(복식)",19),
    /**
     * 등산
     */
    HIKING(7.0f, "등산",20),
    /**
     * 농구
     */
    BASKETBALL(8.0f, "농구",21),
    /**
     * 축구
     */
    SOCCER(10.0f, "축구",22),
    /**
     * 야구
     */
    BASEBALL(5.0f, "야구",23),
    /**
     * 태극권
     */
    TAI_CHI_CHUAN(4.0f, "태극권",24),
    /**
     * 인라인 스케이팅
     */
    INLINE_SKATING(12.5f, "인라인 스케이팅",25),
    /**
     * 스케이트 보딩
     */
    SKATEBOARDING(5.0f, "스케이트 보딩",26),
    /**
     * 스키
     */
    SKIING(7.0f, "스키",27),
    /**
     * 스노우보딩
     */
    SNOWBOARDING(7.0f, "스노우보딩",28),
    /**
     * 가사(걸레질)
     */
    HOUSEWORK_WIPE_CLOTH(3.5f, "가사(걸레질)",29),
    /**
     * 가사(진공청소기로 청소하기)
     */
    HOUSEWORK_VACUUM(3.5f, "가사(진공청소기로 청소하기)",30),
    /**
     * 가사(설거지하기)
     */
    HOUSEWORK_DISH(2.3f, "가사(설거지하기)",31),
    /**
     * 가사(요리 및 음식준비)
     */
    HOUSEWORK_COOK(2.0f, "가사(요리 및 음식준비)",32),
    /**
     * 가사(다림질)
     */
    HOUSEWORK_PRESS(2.3f, "가사(다림질)",33),
    /**
     * 가사(빨래 널기, 접기 등)
     */
    HOUSEWORK_WASH(2.0f, "가사(빨래 널기, 접기 등)",34),
    /**
     * 가사(욕실 청소)
     */
    HOUSEWORK_BATHROOM(3.8f, "가사(욕실 청소)",35);


    private float mets = 0f;
    private String name = null;
    private int idx = 0;

    PhysicalActivityUtil(float mets, String name, int index) {
        this.mets = mets;
        this.name = name;
        this.idx = index;
    }

    public String getName() {
        return name;
    }

    public int getValue()
    {
        return this.ordinal() + 1;
    }

    public String getName(int code)
    {
        for(PhysicalActivityUtil physicalActivityUtil :PhysicalActivityUtil.values())
        {
            if(code == physicalActivityUtil.getValue())
            {
                return physicalActivityUtil.getName();
            }
        }
        return "";
    }

    public String getCalorie(int weight, int minute) {
        String pattern = "####.#";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        float cal = (float) ((0.0175 * weight * mets * minute) + 0.05);
        return decimalFormat.format(cal);
    }

}
