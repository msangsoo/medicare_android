package kr.co.hi.medicare.fragment.community;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;

/**
 * Created by mrsohn on 2017. 3. 14..
 * 혈당관리
 */

public class CommunityImageFragment extends BaseFragmentMedi implements NewActivity.onFinishListener{
    private final String TAG = CommunityImageFragment.class.getSimpleName();

    public static final String KEY_IMAGE_PATH = "KEY_IMAGE_PATH";
    public static final String KEY_NICKNAME = "KEY_NICKNAME";

    private ImageView btn_close;
    private PhotoView image;
    private TextView title;
    private String nick,imagepath;

    public static Fragment newInstance() {
        CommunityImageFragment fragment = new CommunityImageFragment();
        return fragment;
    }

    /**
     * 액션바 세팅
     */
//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        super.loadActionbar(actionBar);
//        actionBar.setWhiteTheme();
//    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_community_image, container, false);


        image = view.findViewById(R.id.image);
        btn_close = view.findViewById(R.id.btn_close);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title = view.findViewById(R.id.title);


        Bundle bundle = getArguments();
        if (bundle != null){
            nick = bundle.getString(KEY_NICKNAME);
            imagepath = bundle.getString(KEY_IMAGE_PATH);
        }




        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        title.setText(nick);
        if(!imagepath.equals("")){
            Glide
                    .with(getContext())
                    .load(imagepath)
                    .into(image);
        }

        ((NewActivity)getContext()).setOnFinishListener(this);
    }

    @Override
    public void catchFinish() {

    }
}
