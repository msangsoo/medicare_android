package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.hi.medicare.utilhw.Logger;

/**
 *임직원몰연동
 input 값
 insures_code : 회사코드
 gclife_id : 임직원몰 id
 gclife_pw : 임직원몰 pw
 mber_sn : 회원키

 output값
 api_code : 호출코드명
 insures_code : 회사코드
 gclife_id : 임직원몰 id
 data_yn : 처리여부(y/n)
 */

public class Tr_gclife_login extends BaseData {
    private final String TAG = Tr_gclife_login.class.getSimpleName();

    public static class RequestData {

        public String mber_sn;
        public String gclife_id;
        public String gclife_pw;
    }


    public Tr_gclife_login() {

        super.conn_url = "http://m.shealthcare.co.kr/HL_MED/WebService/MED_MOBILE_CALL.asmx/MED_mobile_Call";

    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof Tr_gclife_login.RequestData) {
            JSONObject body = new JSONObject();
            Tr_gclife_login.RequestData data = (Tr_gclife_login.RequestData) obj;

            body.put("api_code", getApiCode(TAG) ); //
            body.put("insures_code", INSURES_CODE);
            body.put("mber_sn", data.mber_sn); //  1000
            body.put("gclife_id",  data.gclife_id); //
            body.put("gclife_pw",  data.gclife_pw); //

            return body;
        }

        return super.makeJson(obj);
    }


    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("api_code") // mber_main_call",
    public String api_code; //
    @SerializedName("insures_code") // 303",
    public String insures_code; //
    @SerializedName("data_yn") // 1344",
    public String data_yn; //
    @SerializedName("gclife_id") // 총 남은 포인트.
    public String gclife_id; //
}
