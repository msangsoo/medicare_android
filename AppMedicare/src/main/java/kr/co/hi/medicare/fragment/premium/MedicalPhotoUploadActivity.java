package kr.co.hi.medicare.fragment.premium;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.Header;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.BaseActivityMedicare;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.util.AlertDialogUtil;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.Util;


/**
 * Created by yoonjihoon on 2016-04-25.
 * 진료 소견소 전송하기 화면
 */
public class MedicalPhotoUploadActivity extends BaseActivityMedicare implements View.OnClickListener {

//    public static final String VIEWSTATE   =   "/wEPDwUJOTY2MDQ4OTAyZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WAQUMSW1hZ2VCdXR0b24xr2itDS/DzKvKzcpU+QObxI9E9Y1qUqk7V1OTY6a734s=";
//    public static final String EVENTVALIDATION= "/wEWAwLX2JjXBgKCpKjbBQLSwpnTCEsuQB+4X0ar26p53rGg5IblIIzcefU93OxY73Hy9YiY";
//    public static final String UPLOAD_URL   =   "https://www.walkie.co.kr:449/upload/HS_HL_EXAM/picture_upload.aspx";

    public String VIEWSTATE   =   "";
    public String EVENTVALIDATION= "";
    public static final String UPLOAD_URL   =   "https://wkd.walkie.co.kr:443/HS_HL/upload/HS_HL_EXAM/picture_upload.aspx";

    private UserInfo user;

    private ImageView mPhotoImg;
    private TextView mCancelTv, mSendTv;

    private Intent intent = null;
    private String mPhotoUrl;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.medical_photo_upload_activity);

        init();
        setEvent();

        intent = getIntent();
        if(intent != null ){
            mPhotoUrl   =   intent.getStringExtra("EXTRA_PHOTO");
            CustomImageLoader.displayImage(MedicalPhotoUploadActivity.this, "file://" + mPhotoUrl, mPhotoImg);
        }

    }

    /**
     * 초기화
     */
    public void init(){

        user = new UserInfo(this);

        mPhotoImg       =   (ImageView) findViewById(R.id.photo_img);
        mCancelTv       =   (TextView)  findViewById(R.id.cancel_tv);
        mSendTv         =   (TextView)  findViewById(R.id.send_tv);

    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        mCancelTv.setOnClickListener(this);
        mSendTv.setOnClickListener(this);
    }

    String sViewState = "id=\"__VIEWSTATE\" value=\"";
    String sEventValidation = "id=\"__EVENTVALIDATION\" value=\"";
    String sEndStr = "\" />";

    private void UIThreadTest() {
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            URL url = new URL(UPLOAD_URL);
                            HttpURLConnection conn = (HttpURLConnection)url.openConnection();// 접속
                            if (conn != null) {
                                conn.setConnectTimeout(2000);
                                conn.setUseCaches(false);
                                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK){
//                                    Log.d("hsh", "HTTP_OK");
                                    //    데이터 읽기
                                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(),"euc-kr"));//"utf-8"
                                    while(true) {
                                        String line = br.readLine();
                                        if (line == null) break;
                                        if (line.contains(sViewState)){
                                            line = line.substring(line.indexOf(sViewState)+sViewState.length(),line.indexOf(sEndStr));
                                            VIEWSTATE = line;
//                                            Log.d("hsh", "VIEWSTATE "+VIEWSTATE);
                                        }else if(line.contains(sEventValidation)){
                                            line = line.substring(line.indexOf(sEventValidation)+sEventValidation.length(),line.indexOf(sEndStr));
                                            EVENTVALIDATION = line;
//                                            Log.d("hsh", "EVENTVALIDATION "+EVENTVALIDATION);
                                        }

                                    }
                                    br.close(); // 스트림 해제
                                }
                                conn.disconnect(); // 연결 끊기
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        finally {
                            if(!VIEWSTATE.equals("") && !EVENTVALIDATION.equals("")) {
                                Handler mHandler = new Handler(Looper.getMainLooper());
                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        requestPhotoUpdate(mPhotoUrl);
                                    }
                                }, 0);

                            }else{
                                Toast.makeText(MedicalPhotoUploadActivity.this, getString(R.string.networkexception), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
                t.start(); // 쓰레드 시작
            }
        }, 0);
    }

    public void requestPhotoUpdate(final String upfile){

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                try {

                    AsyncHttpClient client = new AsyncHttpClient();
                    RequestParams params = new RequestParams();
                    client.setTimeout(10000);
                    client.addHeader("Accept-Charset", "UTF-8");
                    client.addHeader("User-Agent", new WebView(getBaseContext()).getSettings().getUserAgentString());

                    // data form
                    params.put("SEQ", user.getSeq());  // 회원 고유번호
                    params.put("__VIEWSTATE", VIEWSTATE);
                    params.put("__EVENTVALIDATION", EVENTVALIDATION);
                    params.put("ImageButton1.x", "27");
                    params.put("ImageButton1.y", "22");
                    params.put("UPFILE1", new File(upfile), MimeTypeMap.getSingleton().getMimeTypeFromExtension("jpg"));


                    CLog.i("UPLOAD_URL =  " +UPLOAD_URL +"?" +params.toString());

                    client.post(UPLOAD_URL, params, mAsyncHttpHandler);

                } catch (Exception e) {
                    CLog.e(e.toString());
                }
            }
        }, 300);
    }

    @Override
    public void onClick(View v) {

        if(getButtonClickEnabled() == false)
            return;

        setButtonClickEnabled(false);

        switch (v.getId()){
            case R.id.cancel_tv:    // 취소
                CLog.i("cancel_tv");
                finish();
                break;
            case R.id.send_tv:
                AlertDialog.Builder alert = new AlertDialog.Builder(MedicalPhotoUploadActivity.this);
                alert.setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        requestPhotoUpdate(mPhotoUrl);
                        UIThreadTest();
                        dialog.dismiss();
                    }
                }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
                alert.setMessage(getString(R.string.msg_dialog_medical_photo_upload_content));
                alert.show();
                break;
        }

        setButtonClickEnabled(true);

    }

    private AsyncHttpResponseHandler mAsyncHttpHandler = new AsyncHttpResponseHandler() {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            CLog.i("onSuccess");

            try {

                int count = headers.length;

                for (int i = 0; i < count; i++) {
                    CLog.v(headers[i].getName() + ": " + headers[i].getValue());
                }

                String response = new String(responseBody, "UTF-8");
                CLog.i("response = " +response);
                JSONObject resultData = null;

                if(response.startsWith("<")){   // api result 문자열 시작값이 "<" 로 시작하는 경우는 xml 폼
                    try {
                        response =  Util.parseXml(response);
                        CLog.i("response = " +response);
                    }catch(Exception e){
                        CLog.e(e.toString());
                    }

                    resultData = new JSONObject(response);

                }else{
                    resultData = new JSONObject(response);
                }

                String resultCode = resultData.getString("RESULT_CODE");
                switch(resultCode){
                    case "0000":
                        setResult(RESULT_OK);
                        finish();
                        break;
                    case "9999":
                        UIThread(MedicalPhotoUploadActivity.this, getString(R.string.confirm), getString(R.string.msg_error));
                        break;
                    default:
                        UIThread(MedicalPhotoUploadActivity.this, getString(R.string.confirm), getString(R.string.msg_error));
                        break;
                }

            } catch (Exception e) {
                CLog.e(e.toString());
            }

//            hideProgress();
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            CLog.i("onFailure");
            UIThread(MedicalPhotoUploadActivity.this, getString(R.string.confirm), getString(R.string.networkexception));
        }

    };

    private void UIThread(final Activity activity, final String confirm, final String message) {
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertDialogUtil.onAlertDialog(activity, confirm, message);
            }
        }, 0);
    }


    @Override
    public void response(Record record) throws UnsupportedEncodingException {

    }

    @Override
    public void request(EServerAPI eServerAPI, Object obj) {

    }

    @Override
    public void networkException(Record record) {

    }
}
