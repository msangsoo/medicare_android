package kr.co.hi.medicare.push;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.activity.SplashActivityMedicare;
import kr.co.hi.medicare.common.DummyWebviewFragment;
import kr.co.hi.medicare.component.PushUtils;
import kr.co.hi.medicare.fragment.community.CommunityNoticeFragment;
import kr.co.hi.medicare.fragment.health.HealthFragment;
import kr.co.hi.medicare.fragment.home.healthinfo.HealthColumnFragment;
import kr.co.hi.medicare.fragment.home.healthinfo.HealthMainFragment;
import kr.co.hi.medicare.fragment.home.healthinfo.HealthMealFragment;
import kr.co.hi.medicare.fragment.mypage.MyPointMainFragment;
import kr.co.hi.medicare.util.Util;
import kr.co.hi.medicare.utilhw.SharedPref;

public class NotiDummyActivity extends Activity {
    public static final String TAG = NotiDummyActivity.class.getSimpleName();
    public static final String MENU_ID = "MENU_ID";
    public static final String HIST_SN = "HIST_SN";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPref.getInstance().initContext(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        CharSequence s = "전달 받은 값: ";
        String menuId = "";
        String histSN = "";

        if (getIntent() == null) {

        } else {
            menuId = getIntent().getStringExtra(MENU_ID);
            histSN = getIntent().getStringExtra(HIST_SN);
        }

//        if (BuildConfig.DEBUG)
//            menuId = "7";

        Log.i(TAG, "NotiDummyActivity.MENU_ID="+menuId);
        String mainActivity = MainActivityMedicare.class.getName();
        boolean isExcuteApp = Util.getServiceTaskName(this, mainActivity); // 앱 실행 중 여부


        if (isExcuteApp) {
            // 앱이 실행중일 때 (MainActivityMedicare 가 떠 있을때)
//            intent= new Intent(this, MainActivityMedicare.class);
            pushmove(this, menuId,histSN);
        } else {
            Intent intent = new Intent(this, SplashActivityMedicare.class);
            SharedPref.getInstance().savePreferences(SharedPref.PUSH_MOVE_INDEX, menuId);
            SharedPref.getInstance().savePreferences(SharedPref.PUSH_MOVE_INDEX_HIST, histSN);
            startActivity(intent);
        }

        finish();
        overridePendingTransition(0,0); // 애니메이션 없애기 코드
    }

    /**
     * 건강관리 탭이동 처리
     */
    public static void moveToMenu(Activity activity,int idx) {
        Intent intent = new Intent(activity,MainActivityMedicare.class);
        intent.putExtra(MainActivityMedicare.INTENT_KEY_MOVE_MENU_PUSH, idx);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        activity.startActivity(intent);
    }



    /**
     * 메뉴 이동 처리
     * @param activity
     * @param move_idx
     */
    public static void pushmove(final Activity activity, String move_idx, String hist_sn){
//        String move_idx = SharedPref.getInstance().getPreferences(SharedPref.PUSH_MOVE_INDEX);
        final Bundle bundle = new Bundle();
//        bundle.putString("BACKTITLE","홈");
        if(TextUtils.isEmpty(move_idx) == false){
            if("1".equals(move_idx)){
                NewActivity.startActivityForResult(activity, CommunityNoticeFragment.REQUEST_CODE_NOTICE, CommunityNoticeFragment.class, bundle );
            } else if("2".equals(move_idx)){
                bundle.putString(DummyWebviewFragment.TITLE,activity.getString(R.string.health_info_menu1) );
                bundle.putString(DummyWebviewFragment.URL, hist_sn);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        NewActivity.startActivity(activity, DummyWebviewFragment.class, bundle);
                    }
                }, 500);
            } else if("4".equals(move_idx)){
                NewActivity.startActivityForResult(activity, CommunityNoticeFragment.REQUEST_CODE_NOTICE, CommunityNoticeFragment.class, bundle );
            } else if("5".equals(move_idx)){
                NewActivity.startActivity(activity, MyPointMainFragment.class, null);
            } else if("6".equals(move_idx)){
                NewActivity.startActivityForResult(activity, CommunityNoticeFragment.REQUEST_CODE_NOTICE, CommunityNoticeFragment.class, bundle );
            } else if("7".equals(move_idx)){
                NewActivity.startActivityForResult(activity, CommunityNoticeFragment.REQUEST_CODE_NOTICE, CommunityNoticeFragment.class, bundle );
            } else if("8".equals(move_idx)){
                HealthFragment.pos = 0;
                moveToMenu(activity,MainActivityMedicare.HOME_MENU_2);

            } else if("9".equals(move_idx)){
                HealthFragment.pos = 2;
                moveToMenu(activity,MainActivityMedicare.HOME_MENU_2);

            } else if("10".equals(move_idx)){
                HealthFragment.pos = 3;
                moveToMenu(activity,MainActivityMedicare.HOME_MENU_2);

            } else if("11".equals(move_idx)){
                HealthFragment.pos = 4;
                moveToMenu(activity,MainActivityMedicare.HOME_MENU_2);

            } else if("12".equals(move_idx)){
                bundle.putString(DummyWebviewFragment.TITLE, activity.getString(R.string.health_info_menu2));
                bundle.putString(DummyWebviewFragment.URL, hist_sn);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        NewActivity.startActivity(activity, DummyWebviewFragment.class, bundle);
                    }
                }, 500);


            }
        }
//        SharedPref.getInstance().savePreferences(SharedPref.PUSH_MOVE_INDEX,"");
    }

    /**
     * 푸시 메시지를 전달 받으면 상태표시바에 표시함
     */
    public static void showPushMessage(Context context, int notiId, String title, String content, String moveMenu, String hist_sn) {

//        boolean isExcuteApp = getServiceTaskName(context.); // 앱 실행 중 여부
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        PushUtils.releaseWakeLock();

//        Intent notificationIntent = new Intent(this, SplashActivity.class);
        Intent notificationIntent = new Intent(context, NotiDummyActivity.class);
        if (TextUtils.isEmpty(moveMenu) == false)
            notificationIntent.putExtra(NotiDummyActivity.MENU_ID, moveMenu); // 이동할 메뉴
            notificationIntent.putExtra(NotiDummyActivity.HIST_SN, hist_sn); // index번호
//        notificationIntent.putExtra("extraString", data.menuId); //전달할 값*/
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(context, (int)(System.currentTimeMillis()/1000), notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationCompat.Builder builder;// = new NotificationCompat.Builder(this);
        long[] vib = {500l, 100l, 500l, 100l};

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String CHANNEL_ID = context.getString(R.string.app_name);
            String CHANNEL_NAME = context.getString(R.string.app_name);
            @SuppressLint("WrongConstant")
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_HIGH);
            channel.enableLights(false);
            channel.enableVibration(true);

            notificationManager.createNotificationChannel(channel);

            builder = new NotificationCompat.Builder(context, CHANNEL_ID);
        } else {
            builder = new NotificationCompat.Builder(context);
        }

//        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        String notiTitle = TextUtils.isEmpty(title) ? context.getString(R.string.app_name) : title;

        builder.setContentTitle(notiTitle)   // 상태바 드래그시 보이는 타이틀
                .setContentText(content)                                // 상태바 드래그시 보이는 서브타이틀
                .setTicker(content)                                     // 상태바 한줄 메시지
                .setSmallIcon(R.mipmap.ic_stat_notify)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setContentIntent(contentIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setDefaults(Notification.DEFAULT_ALL);


//        SharedPref.getInstance().savePreferences(SharedPref.PUSH_MOVE_INDEX, data.menuId);
        nm.notify(notiId, builder.build());
    }



}
