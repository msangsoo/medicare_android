package kr.co.hi.medicare.fragment.health.food;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.net.hwNet.ApiData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.component.CDatePicker;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperFoodCalorie;
import kr.co.hi.medicare.database.DBHelperFoodDetail;
import kr.co.hi.medicare.database.DBHelperFoodMain;
import kr.co.hi.medicare.fragment.PictureBaseFragment;
import kr.co.hi.medicare.net.hwdata.Tr_get_meal_input_data;
import kr.co.hi.medicare.net.hwdata.Tr_get_meal_input_food_data;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.net.hwdata.Tr_meal_input_data;
import kr.co.hi.medicare.net.hwdata.Tr_meal_input_edit_data;
import kr.co.hi.medicare.net.hwdata.Tr_meal_input_food_data;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.utilhw.cameraUtil.ProviderUtil;
import kr.co.hi.medicare.value.Define;


/**
 * Created by mrsohn on 2017. 3. 14..
 */

public class FoodInputFragment extends PictureBaseFragment {

    private final String TAG = FoodInputFragment.class.getSimpleName();

    public static final String BUNDLE_FOOD_INPUT_DATE       = "food_input_date";  // 식사일기에서 현재 날자
    public static final String BUNDLE_FOOD_MEAL_TYPE        = "bundle_food_meal_type";  // mealtype

    public static final String BUNDLE_MEAL_DATA             = "bundle_meal_data";  // 식사일기에서 조회된 식사 데이터(단일)
    public static final String BUNDLE_FOOD_DATA             = "bundle_food_data";  // 식사일기에서 조회된 음식 데이터(배열)
    public static final String ACTION_TITLE             = "action_bar_title";  // 식사일기에서 조회된 음식 데이터(배열)

    public final int REQUEST_FOOD_SEARCH = 4444;

    private TextView mCameraBtn;//, mPictureIv;
    private TextView mDateTv, mTimeTv;
    private EditText mEatTimeEt;
    private String mMealType = "";
    private TextView mTitle;
    private String mAction_Title;
//    private ImageView mFoodPicture; // 음식사진이미지.

    private FoodSwipeListView mSwipeListView;

    private InputMethodManager mImm;

    private String mIdx = CDateUtil.getForamtyyMMddHHmmssSS(new Date(System.currentTimeMillis()));

    private Tr_get_meal_input_data.ReceiveDatas mMealData               = null;
    private List<Tr_get_meal_input_food_data.ReceiveDatas> mFoodList    = new ArrayList<>();  // 아침

    public static Fragment newInstance() {
        FoodInputFragment fragment = new FoodInputFragment();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_food_input, container, false);
        Bundle bundle = getArguments();
        // 아침:a,점심   b ,저녁 c , 아침간식 d , 점심간식 e , 저녁간식  f
        mMealType = bundle.getString(BUNDLE_FOOD_MEAL_TYPE);
        mMealData = bundle.getParcelable(BUNDLE_MEAL_DATA); // 식사 데이터
        mFoodList = bundle.getParcelableArrayList(BUNDLE_FOOD_DATA);    // 음식 데이터
        mAction_Title = bundle.getString(ACTION_TITLE);

        if (mMealData != null) {
            mIdx = mMealData.idx;
        }


        Logger.i(TAG, "onCreateView.mMealData=" + mMealData);
        Logger.i(TAG, "onCreateView.mFoodList=" + mFoodList.size());
        Logger.i(TAG, "onCreateView.mFoodList=" + mIdx);

        super.imagefileName = mIdx;

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mImm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        //액션바 셋팅
        mTitle = view.findViewById(R.id.common_toolbar_title);
        mTitle.setText(mAction_Title);
        TextView saveBtn = view.findViewById(R.id.common_toolbar_right_btn);
        // 식사 데이터가 있으면 수정으로 판단
        if (mMealData == null) {
            saveBtn.setText(getString(R.string.text_save));
        } else {
            saveBtn.setText(getString(R.string.text_modify));
        }
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beforeUploadData();
            }
        });

        mCameraBtn      = view.findViewById(R.id.food_input_camera_btn);
//        mPictureIv      = view.findViewById(R.id.food_input_picture_imageview);
        mDateTv         = (TextView) view.findViewById(R.id.food_input_date_textview);
        mTimeTv         = (TextView) view.findViewById(R.id.food_input_time_textview);
        mEatTimeEt      = view.findViewById(R.id.food_eat_time_tv);
        super.ivPreview    = view.findViewById(R.id.food_picture);     // 음식사진이미지.

        // 스와이프 리스트뷰 세팅 하기
        mSwipeListView  = new FoodSwipeListView(view, FoodInputFragment.this);

        mCameraBtn.setOnClickListener(mClickListener);
//        mPictureIv.setOnClickListener(mClickListener);
        mDateTv.setOnTouchListener(mTouchListener);
        mTimeTv.setOnTouchListener(mTouchListener);

        Bundle bundle   = getArguments();
        String date     = bundle.getString(BUNDLE_FOOD_INPUT_DATE);
        Calendar cal    = CDateUtil.getCalendar_yyyy_MM_dd_HH_mm(date);

        mDateTv.setText(date);
        mDateTv.setTag(date);

        // 백으로 데이터 보내기
        Bundle backBundle = new Bundle();
        backBundle.putString(BUNDLE_FOOD_INPUT_DATE, date);
        setBackData(backBundle);

        view.findViewById(R.id.food_search_button).setOnClickListener(mClickListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, getContext());


        //건강
        mCameraBtn.setOnTouchListener(ClickListener);
        view.findViewById(R.id.food_search_button).setOnTouchListener(ClickListener);

        //코드부여
        mCameraBtn.setContentDescription(getString(R.string.mCameraBtn));
        view.findViewById(R.id.food_search_button).setContentDescription(getString(R.string.food_search_button));


        if (mMealData != null) { // 식사 데이터
            String amounttime = mMealData.amounttime;
            amounttime = TextUtils.isEmpty(amounttime) ? "" : amounttime;
            mEatTimeEt.setText(amounttime);
            if (TextUtils.isEmpty(mMealData.regdate) == false)
                cal         = CDateUtil.getCalendar_yyyy_MM_dd_HH_mm(mMealData.regdate);
            int year        = cal.get(Calendar.YEAR);
            int month       = cal.get(Calendar.MONTH);
            int hourOfDay   = cal.get(Calendar.HOUR_OF_DAY);
            int minute      = cal.get(Calendar.MINUTE);
            setTimeTv(hourOfDay, minute);

            if (TextUtils.isEmpty(mMealData.picture) == false){
                getImageData(mMealData.picture, super.ivPreview);  // 서버에 이미지가 있다면 서버이미지 우선.
            }else{
                getIndexToImageData(mMealData.idx, super.ivPreview);   // 이미지 세팅
            }

            getFoodListData(mMealData.idx);
        } else {
            if (getString(R.string.text_breakfast_code).equals(mMealType)) {
                setTimeTv(8, 00);   // 아침
            } else if (getString(R.string.text_lunch_code).equals(mMealType)) {
                setTimeTv(12, 00);  // 점심
            } else if (getString(R.string.text_dinner_code).equals(mMealType)) {
                setTimeTv(18, 00);  // 저녁
            } else if (getString(R.string.text_breakfast_snack_code).equals(mMealType)) {
                setTimeTv(9, 00);   // 아침간식
            } else if (getString(R.string.text_lunch_snack_code).equals(mMealType)) {
                setTimeTv(14, 00);  // 점심간식
            } else if (getString(R.string.text_dinner_snack_code).equals(mMealType)) {
                setTimeTv(20, 00);  // 저녁간식
            }
        }
    }

    public void getIndexToImageData(final String idx, final ImageView iv) {

        try {
            String path = Define.getFoodPhotoPath(idx);
//            Uri uri = ProviderUtil.getOutputMediaFileUri(getContext(), new File(path));
//            Bitmap bitmapImage =
//                    MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);

            if (TextUtils.isEmpty(idx)) {
                Logger.e(TAG, "getIndexToImageData idx is null");
                return;
            }

            // 로컬에서 받아오기
            Bitmap bitmap;
            Logger.i(TAG, "getIndexToImageData.path="+path);
            File imgFile = new File(path);
            if (imgFile.exists()) {
                bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                iv.setImageBitmap(bitmap);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 데이터 가져오기 (음식)
     */
    private void getFoodListData(String idx) {
        DBHelper helper = new DBHelper(getContext());
        DBHelperFoodDetail db = helper.getFoodDetailDb();

        List<DBHelperFoodCalorie.Data> foodList = db.getFoodList(idx);
        mSwipeListView.setDataList(foodList);
    }

    /**
     * 음식 데이터
     *
     * @return
     */
    private void setFoodListData(Tr_get_meal_input_food_data data) {
        mFoodList.clear();
        for (Tr_get_meal_input_food_data.ReceiveDatas recv : data.data_list) {
            mFoodList.add(recv);
        }

        DBHelper helper = new DBHelper(getContext());
        DBHelperFoodCalorie db = helper.getFoodCalorieDb();
        if (mFoodList.size() > 0) {
            List<DBHelperFoodCalorie.Data> foodList = db.getResult(mFoodList);
            mSwipeListView.setDataList(foodList);
        }
    }

    /**
     * 식사, 음식정보 서버 전송전 체크 및 값 세팅
     */
    private void beforeUploadData() {
        String dayStr = mDateTv.getTag().toString();
        String timeStr = mTimeTv.getTag().toString();
        if (TextUtils.isEmpty(timeStr)) {
            CDialog.showDlg(getContext(), getString(R.string.food_meal_time_input))
                    .setOneBtn(R.drawable.draw_alert_ok_btn);
            return;
        }

        // 식사이미지만으로도 등록 할 수 있도록 수정.
        List<DBHelperFoodCalorie.Data> dataList = mSwipeListView.getListData();

        if(mMealData == null) {
            if (dataList.size() <= 0) {
                CDialog.showDlg(getContext(), getString(R.string.food_add))
                        .setOneBtn(R.drawable.draw_alert_ok_btn);
                return;
            }
        }

        //미래시간 입력방지
        if(StringUtil.getLongVal(mDateTv.getTag().toString()+mTimeTv.getTag().toString()) > StringUtil.getLongVal(CDateUtil.getToday_yyyy_MM_dd_HH_mm())){
            CDialog.showDlg(getContext(), getString(R.string.message_nowtime_over))
                    .setOneBtn(R.drawable.draw_alert_ok_btn);
            return;
        }

        // 식사시간(선택 사항)
        final String amountTime = StringUtil.getIntString(mEatTimeEt.getText().toString());
        final String regDate = CDateUtil.getRegDateFormat_yyyyMMddHHss(dayStr + timeStr);

        Logger.i(TAG, "amountTime=" + amountTime + ", regDate=" + regDate);
        final Tr_login login = UserInfo.getLoginInfo();

        uploadMealData(mIdx, amountTime, regDate, login.mber_sn);
    }


    /**
     * 식사일지 등록하기/수정하기
     *
     * @param idx
     * @param amountTime
     * @param regDate
     */
    private void uploadMealData(final String idx, final String amountTime, final String regDate, final String mber_sn) {

        if (mMealData != null) {
            // 수정하기
            final Tr_meal_input_edit_data.RequestData requestData = new Tr_meal_input_edit_data.RequestData();
            requestData.idx         = mMealData.idx;
            requestData.mber_sn     = mber_sn;
            requestData.amounttime  = amountTime;
            requestData.mealtype    = mMealType;
            requestData.calorie     = "" + mSwipeListView.getSumCalorieData();
            requestData.regdate     = regDate;
            requestData.picture     = "";

            getData(getContext(), Tr_meal_input_edit_data.class, requestData, new ApiData.IStep() {
                @Override
                public void next(Object obj) {
                    if (obj instanceof Tr_meal_input_edit_data) {
                        Tr_meal_input_edit_data data = (Tr_meal_input_edit_data) obj;
                        if ("Y".equals(data.reg_yn)) {



                            updateFoodEatData(requestData,regDate,mber_sn);


//                            onBackPressed();

//                            CDialog.showDlg(getContext(), getString(R.string.modify_success), new CDialog.DismissListener() {
//                                @Override
//                                public void onDissmiss() {
//                                    onBackPressed();
//                                }
//                            });
                        } else {
                            CDialog.showDlg(getContext(), mMealData == null?getString(R.string.text_regist_fail):getString(R.string.text_update_fail))
                                    .setOneBtn(R.drawable.draw_alert_ok_btn);
                        }
                    }
                }
            });
        } else {

            // 등록하기
            final Tr_meal_input_data.RequestData requestData = new Tr_meal_input_data.RequestData();

            requestData.idx         = CDateUtil.getForamtyyMMddHHmmssSS(new Date(System.currentTimeMillis()));
            requestData.mber_sn     = mber_sn;
            requestData.idx         = idx;
            requestData.amounttime  = amountTime;
            requestData.mealtype    = mMealType;
            requestData.calorie     = "" + mSwipeListView.getSumCalorieData();
            requestData.regdate     = regDate;
            requestData.picture     = "";
            getData(getContext(), Tr_meal_input_data.class, requestData, new ApiData.IStep() {
                @Override
                public void next(Object obj) {
                    if (obj instanceof Tr_meal_input_data) {
                        Tr_meal_input_data data = (Tr_meal_input_data) obj;

                        if ("Y".equals(data.reg_yn)) {

                            DBHelper helper = new DBHelper(getContext());
                            DBHelperFoodMain db = helper.getFoodMainDb();
                            db.insert(requestData, true);

                            uploadFoodEatData(idx, amountTime, regDate, mber_sn);
                        } else {
                            CDialog.showDlg(getContext(), mMealData == null?getString(R.string.text_regist_fail):getString(R.string.text_update_fail))
                                    .setOneBtn(R.drawable.draw_alert_ok_btn);
                        }
                    }
                }
            });
        }
    }

    /**
     * 음식 등록하기
     */
    private void uploadFoodEatData(final String idx, final String amountTime, final String regDate, String mber_sn) {
        final List<DBHelperFoodCalorie.Data> dataList = mSwipeListView.getListData();
//        if (dataList.size() <= 0) {
//            CDialog.showDlg(getContext(), getString(R.string.food_add));
//            return;
//        }

        // 음식데이터가 없다면.
        if (dataList.size() <= 0) {

            uploadPicture(idx);

            // 음식데이터가 있다면.
        }else {

            // 등록하기
            final Tr_meal_input_food_data.RequestData requestData = new Tr_meal_input_food_data.RequestData();

            requestData.idx = idx;
            requestData.mber_sn = mber_sn;
            requestData.food_mass = Tr_meal_input_food_data.getArray(dataList, regDate);
            getData(getContext(), Tr_meal_input_food_data.class, requestData, new ApiData.IStep() {
                @Override
                public void next(Object obj) {
                    if (obj instanceof Tr_meal_input_food_data) {
                        Tr_meal_input_food_data data = (Tr_meal_input_food_data) obj;

                        if ("Y".equals(data.reg_yn)) {
                            // 음식 등록하기
                            DBHelper helper = new DBHelper(getContext());
                            DBHelperFoodDetail db = helper.getFoodDetailDb();
                            db.insert(dataList, mIdx, regDate);

                            // 사진업로드.
                            uploadPicture(mIdx);
                        } else {
                            CDialog.showDlg(getContext(), mMealData == null?getString(R.string.text_regist_fail):getString(R.string.text_update_fail))
                                    .setOneBtn(R.drawable.draw_alert_ok_btn);
                        }
                    }
                }
            });
        }
    }


    /**
     * 음식 수정하기
     */
    String tempRegDate;
    private void updateFoodEatData(final Tr_meal_input_edit_data.RequestData mainData, final String regDate, String mber_sn) {
        final List<DBHelperFoodCalorie.Data> dataList = mSwipeListView.getListData();

        tempRegDate = regDate;

        if(dataList.size() == 0){
            DBHelperFoodCalorie.Data data = new DBHelperFoodCalorie.Data();
            data.food_code = "";
            data.forpeople = "";
            tempRegDate = "";

            dataList.add(data);
        }

        // 수정하기
        final Tr_meal_input_food_data.RequestData requestData = new Tr_meal_input_food_data.RequestData();

        requestData.idx = mainData.idx;
        requestData.mber_sn = mber_sn;
        requestData.food_mass = Tr_meal_input_food_data.getArray(dataList, tempRegDate);
        getData(getContext(), Tr_meal_input_food_data.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_meal_input_food_data) {
                    Tr_meal_input_food_data data = (Tr_meal_input_food_data) obj;

                    if ("Y".equals(data.reg_yn)) {
                        // 음식 수정
                        DBHelper helper = new DBHelper(getContext());
                        DBHelperFoodMain db = helper.getFoodMainDb();
                        if (mSwipeListView.getListData().isEmpty() == false)
                            db.update(mainData, mSwipeListView.getListData(), true);
                        else
                            db.deleteFoodList(mMealData.idx);

                        uploadPicture(mainData.idx);
                    } else {
                        CDialog.showDlg(getContext(), mMealData == null?getString(R.string.text_regist_fail):getString(R.string.text_update_fail))
                                .setOneBtn(R.drawable.draw_alert_ok_btn);
                    }
                }
            }
        });
    }



    // 사진업로드.
    private void uploadPicture(final String Db_idx){
        Logger.i(TAG, "uploadPicture.outputFileUri="+outputFileUri);
//        if (outputFileUri != null && new File(outputFileUri.getPath()).isFile()) {
        if (outputFileUri != null) {
            String realPath = ProviderUtil.getOutputFilePath(outputFileUri);
            doUploadPicture(mIdx, realPath , new ApiData.IStep() {
                @Override
                public void next(Object obj) {
                    try {
                        String result = new JSONObject(obj.toString()).getString("data_yn");
                        String filename = new JSONObject(obj.toString()).getString("file_name");
                        if ("Y".equals(result)) {

                            // 음식 업데이트
                            DBHelper helper = new DBHelper(getContext());
                            DBHelperFoodMain db = helper.getFoodMainDb();
                            db.updateImage(Db_idx, "http://m.shealthcare.co.kr/HL_MED/food_img/"+filename);

                            if(mMealData == null){
                                SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MISSION_POP,true);
                            } else{
                                SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MISSION_POP,false);
                            }

                            getActivity().setResult(Activity.RESULT_OK);
                            getActivity().finish();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                        CDialog.showDlg(getContext(), "등록에 실패 했습니다.")
                                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        getActivity().finish();
                                    }
                                });
                    }
                }
            });
        }else{

            if(mMealData == null){
                SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MISSION_POP,true);
            } else{
                SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MISSION_POP,false);
            }
            onBackPressed();
//            CDialog.showDlg(getContext(), mMealData == null?getString(R.string.regist_success):getString(R.string.update_success), new CDialog.DismissListener() {
//                @Override
//                public void onDissmiss() {
//                    onBackPressed();
//
//
//                }
//            })
//                    .setOneBtn(R.drawable.draw_alert_ok_btn);
        }
    }


    /** 음식사진 업로드.
     *  식사
     /HWG/WebService/MED_food_upload.ashx
     cmpny_code=303&mber_sn=1111&sn=기존값
     * @param path
     * @param iStep
     */
    private void doUploadPicture(String sn, String path, final ApiData.IStep iStep) {
        String param        = sn;          // 업로드 페이지에서 추가할 param 값
        String filePath     = path;         // contentUri.getPath();
        Logger.i(TAG, "doUploadPicture.mIdx=" + sn + ", filePath=" + filePath);
        Tr_login login = UserInfo.getLoginInfo();
        String params = "cmpny_code=304&mber_sn="+login.mber_sn+"&sn="+sn;
        HttpAsyncFileTask33 rssTask = new HttpAsyncFileTask33(getString(R.string.food_upload_url), params, new HttpAsyncTaskInterface() {
            @Override
            public void onPreExecute() {
            }
            @Override
            public void onPostExecute(String data) {
            }
            @Override
            public void onError() {
                CDialog.showDlg(getContext(), getString(R.string.text_network_data_send_fail))
                        .setOneBtn(R.drawable.draw_alert_ok_btn);
            }
            @Override
            public void onFileUploaded(String result) {
                Log.i(TAG, "doUploadPicture=" + result);
                iStep.next(result);
            }
        });

        rssTask.setParam(path);
        rssTask.execute();
    }

    View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                int vId = v.getId();
                if (vId == R.id.food_input_date_textview) {
                    showDatePicker(v);
                } else if (vId == R.id.food_input_time_textview) {
                    showTimePicker();
                }
            }
            return false;
        }
    };


    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            if (vId == R.id.food_input_camera_btn) {
                showSelectGalleryCamera();
            } else if (vId == R.id.food_search_button) {
                String title = getArguments().getString(CommonToolBar.TOOL_BAR_TITLE);
                Logger.i(TAG, "food_search_button.title=" + title);
                Bundle bundle = new Bundle();
                bundle.putString(CommonToolBar.TOOL_BAR_TITLE, title);
                NewActivity.startActivityForResult(FoodInputFragment.this, REQUEST_FOOD_SEARCH, FoodSearchFragment.class, bundle);
            }
        }
    };


    private void showDatePicker(View v) {
        GregorianCalendar calendar = new GregorianCalendar();

        int year    = calendar.get(Calendar.YEAR);
        int month   = calendar.get(Calendar.MONTH);
        int day     = calendar.get(Calendar.DAY_OF_MONTH);

        String date = mDateTv.getTag().toString();
        if (TextUtils.isEmpty(date) == false) {
            year    = StringUtil.getIntVal(date.substring(0, 4));
            month   = StringUtil.getIntVal(date.substring(4, 6)) - 1;
            day     = StringUtil.getIntVal(date.substring(6, 8));
        }

        new CDatePicker(getContext(), dateSetListener, year, month, day, false).show();
    }

    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String msg      = String.format("%d.%d.%d", year, monthOfYear + 1, dayOfMonth);
            String tagMsg   = String.format("%d%02d%02d", year, monthOfYear + 1, dayOfMonth);
            Calendar cal    = Calendar.getInstance();

            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, monthOfYear + 1);
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            //미래시간 입력방지
            if(StringUtil.getLongVal(tagMsg) > StringUtil.getLongVal(CDateUtil.getToday_yyyy_MM_dd())){
                return;
            }

            mDateTv.setText(msg);
            mDateTv.setTag(tagMsg);
        }

    };

    private void showTimePicker() {
        Calendar cal = Calendar.getInstance(Locale.KOREA);
        Date now = new Date();
        cal.setTime(now);

        int hour    = cal.get(Calendar.HOUR_OF_DAY);
        int minute  = cal.get(Calendar.MINUTE);
        String time = mTimeTv.getTag().toString();
        if (TextUtils.isEmpty(time) == false) {
            hour = StringUtil.getIntVal(time.substring(0, 2));
            minute = StringUtil.getIntVal(time.substring(2, 4));

            Logger.i(TAG, "hour=" + hour + ", minute=" + minute);
        }

        TimePickerDialog dialog = new TimePickerDialog(getContext(), listener, hour, minute, false);
        dialog.show();
    }

    /**
     * 시간 피커 완료
     */
    private TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            setTimeTv(hourOfDay, minute);
        }
    };

    private void setTimeTv(int hourOfDay, int minute) {
        String amPm = "오전";
        int hour = hourOfDay;
        if (hourOfDay > 11) {
            amPm = "오후";
            if (hourOfDay >= 13)
                hour -= 12;
        } else {
            hour = hour == 0 ? 12 : hour;
        }
        String tagMsg = String.format("%02d%02d", hourOfDay, minute);
        String timeStr = String.format("%02d:%02d", hour, minute);
        mTimeTv.setText(amPm + " " + timeStr);
        mTimeTv.setTag(tagMsg);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_FOOD_SEARCH && resultCode == Activity.RESULT_OK) {
                ArrayList<DBHelperFoodCalorie.Data> info = new ArrayList<>();
                info.addAll((Collection<? extends DBHelperFoodCalorie.Data>) data.getSerializableExtra(FoodSearchFragment.BUNDLE_FOOD_DETAIL_INFO));
                for(int i=0; i<info.size(); i++){
                    Logger.i(TAG, "REQUEST_FOOD_SEARCH=" + info.get(i).food_name);
                    mSwipeListView.setData(info.get(i));
                }
            } else {
                // 카메라 갤러리 인 경우
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mImm.hideSoftInputFromWindow(mEatTimeEt.getWindowToken(), 0);

    }

}

