package kr.co.hi.medicare.fragment.health.question;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.net.hwNet.BaseData;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.StringUtil;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_hra_check_result_input;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;

/**
 * 질병 위험도 체크.
 */
// implements ActionSheet.ActionSheetListener{
public class WebviewQuestionFragment extends BaseFragmentMedi {

    public static final String TITLE     = "title";
    public static final String URL       = "url";
    public static final String POS       = "pos";

    private WebView activity_question_WebView_webview;
    private final Handler handler = new Handler();
    private boolean isContent = false;
    private String content = "";
    private int titlePos = 0;
    private int targetPos = 0;
    private int mSum = 0;
    private String sUrl = "";
    private TextView mTitle;

    public static Fragment newInstance() {
        WebviewQuestionFragment fragment = new WebviewQuestionFragment();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_question_webview, container, false);

        Bundle bundle = getArguments();
        if (bundle != null) {
            titlePos = StringUtil.getIntVal(bundle.getString(TITLE));
            targetPos = StringUtil.getIntVal(bundle.getString(POS));
            sUrl = bundle.getString(URL);
        }

        mTitle = view.findViewById(R.id.common_toolbar_title);

        init(view);

        return view;
    }
    private String initTitle(int position)
    {

        switch (position) {
            case 0:
                return getString(R.string.question1);
            case 1:
                return getString(R.string.question2);
            case 2:
                return getString(R.string.question3);
            case 3:
                return getString(R.string.question4);
            case 4:
                return getString(R.string.question5);
            case 5:
                return getString(R.string.question6);
            case 6:
                return getString(R.string.question7);
            case 7:
                return getString(R.string.question8);
            case 8:
                return getString(R.string.question9);
            case 9:
                return getString(R.string.question10);
            case 100:
                return getString(R.string.answer);
            case 101:
                return "서비스 안내";

        }
        return "서비스 안내";
    }

    private void init(View view) {

        activity_question_WebView_webview = (WebView) view.findViewById(R.id.activity_question_WebView_webview);
        activity_question_WebView_webview.getSettings().setJavaScriptEnabled(true);
        activity_question_WebView_webview.setWebViewClient(new WebViewClient());
        activity_question_WebView_webview.getSettings().setDefaultTextEncodingName("utf-8");
        activity_question_WebView_webview.getSettings().setBuiltInZoomControls(true); // 줌 허용
        activity_question_WebView_webview.getSettings().setSupportZoom(true);
        activity_question_WebView_webview.getSettings().setDisplayZoomControls(false);
        activity_question_WebView_webview.addJavascriptInterface(new AndroidBridge(), "HybridApp");
        activity_question_WebView_webview.setWebViewClient(new WebViewClientClass());
        activity_question_WebView_webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                new AlertDialog.Builder(getContext())
                        .setTitle("알림")
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok,
                                new AlertDialog.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        result.confirm();
                                    }
                                })
                        .setCancelable(false)
                        .create()
                        .show();
                return true;
            }
        });

        if(titlePos < 100)
        {
            activity_question_WebView_webview.loadUrl(sUrl);
        }
        else if(titlePos == 100) // 진단 결과
        {
            UserInfo userInfo = new UserInfo(getContext());
            int pos = targetPos;
            Tr_login login = UserInfo.getLoginInfo();
            mSum = userInfo.getQuestionSum(pos);

            Tr_hra_check_result_input.RequestData requestData = new Tr_hra_check_result_input.RequestData();

            requestData.mber_sn = login.mber_sn;
            requestData.total_score = Integer.toString(mSum);
            requestData.moon_key = Integer.toString(targetPos);

            MediNewNetworkModule.doApi(getContext(), Tr_hra_check_result_input.class, requestData,true,true, new MediNewNetworkHandler() {
                @Override
                public void onSuccess(BaseData responseData) {
                    if (responseData instanceof Tr_hra_check_result_input) {
                        Tr_hra_check_result_input data = (Tr_hra_check_result_input)responseData;
                        try {

                            int level = Integer.parseInt(data.d_level);
                            UserInfo userInfo = new UserInfo(getContext());
                            CLog.i("SAVE : " + targetPos + " / " + level + " / " + mSum);

                            userInfo.setQuestion(targetPos, level, mSum);

                            String str = data.comment;
                            str = str.replaceAll("&lt;", "<");
                            str = str.replaceAll("&gt;", ">");
                            str = str.replaceAll("\n" , "");
                            str = str.replaceAll("\r", "");
                            str = str.replaceAll("\t", "");

                            updateLayoutAnswer(str);

                        } catch (Exception e) {
                            CLog.e(e.toString());
                        }
                    }
                }

                @Override
                public void onFailure(int statusCode, String response, Throwable error) {
                    CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
                }
            });
        }
    }


    private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if(url.startsWith("mailto:") || url.startsWith(("tel:")))
                return false;
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {


            if(isContent)
            {
                isContent = false;

                activity_question_WebView_webview.loadUrl(String.format("javascript:sendContent('%s');",  content));
                mTitle.setText(getString(R.string.answer));
            }
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }

    private void updateLayoutAnswer(final String comment)
    {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                titlePos = 100;
//        loadActionbar(getCommonActionBar());
                content = comment;
                isContent = true;

                activity_question_WebView_webview.loadUrl("file:///android_asset/answer.html");
            }
        });

    }

    private class AndroidBridge {
        @JavascriptInterface
        public void sendSum(String no, String sum) {
            Log.i("SEND", "no = " + no + " sum = " + sum);

            titlePos = 100;
            targetPos = Integer.parseInt(no);
            mSum = Integer.parseInt(sum);
            Tr_login login = UserInfo.getLoginInfo();
            Tr_hra_check_result_input.RequestData requestData = new Tr_hra_check_result_input.RequestData();

            requestData.mber_sn = login.mber_sn;
            requestData.total_score = sum;
            requestData.moon_key = no;

            MediNewNetworkModule.doApi(getContext(), Tr_hra_check_result_input.class, requestData,false,true, new MediNewNetworkHandler() {
                @Override
                public void onSuccess(BaseData responseData) {
                    if (responseData instanceof Tr_hra_check_result_input) {
                        Tr_hra_check_result_input data = (Tr_hra_check_result_input)responseData;
                        try {

                            int level = Integer.parseInt(data.d_level);
                            UserInfo userInfo = new UserInfo(getContext());
//                            CLog.i("SAVE : " + targetPos + " / " + level + " / " + mSum);

                            userInfo.setQuestion(targetPos, level, mSum);

                            String str = data.comment;
                            str = str.replaceAll("&lt;", "<");
                            str = str.replaceAll("&gt;", ">");
                            str = str.replaceAll("\n" , "");
                            str = str.replaceAll("\r", "");
                            str = str.replaceAll("\t", "");

                            updateLayoutAnswer(str);

                        } catch (Exception e) {
                            CLog.e(e.toString());
                        }
                    }
                }

                @Override
                public void onFailure(int statusCode, String response, Throwable error) {
                    CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
                }
            });
        }

        @JavascriptInterface
        public void sendLink(final String url) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    // bmi_input question html에 호출하는 태그가 없음
                }
            });
        }

        @JavascriptInterface
        public void sendReCall() {
            handler.post(new Runnable()
            {
                public void run()
                {
                    int stitle = StringUtil.getIntVal(getArguments().getString(TITLE));
                    int spos = StringUtil.getIntVal(getArguments().getString(POS));

                    Logger.i("sendReCall", " [stitle] : "+stitle+" [surl] : "+sUrl+" [spos] : " +spos);
                    titlePos = spos - 1;
                    targetPos = spos;

//                    loadActionbar(getCommonActionBar());
                    content = "";
                    isContent = false;
                    activity_question_WebView_webview.loadUrl(sUrl);
                }
            });
        }

        @JavascriptInterface
        public void sendFinsh() {
            handler.post(new Runnable() {
                public void run() {
                    onBackPressed();
                }
            });
        }

        @JavascriptInterface
        public void callDial(){
            String tel = "tel:15885814";
            startActivity(new Intent("android.intent.action.DIAL", Uri.parse(tel)));
        }

    }

}
