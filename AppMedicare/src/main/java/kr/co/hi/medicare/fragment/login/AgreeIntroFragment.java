package kr.co.hi.medicare.fragment.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.BaseActivityMedicare;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.common.DummyWebviewFragment;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_offer_info_mod;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.Logger;

/**
 * Created by MrsWin on 2017-02-16.
 */

public class AgreeIntroFragment extends BaseFragmentMedi implements BaseActivityMedicare.onKeyBackCatchListener {
    private final String TAG = AgreeIntroFragment.class.getSimpleName();

    private CheckBox mStep1Cb;
    private CheckBox mStep2Cb;
    private CheckBox mStep3Cb;

    private CheckBox mAllCheckCheckBox;
    private TextView mAllCheckTextView;

    private Button next_button;

    public static Fragment newInstance() {
        AgreeIntroFragment fragment = new AgreeIntroFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.agree_intro_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getToolBar(view).getLeftICon().setVisibility(View.GONE);    // 백 버튼 가리기

        mStep1Cb = (CheckBox) view.findViewById(R.id.join_step1_checkbox1);
        mStep2Cb = (CheckBox) view.findViewById(R.id.join_step1_checkbox2);
        mStep3Cb = (CheckBox) view.findViewById(R.id.join_step1_checkbox3);

        mAllCheckCheckBox = view.findViewById(R.id.join_step_join1_all_checkbox);
        mAllCheckTextView = view.findViewById(R.id.join_step_all_check_textview);
        next_button = (Button) view.findViewById(R.id.next_button);

        view.findViewById(R.id.join_step1_contract_textview).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.join_step1_personal_info_textview).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.join_step1_info_share_textview).setOnClickListener(mOnClickListener);
        mStep1Cb.setOnClickListener(mOnClickListener);
        mStep2Cb.setOnClickListener(mOnClickListener);
        mStep3Cb.setOnClickListener(mOnClickListener);

        mAllCheckCheckBox.setOnClickListener(mOnClickListener);
        mAllCheckTextView.setOnClickListener(mOnClickListener);

        view.findViewById(R.id.next_button).setOnClickListener(mOnClickListener);

        mStep1Cb.setOnCheckedChangeListener(mCheckedChageListener);
        mStep2Cb.setOnCheckedChangeListener(mCheckedChageListener);
        mStep3Cb.setOnCheckedChangeListener(mCheckedChageListener);

    }


    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();

            Bundle bundle = new Bundle();
            String url = "";
            switch (vId) {
                case R.id.next_button :
                    if (BtnEnableCheck()) {
                        agreeApi();
                    }
                    break;
                case R.id.join_step_join1_all_checkbox :
                    boolean isCheck = mAllCheckCheckBox.isChecked();
                    mStep1Cb.setChecked(isCheck);
                    mStep2Cb.setChecked(isCheck);
                    mStep3Cb.setChecked(isCheck);
                    break;
                case R.id.join_step_all_check_textview :
                    isCheck = !mAllCheckCheckBox.isChecked();
                    mAllCheckCheckBox.setChecked(isCheck);
                    mStep1Cb.setChecked(isCheck);
                    mStep2Cb.setChecked(isCheck);
                    mStep3Cb.setChecked(isCheck);
                    break;
                case R.id.join_step1_checkbox1 :
                case R.id.join_step1_checkbox2 :
                case R.id.join_step1_checkbox3 :
                    mAllCheckCheckBox.setChecked(mStep1Cb.isChecked() && mStep2Cb.isChecked() && mStep3Cb.isChecked());
                    break;
                case R.id.join_step1_contract_textview :
                    bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract1_title_1));
                    url = getString(R.string.personal_terms_1_url);
                    bundle.putString(DummyWebviewFragment.URL, url);
                    NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                    break;
                case R.id.join_step1_personal_info_textview :
                    bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract2_title_1));
                    url = getString(R.string.personal_terms_2_url);
                    bundle.putString(DummyWebviewFragment.URL, url);
                    NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                    break;
                case R.id.join_step1_info_share_textview:
                    bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract3_title_1));
                    url = getString(R.string.personal_terms_3_url);
                    bundle.putString(DummyWebviewFragment.URL, url);
                    NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                    break;
            }
        }
    };

    /**
     * Btn enable true / false
     */
    private boolean BtnEnableCheck(){
        // 약관동의여부
        boolean isContacts = mStep1Cb.isChecked() && mStep2Cb.isChecked() && mStep3Cb.isChecked();
        Log.i(TAG, "isContacts="+isContacts);

        next_button.setEnabled(isContacts);

        return isContacts;
    }

    CompoundButton.OnCheckedChangeListener mCheckedChageListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            BtnEnableCheck();
        }
    };


    /**
     * 정회원 가입 가능 여부 판별
     */
    public void agreeApi() {

        Tr_offer_info_mod.RequestData requestData = new Tr_offer_info_mod.RequestData();
        final Tr_login userInfo = UserInfo.getLoginInfo();
        requestData.mber_sn = userInfo.mber_sn;

        MediNewNetworkModule.doApi(getContext(), new Tr_offer_info_mod(), requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                if (responseData instanceof Tr_offer_info_mod) {
                    Tr_offer_info_mod data = (Tr_offer_info_mod)responseData;
                    try {
                        if ("Y".equals(data.offer_yn)) {
                            if ("Y".equals(userInfo.add_reg_yn)) {
                                NewActivity.startActivity(getActivity(), LoginFirstInfoFragment1_2.class, null);
                            } else {
                                Intent intent = new Intent(getActivity(), MainActivityMedicare.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);
                                getActivity().finish();
                                getActivity().overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
                            }


                        } else {
                            CDialog.showDlg(getContext(), getString(R.string.comm_error_common_9999));
                        }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                        CDialog.showDlg(getContext(), getString(R.string.comm_error_common_9999));
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                Log.e(TAG, response);
            }
        });
    }



    @Override
    public void onResume() {
        Logger.d(this.getClass().getSimpleName(), "onResume()");
        super.onResume();
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (getActivity() instanceof BaseActivityMedicare) {
            ((BaseActivityMedicare) context).setonKeyBackCatchListener(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }


    @Override
    public void onCatch() {

    }
}