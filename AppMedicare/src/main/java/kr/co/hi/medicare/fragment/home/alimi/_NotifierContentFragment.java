package kr.co.hi.medicare.fragment.home.alimi;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.fragment.IBaseFragment;
import kr.co.hi.medicare.net.hwNet.ApiData;

import java.util.Timer;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.ApplinkDialog;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.data.Tr_asstb_kbtg_alimi_view;
import kr.co.hi.medicare.util.DownloadUtil;
import kr.co.hi.medicare.utilhw.IntentUtil;


/**
 * Created by MrsWin on 2017-02-16.
 */

public class _NotifierContentFragment extends BaseFragmentMedi implements IBaseFragment, View.OnClickListener {
    private final String TAG = getClass().getSimpleName();

    private TextView noticonTitle, noticonContent;
    private WebView mNotiWebview;
    private ScrollView mContent_scrollview;
    private ImageButton mRight_pdf_down;

    private String HTML_YN="N";

    private String Idx;
    private String mPdf_url;



    public static BaseFragmentMedi newInstance() {
        _NotifierContentFragment fragment = new _NotifierContentFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notifier_content_fragment, container, false);
//        Toolbar toolbar = (Toolbar) view.findViewById(R.id.tool_bar);
//        ((BaseActivity)getContext()).setSupportActionBar(toolbar);

        noticonTitle = view.findViewById(R.id.noticontent_title);
        noticonContent =  view.findViewById(R.id.noticontent_content);
        mNotiWebview = (WebView) view.findViewById(R.id.noti_webview);
        mContent_scrollview = view.findViewById(R.id.content_scrollview);
        mRight_pdf_down = view.findViewById(R.id.right_pdf_down);

        if(getArguments() != null) {
            Idx = getArguments().getString("IDX");
            NotiDetail(Idx);
        }


        return view;
    }

    //알리미 상세페이지
    public void NotiDetail(String idx) {
        Tr_asstb_kbtg_alimi_view tr = new Tr_asstb_kbtg_alimi_view();
        Tr_asstb_kbtg_alimi_view.RequestData requestData = new Tr_asstb_kbtg_alimi_view.RequestData();
        requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;
        requestData.kbta_idx = idx;
        requestData.pushk = "0";

        getData(getContext(), tr, requestData, true, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_asstb_kbtg_alimi_view) {
                    Tr_asstb_kbtg_alimi_view recv = (Tr_asstb_kbtg_alimi_view) obj;
                    if("Y".equals(recv.data_yn)){

                        if (recv.chlmReadern.size() > 0) {
                            Tr_asstb_kbtg_alimi_view.ChlmReadern data = ((Tr_asstb_kbtg_alimi_view) obj).chlmReadern.get(0);
                            HTML_YN = data.html_yn;
                            mPdf_url = data.kbt_pdf;
                            noticonTitle.setText(data.kbt);
                            if(mPdf_url.equals("")){
                                mRight_pdf_down.setVisibility(View.GONE);
                            }else{
                                mRight_pdf_down.setVisibility(View.VISIBLE);
                            }

                            if(HTML_YN.equals("N")) {
                                mNotiWebview.setVisibility(View.GONE);
                                mContent_scrollview.setVisibility(View.VISIBLE);
                                noticonContent.setText(data.kbc);
                            }
                            else {
                                mNotiWebview.setVisibility(View.VISIBLE);
                                mContent_scrollview.setVisibility(View.GONE);
                                mNotiWebview.setWebViewClient(new WebViewClientClass());


                                WebSettings settings = mNotiWebview.getSettings();
                                // 자바스크립트 허용
                                settings.setJavaScriptEnabled(true);
                                settings.setLoadWithOverviewMode(true);
                                settings.setUseWideViewPort(false);
                                settings.setSupportZoom(true);
                                settings.setBuiltInZoomControls(false);
                                settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
                                settings.setDomStorageEnabled(true);
                                settings.setDefaultTextEncodingName("utf-8");
                                settings.setCacheMode(WebSettings.LOAD_NO_CACHE);

                                mNotiWebview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
                                mNotiWebview.setScrollbarFadingEnabled(true);

                                // 스크롤바 없애기
                                mNotiWebview.setHorizontalScrollBarEnabled(false);
                                mNotiWebview.setVerticalScrollBarEnabled(false);
                                mNotiWebview.setBackgroundColor(0);
                                mNotiWebview.setPadding(0, 0, 0, 0);

                                String source = data.kbc;
                                if (Build.VERSION.SDK_INT >= 24) {
                                    Spanned spannedFromHtml =Html.fromHtml(WebViewEscape(source),Html.FROM_HTML_MODE_LEGACY);
                                    Log.i(TAG,WebViewEscape(source));
                                    mNotiWebview.loadDataWithBaseURL(null,spannedFromHtml.toString(), "text/html", "UTF-8",null);
                                } else {
                                    Spanned spannedFromHtml =Html.fromHtml(WebViewEscape(source));
                                    mNotiWebview.loadDataWithBaseURL(null,spannedFromHtml.toString(), "text/html", "UTF-8",null);
                                }

                            }
                        }

                    }else{
                        Log.i(TAG,"KA002 : 알리미 글 존재 안함.");
                    }



                }
            }
        }, null);
    }

    private String getAppname(String PK){
        String name ="";
        try {
            name = (String) getContext().getPackageManager().getApplicationLabel(getContext().getPackageManager().getApplicationInfo(PK, PackageManager.GET_UNINSTALLED_PACKAGES));
        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
        }
        return name;
    }


    private String WebViewEscape(String badString)
    {
        return badString.replace("&amp;quot;", "\"");

    }

//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
////        getToolBar().setVisibility(View.GONE);
//        actionBar.setcommonbar();
//        actionBar.setActionBarTitle(getString(R.string.main_bottom_menu_04));
//    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        view.findViewById(R.id.share_btn).setContentDescription(getString(R.string.ALAM_SHARED));
        OnClickListener mSaveClickListener = new OnClickListener(this,view, _NotifierContentFragment.newInstance(),getContext());
        view.findViewById(R.id.share_btn).setOnTouchListener(mSaveClickListener);
        view.findViewById(R.id.share_btn).setOnClickListener(this);
        mRight_pdf_down.setOnClickListener(this);


    }

    class OnClickListener implements View.OnTouchListener {
//        private static final String TAG = com.greencross.component.OnClickListener.class.getSimpleName();
        private Fragment mFragment;
        private Timer mTimer;
        private Context mContext;


        public OnClickListener(View.OnClickListener clickListener, View v, Fragment fragment, Context context){
            if (clickListener != null)
                clickListener.onClick(v);

            mFragment = fragment;
            mContext = context;
        }

        public void getTimer(Timer timer){
            mTimer = timer;
        }

        @Override
        public boolean onTouch(View v, MotionEvent Event) {
            if (Event.getAction() == MotionEvent.ACTION_DOWN) {
//            if (mFragment instanceof PMainFragment) {
//                ((PMainFragment) mFragment).timerStop(mTimer);
//            }
                Log.i(TAG,"ACTION_DOWN");
            }  else if (Event.getAction() == MotionEvent.ACTION_MOVE) {
//            if (mFragment instanceof PMainFragment) {
//                ((PMainFragment) mFragment).setAutoSwiper();
//            }
            }else if (Event.getAction() == MotionEvent.ACTION_UP) {
//            if (mFragment instanceof PMainFragment) {
//                ((PMainFragment) mFragment).setAutoSwiper();
//            }

//                if (v.getContentDescription() != null && !v.getContentDescription().toString().equals("")) {
//                    // _!: 카운트만 되는 버튼 끝에 붙임
//                    if (v.getContentDescription().toString().contains("_!")) {
//                        String temp = v.getContentDescription().toString().replace("_!", "");
//                        String cod[] = temp.split("_");
//
//                        DBHelper helper = new DBHelper(mContext);
//                        DBHelperLog logdb = helper.getLogDb();
//
//                        if (cod.length == 1) {
//                            logdb.insert(cod[0], "", "", 0, 1);
//                            Log.i(TAG, "view.contentDescription : " + cod[0] + "count : 1");
//                        } else if (cod.length == 2) {
//                            logdb.insert(cod[0], cod[1], "", 0, 1);
//                            Log.i(TAG, "view.contentDescription : " + cod[0] + cod[1] + "count : 1");
//                        } else {
//                            logdb.insert(cod[0], cod[1], cod[2], 0, 1);
//                            Log.i(TAG, "view.contentDescription : " + cod[0] + cod[1] + cod[2] + "count : 1");
//                        }
//
//                    } else if (v.getContentDescription().toString().contains("_")) {
//                        DisplayTimerUtil.getInstance().setDisplayTimer(mContext, v.getContentDescription().toString());
//                    } else if (v.getContentDescription().toString().contains("!")){
//                        DisplayTimerUtil.getInstance().setDisplayTimer(mContext,v.getContentDescription().toString());
//                    }
//                    Log.i(TAG,"ACTION_UP");
//                }
            }
            return false;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.share_btn:
                //공유
                String imgUrl = "https://wkd.walkie.co.kr/KBT/Contents/share/share_alimi.png";

                View view = LayoutInflater.from(getContext()).inflate(R.layout.applink_dialog_layout, null);
                ApplinkDialog dlg = ApplinkDialog.showDlg(getContext(), view);
                dlg.setSharing(imgUrl, "notice", Idx, "","[메디케어"+getAppname(getContext().getPackageName())+"]",noticonTitle.getText().toString(),getString(R.string.Linkbtn1),"",false,"share_alimi.png","","http://wkd.walkie.co.kr/KBT/kbon.html?pakagename=com.greencare.kyobototalcare&tabletname=com.greencare.kyobototaltablets&urlschema=kyobototalcare&service=notice&param1="+Idx+"&param2=");

                break;
            case R.id.right_pdf_down:
                CDialog.showDlg(getContext(), getString(R.string.pdf_down_text1), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = mPdf_url;
                        new DownloadUtil().startDownload(getActivity(), url, "Noti_"+Idx+".pdf","Noti_"+Idx, new DownloadUtil.IDownloadReceiver() {
                            @Override
                            public void success(String filePath) {
                                IntentUtil.sharePdfFile(getActivity(), filePath);
                            }

                            @Override
                            public void fail() {
                                Toast.makeText(getActivity(), "다운로드 취소 : ", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                break;
        }
    }

    private class WebViewClientClass extends WebViewClient {
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            return true;
//        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.i(TAG, "onPageFinished.hideprogress");
            hideProgress();
            // progress 가 해지 안 될 경우 처리
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (getActivity() != null)
                        hideProgress();
                }
            }, 1 * 500);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.i(TAG, "onPageStarted.showProgress");
            showProgress();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            Log.i(TAG, "onReceivedError.hideprogress");
            hideProgress();
            // progress 가 해지 안 될 경우 처리
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (getActivity() != null)
                        hideProgress();
                }
            }, 1 * 500);
        }

        @Override
        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            super.onReceivedHttpError(view, request, errorResponse);
            Log.i(TAG, "onReceivedHttpError.hideprogress");
            hideProgress();
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            Log.i(TAG, "onReceivedSslError.hideprogress");
            hideProgress();
        }
    }

    @Override
    public void onBackPressed() {
        if (mNotiWebview.canGoBack()) {
            mNotiWebview.goBack();
        } else {
            super.onBackPressed();
        }

    }
}