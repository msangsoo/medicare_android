package kr.co.hi.medicare.fragment.health.message;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.hwdata.Tr_infra_message_write;
import kr.co.hi.medicare.utilhw.SharedPref;

public class DlgHealthMessaeView extends LinearLayout {
    private TextView mTitleView, mHealth_message;

    private String mInfraTy;
    private RelativeLayout mDietBtn;
    private String DangerType; // 1:체중, 2: 혈압, 3: 혈당
    private TextView healthname;

    public DlgHealthMessaeView(Context context, int tabPosition) {
        super(context);

        mInfraTy = tabPosition == 1 ? Tr_infra_message_write.INFRA_TY_SUGAR : Tr_infra_message_write.INFRA_TY_HEALTH;
    }

    public DlgHealthMessaeView(Context context, int type, String weight) {
        super(context);
        DangerType = weight;

        if(type == 1){
            mInfraTy =  Tr_infra_message_write.INFRA_TY_SUGAR;
        } else if(type == 2){
            mInfraTy =  Tr_infra_message_write.INFRA_TY_HEALTH;
        } else if(type == 3){
            mInfraTy =  Tr_infra_message_write.INFRA_TY_PRESSURE;
        }
    }

    public DlgHealthMessaeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DlgHealthMessaeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private BaseFragmentMedi mBaseFragment;
    

    public void init(BaseFragmentMedi baseFragment) {
        mBaseFragment = baseFragment;
        View view = inflate(getContext(), R.layout.dlg_health_message_layout, null);
        mTitleView = view.findViewById(R.id.dialog_title);
        mHealth_message = view.findViewById(R.id.health_message);
        mDietBtn = view.findViewById(R.id.btn1);
        healthname = view.findViewById(R.id.healthname);

        if(DangerType.equals("1")){
            //TODO xxx 다이어트 프로그램 임시 숨기기
            if(BuildConfig.DEBUG)
                mDietBtn.setVisibility(VISIBLE);
            else
                mDietBtn.setVisibility(GONE);

            healthname.setText("체중 관리");
        } else if(DangerType.equals("2")){
            mDietBtn.setVisibility(GONE);
            healthname.setText("혈압 관리");
        } else{
            mDietBtn.setVisibility(GONE);
            healthname.setText("혈당 관리");
        }

        setTitle("건강메시지");
        addView(view);


        setHealthMessageSqlite();
    }


    public void setTitle(String title) {
        mTitleView.setText(title);
    }


    /**
     * 건강 메시지 세팅하기
     */
    public void setHealthMessageSqlite() {
//        DBHelper helper = new DBHelper(getContext());
//        DBHelperMessage db = helper.getMessageDb();
//        List<MessageModel> messageList = db.getResultOne(helper, mInfraTy);

//        if(messageList.size() > 0){
//            mHealth_message.setText(messageList.get(0).getMessage());
//        }

         mHealth_message.setText(SharedPref.getInstance().getPreferences(SharedPref.HEALTH_DANGER_MESSAGE));



    }
}


