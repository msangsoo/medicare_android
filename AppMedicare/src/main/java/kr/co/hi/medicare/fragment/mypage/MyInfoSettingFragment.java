package kr.co.hi.medicare.fragment.mypage;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.utilhw.StringUtil;

import kr.co.hi.medicare.net.hwNet.BaseData;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.CDatePicker;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.community.data.ReceiveDataCode;
import kr.co.hi.medicare.fragment.community.dialog.DialogCommon;
import kr.co.hi.medicare.fragment.community.dialog.DialogMeberInfoPicker;
import kr.co.hi.medicare.fragment.login.LoginFirstInfoFragment2_2;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_mber_modify_mberinfo;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.TextWatcherFloatUtil;

import static android.app.Activity.RESULT_OK;

/**
 * Created by MrsWin on 2017-02-16.
 */

public class MyInfoSettingFragment extends BaseFragmentMedi {
    private final String TAG = MyInfoSettingFragment.class.getSimpleName();
    public static final int REQUEST_CODE =5353;
    public static final String INFO_DATA = "INFO_DATA";
    private DialogMeberInfoPicker dialogMeberInfoPicker=null;
    private DialogCommon dialogCommon;
    private TextView name,sex,birth,height,weight,activity,job,disease,smoking, joindate, transform, title_name,title_sex,title_birth,title_phone,phone,explain;
    private RelativeLayout layout_transform;
    private LinearLayout layout_name, layout_sex, layout_birth, layout_info_add,layout_phone;
    private ImageView arrow001, arrow002, arrow003, arrow007;
    private MyPageData myPageData=new MyPageData();
    private MyPageData myPageDataModify=new MyPageData();
    private boolean isLogin=true;

    public static Fragment newInstance() {
        MyInfoSettingFragment fragment = new MyInfoSettingFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_member_info, container, false);

        Bundle bundle = getArguments();
        if(bundle!=null){
            myPageData = (MyPageData)bundle.getSerializable(INFO_DATA);
            myPageDataModify = (MyPageData)bundle.getSerializable(INFO_DATA);
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        name = view.findViewById(R.id.name);
        sex = view.findViewById(R.id.sex);

        transform = view.findViewById(R.id.transform);
        birth = view.findViewById(R.id.birth);
        phone = view.findViewById(R.id.phone);
        height = view.findViewById(R.id.height);
        weight = view.findViewById(R.id.weight);
        activity = view.findViewById(R.id.activity);
        job = view.findViewById(R.id.job);
        disease = view.findViewById(R.id.disease);
        smoking = view.findViewById(R.id.smoking);
        joindate = view.findViewById(R.id.joindate);
        explain = view.findViewById(R.id.explain);
        layout_transform = view.findViewById(R.id.layout_transform);
        layout_name = view.findViewById(R.id.layout_name);
        layout_sex = view.findViewById(R.id.layout_sex);
        layout_birth = view.findViewById(R.id.layout_birth);
        layout_info_add = view.findViewById(R.id.layout_info_add);

        layout_phone = view.findViewById(R.id.layout_phone);

        arrow001 = view.findViewById(R.id.arrow001);
        arrow002 = view.findViewById(R.id.arrow002);
        arrow003 = view.findViewById(R.id.arrow003);

        arrow007 = view.findViewById(R.id.arrow007);

        title_name = view.findViewById(R.id.title_name);
        title_sex = view.findViewById(R.id.title_sex);
        title_birth = view.findViewById(R.id.title_birth);

        title_phone = view.findViewById(R.id.title_phone);


        name.setOnClickListener(mClickListener);
        sex.setOnClickListener(mClickListener);
        birth.setOnClickListener(mClickListener);
        height.setOnClickListener(mClickListener);
        weight.setOnClickListener(mClickListener);
        phone.setOnClickListener(mClickListener);
        layout_transform.setOnClickListener(mClickListener);
        layout_info_add.setOnClickListener(mClickListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, getContext());


        //마이페이지
        name.setOnTouchListener(ClickListener);
        sex.setOnTouchListener(ClickListener);
        birth.setOnTouchListener(ClickListener);
        height.setOnTouchListener(ClickListener);
        weight.setOnTouchListener(ClickListener);
        phone.setOnTouchListener(ClickListener);
        layout_info_add.setOnTouchListener(ClickListener);
        layout_transform.setOnTouchListener(ClickListener);


        //코드부여
        name.setContentDescription(getString(R.string.commoninput));
        sex.setContentDescription(getString(R.string.commoninput));
        phone.setContentDescription(getString(R.string.commoninput));
        birth.setContentDescription(getString(R.string.commoninput));
        height.setContentDescription(getString(R.string.commoninput));
        weight.setContentDescription(getString(R.string.commoninput));
        layout_info_add.setContentDescription(getString(R.string.layout_info_add));
        layout_transform.setContentDescription(getString(R.string.layout_transform));



        setMessageColor(transform,getResources().getString(R.string.info_change_transform), getString(R.string.info_change_transform).indexOf("전") , getString(R.string.info_change_transform).length());
        setView();
        setMbGrad();
    }

    private void setView() {
        name.setText("");
        name.append(myPageData.MBER_NM);
        phone.setText(setPhoneNumber(myPageData.mber_hp));
        sex.setText(myPageData.MBER_SEX.equals("1") ? getString(R.string.info_change_sex_man) : getString(R.string.info_change_sex_woman));
        birth.setText(getBirth(myPageData.MBER_LIFYEA));
        height.setText(myPageData.mber_height+"cm");
        weight.setText(myPageData.mber_bdwgh+"kg");
        activity.setText(getActivity(myPageData.actqy));
        job.setText(myPageData.job_yn.equals("Y") ? getResources().getString(R.string.info_change_job_have) : getResources().getString(R.string.info_change_job_nohave));
        disease.setText(CommonFunction.getDiseaseNM(myPageData.DISEASE_NM, myPageData.DISEASE_TXT));
        smoking.setText(myPageData.smoking_yn.equals("Y") ? getResources().getString(R.string.info_change_smoking_have) : getResources().getString(R.string.info_change_smoking_nohave));
        joindate.setText("가입일 "+getDateFormat(myPageData.MBER_DE));
    }


    private void setUpdateView() {
        name.setText("");
        name.append(myPageDataModify.MBER_NM);
        phone.setText(setPhoneNumber(myPageDataModify.mber_hp));
        sex.setText(myPageDataModify.MBER_SEX.equals("1") ? getString(R.string.info_change_sex_man) : getString(R.string.info_change_sex_woman));
        birth.setText(getBirth(myPageDataModify.MBER_LIFYEA));
        height.setText(myPageDataModify.mber_height+"cm");
        weight.setText(myPageDataModify.mber_bdwgh+"kg");
        activity.setText(getActivity(myPageDataModify.actqy));
        job.setText(myPageDataModify.job_yn.equals("Y") ? getResources().getString(R.string.info_change_job_have) : getResources().getString(R.string.info_change_job_nohave));
        disease.setText(CommonFunction.getDiseaseNM(myPageDataModify.DISEASE_NM,myPageDataModify.DISEASE_TXT));
        smoking.setText(myPageDataModify.smoking_yn.equals("Y") ? getResources().getString(R.string.info_change_smoking_have) : getResources().getString(R.string.info_change_smoking_nohave));
    }


    private String setPhoneNumber(String phoneNumber){
        String phone = phoneNumber;

        try{
            phone = PhoneNumberUtils.formatNumber(phone);
        }catch (Exception e){
            e.printStackTrace();
        }

        return phone;
    }


    private void setMbGrad(){

        //정회원일경우
        if(UserInfo.getLoginInfo().mber_grad.equals("10")){
            layout_transform.setVisibility(View.GONE);
            explain.setVisibility(View.VISIBLE);

            title_name.setTextColor(getResources().getColor(R.color.x143_144_158));
            title_sex.setTextColor(getResources().getColor(R.color.x143_144_158));
            title_birth.setTextColor(getResources().getColor(R.color.x143_144_158));
            title_phone.setTextColor(getResources().getColor(R.color.x143_144_158));
            layout_name.setBackgroundColor(getResources().getColor(R.color.x_246_246_246));
            layout_sex.setBackgroundColor(getResources().getColor(R.color.x_246_246_246));
            layout_birth.setBackgroundColor(getResources().getColor(R.color.x_246_246_246));
            layout_phone.setBackgroundColor(getResources().getColor(R.color.x_246_246_246));

            name.setEnabled(false);
            sex.setEnabled(false);
            birth.setEnabled(false);
            phone.setEnabled(false);


            arrow001.setVisibility(View.INVISIBLE);
            arrow002.setVisibility(View.INVISIBLE);
            arrow003.setVisibility(View.INVISIBLE);
            arrow007.setVisibility(View.INVISIBLE);

        }else{
            //비회원일경우
            layout_transform.setVisibility(View.VISIBLE);
            explain.setVisibility(View.GONE);
            title_name.setTextColor(getResources().getColor(R.color.x12_13_44));
            title_sex.setTextColor(getResources().getColor(R.color.x12_13_44));
            title_birth.setTextColor(getResources().getColor(R.color.x12_13_44));
            title_phone.setTextColor(getResources().getColor(R.color.x12_13_44));


            layout_name.setBackgroundColor(Color.WHITE);
            layout_sex.setBackgroundColor(Color.WHITE);
            layout_birth.setBackgroundColor(Color.WHITE);
            layout_phone.setBackgroundColor(Color.WHITE);

            name.setEnabled(true);
            sex.setEnabled(true);
            birth.setEnabled(true);
            phone.setEnabled(true);

            arrow001.setVisibility(View.VISIBLE);
            arrow002.setVisibility(View.VISIBLE);
            arrow003.setVisibility(View.VISIBLE);
            arrow007.setVisibility(View.VISIBLE);
        }


    }


    private String getActivity(String actqy){
        String value=getResources().getString(R.string.join_step3_active_type1);

        switch (actqy){
            case "1":
                value=getResources().getString(R.string.join_step3_active_type1);
                break;
            case "2":
                value=getResources().getString(R.string.join_step3_active_type2);
                break;

            case "3":
                value=getResources().getString(R.string.join_step3_active_type3);
                break;
        }

        return value;
    }


    private String getBirth(String birth){

        String date=birth;

        if(birth.length()==8){
            date="";
            int year = Integer.parseInt(birth.substring(0,4));
            int month = Integer.parseInt(birth.substring(4,6));
            int day = Integer.parseInt(birth.substring(6,8));

            date = String.format("%s.%02d.%02d", year, month, day);
        }

        return date;
    }


    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Bundle bundle = new Bundle();
            switch (v.getId()){
                case R.id.phone:
                    final View view4 = LayoutInflater.from(getContext()).inflate(R.layout.dialog_modify_info,null);
                    ((TextView)view4.findViewById(R.id.title_info)).setText(getString(R.string.mobilenum));
                    ((EditText)view4.findViewById(R.id.info)).setHint(getString(R.string.msg_empty_phone_msg));
                    ((EditText)view4.findViewById(R.id.info)).setFilters(new InputFilter[] { new InputFilter.LengthFilter(11) });
                    ((EditText)view4.findViewById(R.id.info)).setInputType(EditorInfo.TYPE_CLASS_NUMBER);

                    final CDialog dlg4 = CDialog.showDlg(getContext(), view4,true);
                    view4.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            disableKeyboard(((EditText)view4.findViewById(R.id.info)));
                            dlg4.dismiss();
                        }
                    });
                    view4.findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String value = ((EditText)view4.findViewById(R.id.info)).getText().toString();

                            if(value!=null&&!value.equals("")){
                                disableKeyboard(((EditText)view4.findViewById(R.id.info)));
                                dlg4.dismiss();
                                myPageDataModify.mber_hp =  value;
                                doUpdateInfo();
                            }
                        }
                    });
                    

                    break;
                case R.id.name:


                    final View view1 = LayoutInflater.from(getContext()).inflate(R.layout.dialog_modify_info,null);
                    ((TextView)view1.findViewById(R.id.title_info)).setText(getString(R.string.text_name));
                    ((EditText)view1.findViewById(R.id.info)).setInputType(EditorInfo.TYPE_CLASS_TEXT);
                    ((EditText)view1.findViewById(R.id.info)).setFilters(new InputFilter[] { new InputFilter.LengthFilter(15) });
                    final CDialog dlg1 = CDialog.showDlg(getContext(), view1,true);
                    view1.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            disableKeyboard(((EditText)view1.findViewById(R.id.info)));
                            dlg1.dismiss();
                        }
                    });
                    view1.findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String value = ((EditText)view1.findViewById(R.id.info)).getText().toString();
                            if(value!=null&&!value.equals("")){
                                disableKeyboard(((EditText)view1.findViewById(R.id.info)));
                                dlg1.dismiss();
                                myPageDataModify.MBER_NM =  value;
                                doUpdateInfo();
                            }
                        }
                    });
                    break;


                case R.id.layout_info_add:
                    bundle.putString(LoginFirstInfoFragment2_2.BUNDLE_KEY_HEIGHT, myPageDataModify.mber_height);
                    bundle.putString(LoginFirstInfoFragment2_2.BUNDLE_KEY_WEIGHT, myPageDataModify.mber_bdwgh);
                    bundle.putString(LoginFirstInfoFragment2_2.BUNDLE_KEY_WEIGHT_GOAL, myPageDataModify.mber_bdwgh_goal);
                    bundle.putSerializable(MyInfoSettingAddFragment.MYPAGE_DATA,myPageData);
                    NewActivity.startActivityForResult(MyInfoSettingFragment.this, MyInfoSettingAddFragment.REQUEST_CODE,MyInfoSettingAddFragment.class,bundle);
                    break;
                //정회원 전환 수정 필요
                case R.id.layout_transform:
                    bundle.putString(CommonToolBar.TOOL_BAR_TITLE, getString(R.string.myinfo_change));
        //            NewActivity.startActivityForResult(MyInfoSettingFragment.this, REQ_EDIT_INFO, LoginFirstInfoFragment1_2.class, bundle);
        //            NewActivity.startActivityForResult(MyInfoSettingFragment.this, REQ_EDIT_INFO, PremiumMainFragment.class, bundle);

                    View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_member_change,null);
                    final CDialog dlg = CDialog.showDlg(getContext(), view,true);
                    dlg.setTitle("");

                    view.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dlg.dismiss();
                        }
                    });
                    view.findViewById(R.id.consulting_dlg_apply).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dlg.dismiss();
                            SharedPref.getInstance().savePreferences(SharedPref.IS_DRUG_REQUEST, true);
                            String tel = "tel:0215885656";
                            startActivity(new Intent("android.intent.action.DIAL", Uri.parse(tel)));
                        }
                    });
                    view.findViewById(R.id.member_dlg_apply).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dlg.dismiss();
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(MyInfoMemberChangeFragment.MYPAGE_DATA,myPageData);
                            NewActivity.startActivityForResult(MyInfoSettingFragment.this, MyInfoMemberChangeFragment.REQUEST_CODE,MyInfoMemberChangeFragment.class,bundle);
                        }
                    });



                    return;


                case R.id.sex:
                //성별 변경
                    dialogMeberInfoPicker = DialogMeberInfoPicker.showDialog(getContext(),myPageDataModify.MBER_SEX.equals("1") ? 0 : 1 , DialogMeberInfoPicker.TYPE_SEX);

                    dialogMeberInfoPicker.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if(dialogMeberInfoPicker.getIsConfirm()) {
                                int value = dialogMeberInfoPicker.getPicker().getValue();
                                dialogMeberInfoPicker.setIsConfirm(false);
                                myPageDataModify.MBER_SEX =  (value==0 ? "1" : "2");
                                doUpdateInfo();

                            }
                        }
                    });

                    return;

                case R.id.birth:
                    showDatePicker(v);

                    return;

                case R.id.height:
                    final View view3 = LayoutInflater.from(getContext()).inflate(R.layout.dialog_modify_info,null);
                    ((TextView)view3.findViewById(R.id.title_info)).setText(getString(R.string.heigh));
                    new TextWatcherFloatUtil().setTextWatcher(((EditText)view3.findViewById(R.id.info)), 230, 1);

                    final CDialog dlg3 = CDialog.showDlg(getContext(), view3,true);
                    view3.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            disableKeyboard(((EditText)view3.findViewById(R.id.info)));
                            dlg3.dismiss();
                        }
                    });
                    view3.findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String value = ((EditText)view3.findViewById(R.id.info)).getText().toString();
                            if(value.endsWith(".")) //마지막문자가 점인경우 점 제거
                                value=value.replaceAll("\\.","");

                            if(value!=null&&!value.equals("")){
                                disableKeyboard(((EditText)view3.findViewById(R.id.info)));
                                dlg3.dismiss();
                                myPageDataModify.mber_height =  value;
                                doUpdateInfo();
                            }
                        }
                    });

//                    int defaultHeight=170;
//                    try{
//                        defaultHeight = Integer.parseInt(myPageDataModify.mber_height);
//                        if(defaultHeight<100){
//                            defaultHeight=170;
//                        }
//                    }catch (Exception e){
//                        defaultHeight=170;
//                    }
//
//                    dialogMeberInfoPicker = DialogMeberInfoPicker.showDialog(getContext(),defaultHeight, DialogMeberInfoPicker.TYPE_HEIGHT);
//
//                    dialogMeberInfoPicker.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                        @Override
//                        public void onDismiss(DialogInterface dialog) {
//                            if(dialogMeberInfoPicker.getIsConfirm()) {
//                                int value = dialogMeberInfoPicker.getPicker().getValue();
//                                dialogMeberInfoPicker.setIsConfirm(false);
//                                myPageDataModify.mber_height =  value+"";
//                                doUpdateInfo();
//                            }
//                        }
//                    });
                    return;

                case R.id.weight:
//                    int defaultWeight=50;
//                    try{
//                        defaultWeight = Integer.parseInt(myPageDataModify.mber_bdwgh);
//                        if(defaultWeight<20){
//                            defaultWeight=50;
//                        }
//                    }catch (Exception e){
//                        defaultWeight=50;
//                    }
//
//                    dialogMeberInfoPicker = DialogMeberInfoPicker.showDialog(getContext(),defaultWeight, DialogMeberInfoPicker.TYPE_WEIGHT);
//
//                    dialogMeberInfoPicker.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                        @Override
//                        public void onDismiss(DialogInterface dialog) {
//                            if(dialogMeberInfoPicker.getIsConfirm()) {
//                                int value = dialogMeberInfoPicker.getPicker().getValue();
//                                dialogMeberInfoPicker.setIsConfirm(false);
//                                myPageDataModify.mber_bdwgh =  value+"";
//                                doUpdateInfo();
//                            }
//                        }
//                    });


                    final View view2 = LayoutInflater.from(getContext()).inflate(R.layout.dialog_modify_info,null);
                    ((TextView)view2.findViewById(R.id.title_info)).setText(getString(R.string.weight));
                    new TextWatcherFloatUtil().setTextWatcher(((EditText)view2.findViewById(R.id.info)), 300, 2);

                    final CDialog dlg2 = CDialog.showDlg(getContext(), view2,true);
                    view2.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            disableKeyboard(((EditText)view2.findViewById(R.id.info)));
                            dlg2.dismiss();
                        }
                    });
                    view2.findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String value = ((EditText)view2.findViewById(R.id.info)).getText().toString();
                            if(value.endsWith(".")) //마지막문자가 점인경우 점 제거
                                value=value.replaceAll("\\.","");

                            if(!TextUtils.isEmpty(value)){
                                if(StringUtil.getFloat(value) > 300.0f || StringUtil.getFloat(value) < 20.0f){
                                    CDialog.showDlg(getContext(), getString(R.string.weight_range));
                                    return;
                                }
                            }


                            if(value!=null&&!value.equals("")){
                                disableKeyboard(((EditText)view2.findViewById(R.id.info)));
                                dlg2.dismiss();
                                myPageDataModify.mber_bdwgh =  value;
                                doUpdateInfo();
                            }
                        }
                    });
                    return;
            }

        }
    };

    private void disableKeyboard(EditText editText) {

        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            if (requestCode == MyInfoSettingAddFragment.REQUEST_CODE) {
                MyPageData newData = (MyPageData)data.getSerializableExtra(MyInfoSettingAddFragment.MYPAGE_DATA);
                myPageData.smoking_yn = newData.smoking_yn;
                myPageData.DISEASE_NM = newData.DISEASE_NM;
                myPageData.DISEASE_TXT = newData.DISEASE_TXT;
                myPageData.actqy = newData.actqy;
                myPageData.job_yn = newData.job_yn;
                myPageDataModify.smoking_yn = newData.smoking_yn;
                myPageDataModify.DISEASE_NM = newData.DISEASE_NM;
                myPageDataModify.DISEASE_TXT = newData.DISEASE_TXT;
                myPageDataModify.actqy = newData.actqy;
                myPageDataModify.job_yn = newData.job_yn;
                setUpdateView();
            }
            if(requestCode == MyInfoMemberChangeFragment.REQUEST_CODE){

                if(!data.getBooleanExtra(MyInfoMemberChangeFragment.MYPAGE_ISLOGIN,true)){
                    isLogin=false;
                    return;
                }


                MyPageData newData = (MyPageData)data.getSerializableExtra(MyInfoMemberChangeFragment.MYPAGE_DATA);
                myPageData.mber_grad = newData.mber_grad;
                myPageData.MBER_NM = newData.MBER_NM;
                myPageData.mber_hp = newData.mber_hp;
                myPageData.MBER_SEX = newData.MBER_SEX;
                myPageData.MBER_LIFYEA = newData.MBER_LIFYEA;

                myPageDataModify.mber_grad = newData.mber_grad;
                myPageDataModify.MBER_NM = newData.MBER_NM;
                myPageDataModify.mber_hp = newData.mber_hp;
                myPageDataModify.MBER_SEX = newData.MBER_SEX;
                myPageDataModify.MBER_LIFYEA = newData.MBER_LIFYEA;

                setUpdateView();
                setMbGrad();
            }

        }
    }

    private void setMessageColor(TextView view,String message, int start, int end){
        SpannableStringBuilder sp = new SpannableStringBuilder(message);
        sp.setSpan( new ForegroundColorSpan(getResources().getColor(R.color.x12_13_44)),start,end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        view.setText(sp);
    }


    private void showDatePicker(View v) {
        GregorianCalendar calendar = new GregorianCalendar();
        String birth = ((TextView)v).getText().toString();
        String[] birthSpl = birth.split("\\.");

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        if (birthSpl.length == 3) {
            year = Integer.parseInt("".equals(birthSpl[0]) ? "0" : birthSpl[0].trim());
            month = Integer.parseInt("".equals(birthSpl[1]) ? "0" : birthSpl[1].trim()) - 1;
            day = Integer.parseInt("".equals(birthSpl[2]) ? "0" : birthSpl[2].trim());
        }

        new CDatePicker(getContext(), dateSetListener, year, month, day).show();
    }
    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String msg = String.format("%04d.%02d.%02d", year, monthOfYear + 1, dayOfMonth);

            myPageDataModify.MBER_LIFYEA = msg.replaceAll("\\.","");
            doUpdateInfo();
        }

    };





    public void doUpdateInfo(){
        final Tr_mber_modify_mberinfo.RequestData requestData = new Tr_mber_modify_mberinfo.RequestData();
        boolean isShowProgress=true;

        requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;

        requestData.mber_name = myPageDataModify.MBER_NM;
        requestData.mber_lifyea = myPageDataModify.MBER_LIFYEA;
        requestData.mberhp = myPageDataModify.mber_hp;
        requestData.mber_sex = myPageDataModify.MBER_SEX;
        requestData.mber_height = myPageDataModify.mber_height;
        requestData.mber_bdwgh = myPageDataModify.mber_bdwgh;


        requestData.medicine_yn = UserInfo.getLoginInfo().medicine_yn;
        requestData.actqy = myPageDataModify.actqy;
        requestData.disease_nm = myPageDataModify.DISEASE_NM;
        requestData.disease_txt = myPageDataModify.DISEASE_TXT;
        requestData.smkng_yn = myPageDataModify.smoking_yn;
        requestData.mber_job = myPageDataModify.job_yn;


        MediNewNetworkModule.doApi(getContext(), new Tr_mber_modify_mberinfo(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_mber_modify_mberinfo) {
                    Tr_mber_modify_mberinfo data = (Tr_mber_modify_mberinfo)responseData;
                    try {
                        switch (data.result_code){
                            case ReceiveDataCode.MBER_LOAD_MYPAGE_SUCCESS:
                                myPageData.MBER_NM = myPageDataModify.MBER_NM;
                                myPageData.MBER_LIFYEA = myPageDataModify.MBER_LIFYEA;
                                myPageData.MBER_SEX = myPageDataModify.MBER_SEX;
                                myPageData.mber_hp = myPageDataModify.mber_hp;
                                myPageData.mber_height = myPageDataModify.mber_height;
                                myPageData.mber_bdwgh = myPageDataModify.mber_bdwgh;
                                setUpdateView();
                                CommonFunction.setUpdateLoginInfo(getContext());


                                break;
                            case ReceiveDataCode.MBER_LOAD_MYPAGE_ERROR_FAIL:
                                errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:1";
                                break;

                            default:
                                errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:2";
                                break;
                        }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                        errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:3";
                    }
                }

                if(!errorMessage.equals("")){
                    myPageDataModify.MBER_NM = myPageData.MBER_NM;
                    myPageDataModify.MBER_LIFYEA = myPageData.MBER_LIFYEA;
                    myPageDataModify.MBER_SEX = myPageData.MBER_SEX;
                    myPageDataModify.mber_hp = myPageData.mber_hp;
                    myPageDataModify.mber_height = myPageData.mber_height;
                    myPageDataModify.mber_bdwgh = myPageData.mber_bdwgh;
                    setView();

                    CDialog.showDlg(getContext(), errorMessage);
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                Log.e(TAG, response);

                myPageDataModify.MBER_NM = myPageData.MBER_NM;
                myPageDataModify.MBER_LIFYEA = myPageData.MBER_LIFYEA;
                myPageDataModify.MBER_SEX = myPageData.MBER_SEX;
                myPageDataModify.mber_hp = myPageData.mber_hp;
                myPageDataModify.mber_height = myPageData.mber_height;
                myPageDataModify.mber_bdwgh = myPageData.mber_bdwgh;
                setView();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();

        if(!isLogin){
            getActivity().setResult(RESULT_OK, null);
            getActivity().finish();
        }

    }

    private String getDateFormat(String date){
        String value ="";
        Date date_ori=null;
        try {
            date_ori = new SimpleDateFormat("yyyyMMddHHmm").parse(date);
            value = new SimpleDateFormat("yyyy년 MM월 dd일").format(date_ori);
        }catch (Exception e){
            value="";
        }

        return value;
    }

}
