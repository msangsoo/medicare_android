package kr.co.hi.medicare.charting.interfaces.dataprovider;

import kr.co.hi.medicare.charting.data.ScatterData;

public interface ScatterDataProvider extends BarLineScatterCandleBubbleDataProvider {

    ScatterData getScatterData();
}
