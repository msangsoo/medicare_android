package kr.co.hi.medicare.fragment.mypage;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.utilhw.StringUtil;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.data.ReceiveDataCode;
import kr.co.hi.medicare.fragment.community.dialog.DialogCommon;
import kr.co.hi.medicare.fragment.login.LoginFirstInfoFragment2_2;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_mber_modify_mberinfo;
import kr.co.hi.medicare.util.CLog;

import static android.app.Activity.RESULT_OK;


public class MyInfoSettingAddFragment extends LoginFirstInfoFragment2_2 {
    private final String TAG = MyInfoSettingAddFragment.class.getSimpleName();
    public static final int REQUEST_CODE =3434;
    public static final String MYPAGE_DATA ="MYPAGE_DATA";
    private MyPageData myPageData = new MyPageData();
    private DialogCommon dialog;

    public static BaseFragmentMedi newInstance() {
        MyInfoSettingAddFragment fragment = new MyInfoSettingAddFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((Button)view.findViewById(R.id.complete_btn)).setText(getResources().getString(R.string.comm_confirm));
        ((CommonToolBar)view.findViewById(R.id.toolbar)).setTitle(getResources().getString(R.string.info_change_addinfo_title));
        setView();
    }


    private void setView(){

        Bundle bundleAdd = getArguments();

        if(bundleAdd!=null){
            myPageData = (MyPageData)bundleAdd.getSerializable(MYPAGE_DATA);
            requestData.mber_actqy = myPageData.actqy;
            requestData.job_yn =  myPageData.job_yn;
            requestData.smkng_yn = myPageData.smoking_yn;
            requestData.disease_nm = myPageData.DISEASE_NM;
            requestData.disease_txt = myPageData.DISEASE_TXT;
            View view =null;

            switch (myPageData.actqy){
                case "1":
                    view = mActiveLayoutArr[0];
                    break;
                case "2":
                    view = mActiveLayoutArr[1];
                    break;
                default:
                    view = mActiveLayoutArr[2];
                    break;
            }
            setActivity(view);


            switch (myPageData.job_yn){
                case "Y":
                    setJob(R.id.join_step2_2job_yes);
                    break;
                default:
                    setJob(R.id.join_step2_2job_no);
                    break;
            }


            switch (myPageData.smoking_yn){
                case "Y":
                    setSmoking(R.id.join_step2_2smoking_yes_radio_button);
                    break;
                default:
                    setSmoking(R.id.join_step2_2smoking_no_radio_button);
                    break;
            }

            switch (myPageData.DISEASE_NM){
                case "1":
                    view = mDeseaseCheckboxs[0];
                    break;
                case "2":
                    view = mDeseaseCheckboxs[1];
                    break;
                case "3":
                    view = mDeseaseCheckboxs[2];
                    break;
                case "4":
                    view = mDeseaseCheckboxs[3];
                    break;
                case "5":
                    view = mDeseaseCheckboxs[4];
                    break;
                default://기타
                    view = mDeseaseCheckboxs[5];
                    break;
            }

            setDisease(view);
        }

    }



    private void setActivity(View v){

        if (mActiveLayout == v) {
            mActiveLayout.setBackgroundColor(Color.TRANSPARENT);
            mActiveLayout = null;
        } else {
            for (LinearLayout ll : mActiveLayoutArr) {
                ll.setBackgroundColor(Color.TRANSPARENT);
            }
            mActiveLayout = v;
            mActiveLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.x105_129_236));
        }
        String activeTab = mActiveLayout.getTag().toString();
        mActiveIdx = StringUtil.getIntVal(activeTab)-1;
        for (int i = 0; i < mActiveTitleArr.length; i++) {
            if (mActiveIdx == i) {
                mActiveTitleArr[i].setTextColor(ContextCompat.getColor(getContext(), R.color.colorWhite));
                mActiveContentArr[i].setTextColor(ContextCompat.getColor(getContext(), R.color.colorWhite));
            } else {
                mActiveTitleArr[i].setTextColor(ContextCompat.getColor(getContext(), R.color.x12_13_44));
                mActiveContentArr[i].setTextColor(ContextCompat.getColor(getContext(), R.color.x143_144_158));
            }
        }

    }

    private void setJob(int viewId){
        mJobRadioGroup.check(viewId);
    }

    private void setDisease(View v){

        for (CheckBox checkBox : mDeseaseCheckboxs) {
            if (v != checkBox) {
                checkBox.setChecked(false);
            }
            else{
                if(!checkBox.isChecked())
                    checkBox.setChecked(true);
            }
        }
        if(v == mDeseaseCheckboxs[5] && mDeseaseCheckboxs[5].isChecked()){
            mDeseaseEditText.setVisibility(View.VISIBLE);
            mDeseaseEditText.setText("");
            mDeseaseEditText.append(CommonFunction.getDiseaseNM(myPageData.DISEASE_NM,myPageData.DISEASE_TXT));
        }else{
            mDeseaseEditText.setVisibility(View.GONE);
        }
    }

    private void setSmoking(int viewId){
        mSmokeRadioGroup.check(viewId);
    }


    @Override
    public void doUpdateInfo(){
        final Tr_mber_modify_mberinfo.RequestData requestData = new Tr_mber_modify_mberinfo.RequestData();
        boolean isShowProgress=true;

        requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;

        requestData.mber_name = myPageData.MBER_NM;
        requestData.mber_lifyea = myPageData.MBER_LIFYEA;
        requestData.mberhp = UserInfo.getLoginInfo().mber_hp;
        requestData.mber_sex = myPageData.MBER_SEX;
        requestData.mber_height = myPageData.mber_height;
        requestData.mber_bdwgh = myPageData.mber_bdwgh;
        requestData.medicine_yn = UserInfo.getLoginInfo().medicine_yn;

        requestData.actqy = this.requestData.mber_actqy;
        requestData.disease_nm = this.requestData.disease_nm;
        requestData.disease_txt = this.requestData.disease_txt;
        requestData.smkng_yn = this.requestData.smkng_yn;
        requestData.mber_job = this.requestData.job_yn;


        MediNewNetworkModule.doApi(getContext(), new Tr_mber_modify_mberinfo(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_mber_modify_mberinfo) {
                    Tr_mber_modify_mberinfo data = (Tr_mber_modify_mberinfo)responseData;
                    try {
                        switch (data.result_code){
                            case ReceiveDataCode.MBER_LOAD_MYPAGE_SUCCESS:
                                Intent info = new Intent();
                                myPageData.actqy = requestData.actqy;
                                myPageData.DISEASE_NM = requestData.disease_nm;
                                myPageData.DISEASE_TXT = requestData.disease_txt;
                                myPageData.smoking_yn = requestData.smkng_yn;
                                myPageData.job_yn = requestData.mber_job;
                                info.putExtra(MYPAGE_DATA, myPageData);
                                getActivity().setResult(RESULT_OK, info);
                                onBackPressed();
                                break;
                            case ReceiveDataCode.MBER_LOAD_MYPAGE_ERROR_FAIL:
                                errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:1";
                                break;

                            default:
                                errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:2";
                                break;
                        }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                        errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:3";
                    }
                }

                if(!errorMessage.equals("")){
//                    dialog = DialogCommon.showDialogSingleBtn(getContext(), errorMessage, getResources().getString(R.string.comm_confirm), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialog.dismiss();
//                        }
//                    });
                    CDialog.showDlg(getContext(), errorMessage);
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                Log.e(TAG, response);
            }
        });
    }


}