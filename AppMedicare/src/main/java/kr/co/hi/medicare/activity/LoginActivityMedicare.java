package kr.co.hi.medicare.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.co.hi.medicare.googleFitness.SplashUploadGoogleFitData;
import kr.co.hi.medicare.net.hwNet.ApiData;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperFoodDetail;
import kr.co.hi.medicare.database.DBHelperFoodMain;
import kr.co.hi.medicare.database.DBHelperLog;
import kr.co.hi.medicare.database.DBHelperMessage;
import kr.co.hi.medicare.fragment.login.AgreeIntroFragment;
import kr.co.hi.medicare.fragment.login.FindIdFragment;
import kr.co.hi.medicare.fragment.login.FindPwdFragment;
import kr.co.hi.medicare.fragment.login.LoginFirstInfoFragment1_2;
import kr.co.hi.medicare.fragment.login.join.JoinMainFragment;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_get_hedctdata;
import kr.co.hi.medicare.net.data.Tr_menu_log_hist;
import kr.co.hi.medicare.net.hwdata.Tr_get_infomation;
import kr.co.hi.medicare.net.hwdata.Tr_get_meal_input_data;
import kr.co.hi.medicare.net.hwdata.Tr_get_meal_input_food_data;
import kr.co.hi.medicare.net.hwdata.Tr_infra_message;
import kr.co.hi.medicare.net.hwdata.Tr_infra_message_write;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.AlertDialogUtil;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.value.Define;
import kr.co.hi.medicare.value.model.MessageModel;

/**
 * 로그인.
 */
public class LoginActivityMedicare extends BaseActivityMedicare {

    private final int REQ_CODE_JOIN = 0x324;

    private EditText activity_login_EditText_id, activity_login_EditText_pw;
    private TextView mLoginBtn;
    private UserInfo user;
//    private boolean auto = false,
//            save = false;
    private String id = null, pw = null;
//    private PackageInfo packageInfo;
    private CheckBox activity_login_ImageView_auto;

    private LinearLayout mCurverFrame;
    private LinearLayout mLoginFrame;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_medicare);
        init();

    }

    private void init() {

        user = new UserInfo(this);
        Intent intent = null;
        // 유저 버전 정보.
//        try {
//            packageInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }

        activity_login_EditText_id = findViewById(R.id.activity_login_EditText_id);
        activity_login_EditText_pw = findViewById(R.id.activity_login_EditText_pw);
        mLoginBtn = findViewById(R.id.activity_login_btn);
        activity_login_ImageView_auto = findViewById(R.id.activity_login_TextView_auto);

        //자동로그인 활성화
        activity_login_ImageView_auto.setChecked(true);

        activity_login_EditText_id.addTextChangedListener(textWatcher);
        activity_login_EditText_pw.addTextChangedListener(textWatcher2);

        mCurverFrame = findViewById(R.id.login_fore_frame);
        mLoginFrame = findViewById(R.id.login_layout_frame);

        // 신규 사용자
        findViewById(R.id.login_new_user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    click_event(v);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        // 기존 사용자
        findViewById(R.id.login_regist_user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurverFrame.setVisibility(View.GONE);
                mLoginFrame.setVisibility(View.VISIBLE);
                slideLeft(mLoginFrame);
            }
        });

        if (user.getIsAutoLogin()) {
            // 자동 로그인이 true일때.
            id = user.getSaveId();
            pw = user.getSavedPw();

            doMediLoginNew(id, pw, true);
        }

    }

    /**
     * 클릭로그
     */

    private void sendLog(final Tr_login login){
        String sendFlag = SharedPref.getInstance().getPreferences(SharedPref.SEND_LOG_DATE);

        if(sendFlag == null || sendFlag.equals("")){
            sendFlag = CDateUtil.getToday_yyyy_MM_dd();
            SharedPref.getInstance().savePreferences(SharedPref.SEND_LOG_DATE,sendFlag);
        }

        if(StringUtil.getLong(sendFlag) < StringUtil.getLong(CDateUtil.getToday_yyyy_MM_dd())) {
            Tr_menu_log_hist.RequestData requestData = new Tr_menu_log_hist.RequestData();
            requestData.mber_sn = login.mber_sn;
            requestData.DATA = Tr_menu_log_hist.getArray(LoginActivityMedicare.this);
            requestData.DATA_LENGTH = String.valueOf(requestData.DATA.length());

            if(requestData.DATA.length() > 0) {

                MediNewNetworkModule.doApi(LoginActivityMedicare.this, Tr_menu_log_hist.class, requestData, new MediNewNetworkHandler() {
                    @Override
                    public void onSuccess(BaseData responseData) {
                        if (responseData instanceof Tr_menu_log_hist) {
                            Tr_menu_log_hist data = (Tr_menu_log_hist) responseData;

                            if (data.result_code.equals("0000")) {
                                DBHelper helper = new DBHelper(LoginActivityMedicare.this);
                                DBHelperLog db = helper.getLogDb();
                                db.delete_log();

                                SharedPref.getInstance().savePreferences(SharedPref.SEND_LOG_DATE, CDateUtil.getToday_yyyy_MM_dd());
                                //TODO 이동처리
                                doFirstData(login.mber_sn, login.add_reg_yn);

                            } else {
                                //TODO 이동처리
                                doFirstData(login.mber_sn, login.add_reg_yn);

                            }
                        } else {
                            //TODO 이동처리
                            doFirstData(login.mber_sn, login.add_reg_yn);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, String response, Throwable error) {
                        CDialog.showDlg(LoginActivityMedicare.this, getString(R.string.networkexception))
                                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        finish();
                                    }
                                });
                    }
                });
            } else {
                //TODO 이동처리
                doFirstData(login.mber_sn, login.add_reg_yn);
            }
        } else {
            //TODO 이동처리
            doFirstData(login.mber_sn, login.add_reg_yn);
        }
    }

    /**
     * 신규 로그인 전문
     * @param id
     * @param pw
     * @param isAutoLogin
     */
    private void doMediLoginNew(final String id, final String pw, final boolean isAutoLogin) {
        final Tr_login.RequestData requestData = new Tr_login.RequestData();

        requestData.mber_id = id;
        requestData.mber_pwd = pw;
        showProgress();
        MediNewNetworkModule.doApi(LoginActivityMedicare.this, Tr_login.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_login) {
                    showProgress();
                    Tr_login data = (Tr_login)responseData;
                    try {

                        isFirstLogin = data.add_reg_yn;
                        String resultCode = data.log_yn;
                        if ("Y".equals(resultCode)) {

                            if (requestData.mber_id.equals(user.getSaveId()) == false) {
//                                String loginJson = UserInfo.getLoginInfoJson(); // 로그인 정보 Json 백업

                                // 기존 사용자 아이디와 다른경우 내부 sqlite 삭제
                                DBHelper helper = new DBHelper(LoginActivityMedicare.this);
                                helper.deleteAll();
                                SharedPref.getInstance().removeAllPreferences();    // sharedprefrence 초기화

                                // Sqlash 음식 초기데이터 true
                                SharedPref.getInstance().savePreferences(SharedPref.INTRO_FOOD_DB, true);

                                //건강메시지 On/Off
                                SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MESSAGE_CONFIRIM, false);

                                // 로그인 정보 Json 다시 저장
                                SharedPref.getInstance().savePreferences(SharedPref.LOGIN_JSON, responseData.getJsonString());
                            } else {
                                SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MESSAGE_CONFIRIM, true);
                            }
                            user.setSavedId(id);
                            user.setSavePwd(pw);
                            user.setIsAutoLogin(isAutoLogin);
                            if (isAutoLogin) {  // 자동로그인인 경우 가이드 보지 않기 처리
                                SharedPref.getInstance().savePreferences(GuideActivity.PREF_KEY_GUIDE, false);    // 가이드 보지 않기
                            }

                            SharedPref.getInstance().savePreferences(SharedPref.SPLASH_BUILD_GOOGLE_ACCOUNT, true);

                            SharedPref.getInstance().savePreferences(SharedPref.SAVED_LOGIN_PWD, requestData.mber_pwd);
                            SharedPref.getInstance().savePreferences(SharedPref.MBER_SN, data.mber_sn);

                            SharedPref.getInstance().savePreferences(SharedPref.SAVED_LOGIN_ID, requestData.mber_id);
                            SharedPref.getInstance().savePreferences(SharedPref.IS_LOGIN_SUCEESS, true);
//                        SharedPref.getInstance().savePreferences(SharedPref.IS_AUTO_LOGIN, mAutoLoginCheckBox.isChecked());

//                            data.mber = requestData.mber_id;

                            if(data.mber_bdwgh_app.equals("") || data.mber_bdwgh_app.isEmpty()){
                                data.mber_bdwgh_app = data.mber_bdwgh;
                            }

                            //클릭로그 저장
                            sendLog(data);

                        } else {
                            hideProgress();
                            // 존재하지 않는 사용자.
                            user.setIsAutoLogin(false);
                            CDialog.showDlg(LoginActivityMedicare.this, getString(R.string.login_wrong_id));
                        }
                    } catch (Exception e) {
                        hideProgress();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                hideProgress();
                Log.e(TAG, "onFailure.statusCode="+statusCode+", response="+response);
                CDialog.showDlg(LoginActivityMedicare.this, getString(R.string.networkexception));
            }
        });
    }
    
    

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            validCheck();
//            ImageView activity_login_ImageView_clear = (ImageView) findViewById(R.id.activity_login_ImageView_clear);
//            if (s.length() > 0) {
//                if (activity_login_EditText_pw.getText().toString().length() != 0) {
////                    mLoginBtn.setBackgroundResource(R.drawable.login_round_mint_press);
//                }
//                activity_login_ImageView_clear.setVisibility(View.VISIBLE);
//            } else {
//                activity_login_ImageView_clear.setVisibility(View.GONE);
//            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    TextWatcher textWatcher2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            validCheck();
//            if (s.length() > 0 && activity_login_EditText_id.getText().toString().length() > 0) {
////                mLoginBtn.setBackgroundResource(R.drawable.login_round_mint_press);
//            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void validCheck() {
        String id = activity_login_EditText_id.getText().toString();
        boolean valid = StringUtil.isValidEmail(id);
        if (valid == false) {
            mLoginBtn.setEnabled(false);
            return;
        }
        String pwd = activity_login_EditText_pw.getText().toString();
        if (pwd.length() < 4) {
            mLoginBtn.setEnabled(false);
            return;
        }

        mLoginBtn.setEnabled(true);
    }

    public void click_event(View v) throws JSONException {
        Intent intent;
        switch (v.getId()) {
            case R.id.activity_login_btn:
                hideKeyBoard();
                // 로그인 버튼
                id = activity_login_EditText_id.getText().toString();
                pw = activity_login_EditText_pw.getText().toString();
                if (id.length() == 0) {
                    // id를 입력하지 않았을때
                    AlertDialogUtil.onAlertDialog(LoginActivityMedicare.this, getString(R.string.confirm), getString(R.string.login_id_warning));
                } else if (pw.length() == 0) {
                    // pw를 입력하지 않았을때
                    AlertDialogUtil.onAlertDialog(LoginActivityMedicare.this, getString(R.string.confirm), getString(R.string.login_pw_warning));
                } else if (!checkEmail(id)) {
                    // email 형식 검사.
                    AlertDialogUtil.onAlertDialog(LoginActivityMedicare.this, getString(R.string.confirm), getString(R.string.msg_not_email));
                } else {

                    doMediLoginNew(id, pw, activity_login_ImageView_auto.isChecked());
                }
                break;
//            case R.id.activity_login_Layout_auto:
//                if (auto) {
//                    auto = false;
//                    activity_login_ImageView_auto.setImageResource(R.drawable.btn_check_off_01);
//                } else {
//                    auto = true;
//                    activity_login_ImageView_auto.setImageResource(R.drawable.btn_check_on_01);
//                }
//                break;
//            case R.id.activity_login_Layout_save:
//                if (save) {
//                    save = false;
//                    activity_login_ImageView_save.setImageResource(R.drawable.btn_check_off_01);
//                } else {
//                    save = true;
//                    activity_login_ImageView_save.setImageResource(R.drawable.btn_check_on_01);
//                }
//                break;
            case R.id.activity_login_ImageView_clear:
                activity_login_EditText_id.setText(null);
                break;
            case R.id.login_new_user:
                // 가입하기
                NewActivity.startActivityForResult(LoginActivityMedicare.this, REQ_CODE_JOIN, JoinMainFragment.class, null);
                break;
            case R.id.activity_login_TextView_findid:
                // 아이디 찾기.
                NewActivity.startActivity(LoginActivityMedicare.this, FindIdFragment.class, null);
                break;
            case R.id.activity_login_TextView_findpw:
                // 비밀번호 찾기.
                NewActivity.startActivity(LoginActivityMedicare.this, FindPwdFragment.class, null);
                break;
        }
    }

    // 이메일 형식 검사.
    private boolean checkEmail(String email) {
        String mail = "^[_a-zA-Z0-9-\\.]+@[\\.a-zA-Z0-9-]+\\.[a-zA-Z]+$";
        Pattern p = Pattern.compile(mail);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity_login_EditText_id.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(activity_login_EditText_pw.getWindowToken(), 0);
    }

    private String isFirstLogin;
    /**
     * 앱삭제시 저장된 3개월치 데이터 가져오기
     */
    private int m3MonthIdx = 2;
    private int m3MonthIdxCnt = 7;
    private void doFirstData(final String mberSn, final String addRegYn) {
//        showProgress();
        if (m3MonthIdx <= m3MonthIdxCnt) {
            getFirstData(""+m3MonthIdx, mberSn, new ApiData.IStep() {
                @Override
                public void next(Object obj) {
                    if (m3MonthIdx == m3MonthIdxCnt) {
                        // 데이터가 7개 데이터 가져오기 되면 음식 데이터 가져오기(걸음,혈당,혈압,체중,물)
                        getFoodData(new ApiData.IStep() {
                            @Override
                            public void next(Object obj) {
                                // 건강 메시지 가져오기
                                getHealthMessageData(new ApiData.IStep() {
                                    @Override
                                    public void next(Object obj) {
                                        boolean isConfirmMsg = SharedPref.getInstance().getPreferences(SharedPref.HEALTH_MESSAGE_CONFIRIM, false);
                                        if(isConfirmMsg == false){
                                            DBHelper helper = new DBHelper(LoginActivityMedicare.this);
                                            DBHelperMessage db = helper.getMessageDb();
                                            List<MessageModel> messageList = db.getResultAll(helper, Tr_infra_message_write.INFRA_TY_ALL);
                                            if(messageList.size() > 0) {
                                                SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MESSAGE, true);
                                            }
                                        }
                                        if("N".equals(isFirstLogin)) {
//                                            stratGoogleClient(MainFragment.newInstance(), null);
                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    googleFitnessDateUpload(addRegYn);
                                                }
                                            }, 300);
                                        } else{
                                            googleFitnessDateUpload(addRegYn);
                                        }
                                    }
                                });
                            }
                        });

                    } else {
                        // 3개월 데이터 가져오기 (걸음, 혈당 혈압 체중)
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                m3MonthIdx++;
                                doFirstData(mberSn, addRegYn);
                            }
                        }, 500);
                    }
                }
            });
        }
    }

    /**
     * 구글 피트니스 걸음 데이터 업로드
     * @param isFirstInfo
     */
    private void googleFitnessDateUpload(final String isFirstInfo) {
        new SplashUploadGoogleFitData().uploadGoogleStepData(this, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                getInformation(isFirstInfo);
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                getInformation(isFirstInfo);
            }
        });
    }

    private void getInformation(final String addRegYn) {
        Tr_get_infomation.RequestData reqData = new Tr_get_infomation.RequestData();
        getData(LoginActivityMedicare.this, Tr_get_infomation.class, reqData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                hideProgress();
                if (obj instanceof Tr_get_infomation) {
                    Tr_get_infomation data = (Tr_get_infomation) obj;
                    Define.getInstance().setInformation(data);



                    Tr_login userInfo = UserInfo.getLoginInfo();

                    if(userInfo.mber_grad.equals("10")){
                        if(userInfo.offerinfo_yn.equals("N")){
                            NewActivity.startActivity(LoginActivityMedicare.this, AgreeIntroFragment.class, null);
                        } else {
                            if ("Y".equals(addRegYn)) {
                                NewActivity.startActivity(LoginActivityMedicare.this, LoginFirstInfoFragment1_2.class, null);
                            } else {
                                Intent intent = new Intent(LoginActivityMedicare.this, MainActivityMedicare.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
                            }
                        }
                    } else {
                        if ("Y".equals(addRegYn)) {
                            NewActivity.startActivity(LoginActivityMedicare.this, LoginFirstInfoFragment1_2.class, null);
                        } else {
                            Intent intent = new Intent(LoginActivityMedicare.this, MainActivityMedicare.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
                        }
                    }



                } else {
                    CDialog.showDlg(LoginActivityMedicare.this, "네트워크 연결 상태를 확인해주세요.");
                }
            }
        });
    }


    /**
     * 3개월치 데이터 가져오기
     * @param code
     * @param iStep
     */
    private void getFirstData(final String code, final String mberSn, final ApiData.IStep iStep) {
        // 한번 저장 완료가 되면 호출 하지 않기 위한 값
        boolean isSaved = SharedPref.getInstance().getPreferences(SharedPref.SAVED_LOGIN_ID+code , false);
        Logger.i(TAG, "getFirstData.code="+code +", isSaved="+isSaved);
        if (isSaved) {
            iStep.next(null);
            return;
        }

        Calendar cal = Calendar.getInstance(Locale.KOREA);
        Date now = new Date();
        cal.setTime(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        Tr_get_hedctdata.RequestData requestData = new Tr_get_hedctdata.RequestData();
        requestData.mber_sn = mberSn;//UserInfo.getLoginInfo().mber_sn;

        requestData.get_data_typ = code;
        requestData.end_day = CDateUtil.getToday_yyyy_MM_dd();    // 오늘 날자
        cal.set(Calendar.MONTH, -3);
        requestData.begin_day = sdf.format(cal.getTimeInMillis());   // 20170301

        showProgress();

        getData(LoginActivityMedicare.this, Tr_get_hedctdata.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_get_hedctdata) {
                    DBHelper helper = new DBHelper(LoginActivityMedicare.this);

                    // 1: 운동      2:혈당    3:혈압    4:체중    5:물data (제일 Low data), 6:심박수
                    Tr_get_hedctdata data = (Tr_get_hedctdata)obj;
                    if ("1".equals(data.get_data_typ)) {
                        helper.getStepDb().insert( data, true);
                    } else if ("2".equals(data.get_data_typ)) {
                        helper.getSugarDb().insert(data, true);
                    } else if ("3".equals(data.get_data_typ)) {
                        helper.getPresureDb().insert(helper, data, true);
                    } else if ("4".equals(data.get_data_typ)) {
                        helper.getWeightDb().insert(data, true);
                    } else if ("5".equals(data.get_data_typ)) {
                        helper.getWaterDb().insert(data.data_list, true);
                    } else if ("6".equals(data.get_data_typ)) {
                        helper.getPPGDb().insert(data, true);
                    } else if ("7".equals(data.get_data_typ)) {
                        helper.getCalorieDb().insert(data, true);
                    }

                    SharedPref.getInstance().savePreferences(SharedPref.SAVED_LOGIN_ID+code , true);
                    iStep.next(obj);

                } else {

                }
            }
        });
    }

    private void getFoodData(final ApiData.IStep iStep) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        final String endDate = CDateUtil.getToday_yyyy_MM_dd();
        Calendar cal =  CDateUtil.getCalendar_yyyyMMdd(endDate);
        cal.add(Calendar.MONTH, -3);    // 3개월 전
        final String startDate = sdf.format(cal.getTimeInMillis());

        // 식사데이터 가져와서 sqlite 저장하기
        getFirstMealData(startDate, endDate, new ApiData.IStep() {
            @Override
            public void next(Object obj) {

                // 음식 데이터 가져와서 sqlite 저장하기
                getFirstFoodData(startDate, endDate, new ApiData.IStep() {
                    @Override
                    public void next(Object obj) {
                        iStep.next(obj);
                    }
                });
            }
        });
    }

    /**
     * 데이터 가져오기(식사)
     */
    private void getFirstMealData(final String beginDate, final String endDate, final ApiData.IStep iStep) {
        final boolean isSaved = SharedPref.getInstance().getPreferences(SharedPref.IS_SAVED_MEAL_DB, false);

        if (isSaved) {
            iStep.next(isSaved);
            return;
        }

        Tr_get_meal_input_data.RequestData requestData = new Tr_get_meal_input_data.RequestData();

        Tr_login login = UserInfo.getLoginInfo();
        requestData.mber_sn = login.mber_sn;
        requestData.begin_day = beginDate;
        requestData.end_day = endDate;

        getData(LoginActivityMedicare.this, Tr_get_meal_input_data.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_get_meal_input_data) {
                    Tr_get_meal_input_data data = (Tr_get_meal_input_data) obj;

                    DBHelper helper = new DBHelper(LoginActivityMedicare.this);
                    DBHelperFoodMain db = helper.getFoodMainDb();
                    db.insert(data.data_list);

                    SharedPref.getInstance().savePreferences(SharedPref.IS_SAVED_MEAL_DB, true);

                    iStep.next(true);
                } else {
                    iStep.next(false);
                }
            }
        });
    }

    /**
     * 데이터 가져오기 (음식)
     */
    private void getFirstFoodData(String startDate, String endDate, final ApiData.IStep iStep) {
        boolean isSaved = SharedPref.getInstance().getPreferences(SharedPref.IS_SAVED_FOOD_DB, false);
        Logger.i(TAG, "getFirstFoodData, isSaved="+isSaved);
        if (isSaved) {
            iStep.next(isSaved);
            return;
        }

        final Tr_get_meal_input_food_data.RequestData requestData = new Tr_get_meal_input_food_data.RequestData();
        Tr_login login = UserInfo.getLoginInfo();

        requestData.mber_sn = login.mber_sn;
        requestData.begin_day = startDate;
        requestData.end_day = endDate;

        getData(LoginActivityMedicare.this, Tr_get_meal_input_food_data.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_get_meal_input_food_data) {
                    Tr_get_meal_input_food_data data = (Tr_get_meal_input_food_data) obj;

                    DBHelper helper = new DBHelper(LoginActivityMedicare.this);
                    DBHelperFoodDetail db = helper.getFoodDetailDb();
                    db.insert(data.data_list);

                    SharedPref.getInstance().savePreferences(SharedPref.IS_SAVED_FOOD_DB, true);

                    iStep.next(true);
                } else {
                    iStep.next(false);
                }
            }
        });
    }

    /**
     * 건강 메시지 가져와 Sqlite 저장하기
     */
    private int mHealthMsgCnt = 1;
    private void getHealthMessageData(final ApiData.IStep iStep) {

        final boolean isSaved = SharedPref.getInstance().getPreferences(SharedPref.IS_SAVED_HEALTH_MESSAGE_DB, false);

        if (isSaved) {
            iStep.next(isSaved);
            return;
        }

        Tr_infra_message.RequestData reqData = new Tr_infra_message.RequestData();
        Tr_login login = UserInfo.getLoginInfo();

        reqData.mber_sn = login.mber_sn;
        reqData.pageNumber = ""+mHealthMsgCnt;

        getData(LoginActivityMedicare.this, Tr_infra_message.class, reqData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_infra_message) {
                    Tr_infra_message recvData = (Tr_infra_message) obj;
                    int pageNum = StringUtil.getIntVal(recvData.pageNumber);
                    int pageMaxNum = StringUtil.getIntVal(recvData.maxpageNumber);

                    DBHelper helper = new DBHelper(LoginActivityMedicare.this);
                    DBHelperMessage db = helper.getMessageDb();
                    db.insert(recvData.message_list, true);

                    if (pageNum > pageMaxNum) {
                        getHealthMessageData(iStep);
                    } else {
                        SharedPref.getInstance().savePreferences(SharedPref.IS_SAVED_HEALTH_MESSAGE_DB, true);
                        iStep.next(obj);
                    }

                    mHealthMsgCnt++;
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (mLoginFrame.getVisibility() == View.VISIBLE) {
            slideRight(mLoginFrame);
            mCurverFrame.setVisibility(View.VISIBLE);
            mLoginFrame.setVisibility(View.GONE);
        } else {
//            super.onBackPressed();
            finishStep();
        }
    }

    public void slideLeft(View view){
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                view.getWidth(),                 // fromXDelta
                0,                 // toXDelta
                0,  // fromYDelta
                0);                // toYDelta
        animate.setDuration(400);
        animate.setFillAfter(false);
        view.startAnimation(animate);
    }

    public void slideRight(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                view.getWidth(),                 // toXDelta
                0,                 // fromYDelta
                0); // toYDelta
        animate.setDuration(400);
        animate.setFillAfter(false);
        view.startAnimation(animate);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPref.getInstance().initContext(this);
    }
}
