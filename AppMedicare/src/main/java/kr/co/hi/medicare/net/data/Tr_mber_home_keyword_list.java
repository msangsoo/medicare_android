package kr.co.hi.medicare.net.data;

/**
 *
 * 상담키워드
 *
 input값
 insures_code : 회사코드
 mber_sn : 회원키값
 pageNumber : 페이지 번호
 sex : 성별(1 남자) , 2(여자)

 output 값
 api_code : 호출코드명
 insures_code : 회사코드
 pageNumber :
 maxpageNumber :
 counsel_cnt : 건강상담 이용자수

 keyword_list : 2차배열
 ranking : 랭킹
 keyword : 키워드명
 sex : 성별(1남) 2(여)
 */

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


public class Tr_mber_home_keyword_list extends BaseData {
    private final String TAG = getClass().getSimpleName();

    public static class RequestData {
        public String mber_sn; // 1000
        public String pageNumber; // 1000
        public String sex; // 99
    }


    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {

            JSONObject body = new JSONObject();

            RequestData data = (RequestData) obj;
            body.put("api_code", TAG.replace("Tr_", ""));
            body.put("insures_code", INSURES_CODE);
            body.put("mber_sn", data.mber_sn); // 1000",
            body.put("pageNumber", data.pageNumber); // 1000",
            body.put("sex", data.sex); // 99

            return body;
        }

        return super.makeJson(obj);
    }

/**************************************************************************************************/
/***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("insures_code")
    public String insures_code;
    @SerializedName("api_code")
    public String api_code;


    @SerializedName("data_yn")
    public String data_yn;
    @SerializedName("counsel_cnt")
    public String counsel_cnt;
    @SerializedName("tot_counsel_cnt")
    public String tot_counsel_cnt;
    @SerializedName("keyword_info_woman_list")
    public List<Keyword_info_woman_list> keyword_info_woman_list;
    @SerializedName("keyword_info_man_list")
    public List<Keyword_info_man_list> keyword_info_man_list;
    @SerializedName("my_keyword_list")
    public List<My_keyword_list> my_keyword_list;


    public static class Keyword_info_woman_list {
        @SerializedName("keyword_txt")
        public String keyword_txt;
        @SerializedName("keyword_nm")
        public String keyword_nm;
        @SerializedName("keyword_sn")
        public String keyword_sn;
        @SerializedName("view_order")
        public String view_order;
        @SerializedName("age_sex")
        public String age_sex;
        @SerializedName("age_group")
        public String age_group;
    }

    public static class Keyword_info_man_list {
        @SerializedName("keyword_txt")
        public String keyword_txt;
        @SerializedName("keyword_nm")
        public String keyword_nm;
        @SerializedName("keyword_sn")
        public String keyword_sn;
        @SerializedName("view_order")
        public String view_order;
        @SerializedName("age_sex")
        public String age_sex;
        @SerializedName("age_group")
        public String age_group;
    }

    public static class My_keyword_list {
        @SerializedName("keyword_txt")
        public String keyword_txt;
        @SerializedName("keyword_nm")
        public String keyword_nm;
        @SerializedName("keyword_sn")
        public String keyword_sn;
        @SerializedName("view_order")
        public String view_order;
        @SerializedName("age_sex")
        public String age_sex;
        @SerializedName("age_group")
        public String age_group;
    }


}
