package kr.co.hi.medicare.fragment.health.food;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.utilhw.SharedPref;

/**
 * Created by mrsohn on 2017. 3. 14..
 */

public class FoodManageFragment extends BaseFragmentMedi {


    private FoodManageWriteView mRegistView;
    private FoodChartView mChartView;

    private RadioButton[] mFragmentRadio;
    private RadioGroup mFragmentGroup;
    private View tabview;

    private CoordinatorLayout BreakfastCL;
    private CoordinatorLayout BreakfastDisertCL;
    private CoordinatorLayout LauchCL;
    private CoordinatorLayout LauchDisertCL;
    private CoordinatorLayout DinnerCL;
    private CoordinatorLayout DinnerDisertCL;

    private ImageView mHealthMessage;
    private RelativeLayout message_lv;

    private View mVisibleView1;

    public static Fragment newInstance() {
        FoodManageFragment fragment = new FoodManageFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        message_lv = getActivity().findViewById(R.id.message_lv);
        mHealthMessage = getActivity().findViewById(R.id.common_toolbar_back_btn);


    }

    @Nullable
    @Override//activity_food_manage
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_food_manage, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        View TabView = view.findViewById(R.id.food_tab);
        ScrollView chartView = view.findViewById(R.id.layout_food_chart);
        View registView = view.findViewById(R.id.layout_food_write);

        mVisibleView1 = view.findViewById(R.id.visible_view1);


        //탭뷰
        tabview = view;

        mFragmentGroup = view.findViewById(R.id.tab_radiogroup);

        final String[] mFragmentNames = new String[] {
                getString(R.string.text_wriging),
                getString(R.string.text_history),
        };

        mFragmentRadio = new RadioButton[]{
                view.findViewById(R.id.tab1),
                view.findViewById(R.id.tab2),
        };

        int i = 0;
        for (String name : mFragmentNames) {
            mFragmentRadio[i].setText(name);
            i++;
        }

        mFragmentGroup.setOnCheckedChangeListener(mCheckedChangeListener);



        // 차트 화면
        mChartView = new FoodChartView(FoodManageFragment.this, chartView);
        // 등록하기 화면
        mRegistView = new FoodManageWriteView(FoodManageFragment.this, registView);


        //click 저장
        OnClickListener ClickListener = new OnClickListener(null, view, getContext());


        //건강
        view.findViewById(R.id.tab1).setOnTouchListener(ClickListener);
        view.findViewById(R.id.tab2).setOnTouchListener(ClickListener);

        //코드부여
        view.findViewById(R.id.tab1).setContentDescription(getString(R.string.food_wri));
        view.findViewById(R.id.tab2).setContentDescription(getString(R.string.food_his));


        mFragmentRadio[0].setChecked(true);


        //원모양 처리
        BreakfastCL = view.findViewById(R.id.coordinatorLayout_1);
        BreakfastDisertCL = view.findViewById(R.id.coordinatorLayout_2);
        LauchCL = view.findViewById(R.id.coordinatorLayout_3);
        LauchDisertCL = view.findViewById(R.id.coordinatorLayout_4);
        DinnerCL = view.findViewById(R.id.coordinatorLayout_5);
        DinnerDisertCL = view.findViewById(R.id.coordinatorLayout_6);

        BreakfastCL.getViewTreeObserver().addOnGlobalLayoutListener(myOnGlobalLayoutListener);
        BreakfastDisertCL.getViewTreeObserver().addOnGlobalLayoutListener(myOnGlobalLayoutListener);
        LauchCL.getViewTreeObserver().addOnGlobalLayoutListener(myOnGlobalLayoutListener);
        LauchDisertCL.getViewTreeObserver().addOnGlobalLayoutListener(myOnGlobalLayoutListener);
        DinnerCL.getViewTreeObserver().addOnGlobalLayoutListener(myOnGlobalLayoutListener);
        DinnerDisertCL.getViewTreeObserver().addOnGlobalLayoutListener(myOnGlobalLayoutListener);

        mChartView.setVisibleOrientationLayout(getActivity(), false);

    }


    ViewTreeObserver.OnGlobalLayoutListener myOnGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            ViewGroup.LayoutParams params = BreakfastCL.getLayoutParams();
            params.height = (int)(BreakfastCL.getMeasuredWidth());
            BreakfastCL.setLayoutParams(params);
            BreakfastCL.getViewTreeObserver().removeOnGlobalLayoutListener(this);

            ViewGroup.LayoutParams params2 = BreakfastDisertCL.getLayoutParams();
            params2.height = (int)(BreakfastDisertCL.getMeasuredWidth());
            BreakfastDisertCL.setLayoutParams(params2);
            BreakfastDisertCL.getViewTreeObserver().removeOnGlobalLayoutListener(this);

            ViewGroup.LayoutParams params3 = LauchCL.getLayoutParams();
            params3.height = (int)(LauchCL.getMeasuredWidth());
            LauchCL.setLayoutParams(params3);
            LauchCL.getViewTreeObserver().removeOnGlobalLayoutListener(this);

            ViewGroup.LayoutParams params4 = LauchDisertCL.getLayoutParams();
            params4.height = (int)(LauchDisertCL.getMeasuredWidth());
            LauchDisertCL.setLayoutParams(params4);
            LauchDisertCL.getViewTreeObserver().removeOnGlobalLayoutListener(this);

            ViewGroup.LayoutParams params5 = DinnerCL.getLayoutParams();
            params5.height = (int)(DinnerCL.getMeasuredWidth());
            DinnerCL.setLayoutParams(params5);
            DinnerCL.getViewTreeObserver().removeOnGlobalLayoutListener(this);

            ViewGroup.LayoutParams params6 = DinnerDisertCL.getLayoutParams();
            params6.height = (int)(DinnerDisertCL.getMeasuredWidth());
            DinnerDisertCL.setLayoutParams(params6);
            DinnerDisertCL.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    };

    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            int pos=group.indexOfChild(tabview.findViewById(checkedId));
            if(pos == 0){
                mRegistView.setVisibility(View.VISIBLE);
                mChartView.setVisibility(View.GONE);

                mRegistView.getData();

            }else {
                mChartView.setVisibility(View.VISIBLE);
                mRegistView.setVisibility(View.GONE);

                mChartView.getData();
            }

        }
    };

    @Override
    public void onResume() {
        super.onResume();
        if (getCurrentFragment() == MainActivityMedicare.HOME_MENU_2) {
            if(getCurrentTopFragment(FoodManageFragment.this) == 0) {
                if (mRegistView != null && mRegistView.getVisibility() == View.VISIBLE) {
                    mRegistView.onResume();
                }

                if (mChartView != null && mChartView.getVisibility() == View.VISIBLE) {
                    mChartView.getData();
                }

                setReadyDataView();
            }
        }
    }


    private void setReadyDataView(){

        if (SharedPref.getInstance().getPreferences(SharedPref.HEALTH_MESSAGE, false)) {
            mHealthMessage.setImageResource(R.drawable.health_m_btn_01ov);
            message_lv.setVisibility(View.VISIBLE);
        } else {
            mHealthMessage.setImageResource(R.drawable.health_m_btn_01);
            message_lv.setVisibility(View.GONE);
        }

        if(SharedPref.getInstance().getPreferences(SharedPref.HEALTH_MISSION_POP, false)){
            health_point_popup("3",R.drawable.main_coin_4p_4); //식사
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.i(TAG, "onConfigurationChanged="+newConfig.orientation);
        if (getCurrentFragment() == MainActivityMedicare.HOME_MENU_2) {
            if (getCurrentTopFragment(FoodManageFragment.this) == 0) {
                boolean isLandScape = newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE;
                mVisibleView1.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
                mChartView.setVisibleOrientationLayout(getActivity(), isLandScape);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == FoodManageWriteView.FOOD_INPUT_REQ) {
                mRegistView.onActivityResult(requestCode, resultCode, data);
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
