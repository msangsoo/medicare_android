package kr.co.hi.medicare.fragment.health.step;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.net.hwNet.BaseData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperCalorie;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.IBaseFragment;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_mvm_info_input_data_sport;
import kr.co.hi.medicare.net.data.Tr_mvm_info_sport_list;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 * Created by insystemscompany on 2017. 2. 28..
 */

public class CalorieInputFragment extends BaseFragmentMedi implements IBaseFragment {

    private List<Tr_mvm_info_sport_list.active_list> ExercizeList = new ArrayList<>();
    private String active_seq = "", mets = "", active_nm = "";

    private TextView ex_category,ex_date, ex_starttime, ex_endtime;

    private String type = "";

    private CalorieSwipeListView mSwipeListView;


    public static Fragment newInstance() {
        CalorieInputFragment fragment = new CalorieInputFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_calorie_input, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        String date = "";
        date = bundle.getString("DATE_TIME");



        ex_category = view.findViewById(R.id.ex_category);
        ex_date = view.findViewById(R.id.ex_date);
        ex_starttime = view.findViewById(R.id.ex_starttime);
        ex_endtime = view.findViewById(R.id.ex_endtime);

        ex_date.setText(date);
        ex_date.setTag(date.replace(".",""));
        ex_category.setText("운동선택");
        ex_starttime.setText(CDateUtil.getTodayTimeHH_MM(-1));
        ex_starttime.setTag(CDateUtil.getTodayTimeHH_MM(-1).replace(":",""));
        ex_endtime.setText(CDateUtil.getTodayTimeHH_MM(0));
        ex_endtime.setTag(CDateUtil.getTodayTimeHH_MM(0).replace(":",""));

        ex_category.setOnClickListener(mClickListener);
        ex_starttime.setOnClickListener(mClickListener);
        ex_endtime.setOnClickListener(mClickListener);
        view.findViewById(R.id.add_btn).setOnClickListener(mClickListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, getContext());


        //건강관리
        view.findViewById(R.id.add_btn).setOnTouchListener(ClickListener);

        //코드부여
        view.findViewById(R.id.add_btn).setContentDescription(getString(R.string.walk_calorie_input));

        mSwipeListView  = new CalorieSwipeListView(view, CalorieInputFragment.this, ex_date.getTag().toString());

    }

    @Override
    public void onResume() {
        super.onResume();
        getList();
    }


    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ex_category:
                    showListDlg();
                    break;
                case R.id.ex_starttime:
                    type ="start";
                    showTimePicker();
                    break;
                case R.id.ex_endtime:
                    type ="end";
                    showTimePicker();
                    break;
                case R.id.add_btn:
                    if(ex_category.getText().toString().equals("") || ex_category.getText().toString().equals("운동선택")){
                        CDialog.showDlg(getContext(),getString(R.string.ex_no_select));
                    }else{
//                        if(mSwipeListView.getHistoryData().size() > 0){
//                            for (Tr_mvm_info_mber_sport_list.sport_list data : mSwipeListView.getHistoryData()) {
//                                if(ex_starttime.getText().toString().equals(data.active_de_stime) && ex_endtime.getText().toString().equals(data.active_de_etime)){
//                                    CDialog.showDlg(getContext(),getString(R.string.ex_same_time));
//                                    break;
//                                }
//                            }
//                            uploadEx();
//                        }else {
                            uploadEx();
//                        }
                    }

                    break;
            }

        }
    };

    /**
     * 시간 피커
     */

    private void showTimePicker() {
        Calendar cal = Calendar.getInstance(Locale.KOREA);
        Date now = new Date();
        cal.setTime(now);

        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        String time = ex_starttime.getText().toString();
        if (TextUtils.isEmpty(time) == false) {
            hour = StringUtil.getIntVal(time.substring(0, 2));
            minute = StringUtil.getIntVal(time.substring(2 , 4));

            Logger.i(TAG, "hour="+hour+", minute="+minute);
        }

        TimePickerDialog dialog = new TimePickerDialog(getContext(), listener, hour, minute, false);
        dialog.show();

    }


    private TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            if(DateTimeCheck(type,hourOfDay, minute)) {
               if(type.equals("start")){
                   ex_starttime.setText(String.format("%02d:%02d", hourOfDay, minute));
                   ex_starttime.setTag(String.format("%02d%02d", hourOfDay, minute));
               } else {
                   ex_endtime.setText(String.format("%02d:%02d", hourOfDay, minute));
                   ex_endtime.setTag(String.format("%02d%02d", hourOfDay, minute));
               }
            }
        }
    };

    private boolean DateTimeCheck(String type, int pram1, int pram2){
        Calendar cal = Calendar.getInstance(Locale.KOREA);

        if(type.equals("start")) {
            cal.set(Calendar.HOUR_OF_DAY, pram1);
            cal.set(Calendar.MINUTE, pram2);
           if(StringUtil.getIntVal(String.format("%02d%02d", pram1, pram2)) >= StringUtil.getIntVal(ex_endtime.getTag().toString())){
                CDialog.showDlg(getContext(), getString(R.string.message_starttime_over));
                return false;
            }
            return true;
        } else {
            cal.set(Calendar.HOUR_OF_DAY, pram1);
            cal.set(Calendar.MINUTE, pram2);

            Log.d(TAG,"cal : " + cal.getTimeInMillis() + " system : "+System.currentTimeMillis());
            if(cal.getTimeInMillis() > System.currentTimeMillis()){
                CDialog.showDlg(getContext(), getString(R.string.message_nowtime_over));
                return false;
            } if(StringUtil.getIntVal(String.format("%02d%02d", pram1, pram2)) <= StringUtil.getIntVal(ex_starttime.getTag().toString())){
                CDialog.showDlg(getContext(), getString(R.string.message_endtime_low));
                return false;
            }
            return true;
        }
    }

    /**
     *운동 카테고리 리스트 Dialog
     * @param data
     */
    private CalorieListDialog mCalorieListDlg;
    private void showListDlg() {
        mCalorieListDlg = new CalorieListDialog(CalorieInputFragment.this, ExercizeList, null);
    }


    private void getList(){
        Tr_login login = UserInfo.getLoginInfo();
        Tr_mvm_info_sport_list.RequestData requestData = new Tr_mvm_info_sport_list.RequestData();

        requestData.mber_sn = login.mber_sn;

        MediNewNetworkModule.doApi(getContext(), Tr_mvm_info_sport_list.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_mvm_info_sport_list) {
                    Tr_mvm_info_sport_list data = (Tr_mvm_info_sport_list)responseData;
                    try {
                        List<Tr_mvm_info_sport_list.active_list> list = data.active_list;
                        ExercizeList.clear();
                        ExercizeList.addAll(list);


                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
            }
        });
    }

    public void saveExercize(String seq, String met,String nm){
        active_seq = seq;
        mets = met;
        active_nm = nm;

        ex_category.setText(active_nm);
    }


    /**
     * 운동 입력
     */
    private void uploadEx(){
        Tr_login login = UserInfo.getLoginInfo();
        Tr_mvm_info_input_data_sport.RequestData requestData = new Tr_mvm_info_input_data_sport.RequestData();

        requestData.mber_sn = login.mber_sn;
        requestData.active_seq = active_seq;
        requestData.mets = mets;
        requestData.active_de = ex_date.getTag().toString();
        requestData.active_nm = active_nm;
        requestData.active_de_stime = ex_starttime.getText().toString();
        requestData.active_de_etime = ex_endtime.getText().toString();

        MediNewNetworkModule.doApi(getContext(), Tr_mvm_info_input_data_sport.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_mvm_info_input_data_sport) {
                    Tr_mvm_info_input_data_sport data = (Tr_mvm_info_input_data_sport)responseData;
                    try {
                        if(data.reg_yn.equals("Y")) {
                            List<Tr_mvm_info_input_data_sport.sport_list> list = data.sport_list;

                            DBHelper helper = new DBHelper(getContext());
                            DBHelperCalorie db = helper.getCalorieDb();
                            db.insert(list,data.sport_sn,true);

                            mSwipeListView.getHistoryData(false);

//                            CDialog.showDlg(getContext(),getString(R.string.regist_success));
                        } else if(data.reg_yn.equals("YN")){
                            CDialog.showDlg(getContext(),getString(R.string.ex_same_time));
                        } else{
                            CDialog.showDlg(getContext(),getString(R.string.text_regist_fail));
                        }



                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
            }
        });
    }



}