package kr.co.hi.medicare.bean;

/**
 * Created by suwun on 2016-06-21.
 * 서비스 이력 면역세포 / 면역력,세포검사 / PET-CT
 * 서비스 뇌졸중 방문재활
 * @since 0, 1
 */
public class ServiceSecondItem {

    private String PS_SEQ;
    private String mREQDATE;
    private String mUSEDATE;
    private String mSO_NAME;
    private String mUSE_MEMO;
    private String mSTATE_NAME;
    private String mSCOUNT;
    private String mOSEQ;
    private String mRESULT_CODE;

    public ServiceSecondItem(String seq,
                             String reqdate,
                             String usedate,
                             String sq_name,
                             String use_memo,
                             String state_name,
                             String scount,
                             String oseq,
                             String result_code){
        this.PS_SEQ =   seq;
        this.mREQDATE  =   reqdate;
        this.mUSEDATE =   usedate;
        this.mSO_NAME   =   sq_name;
        this.mUSE_MEMO   =   use_memo;
        this.mSTATE_NAME   =   state_name;
        this.mSCOUNT   =   scount;
        this.mOSEQ   =   oseq;
        this.mRESULT_CODE   =   result_code;
    }

    public String getPS_SEQ() {
        return PS_SEQ;
    }

    public void setPS_SEQ(String PS_SEQ) {
        this.PS_SEQ = PS_SEQ;
    }

    public String getmREQDATE() {
        return mREQDATE;
    }

    public void setmREQDATE(String mREQDATE) {
        this.mREQDATE = mREQDATE;
    }

    public String getmUSEDATE() {
        return mUSEDATE;
    }

    public void setmUSEDATE(String mUSEDATE) {
        this.mUSEDATE = mUSEDATE;
    }

    public String getmSO_NAME() {
        return mSO_NAME;
    }

    public void setmSO_NAME(String mSO_NAME) {
        this.mSO_NAME = mSO_NAME;
    }

    public String getmSCOUNT() {
        return mSCOUNT;
    }

    public void setmmSCOUNT(String mSCOUNT) {
        this.mSCOUNT = mSCOUNT;
    }

    public String getmSTATE_NAME() {
        return mSTATE_NAME;
    }

    public void setmSTATE_NAME(String mSTATE_NAME) {
        this.mSTATE_NAME = mSTATE_NAME;
    }

    public String getmUSE_MEMO() {
        return mUSE_MEMO;
    }

    public void setmUSE_MEMO(String mUSE_MEMO) {
        this.mUSE_MEMO = mUSE_MEMO;
    }

    public String getmOSEQ() {
        return mOSEQ;
    }

    public void setmOSEQ(String mOSEQ) {
        this.mOSEQ = mOSEQ;
    }

    public String getmRESULT_CODE() {
        return mRESULT_CODE;
    }

    public void setmRESULT_CODE(String mRESULT_CODE) {
        this.mRESULT_CODE = mRESULT_CODE;
    }

}
