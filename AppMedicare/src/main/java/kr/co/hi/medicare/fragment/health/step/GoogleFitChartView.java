/*
 * Copyright (C) 2016 Google, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package kr.co.hi.medicare.fragment.health.step;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.charting.data.BarEntry;
import kr.co.hi.medicare.chartview.valueFormat.AxisValueFormatter;
import kr.co.hi.medicare.chartview.walk.BarChartView;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperCalorie;
import kr.co.hi.medicare.database.DBHelperPPG;
import kr.co.hi.medicare.database.DBHelperWeight;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.googleFitness.GoogleFitInstance;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.ChartTimeUtil;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.value.TypeDataSet;

import static java.text.DateFormat.getDateTimeInstance;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class GoogleFitChartView {
    public static final String TAG = GoogleFitChartView.class.getSimpleName();

    protected List<BarEntry> mYVals = new ArrayList<>();
    protected Map<Integer, Float> mGoogleFitDataMap = new HashMap<>();           // 구글 피트니스 조회 데이터 저장
    private Map<Integer, Integer> mDbResultMap = new HashMap<>();   // Sqlite 조회 내용 저장

    private BaseFragmentMedi mBaseFragment;
    private Context mContext;

    private boolean mChartType;
    public boolean mIslandscape;

//    private int mArrIdx = 0;
    public ChartTimeUtil mTimeClass;

//    public GoogleApiClient mClient = null;
    protected BarChartView mChart;

    protected ImageButton mNextbtn;
    protected ImageButton mPrebtn;
    private TextView mDateTv;

    private Long mTotalVal = 0L;
    private DecimalFormat mFloatFormat = new DecimalFormat("#,###.00");

    private TextView tvCalorieBoxValue01;
    private TextView tvCalorieBoxValue02;
    private TextView tvCalorieBoxValue03;

    private TextView tvStepBoxValue01;
    private TextView tvStepBoxValue02;
    private TextView tvStepBoxValue03;
    private TextView tvStepBoxValue04;

    private TextView tvActiveTitle;
    private TextView tvActiveValue;
    private TextView tvActiveValueTag;
    private TextView tvTargetValue;
    private TextView tvTargetValueTag;
    private TextView chartunit;
    private View mCarorieLayout, mStepLayout;

    private TextView mRadio_btn_calory_tv,mRadio_btn_step_tv;
    private LinearLayout mRadio_btn_calory, mRadio_btn_step, mLabelayout;

    private ImageView action_bar_write_btn, share_write;

    private String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private OnDataPointListener mListener;

//    int tmp=0;
    String _startDate;
    String _endDate;

    public GoogleFitChartView(BaseFragmentMedi baseFragment, View view) {
        mContext = baseFragment.getContext();
        mBaseFragment = baseFragment;
        initView(view);
    }

    private void initView(View view) {
        mChartType = false;
        mRadio_btn_calory = view.findViewById(R.id.radio_btn_calory);
        mRadio_btn_step = view.findViewById(R.id.radio_btn_step);
        mRadio_btn_calory_tv = view.findViewById(R.id.radio_btn_calory_tv);
        mRadio_btn_step_tv = view.findViewById(R.id.radio_btn_step_tv);

        mPrebtn = (ImageButton) view.findViewById(R.id.pre_btn);
        mNextbtn = (ImageButton) view.findViewById(R.id.next_btn);
        mDateTv = (TextView) view.findViewById(R.id.period_date_textview);
        mLabelayout  = view.findViewById(R.id.chart_label_layout);

        RadioGroup periodRg = (RadioGroup) view.findViewById(R.id.period_radio_group);
        RadioButton radioBtnDay = (RadioButton) view.findViewById(R.id.period_radio_btn_day);
        RadioButton radioBtnWeek = (RadioButton) view.findViewById(R.id.period_radio_btn_week);
        RadioButton radioBtnMonth = (RadioButton) view.findViewById(R.id.period_radio_btn_month);
        mTimeClass = new ChartTimeUtil(radioBtnDay, radioBtnWeek, radioBtnMonth);


        mChart = new BarChartView(mContext, view);
        mChart.setDefaultDummyData(mTimeClass);



        mCarorieLayout = (View) view.findViewById(R.id.calorie_lv);
        mStepLayout = (View) view.findViewById(R.id.step_lv);

        tvStepBoxValue01 = (TextView) view.findViewById(R.id.step_target_value);
        tvStepBoxValue02 = (TextView) view.findViewById(R.id.step_km_value);
        tvStepBoxValue03 = (TextView) view.findViewById(R.id.step_money_value);
        tvStepBoxValue04 = (TextView) view.findViewById(R.id.step_cm_value);

        tvCalorieBoxValue01 = (TextView) view.findViewById(R.id.calorie_target_value);
        tvCalorieBoxValue02 = (TextView) view.findViewById(R.id.calorie_time_value);
        tvCalorieBoxValue03 = (TextView) view.findViewById(R.id.calorie_span_value);

        chartunit = (TextView) view.findViewById(R.id.chart_unit);
        tvActiveTitle = (TextView) view.findViewById(R.id.tvActiveTitle);


        tvActiveValue = (TextView) view.findViewById(R.id.tvActiveValue);
        tvTargetValue = (TextView) view.findViewById(R.id.tvTargetValue);


        tvActiveValueTag = (TextView) view.findViewById(R.id.tvActiveValueaTag);
        tvTargetValueTag = (TextView) view.findViewById(R.id.tvTargetValueTag);

        share_write = view.findViewById(R.id.share_write);
        action_bar_write_btn = view.findViewById(R.id.action_bar_write_btn);

        mPrebtn.setOnClickListener(mClickListener);
        mNextbtn.setOnClickListener(mClickListener);
        mRadio_btn_calory.setOnClickListener(mClickListener);
        mRadio_btn_step.setOnClickListener(mClickListener);
        share_write.setOnClickListener(mClickListener);
        action_bar_write_btn.setOnClickListener(mClickListener);
        periodRg.setOnCheckedChangeListener(mCheckedChangeListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, mContext);


        //건강관리
        mRadio_btn_calory.setOnTouchListener(ClickListener);
        mRadio_btn_step.setOnTouchListener(ClickListener);
        action_bar_write_btn.setOnTouchListener(ClickListener);
        share_write.setOnTouchListener(ClickListener);

        //코드부여
        mRadio_btn_calory.setContentDescription(mContext.getString(R.string.mRadio_btn_calory));
        mRadio_btn_step.setContentDescription(mContext.getString(R.string.mRadio_btn_step));
        action_bar_write_btn.setContentDescription(mContext.getString(R.string.walk_input));
        share_write.setContentDescription(mContext.getString(R.string.share_write_walk));





        mRadio_btn_calory.setSelected(true);
        mRadio_btn_calory_tv.setSelected(true);
        mRadio_btn_step.setSelected(false);
        mRadio_btn_step_tv.setSelected(false);

        setNextButtonVisible();
        getData();
    }

    /**
     * 걸음수 조회 하기
     */
    public DataReadRequest queryFitnessData(TypeDataSet.Type type) {
        DataType dataType1 = null;
        DataType dataType2 = null;

        /* 조회 타입 설정(칼로리, 걸음수) */
//        if (getType() == TypeDataSet.Type.TYPE_CALORY) {
        if (type == TypeDataSet.Type.TYPE_CALORY) {
            // 칼로리
            dataType1 = DataType.TYPE_STEP_COUNT_DELTA;
            dataType2 = DataType.AGGREGATE_STEP_COUNT_DELTA;
            mStepLayout.setVisibility(View.GONE);
            mCarorieLayout.setVisibility(View.VISIBLE);
            mLabelayout.setVisibility(View.VISIBLE);
            mRadio_btn_calory_tv.setSelected(true);
            mRadio_btn_calory.setSelected(true);
            mRadio_btn_step_tv.setSelected(false);
            mRadio_btn_step.setSelected(false);

        } else if (getType() == TypeDataSet.Type.TYPE_STEP) {
            // 걸음수
            dataType1 = DataType.TYPE_STEP_COUNT_DELTA;
            dataType2 = DataType.AGGREGATE_STEP_COUNT_DELTA;
            mCarorieLayout.setVisibility(View.GONE);
            mStepLayout.setVisibility(View.VISIBLE);
            mLabelayout.setVisibility(View.GONE);
            mRadio_btn_calory_tv.setSelected(false);
            mRadio_btn_calory.setSelected(false);
            mRadio_btn_step_tv.setSelected(true);
            mRadio_btn_step.setSelected(true);
        }

        long startTime = mTimeClass.getStartTime();
        long endTime = mTimeClass.getEndTime();

        DateFormat dateFormat = getDateTimeInstance();
        Log.i(TAG, "Range Start: " + dateFormat.format(startTime));
        Log.i(TAG, "Range End: " + dateFormat.format(endTime));
        Log.i(TAG, "TimeUnit: " + mTimeClass.getTimeUnit());

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(dataType1, dataType2)
                .bucketByTime(1, mTimeClass.getTimeUnit())
                .setTimeRange(startTime, endTime, MILLISECONDS)
                .build();

        return readRequest;
    }

    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            // 일간, 주간, 월간
            TypeDataSet.Period periodType = mTimeClass.getPeriodType();
            mTimeClass.clearTime();         // 날자 초기화

            AxisValueFormatter formatter = new AxisValueFormatter(periodType);
            mChart.setXValueFormat(formatter);

            if (mYVals != null)
                mYVals.clear();

            getData();
        }
    };

    public RadioGroup.OnCheckedChangeListener mTypeCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            getTime();

            if (mYVals != null)
                mYVals.clear();

            getData();
        }
    };


    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            if (vId == R.id.pre_btn) {
                mTimeClass.calTime(-1);
            } else if (vId == R.id.next_btn) {
                // 초기값 일때 다음날 데이터는 없으므로 리턴
                if (mTimeClass.getCalTime() == 0)
                    return;

                mTimeClass.calTime(1);
            } else if(vId == R.id.radio_btn_calory){
                getTime();

                if (mYVals != null)
                    mYVals.clear();

                mRadio_btn_calory.setSelected(true);
                mRadio_btn_calory_tv.setSelected(true);
                mRadio_btn_step.setSelected(false);
                mRadio_btn_step_tv.setSelected(false);


                getData();
            } else if(vId == R.id.radio_btn_step){
                getTime();

                if (mYVals != null)
                    mYVals.clear();

                mRadio_btn_calory.setSelected(false);
                mRadio_btn_calory_tv.setSelected(false);
                mRadio_btn_step.setSelected(true);
                mRadio_btn_step_tv.setSelected(true);

                getData();
            } else if(vId == R.id.share_write){
                CommunityListViewData sharelist = new CommunityListViewData();
                sharelist.ISSHARE=true;
                sharelist.CM_CONTENT = "";
                sharelist.CM_MEAL_LIST = new ArrayList<>();
                sharelist.CM_MEAL_LIST.add(tvActiveTitle.getText().toString()+" "+tvActiveValue.getText().toString()+ " " + tvActiveValueTag.getText().toString() + " / "
                        + tvTargetValue.getText().toString() + " "+tvTargetValueTag.getText().toString());

                NewActivity.moveToWritePage(mBaseFragment, sharelist,  "");
            } else if(vId == R.id.action_bar_write_btn){
                Bundle bundle = new Bundle();
                bundle.putString("DATE_TIME",mDateTv.getText().toString());
                NewActivity.startActivity(mBaseFragment.getActivity(), CalorieInputFragment.class,bundle);
            }


            if (mYVals != null)
                mYVals.clear();

            getData();
            setNextButtonVisible();
        }
    };

    private void setNextButtonVisible(){
        // 초기값 일때 다음날 데이터는 없으므로 리턴
        if (mTimeClass.getCalTime() == 0) {
            mNextbtn.setVisibility(View.INVISIBLE);
        }else{
            mNextbtn.setVisibility(View.VISIBLE);
        }
    }


    protected void getTime() {
        if (mTimeClass != null)
            mTimeClass.getTime();
    }

    /**
     * 날자 계산 후 조회
     */
    private void getData() {
        // 활동소모 초기화
        mBaseFragment.showProgress();
        mChart.getBarChart().setDrawMarkers(false);  // 새로운 조회시 마커 사라지게 하기
        mFoodCalories = new ArrayList<>();
        String[] name = mContext.getResources().getStringArray(R.array.life_food);
        String[] unit = mContext.getResources().getStringArray(R.array.life_food_unit);
        String[] value = mContext.getResources().getStringArray(R.array.life_food_calorie);

        for (int i = 0; i < name.length; i++)
            mFoodCalories.add(new FoodCalories(name[i], unit[i], Integer.valueOf(value[i])));

        long startTime = mTimeClass.getStartTime();
        long endTime = mTimeClass.getEndTime();

        String format = "yyyy.MM.dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        _startDate = sdf.format(startTime);
        _endDate = sdf.format(endTime);

        if (mTimeClass.getPeriodType() == TypeDataSet.Period.PERIOD_DAY) {
            mDateTv.setText(_startDate);
        } else {
            mDateTv.setText(_startDate + " ~ " + _endDate);
        }

        if (GoogleFitInstance.isFitnessAuth(mBaseFragment.getActivity())) {
            Fitness.getHistoryClient(mBaseFragment.getActivity(),
                    GoogleSignIn.getLastSignedInAccount(mBaseFragment.getActivity()))
                    .readData(queryFitnessData(getType()))
                    .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
                        @Override
                        public void onSuccess(DataReadResponse response) {
                            readData(response.getBuckets());
                            fintnessResult();
                        }
                    });
        } else {
            Log.e(TAG, "구글 피트니스 Noti 작동 태스트 중 계정인증 안됨.");
            GoogleFitInstance.requestGoogleFitnessAuth(mBaseFragment.getActivity());
        }
    }

    private void fintnessResult() {
        mBaseFragment.hideProgress();

        Tr_login login = UserInfo.getLoginInfo();
        TypeDataSet.Period period = mTimeClass.getPeriodType();
        Long dayDate = 1L;
        if (period == TypeDataSet.Period.PERIOD_DAY) {
            dayDate = 1L;
            action_bar_write_btn.setVisibility(View.VISIBLE);
        } else if (period == TypeDataSet.Period.PERIOD_WEEK) {
            dayDate = 7L;
            action_bar_write_btn.setVisibility(View.GONE);
        } else if (period == TypeDataSet.Period.PERIOD_MONTH) {
            dayDate = (long) mTimeClass.getDayOfMonth();
            action_bar_write_btn.setVisibility(View.GONE);
        }

        DBHelper helper = new DBHelper(mContext);
        DBHelperCalorie calDB = helper.getCalorieDb();

        // 주간 조회시 남은 주간 채워주기
        int max = 10;
        String startDate = StringUtil.getIntString(_startDate);
        String endDate = StringUtil.getIntString(_endDate);
        List<BarEntry> sportList = new ArrayList<>();
        if (period == TypeDataSet.Period.PERIOD_WEEK) {
            max = 7 - (mYVals.size());
            sportList = calDB.getResultWeek(startDate, endDate);
        } else if (period == TypeDataSet.Period.PERIOD_DAY) {
            max = 24 - (mYVals.size());
            sportList = calDB.getResultDay(startDate);
        } else if (period == TypeDataSet.Period.PERIOD_MONTH) {
            max = mTimeClass.getDayOfMonth() - (mYVals.size());
            String startDay = CDateUtil.getFormattedString_yyyy(mTimeClass.getStartTime());
            String endDay = CDateUtil.getFormattedString_MM(mTimeClass.getStartTime());
            sportList = calDB.getResultMonth(startDay, endDay);
        }

        mTotalVal = 0L;
        List<BarEntry> yVals = new ArrayList<>();
//        for (int i = 0; i < sportList.size(); i++) {
        for (int i = 0; i < max; i++) {

            float fitData = mGoogleFitDataMap.get(i) == null ? 0f : mGoogleFitDataMap.get(i);
            if (getType() == TypeDataSet.Type.TYPE_CALORY) {
                yVals.add(new BarEntry(i, new float[]{fitData, sportList.get(i).getY()}));
                mTotalVal += ((long)sportList.get(i).getY()  + (long)fitData);
            } else {
                yVals.add(new BarEntry(i, new float[]{fitData, 0}));
                mTotalVal += (long)fitData;
            }
        }
        Log.i(TAG, "mTotalVal=" + mTotalVal);

        setBottomField();
        mChart.setData(yVals, mTimeClass);
//        mChart.animateY();
        mChart.invalidate();


        int Goal_Cal = StringUtil.getIntVal(login.goal_mvm_calory);

        if (Goal_Cal==0) {
            Goal_Cal  =  mBaseFragment.getDefaultTargetCalrori();
        }

        int Goal_Step = StringUtil.getIntVal(login.goal_mvm_stepcnt);

        if (Goal_Step==0) {
            int sex = StringUtil.getIntVal(login.mber_sex);
            float weight = 0.0f;
            DBHelperWeight weightDb = helper.getWeightDb();
            DBHelperPPG ppgDB = helper.getPPGDb();

            DBHelperWeight.WeightStaticData bottomData = weightDb.getResultStatic();

            weight = StringUtil.getFloatVal(bottomData.getWeight());

            if (weight <= 0) {
                weight = StringUtil.getFloatVal(login.mber_bdwgh_app);

                if (weight <= 0) {
                    weight = StringUtil.getFloatVal(login.mber_bdwgh);
                }
            }

            float height = StringUtil.getFloatVal(login.mber_height);

            Goal_Step = StringUtil.getIntVal(mBaseFragment.getStepTargetCalulator(sex, height, weight, Goal_Cal));
        }


        float avgStepDistance = (StringUtil.getFloat(login.mber_height) - 100f); // 평균보폭
        if (getType() == TypeDataSet.Type.TYPE_CALORY) {    // 칼로리

            int totalActiveTime = (int) ((avgStepDistance * mTotalStep) * 1.8);
            int maxActiveTime = (int) ((avgStepDistance * mMaxStep) * 1.8);

            // 녹십자 헬스케어의 계산식을 사용한 칼로리계산
//                tvActiveValue.setText(StringUtil.getFormatPrice("" + mBaseFragment.getCalroriTargetCalulator(sex, height, weight, StringUtil.getIntVal("" + mTotalVal))));
//                Log.d(TAG, mTotalVal + "");


            // 구글피트의 칼로리 계산시을 사용한 칼로리계산
            tvActiveValue.setText(StringUtil.getFormatPrice("" +  mTotalVal));
            tvTargetValue.setText(StringUtil.getFormatPrice("" + Goal_Cal * dayDate));

            float value = ((float) StringUtil.getFloatVal(tvActiveValue.getText().toString()) / ((float) Goal_Cal * dayDate)) * 100;
            if (Float.isInfinite(value) || Float.isNaN(value)) {
                value = 0f;
            }

            if (value == 0.0f || Float.isNaN(value) || Float.isInfinite(value)) {
                tvCalorieBoxValue01.setText("-");
            } else {
                if (value == 0.0f || Float.isNaN(value) || Float.isInfinite(value)) {
                    tvCalorieBoxValue01.setText("-");
                } else {
                    tvCalorieBoxValue01.setText(StringUtil.getFormatPriceFloat(""+value));
                }
            }
            value = (totalActiveTime / 60f) * 0.01f;
            tvCalorieBoxValue02.setText(StringUtil.getFormatPriceFloat( ""+Math.abs((int) value)));   // 총활동시간
            tvCalorieBoxValue03.setText(calcClaorieCompare(mTotalVal.intValue()));

//            DBHelperPPG.PPGValue ppgvalue = null;
//            if (period == TypeDataSet.Period.PERIOD_DAY) {  // 최근
//                ppgvalue = ppgDB.getResultFastPPG(_startDate, _endDate);
//            }else{                                          // 주간, 월간
//                ppgvalue = ppgDB.getResultPPG(_startDate, _endDate);
//            }
//                if (ppgvalue.hrm.equals("0")){
//                    //tvStepBoxValue04.setText("");
//                    tvStepBoxValue02.setText("");
//                }else{
//                    //tvStepBoxValue04.setText(StringUtil.getIntVal(ppgvalue.hrm) + "회/분");   // 심박수 (구 최장연속활동시간)
//                    tvStepBoxValue02.setText("");
//                }

        } else if (getType() == TypeDataSet.Type.TYPE_STEP) {
            float totalMoveDistance = (avgStepDistance * mTotalVal) * 0.1f;
            Log.i(TAG, "avgStepDistince=" + avgStepDistance + ", mTotalVal=" + mTotalVal);
            tvActiveValue.setText(StringUtil.getFormatPrice("" + mTotalVal));
            tvTargetValue.setText(StringUtil.getFormatPrice("" + Goal_Step *dayDate));
            float value = ((float) mTotalVal / ((float) Goal_Step*dayDate)) * 100f;
            if (Float.isInfinite(value) || Float.isNaN(value)) {
                value = 0f;
            }

            if (value == 0.0f || Float.isNaN(value) || Float.isInfinite(value)) {
                tvStepBoxValue01.setText("-");
            } else {

                if (value == 0.0f || Float.isNaN(value) || Float.isInfinite(value)) {
                    tvStepBoxValue01.setText("-");
                } else {
                    tvStepBoxValue01.setText(StringUtil.getFormatPriceFloat(value+""));
                }
            }
            float dispTotalDistance = totalMoveDistance / 1000f;
            value = dispTotalDistance * 0.1f;
            tvStepBoxValue02.setText(StringUtil.getFormatPriceFloat(""+ Math.abs(value)));


//                tmp += 200;
//                dispTotalDistance = dispTotalDistance + (tmp);
//
//                CDialog.showDlg(mBaseFragment.getContext(), "dispTotalDistance:"+dispTotalDistance +", tmp:" +tmp);

            float savedMoney = calcSaveMoney(dispTotalDistance*100);

            //십단위 절삭
            savedMoney = (float) Math.floor((savedMoney / 100)) * 100;
            tvStepBoxValue03.setText(StringUtil.getFormatPrice("" + (int)savedMoney));
            float kmcm = StringUtil.getFloatVal(""+ value) * 100000;
            if(kmcm == 0.0f){
                tvStepBoxValue04.setText("0");
            }else{
                tvStepBoxValue04.setText(StringUtil.getFormatPriceFloat(""+(kmcm / mTotalVal)));
            }


        }
//        uploadGoogleFitStepData();
        setNextButtonVisible();
    }

//    /**
//     * 구글 걸음 데이터를 서버에 전송 및 Sqlite에 저장하기
//     * 일별 조회 일때만 저장하기
//     */
//    private void uploadGoogleFitStepData() {
//        if (mTimeClass.getPeriodType() == TypeDataSet.Period.PERIOD_DAY) {
//            int hour = 0;
//            List<BandModel> dataModelArr = new ArrayList<>();
//
//            for(int key : mGoogleFitDataMap.keySet() ){
//                hour = key;
//                int step = mGoogleFitDataMap.get(key);
//                System.out.println( String.format("key(hour)=%s, value=%s", key, step)+", mDbResultMap.get("+hour+")="+mDbResultMap.get(hour));
//
//                Calendar calendar = Calendar.getInstance();
//                calendar.setTimeInMillis(mTimeClass.getStartTime());
//
//                calendar.set(Calendar.HOUR, hour);
//                calendar.set(Calendar.MINUTE, 0);
//                calendar.set(Calendar.SECOND, 0);
//                calendar.set(Calendar.MILLISECOND, 0);
//
//                BandModel model = new BandModel();
//                model.setStep(step);
//                model.setRegtype("G");  // 구글 피트니스
//                model.setIdx(CDateUtil.getForamtyyMMddHHmmssSS(new Date(calendar.getTimeInMillis())));
//                model.setRegDate(CDateUtil.getForamtyyyyMMddHHmmss(new Date(calendar.getTimeInMillis())));
//
//                Log.i(TAG, "StepGoogle.regDate=" + model.getRegDate() + ", idx=" + model.getIdx() + ", step=" + model.getStep()
//                        +", mDbResultMap.get("+hour+") ="+mDbResultMap.get(hour) );
//
//                // Sqlite에서 조회 했던 결과가 없으면 서버저장 전문에 사용할 데이터와
//                // Sqlite에서에 저장할 데이터를 생성
//                if (mDbResultMap.get(hour) == null) {
//                    if (isToday() && getNowHour() == hour) {
//                        // 현재 시간은 저장하지 않음
//                    } else {
//                        dataModelArr.add(model);
//                    }
//                 }
//            }
//
//            if (dataModelArr.size() <= 0)
//                return;
//
//            DBHelper helper = new DBHelper(mContext);
//            DBHelperStep db = helper.getStepDb();
//            List<BandModel> newModelArr = db.getResultRegistData(dataModelArr);
//
//            if (newModelArr.size() > 0)
//                new DeviceDataUtil().uploadStepData(mBaseFragment, newModelArr);
//        }
//    }

    /*
        Save Money 계산 함수
     */
    private float calcSaveMoney(float distance) {
        if (distance == 0)
            return 0.0f;

//        distance = distance * 1000;

        final int initDist = 2000;
        final int initFee = 3800;
        final int unitDist = 132;
        final int unitDistFee = 100;

        if (distance <= initDist)
            return initFee;

        distance -= initDist;
        float tmp = (initFee + (distance / unitDist + 1) * unitDistFee);
        return tmp;
    }

    // 활동소모 칼로리비교 가져오기 : 삼계탕 0.0인분
    private String calcClaorieCompare(int calories) {
        if (calories == 0)
            return "-";

        Random random = new Random();
        int r = random.nextInt(mFoodCalories.size());

        FoodCalories target = mFoodCalories.get(r);

        String value = StringUtil.getFormatPriceFloat((calories / (float) target.calories)+"");

        if (value != null && value.endsWith(".0"))
            value = value.substring(0, value.length() - 2);

        String result = target.name + " " + value + " " + target.unit;
        return result;
    }

    // 활동소모 선언
    private ArrayList<FoodCalories> mFoodCalories;

    private class FoodCalories {
        FoodCalories(String name, String unit, int calories) {
            this.name = name;
            this.unit = unit;
            this.calories = calories;
        }

        String name, unit;
        int calories;
    }

//    /**
//     * 구글 fit API칼로리, 걸음수 최대 값을 가져온다.
//     *
//     * @param dataType
//     */
//    private void getTotalVal(GoogleApiClient mClient, DataType dataType) {
//        new GoogleFitInstance().totalValDataTask(mClient, dataType, new ApiData.IStep() {
//            @Override
//            public void next(Object obj) {
//                Long val = (long) obj;
//                tvActiveValue.setText(StringUtil.getFormatPrice("" + val));
//            }
//        });
//    }

    private int mMaxStep = 0;
    private int mTotalStep = 0;
    /**
     * 조회된 구글 피트니스 데이터 데이터 형태로 파싱하기
     * @param buckets
     */
    private int mXIndex = 0;
    public void readData(List<Bucket> buckets) {
        mTotalVal = 0L;

        mMaxStep = 0;
        mTotalStep = 0;

        DBHelper helper = new DBHelper(mContext);
        DBHelperCalorie calDB = helper.getCalorieDb();

        mGoogleFitDataMap.clear();
        mXIndex = 0;
        if (buckets.size() > 0) {
            Log.i(TAG, "BucketSize=" + buckets.size());
            for (Bucket bucket : buckets) {
                List<DataSet> dataSets = bucket.getDataSets();
                for (DataSet dataSet : dataSets) {
                    dumpDataSet(dataSet);
                }
            }
        }

//        return mYVals;
    }

    /**
     * 구글 피트니스 데이터 복잡한 데이터 타입에서 필요한 데이터만 뽑아서 차트 및 데이터에 사용할 데이터 추출
     * @param dataSet
     * @return
     */
    private DBHelper helper;
    private void dumpDataSet(DataSet dataSet) {
        Log.i(TAG, "====================");
        DateFormat dateFormat = getDateTimeInstance();

        long dumpTotalVal = 0;   // 총 칼로리, 총 걸음수

        Tr_login login = UserInfo.getLoginInfo();
        int sex = StringUtil.getIntVal(login.mber_sex);
        float height = StringUtil.getIntVal(login.mber_height);
        float weight = 0.0f;
        if (helper == null)
            helper = new DBHelper(mBaseFragment.getContext());
        DBHelperWeight weightDb = helper.getWeightDb();
        DBHelperWeight.WeightStaticData bottomData = weightDb.getResultStatic();
        if (!bottomData.getWeight().isEmpty()) {
            weight = StringUtil.getFloatVal(login.mber_bdwgh_app);
        } else {
            weight = StringUtil.getFloatVal(login.mber_bdwgh);
        }

        for (DataPoint dp : dataSet.getDataPoints()) {
//            Log.i(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(MILLISECONDS)) +" ~ "+dateFormat.format(dp.getEndTime(MILLISECONDS)) +"::");
//            Log.i(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(MILLISECONDS)));
            for (Field field : dp.getDataType().getFields()) {
//                Log.i(TAG, "\t Value:" + field.getName() + "=" + dp.getValue(field) );
                Log.i(TAG, "\tdumpDataSet Start: " + dateFormat.format(dp.getStartTime(MILLISECONDS)) +" ~ "+dateFormat.format(dp.getEndTime(MILLISECONDS)) +"::"+dp.getValue(field) );
                if (getType() == TypeDataSet.Type.TYPE_CALORY) {

                    int value = 0;
                    float calorie = 0;
                    try {
                        value = dp.getValue(field).asInt(); // 걸음수
                        // 일반적인 계산식 (기존) - 구글피트계산방식.
                        // calorie = getMileToCalorie(value, height, weight);

                        // 녹십자 헬스케어의 계산식을 사용한 칼로리계산
                        calorie = StringUtil.getFloatVal(mBaseFragment.getCalroriTargetCalulator(sex, height, weight, value));

                        mMaxStep = mMaxStep < value ? value : mMaxStep; // 최대 걸음수
                        mTotalStep += value;                       // 걸음수 총 합
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mGoogleFitDataMap.put(mXIndex, calorie);
                } else if (getType() == TypeDataSet.Type.TYPE_STEP) {
                    int steps = 0;
                    try {
                        steps += StringUtil.getFloat(dp.getValue(field).toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mGoogleFitDataMap.put(mXIndex, (float) steps);
                }
            }
        }
        Log.i(TAG, "mDumpValue=" + dumpTotalVal);
        mXIndex++;

//        return dumpTotalVal;
    }


//    /**
//     * 조회된 구글 피트니스 데이터 데이터 형태로 파싱하기
//     * @param dataReadResult
//     */
//    public void readData(DataReadResult dataReadResult) {
//        mArrIdx = 0;
//        mTotalVal = 0L;
//
//        mMaxStep = 0;
//        mTotalStep = 0;
//        if (dataReadResult.getBuckets().size() > 0) {
//            Log.i(TAG, "BucketSize=" + dataReadResult.getBuckets().size());
//            for (Bucket bucket : dataReadResult.getBuckets()) {
//                List<DataSet> dataSets = bucket.getDataSets();
//                for (DataSet dataSet : dataSets) {
//                    mTotalVal += dumpDataSet(dataSet);
//                }
//            }
//            TypeDataSet.Period period = mTimeClass.getPeriodType();
//            // 주간 조회시 남은 주간 채워주기
//            int max = 10;
//            if (period == TypeDataSet.Period.PERIOD_WEEK) {
//                max = 7 - (mYVals.size());
//            } else if (period == TypeDataSet.Period.PERIOD_DAY) {
//                max = 24 - (mYVals.size());
//            } else if (period == TypeDataSet.Period.PERIOD_MONTH) {
//                max = mTimeClass.getDayOfMonth() - (mYVals.size());
//            }
//
//            for (int i = 0; i < max; i++) {
//                mYVals.add(new BarEntry(mArrIdx++, 0f));
//                Log.i(TAG, "PERIOD.size=" + mYVals.size() + ", idx=" + mArrIdx);
//            }
//
//        } else if (dataReadResult.getDataSets().size() > 0) {
//            Log.i(TAG, "DataSetsSize=" + dataReadResult.getBuckets().size());
//            for (DataSet dataSet : dataReadResult.getDataSets()) {
//                mTotalVal += dumpDataSet(dataSet);
//            }
//        }
//    }

//    /**
//     * 구글 피트니스 데이터 복잡한 데이터 타입에서 필요한 데이터만 뽑아서 차트 및 데이터에 사용할 데이터 추출
//     * @param dataSet
//     * @return
//     */
//    private DBHelper helper;
//    private long dumpDataSet(DataSet dataSet, List<BarEntry> sportList) {
//        Log.i(TAG, "====================");
//
//        DateFormat dateFormat = getDateTimeInstance();
//
//        long dumpTotalVal = 0;   // 총 칼로리, 총 걸음수
//
//        if (getType() == TypeDataSet.Type.TYPE_CALORY) {
//            float yVal = sportList.get(mArrIdx).getY();
//            mYVals.add(new BarEntry(mArrIdx, new float[]{0, yVal}));   //,null));
//            mYVals.get(mArrIdx).setY(yVal);
//        } else {
//            mYVals.add(new BarEntry(mArrIdx, new float[]{0, 0}));   //,null));
//        }
//
//        Tr_login login = UserInfo.getLoginInfo();
//        int sex = StringUtil.getIntVal(login.mber_sex);
//        float height = StringUtil.getIntVal(login.mber_height);
//        float weight = 0.0f;
//        if (helper == null)
//            helper = new DBHelper(mBaseFragment.getContext());
//        DBHelperWeight weightDb = helper.getWeightDb();
//        DBHelperWeight.WeightStaticData bottomData = weightDb.getResultStatic();
//        if (!bottomData.getWeight().isEmpty()) {
//            weight = StringUtil.getFloatVal(login.mber_bdwgh_app);
//        } else {
//            weight = StringUtil.getFloatVal(login.mber_bdwgh);
//        }
//
//        for (DataPoint dp : dataSet.getDataPoints()) {
//            Log.i(TAG, "\tStart["+mArrIdx+"]: " + dateFormat.format(dp.getStartTime(MILLISECONDS)));
//            Log.i(TAG, "\tEnd["+mArrIdx+"]: " + dateFormat.format(dp.getEndTime(MILLISECONDS)));
//            for (Field field : dp.getDataType().getFields()) {
//                Log.i(TAG, "\t Value["+mArrIdx+"]:" + field.getName() + "=" + dp.getValue(field) + "[" + mArrIdx + "]");
//                // sqlite에 저장 여부를 판단할 용도로 사용할 Map
//                mGoogleFitDataMap.put(mArrIdx, (int) StringUtil.getFloat(dp.getValue(field).toString()));
//
//                if (getType() == TypeDataSet.Type.TYPE_CALORY) {
//
//                    int value = 0;
//                    float calorie = 0;
//                    try {
//                        value = dp.getValue(field).asInt(); // 걸음수
//                        // 일반적인 계산식 (기존) - 구글피트계산방식.
//                        // calorie = getMileToCalorie(value, height, weight);
//
//                        // 녹십자 헬스케어의 계산식을 사용한 칼로리계산
//                        calorie = StringUtil.getFloatVal(mBaseFragment.getCalroriTargetCalulator(sex, height, weight, value));
//
//                        mMaxStep = mMaxStep < value ? value : mMaxStep; // 최대 걸음수
//                        mTotalStep += value;                       // 걸음수 총 합
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    float sportCalory = sportList.get(mArrIdx).getY();
//                    mYVals.remove(mArrIdx);
//                    BarEntry entry = new BarEntry(
//                            mArrIdx,
//                            new float[]{calorie, sportCalory});
//                    mYVals.add(mArrIdx, entry);
//                    mYVals.get(mArrIdx).setY(calorie + sportCalory);
//                    Log.i(TAG, "calorie[" + mArrIdx + "]" + calorie+", sportCalory="+sportCalory);
//
//                    dumpTotalVal += (calorie + sportCalory);
//                } else if (getType() == TypeDataSet.Type.TYPE_STEP) {
//                    int steps = 0;
//                    try {
//                        steps += StringUtil.getFloat(dp.getValue(field).toString());
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    mYVals.remove(mArrIdx);
//                    BarEntry entry = new BarEntry(
//                            mArrIdx,
//                            new float[]{steps, 0});
//                    mYVals.add(mArrIdx, entry);
//                    mYVals.get(mArrIdx).setY(steps);
//                    dumpTotalVal += steps;
//                }
//
//
//            }
//
//        }
//        Log.i(TAG, "mDumpValue=" + dumpTotalVal);
//        mArrIdx++;
//
//        return dumpTotalVal;
//    }

    /**
     * 마일당 칼로리 구하기
     *
     * @param stepCnt
     * @param height
     * @param weight
     * @return
     */
    private float getMileToCalorie(int stepCnt, float height, float weight) {

        float moveDistance = (height - 100) * stepCnt / 100; // (키-100 * 걸음수) / 100
        float mc = (float) (3.7103f + weight + (0.0359f * (weight * 60 * 0.0006213) * 2) * weight);
        float calorie = (float) ((moveDistance * mc) * 0.006213);
        return calorie;
    }

    /**
     * 칼로리, 걸음 여부 판단
     *
     * @return
     */
    public TypeDataSet.Type getType() {
        if (mRadio_btn_calory.isSelected() == true) {
            mStepLayout.setVisibility(View.GONE);
            mCarorieLayout.setVisibility(View.VISIBLE);
            mLabelayout.setVisibility(View.VISIBLE);
            mRadio_btn_calory_tv.setSelected(true);
            mRadio_btn_calory.setSelected(true);
            mRadio_btn_step_tv.setSelected(false);
            mRadio_btn_step.setSelected(false);
            return TypeDataSet.Type.TYPE_CALORY;
        } else if (mRadio_btn_step.isSelected() == true) {
            mCarorieLayout.setVisibility(View.GONE);
            mStepLayout.setVisibility(View.VISIBLE);
            mLabelayout.setVisibility(mIslandscape? View.INVISIBLE : View.GONE);
            mRadio_btn_calory_tv.setSelected(false);
            mRadio_btn_calory.setSelected(false);
            mRadio_btn_step_tv.setSelected(true);
            mRadio_btn_step.setSelected(true);
            return TypeDataSet.Type.TYPE_STEP;
        }
        return TypeDataSet.Type.TYPE_CALORY;
    }

    public void setLabelLayoutVisible(boolean islandscape) {
        if (mRadio_btn_calory.isSelected() == true) {
            mLabelayout.setVisibility(View.VISIBLE);
        } else if (mRadio_btn_step.isSelected() == true) {
            mLabelayout.setVisibility(islandscape? View.INVISIBLE : View.GONE);
        }
    }


    protected void onResume() {
        getData();
//        mBaseFragment.permissionRequest(permissions, 123, new IPermissionResult() {
//            @Override
//            public void onPermissionResult(int requestCode, boolean isGranted, List<String> deniedPermissionList) {
//                if (isGranted) {
//                    getData();
//                } else {
//                    mBaseFragment.hideProgress();
//                    mBaseFragment.onBackPressed();
//                }
//            }
//        });



    }

    public void onStop() {

//        Log.i(TAG, "onStop.mClient=" + mClient);
//        GoogleFitInstance.getInstance().stopClient(mBaseFragment);
//        mClient = null;
//        if (mClient != null) {
//            Log.i(TAG, "onDetach.mClient=" + mClient + ", isConnected()=" + mClient.isConnected());
//            mClient.stopAutoManage(mBaseFragment.getActivity());
//            mClient.disconnect();
//            mClient = null;
//        }
    }



    public void onDetach() {
//        Log.i(TAG, "onDetach.mClient=" + mClient);
//        if (mClient != null) {
//            Log.i(
// \TAG, "onDetach.mClient=" + mClient + ", isConnected()=" + mClient.isConnected());
//            mClient.stopAutoManage(mBaseFragment.getActivity());
//            mClient.disconnect();
//            mClient = null;
//        }
    }

    /**
     * 칼로리,걸음 하단 화면 초기화
     */
    private void setBottomField() {
        if (getType() == TypeDataSet.Type.TYPE_CALORY) {    // 칼로리
            tvActiveTitle.setText("소모 칼로리");

            tvStepBoxValue01.setText("0%");
            tvActiveValueTag.setText(" Kcal");
            tvTargetValueTag.setText(" Kcal");

            chartunit.setText("kcal");
        } else if (getType() == TypeDataSet.Type.TYPE_STEP) {
            tvActiveTitle.setText("총 걸음 수");

            tvStepBoxValue01.setText("0%");
            tvActiveValueTag.setText(" Steps");
            tvTargetValueTag.setText(" Steps");

            chartunit.setText("steps");
        }
    }

    /**
     * 오늘인지 여부
     *
     * @return
     */
    private boolean isToday() {
        // 오늘일 경우
        return mTimeClass.getCalTime() == 0;
    }

    /**
     * 현재 시간을 구한다
     * @return
     */
    private int getNowHour() {
            Calendar nowCal = Calendar.getInstance();
            nowCal.setTimeInMillis(System.currentTimeMillis());

        return nowCal.get(Calendar.HOUR_OF_DAY);
    }
}
