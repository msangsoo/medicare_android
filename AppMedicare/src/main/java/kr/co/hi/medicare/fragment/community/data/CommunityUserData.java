package kr.co.hi.medicare.fragment.community.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CommunityUserData implements Serializable {
    @Expose
    @SerializedName("NICKNAME")
    public String NICKNAME;
    @Expose
    @SerializedName("MBER_SN")
    public String MBER_SN;
    @Expose
    @SerializedName("PROFILE_PIC")
    public String PROFILE_PIC;
    @Expose
    @SerializedName("MBER_GRAD")
    public String MBER_GRAD;
}
