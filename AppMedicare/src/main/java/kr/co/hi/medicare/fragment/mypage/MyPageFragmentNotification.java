package kr.co.hi.medicare.fragment.mypage;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.view.MotionEventCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import kr.co.hi.medicare.component.OnClickListener;

import com.loopeer.itemtouchhelperextension.Extension;
import com.loopeer.itemtouchhelperextension.ItemTouchHelperExtension;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.util.AlramUtil;


public class MyPageFragmentNotification extends BaseFragmentMedi {
    private final String TAG = getClass().getSimpleName();
    private DBHelper mDbHelper;
    private TextView btn_add_alram;
    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private MainRecyclerAdapter mainRecyclerAdapter;

    private ItemTouchHelperExtension mItemTouchHelper;
    private ItemTouchHelperExtension.Callback mCallback;

    public static BaseFragmentMedi newInstance() {
        MyPageFragmentNotification fragment = new MyPageFragmentNotification();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mypage_notification, container, false);

        btn_add_alram = view.findViewById(R.id.btn_add_alram);
        btn_add_alram.setOnClickListener(mClickListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, getContext());


        //마이페이지
        btn_add_alram.setOnTouchListener(ClickListener);


        //코드부여
        btn_add_alram.setContentDescription(getString(R.string.btn_add_alram));

        return view;
    }


    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Bundle bundle = new Bundle();
            switch (v.getId()) {
                case R.id.btn_add_alram:
                    NewActivity.startActivity(MyPageFragmentNotification.this, MyPageFragmentNotificationAdd.class,bundle);
                    break;

            }
        }
    };


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDbHelper = new DBHelper(getContext());
        recyclerView = view.findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
//        mainRecyclerAdapter = new MainRecyclerAdapter(getContext());
//        recyclerView.setAdapter(mainRecyclerAdapter);
//        recyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
//        mCallback = new ItemTouchHelperCallback();
//        mItemTouchHelper = new ItemTouchHelperExtension(mCallback);
//        mItemTouchHelper.attachToRecyclerView(recyclerView);
//        mainRecyclerAdapter.setItemTouchHelperExtension(mItemTouchHelper);
    }

    @Override
    public void onResume(){
        super.onResume();
        mainRecyclerAdapter = new MainRecyclerAdapter(getContext());
        recyclerView.setAdapter(mainRecyclerAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
        mCallback = new ItemTouchHelperCallback();
        mItemTouchHelper = new ItemTouchHelperExtension(mCallback);
        mItemTouchHelper.attachToRecyclerView(recyclerView);
        mainRecyclerAdapter.setItemTouchHelperExtension(mItemTouchHelper);
        mainRecyclerAdapter.setDatas(mDbHelper.getAlramHealth().getResultAll(mDbHelper));
    }

    private class MainRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<MyPageHealthNoticeData> mDatas;
        private Context mContext;
        private ItemTouchHelperExtension mItemTouchHelperExtension;

        public MainRecyclerAdapter(Context context) {
            mDatas = new ArrayList<>();
            mContext = context;
        }

        public void setDatas(List<MyPageHealthNoticeData> datas) {
            mDatas.clear();
            mDatas.addAll(datas);
            notifyDataSetChanged();
        }

        public MyPageHealthNoticeData getItem(int position){

            return mDatas.get(position);
        }

//        public void move(int from, int to) {
//            MyPageHealthNoticeData prev = mDatas.remove(from);
//            mDatas.add(to > from ? to - 1 : to, prev);
//            notifyItemMoved(from, to);
//        }

        public void updateData(List<MyPageHealthNoticeData> datas) {
            setDatas(datas);
            notifyDataSetChanged();
        }

        public void setItemTouchHelperExtension(ItemTouchHelperExtension itemTouchHelperExtension) {
            mItemTouchHelperExtension = itemTouchHelperExtension;
        }

        private LayoutInflater getLayoutInflater() {
            return LayoutInflater.from(mContext);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.medi_item_mypage_healthalram, parent, false);
            return new ItemSwipeWithActionWidthNoSpringViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
            ItemBaseViewHolder baseViewHolder = (ItemBaseViewHolder) holder;
            baseViewHolder.bind(mDatas.get(position),position);

           if (holder instanceof ItemSwipeWithActionWidthNoSpringViewHolder) {
               ItemSwipeWithActionWidthNoSpringViewHolder viewHolder = (ItemSwipeWithActionWidthNoSpringViewHolder) holder;
//                viewHolder.mActionViewRefresh.setOnClickListener(
//                        new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                Toast.makeText(mContext, "Refresh Click" + holder.getAdapterPosition()
//                                        , Toast.LENGTH_SHORT).show();
//                                mItemTouchHelperExtension.closeOpened();
//                            }
//                        }
//
//                );
               viewHolder.behind.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                doDelete(holder.getAdapterPosition());
                            }
                        }

                );
            }
        }

        private void doDelete(int position) {
            int idx = getItem(position).idx;

            if(mDbHelper.getAlramHealth().deleteDb(idx)){
                AlramUtil.releaseAlarm(getContext(),idx);
                mDatas.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position,getItemCount());
            }
        }


        private void update(int position,String isActive){

            int idx = getItem(position).idx;

            if(mDbHelper.getAlramHealth().update(isActive,idx)){
                mDatas.get(position).isactive =isActive;
            }

            notifyItemChanged(position);
        }


//        @Override
//        public int getItemViewType(int position) {
//                return ITEM_TYPE_ACTION_WIDTH_NO_SPRING;
//        }

        @Override
        public int getItemCount() {
            return mDatas.size();
        }

        public void setEnableDelteBtn(int position) {

            for(int i=0; i<getItemCount();i++){
                if(i==position)
                    mDatas.get(i).isDelete=true;
                else
                    mDatas.get(i).isDelete=false;
            }

            notifyDataSetChanged();

        }

        class ItemBaseViewHolder extends RecyclerView.ViewHolder {
            TextView btn_swipe,time_hhmm,gubun;
            ImageView image;
            CheckBox isactive;
            LinearLayout behind,front;

//            TextView mTextTitle;
//            TextView mTextIndex;
//            View mViewContent;
//            View mActionContainer;

            public ItemBaseViewHolder(View itemView) {
                super(itemView);
                btn_swipe = itemView.findViewById(R.id.btn_swipe);
                time_hhmm = itemView.findViewById(R.id.time_hhmm);
                image = itemView.findViewById(R.id.image);
                gubun = itemView.findViewById(R.id.gubun);
                isactive = itemView.findViewById(R.id.isactive);
                behind = itemView.findViewById(R.id.behind);
                front = itemView.findViewById(R.id.front);
            }

            public void bind(MyPageHealthNoticeData data,int position) {

                time_hhmm.setText(data.time_hhmm);
                gubun.setText(data.gubun);

                if(data.isactive.equals("Y")){
                    isactive.setChecked(true);
                }else{
                    isactive.setChecked(false);
                }

                switch (data.gubun){
                    case "식사":
                        image.setImageResource(R.drawable.ico_16);
                        break;
                    case "체중":
                        image.setImageResource(R.drawable.ico_17);
                        break;
                    case "혈압":
                        image.setImageResource(R.drawable.ico_18);
                        break;
                    default:
                        image.setImageResource(R.drawable.ico_19);
                        break;
                }


                isactive.setTag(R.id.mypage_alram_isactive,position);
                isactive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int positon = Integer.parseInt(v.getTag(R.id.mypage_alram_isactive).toString());
                        update(  positon   ,((CheckBox)v).isChecked() ? "Y" : "N" );
                    }
                });

                itemView.setTag(R.id.isdel,position);
                itemView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                            mItemTouchHelperExtension.startDrag(ItemBaseViewHolder.this);
                        }
                        return true;
                    }
                });

                behind.setTag(R.id.isdel, position);
                behind.setEnabled(data.isDelete);

                if(data.isDelete){
                    btn_swipe.setBackgroundResource(R.drawable.arrow_right_04);
                }else{
                    btn_swipe.setBackgroundResource(R.drawable.btn_12);
                }
            }
        }



        class ItemSwipeWithActionWidthViewHolder extends ItemBaseViewHolder implements Extension {


            public ItemSwipeWithActionWidthViewHolder(View itemView) {
                super(itemView);
            }

            @Override
            public float getActionWidth() {
                return behind.getWidth();
            }
        }

        class ItemSwipeWithActionWidthNoSpringViewHolder extends ItemSwipeWithActionWidthViewHolder implements Extension {


            public ItemSwipeWithActionWidthNoSpringViewHolder(View itemView) {
                super(itemView);

            }

            @Override
            public float getActionWidth() {
                return behind.getWidth();
            }
        }


    }





    public class ItemTouchHelperCallback extends ItemTouchHelperExtension.Callback {

        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return makeMovementFlags(0, ItemTouchHelper.START);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
//            MainRecyclerAdapter adapter = (MainRecyclerAdapter) recyclerView.getAdapter();
//            adapter.move(viewHolder.getAdapterPosition(), target.getAdapterPosition());
            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            MainRecyclerAdapter.ItemBaseViewHolder holder = (MainRecyclerAdapter.ItemBaseViewHolder) viewHolder;
            mainRecyclerAdapter.setEnableDelteBtn(Integer.parseInt(holder.behind.getTag(R.id.isdel).toString()));
        }

        @Override
        public boolean isLongPressDragEnabled() {

            return true;
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            if (dY != 0 && dX == 0) super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            MainRecyclerAdapter.ItemBaseViewHolder holder = (MainRecyclerAdapter.ItemBaseViewHolder) viewHolder;
            if (viewHolder instanceof MainRecyclerAdapter.ItemSwipeWithActionWidthNoSpringViewHolder) {
                if (dX < -holder.behind.getWidth()) {
                    dX = -holder.behind.getWidth();
                }
                holder.front.setTranslationX(dX);
                return;
            }
//            if (viewHolder instanceof MainRecyclerAdapter.ItemBaseViewHolder) {
//                Log.v("testswipe","testswipe:onChildDraw2:"+dX+","+dY);
//                holder.front.setTranslationX(dX);
//            }
        }

        @Override
        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            if(viewHolder!=null){

                MainRecyclerAdapter.ItemBaseViewHolder holder = (MainRecyclerAdapter.ItemBaseViewHolder) viewHolder;
                holder.btn_swipe.setBackgroundResource(R.drawable.btn_12);
                holder.behind.setEnabled(false);
            }
            super.clearView(recyclerView,viewHolder);
        }

    }





    public class DividerItemDecoration extends RecyclerView.ItemDecoration {
        private static final int DEFAULT_DIVIDER_HEIGHT = 1;

        public static final int HORIZONTAL_LIST = LinearLayoutManager.HORIZONTAL;

        public static final int VERTICAL_LIST = LinearLayoutManager.VERTICAL;

        protected int mOrientation;
        protected int padding;
        protected int startpadding;
        protected int endpadding;
        protected int dividerHeight;
        protected Context mContext;
        protected Paint mPaddingPaint;
        protected Paint mDividerPaint;

        public DividerItemDecoration(Context context) {
            this(context, VERTICAL_LIST, -1, -1);
        }

        public DividerItemDecoration(Context context, int orientation) {
            this(context, orientation, -1, -1);
        }

        public DividerItemDecoration(Context context, int orientation, int padding, int dividerHeight) {
            setOrientation(orientation);
            mContext = context;

            init();
            if (padding != -1) this.padding = padding;
            updatePaddint();
            if (dividerHeight != -1) this.dividerHeight = dividerHeight;
        }

        public DividerItemDecoration(Context context, int orientation, int startpadding, int endpadding, int dividerHeight) {
            setOrientation(orientation);
            mContext = context;

            init();
            if (startpadding != -1) this.startpadding = startpadding;
            if (endpadding != -1) this.endpadding = endpadding;
            if (dividerHeight != -1) this.dividerHeight = dividerHeight;
        }

        private void updatePaddint() {
            startpadding = padding;
            endpadding = padding;
        }

        private void init() {
            padding = mContext.getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin);
            updatePaddint();
            dividerHeight = DEFAULT_DIVIDER_HEIGHT;

            mPaddingPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mPaddingPaint.setColor(ContextCompat.getColor(mContext, android.R.color.white));
            mPaddingPaint.setStyle(Paint.Style.FILL);

            mDividerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mDividerPaint.setColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
            mDividerPaint.setStyle(Paint.Style.FILL);
        }

        public void setOrientation(int orientation) {
            if (orientation != HORIZONTAL_LIST && orientation != VERTICAL_LIST) {
                throw new IllegalArgumentException("invalid orientation");
            }
            mOrientation = orientation;
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            super.onDraw(c, parent, state);
            if (mOrientation == VERTICAL_LIST) {
                drawVertical(c, parent);
            } else {
                drawHorizontal(c, parent);
            }
        }

        public void drawVertical(Canvas c, RecyclerView parent) {
            final int left = parent.getPaddingLeft();
            final int right = parent.getWidth() - parent.getPaddingRight();

            final int childCount = parent.getChildCount();
            for (int i = 0; i < childCount - 1; i++) {
                final View child = parent.getChildAt(i);
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
                final int top = child.getBottom() + params.bottomMargin +
                        Math.round(ViewCompat.getTranslationY(child));
                final int bottom = top + dividerHeight;

                c.drawRect(left, top, left + startpadding, bottom, mPaddingPaint);
                c.drawRect(right - endpadding, top, right, bottom, mPaddingPaint);
                c.drawRect(left + startpadding, top, right - endpadding, bottom, mDividerPaint);
            }
        }

        public void drawHorizontal(Canvas c, RecyclerView parent) {
            final int top = parent.getPaddingTop();
            final int bottom = parent.getHeight() - parent.getPaddingBottom();

            final int childCount = parent.getChildCount();
            for (int i = 0; i < childCount - 1; i++) {
                final View child = parent.getChildAt(i);
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
                final int left = child.getRight() + params.rightMargin +
                        Math.round(ViewCompat.getTranslationX(child));
                final int right = left + dividerHeight;
                c.drawRect(left, top, right, top + startpadding, mPaddingPaint);
                c.drawRect(left, bottom - endpadding, right, bottom, mPaddingPaint);
                c.drawRect(left, top + startpadding, right, bottom - endpadding, mDividerPaint);
            }
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            if (mOrientation == VERTICAL_LIST) {
                if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
                    outRect.set(0, 0, 0, dividerHeight);
                } else {
                    outRect.set(0, 0, 0, 0);
                }
            } else {
                if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
                    outRect.set(0, 0, dividerHeight, 0);
                } else {
                    outRect.set(0, 0, 0, 0);
                }
            }

        }
    }



}