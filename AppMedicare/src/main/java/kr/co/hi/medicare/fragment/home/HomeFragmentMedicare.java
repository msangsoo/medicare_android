package kr.co.hi.medicare.fragment.home;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.firebase.iid.FirebaseInstanceId;
import kr.co.hi.medicare.net.hwNet.ApiData;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.home.alimi.NotifierFragment;
import kr.co.hi.medicare.fragment.home.healthinfo.HealthMainFragment;
import kr.co.hi.medicare.fragment.mypage.MyPointMainFragment;
import kr.co.hi.medicare.fragment.premium.PremiumMainFragment;
import kr.co.hi.medicare.googleFitness.HomeTodayGoogleFitData;
import kr.co.hi.medicare.googleFitness.noti.FitnessForeGroundService;
import kr.co.hi.medicare.net.MediNewNetWeather;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_getMsrstnAcctoRltmMesureDnsty;
import kr.co.hi.medicare.net.data.Tr_getNearbyMsrstnList;
import kr.co.hi.medicare.net.data.Tr_mvm_misson_goal_alert;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.net.hwdata.Tr_mber_main_call;
import kr.co.hi.medicare.sample.SampleFragmentMedi;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.permission.IPermissionResult;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.weather.Define;
import kr.co.hi.medicare.weather.DustManager;
import kr.co.hi.medicare.weather.WeatherVo;


/**
 *
 **/
public class HomeFragmentMedicare extends BaseFragmentMedi {

    public static final String PREF_KEY_STEP_MISSION_COMPLETE = "PREF_KEY_STEP_MISSION_COMPLETE";

    private LinearLayout mAlimiLayout;
    private TextView mAlimiTv;
    private TextView mTitle, mRightBtn, new_message;
    private RelativeLayout message_lv;

    private String ArraystationName[];
    private TextView mCityTextView;
    private TextView mTempTextView;
    private TextView mDustTextView;
    private TextView mTipTextView;

    private TextView mPointTv1;
    private TextView mPointTv2;
    private LinearLayout mQuizLayout;
    private TextView mKeywordTv1;
    private TextView mKeywordTv2;
    private TextView mQuizTextView;
    private TextView mCallListTv;
    private TextView mTestStepTv;
    private TextView[] mHealthInfoListTvs;
    private ImageView[] mMissionIvArr;
    private TextView[] mMissionTvArr;

    private LinearLayout mMiddleMenu3;

    private ImageView alimi_new_iv;
    private LinearLayout main_title_alimi;

    private String HEALTH_CALL_TYPE_1 = "1"; // 헬스콜센터
    private String HEALTH_CALL_TYPE_2 = "2"; // 담당하이플래너
    private String HEALTH_CALL_TYPE_3 = "3"; // 전담간호사
    private Tr_mber_main_call.Quiz_day quiz;

    private Map<String, Tr_mber_main_call.Health_call_list> mCallMap = new HashMap<>();
    public static String EXAM_DATA = "EXAM_DATA";

    public static Fragment newInstance() {
        HomeFragmentMedicare fragment = new HomeFragmentMedicare();
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    /**
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frament_home, container, false);
        mCityTextView = view.findViewById(R.id.main_city_textview);
        mTempTextView = view.findViewById(R.id.main_temp_textview);
        mDustTextView = view.findViewById(R.id.main_dust_textview);
        mKeywordTv1 = view.findViewById(R.id.home_keyword_cont_1);
        mKeywordTv2 = view.findViewById(R.id.home_keyword_cont_2);
        mQuizLayout = view.findViewById(R.id.main_middle_menu2);
        mQuizTextView = view.findViewById(R.id.home_quiz_textview);

        mTestStepTv = view.findViewById(R.id.home_test_step_tv);

        mTipTextView = view.findViewById(R.id.home_tip_textview);
        mTipTextView.setSelected(true);

        mPointTv1 = view.findViewById(R.id.home_point_tv_1);
        mPointTv2 = view.findViewById(R.id.home_point_tv_2);

        mMissionIvArr = new ImageView[] {
                view.findViewById(R.id.home_mission_iv_1)
                , view.findViewById(R.id.home_mission_iv_2)
                , view.findViewById(R.id.home_mission_iv_3)
                , view.findViewById(R.id.home_mission_iv_4)
                , view.findViewById(R.id.home_mission_iv_5)
                , view.findViewById(R.id.home_mission_iv_6)
                , view.findViewById(R.id.home_mission_iv_7) };

        mMissionTvArr = new TextView[]{
                view.findViewById(R.id.home_mission_tv_1)
                , view.findViewById(R.id.home_mission_tv_2)
                , view.findViewById(R.id.home_mission_tv_3)
                , view.findViewById(R.id.home_mission_tv_4)
                , view.findViewById(R.id.home_mission_tv_5)
                , view.findViewById(R.id.home_mission_tv_6)
                , view.findViewById(R.id.home_mission_tv_7) };
        mQuizLayout = view.findViewById(R.id.main_middle_menu2);    // 오늘의 퀴즈
        mCallListTv = view.findViewById(R.id.home_call_list_tv);    // 전화하기

        mHealthInfoListTvs = new TextView[]{ (TextView)view.findViewById(R.id.home_health_info_cont_tv)
                , (TextView)view.findViewById(R.id.home_health_info_cont_tv2)};
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (BuildConfig.DEBUG) {
            Log.i(TAG, "Firebase Token::"+FirebaseInstanceId.getInstance().getToken());
            view.findViewById(R.id.home_title_iv).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NewActivity.startActivity(getActivity(), SampleFragmentMedi.class, null);
                }
            });
        }
        FitnessForeGroundService.startForegroundService(getContext());

        Tr_login userInfo = UserInfo.getLoginInfo();
        ((TextView)view.findViewById(R.id.home_name_tv)).setText(userInfo.mber_nm);

        main_title_alimi = view.findViewById(R.id.main_title_alimi);

        view.findViewById(R.id.main_title_alimi).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.main_toolbar_premium).setOnClickListener(mOnClickListener);

        alimi_new_iv = view.findViewById(R.id.alimi_new_iv);

        Tr_login login = UserInfo.getLoginInfo();


        //TODO xxx 2차오픈으로 숨김
        if(BuildConfig.DEBUG){
            view.findViewById(R.id.main_toolbar_premium).setVisibility(View.VISIBLE);
        } else {
            if(login.mber_grad.equals("10")){
                if(login.pm_yn.equals("N") && login.st_yn.equals("N")){
                    view.findViewById(R.id.main_toolbar_premium).setVisibility(View.GONE);
                } else {
                    view.findViewById(R.id.main_toolbar_premium).setVisibility(View.VISIBLE);
                }
            } else {
                view.findViewById(R.id.main_toolbar_premium).setVisibility(View.GONE);
            }

        }

        mAlimiLayout = view.findViewById(R.id.home_alimi_balloon);
        mAlimiTv = view.findViewById(R.id.home_alimi_balloon_tv);
        mAlimiLayout.setOnClickListener(mOnClickListener);
        mMiddleMenu3 = view.findViewById(R.id.main_middle_menu3);

        view.findViewById(R.id.main_middle_menu1).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.main_middle_menu4).setOnClickListener(mOnClickListener);
        mMiddleMenu3.setOnClickListener(mOnClickListener);

        // 상단 타이틀 미세먼지 클릭시
        view.findViewById(R.id.main_dust_textview).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.main_dust_title_textview).setOnClickListener(mOnClickListener);

        view.findViewById(R.id.home_point_layout).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.home_point_icon).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.main_dayly_mission_text).setOnClickListener(mOnClickListener);


        //click 저장
        OnClickListener ClickListener = new OnClickListener(mOnClickListener, view, getContext());


        //홈
        view.findViewById(R.id.main_dayly_mission_text).setOnTouchListener(ClickListener);
        view.findViewById(R.id.home_point_layout).setOnTouchListener(ClickListener);
        view.findViewById(R.id.home_point_icon).setOnTouchListener(ClickListener);

        view.findViewById(R.id.main_middle_menu1).setOnTouchListener(ClickListener);
        mMiddleMenu3.setOnTouchListener(ClickListener);
        view.findViewById(R.id.main_middle_menu4).setOnTouchListener(ClickListener);

        view.findViewById(R.id.main_title_alimi).setOnTouchListener(ClickListener);
        mAlimiLayout.setOnTouchListener(ClickListener);

        //코드부여
        view.findViewById(R.id.main_dayly_mission_text).setContentDescription(getString(R.string.main_dayly_mission_text));
        view.findViewById(R.id.home_point_layout).setContentDescription(getString(R.string.home_point_layout));
        view.findViewById(R.id.home_point_icon).setContentDescription(getString(R.string.home_point_layout));

        view.findViewById(R.id.main_middle_menu1).setContentDescription(getString(R.string.main_middle_menu1));
        mMiddleMenu3.setContentDescription(getString(R.string.mMiddleMenu3));
        view.findViewById(R.id.main_middle_menu4).setContentDescription(getString(R.string.main_middle_menu4));

        view.findViewById(R.id.main_title_alimi).setContentDescription(getString(R.string.main_title_alimi));
        mAlimiLayout.setContentDescription(getString(R.string.main_title_alimi));



    }

    /**
     * 전화연결 다이얼로그 세팅 하기
     */
    private void showCallDialog() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_planer_call, null);
        setCallClickListener(view.findViewById(R.id.dlg_call_btn_tv1));
        setCallClickListener(view.findViewById(R.id.dlg_call_btn_tv2));
        setCallClickListener(view.findViewById(R.id.dlg_call_btn_tv3));

        final CDialog dlg = CDialog.showDlg(getContext(), view,true);
        view.findViewById(R.id.dlg_call_btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });
    }

    /**
     * 전화하기 다이얼로그 버튼 클릭 리스너
     * @param view
     */
    private void setCallClickListener(View view) {

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tr_mber_main_call.Health_call_list call;
                switch (v.getId()) {
                    case R.id.dlg_call_btn_tv1 :
                        call = mCallMap.get(HEALTH_CALL_TYPE_1);
                        if (TextUtils.isEmpty(call.call_hp)) {
                            CDialog dlg = CDialog.showDlg(getContext(), "현대해상 메디케어서비스 미 가입자는\n상담하기를 이용할 수 없습니다.\n\n서비스 가입 및 문의는\n1588-5656에서 가능합니다.")
                                    .setIconView(R.drawable.login_icon3);
                            dlg.setNoButton(getString(R.string.cancel), null);
                            dlg.setOkButton(getString(R.string.do_call), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    call("15885656");
                                }
                            });
                        } else
                            call(call.call_hp);
                        break;
                    case R.id.dlg_call_btn_tv2 :
                        call = mCallMap.get(HEALTH_CALL_TYPE_2);
                        if (TextUtils.isEmpty(call.call_hp)) {
                            CDialog dlg = CDialog.showDlg(getContext(), "하이플래너 정보가 없습니다.\n\n현대해상 고객센터\n1588-5656로 문의 바랍니다.");
                            dlg.setIconView(R.drawable.login_icon3);
                            dlg.setNoButton(getString(R.string.cancel), null);
                            dlg.setOkButton(getString(R.string.do_call), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    call("15885656");
                                }
                            });
                        } else
                            call(call.call_hp);
                        break;
                    case R.id.dlg_call_btn_tv3 :
                        call = mCallMap.get(HEALTH_CALL_TYPE_3);
                        if (TextUtils.isEmpty(call.call_hp )) {
                            CDialog.showDlg(getContext(), "전담간호사 정보가 없습니다.\n\n전담간호사는 암 진단 후\n고객님을 방문하는 간호사 입니다.")
                                    .setIconView(R.drawable.login_icon3);
                        } else
                            call(call.call_hp);
                        break;
                }
            }
        });
    }

    /**
     * 메인페이지데이터(건강점수, 포인트)트
     */

//    List<Tr_mber_main_call.Keyword_info_man_list> mKeywordListMan;
//    List<Tr_mber_main_call.Keyword_info_woman_list> mKeywordListWoman;
    private String day_health_amt = "";
    private ArrayList<Tr_mber_main_call.Mission_point_list> mission_point_list;
    private void getMainData() {
        final Tr_login login = UserInfo.getLoginInfo();
        Tr_mber_main_call.RequestData requestData = new Tr_mber_main_call.RequestData();
        final String toDay = CDateUtil.getToday_yyyy_MM_dd();
        requestData.mber_sn = login.mber_sn;
        requestData.input_de = toDay;

        MediNewNetworkModule.doApi(getContext(), Tr_mber_main_call.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_mber_main_call) {
                    final Tr_mber_main_call data = (Tr_mber_main_call) responseData;
//                    "day_health_amt": 오늘의 건강점수
//                    "user_point_amt": 내포인트

                    // 로그인 미션 지급 포인트 안내
                    if ("Y".equals(data.misson_goal_alert_yn)) {
                        CDialog.showDlg(getContext(), getString(R.string.login_welcome_dialog_message, data.misson_goal_point))
                                .setPointTvView(data.misson_goal_point+"P");

                    }

                    mPointTv1.setText(StringUtil.getFormatPrice(data.user_point_amt));  // 가용포인트
                    mPointTv2.setText(StringUtil.getFormatPrice(data.total_accml_amt)); // 누적포인트

//                    int idx = (point / 5) - 1;
//                    if(idx < 0){
//                        idx = 0;}
                    day_health_amt = data.user_point_amt;   // 오늘 달성 포인

                    // 알리미
                    if (data.alimi_list.size() > 0) {
                        Tr_mber_main_call.Alimi_list alimi = data.alimi_list.get(0);
                        setAlimiText(alimi.alimi_subject, alimi.notice_sn, "Y".equals(alimi.alimi_yn));

                    }

                    // 홈 상단 건강 메시지
                    if (data.day_tip_list.size() > 0) {
                        Tr_mber_main_call.Day_tip_list tip = data.day_tip_list.get(0);

//                        TranslateAnimation tanim = new TranslateAnimation(
//                                TranslateAnimation.ABSOLUTE, 1.0f * mTipTextView.getWidth(),
//                                TranslateAnimation.ABSOLUTE, -1.0f * mTipTextView.getWidth(),
//                                TranslateAnimation.ABSOLUTE, 0.0f,
//                                TranslateAnimation.ABSOLUTE, 0.0f);
//                        tanim.setDuration(16000);//set the duration
//                        tanim.setInterpolator(new LinearInterpolator());
//                        tanim.setRepeatCount(Animation.INFINITE);
//                        tanim.setRepeatMode(Animation.ABSOLUTE);
//                        mTipTextView.startAnimation(tanim);
                        mTipTextView.setText(tip.tip_nm);

                        if(tip.tip_nm!=null&&!tip.tip_nm.equals("")&&mTipTextView.getWidth()>0){
//                            Rect realSize = new Rect();
//                            mTipTextView.getPaint().getTextBounds(mTipTextView.getText().toString(), 0, mTipTextView.getText().length(), realSize);
                            float realSize = mTipTextView.getPaint().measureText(mTipTextView.getText().toString());

                            while (mTipTextView.getWidth()>=realSize&&realSize!=0){
                                mTipTextView.append("      ");
                                realSize = mTipTextView.getPaint().measureText(mTipTextView.getText().toString());
                            }
                        }
                    }

                    // 0. 일일미션 포인트
                    if (data.mission_point_list.size() > 0) {
                        mission_point_list = data.mission_point_list;
                        for (int i = 0; i < data.mission_point_list.size(); i++) {
                            Tr_mber_main_call.Mission_point_list mission = data.mission_point_list.get(i);
                            if (mMissionTvArr.length > i) {
                                mMissionTvArr[i].setText(mission.point_nm);
                                if ("Y".equals(mission.mission_yn))
                                    mMissionTvArr[i].setTextColor(ContextCompat.getColor(getContext(), R.color.color_fFCB540));
                                else
                                    mMissionTvArr[i].setTextColor(ContextCompat.getColor(getContext(), R.color.x143_144_158));

                                setDayMissionIcon(mission, mMissionIvArr[i]);
                            }
                        }
//                        for (Tr_mber_main_call.Mission_point_list missionPoint : data.mission_point_list) {
//                            switch (missionPoint.point_code) {
//                                case "1006" :   // 로그인
//                                    break;
//                                case "1007" :   // 건강기록
//                                    break;
//                                case "1008" :   // 일일퀴즈
//                                    break;
//                                case "1009" :   // 건강정보
//                                    break;
//                                case "1010" :   // 5,000보
//                                    break;
//                                case "1011" :   // 글쓰기
//                                    break;
//                                case "1012" :   // 댓글쓰기
//                                    break;
//                            }
//                    }
                    }


                    // 1. 상담 키워드
                    if (data.keyword_list != null) {
                        if (data.keyword_list.size() > 0) {
                            if (data.keyword_list.size() > 0) {
                                StringBuffer sb = new StringBuffer();
                                for (int i = 0; i < data.keyword_list.size(); i++) {
                                    Tr_mber_main_call.keyword_list keyword = data.keyword_list.get(i);
                                    int pos = i + 1;
                                    if (i == 0) {
                                        mKeywordTv1.setText(pos + ". " + keyword.keyword_nm);
                                    } else {
                                        sb.append(pos + ". " + keyword.keyword_nm);
                                        if (i != data.keyword_list.size() - 1)
                                            sb.append("\n");
                                    }
                                }
                                mKeywordTv2.setText(sb.toString());
                            }
                        }
                    }


                    // 2. 일일퀴즈
                    if (data.quiz_day != null) {
                        if (data.quiz_day.size() > 0) {
                            quiz = data.quiz_day.get(0);
                            mQuizTextView.setText(quiz.quiz_question);
                            mQuizLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // 구글피트니스 노티 바
//                FitnessForeGroundService.startForegroundService(getActivity());
                                    Bundle bundle = new Bundle();

                                    bundle.putString(QuizFragment.BUNDLE_KEY_QUIZ_REPLAY, quiz.quiz_replay);
                                    bundle.putString(QuizFragment.BUNDLE_KEY_QUIZ_EXPLAIN, quiz.quiz_explain);
                                    bundle.putString(QuizFragment.BUNDLE_KEY_QUIZ_QUESTION, quiz.quiz_question);
                                    bundle.putString(QuizFragment.BUNDLE_KEY_QUIZ_SN, quiz.quiz_sn);
                                    NewActivity.startActivity(getActivity(), QuizFragment.class, bundle);
                                }
                            });
                            //click 저장
                            OnClickListener ClickListener = new OnClickListener(null, mQuizLayout, getContext());

                            //홈
                            mQuizLayout.setOnTouchListener(ClickListener);

                            //코드부여
                            mQuizLayout.setContentDescription(getString(R.string.mQuizLayout));
                        }
                    }

                    // 3. 전화하기 리스트
                    List<Tr_mber_main_call.Health_call_list> callList = data.health_call_list;
                    if (callList.size() > 0) {
                        StringBuffer sb = new StringBuffer();
                        for (int i = 0; i < callList.size(); i++) {
                            Tr_mber_main_call.Health_call_list call = callList.get(i);
                            sb.append(call.call_nm);
                            if (i != callList.size() - 1)
                                sb.append("/");

                            mCallMap.put(call.call_typ, call);  // 전화하기 정보 저장
                        }
                        mCallListTv.setText(sb.toString());
                    }


//                    // 4. 건강정보 리스트
                    List<Tr_mber_main_call.Health_info_list> healthCallList = data.health_info_list;
                    if (healthCallList.size() > 0) {
                        StringBuffer sb = new StringBuffer();
                        for (int i = 0; i < healthCallList.size(); i++) {
                            Tr_mber_main_call.Health_info_list call = healthCallList.get(i);
                            if (i < mHealthInfoListTvs.length) {
                                mHealthInfoListTvs[i].setText(call.health_nm);
                            }
                        }
                    }

//                    int totalIdx = mProgressArrays.length();
//                    if (idx >= totalIdx)
//                        idx = totalIdx - 1;
//
//
//                    // 헬스정보 들어갔을 때 N
//
//                    health_info_new = SharedPref.getInstance().getPreferences(SharedPref.HEALTH_INFO_NEW);
//
//                    if (!"".equals(health_info_new) && health_info_new.length() > 0) {
//                        if (!StringUtil.getIntger(data.health_view_de).equals(StringUtil.getIntger(health_info_new))) {
//                            mHealthNewIcon.setVisibility(View.VISIBLE);
//                        } else {
//                            mHealthNewIcon.setVisibility(View.GONE);
//
//                        }
//                    } else {
//                        mHealthNewIcon.setVisibility(View.VISIBLE);
//                    }
//                    health_info_new = data.health_view_de;
//
//                    /*if (data.health_view_de.equals(toDay) == false) {
//                        mHealthNewIcon.setVisibility(View.VISIBLE);
//                    } else {
//                        mHealthNewIcon.setVisibility(View.GONE);
//                    }*/
//
//                    mProgressLayout.setBackgroundResource(mProgressArrays.getResourceId(idx, R.drawable._pe_05));
//                    Calendar cal = Calendar.getInstance();
//                    int hour = cal.get(Calendar.HOUR_OF_DAY);
//
//                    Log.i(TAG, "");
//                    if (hour >= 23 ) {
//                        mHealthMessageTv.setText("오늘 하루 힘드셨죠?\n안녕히주무세요.");
//                        return;
//                    } else if (hour < 4) {
//                        mHealthMessageTv.setText("오늘 하루 힘드셨죠?\n안녕히주무세요.");
//                        return;
//                    }
//
//                    Log.i(TAG, "mHealthMessageTv.point=" + point + ", hour=" + hour + "," + (hour <= 12 && hour > 23));
//                    if (hour >= 12 && hour < 23) {
//                        String message = "";
//                        if (point <= 30) {
//                            message = "건강기록은 관리의 시작!\n오늘의 건강상태를 기록해주세요.";
//                        } else if (point > 30 && point < 70) {
//                            message = "열심히 건강을 기록하셨네요.\n건강에 한발 더 가까워졌어요.";
//                        } else if (point >= 70 && point < 100) {
//                            message = "건강관리를 정말 잘하시네요!\n이제 100점을 목표로!";
//                        } else if (point >= 100) {
//                            message = "건강점수 100점 달성!\n축하드립니다.!!!";
//                        }
//                        Log.i(TAG, "mHealthMessageTv.point=" + point + ", resourceIdx=" + idx + ", message=" + message);
//                        mHealthMessageTv.setText(message);
//                    }
//
//                    String message = mHealthMessageTv.getText().toString();
//                    if (TextUtils.isEmpty(message) == false)
//                        kr.co.hi.medicare.value.Define.getInstance().setHomeHealthMessage(message);
//
//                    Log.i(TAG, "healthMessage set="+message);
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {

            }
        });
    }

    /**
     * 미션 포인트 아이콘 세팅
     * @param mission
     * @param iv
     */
    private void setDayMissionIcon(Tr_mber_main_call.Mission_point_list mission, ImageView iv) {
        int resId = 0;
        switch (mission.point_max_day_amt) {
            case "1" :
                if ("0".equals(mission.accml_amt))
                    resId = R.drawable.main_coin_1p_0;
                if ("1".equals(mission.accml_amt))
                    resId = R.drawable.main_coin_1p_1;
                break;
            case "2" :
                if ("0".equals(mission.accml_amt))
                    resId = R.drawable.main_coin_2p_0;
                if ("1".equals(mission.accml_amt))
                    resId = R.drawable.main_coin_2p_1;
                if ("2".equals(mission.accml_amt))
                    resId = R.drawable.main_coin_2p_2;
                break;
            case "3" :
                if ("0".equals(mission.accml_amt))
                    resId = R.drawable.main_coin_3p_0;
                if ("3".equals(mission.accml_amt))
                    resId = R.drawable.main_coin_3p_3;
                break;
            case "4" :
                if ("0".equals(mission.accml_amt))
                    resId = R.drawable.main_coin_4p_0;
                if ("2".equals(mission.accml_amt))
                    resId = R.drawable.main_coin_4p_2;
                if ("4".equals(mission.accml_amt))
                    resId = R.drawable.main_coin_4p_4;
                break;
            case "8" :
                if ("0".equals(mission.accml_amt))
                    resId = R.drawable.main_coin_8p_0;
                if ("4".equals(mission.accml_amt))
                    resId = R.drawable.main_coin_8p_4;
                if ("8".equals(mission.accml_amt))
                    resId = R.drawable.main_coin_8p_8;
                break;
        }

        Log.i(TAG, "setDayMissionIcon="+mission.point_nm+"("+mission.accml_amt+", "+mission.point_max_day_amt +") resId="+resId);

        if (resId != 0) {
            iv.setImageDrawable(ContextCompat.getDrawable(getContext(), resId));
            iv.setVisibility(View.VISIBLE);
        }


    }

    /**
     * 알리미 리스트 1개
     */
//    private void getAlimi() {
//        Tr_asstb_kbtg_alimi_view_on.RequestData requestData = new Tr_asstb_kbtg_alimi_view_on.RequestData();
//        requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;
//        requestData.pushk = "0";
//
//        MediNewNetworkModule.doApi(getContext(), Tr_asstb_kbtg_alimi_view_on.class, requestData, false, new MediNewNetworkHandler() {
//            @Override
//            public void onSuccess(BaseData responseData) {
//                Tr_asstb_kbtg_alimi_view_on recv = (Tr_asstb_kbtg_alimi_view_on) responseData;
//                List<Tr_asstb_kbtg_alimi_view_on.ChlmReadern> list = recv.chlmReadern;
//
//                if (list.size() > 0) {
//                    Tr_asstb_kbtg_alimi_view_on.ChlmReadern data = list.get(0);
//                    if (getActivity() instanceof MainActivityMedicare) {
//                        setAlimiText(data.sub_tit, data.kbta_idx);
//                    }
//                } else {
//                    Log.e(TAG, "알리미 정보 없음.");
//                }
//            }
//
//            @Override
//            public void onFailure(int statusCode, String response, Throwable error) {
//                Log.e(TAG, "알리미 정보 없음.");
//            }
//        });
//    }

    private LocationManager mLM;
    private void registerLocationUpdates() {
        String[] permissions = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
        permissionRequest(permissions, 0, new IPermissionResult() {
            @Override
            public void onPermissionResult(int requestCode, boolean isGranted, List<String> deniedPermissionList) {
                Log.i(TAG, "registerLocationUpdates() , isGranted="+isGranted);
                if (isGranted == false)
                    return;

                mLM = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

                mLM.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, mLocationListener);
                mLM.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, mLocationListener);
                //1000은 1초마다, 1은 1미터마다 해당 값을 갱신한다는 뜻으로, 딜레이마다 호출하기도 하지만
                //위치값을 판별하여 일정 미터단위 움직임이 발생 했을 때에도 리스너를 호출 할 수 있다.

                // GPS 정보 가져오기
                Boolean isGPSEnabled = mLM.isProviderEnabled(LocationManager.GPS_PROVIDER);
                // 현재 네트워크 상태 값 알아오기
                Boolean isNetworkEnabled = mLM.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                Log.i(TAG, "isGPSEnabled="+isGPSEnabled+", isNetworkEnabled="+isNetworkEnabled);
                if (isGPSEnabled && isNetworkEnabled) {
                    Log.i(TAG, "위치 켜져 있음.");

                } else {
                    Log.e(TAG, "위치 꺼져 있음.");
                    Boolean is_gps_message =  SharedPref.getInstance().getPreferences(CDateUtil.getToday_yyyy_MM_dd(),false);
                    if(!is_gps_message) {
                        CDialog.showDlg(getContext(), getString(R.string.gps_message), new CDialog.DismissListener() {
                            @Override
                            public void onDissmiss() {
                                SharedPref.getInstance().savePreferences(CDateUtil.getToday_yyyy_MM_dd(),true);
                            }
                        });
                    }
                }
            }
        });
    }

    /**
     * 위치정보 Listener
     * GPS, Network 에서 위치정보 가져온다
     */
    private int mLocationRetryCnt = 0;  // 위치정보 요청 시도 횟수
    private final LocationListener mLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            //여기서 위치값이 갱신되면 이벤트가 발생한다.
            //값은 Location 형태로 리턴되며 좌표 출력 방법은 다음과 같다.
            double longitude;
            double latitude;
            if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
                //Gps 위치제공자에 의한 위치변화. 오차범위가 좁다.
                longitude = location.getLongitude();    //경도
                latitude = location.getLatitude();         //위도
                float accuracy = location.getAccuracy();        //신뢰도
                Log.i(TAG, "위치정보 GPS_PROVIDER.longitude="+longitude+", latitude="+latitude+", accuracy="+accuracy);
            } else {
                //Network 위치제공자에 의한 위치변화
                //Network 위치는 Gps에 비해 정확도가 많이 떨어진다.
                longitude = location.getLongitude();    //경도
                latitude = location.getLatitude();         //위도
                float accuracy = location.getAccuracy();        //신뢰도
                Log.i(TAG, "위치정보 NETWORK.longitude="+longitude+", latitude="+latitude+", accuracy="+accuracy);
            }

            String cityName = getCurrentAddrss(latitude, longitude);

            Log.i(TAG, "위치정보.cityName="+cityName+", latitude="+latitude+", longitude="+longitude);
            if (TextUtils.isEmpty(cityName)) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // 위치 확인 실패시 10초 후 연결 재시도
                        Log.e(TAG, "위치정보 확인 실패시 10초 후 연결 재시도");

                        mLocationRetryCnt ++;
                        if (mLocationRetryCnt == 5) {
                            Log.e(TAG, "위치정보 확인 실패 5회 시도 재요청 안 함");
                            return;
                        }

//                        mHealthMessageTv.setText("오늘은 즐겁고 행복한 일만 가득하기를!!");
                        registerLocationUpdates();

                        WeatherVo weatherData = Define.getInstance().getWeatherData();
                        String savedCityName = weatherData.cityName;
                        if (TextUtils.isEmpty(savedCityName) == false)
                            getDustInfo(savedCityName);
                    }
                }, 10 * 1000);
            } else {
////                mZoneName = DustManager.getCityName(mZoneName);
                WeatherVo weatherData = Define.getInstance().getWeatherData();
                weatherData.cityName = cityName;

                getDustInfo(cityName);

                if (mLM != null)
                    mLM.removeUpdates(mLocationListener);    // 위치정보 가져오기 해제

                getDustStationList(latitude, longitude);
            }
        }

        public void onProviderDisabled(String provider) {
            Log.e(TAG, "LocationListener.onProviderDisabled()");
        }

        public void onProviderEnabled(String provider) {
            Log.e(TAG, "LocationListener.onProviderEnabled().provider="+provider);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "LocationListener.onProviderDisabled().provider="+provider);
        }
    };

    /**
     * 측정소 리스트 가져오기
     * @param longitude
     * @param latitude
     */
    private void getDustStationList(double latitude, double longitude) {
        int savedCal = Define.getInstance().getWeatherRequestedTime(); // 조회 했던 시간
        final Calendar nowCal = Calendar.getInstance();             //

        if (savedCal != -1) {
            int nowHour = nowCal.get(Calendar.HOUR_OF_DAY);

            Log.i(TAG, "getDustInfo.savedHour=" + savedCal + ", nowHour=" + nowHour);
            // 조회 했던 시간과 현재 시간이 동일하면 리턴
            if (savedCal == nowHour) {
//                startBluetoothListener(isInit);
                weather = Define.getInstance().getWeatherData();

                int dustLevel = Define.getInstance().getDustLevel();
                String dustStatus = DustManager.getDustStatusStr(dustLevel);
                mDustTextView.setText(dustStatus);
                weather.dust = dustStatus;

                String watherStr = "-";
                String tempStr = "-";
                String date = "-";
                // 날씨정보 못가져오는 경우 "-" 처리.
                if (TextUtils.isEmpty(SharedPref.getInstance().getPreferences(SharedPref.CITY_STATION)) == false) {
                    mCityTextView.setText(SharedPref.getInstance().getPreferences(SharedPref.CITY_STATION));
                }

                if (weather.weather != null && !weather.weather.equals("null"))
                    watherStr = weather.weather;
                if (weather.temp != null && !weather.temp.equals("null"))
                    tempStr = weather.temp;


                mTempTextView.setText( tempStr+ " °C");//+watherStr);

                return;
            }
        }

        Tr_getNearbyMsrstnList msrstnList = new Tr_getNearbyMsrstnList(longitude, latitude);
        MediNewNetWeather.doApi(getContext(), msrstnList, null, false, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                Tr_getNearbyMsrstnList recv = (Tr_getNearbyMsrstnList) responseData;
                List<Tr_getNearbyMsrstnList.VoList> list = recv.list;

                if (list.size() > 0) {
                    Log.i("", "가까운측정소명:"+list.get(0).stationName + "/" + list.size());
                    ArraystationName = new String[list.size()];
                    for(int i=0; i < list.size(); i++){
                        ArraystationName[i] = list.get(i).stationName;
                    }
                    //String stationName = list.get(0).stationName;
                    WeatherVo weatherData = Define.getInstance().getWeatherData();
                    doDust(ArraystationName[0], weatherData.weather, StringUtil.getIntVal(weatherData.temp));

                } else {
                    Log.e(TAG, "측정소 정보 없음.");
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                Log.e(TAG, "측정소 정보 없음.");
            }
        });
    }



    /**
     * 좌표로 지역 이름 가져오기
     * @param latitude
     * @param longitude
     * @return
     */
    private String getCurrentAddrss(double latitude, double longitude) {
//        if (BuildConfig.DEBUG) {
        // 경기도 성남시 좌표
//            latitude = 37.429396;
//            longitude = 127.131160;
//        }
        if (getContext() == null) {
            // 앱을 빠르게 취소 하고 재 시작 시 오류 발생 될 때 있음
            return "";
        }

        final Geocoder geocoder = new Geocoder(getContext(), Locale.KOREA);
        // 위도,경도 입력 후 변환 버튼 클릭
        String city = "";
        List<Address> list = null;
        try {
            list = geocoder.getFromLocation(
                    StringUtil.getFloat(String.format("%.3f",latitude)), // 위도
                    StringUtil.getFloat(String.format("%.3f",longitude)), // 경도
                    3); // 얻어올 값의 개수
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("test", "입출력 오류 - 서버에서 주소변환시 에러발생");
        }

        if (list != null) {
            if (list.size() == 0) {
//                tv.setText("해당되는 주소 정보는 없습니다");
                Log.e(TAG, "주소정보 가져오기 실패");
            } else {
//                tv.setText(list.get(0).toString());
//                city = list.get(0).getLocality();

                Address address = list.get(0);
                String[] data = list.get(0).toString().split(" ");

                Log.i(TAG, "위치정보 address="+address.toString());
                Log.i(TAG, "위치정보 getAddressLine="+address.getAdminArea());


                city =  DustManager.checkCity(data[1])+ " " + data[2];
                SharedPref.getInstance().savePreferences(SharedPref.CITY_STATION,data[2]);

            }
        }
        Log.i(TAG, "위치정보 city="+city);
        Log.i(TAG,"주소list ="+list);
        Log.i(TAG,"latitude : " + latitude+" longitude : "+longitude);
        if (weather != null)
            Log.i(TAG, "위치정보 weather.observationpoint="+weather.observationpoint);
        return city;
    }

    /**
     * 미세먼지, 날씨 데이터 가져오기
     */
    private void getDustInfo(final String cityName) {
//        Tr_login loginData = UserInfo.getLoginInfo();
//        mZoneName = DustManager.getCityName(loginData.mber_zone);//loginData.mber_zone
//        txtCityName.setText(mZoneName);

        int savedCal = Define.getInstance().getWeatherRequestedTime(); // 조회 했던 시간
        final Calendar nowCal = Calendar.getInstance();             //

        if (savedCal != -1) {
            int nowHour = nowCal.get(Calendar.HOUR_OF_DAY);

            Log.i(TAG, "getDustInfo.savedHour=" + savedCal + ", nowHour=" + nowHour);
            // 조회 했던 시간과 현재 시간이 동일하면 리턴
            if (savedCal == nowHour) {
//                startBluetoothListener(isInit);
                weather = Define.getInstance().getWeatherData();

                int dustLevel = Define.getInstance().getDustLevel();
                String dustStatus = DustManager.getDustStatusStr(dustLevel);
                mDustTextView.setText(dustStatus);
                weather.dust = dustStatus;

                String watherStr = "-";
                String tempStr = "-";
                String date = "-";
                // 날씨정보 못가져오는 경우 "-" 처리.
                if (TextUtils.isEmpty(SharedPref.getInstance().getPreferences(SharedPref.CITY_STATION)) == false) {
                    mCityTextView.setText(SharedPref.getInstance().getPreferences(SharedPref.CITY_STATION));
                }

                if (weather.weather != null && !weather.weather.equals("null"))
                    watherStr = weather.weather;
                if (weather.temp != null && !weather.temp.equals("null"))
                    tempStr = weather.temp;


                mTempTextView.setText(tempStr + " °C");//+ watherStr);

                return;
            }
        }


        new DustManager().getWeather(getContext(), cityName, new DustManager.IResult() {
            @Override
            public void result(kr.co.hi.medicare.weather.WeatherVo weather) {
                Log.d(TAG, "MsnWeatherData.mZoneName="+cityName+", weather:" + weather.weather + ", temp=" + weather.temp +" SharedPref.getInstance().getPreferences(SharedPref.CITY_STATION)="+SharedPref.getInstance().getPreferences(SharedPref.CITY_STATION));
                if (weather != null) {
                    if(TextUtils.isEmpty(SharedPref.getInstance().getPreferences(SharedPref.CITY_STATION)) == false) {
                        mCityTextView.setText(SharedPref.getInstance().getPreferences(SharedPref.CITY_STATION));
                    }


                    Define.getInstance().setWeatherData(weather);
                    int temp = StringUtil.getIntVal(weather.temp);
                    // 온도
//                    mTempTextView.setText(temp + "° " +weather.weather);
                    mTempTextView.setText(temp + " °C");
                    //미세먼지
                    //doDust(cityName, weather.weather, temp);
                }

                Define.getInstance().setWeatherRequestedTime(nowCal.get(Calendar.HOUR_OF_DAY));

            }
        });
    }

    /**
     * 미세먼지 정보 가져오기
     *
     //     * @param zoneName
     */
    int count = 0;
    private void doDust(final String stationName, final String weather, final int temp) {
        if (getContext() == null) {
            // 앱을 빠르게 취소 하고 재 시작 시 오류 발생 될 때 있음
            return;
        }

//        String zonCode = DustManager.getDustLocation(zoneName);

        Tr_getMsrstnAcctoRltmMesureDnsty dnsty = new Tr_getMsrstnAcctoRltmMesureDnsty(stationName);
        MediNewNetWeather.doApi(getContext(), dnsty, null, false, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                Tr_getMsrstnAcctoRltmMesureDnsty recv = (Tr_getMsrstnAcctoRltmMesureDnsty) responseData;

                Log.i(TAG, "xxxtotalCount="+recv.totalCount);

                List<Tr_getMsrstnAcctoRltmMesureDnsty.VoList> list = recv.list;
                if (list.size() > 0) {

                    Log.i("", "미세먼지농도:"+list.get(0).pm10Value);

                    count++;
                    if(("".equals(list.get(0).pm10Value) || "-".equals(list.get(0).pm10Value)) && count != ArraystationName.length){
                        WeatherVo weatherData = Define.getInstance().getWeatherData();
                        doDust(ArraystationName[count], weatherData.weather, StringUtil.getIntVal(weatherData.temp));

                        return;
                    }

                    int dustLevel = StringUtil.getIntValMinus("".equals(list.get(0).pm10Value) || "-".equals(list.get(0).pm10Value)? "-1":list.get(0).pm10Value);

                    Define.getInstance().setDustLevel(dustLevel);
                    String dust = DustManager.getDustStatusStr(dustLevel);
                    mDustTextView.setText(dust);

//                    setHomeHealthMessage(weather, temp, dust);
                } else {
                    Define.getInstance().setDustLevel(-1);
                    String dust = DustManager.getDustStatusStr(-1);
                    mDustTextView.setText(dust);
//                    setHomeHealthMessage(weather, temp, dust);
                    Log.e(TAG, "미세먼지 정보 없음");
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                Log.e(TAG, "미세먼지 정보 조회 실패 ");
            }
        });
    }

    private WeatherVo weather;
    private void resumeReqWeather() {
        // 날씨정보를 불러오는 것이 딜레이가 있어 저장 된 데이터를 먼저 보여줌
        weather = Define.getInstance().getWeatherData();
        if (weather != null && TextUtils.isEmpty(SharedPref.getInstance().getPreferences(SharedPref.CITY_STATION)) == false) {
            Log.i(TAG, "weather=" + weather + ", weather.observationpoint=" + weather.observationpoint);
//            mCityTextView.setText(SharedPref.getInstance().getPreferences(SharedPref.CITY_STATION));
//            mDustTextView.setText(weather.dust);
            mTempTextView.setText(weather.temp + " °C");//+weather.weather);
            String healthMessage = Define.getInstance().getHomeHealthMessage();
//            if (TextUtils.isEmpty(healthMessage) == false)
//                mHealthMessageTv.setText(healthMessage);
        }
    }

//    /**
//     * 전화걸기
//     * @param checkflag true = 하이플래너 , false = 담당간호사
//     */
//    private void newcall(boolean checkflag) {
//        String fphp = "", nuhp = "";
//
//        Tr_login userInfo = UserInfo.getLoginInfo();
//
//        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
//
//        if (checkflag){ // 하이플래너
//            if (user.getFphp() != null && !user.getFphp().equals("") && user.getFpnm() != null && !user.getFpnm().equals("")) {
//
//                if (user.getFphp().length() == 11){
//                    fphp = user.getFphp().substring(0,3) + "-" + user.getFphp().substring(3,7) + "-" + user.getFphp().substring(7,11);
//                }else {
//                    fphp = user.getFphp();
//                }
//                CLog.i("fphp --> " + fphp);
//
//                alert.setTitle(getString(R.string.call_planner2));
//                alert.setPositiveButton(getString(R.string.call), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        call(user.getFphp());
//                    }
//                });
//                alert.setMessage(getString(R.string.call_name) + " " + user.getFpnm() + "\n\n" + getString(R.string.call_number) + " " + fphp);
//                alert.show();
//            } else {
//                alert.setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//                alert.setMessage(getString(R.string.call_planner_no));
//                alert.show();
//            }
//
//        } else { //전담간호사
//            if (user.getCancerMember()){
//                if (user.getNuhp() != null && !user.getNuhp().equals("") && user.getNunm() != null && !user.getNunm().equals("")) {
//
//                    if (user.getNuhp().length() == 11){
//                        nuhp = user.getNuhp().substring(0,3) + "-" + user.getNuhp().substring(3,7) + "-" + user.getNuhp().substring(7,11);
//                    }else {
//                        nuhp = user.getNuhp();
//                    }
//
//                    CLog.i("nuhp --> " + nuhp);
//
//
//                    alert.setTitle(getString(R.string.call_nurse2));
//                    alert.setPositiveButton(getString(R.string.call), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            call(user.getNuhp());
//                        }
//                    });
//                    alert.setMessage(getString(R.string.call_name) + " " + user.getNunm() + "\n\n" + getString(R.string.call_number) + " " + nuhp);
//                    alert.show();
//                } else {
//                    alert.setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
//                        }
//                    });
//                    alert.setMessage(getString(R.string.call_nurse_no));
//                    alert.show();
//                }
//            }else {
//                alert.setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//                alert.setMessage(getString(R.string.call_nurse_no2));
//                alert.show();
//            }
//        }
//    }

    /**
     * 전화걸기
     **/
    private void call(String phone) {
        String phonenum = phone;
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phonenum));
        startActivity(intent);
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            Intent intent = new Intent(Intent.ACTION_DIAL);
//            intent.setData(Uri.parse("tel:" + phonenum));
//            startActivity(intent);
//        } else {
//            if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                // 권한 없음.
//                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
//                alert.setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        ActivityCompat.requestPermissions(getContext(), new String[]{
//                                Manifest.permission.CALL_PHONE
//                        }, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
//                        dialog.dismiss();
//                    }
//                });
//                alert.setMessage(getString(R.string.permission));
//                alert.show();
//            } else {
//                // 권한 있음.
//                Intent intent = new Intent(Intent.ACTION_CALL);
//                intent.setData(Uri.parse("tel:" + phonenum));
//                startActivity(intent);
//            }
//        }
    }


    //클릭 이벤트 정의
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int viewId = v.getId();
            Bundle bundle = new Bundle();
            int reqCode = 0;
            switch (viewId) {
                case R.id.main_dust_textview :
                case R.id.main_dust_title_textview :
                    CDialog.showDlg(getContext(), R.layout.popup_dust_info_dialog)
                            .setOkButton(null);
                    break;

                case R.id.home_point_layout :
                case R.id.home_point_icon :
                    NewActivity.startActivity(getActivity(), MyPointMainFragment.class, bundle);
                    break;

                case R.id.main_dayly_mission_text :
                    bundle.putString(MissionFragment.BUNDLE_KEY_TODAY_POINT, day_health_amt);
                    bundle.putParcelableArrayList(MissionFragment.BUNDLE_KEY_MISSION_LIST, mission_point_list);
                    bundle.putString(QuizFragment.BUNDLE_KEY_QUIZ_REPLAY, quiz.quiz_replay);
                    bundle.putString(QuizFragment.BUNDLE_KEY_QUIZ_EXPLAIN, quiz.quiz_explain);
                    bundle.putString(QuizFragment.BUNDLE_KEY_QUIZ_QUESTION, quiz.quiz_question);
                    bundle.putString(QuizFragment.BUNDLE_KEY_QUIZ_SN, quiz.quiz_sn);
                    reqCode = 333;
                    NewActivity.startActivityForResult(getActivity(), reqCode, MissionFragment.class, bundle);
                    break;
                case R.id.main_title_alimi:
                    SharedPref.getInstance().savePreferences(SharedPref.ALIMI_MESSAGE, v.getTag().toString());
                    NewActivity.startActivity(getActivity(), NotifierFragment.class,null);
                    break;
                case R.id.main_toolbar_premium:
                    NewActivity.startActivity(getActivity(), PremiumMainFragment.class, null);
                    break;
                case R.id.home_alimi_balloon :
                    SharedPref.getInstance().savePreferences(SharedPref.ALIMI_MESSAGE, v.getTag().toString());
                    v.setVisibility(View.GONE);
                    NewActivity.startActivity(getActivity(), NotifierFragment.class, null);
                    break;
                case R.id.main_middle_menu1:
                    reqCode = 111;
                    NewActivity.startActivityForResult(getActivity(), reqCode, KeywordMainFragment.class, bundle);
                    break;
                case R.id.main_middle_menu3:
                    Tr_login userInfo = UserInfo.getLoginInfo();
                    if ("10".equals(userInfo.mber_grad)) {
                        showCallDialog();
                    } else {
                        CDialog dlg = CDialog.showDlg(getContext(), "현대해상 메디케어서비스 미 가입자는\n상담하기를 이용할 수 없습니다.\n\n서비스 가입 및 문의는\n1588-5656에서 가능합니다.")
                                .setIconView(R.drawable.login_icon3);
                        dlg.setNoButton(getString(R.string.cancel), null);
                        dlg.setOkButton(getString(R.string.do_call), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                call("15885656");
                            }
                        });
                    }
                    break;
                case R.id.main_middle_menu4:
                    NewActivity.startActivity(getActivity(), HealthMainFragment.class, null);
                    break;

            }
        }
    };


    /** Finds available data sources and attempts to register on a specific {@link DataType}. */
//    private OnDataPointListener mListener;
//    private Task<Void> mSencsorClient;
//    private void registGoogleFitSensor() {
//        if (mSencsorClient != null) {
//            return;
//        }
//
//        GoogleSignInOptionsExtension fitnessOptions =
//                FitnessOptions.builder()
//                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
//                        .build();
//
//        GoogleSignInAccount gsa = GoogleSignIn.getAccountForExtension(getActivity(), fitnessOptions);
//        mSencsorClient = Fitness.getSensorsClient(getActivity(), gsa)
//                .add(new SensorRequest.Builder()
//                                .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
////                                .setFastestRate(1, TimeUnit.MILLISECONDS)  // sample once per minute
//                                .setSamplingRate(60, TimeUnit.SECONDS)  // sample once per minute
//                                .build(),
//                        googleFitStepCountListener);
//
//    }

    /**
     * 구글 센서API 리스너
     */
    OnDataPointListener googleFitStepCountListener = new OnDataPointListener() {
        @Override
        public void onDataPoint(DataPoint dataPoint) {
            for (Field field : dataPoint.getDataType().getFields()) {
                Value val = dataPoint.getValue(field);
                Log.i(TAG, "Detected DataPoint field: " + field.getName());
                Log.i(TAG, "Detected DataPoint value: " + val);
                mTodayStepTotal += val.asInt();

            }

            if (BuildConfig.DEBUG) {
                mTestStepTv.setText(StringUtil.getFormatPrice("" + mTodayStepTotal));

            }
            todayStepMissionAlert();
        }
    };

    /**
     * 구글 센서API 해지
     */
//    private void removeSensorClient() {
//        GoogleFitInstance.unregisterSensorFitnessDataListener(getContext(), googleFitStepCountListener);
//        mSencsorClient = null;
//    }

    /**
     * 알리미 세팅
     */
    public void setVisibleAlimiLayout(int visible) {
        mAlimiLayout.setVisibility(visible);
    }

    /**
     *
     * @param str       알리미제목
     * @param notice_sn 알리미 고유키값
     */
    public void setAlimiText(String str, String notice_sn, boolean isShow) {
        mAlimiTv.setText(str);
        mAlimiTv.setTag(notice_sn);
        mAlimiLayout.setTag(notice_sn);
        main_title_alimi.setTag(notice_sn);
        if (!isShow) {
            mAlimiLayout.setVisibility(View.GONE);
            alimi_new_iv.setImageResource(R.drawable.commu_btn_3);
        } else {
            String alimiSn = SharedPref.getInstance().getPreferences(SharedPref.ALIMI_MESSAGE);
            mAlimiLayout.setVisibility(alimiSn.equals(notice_sn) ? View.GONE : View.VISIBLE);
            alimi_new_iv.setImageResource(alimiSn.equals(notice_sn) ? R.drawable.commu_btn_3 : R.drawable.commu_btn_4);
        }

    }


    /**
     * 오늘 걸음 미션 확인
     */
    private int mTodayStepTotal = 0;
    private void getGoogleFitDayStep() {
        new HomeTodayGoogleFitData().getDayStepCount(getActivity(), 0,new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof String) {
                    int todayTotalStep = StringUtil.getIntVal(obj.toString());
                    Log.i(TAG, "오늘걸음수="+todayTotalStep);
                    mTodayStepTotal = todayTotalStep;

                    if (BuildConfig.DEBUG)
                        mTestStepTv.setText(StringUtil.getFormatPrice(""+mTodayStepTotal));

//
//               // 구글 피트니스 걸음 센서 등록
                    todayStepMissionAlert();
                }
            }
        });
    }

    boolean isShowAlert = false;
    private void todayStepMissionAlert() {
        if (mTodayStepTotal >= 10000) {

            if (isShowAlert)
                return;

            final String today = CDateUtil.getToday_yyyy_MM_dd();
            String prefToday = SharedPref.getInstance().getPreferences(PREF_KEY_STEP_MISSION_COMPLETE);

            if (today.equals(prefToday) == false) {
                isShowAlert = true;
                Tr_mvm_misson_goal_alert.RequestData requestData = new Tr_mvm_misson_goal_alert.RequestData();
                requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;
                requestData.misson_work_typ = "1";

                MediNewNetworkModule.doApi(getContext(), Tr_mvm_misson_goal_alert.class, requestData, new MediNewNetworkHandler() {
                    @Override
                    public void onSuccess(BaseData responseData) {
                        if (responseData instanceof Tr_mvm_misson_goal_alert) {
                            Tr_mvm_misson_goal_alert data = (Tr_mvm_misson_goal_alert)responseData;
                            try {

                                if(data.data_yn.equals("Y")) {
                                    SharedPref.getInstance().savePreferences(PREF_KEY_STEP_MISSION_COMPLETE, today);
                                    CDialog.showDlg(getContext(),data.misson_goal_txt)
                                            .setPointTvView(data.misson_goal_point+"P")
                                            .setOkButton(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    getMainData();
                                                }
                                            });

                                }


                            } catch (Exception e) {
                                CLog.e(e.toString());
                            }
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, String response, Throwable error) {

                    }
                });
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getCurrentFragment()  == MainActivityMedicare.HOME_MENU_1) {
            getMainData();      // 메인데이터
//        getAlimi();         // 알리미 리스트    (홈 전문으로 대체)
            resumeReqWeather(); // 날씨정보
            getGoogleFitDayStep();    //오늘 걸음 미션 확인 10000

            registerLocationUpdates();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
//        removeSensorClient();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        removeSensorClient();
    }
}
