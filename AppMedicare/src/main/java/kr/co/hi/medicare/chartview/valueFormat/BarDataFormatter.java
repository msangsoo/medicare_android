package kr.co.hi.medicare.chartview.valueFormat;


import kr.co.hi.medicare.charting.data.CEntry;
import kr.co.hi.medicare.charting.formatter.IValueFormatter;
import kr.co.hi.medicare.charting.utils.ViewPortHandler;

/**
 * Created by mrsohn on 2017. 3. 1..
 */

public class BarDataFormatter implements IValueFormatter {

    @Override
    public String getFormattedValue(float value, CEntry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        int idx = (int) value;
        return idx == 0 ? "": String.format("%,d", idx);
    }
}
