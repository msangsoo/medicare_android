package kr.co.hi.medicare.fragment.community;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.util.ArrayList;
import java.util.List;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.customview.ClearEditText;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.adapter.CommunityPostsAdapter;
import kr.co.hi.medicare.fragment.community.adapter.CommunitySearcherAdapter;
import kr.co.hi.medicare.fragment.community.adapter.CommunityUserAdapter;
import kr.co.hi.medicare.fragment.community.adapter.LoadMoreListener;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.community.data.CommunitySearchesData;
import kr.co.hi.medicare.fragment.community.data.CommunityUserData;
import kr.co.hi.medicare.fragment.community.data.ReceiveDataCode;
import kr.co.hi.medicare.fragment.community.data.SendDataCode;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_DB001;
import kr.co.hi.medicare.net.data.Tr_DB013;
import kr.co.hi.medicare.net.data.Tr_DB015;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by mrsohn on 2017. 3. 14..
 * 혈당관리
 */

public class CommunitySearchFragment extends BaseFragmentMedi implements LoadMoreListener, SwipeRefreshLayout.OnRefreshListener{
    private final String TAG = CommunitySearchFragment.class.getSimpleName();

    private DBHelper mDbHelper;
    private Tr_login user;

    private final int PAGE_TAG=0;
    private final int PAGE_SEARCH=1;
    private final int PAGE_DETAIL=2;
    private int PAGE=PAGE_TAG;

    //common
    ClearEditText edit_search;
    TextView btn_cancel;
    RelativeLayout layout_tag,layout_search,layout_search_detail;
    LinearLayout btn_back;

    //tag
    TagContainerLayout tag;


    //search
    RecyclerView recyclerView;
    CommunitySearcherAdapter searchesAdapter=null;
    LinearLayoutManager mLayoutManager,mLayoutManager2;
    TextView btn_delete_all;

    //detail
    RecyclerView recyclerView2;
    CommunityPostsAdapter communityPostsAdapter=null;
    CommunityUserAdapter communityUserAdapter=null;
    private TextView text_write,text_user,text_keyword;
    private ConstraintLayout tab_write,tab_user;
    private LinearLayout layout_write,layout_user;
    private int TAB_STATUS=R.id.tab_write;
    private SwipeRefreshLayout swipeRefresh;
    private int TotalPage = 1;
    private int firstVisibleItem, visibleItemCount, totalItemCount;
    private int visibleThreshold = 1;



    TagView.OnTagClickListener onTagClickListener = new TagView.OnTagClickListener() {
        @Override
        public void onTagClick(int position, String text) {
            NewActivity.moveToTagPage(getActivity(), text, getContext().getString(R.string.comm_search));
        }

        @Override
        public void onTagLongClick(int position, String text) {

        }

        @Override
        public void onSelectedTagDrag(int position, String text) {

        }

        @Override
        public void onTagCrossClick(int position) {

        }
    };


    public static Fragment newInstance() {
        CommunitySearchFragment fragment = new CommunitySearchFragment();
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_community_search, container, false);

        user = new Tr_login();

        btn_back = view.findViewById(R.id.btn_back);

        edit_search = view.findViewById(R.id.edit_search);
        btn_cancel = view.findViewById(R.id.btn_cancel);

        layout_tag = view.findViewById(R.id.layout_tag);
        layout_search = view.findViewById(R.id.layout_search);
        layout_search_detail = view.findViewById(R.id.layout_search_detail);

        tag = view.findViewById(R.id.tag);
        tag.setTagBorderRadius(0);
        tag.setOnTagClickListener(onTagClickListener);


//        tag.setOnTagClickListener(new TagView.OnTagClickListener() {
//            @Override
//            public void onTagClick(int position, String text) {
//
//                Bundle bundle = new Bundle();
//                bundle.putString(CommunityTagFragment.TAG_TITLE, text);
//                bundle.putString(CommunityTagFragment.PRE_PAGE, getContext().getString(R.string.comm_search));
//                replaceFragment(CommunityTagFragment.newInstance(), bundle);
//
//            }
//
//            @Override
//            public void onTagLongClick(int position, String text) {
//
//            }
//
//            @Override
//            public void onTagCrossClick(int position) {
//
//            }
//        });

        btn_delete_all = view.findViewById(R.id.btn_delete_all);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView = view.findViewById(R.id.recyclerView);
        searchesAdapter = new CommunitySearcherAdapter(getContext(),mOnClickListener);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(searchesAdapter);


        mLayoutManager2 = new LinearLayoutManager(getActivity());
        recyclerView2 = view.findViewById(R.id.recyclerView2);
        recyclerView2.setLayoutManager(mLayoutManager2);

        text_write = view.findViewById(R.id.text_write);
        text_user = view.findViewById(R.id.text_user);
        tab_write = view.findViewById(R.id.tab_write);
        tab_user = view.findViewById(R.id.tab_user);
        text_keyword = view.findViewById(R.id.text_keyword);

        layout_write = view.findViewById(R.id.layout_write);
        layout_user = view.findViewById(R.id.layout_user);
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(this);
        btn_delete_all.setOnClickListener(mOnClickListener);
        tab_user.setOnClickListener(mOnClickListener);
        tab_write.setOnClickListener(mOnClickListener);
        btn_cancel.setOnClickListener(mOnClickListener);
        btn_back.setOnClickListener(mOnClickListener);
        edit_search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    setLayout(PAGE_SEARCH);
                }
            }
        });




        edit_search.setOnKeyListener(mKeyListener);
        mDbHelper = new DBHelper(getContext());

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //글쓰기 완료 후 다시 커뮤니티 메인 페이지로 이동 시 리스트뷰 갱신
        if(resultCode==RESULT_OK) {
            if(requestCode == CommunityCommentFragment.REQUEST_CODE_COMMENTINFO){
                CommunityListViewData comm_data =(CommunityListViewData)data.getSerializableExtra(CommunityCommentFragment.REQUEST_CODE_COMMDATA);

                boolean isDel = false;
                try{
                    isDel = data.getBooleanExtra(CommunityCommentFragment.REQUEST_CODE_ISDEL,false);
                }catch (Exception e){
                    e.printStackTrace();
                }


                if(communityPostsAdapter!=null){

                    if(!isDel)
                        communityPostsAdapter.updateData(comm_data);
                    else
                        communityPostsAdapter.deleteItem(comm_data.CM_SEQ);
                }
            }
        }
    }




    View.OnKeyListener mKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            Log.i(TAG, "keyCode"+keyCode);
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                searchKeyword();
                return true;
            }
            return false;
        }
    };


    private void searchKeyword(){
        String word = getKeywordFromView();
        if(!word.equals("")){
            if(mDbHelper.getmCommunitySearches().insert(word)){
                setLayout(PAGE_DETAIL);
            }

        }
    }

    private String getKeywordFromView(){

        return edit_search.getText().toString();
    }


    private void clearEditText(){
        edit_search.clearFocus();
        InputMethodManager imm2 = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        imm2.hideSoftInputFromWindow (edit_search.getWindowToken(),0);
    }



    private void setLayout(int PAGE_TYPE){
        PAGE = PAGE_TYPE;

        switch (PAGE){
            case PAGE_TAG:
                layout_tag.setVisibility(View.VISIBLE);
                layout_search.setVisibility(View.GONE);
                layout_search_detail.setVisibility(View.GONE);
                text_keyword.setVisibility(View.VISIBLE);
                btn_cancel.setBackgroundColor(getResources().getColor(R.color.color_f2f2f2));
                setTabStatus(R.id.tab_write);
                edit_search.setText("");
                clearEditText();
                getTagListFromServer();
                break;

            case PAGE_SEARCH:
                layout_tag.setVisibility(View.GONE);
                layout_search.setVisibility(View.VISIBLE);
                layout_search_detail.setVisibility(View.GONE);
                text_keyword.setVisibility(View.VISIBLE);
                btn_cancel.setBackgroundColor(getResources().getColor(R.color.color_6981ec));
                getSearchesDataFromDB();
                break;

            case PAGE_DETAIL:
                layout_tag.setVisibility(View.GONE);
                layout_search.setVisibility(View.GONE);
                text_keyword.setVisibility(View.GONE);
                layout_search_detail.setVisibility(View.VISIBLE);
                clearEditText();
                setTabView();
                getPosts();

                break;
        }
    }

    private void getPosts() {

        switch (TAB_STATUS){
            case R.id.tab_write:
                communityPostsAdapter = new CommunityPostsAdapter(getContext(),mOnClickListener,onTagClickListener);
                recyclerView2.setAdapter(communityPostsAdapter);
                recyclerView2.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);

                        LinearLayoutManager llManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                        visibleItemCount = recyclerView.getChildCount();
                        totalItemCount = llManager.getItemCount();
                        firstVisibleItem = llManager.findFirstVisibleItemPosition();

                        if (dy>0&&!communityPostsAdapter.getMore() && (totalItemCount - visibleItemCount)<= (firstVisibleItem + visibleThreshold)) {
                            onLoadMore();
                        }
                    }
                });
                break;
            default:
                communityUserAdapter = new CommunityUserAdapter(getContext(),mOnClickListener);
                recyclerView2.setAdapter(communityUserAdapter);
                recyclerView2.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
//                        LinearLayoutManager llManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//                        visibleItemCount = recyclerView.getChildCount();
//                        totalItemCount = llManager.getItemCount();
//                        firstVisibleItem = llManager.findFirstVisibleItemPosition();
//
//                        if (dy>0&&!communityUserAdapter.getMore() && (totalItemCount - visibleItemCount)<= (firstVisibleItem + visibleThreshold)) {
//                            onLoadMore();
//                        }


                    }
                });
                break;
        }

        getList(PROGRESSBAR_TYPE_CENTER,"1",getKeywordFromView(),"");
    }

    private void getSearchesDataFromDB() {
        searchesAdapter.addAllItem(mDbHelper.getmCommunitySearches().getResultAll(mDbHelper));
    }


    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int viewId = v.getId();
            Bundle bundle = new Bundle();
            switch (viewId){
                case R.id.tab_write:
                case R.id.tab_user:
                    setTabStatus(viewId);
                    setLayout(PAGE_DETAIL);
                    break;


                case R.id.btn_cancel:
                    PAGE = PAGE_TAG;
                    setLayout(PAGE_TAG);
                    break;

                case R.id.word:
                case R.id.btn_search:
                    CommunitySearchesData data = (CommunitySearchesData) v.getTag();
                    if(data!=null&&data.word!=null&&!data.word.equals("")) {
                        edit_search.setText("");
                        edit_search.append(data.word);
                        searchKeyword();
                    }

                    break;

                case R.id.btn_delete_all:

                    if(mDbHelper.getmCommunitySearches().deleteDbAll()){
                        searchesAdapter.delItemAll();
                    }


                    break;

                case R.id.btn_delete:
                    CommunitySearchesData data2 = (CommunitySearchesData) v.getTag();
                    if(mDbHelper.getmCommunitySearches().deleteDb(data2.word))
                        searchesAdapter.delItem(data2);

                    break;

                case R.id.btn_back:

                    onBackPressed();
                    break;


                case R.id.nick:
                case R.id.profile:
                    int position = Integer.parseInt(v.getTag(R.id.comm_user).toString());
                    CommunityUserData userData = new CommunityUserData();
                    if(TAB_STATUS==R.id.tab_write){
                        userData.MBER_SN = communityPostsAdapter.getData(position).MBER_SN;
                        userData.NICKNAME = communityPostsAdapter.getData(position).NICK;
                    }else{
                        userData = communityUserAdapter.getData(position);
                    }

                    NewActivity.moveToProfilePage(getString(R.string.comm_search), userData,getActivity());

                    break;

                case R.id.text:
                    NewActivity.moveToCommentPage(CommunitySearchFragment.this,communityPostsAdapter.getData(Integer.parseInt(v.getTag(R.id.comm_main_text).toString())),false,getString(R.string.comm_search),SendDataCode.CM_ALL);
                    break;


            }

        }
    };

    private void setTabStatus(int id){
        if(TAB_STATUS!=id) {
            TAB_STATUS = id;
        }
    }


    private void setTabView() {
            if(TAB_STATUS==R.id.tab_write){
                layout_write.setBackgroundColor(getResources().getColor(R.color.color_6981ec));
                layout_user.setBackgroundColor(getResources().getColor(R.color.color_BFBFBF));
                text_write.setTextColor(getResources().getColor(R.color.color_6981ec));
                text_user.setTextColor(getResources().getColor(R.color.color_BFBFBF));
            }else{
                layout_write.setBackgroundColor(getResources().getColor(R.color.color_BFBFBF));
                layout_user.setBackgroundColor(getResources().getColor(R.color.color_6981ec));
                text_write.setTextColor(getResources().getColor(R.color.color_BFBFBF));
                text_user.setTextColor(getResources().getColor(R.color.color_6981ec));
            }
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        setLayout(PAGE_TAG);

    }

    @Override
    public void onLoadMore(){
        switch (TAB_STATUS) {
            case R.id.tab_write:
                communityPostsAdapter.setProgressMore(true);
                communityPostsAdapter.setMore(true);
                break;
//            default:
//                communityUserAdapter.setProgressMore(true);
//                communityUserAdapter.setMore(true);
//                break;

        }
        getList(PROGRESSBAR_TYPE_BOTTOM,Integer.toString(TotalPage+1),getKeywordFromView(),"");
    }

    @Override
    public void onRefresh() {

        getList(PROGRESSBAR_TYPE_TOP,"1",getKeywordFromView(),"");
    }



    private void getList(final String loadingType,final String PG,String sword,String tag){
        if(TAB_STATUS==R.id.tab_write) {
            getListFromServer(loadingType,PG,sword,tag);
        }else{
            getListFromServer(sword);
        }
    }

    public void addTag(List<String> data){

        if(data!=null&&data.size()>0){
            tag.setTags(data);
        }

    }

    /**
     * 서버에 태그 리스트 요청 DB015
     */
    private void getTagListFromServer() {
        final Tr_DB015.RequestData requestData = new Tr_DB015.RequestData();
        boolean isShowProgress=true;

        requestData.DOCNO ="DB015";

        MediNewNetworkModule.doApi(getContext(), new Tr_DB015(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                if (responseData instanceof Tr_DB015) {
                    Tr_DB015 data = (Tr_DB015)responseData;
                    try {
                        if(data.DATA!=null&&data.DATA.size()>0){
                            switch (data.RESULT_CODE){
                                case ReceiveDataCode.DB015_SUCCESS:

                                    List<String> tagList = new ArrayList<>();

                                    for(int i=0; i<data.DATA.size();i++){
                                        tagList.add(data.DATA.get(i).TAG_WORD);
                                    }

                                    addTag(tagList);

                                    break;

                                case ReceiveDataCode.DB015_ERROR_ETC:

                                    break;

                                default:

                                    break;
                            }
                        }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                Log.e(TAG, response);
            }
        });
    }


    /**
     * 서버에 게시글 리스트 데이터 요청 DB002
     * @param loadingType 프로그레스바 로딩 타입
     * @param PG 요청 페이지
     * @param sword 키워드
     * @param tag 태그
     */
    private void getListFromServer(final String loadingType,final String PG,String sword,String tag) {
        final Tr_DB001.RequestData requestData = new Tr_DB001.RequestData();
        boolean isShowProgress=false;

        requestData.CMGUBUN = SendDataCode.CM_ALL;
        requestData.PG_SIZE = PAGE_SIZE;
        requestData.PG = PG;
        requestData.SEQ = user.mber_sn;
        requestData.SWORD = sword;
        requestData.CM_TAG = tag;

        if(loadingType.equals(PROGRESSBAR_TYPE_CENTER))
            isShowProgress=true;

        MediNewNetworkModule.doApi(getContext(), new Tr_DB001(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                //하단 프로그레스바일 경우 프로그레스바 종료
                if(loadingType.equals(PROGRESSBAR_TYPE_BOTTOM))
                    communityPostsAdapter.setProgressMore(false);

                if (responseData instanceof Tr_DB001) {
                    Tr_DB001 data = (Tr_DB001)responseData;
                    try {

                        if(data.DATA!=null&&data.DATA.size()>0){

                            int requestPage = Integer.parseInt(PG);
                            int receivePage = Integer.parseInt(data.DATA.get(0).TPAGE);

                            if(requestPage<=receivePage){
                                TotalPage = requestPage;
                                if(TotalPage<2){//서버로부터 받은 페이지가 2보다 작으면 클린 후 추가
                                    communityPostsAdapter.addAllItem(data.DATA);
                                }else{ //
                                    communityPostsAdapter.addItemMore(data.DATA);
                                }

                            }
                        }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
                switch (loadingType){
                    case PROGRESSBAR_TYPE_CENTER:

                        break;
                    case PROGRESSBAR_TYPE_BOTTOM:
                        communityPostsAdapter.setMore(false);
                        break;
                    case PROGRESSBAR_TYPE_TOP:
                        swipeRefresh.setRefreshing(false);
                        break;
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                Log.e(TAG, response);
                switch (loadingType){
                    case PROGRESSBAR_TYPE_CENTER:
                        break;
                    case PROGRESSBAR_TYPE_BOTTOM:
                        communityPostsAdapter.setProgressMore(false);
                        communityPostsAdapter.setMore(false);
                        break;
                    case PROGRESSBAR_TYPE_TOP:
                        swipeRefresh.setRefreshing(false);
                        break;
                }
            }
        });
    }



    /**
     * 사용자 리스트 요청 DB013
     * @param sword 키워드
     */
    private void getListFromServer(String sword) {
        final Tr_DB013.RequestData requestData = new Tr_DB013.RequestData();
        boolean isShowProgress=true;

        requestData.NICK = sword;

        MediNewNetworkModule.doApi(getContext(), new Tr_DB013(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_DB013) {
                    Tr_DB013 data = (Tr_DB013)responseData;
                    try {

                        switch (data.RESULT_CODE){
                            case ReceiveDataCode.DB013_SUCCESS:
                                if(data.DATA!=null&&data.DATA.size()>0){
                                    communityUserAdapter.addAllItem(data.DATA);
                                }

                                break;
                            case ReceiveDataCode.DB013_ERROR_ETC:
                                errorMessage = getResources().getString(R.string.comm_error_common_9999);
                                break;
                            default:
                                errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:1";
                                break;
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                        errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:2";
                    }
                }else{
                    errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:3";
                }

                if(!errorMessage.equals("")){
//                    dialog = DialogCommon.showDialogSingleBtn(getContext(), errorMessage, getResources().getString(R.string.comm_confirm), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialog.dismiss();
//                        }
//                    });
                    CDialog.showDlg(getContext(), errorMessage);
                }

                swipeRefresh.setRefreshing(false);
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                Log.e(TAG, response);
                swipeRefresh.setRefreshing(false);
            }
        });
    }



}
