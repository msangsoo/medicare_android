
package kr.co.hi.medicare.chartview.food;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.charting.data.BarData;
import kr.co.hi.medicare.charting.data.BarDataSet;
import kr.co.hi.medicare.charting.data.BarEntry;
import kr.co.hi.medicare.charting.interfaces.datasets.IBarDataSet;
import kr.co.hi.medicare.chartview.valueFormat.BarDataFormatter;
import kr.co.hi.medicare.chartview.walk.BarChartView;
import kr.co.hi.medicare.utilhw.ChartTimeUtil;


public class FoodBarChartView extends BarChartView {
    private final String TAG = getClass().getSimpleName();
    public FoodBarChartView(Context context, View v) {
        super(context, v);
    }

    public void setData(List<BarEntry> yVals1, ChartTimeUtil timeClass) {
        BarDataSet set1;

        if (mChart.getTimeClass() != null && mChart.getTimeClass().getTimeUnit() != timeClass.getTimeUnit()) {
            mChart.clear();
        }

        mChart.setTimeClass(timeClass);

        // 테스트 데이터 넣기   (2개 다르게 나오는 바 차트 )
//        ArrayList<BarEntry> values = new ArrayList<>();
//
//        for (int i = 0; i < 100; i++) {
//            float mul = (50 + 1);
//            float val1 = (float) (Math.random() * mul) + mul / 3;
//            float val2 = (float) (Math.random() * mul) + mul / 3;
//            float val3 = (float) (Math.random() * mul) + mul / 3;
//
//            Log.i(TAG, "onProgressChanged["+i+"].val1="+val1+", val2="+val2+", val3="+val3);
//
//            values.add(new BarEntry(
//                    i,
//                    new float[]{val1, val2},null));
//        }
//        yVals1.clear();;
//        yVals1.addAll(values);


        BarData data;

        if (mChart.getData() != null && mChart.getData().getDataSetCount() > 0) {
            Log.i(TAG, "setData_1");
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            set1.setHighLightColor(Color.TRANSPARENT);  // 차트 클릭시 하이라이트 색상
            set1.setHighLightAlpha(Color.TRANSPARENT);

            data = mChart.getData();
            data.setDrawValues(false);  // 바차트 상단에 값을 표시할지 여부

            mChart.setData(data, timeClass);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            Log.i(TAG, "setData_2");
            set1 = new BarDataSet(yVals1, "The year 2017");

            set1.setDrawIcons(false);

            // 그래프 색상 설정
            set1.setColors(getColors());
            set1.setHighLightColor(Color.TRANSPARENT);
            set1.setHighLightAlpha(Color.TRANSPARENT);

            List<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            // 그래프 두께 및 상단 값 세팅
            data = new BarData(dataSets);

            data.setValueTextSize(10f);
            data.setValueTextColor(Color.BLACK);
//            data.setValueTypeface(mTfLight);
            data.setDrawValues(false);
            data.setBarWidth(0.9f);
            data.setValueFormatter(new BarDataFormatter());

            mChart.setData(data, timeClass);
        }
    }

    /**
     * 컬러 지정
     * @return
     */
    private int[] getColors() {
        int[] colors = new int[2];
        int[] MATERIAL_COLORS2 = {ContextCompat.getColor(mContext, R.color.colorMain), ContextCompat.getColor(mContext, R.color.x_255_157_48)};
        System.arraycopy(MATERIAL_COLORS2, 0, colors, 0, 2);
        return colors;
    }

    public int rgb(String hex) {
        int color = (int) Long.parseLong(hex.replace("#", ""), 16);
        int r = (color >> 16) & 0xFF;
        int g = (color >> 8) & 0xFF;
        int b = (color >> 0) & 0xFF;
        return Color.rgb(r, g, b);
    }
}
