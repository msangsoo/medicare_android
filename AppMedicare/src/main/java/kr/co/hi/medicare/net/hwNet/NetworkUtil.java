package kr.co.hi.medicare.net.hwNet;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {
    private final String TAG = NetworkUtil.class.getSimpleName();
     
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
     
     
    public static boolean getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
 
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)

                return true;
            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)

                return true;
        } 

        return false;
    }


//    public void getData(final Context context, final Class<?> cls, final Object obj, final ApiData.IStep step) {
//
//        if (com.greencross.hanwha.sugar.util.NetworkUtil.getConnectivityStatus(context) == false) {
//            CDialog.showDlg(context, context.getString(R.string.text_network_error), new CDialog.DismissListener() {
//                @Override
//                public void onDissmiss() {
////                    finish();
//                }
//            });
//            return;
//        }
//
//        CConnAsyncTask.CConnectorListener queryListener = new CConnAsyncTask.CConnectorListener() {
//            @Override
//            public Object run() {
//                ApiData data = new ApiData();
//                return data.getData(context, cls, obj);
//            }
//
//            @Override
//            public void view(CConnAsyncTask.CQueryResult result) {
//                Logger.i(TAG, "result.result=" + result.result + ", result.data=" + result.data);
//                if (result.result == CConnAsyncTask.CQueryResult.SUCCESS && result.data != null) {
//                    if (step != null) {
//                        step.next(result.data);
//                    }
//
//                } else {
//                    Logger.e(TAG, context.getString(R.string.text_network_data_rec_fail));
//                    Logger.e(TAG, "CConnAsyncTask error=" + result.errorStr);
//                    try {
//                        CDialog.showDlg(context, context.getString(R.string.text_network_data_rec_fail), new CDialog.DismissListener() {
//                            @Override
//                            public void onDissmiss() {
////                                finish();
//                            }
//                        });
//                    } catch (Exception e) {
//
//                        try {
//                            CDialog.showDlg(MainActivityMedicare.this, getString(R.string.text_network_data_rec_fail), new CDialog.DismissListener() {
//                                @Override
//                                public void onDissmiss() {
//                                    finish();
//                                }
//                            });
//                        } catch (Exception e2) {
//                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivityMedicare.this);
//                            builder.setMessage(getString(R.string.text_network_data_rec_fail));
//                            builder.setPositiveButton(getString(R.string.text_confirm), null);
//                            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                                @Override
//                                public void onDismiss(DialogInterface dialog) {
//                                    finish();
//                                }
//                            });
//                            AlertDialog dlg = builder.create();
//                            dlg.show();
//                        }
//                    }
//                }
//            }
//        };
//
//        CConnAsyncTask asyncTask = new CConnAsyncTask();
//        asyncTask.execute(queryListener);
//    }
}