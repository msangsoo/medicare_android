package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 정회원전환 가능 여부
 * inpuinput 값
 * insures_code : 회사코드
 * mber_nm : 회원명
 * mber_lifyea : 생년월일
 * mber_hp : 전화번호
 * mber_sex : 성별
 * mber_sn : 회원키
 * <p>
 * output값
 * api_code : 호출코드명
 * insures_code : 회사코드
 * alert_txt : 리턴메세지
 * data_yn : Y:가입가능 / N: 가입불가 / YN:정회원 가입된 아이디 / YY:웹사이트에 아이디가 존재함
 */

public class Tr_mber_find_yn extends BaseData {
	private final String TAG = Tr_mber_find_yn.class.getSimpleName();

	public static class RequestData {
		public String mber_nm;
		public String mber_lifyea;
		public String mber_hp;
		public String mber_sex;
		public String mber_sn; //회원 일련번호
	}

	public Tr_mber_find_yn() {
		super.conn_url = "http://m.shealthcare.co.kr/HL_MED/WebService/MED_MOBILE_CALL.asmx/MED_mobile_Call";
	}

	@Override
	public JSONObject makeJson(Object obj) throws JSONException {
		if (obj instanceof RequestData) {
			JSONObject body = getBaseJsonObj();

			RequestData data = (RequestData) obj;
			body.put("api_code", getApiCode(TAG));
			body.put("insures_code", INSURES_CODE);
			body.put("mber_sn", data.mber_sn);
			body.put("mber_sex", data.mber_sex);
			body.put("mber_hp", data.mber_hp);
			body.put("mber_lifyea", data.mber_lifyea);
			body.put("mber_nm", data.mber_nm);
			return body;
		}

		return super.makeJson(obj);
	}

	/**************************************************************************************************/
	/***********************************************RECEIVE********************************************/
	/**************************************************************************************************/

	@Expose
	@SerializedName("alert_txt")
	public String alert_txt;
	@Expose
	@SerializedName("data_yn")
	public String data_yn;
	@Expose
	@SerializedName("insures_code")
	public String insures_code;
	@Expose
	@SerializedName("api_code")
	public String api_code;

	@Expose
	@SerializedName("mber_no")
	public String mber_no;

	@Expose
	@SerializedName("site_memid")
	public String site_memid;

	@Expose
	@SerializedName("app_memid")
	public String app_memid;

	@Expose
	@SerializedName("memname")
	public String memname;
}
