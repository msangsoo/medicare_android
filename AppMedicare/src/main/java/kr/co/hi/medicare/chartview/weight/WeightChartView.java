
package kr.co.hi.medicare.chartview.weight;

import android.content.Context;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;

import androidx.core.content.ContextCompat;

import kr.co.hi.medicare.value.TypeDataSet;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.charting.charts.WeightChart;
import kr.co.hi.medicare.charting.components.Legend;
import kr.co.hi.medicare.charting.components.XAxis;
import kr.co.hi.medicare.charting.components.YAxis;
import kr.co.hi.medicare.charting.data.BarData;
import kr.co.hi.medicare.charting.data.BarDataSet;
import kr.co.hi.medicare.charting.data.BarEntry;
import kr.co.hi.medicare.charting.formatter.IAxisValueFormatter;
import kr.co.hi.medicare.charting.interfaces.datasets.IBarDataSet;
import kr.co.hi.medicare.chartview.valueFormat.AxisValueFormatter;
import kr.co.hi.medicare.chartview.valueFormat.BarDataFormatter;
import kr.co.hi.medicare.chartview.valueFormat.XYMarkerViewWeight;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.utilhw.ChartTimeUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.StringUtil;


public class WeightChartView {

    private final String TAG = WeightChartView.class.getSimpleName();

    protected WeightChart mChart;
//    private SeekBar mSeekBarX, mSeekBarY;
//    private TextView tvX, tvY;
    private Context mContext;

    protected Typeface mTfRegular;
    protected Typeface mTfLight;

    public WeightChartView(Context context, View v) {
        mContext = context;

//        mTfRegular = ResourcesCompat.getFont(context, R.font.nanum_barun_gothic);
//        mTfLight = ResourcesCompat.getFont(context, R.font.nanum_barun_gothic_light);

//        tvX = (TextView) v.findViewById(R.id.tvXMax);
//        tvY = (TextView) v.findViewById(R.id.tvYMax);
//
//        mSeekBarX = (SeekBar) v.findViewById(R.id.seekBar1);
//        mSeekBarY = (SeekBar) v.findViewById(R.id.seekBar2);

        mChart = (WeightChart) v.findViewById(R.id.chart1);
        mChart.setTouchEnabled(true);       // 클릭시 값 표시 해주려면 true

        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);

        mChart.getDescription().setEnabled(false);
        mChart.getDefaultValueFormatter();

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(false);
        mChart.setDoubleTapToZoomEnabled(false);
        mChart.setScaleEnabled(false);

        mChart.setDrawGridBackground(false);

        AxisValueFormatter xAxisFormatter = new AxisValueFormatter(TypeDataSet.Period.PERIOD_DAY);
        xAxisFormatter.setUnitStr("Kg");

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(15);
        xAxis.setValueFormatter(xAxisFormatter);

//        IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = mChart.getAxisLeft();
//        leftAxis.setTypeface(mTfLight);
        leftAxis.setLabelCount(8, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);

        // 그래프 왼쪽 수치 몸무게 값 범위 지정
//        leftAxis.setAxisMinimum(40);
//        leftAxis.setAxisMaximum(65);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);

        // 하단 설명 문구 (bottom label)
        Legend l = mChart.getLegend();
        l.setEnabled(false);

        // 차트 클릭시 나오는 마커
        XYMarkerViewWeight mv = new XYMarkerViewWeight(mContext, xAxisFormatter);
        mv.setChartView(mChart);
        mChart.setMarker(mv);
        mv.setEnabled(true);

        // setting data
//        mSeekBarY.setProgress(50);
//        mSeekBarX.setProgress(12);

        mChart.setExtraTopOffset(30);   // 차트 상단 여백
//        mChart.setDrawValueAboveBar(false); // 그래프 상단 값 표시

        mChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChart.setDrawMarkers(true);  // 마커 표시 활성화 시키기
            }
        });
    }

    public void setXValueFormat(IAxisValueFormatter f) {
        XAxis xAxis = mChart.getXAxis();
        xAxis.setValueFormatter(f);
    }

    /**
     * X축 최대 값을 설정 한다
     * @param max
     */
    public void setXvalMinMax(float min, float max, int labelCnt) {
        XAxis xAxis = mChart.getXAxis();
        xAxis.setAxisMinimum(min);
        xAxis.setAxisMaximum(max);
        xAxis.setLabelCount(labelCnt);
    }

    /**
     * 차트 왼쪽 그래프
     * @return
     */
    public YAxis getYAxisLeft() {
        YAxis leftAxis = mChart.getAxisLeft();
        return leftAxis;
    }

    /**
     * Y축 최대 값을 설정 한다
     * @param max
     */
    public void setYvalMinMax(float min, float max, int labelCnt) {
        YAxis yAxis = mChart.getAxisLeft();
        yAxis.setAxisMinimum(min);
        yAxis.setAxisMaximum(max);
        yAxis.setLabelCount(labelCnt);
    }


    public void invalidate() {
        if (mChart != null)
            mChart.invalidate();
    }


    public void animateXY() {
        mChart.animateXY(500, 500);
    }

    public void animateY() {
        mChart.animateY(5);
    }

    public void setData(List<BarEntry> yVals1, ChartTimeUtil timeUtil) {
        mChart.setTimeClass(timeUtil);
        BarDataSet set1;
        setYMinMax(yVals1);
        if (mChart.getData() != null && mChart.getData().getDataSetCount() > 0) {
            Logger.i(TAG, "setData no Data");
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            // 목표체중 라인 값 설정해 주기
//            Tr_login login = UserInfo.getLoginInfo();
//            if (login != null) {
//                float goalWeight = StringUtil.getFloatVal(login.mber_bdwgh_goal);
//                float maxYVal = mChart.getAxisLeft().mAxisMaximum;
//
//                YAxis leftAxis = mChart.getAxisLeft();
//                if (maxYVal < goalWeight) {
//                    leftAxis.setAxisMaximum(goalWeight + 10);
//                } else {
//                    leftAxis.setAxisMaximum(maxYVal + 10);
//                }
//            }

            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            Logger.i(TAG, "setData "+mChart.getData());
            set1 = new BarDataSet(yVals1, "bottom label");

            set1.setDrawIcons(false);

            // 그래프 색상 설정
            set1.setColor(ContextCompat.getColor(mContext, R.color.x_91_132_230));

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            // 그래프 두께 및 상단 값 세팅
            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setValueTextColor(Color.BLACK);
            data.setValueTypeface(mTfLight);
            data.setBarWidth(0.9f);
            data.setValueFormatter(new BarDataFormatter());

            mChart.setData(data, timeUtil);
        }
    }

    /**
     * y라벨 구하기
     * @param weightYVals
     */
    private void setYMinMax(List<BarEntry> weightYVals) {
        float yMin = Float.MAX_VALUE;
        float yMax = Float.MIN_VALUE;
        Log.i(TAG, "#######yLabelCnt##############");
        for (BarEntry entry : weightYVals) {
            float y = entry.getY();
            if (y != 0 && y < yMin) {
                yMin = y;
            }

            if (y != 0 && y > yMax) {
                yMax = y;
            }
        }

        // y min값이 없는 경우
        if (yMin == Float.MAX_VALUE) {
            Tr_login user = UserInfo.getLoginInfo();
            yMin = StringUtil.getFloat(user.mber_bdwgh);
        }
        // y max값이 없는 경우
        if (yMax == Float.MIN_VALUE) {
            yMax = yMin;
        }

        yMin = yMin -3;
        yMax = yMax +3;

        int yLabelCnt = (int)(yMax - yMin);
        setYvalMinMax(yMin, yMax, yLabelCnt);
        Log.i(TAG, "setYMinMax yMin="+yMin+", yMax="+yMax+", yLabelCnt="+yLabelCnt);
    }



//    public void setTestData(int count, float range) {
//
//        float start = 0f;
//
//        List<BarEntry> yVals1 = new ArrayList<>();
//        for (int i = (int) start; i < start + count; i++) {
//            float mult = (range + 1);
//            float val = (float) (Math.random() * mult);
//
//            if (Math.random() * 100 < 25) {
//                yVals1.add(new BarEntry(i, val, ContextCompat.getDrawable(mContext, android.R.drawable.btn_star)));
//            } else {
//                yVals1.add(new BarEntry(i, val));
//            }
//        }
//
//        BarDataSet set1;
//
//        if (mChart.getData() != null && mChart.getData().getDataSetCount() > 0) {
//            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
//            set1.setValues(yVals1);
//            mChart.getData().notifyDataChanged();
//            mChart.notifyDataSetChanged();
//        } else {
//            set1 = new BarDataSet(yVals1, "bottom label");
//
//            set1.setDrawIcons(false);
//
//            // 그래프 색상 설정
//            set1.setColor(ContextCompat.getColor(mContext, R.color.x_91_132_230));
//
//            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
//            dataSets.add(set1);
//
//            // 그래프 두께 및 상단 값 세팅
//            BarData data = new BarData(dataSets);
//            data.setValueTextSize(10f);
//            data.setValueTextColor(Color.RED);
//            data.setValueTypeface(mTfLight);
//            data.setBarWidth(0.9f);
//            data.setValueFormatter(new BarDataFormatter());
//
//            mChart.setData(data, mChart.getTimeClass());
//        }
//    }

    protected RectF mOnValueSelectedRectF = new RectF();
}
