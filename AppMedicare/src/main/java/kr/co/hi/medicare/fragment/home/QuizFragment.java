package kr.co.hi.medicare.fragment.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_quiz_point_input;
import kr.co.hi.medicare.utilhw.CDateUtil;

/**
 *
 **/
public class QuizFragment extends BaseFragmentMedi {

    public static final String BUNDLE_KEY_QUIZ_REPLAY = "quiz_replay";
    public static final String BUNDLE_KEY_QUIZ_EXPLAIN = "quiz_explain";
    public static final String BUNDLE_KEY_QUIZ_QUESTION = "quiz_question";
    public static final String BUNDLE_KEY_QUIZ_SN = "quiz_sn";

    private TextView mQuizTitleTv;
    private LinearLayout mAnswerLayout;
    private TextView mQuizContentTv;
    private ImageView mAnswerTitleIcon;
    private ImageView mOkBtn;
    private ImageView mXBtn;
    private LinearLayout mBtnLayout;

    private String mAnswer;
    private String mExplain;
    private String mQuestion;
    private String mQuizSn;

    public static Fragment newInstance() {
        QuizFragment fragment = new QuizFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frament_quiz, container, false);
        this.mBtnLayout = (LinearLayout) view.findViewById(R.id.quiz_answer_btn_layout);
        this.mAnswerTitleIcon = (ImageView) view.findViewById(R.id.quiz_answer_title_iv);
        this.mXBtn = (ImageView) view.findViewById(R.id.question_x_btn);
        this.mOkBtn = (ImageView) view.findViewById(R.id.question_ok_btn);
        this.mQuizContentTv = (TextView) view.findViewById(R.id.quiz_content_textview);
        this.mAnswerLayout = (LinearLayout) view.findViewById(R.id.quiz_answer_title);
        this.mQuizTitleTv = (TextView) view.findViewById(R.id.quiz_title_tv);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            mAnswer = bundle.getString(BUNDLE_KEY_QUIZ_REPLAY);
            mExplain = bundle.getString(BUNDLE_KEY_QUIZ_EXPLAIN);
            mQuestion = bundle.getString(BUNDLE_KEY_QUIZ_QUESTION);
            mQuizSn = bundle.getString(BUNDLE_KEY_QUIZ_SN);

            // 퀴즈 답변 (o, x)   1(O) , 0(X)
            mAnswerTitleIcon.setImageResource("1".equals(mAnswer) ? R.drawable.m_quiz_answer_title_icon_o : R.drawable.m_quiz_answer_title_icon_x);
            mQuizContentTv.setText(mQuestion);

            mXBtn.setOnClickListener(mOnClickListener);
            mOkBtn.setOnClickListener(mOnClickListener);

            //click 저장
            OnClickListener ClickListener = new OnClickListener(mOnClickListener, view, getContext());


            //홈
            mXBtn.setOnTouchListener(ClickListener);
            mOkBtn.setOnTouchListener(ClickListener);

            //코드부여
            mXBtn.setContentDescription(getString(R.string.mOkBtn));
            mOkBtn.setContentDescription(getString(R.string.mOkBtn));
        }
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.question_ok_btn :
                case R.id.question_x_btn :
                    mQuizTitleTv.setVisibility(View.GONE);
                    mAnswerLayout.setVisibility(View.VISIBLE);
                    boolean isOK = view.getTag().equals(mAnswer);
                    mQuizContentTv.setText(mExplain);
                    mBtnLayout.setVisibility(View.GONE);

                    doPoint();
                    break;
            }
        }
    };

    /**
     * 퀴즈 풀기 입력값
     */
    private void doPoint() {
        Tr_quiz_point_input.RequestData requestData = new Tr_quiz_point_input.RequestData();
        requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;
        requestData.quiz_sn = mQuizSn;
        requestData.quiz_input_de = CDateUtil.getToday_yyyy_MM_dd();

        MediNewNetworkModule.doApi(getContext(), Tr_quiz_point_input.class, requestData, false, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                Log.i(TAG, "퀴즈 풀기 완료");
                if (responseData instanceof Tr_quiz_point_input) {
                    Tr_quiz_point_input recv = (Tr_quiz_point_input)responseData;
                    if ("Y".equals(recv.misson_alert_yn)) {
                        CDialog.showDlg(getContext(), recv.misson_goal_txt)
                                .setPointTvView(recv.misson_goal_point+"P");
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                Log.e(TAG, "퀴즈 풀기 전문 오류 .");
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
