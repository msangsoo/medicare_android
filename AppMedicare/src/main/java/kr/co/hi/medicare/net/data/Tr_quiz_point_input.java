package kr.co.hi.medicare.net.data;

/**
 *
 * 퀴즈 -> 포인트적립
 *
 * input값
 * insures_code : 회사코드
 * mber_sn : 회원고유키값
 * api_code : api 키값
 * quiz_sn : 퀴즈 고유키값
 * quiz_input_de : 퀴즈 등록날짜(일일 포인트)
 *
 * output 값
 * api_code : 호출코드명
 * insures_code : 회사코드
 * misson_alert_yn : 알림여부
 * misson_goal_txt : 텍스트 (내용)
 * misson_goal_point : 포인트
 * reg_yn : 등록여부
 */

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;


public class Tr_quiz_point_input extends BaseData {
    private final String TAG = getClass().getSimpleName();


    public static class RequestData {
        public String mber_sn;
        public String quiz_sn;
        public String quiz_input_de;

    }


    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {

            JSONObject body = new JSONObject();

            RequestData data = (RequestData) obj;
            body.put("api_code", TAG.replace("Tr_", ""));
            body.put("insures_code", INSURES_CODE);

             body.put("mber_sn", data.mber_sn);
             body.put("quiz_sn", data.quiz_sn);
             body.put("quiz_input_de", data.quiz_input_de);

            return body;
        }

        return super.makeJson(obj);
    }

/**************************************************************************************************/
/***********************************************RECEIVE********************************************/
    /**************************************************************************************************/
    @SerializedName("insures_code")
    private String insures_code;
    @SerializedName("api_code")
    private String api_code;

    @SerializedName("reg_yn")
    public String reg_yn;

    @SerializedName("misson_alert_yn")
    public String misson_alert_yn;
    @SerializedName("misson_goal_txt")
    public String misson_goal_txt;
    @SerializedName("misson_goal_point")
    public String misson_goal_point;


}
