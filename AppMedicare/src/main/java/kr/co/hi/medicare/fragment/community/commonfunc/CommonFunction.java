package kr.co.hi.medicare.fragment.community.commonfunc;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.iojjj.rcbs.RoundedCornersBackgroundSpan;
import com.github.iojjj.rcbs.TextAlignment;
import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.fragment.community.dialog.DialogCommon;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.hwdata.Tr_login;

public class CommonFunction {
    public static String getThumbnail(String path){
        StringBuffer origin=null;
        try{
            origin = new StringBuffer(path);
            origin.insert(path.lastIndexOf("/")+1, "tm_");
        }catch (Exception e){

        }
        return origin.toString();
    }


    public static void setImage(final Context context, String ImagePath, final ImageView image){

        Glide.with(context)
                .asBitmap()
                .load(ImagePath)
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        image.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                    if(resource==null){
                        image.setVisibility(View.GONE);
                    }else{
                        image.setVisibility(View.VISIBLE);
                        image.setImageBitmap(resource);
                    }

                        return false;
                    }
                })
                .into(image);

//                        new SimpleTarget<Bitmap>() {
//            @Override
//            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                Log.v("testbitmap","testbitmap1");
//                if(resource==null){
//                    image.setVisibility(View.GONE);
//                    Log.v("testbitmap","testbitmap2");
//                }else{
//                    image.setVisibility(View.VISIBLE);
//                    image.setImageBitmap(resource);
//                    Log.v("testbitmap","testbitmap3");
//                }
//            }
//        });

    }





    public static void setProfile(Context context, String PROFILE_PIC, ImageView profile){

        if(PROFILE_PIC==null||PROFILE_PIC.equals("")){
            Glide
                    .with(context)
                    .load(R.drawable.commu_m_03)
                    .into(profile);
        }else{
            Glide
                    .with(context)
                    .load(CommonFunction.getThumbnail(PROFILE_PIC))
                    .into(profile);

        }
    }

    public static void setProfileTeduri(Context context, String MBER_GRAD, ImageView profile_teduri){
        if(MBER_GRAD==null||!MBER_GRAD.equals("10")){
            Glide
                    .with(context)
                    .load(R.drawable.commu_s_01)
                    .into(profile_teduri);
        }else{
            Glide
                    .with(context)
                    .load(R.drawable.commu_s_02)
                    .into(profile_teduri);
        }

    }

    public static void setUpdateLoginInfo(Context mContext){
        MediNewNetworkModule.doReLoginMediCare(mContext, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {

            }
        });
    }



//    public static void setTextByParts(@NonNull List<String> parts, @NonNull TextView textView, @TextAlignment int alignment,Context context) {
//
//        final RoundedCornersBackgroundSpan.Builder builder = new RoundedCornersBackgroundSpan.Builder(context)
//                .setTextPaddingRes(R.dimen.px_12)
//                .setCornersRadiusRes(R.dimen.px_6);
//
////        for (int i = 0; i < parts.size(); i++) {
////
////            String part = parts.get(i);
////            final SpannableString string = new SpannableString(part);
////            final ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.WHITE);
////            string.setSpan(colorSpan, 0, string.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
////            builder.addTextPart(string, context.getResources().getColor(R.color.color_8f909e));
////
////            if(i+1!=parts.size())
////                builder.addTextPart("\n");
////        }
//
//        String part="";
//
//        for (int i = 0; i < parts.size(); i++) {
//            part += parts.get(i);
//            if(i+1!=parts.size())
//                part += ",";
//        }
//
//        final SpannableString string = new SpannableString(part);
//        final ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.WHITE);
//        string.setSpan(colorSpan, 0, string.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        builder.addTextPart(string, context.getResources().getColor(R.color.color_8f909e));
//        final Spannable firstText = builder.setTextAlignment(alignment).setPartsSpacingRes(R.dimen.px_12).build();
//        textView.append(firstText);
//
//    }


    public static void setTextByPartsString(@NonNull String part, @NonNull TextView textView, @TextAlignment int alignment, Context context) {

        final RoundedCornersBackgroundSpan.Builder builder = new RoundedCornersBackgroundSpan.Builder(context)
                .setTextPaddingRes(R.dimen.px_12)
                .setCornersRadiusRes(R.dimen.px_6);

        final SpannableString string = new SpannableString(part);
        final ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.WHITE);
        string.setSpan(colorSpan, 0, string.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.addTextPart(string, context.getResources().getColor(R.color.color_8f909e));
        final Spannable firstText = builder.setTextAlignment(alignment).setPartsSpacingRes(R.dimen.px_12).build();
        textView.setText(firstText);

    }

    public static String getDiseaseNM(String DISEASE_NM, String DISEASE_TXT){
        String disease="";

        switch (DISEASE_NM){
            case "1":
                disease = "고혈압";
                break;
            case "2":
                disease = "당뇨";
                break;
            case "3":
                disease = "고지혈증";
                break;
            case "4":
                disease = "비만";
                break;
            case "5":
                disease = "없음";
                break;
            default:
                disease = DISEASE_TXT;
                break;
        }

        return disease;
    }



    public static void openDialogRegisterNick(Context context,final DialogCommon.UpdateProfile updateProfile2) {
        Tr_login user = UserInfo.getLoginInfo();
        DialogCommon.showDialog(
                context,
                "",
                "Y",
                CommonFunction.getDiseaseNM(user.disease_nm,user.disease_txt),
                user.mber_sn,
                new DialogCommon.UpdateProfile() {
                    @Override
                    public void updateProfile(String NICK, String DISEASE_OPEN,String DISEASE_NM) {
                        updateProfile2.updateProfile(NICK,DISEASE_OPEN,DISEASE_NM);
                    }
                });
    }


}
