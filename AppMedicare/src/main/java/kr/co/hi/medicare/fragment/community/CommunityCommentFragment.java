package kr.co.hi.medicare.fragment.community;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.adapter.CommunityCommentListViewAdapter;
import kr.co.hi.medicare.fragment.community.adapter.LoadMoreListener;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.community.data.CommunityUserData;
import kr.co.hi.medicare.fragment.community.data.ReceiveDataCode;
import kr.co.hi.medicare.fragment.community.data.SendDataCode;
import kr.co.hi.medicare.fragment.community.holder.CommunityListViewHolder;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_DB003;
import kr.co.hi.medicare.net.data.Tr_DB004;
import kr.co.hi.medicare.net.data.Tr_DB005;
import kr.co.hi.medicare.net.data.Tr_DB007;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.EditTextUtil;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by mrsohn on 2017. 3. 14..
 * 혈당관리
 */

public class CommunityCommentFragment extends BaseFragmentMedi implements LoadMoreListener, SwipeRefreshLayout.OnRefreshListener, NewActivity.onKeyBackPressedListener  {
    private final String TAG = CommunityCommentFragment.class.getSimpleName();

    public static final int REQUEST_CODE_COMMENTINFO=2111;
    public static final String REQUEST_CODE_COMMDATA="REQUEST_CODE_COMMDATA";
    public static final String REQUEST_CODE_ISDEL="REQUEST_CODE_ISDEL";

    public static final String KEYPAD_STATUS = "KEYPAD_STATUS";
    public static final String POSTS_INFO="POSTS_INFO";
    public static final String PRE_PAGE = "PRE_PAGE";
    public static final String KEY_CMGUBUN = "KEY_CMGUBUN";

    //게시글
//    private CommunityListViewData data = null;
//    private CommunityListViewHolder communityListViewHolder = null;

    //탑레이아웃
    private TextView text_pre_page;
    private LinearLayout btn_back;
    private String prePage;

    //댓글쓰기
    private EditText edit_comment;
    private TextView reg_comment;
    private boolean IS_KEYPAD=false;
    private String T_MBER_SN="";

    //댓글리스트
    private CommunityCommentListViewAdapter mAdapter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefresh;
    private LinearLayoutManager mLayoutManager;
    private int TotalPage = 1;
    private int firstVisibleItem, visibleItemCount, totalItemCount;
    private int visibleThreshold = 0;
    private CommunityListViewData data;
    private Tr_login user = null;


    public static Fragment newInstance() {
        CommunityCommentFragment fragment = new CommunityCommentFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_community_comment, container, false);
        user = UserInfo.getLoginInfo();
        initView(view);
        return view;
    }

    private CommunityListViewData setBundleData() {
        data = new CommunityListViewData();
        Bundle bundle = getArguments();
        if (bundle != null) {
            IS_KEYPAD = bundle.getBoolean(KEYPAD_STATUS);
            data = (CommunityListViewData)bundle.get(POSTS_INFO);
            data.CM_GUBUN = bundle.getString(KEY_CMGUBUN);
            if(data.CM_GUBUN==null||data.CM_GUBUN.equals(""))
                data.CM_GUBUN= SendDataCode.CM_ALL;

            prePage = bundle.getString(PRE_PAGE);
            if(prePage==null||prePage.equals("")){
                prePage="";
            }
            this.text_pre_page.setText(prePage);

            if(IS_KEYPAD){
                doHideKeyPad=false;
                EditTextUtil.focusAndShowKeyboard(getContext(),edit_comment);
//                edit_comment.requestFocus();
//                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
//                imm.showSoftInput(edit_comment,0);
            }

        }
        return data;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //글쓰기 완료 후 다시 커뮤니티 메인 페이지로 이동 시 리스트뷰 갱신
        if(resultCode==RESULT_OK) {
            if (requestCode == CommunityWriteFragment.REQUEST_CODE_WRITEINFO) {
                getWriteInfoFromServer(mAdapter.getCommunityListViewData().CM_SEQ);
            }
        }
    }






    private void initView(View view) {
        edit_comment = view.findViewById(R.id.edit_comment);
        reg_comment = view.findViewById(R.id.reg_comment);
        text_pre_page = view.findViewById(R.id.text_pre_page);
        btn_back = view.findViewById(R.id.btn_back);
        btn_back.setOnClickListener(mOnClickListener);
        reg_comment.setOnClickListener(mOnClickListener);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CommunityListViewData data = setBundleData();

        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        recyclerView = view.findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new CommunityCommentListViewAdapter(getContext(),mOnClickListener,user.mber_sn,data,this);

        if(prePage!=null&&prePage.equals(getResources().getString(R.string.comm_title))){
            mAdapter.setViewType(CommunityListViewHolder.TYPE_VIEW_GONE);
        }

        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager llManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = llManager.getItemCount();
                firstVisibleItem = llManager.findFirstVisibleItemPosition();
                if (dy>0&&!mAdapter.getMore() && (totalItemCount - visibleItemCount)<= (firstVisibleItem + visibleThreshold)) {
                    Log.v("testrecyclerview","testrecyclerview:visibleitemcount:"+visibleItemCount+"   totalItemCount:"+totalItemCount+"   firstVisibleItem:"+firstVisibleItem );
                    onLoadMore();
                }
            }
        });

        swipeRefresh.setOnRefreshListener(this);



//            communityListViewHolder.SetView(data,CommunityListViewHolder.TYPE_VIEW,0);


        if(data.ISNODATA){
            getWriteInfoFromServer(data.CM_SEQ);
        }
        getListFromServer(PROGRESSBAR_TYPE_CENTER,"1",mAdapter.getCommunityListViewData().CM_SEQ);

    }

    @Override
    public void onRefresh() {
//        refreshListView("TOP",1);
        getListFromServer(PROGRESSBAR_TYPE_TOP,"1",mAdapter.getCommunityListViewData().CM_SEQ);

    }

    @Override
    public void onLoadMore() {
        mAdapter.setProgressMore(true);
        mAdapter.setMore(true);
        getListFromServer(PROGRESSBAR_TYPE_BOTTOM,Integer.toString(TotalPage+1),mAdapter.getCommunityListViewData().CM_SEQ);
//        refreshListView("BOTTOM",TotalPage+1);
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int viewId = v.getId();
            switch (viewId){
                case R.id.comment_nick:
                    if(user.nickname.equals(((TextView)v).getText().toString()))//자기자신태그는 불가
                        return;

                    edit_comment.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    imm.showSoftInput(edit_comment,0);

                    String nick = "@"+((TextView)v).getText().toString()+" ";

                    SpannableStringBuilder sp = new SpannableStringBuilder(nick);
                    sp.setSpan(new StyleSpan(Typeface.BOLD),0,nick.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    String edit = edit_comment.getText().toString();

                    if(edit.indexOf("@")==0&&edit.contains(" ")){
                        edit_comment.setText("");
                        edit_comment.append(sp);
                        edit_comment.append(edit.substring(edit.indexOf(" ")+1));
                    }else if(edit.indexOf("@")==0){
                        edit_comment.setText("");
                        edit_comment.append(sp);
                    }else if(!edit.equals("")){
                        edit_comment.setText("");
                        edit_comment.append(sp);
                        edit_comment.append(edit);
                    }else{
                        edit_comment.append(sp);
                    }

                    T_MBER_SN =  mAdapter.getItem(Integer.parseInt(v.getTag(R.id.comm_comment_nick).toString())).OSEQ;

                   break;

                case R.id.reg_comment:
                    if(user.nickname.equals("")){
                        CDialog.showDlg(getContext(), "커뮤니티 닉네임을 등록하여야 댓글작성이 가능합니다.");
                    } else if(!edit_comment.getText().toString().equals("")){
                        reg_comment.setEnabled(false);
                        sendCommentToServer(edit_comment.getText().toString(),T_MBER_SN, mAdapter.getCommunityListViewData().CM_SEQ);
                    }
                    break;


                case R.id.btn_back:
                    onBack();
                    onBackPressed();
                    break;


                case R.id.btn_delete:

                    final int position =  Integer.parseInt(v.getTag(R.id.comm_comment_delete).toString());
//                    commonDialog = DialogCommon.showDialogCommon(getContext(), getResources().getString(R.string.comm_confirm),  getResources().getString(R.string.comm_cancel), getResources().getString(R.string.comm_comment_delete),
//                            new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    commonDialog.dismiss();
//                                    sendDeleteCommentToServer(mAdapter.getItem(position).CC_SEQ);
//                                }
//                            },new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    commonDialog.dismiss();
//                                }
//                            });


                    CDialog.showDlg(getContext(), getResources().getString(R.string.comm_comment_delete))
                            .setOkButton(getResources().getString(R.string.text_confirm), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
//                                    commonDialog.dismiss();
                                    sendDeleteCommentToServer(mAdapter.getItem(position).CC_SEQ);
                                }
                            })
                            .setNoButton(getResources().getString(R.string.comm_cancel), null);
                    break;

                case R.id.profile:
                    CommunityUserData userData2 = new CommunityUserData();
                    userData2.NICKNAME = mAdapter.getCommunityListViewData().NICK;
                    userData2.MBER_SN = mAdapter.getCommunityListViewData().MBER_SN;
                    userData2.PROFILE_PIC = mAdapter.getCommunityListViewData().PROFILE_PIC;
                    NewActivity.moveToProfilePage(getString(R.string.comm_comment), userData2,getActivity());
                    break;


                case R.id.comment_profile:
                    int position2 =  Integer.parseInt(v.getTag(R.id.comm_comment_profile).toString());
                    CommunityUserData userData = new CommunityUserData();
                    userData.NICKNAME = mAdapter.getItem(position2).NICK;
                    userData.MBER_SN = mAdapter.getItem(position2).OSEQ;
                    userData.PROFILE_PIC = mAdapter.getItem(position2).PROFILE_PIC;
                    NewActivity.moveToProfilePage(getString(R.string.comm_comment), userData,getActivity());
                    break;

            }

        }
    };

    private void clearEditText(){
        InputMethodManager imm2 = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        imm2.hideSoftInputFromWindow (edit_comment.getWindowToken(),0);
        edit_comment.setText("");
        T_MBER_SN="";
    }



    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        ((NewActivity)context).setOnKeyBackPressedListener(this);
    }

    @Override
    public void onBack() {
        Intent comm_info = new Intent();
        comm_info.putExtra(REQUEST_CODE_COMMDATA, mAdapter.getCommunityListViewData());
        getActivity().setResult(RESULT_OK, comm_info);
    }



    /**
     * 서버에 게시글 리스트 데이터 요청 DB002
     * @param loadingType 프로그레스바 로딩 타입
     * @param PG 요청 페이지
     * @param CM_SEQ 게시글일련번호
     */
    private void getListFromServer(final String loadingType,final String PG,final String CM_SEQ) {

        final Tr_DB003.RequestData requestData = new Tr_DB003.RequestData();
        boolean isShowProgress=false;


        requestData.PG_SIZE = PAGE_SIZE;
        requestData.PG = PG;
        requestData.SEQ = user.mber_sn;
        requestData.CM_SEQ = CM_SEQ;

        if(loadingType.equals(PROGRESSBAR_TYPE_CENTER))
            isShowProgress=true;

        MediNewNetworkModule.doApi(getContext(), new Tr_DB003(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                //하단 프로그레스바일 경우 프로그레스바 종료
                if(loadingType.equals(PROGRESSBAR_TYPE_BOTTOM))
                    mAdapter.setProgressMore(false);

                if (responseData instanceof Tr_DB003) {
                    Tr_DB003 data = (Tr_DB003)responseData;
                    try {

                        if(data.DATA!=null&&data.DATA.size()>0){

                            int requestPage = Integer.parseInt(PG);
                            int receivePage = Integer.parseInt(data.DATA.get(0).TPAGE);

                            if(requestPage<=receivePage){
                                TotalPage = requestPage;
                                if(TotalPage<2){//서버로부터 받은 페이지가 2보다 작으면 클린 후 추가
                                    mAdapter.addAllData(data.DATA);
                                }else{ //
                                    mAdapter.addItemMore(data.DATA);
                                }

                            }
                        }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
                switch (loadingType){
                    case PROGRESSBAR_TYPE_CENTER:
                        break;
                    case PROGRESSBAR_TYPE_BOTTOM:
                        mAdapter.setMore(false);
                        break;
                    case PROGRESSBAR_TYPE_TOP:
                        swipeRefresh.setRefreshing(false);
                        break;
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                Log.e(TAG, response);
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
                switch (loadingType){
                    case PROGRESSBAR_TYPE_CENTER:
                        break;
                    case PROGRESSBAR_TYPE_BOTTOM:
                        mAdapter.setMore(false);
                        mAdapter.setProgressMore(false);
                        break;
                    case PROGRESSBAR_TYPE_TOP:
                        swipeRefresh.setRefreshing(false);
                        break;
                }

            }
        });
    }


    /**
     * 댓글쓰기 DB004
     * @param T_MBER_SN 언급회원일련번호
     * @param CC_CONTENT 게시글
     * @param CM_SEQ 게시글일련번호
     */
    private void sendCommentToServer(final String CC_CONTENT,final String T_MBER_SN,final String CM_SEQ) {
        final Tr_DB004.RequestData requestData = new Tr_DB004.RequestData();
        boolean isShowProgress=true;

        requestData.CC_CONTENT = CC_CONTENT;
        requestData.T_MBER_SN = T_MBER_SN;
        requestData.SEQ = user.mber_sn;
        requestData.CM_SEQ = CM_SEQ;

        MediNewNetworkModule.doApi(getContext(), new Tr_DB004(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_DB004) {
                    Tr_DB004 data = (Tr_DB004)responseData;
                    try {
                        reg_comment.setEnabled(true);

                        switch (data.RESULT_CODE){
                            case ReceiveDataCode.DB004_SUCCESS:
                                mAdapter.setDataComment(0,"1");
                                getListFromServer(PROGRESSBAR_TYPE_CENTER,"1",CM_SEQ);
                                clearEditText();
                                mLayoutManager.scrollToPosition(1);

                                break;
                            case ReceiveDataCode.DB004_ERROR_ALREADY:

                                errorMessage = getResources().getString(R.string.comm_error_db004_4444);
                                break;
                            case ReceiveDataCode.DB004_ERROR_NOMEMBER:
                                errorMessage = getResources().getString(R.string.comm_error_db004_6666);

                                break;
                            case ReceiveDataCode.DB004_ERROR_REJECTMEMBER:
                                errorMessage = getResources().getString(R.string.comm_error_db004_7777);

                                break;
                            case ReceiveDataCode.DB004_ERROR_ETC:
                                errorMessage = getResources().getString(R.string.comm_error_db004_9999);
                                break;
                            default:
                                errorMessage = getResources().getString(R.string.comm_error_db004)+"\ncode:1";
                                break;
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                        errorMessage = getResources().getString(R.string.comm_error_db004)+"\ncode:2";
                        reg_comment.setEnabled(true);
                    }
                }else{
                    errorMessage = getResources().getString(R.string.comm_error_db004)+"\ncode:3";
                    reg_comment.setEnabled(true);
                }

                if(!errorMessage.equals("")){
                    reg_comment.setEnabled(true);
//                    commonDialog = DialogCommon.showDialogSingleBtn(getContext(), errorMessage, getResources().getString(R.string.comm_confirm), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            commonDialog.dismiss();
//                        }
//                    });
                    CDialog.showDlg(getContext(), errorMessage);
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
                reg_comment.setEnabled(true);
//                Log.e(TAG, response);
            }
        });
    }



    /**
     * 댓글쓰기 DB004
     * @param CC_SEQ 게시글일련번호
     */
    private void sendDeleteCommentToServer(final String CC_SEQ) {
        final Tr_DB005.RequestData requestData = new Tr_DB005.RequestData();
        boolean isShowProgress=true;

        requestData.SEQ = user.mber_sn;
        requestData.CC_SEQ = CC_SEQ;

        MediNewNetworkModule.doApi(getContext(), new Tr_DB005(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_DB005) {
                    Tr_DB005 data = (Tr_DB005)responseData;
                    try {

                        switch (data.RESULT_CODE){
                            case ReceiveDataCode.DB005_SUCCESS:
                                mAdapter.deleteItem(CC_SEQ);
                                break;
                            case ReceiveDataCode.DB005_ERROR_FAIL:
                                errorMessage = getResources().getString(R.string.comm_error_db005_4444);
                                break;
                            case ReceiveDataCode.DB005_ERROR_NOMEMBER:
                                errorMessage = getResources().getString(R.string.comm_error_db005_6666);
                                break;
                            case ReceiveDataCode.DB005_ERROR_ETC:
                                errorMessage = getResources().getString(R.string.comm_error_db005_9999);
                                break;
                            default:
                                errorMessage = getResources().getString(R.string.comm_error_db005)+"\ncode:1";
                                break;
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                        errorMessage = getResources().getString(R.string.comm_error_db005)+"\ncode:2";
                    }
                }else{
                    errorMessage = getResources().getString(R.string.comm_error_db005)+"\ncode:3";
                }

                if(!errorMessage.equals("")){
//                    commonDialog = DialogCommon.showDialogSingleBtn(getContext(), errorMessage, getResources().getString(R.string.comm_confirm), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            commonDialog.dismiss();
//                        }
//                    });

                    CDialog.showDlg(getContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                Log.e(TAG, response);
            }
        });
    }





    private void getWriteInfoFromServer(final String CM_SEQ) {
        final Tr_DB007.RequestData requestData = new Tr_DB007.RequestData();
        boolean isShowProgress=true;

        requestData.SEQ = user.mber_sn;
        requestData.CM_SEQ = CM_SEQ;

        MediNewNetworkModule.doApi(getContext(), new Tr_DB007(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_DB007) {
                    Tr_DB007 data = (Tr_DB007)responseData;
                    try {

                        switch (data.RESULT_CODE){
                            case ReceiveDataCode.DB007_SUCCESS:
                                CommunityListViewData communityListViewData = new CommunityListViewData();
                                communityListViewData.RCNT = data.RCNT;
                                communityListViewData.HCNT = data.HCNT;
                                communityListViewData.REGDATE =data.REGDATE;
                                communityListViewData.CM_TITLE = data.CM_TITLE;
                                communityListViewData.NICK = data.NICK;
                                communityListViewData.CM_SEQ = CM_SEQ;
                                communityListViewData.CM_IMG1="";
                                communityListViewData.PROFILE_PIC = data.PROFILE_PIC;
                                communityListViewData.CM_CONTENT = data.CM_CONTENT;
                                communityListViewData.CM_TAG = data.CM_TAG;
                                communityListViewData.MYHEART = data.MYHEART;
                                communityListViewData.MBER_SN = data.OSEQ;
                                communityListViewData.MBER_GRAD = data.MBER_GRAD;
                                communityListViewData.CM_GUBUN = data.CM_GUBUN;
                                communityListViewData.CM_MEAL = data.CM_MEAL;
                                mAdapter.setCommunityListViewData(communityListViewData);
                                break;
                            case ReceiveDataCode.DB007_ERROR_NOHAVE:
                                errorMessage = getResources().getString(R.string.comm_error_db007_4444);
                                break;
                            case ReceiveDataCode.DB007_ERROR_NOMEMBER:
                                errorMessage = getResources().getString(R.string.comm_error_db007_6666);
                                break;
                            case ReceiveDataCode.DB007_ERROR_ETC:
                                errorMessage = getResources().getString(R.string.comm_error_db007_9999);
                                break;
                            default:
                                errorMessage = getResources().getString(R.string.comm_error_db007)+"\ncode:1";
                                break;
                        }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                        errorMessage = getResources().getString(R.string.comm_error_db007)+"\ncode:2";
                    }
                }else{
                    errorMessage = getResources().getString(R.string.comm_error_db007)+"\ncode:3";
                }

                if(!errorMessage.equals("")){
                    CDialog.showDlg(getContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                Log.e(TAG, response);
            }
        });
    }



}
