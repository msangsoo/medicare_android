package kr.co.hi.medicare.net.data;


import kr.co.hi.medicare.net.hwNet.BaseData;

public interface MediNewNetworkHandler {
    void onSuccess(BaseData responseData);

    void onFailure(int statusCode, String response, Throwable error);

}
