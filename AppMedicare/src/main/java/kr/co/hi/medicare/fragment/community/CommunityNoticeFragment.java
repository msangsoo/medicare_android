package kr.co.hi.medicare.fragment.community;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.adapter.CommunityAlramAdapter;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.community.data.CommunityNoticeData;
import kr.co.hi.medicare.fragment.community.data.ReceiveDataCode;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_DB007;
import kr.co.hi.medicare.net.data.Tr_DB014;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;

import static android.app.Activity.RESULT_OK;

/**
 * Created by mrsohn on 2017. 3. 14..
 * 혈당관리
 */

public class CommunityNoticeFragment extends BaseFragmentMedi implements NewActivity.onKeyBackPressedListener {
    private final String TAG = CommunityNoticeFragment.class.getSimpleName();

    public static final int REQUEST_CODE_NOTICE=9840;


    private DBHelper mDbHelper;
    private LinearLayout btn_back;
    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private TextView text_pre_page;

    private CommunityAlramAdapter communityAlramAdapter;
    Tr_login user = null;

    public static Fragment newInstance() {
        CommunityNoticeFragment fragment = new CommunityNoticeFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        ((NewActivity)context).setOnKeyBackPressedListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_community_notice, container, false);

        mDbHelper = new DBHelper(getContext());
        user = UserInfo.getLoginInfo();
        btn_back = view.findViewById(R.id.btn_back);
        btn_back.setOnClickListener(mOnClickListener);
        recyclerView = view.findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        communityAlramAdapter = new CommunityAlramAdapter(getContext(),mOnClickListener);
        recyclerView.setAdapter(communityAlramAdapter);

        //백타이틀 수정가능하게
        text_pre_page = view.findViewById(R.id.text_pre_page);

        if (getArguments() != null) {
            Bundle bundle = getArguments();
            String backTitle = bundle.getString("BACKTITLE");
            text_pre_page.setText(backTitle);
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        checkAlramFromServer();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //글쓰기 완료 후 다시 커뮤니티 메인 페이지로 이동 시 리스트뷰 갱신
        if(resultCode==RESULT_OK) {
            if(requestCode == CommunityCommentFragment.REQUEST_CODE_COMMENTINFO){
//                CommunityListViewData comm_data =(CommunityListViewData)data.getSerializableExtra(CommunityCommentFragment.REQUEST_CODE_COMMDATA);
//
//                boolean isDel = false;
//                try{
//                    isDel = data.getBooleanExtra(CommunityCommentFragment.REQUEST_CODE_ISDEL,false);
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//
//
//                if(communityAlramAdapter!=null){
//
//                    if(isDel)
//                        communityAlramAdapter.deleteItem(comm_data.CM_SEQ);
//
//                }
            }
        }
    }


    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int viewId = v.getId();
            Bundle bundle = new Bundle();
            switch (viewId){
                case R.id.btn_back:
                    onBackPressed();
                    break;
                case R.id.profile:
                case R.id.msg:
                    int position = Integer.parseInt(v.getTag(R.id.comm_alram).toString());
                    if(user.nickname.equals("")){
                        CDialog.showDlg(getContext(), "커뮤니티 닉네임을 등록하여야 커뮤니티 진입이 가능합니다.")
                                .setOkButton(getString(R.string.confirm), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        onBackPressed();
                                    }
                                });
                    } else {
                        getListFromServer(communityAlramAdapter.getData(position).CM_SEQ);
                    }
                    break;
            }

        }
    };

    private void getDataFromDB(){

        List<CommunityNoticeData> data = mDbHelper.getmCommunityNotice().getResultAll(mDbHelper);

        if(data!=null&&data.size()>0){
            communityAlramAdapter.addAllItem(data);
        }

    }


    private void setIsNewFalse(){
        mDbHelper.getmCommunityNotice().updateIsNew();
    }


    private void checkAlramFromServer() {
        final Tr_DB014.RequestData requestData = new Tr_DB014.RequestData();
        boolean isShowProgress=false;

        requestData.SEQ = user.mber_sn;

        MediNewNetworkModule.doApi(getContext(), new Tr_DB014(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_DB014) {
                    Tr_DB014 data = (Tr_DB014)responseData;
                    try {

                        switch (data.RESULT_CODE){
                            case ReceiveDataCode.DB014_SUCCESS:

                                int totalCount =  data.DATA.size();


                                if(totalCount>0) {
                                    if(totalCount>50)
                                        totalCount=50;

                                    int ALM_SEQ = Integer.parseInt(mDbHelper.getmCommunityNotice().getMaxALMSEQ(mDbHelper));

                                    for(int i=0; i<totalCount;i++){
                                        int alm_seq = 0;

                                        try{
                                            if(data.DATA.get(i).ALM_SEQ==null|| data.DATA.get(i).ALM_SEQ.equals(""))
                                                continue;

                                            alm_seq = Integer.parseInt(data.DATA.get(i).ALM_SEQ);
                                        }catch (Exception e){
                                            continue;
                                        }

                                        if(alm_seq>ALM_SEQ){
                                            mDbHelper.getmCommunityNotice().insert(data.DATA.get(i).REGDATE,data.DATA.get(i).MSG, data.DATA.get(i).CM_SEQ, data.DATA.get(i).ALM_SEQ , data.DATA.get(i).PROFILE_PIC, data.DATA.get(i).MBER_GRAD, data.DATA.get(i).CM_GUBUN, data.DATA.get(i).MBER_SN, data.DATA.get(i).NICK);
                                        }else{
                                            mDbHelper.getmCommunityNotice().update(data.DATA.get(i).REGDATE,data.DATA.get(i).MSG, data.DATA.get(i).CM_SEQ, data.DATA.get(i).ALM_SEQ, data.DATA.get(i).PROFILE_PIC, data.DATA.get(i).MBER_GRAD, data.DATA.get(i).CM_GUBUN, data.DATA.get(i).MBER_SN, data.DATA.get(i).NICK);
                                        }
                                    }
                                }

                                break;
                            case ReceiveDataCode.DB014_ERROR_ETC:
//                                errorMessage = getResources().getString(R.string.comm_error_common_9999);
                                break;
                            default:
//                                errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:1";
                                break;
                        }


                    } catch (Exception e) {
                        CLog.e(e.toString());
//                        errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:2";
                    }
                }else{
//                    errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:3";
                }

                if(!errorMessage.equals("")){
//                    dialog = DialogCommon.showDialogSingleBtn(getContext(), errorMessage, getResources().getString(R.string.comm_confirm), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialog.dismiss();
//                        }
//                    });
                    CDialog.showDlg(getContext(), errorMessage)
                            .setOkButton(getResources().getString(R.string.comm_confirm), null);
                }

                setIsNewFalse();
                getDataFromDB();
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                Log.e(TAG, response);

                setIsNewFalse();
                getDataFromDB();

            }
        });
    }


    private void getListFromServer(final String CM_SEQ) {
        final Tr_DB007.RequestData requestData = new Tr_DB007.RequestData();
        boolean isShowProgress=true;

        requestData.SEQ = user.mber_sn;
        requestData.CM_SEQ = CM_SEQ;

        MediNewNetworkModule.doApi(getContext(), new Tr_DB007(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";


                if (responseData instanceof Tr_DB007) {
                    Tr_DB007 data = (Tr_DB007)responseData;
                    try {

                        switch (data.RESULT_CODE){
                            case ReceiveDataCode.DB007_SUCCESS:
                                CommunityListViewData communityListViewData = new CommunityListViewData();
                                communityListViewData.RCNT = data.RCNT;
                                communityListViewData.HCNT = data.HCNT;
                                communityListViewData.REGDATE =data.REGDATE;
                                communityListViewData.CM_TITLE = data.CM_TITLE;
                                communityListViewData.NICK = data.NICK;
                                communityListViewData.CM_SEQ = CM_SEQ;
                                communityListViewData.CM_IMG1="";
                                communityListViewData.PROFILE_PIC = data.PROFILE_PIC;
                                communityListViewData.CM_CONTENT = data.CM_CONTENT;
                                communityListViewData.CM_TAG = data.CM_TAG;
                                communityListViewData.MYHEART = data.MYHEART;
                                communityListViewData.MBER_SN = data.OSEQ;
                                communityListViewData.MBER_GRAD = data.MBER_GRAD;
                                communityListViewData.CM_GUBUN = data.CM_GUBUN;
                                communityListViewData.CM_MEAL = data.CM_MEAL;

                                NewActivity.moveToCommentPage(CommunityNoticeFragment.this,communityListViewData,false,getString(R.string.comm_alram),communityListViewData.CM_GUBUN);

                                break;
                            case ReceiveDataCode.DB007_ERROR_NOHAVE:
                                errorMessage = getResources().getString(R.string.comm_error_db007_4444);
                                break;
                            case ReceiveDataCode.DB007_ERROR_NOMEMBER:
                                errorMessage = getResources().getString(R.string.comm_error_db007_6666);
                                break;
                            case ReceiveDataCode.DB007_ERROR_ETC:
                                errorMessage = getResources().getString(R.string.comm_error_db007_9999);
                                break;
                            default:
                                errorMessage = getResources().getString(R.string.comm_error_db007)+"\ncode:1";
                                break;
                        }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                        errorMessage = getResources().getString(R.string.comm_error_db007)+"\ncode:2";
                    }
                }else{
                    errorMessage = getResources().getString(R.string.comm_error_db007)+"\ncode:3";
                }

                if(!errorMessage.equals("")){
                    CDialog.showDlg(getContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                Log.e(TAG, response);
            }
        });
    }

    @Override
    public void onBack() {
        getActivity().setResult(RESULT_OK, null);
    }
}
