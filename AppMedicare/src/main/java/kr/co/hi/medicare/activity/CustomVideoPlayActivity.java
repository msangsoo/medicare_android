package kr.co.hi.medicare.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;

import java.io.UnsupportedEncodingException;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.util.CLog;

/**
 * Created by jihoon on 2016-05-31.
 * 맞춤 동영상 플레이 화면
 * @since 0, 1
 */
public class CustomVideoPlayActivity extends BaseActivityMedicare implements UniversalVideoView.VideoViewCallback {

    private static final String SEEK_POSITION_KEY = "SEEK_POSITION_KEY";
    private static final String VIDEO_URL = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";

    // 탑
    private LinearLayout mMonthLayout, mTabLayout;

    // 재활운동 재생전
    private LinearLayout mExerciseBeforeLayout;
    private TextView mExerciseWarningTv, mExerciseAwarenessTv;
    private Button mExercisePlayBtn;
    private ImageView mExerciseImg;

    // 재활운동 재생후
    private LinearLayout mExerciseAfterLayout, mExerciseDescLayout;

    // 동영상
    private View mVideoLayout;
    private UniversalVideoView mVideoView;
    private UniversalMediaController mMediaController;

    // 동영상 사이즈 관련
    private int mSeekPosition;
    private int cachedHeight;
    private boolean isFullscreen;


    private TextView mTitleTv , mMvContentTv , mMvTitleTv;
    private Intent intent;
    private String mTitle, mMvContent, mMvTitle, mMvUrl;
    private View view;
    private String mType; // 동영상 타입 E = 재활운동 , R = 특별레시피

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_playvideo);
        init();
        setEvent();
    }

    @Override
    protected void onPause() {
        super.onPause();
        CLog.d("onPause ");
        if (mVideoView != null && mVideoView.isPlaying()) {
            mSeekPosition = mVideoView.getCurrentPosition();
            CLog.d("onPause mSeekPosition=" + mSeekPosition);
            mVideoView.pause();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        CLog.d("onSaveInstanceState Position=" + mVideoView.getCurrentPosition());
        outState.putInt(SEEK_POSITION_KEY, mSeekPosition);
    }

    @Override
    protected void onRestoreInstanceState(Bundle outState) {
        super.onRestoreInstanceState(outState);
        mSeekPosition = outState.getInt(SEEK_POSITION_KEY);
        CLog.d("onRestoreInstanceState Position=" + mSeekPosition);
    }

    /**
     * 초기화
     */
    public void init(){

        intent = getIntent();
        if (intent != null){
            mMvTitle = intent.getStringExtra(EXTRA_MV_CATE_TITLE);
            mMvContent = intent.getStringExtra(EXTRA_MV_CONTENT);
            mMvUrl = intent.getStringExtra(EXTRA_MV_URL);
            mType = intent.getStringExtra(EXTRA_TYPE);
        }

//        mMonthLayout            =   (LinearLayout)  findViewById(R.id.month_layout);
//        mTabLayout              =   (LinearLayout)  findViewById(R.id.tab_layout);

//        mExerciseBeforeLayout   =   (LinearLayout)  findViewById(R.id.exercise_before_play_layout);
//        mExerciseAfterLayout    =   (LinearLayout)  findViewById(R.id.exercise_after_play_layout);
//        mExerciseDescLayout     =   (LinearLayout)  findViewById(R.id.exercise_desc_layout);

        mExerciseWarningTv      =   (TextView)      findViewById(R.id.exercise_warning_tv); // 경고
        mExerciseAwarenessTv    =   (TextView)      findViewById(R.id.exercise_awareness_tv);   // 운동자각도
        mExercisePlayBtn        =   (Button)        findViewById(R.id.exercise_play_btn);
        mExerciseImg            =   (ImageView)     findViewById(R.id.exercise_img_view);

        mTitleTv                =   (TextView)      findViewById(R.id.common_toolbar_title);   // 제목
        mMvTitleTv              =   (TextView)      findViewById(R.id.mvtitle_tv);   // 동영상제목
        mMvContentTv            =   (TextView)      findViewById(R.id.mvcontent_tv);   // 동영상내용

        view                    =   (View)          findViewById(R.id.view);

        if (mType.equals("E")){
            mTitleTv.setText(getString(R.string.exercise) + "(" + mMvTitle + ")");
        }else {
            mTitleTv.setText(getString(R.string.special_recipe) + "(" + mMvTitle + ")");
        }

        mMvTitleTv.setText(mMvTitle);
        mMvContentTv.setText(mMvContent);


        // 동영상
        mVideoLayout = findViewById(R.id.exercise_video_layout);
        mVideoView = (UniversalVideoView) findViewById(R.id.exercise_videoView);
        mMediaController = (UniversalMediaController) findViewById(R.id.exercise_media_controller);
        mVideoView.setMediaController(mMediaController);
        setVideoAreaSize();

    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        mVideoView.setVideoViewCallback(this);

        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() { // 재생 완료시 기본화면 돌아가기
            @Override
            public void onCompletion(MediaPlayer mp) {

                finish();
//                mExerciseBeforeLayout.setVisibility(View.VISIBLE);
//                mExerciseAfterLayout.setVisibility(View.GONE);
//
//                mExerciseDescLayout.setVisibility(View.VISIBLE);
//                mMonthLayout.setVisibility(View.VISIBLE);
//                mTabLayout.setVisibility(View.VISIBLE);
            }
        });

        if (mSeekPosition > 0) {
            mVideoView.seekTo(mSeekPosition);
        }
        mVideoView.start();
//        mMediaController.setTitle(mMvTitle);

//        mExerciseBeforeLayout.setVisibility(View.GONE);
//        mExerciseAfterLayout.setVisibility(View.VISIBLE);

    }

    /**
     * 비디오 뷰 사이즈 조절
     */
    private void setVideoAreaSize() {
        mVideoLayout.post(new Runnable() {
            @Override
            public void run() {
                int width = 1080;
                CLog.d("width = " +width);
                cachedHeight = (int) (width * 405f / 720f);
                CLog.d("cachedHeight = " +cachedHeight);
                ViewGroup.LayoutParams videoLayoutParams = mVideoLayout.getLayoutParams();
                videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                videoLayoutParams.height = cachedHeight;
                mVideoLayout.setLayoutParams(videoLayoutParams);
                mVideoView.setVideoPath(mMvUrl);
                mVideoView.requestFocus();
            }
        });
    }

    @Override
    public void response(Record record) throws UnsupportedEncodingException {

    }

    @Override
    public void request(EServerAPI eServerAPI, Object obj) {

    }

    @Override
    public void networkException(Record record) {

    }

    public void click_event(View v) {
        Intent intent;
        switch (v.getId()) {
        }
    }

    @Override
    public void onScaleChange(boolean bool) {
        this.isFullscreen = bool;
        if (isFullscreen) {
            ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mVideoLayout.setLayoutParams(layoutParams);

//            mExerciseDescLayout.setVisibility(View.GONE);
//            mMonthLayout.setVisibility(View.GONE);
//            mTabLayout.setVisibility(View.GONE);


        } else {
            ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.height = this.cachedHeight;
            mVideoLayout.setLayoutParams(layoutParams);

//            mExerciseDescLayout.setVisibility(View.VISIBLE);
//            mMonthLayout.setVisibility(View.VISIBLE);
//            mTabLayout.setVisibility(View.VISIBLE);

        }

        switchTitleBar(!isFullscreen);
    }

    /**
     * 네비바 활성 유무
     * @param show ( true - 활성, false - 비활성 )
     */
    private void switchTitleBar(boolean show) {
        CommonToolBar supportActionBar = (CommonToolBar) findViewById(R.id.toolbar);
        if (supportActionBar != null) {
            CLog.d("supportActionBar != null");
            if (show) {
                supportActionBar.setVisibility(View.VISIBLE);
                view.setVisibility(View.VISIBLE);
            } else {
                supportActionBar.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
            }
        }else{
            CLog.d("supportActionBar == null");
        }

    }

    @Override
    public void onPause(MediaPlayer mediaPlayer) {
        CLog.d("onPause");
    }

    @Override
    public void onStart(MediaPlayer mediaPlayer) {
        CLog.d("onStart");
    }

    @Override
    public void onBufferingStart(MediaPlayer mediaPlayer) {
        CLog.d("onBufferingStart");
    }

    @Override
    public void onBufferingEnd(MediaPlayer mediaPlayer) {
        CLog.d("onBufferingEnd");
    }

    @Override
    public void onBackPressed() {
        if (this.isFullscreen) {
            mVideoView.setFullscreen(false);
        } else {
            super.onBackPressed();
        }
    }
}
