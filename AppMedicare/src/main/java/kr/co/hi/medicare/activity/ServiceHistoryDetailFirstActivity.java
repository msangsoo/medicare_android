package kr.co.hi.medicare.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.Defined;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.adapter.HistoryFirstListView_Adapter;
import kr.co.hi.medicare.bean.ServiceFirstItem;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.AlertDialogUtil;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.JsonUtil;
import kr.co.hi.medicare.util.Util;

/**
 * Created by suwun on 2016-06-16.
 * 서비스 이력보기  (면역세포보관 / 면역력 검사 / PEC-TC)
 * @since 0, 1
 */
public class ServiceHistoryDetailFirstActivity extends BaseActivityMedicare {

    private UserInfo user;
    private TextView mTopTv1 , mTopTv2 , mTitleTv;
    private ListView mListView;
    private int mType; // 1 면역세포보관 , 3 면역력 검사 , 4 PEC-TC
    private Intent intent;

    private ArrayList<ServiceFirstItem> mItem = new ArrayList<ServiceFirstItem>();
    private HistoryFirstListView_Adapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_history_first);
        user = new UserInfo(this);

        init();
    }

    private void init() {

        intent = getIntent();
        if (intent != null){
            mType = intent.getIntExtra(EXTRA_SERVICETYPE , 0);
        }

        CLog.i("mType --> " + mType);


        mTitleTv        = (TextView) findViewById(R.id.title);
        mTopTv1         = (TextView) findViewById(R.id.top_tv1);
        mTopTv2         = (TextView) findViewById(R.id.top_tv2);
        mListView       = (ListView) findViewById(R.id.data_list);

//        mAdapter = new HistoryFirstListView_Adapter(ServiceHistoryDetailFirstActivity.this , mItem, mType);
//        mListView.setAdapter(mAdapter);

        requestHistoryApi(mType);

    }


    /**
     * api 호출
     * @param mtype // 1 면역세포보관 , 3 면역력 검사 , 4 PEC-TC
     */

    public void requestHistoryApi(int mtype) {

        /*
        http -> https 변경 16-06-27

        CLog.i("mtype --> " + mtype);
        JSONObject jObject = new JSONObject();
        try {
            jObject.put("SEQ", user.getSeq());      // 회원일렬번호
        } catch (JSONException e) {
            e.printStackTrace();
        }
        switch (mtype){
            case 1:
                request(EServerAPI.API_POST, RequestUtil.getJSONParameter(jObject, "DZ003"));
                break;
            case 3:
                request(EServerAPI.API_POST, RequestUtil.getJSONParameter(jObject, "DZ005"));
                break;
            case 4:
                request(EServerAPI.API_POST, RequestUtil.getJSONParameter(jObject, "DZ006"));
                break;
        }
        */

        try {
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            client.setTimeout(10000);
            client.addHeader("Accept-Charset", "UTF-8");
            client.addHeader("User-Agent", new WebView(getBaseContext()).getSettings().getUserAgentString());
            Tr_login login = UserInfo.getLoginInfo();
            JSONObject jObject = new JSONObject();
            if(BuildConfig.DEBUG){
                jObject.put("SEQ", "0001013000015");
            }else{
                jObject.put("SEQ", login.seq);
            }


            switch (mtype){
                case 1:
                    jObject.put("DOCNO", "DZ003");
                    break;
                case 3:
                    jObject.put("DOCNO", "DZ005");
                    break;
                case 4:
                    jObject.put("DOCNO", "DZ006");
                    break;
            }

            params.put("strJson", jObject.toString());
            CLog.i("url = " + Defined.COMMUNITY_HTTPS_URL +"?" +params.toString());
            client.post(Defined.COMMUNITY_HTTPS_URL, params, mAsyncHttpHandler);
        }catch(Exception e){
            CLog.e(e.toString());
        }

    }

    private AsyncHttpResponseHandler mAsyncHttpHandler = new AsyncHttpResponseHandler() {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            CLog.i("onSuccess");

            try {

                int count = headers.length;

                for (int i = 0; i < count; i++) {
                    CLog.v(headers[i].getName() + ": " + headers[i].getValue());
                }

                String response = new String(responseBody, "UTF-8");
                CLog.i("response = " +response);
                JSONObject resultData = null;

                if(response.startsWith("<")){   // api result 문자열 시작값이 "<" 로 시작하는 경우는 xml 폼
                    try {
                        response =  Util.parseXml(response);
                    }catch(Exception e){
                        CLog.e(e.toString());
                    }

                    resultData = new JSONObject(response);

                }else{
                    resultData = new JSONObject(response);
                }

                Intent intent;

                String resultCode = resultData.getString("DOCNO");
                switch(resultCode){

                    case "DZ003": //
                        try {

                            String resultLength = (String) resultData.get("DATA_LENGTH");

                            if (!resultLength.equals("0")) {
                                JSONArray jsonArray = new JSONArray(resultData.getString("DATA"));
                                JSONObject object1 = jsonArray.getJSONObject(0);

                                switch (object1.getString("RESULT_CODE")) {
                                    case "0000":
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject object = jsonArray.getJSONObject(i);
                                            ServiceFirstItem item = new ServiceFirstItem(object.getString("IC_SEQ"),
                                                    object.getString("REQDATE"),
                                                    object.getString("USEDATE"),
                                                    object.getString("SO_NAME"),
                                                    object.getString("USE_MEMO"),
                                                    object.getString("OSEQ"),
                                                    object.getString("RESULT_CODE"));
                                            mItem.add(item);

                                        }
                                        break;
                                    case "4444":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record)  , true);
                                        break;
                                    case "6666":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record)  , true);
                                        break;
                                    case "9999":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.no_user) , true);
                                        break;

                                }
                                UiThread();
                                break;
                            } else {
                                UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.meal_tok_notdata) , true);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case "DZ005":
                        try {

                            String resultLength = (String) resultData.get("DATA_LENGTH");

                            if (!resultLength.equals("0")) {
                                JSONArray jsonArray = new JSONArray(resultData.getString("DATA"));
                                JSONObject object1 = jsonArray.getJSONObject(0);

                                switch (object1.getString("RESULT_CODE")) {
                                    case "0000":
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject object = jsonArray.getJSONObject(i);
                                            ServiceFirstItem item = new ServiceFirstItem(object.getString("IM_SEQ"),
                                                    object.getString("REQDATE"),
                                                    object.getString("USEDATE"),
                                                    object.getString("SO_NAME"),
                                                    object.getString("USE_MEMO"),
                                                    object.getString("OSEQ"),
                                                    object.getString("RESULT_CODE"));
                                            mItem.add(item);

                                        }
                                        break;
                                    case "4444":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "6666":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "9999":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.no_user) , true);
                                        break;

                                }
                                UiThread();
                                break;
                            } else {
                                UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.meal_tok_notdata) , true);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case "DZ006":
                        try {

                            String resultLength = (String) resultData.get("DATA_LENGTH");

                            if (!resultLength.equals("0")) {
                                JSONArray jsonArray = new JSONArray(resultData.getString("DATA"));
                                JSONObject object1 = jsonArray.getJSONObject(0);

                                switch (object1.getString("RESULT_CODE")) {
                                    case "0000":
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject object = jsonArray.getJSONObject(i);
                                            ServiceFirstItem item = new ServiceFirstItem(object.getString("PC_SEQ"),
                                                    object.getString("REQDATE"),
                                                    object.getString("USEDATE"),
                                                    object.getString("SO_NAME"),
                                                    object.getString("USE_MEMO"),
                                                    object.getString("OSEQ"),
                                                    object.getString("RESULT_CODE"));
                                            mItem.add(item);

                                        }
                                        break;
                                    case "4444":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "6666":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "9999":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.no_user) , true);
                                        break;

                                }
                                UiThread();
                                break;
                            } else {
                                UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.meal_tok_notdata) , true);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }


            } catch (Exception e) {
                CLog.e(e.toString());
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            CLog.i("onFailure");
            UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.networkexception) , true);

        }
    };


    public void click_event(View v) {

        switch (v.getId()) {
            // 뒤로
            case R.id.activity_actrecord_ImageView_back:
                finish();
                break;
        }
    }

    @Override
    public void request(EServerAPI eServerAPI, Object obj) {
        Record record = new Record();
        record.setEServerAPI(eServerAPI);
        switch (eServerAPI) {
            case API_GET:
                record.setRequestData(obj);
                break;
            case API_POST:
                record.setRequestData(obj);
                break;
        }
        sendApi(record);
    }

    @Override
    public void response(Record record) throws UnsupportedEncodingException {
        String json = new String(record.getData());
        json = json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1);
        Map<String, Object> result = JsonUtil.getJSONParserData(record.getData());
        if (result != null && result.containsKey("DOCNO")) {
            try {
                mItem.clear();
                switch ((String) result.get("DOCNO")) {
                    case "DZ003": //
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            CLog.d(jsonObject.toString());

                            String resultLength = (String) result.get("DATA_LENGTH");

                            if (!resultLength.equals("0")) {
                                JSONArray jsonArray = new JSONArray(jsonObject.getString("DATA"));
                                JSONObject object1 = jsonArray.getJSONObject(0);

                                switch (object1.getString("RESULT_CODE")) {
                                    case "0000":
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject object = jsonArray.getJSONObject(i);
                                            ServiceFirstItem item = new ServiceFirstItem(object.getString("IC_SEQ"),
                                                    object.getString("REQDATE"),
                                                    object.getString("USEDATE"),
                                                    object.getString("SO_NAME"),
                                                    object.getString("USE_MEMO"),
                                                    object.getString("OSEQ"),
                                                    object.getString("RESULT_CODE"));
                                            mItem.add(item);

                                        }
                                        break;
                                    case "4444":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "6666":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "9999":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.no_user) , true);
                                        break;

                                }
                                UiThread();
                                break;
                            } else {
                                UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.meal_tok_notdata) , true);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case "DZ005":
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            CLog.d(jsonObject.toString());

                            String resultLength = (String) result.get("DATA_LENGTH");

                            if (!resultLength.equals("0")) {
                                JSONArray jsonArray = new JSONArray(jsonObject.getString("DATA"));
                                JSONObject object1 = jsonArray.getJSONObject(0);

                                switch (object1.getString("RESULT_CODE")) {
                                    case "0000":
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject object = jsonArray.getJSONObject(i);
                                            ServiceFirstItem item = new ServiceFirstItem(object.getString("IM_SEQ"),
                                                    object.getString("REQDATE"),
                                                    object.getString("USEDATE"),
                                                    object.getString("SO_NAME"),
                                                    object.getString("USE_MEMO"),
                                                    object.getString("OSEQ"),
                                                    object.getString("RESULT_CODE"));
                                            mItem.add(item);

                                        }
                                        break;
                                    case "4444":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "6666":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "9999":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.no_user) , true);
                                        break;

                                }
                                UiThread();
                                break;
                            } else {
                                UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.meal_tok_notdata) , true);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case "DZ006":
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            CLog.d(jsonObject.toString());

                            String resultLength = (String) result.get("DATA_LENGTH");

                            if (!resultLength.equals("0")) {
                                JSONArray jsonArray = new JSONArray(jsonObject.getString("DATA"));
                                JSONObject object1 = jsonArray.getJSONObject(0);

                                switch (object1.getString("RESULT_CODE")) {
                                    case "0000":
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject object = jsonArray.getJSONObject(i);
                                            ServiceFirstItem item = new ServiceFirstItem(object.getString("PC_SEQ"),
                                                    object.getString("REQDATE"),
                                                    object.getString("USEDATE"),
                                                    object.getString("SO_NAME"),
                                                    object.getString("USE_MEMO"),
                                                    object.getString("OSEQ"),
                                                    object.getString("RESULT_CODE"));
                                            mItem.add(item);

                                        }
                                        break;
                                    case "4444":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "6666":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "9999":
                                        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.no_user) , true);
                                        break;

                                }
                                UiThread();
                                break;
                            } else {
                                UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.meal_tok_notdata) , true);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void networkException(Record record) {
        CLog.e("ERROR STATE : " + record.stateCode);
        UIThread(ServiceHistoryDetailFirstActivity.this, getString(R.string.confirm), getString(R.string.networkexception) , true);
    }

    private void UIThread(final Activity activity, final String confirm, final String message, final boolean asd) {
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertDialogUtil.onAlertDialog(activity, confirm, message , asd);
            }
        }, 0);
    }

    private void UiThread() {
        final Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                CLog.i("UiThread ---> ");
                mAdapter = new HistoryFirstListView_Adapter(ServiceHistoryDetailFirstActivity.this , mItem,  mType);
                mListView.setAdapter(mAdapter);

            }

        }, 0);


    }
}
