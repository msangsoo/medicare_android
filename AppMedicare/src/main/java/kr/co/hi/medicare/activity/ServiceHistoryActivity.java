package kr.co.hi.medicare.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.io.UnsupportedEncodingException;

import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 * Created by jihoon on 2016-06-16.
 * 서비스 이력보기
 * @since 0, 1
 */
public class ServiceHistoryActivity extends BaseActivityMedicare {

    public static final int REQUEST_CODE_CERT   =   1;
//    public static final String CERT_API_URL = "http://www.walkie.co.kr/hL/cert/HS_HL_CERT_STEP01.asp";
    public static final String CERT_API_URL = "https://m.shealthcare.co.kr/HL_MED_COMMUNITY/CERT/HS_HL_CERT_STEP01.asp";


    private boolean mIsCert = false;    // 휴대폰 인증 여부
    private int mType = -1;
    private UserInfo user;

    private Intent intent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_history);
        user = new UserInfo(this);

        init();
    }

    private void init() {
        intent = getIntent();
        if (intent != null){
            mIsCert = intent.getBooleanExtra(EXTRA_AUTH , false);
        }

        String certidate = SharedPref.getInstance().getPreferences(SharedPref.CERTI_DATE);

        if(!certidate.equals("")){
            if(StringUtil.getIntVal(certidate) == StringUtil.getIntVal(CDateUtil.getToday_yyyy_MM_dd()))
                mIsCert = true;
            else
                mIsCert = false;
        }
    }

    public void click_event(View v) {
        boolean backflag = false;

        switch (v.getId()) {
            case R.id.history_1_layout:
                mType   =   0;
                break;
            case R.id.history_2_layout:
                mType   =   1;
                break;
            case R.id.history_3_layout:
                mType   =   2;
                break;
            case R.id.history_4_layout:
                mType   =   3;
                break;
            case R.id.history_5_layout:
                mType   =   4;
                break;
        }
        if (!backflag) {
            CLog.i("mType = " + mType);
            setIntent(mType);
        }
    }

    /**
     * 세부 이력보기 이동처리
     * @param type 타입 ( 0 ~ 4 )
     */
    public void setIntent(int type){
        Intent intent = null;

        if(BuildConfig.DEBUG){
            mIsCert = true;
        }

        if(mIsCert){
            switch(type){
                case 0: // 심리케어
                    intent = new Intent(ServiceHistoryActivity.this, ServiceHistoryDetailSecondActivity.class);
                    break;
                case 1: // 면역세포보관
                    intent = new Intent(ServiceHistoryActivity.this, ServiceHistoryDetailFirstActivity.class);
                    break;
                case 2: // 맞춤형 운동
                    intent = new Intent(ServiceHistoryActivity.this, ServiceHistoryDetailThirdActivity.class);
                    break;
                case 3: //  면역력 검사
                    intent = new Intent(ServiceHistoryActivity.this, ServiceHistoryDetailFirstActivity.class);
                    break;
                case 4: // PET-CT
                    intent = new Intent(ServiceHistoryActivity.this, ServiceHistoryDetailFirstActivity.class);
                    break;
            }
            intent.putExtra(EXTRA_SERVICETYPE , mType);
            startActivity(intent);
        }else{
            Tr_login login = UserInfo.getLoginInfo();
            intent = new Intent(ServiceHistoryActivity.this, BackWebViewActivity.class);
            if(BuildConfig.DEBUG)
                intent.putExtra("EXTRA_URL", CERT_API_URL + "?seq=" + login.seq);
            else
                intent.putExtra("EXTRA_URL", CERT_API_URL + "?seq=" +login.seq);

            startActivityForResult(intent, REQUEST_CODE_CERT);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        CLog.i("resultCode : " + resultCode);
        CLog.i("requestCode :" + requestCode);

        if (resultCode != Activity.RESULT_OK) {
            CLog.i("resultCode != RESULT_OK");
            return;
        }

        switch (requestCode) {
            case REQUEST_CODE_CERT:
                mIsCert =   true;
                setIntent(mType);
                break;
        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void response(Record record) throws UnsupportedEncodingException {

    }

    @Override
    public void request(EServerAPI eServerAPI, Object obj) {

    }

    @Override
    public void networkException(Record record) {

    }
}
