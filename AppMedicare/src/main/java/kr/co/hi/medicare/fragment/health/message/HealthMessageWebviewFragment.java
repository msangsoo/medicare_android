package kr.co.hi.medicare.fragment.health.message;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.HashMap;

import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperLog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.health.dietprogram.DietMainFragment;
import kr.co.hi.medicare.utilhw.Logger;

/**
 * 질병 위험도 체크.
 */
public class HealthMessageWebviewFragment extends BaseFragmentMedi implements View.OnClickListener {
    private final String TAG = HealthMessageWebviewFragment.class.getSimpleName();

    public static final String TITLE = "title";
    public static final String URL = "url";
    public static final String Type = "type";

    private WebView mWebview;
    private String titlePos = "";
    public static String sUrl = "";
    private String stype = "0";

    private RadioButton mCategory1, mCategory2, mCategory3, mCategory4, mCategory5;
    private RadioGroup mFragmentGroup;
    private HashMap<String, String> mUrl = new HashMap<String, String>();
    private View tabview;
    private RelativeLayout mDietbtn;

    public static Fragment newInstance() {
        HealthMessageWebviewFragment fragment = new HealthMessageWebviewFragment();
        return fragment;
    }

    public static Fragment newInstance(String Url) {
        HealthMessageWebviewFragment fragment = new HealthMessageWebviewFragment();
        sUrl = Url;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_health_tip_webview, container, false);
//        mServiceBtn = (Button) view.findViewById(R.id.service_button);

        Bundle bundle = getArguments();
        if (bundle != null) {
            titlePos = bundle.getString(TITLE);
            sUrl = bundle.getString(URL);
            stype = bundle.getString(Type);


            Logger.i(TAG, "DummyWebviewFragment.title=" + titlePos);
            Logger.i(TAG, "DummyWebviewFragment.sUrl=" + sUrl);
            Logger.i(TAG, "DummyWebviewFragment.stype=" + stype);

        }


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentGroup = view.findViewById(R.id.tab_radiogroup);

        mDietbtn = view.findViewById(R.id.btn);
        mDietbtn.setOnClickListener(this);

        tabview = view;

        String CommonUrl = "https://m.shealthcare.co.kr/HL_MED_COMMUNITY/MTIP/MTIP.ASP?WKEY=";
        mUrl.put("weight", CommonUrl + "HL0102");
        mUrl.put("walk", CommonUrl + "HL0103");
        mUrl.put("meal", CommonUrl + "HL0101");
        mUrl.put("pressure", CommonUrl + "HL0104");
        mUrl.put("sugar", CommonUrl + "HL0105");

        mFragmentGroup.setOnCheckedChangeListener(mCheckedChangeListener);

        init(view);
    }

    /**
     * @param view
     */
    private void init(View view) {

        mWebview = (WebView) view.findViewById(R.id.dummy_webview);

        WebSettings settings = mWebview.getSettings();
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.setWebViewClient(new WebViewClient());
        settings.setDefaultTextEncodingName("utf-8");
        settings.setBuiltInZoomControls(true); // 줌 허용
        settings.setSupportZoom(true);
        settings.setDisplayZoomControls(false);
        settings.setUseWideViewPort(true);

        settings.setLoadWithOverviewMode(true);
//        mWebview.addJavascriptInterface(new AndroidBridge(), "HybridApp");
        mWebview.setWebViewClient(new WebViewClientClass());
        mWebview.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                new AlertDialog.Builder(getContext())
                        .setTitle("알림")
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok,
                                new AlertDialog.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        result.confirm();
                                    }
                                })
                        .setCancelable(false)
                        .create()
                        .show();
                return true;
            }
        });

        if(HealthMessageFragment.bottomPosition.equals("")){
            setCategory(0);
        } else {
            if(HealthMessageFragment.bottomPosition.equals("2")){
                mFragmentGroup.getChildAt(2).performClick();
            } else if(HealthMessageFragment.bottomPosition.equals("3")){
                mFragmentGroup.getChildAt(3).performClick();
            } else if(HealthMessageFragment.bottomPosition.equals("4")){
                mFragmentGroup.getChildAt(4).performClick();
            }
        }

    }



    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            int pos=group.indexOfChild(tabview.findViewById(checkedId));

            int j = 43;
            j = pos + j;

            if(pos == 1){
                j = 45;
            } else if( pos == 2){
                j = 44;
            }
            tabview.findViewById(checkedId).setContentDescription("HM03_0"+j+"_001_!");
            if (tabview.findViewById(checkedId).getContentDescription().toString().contains("_!")) {
                String temp = tabview.findViewById(checkedId).getContentDescription().toString().replace("_!", "");
                String cod[] = temp.split("_");

                DBHelper helper = new DBHelper(getContext());
                DBHelperLog logdb = helper.getLogDb();

                if (cod.length == 1) {
                    logdb.insert(cod[0], "", "", 0, 1);
                    Log.i(TAG, "view.contentDescription : " + cod[0] + "count : 1");
                } else if (cod.length == 2) {
                    logdb.insert(cod[0], cod[1], "", 0, 1);
                    Log.i(TAG, "view.contentDescription : " + cod[0] + cod[1] + "count : 1");
                } else {
                    logdb.insert(cod[0], cod[1], cod[2], 0, 1);
                    Log.i(TAG, "view.contentDescription : " + cod[0] + cod[1] + cod[2] + "count : 1");
                }

            }

            setCategory(pos);

        }
    };

    public void setCategory(int pos) {
        switch (pos) {
            case 0:
                sUrl = mUrl.get("meal");
                mWebview.loadUrl(sUrl);
                mDietbtn.setVisibility(View.GONE);
                break;
            case 1:
                sUrl = mUrl.get("walk");
                mWebview.loadUrl(sUrl);
                mDietbtn.setVisibility(View.GONE);
                break;
            case 2:
                sUrl = mUrl.get("weight");
                mWebview.loadUrl(sUrl);
                //TODO xxx 다이어트 프로그램 임시 숨기기
                if(BuildConfig.DEBUG)
                    mDietbtn.setVisibility(View.VISIBLE);
                else
                mDietbtn.setVisibility(View.GONE);

                break;
            case 3:
                sUrl = mUrl.get("pressure");
                mWebview.loadUrl(sUrl);
                mDietbtn.setVisibility(View.GONE);
                break;
            case 4:
                sUrl = mUrl.get("sugar");
                mWebview.loadUrl(sUrl);
                mDietbtn.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn:
                NewActivity.startActivity(HealthMessageWebviewFragment.this, DietMainFragment.class,null);
                break;
        }
    }


    private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if( url.startsWith("http:") || url.startsWith("https:") )
            {
                return false;
            }
            // tel일경우 아래와 같이 처리해준다.
            else if (url.startsWith("tel:"))
            {
                Intent tel = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(tel);

//                IntentUtil.requestPhoneDialActivity(getContext(), tel);
                return true;
            }
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Logger.i(TAG, "onPageFinished.hideprogress");
            hideProgress();
            // progress 가 해지 안 될 경우 처리
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (getActivity() != null)
                        hideProgress();
                }
            }, 1 * 500);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Logger.i(TAG, "onPageStarted.showProgress");
            showProgress();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            Logger.i(TAG, "onReceivedError.hideprogress");
            hideProgress();
            // progress 가 해지 안 될 경우 처리
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (getActivity() != null)
                        hideProgress();
                }
            }, 1 * 500);
        }

        @Override
        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            super.onReceivedHttpError(view, request, errorResponse);
            Logger.i(TAG, "onReceivedHttpError.hideprogress");
            hideProgress();
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            Logger.i(TAG, "onReceivedSslError.hideprogress");
            hideProgress();
        }
    }

    @Override
    public void onBackPressed() {
        if (mWebview.canGoBack()) {
            mWebview.goBack();
        } else {
            super.onBackPressed();
        }

    }
}
