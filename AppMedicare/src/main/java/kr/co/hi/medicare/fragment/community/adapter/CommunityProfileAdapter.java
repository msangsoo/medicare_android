package kr.co.hi.medicare.fragment.community.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.iojjj.rcbs.RoundedCornersBackgroundSpan;

import java.util.ArrayList;
import java.util.List;

import co.lujun.androidtagview.TagContainerLayout;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.community.holder.ProgressViewHolder;
import kr.co.hi.medicare.tempfunc.TemporaryFunction;


public class CommunityProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private View.OnClickListener onClickListener;
    private Context context;
    private ArrayList<CommunityListViewData> data;
    private boolean isMoreLoading = false;

    public void updateData(CommunityListViewData comm_data) {
        if(comm_data!=null){
            for(int i=0; i< data.size();i++){
                if(data.get(i).CM_SEQ.equals(comm_data.CM_SEQ)){
                    data.get(i).RNUM = comm_data.RNUM;
                    data.get(i).RCNT = comm_data.RCNT;
                    data.get(i).HCNT = comm_data.HCNT;
                    data.get(i).REGDATE = comm_data.REGDATE;
                    data.get(i).CM_TITLE = comm_data.CM_TITLE;
                    data.get(i).NICK = comm_data.NICK;
                    data.get(i).CM_SEQ = comm_data.CM_SEQ;
                    data.get(i).TPAGE = comm_data.TPAGE;
                    data.get(i).CM_IMG1 = comm_data.CM_IMG1;
                    data.get(i).PROFILE_PIC = comm_data.PROFILE_PIC;
                    data.get(i).CM_CONTENT = comm_data.CM_CONTENT;
                    data.get(i).CM_TAG = comm_data.CM_TAG;
                    data.get(i).MYHEART = comm_data.MYHEART;
                    data.get(i).ISDETAIL = comm_data.ISDETAIL;
                    data.get(i).MBER_SN = comm_data.MBER_SN;
                    data.get(i).CM_GUBUN = comm_data.CM_GUBUN;
                    data.get(i).MBER_GRAD = comm_data.MBER_GRAD;
                    data.get(i).CM_MEAL = comm_data.CM_MEAL;

                    notifyItemChanged(i);
                    break;
                }
            }
        }
    }

    public void deleteItem(String CM_SEQ) {
        for(int i=0; i<data.size();i++){
            if(data.get(i).CM_SEQ.equals(CM_SEQ)){
                data.remove(i);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i,data.size());
                break;
            }
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView text,like,comment,regdate,cm_meal;

        public ViewHolder(View itemView) {
            super(itemView);
            regdate = itemView.findViewById(R.id.regdate);
            image = itemView.findViewById(R.id.image);
            text = itemView.findViewById(R.id.text);
            like = itemView.findViewById(R.id.like);
            comment = itemView.findViewById(R.id.comment);
            cm_meal = itemView.findViewById(R.id.cm_meal);
        }

        public void SetView(CommunityListViewData data,Context context){

            regdate.setText(TemporaryFunction.getDateFormat(data.REGDATE)); //파싱필요
            like.setText( data.HCNT );
            comment.setText( data.RCNT );
            text.setText(data.CM_CONTENT); //10글자 이상이면 ...처리

            if(data.CM_IMG1.equals("")){
                image.setVisibility(View.INVISIBLE);
            }else{
                image.setVisibility(View.VISIBLE);
                Glide
                        .with(context)
                        .load(CommonFunction.getThumbnail(data.CM_IMG1))
                        .into(image);

                image.setClipToOutline(true);

            }



            if(data.CM_MEAL!=null&&!data.CM_MEAL.equals("")){
                cm_meal.setVisibility(View.VISIBLE);
                cm_meal.setText(data.CM_MEAL);
//                CommonFunction.setTextByPartsString(data.CM_MEAL,cm_meal, RoundedCornersBackgroundSpan.ALIGN_START,context);
            }else{
                cm_meal.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position) != null ? LoadMoreListener.VIEW_ITEM : LoadMoreListener.VIEW_PROG;
    }


    public CommunityProfileAdapter(Context context, View.OnClickListener onClickListener) {
        this.context = context;
        this.onClickListener = onClickListener;
        data = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType == LoadMoreListener.VIEW_ITEM) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_community_profile, parent, false));
        }else{
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_progress, parent, false));
        }

    }

    public void setMore(boolean isMore) {
        this.isMoreLoading = isMore;
    }

    public boolean getMore() {
        return isMoreLoading;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ViewHolder){
            if (data.size() > 0) {
                if (position < data.size()) {
                    //뷰홀더의 자원을 초기화//
                    final ViewHolder commViewHolder = (ViewHolder) holder;
                    commViewHolder.SetView(data.get(position),context);
                    commViewHolder.text.setTag(R.id.comm_main_text, position);
                    commViewHolder.text.setOnClickListener(onClickListener);
                    commViewHolder.image.setTag(R.id.comm_main_image,position);
                    commViewHolder.image.setOnClickListener(onClickListener);
                    return;
                }
            }
        }


    }

    @Override
    public int getItemCount() {
        if (data==null) {
            return 0;
        }
        return data.size();
    }

    public void addAllItem(List<CommunityListViewData> data) {
        if(this.data==null)
            this.data = new ArrayList<>();

        if(data!=null){
            this.data.clear();
            this.data.addAll(data);
            notifyDataSetChanged();
        }
    }

    public void addItemMore(List<CommunityListViewData> data){
        int sizeInit = this.data.size();
        this.data.addAll(data);
        notifyItemRangeChanged(sizeInit, this.data.size());
    }


    public void addItem(CommunityListViewData data) {
        if(this.data==null)
            this.data = new ArrayList<>();

        if(data!=null){
            ArrayList<CommunityListViewData> datas = new ArrayList<>();
            datas.add(data);
            datas.addAll(this.data);
            this.data.clear();
            this.data = datas;
            notifyDataSetChanged();
        }
    }

    public void setProgressMore(final boolean isProgress) {
        if (isProgress) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    data.add(null);
                    notifyItemInserted(data.size() - 1);
                }
            });
        } else {

            if(data.size()>0) {
                data.remove(data.size() - 1);
                notifyItemRemoved(data.size());
            }
        }
    }

    public CommunityListViewData getItem(int position, String MBER_SN){
        data.get(position).MBER_SN = MBER_SN;
        return data.get(position);
    }

    public CommunityListViewData getItem(int position){

        return data.get(position);
    }

}
