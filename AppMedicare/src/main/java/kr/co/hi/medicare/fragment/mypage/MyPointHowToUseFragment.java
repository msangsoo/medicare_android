package kr.co.hi.medicare.fragment.mypage;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.component.CDialog;

import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.value.Define;
import kr.co.hi.medicare.net.hwNet.ApiData;
import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_gclife_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.StringUtil;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.net.hwdata.Tr_mber_main_call;
import kr.co.hi.medicare.net.data.Tr_mber_point_move_yn;

/**
 * Created by MrsWin on 2017-02-16.
 *
 */

public class MyPointHowToUseFragment extends BaseFragmentMedi {
    private final String TAG = MyPointHowToUseFragment.class.getSimpleName();

    private EditText mExchangePointEt;
    private TextView mChagePossibleTv;


    public static Fragment newInstance() {
        MyPointHowToUseFragment fragment = new MyPointHowToUseFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mypage_point_howtouse, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    private void initView(View view) {
        mChagePossibleTv = view.findViewById(R.id.change_possible_point_textview);
        mExchangePointEt = view.findViewById(R.id.exchange_point_edittext);
        mExchangePointEt.getBackground().setColorFilter(getResources().getColor(R.color.x229_229_229), PorterDuff.Mode.SRC_ATOP);
//        new TextWatcherUtil().setFormatWatcher(mExchangePointEt);
        view.findViewById(R.id.go_lifecaremall_btn).setOnClickListener(mClickListener);
        view.findViewById(R.id.exchange_point_button).setOnClickListener(mClickListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, getContext());


        //마이페이지
        view.findViewById(R.id.exchange_point_button).setOnTouchListener(ClickListener);
        view.findViewById(R.id.go_lifecaremall_btn).setOnTouchListener(ClickListener);


        //코드부여
        view.findViewById(R.id.exchange_point_button).setContentDescription(getString(R.string.exchange_point_button));
        view.findViewById(R.id.go_lifecaremall_btn).setContentDescription(getString(R.string.go_lifecaremall_btn));


        String life = getString(R.string.라이프케어몰바로가기);
        setMessageColor((TextView)view.findViewById(R.id.lifecaremall), life, life.indexOf("바"),life.length(),R.color.colorBlack);

        getMainData();
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.exchange_point_button:
                    doExchangePoint();
                    break;
                case R.id.go_lifecaremall_btn:
                    CDialog.showDlg(getContext(), getString(R.string.go_lifecaremall_move_alert), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.life_care_url)));
                            startActivity(browserIntent);
                        }
                    }, null);
                    break;
            }
        }
    };

    /**
     * 메인페이지데이터(건강점수, 포인트)
     */
    private void getMainData() {

        Tr_login login = UserInfo.getLoginInfo();
        Tr_mber_main_call.RequestData requestData = new Tr_mber_main_call.RequestData();
        requestData.mber_sn = login.mber_sn;
        requestData.input_de = CDateUtil.getToday_yyyy_MM_dd();

        getData(getContext(), Tr_mber_main_call.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_mber_main_call) {
                    Tr_mber_main_call data = (Tr_mber_main_call) obj;

                    //총포인트 반영
                    Tr_login login = UserInfo.getLoginInfo();
                    login.user_point_amt = data.user_point_amt;
                    Define.getInstance().setLoginInfo(login);
                    setChangePossiblePoint(data.user_point_amt);
                }
            }
        });
    }

    private void setChangePossiblePoint(String point){
        String msg = getString(R.string.howtouse_content_title6)+" "+StringUtil.getFormatPrice(point)+" Point";
        setMessageColor(mChagePossibleTv, msg, msg.indexOf("트")+1,msg.indexOf("P"),R.color.colorWhite);
    }


    private void setMessageColor(TextView view,String message, int start, int end,int textColor){
        SpannableStringBuilder sp = new SpannableStringBuilder(message);

        if(textColor!=0)
            sp.setSpan( new ForegroundColorSpan(getContext().getResources().getColor(textColor)),start,end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        sp.setSpan( new StyleSpan(Typeface.BOLD),start,end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        view.setText(sp);
    }

    /**
     * 포인트 전환하기
     */
    private void doExchangePoint() {
        // 보유포인트 보다 많은 포인트를 전환 할 수 없습니다
        final int exChangePoint = StringUtil.getIntVal(mExchangePointEt.getText().toString());
        int myPoint = StringUtil.getIntVal(mChagePossibleTv.getText().toString());


        if (TextUtils.isEmpty(mExchangePointEt.getText().toString()) || exChangePoint == 0) {
            CDialog.showDlg(getContext(), "전환하실 포인트를 입력해 주세요.");
            return;
        }

        if (myPoint < exChangePoint) {
            CDialog.showDlg(getContext(), "보유포인트 보다 많은 포인트를 전환 할 수 없습니다");
            return;
        }


        if(UserInfo.getLoginInfo().gclife_id==null||UserInfo.getLoginInfo().gclife_id.trim().equals("")){
            CDialog.showDlg(getContext(), getString(R.string.howtouse_content_title7)).setOkButton(getString(R.string.join_link), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //아이디연동

                    openDialogLinkLifecaremall();
                }
            }).setNoButton(getString(R.string.join), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //가입하기
                    NewActivity.startActivity(MyPointHowToUseFragment.this , MyPointJoinLifecaremallFragment.class,null);
                }
            });
            return;
        }



        CDialog.showDlg(getContext(), getString(R.string.do_you_want_exchange_point), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestChangePoint(exChangePoint);
            }
        }, null);

    }

    private void openDialogLinkLifecaremall() {

        final View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_link_lifecaremall,null);
        final CDialog dlg = CDialog.showDlg(getContext(), view,true);

        view.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });
        view.findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = ((TextView)view.findViewById(R.id.id)).getText().toString().trim();
                String pw = ((TextView)view.findViewById(R.id.pw)).getText().toString().trim();

                if(!id.equals("")&&!pw.equals("")){
                    dlg.dismiss();
                    requestLinkLifecaremall(id,pw);
                }
            }
        });
    }



    private void requestChangePoint(int point){
        final Tr_mber_point_move_yn.RequestData requestData = new Tr_mber_point_move_yn.RequestData();

        Tr_login info = UserInfo.getLoginInfo();
        requestData.mber_sn = info.mber_sn;
        requestData.mber_id = info.gclife_id;
        requestData.user_point_amt = Integer.toString(point);

        MediNewNetworkModule.doApi(getContext(), new Tr_mber_point_move_yn(), requestData,true, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String message="";
                boolean isOpenLinkDialog=false;


                if (responseData instanceof Tr_mber_point_move_yn) {
                    Tr_mber_point_move_yn data = (Tr_mber_point_move_yn)responseData;
                    try {
                        if(data.reg_yn!=null&&data.reg_yn.equals("Y")){
                            CommonFunction.setUpdateLoginInfo(getContext());
                            mExchangePointEt.setText("");
                            setChangePossiblePoint(data.point_total_amt);
                            message = getString(R.string.success_exchange_point);
                        }else{
                            message = "포인트 전환에 실패 하였습니다.";
                            isOpenLinkDialog=true;
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                        message = getResources().getString(R.string.comm_error_common)+"\ncode:1";
                        isOpenLinkDialog=true;
                    }
                }

                if(!message.equals("")){
                    CDialog dig = CDialog.showDlg(getContext(), message);
                    if(isOpenLinkDialog){
                        dig.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                openDialogLinkLifecaremall();
                            }
                        });
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                Log.e(TAG, response);
                CDialog.showDlg(getContext(), response);
            }
        });
    }



    private void requestLinkLifecaremall(String gclife_id, String gclife_pw){
        final Tr_gclife_login.RequestData requestData = new Tr_gclife_login.RequestData();
        boolean isShowProgress=true;

        requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;
        requestData.gclife_id = gclife_id;
        requestData.gclife_pw = gclife_pw;

        MediNewNetworkModule.doApi(getContext(), new Tr_gclife_login(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_gclife_login) {
                    Tr_gclife_login data = (Tr_gclife_login)responseData;
                    try {
                        if(data.data_yn!=null&&data.data_yn.equals("Y")&&data.gclife_id!=null&&!data.gclife_id.trim().equals("")) {
                            CommonFunction.setUpdateLoginInfo(getContext());
                            errorMessage=getResources().getString(R.string.join_link_message);
                        }
                        else if(data.data_yn!=null&&data.data_yn.equals("N")){
                            errorMessage="계정정보가 확인되지 않습니다.\n계정정보를 잊으신 경우 라이프케어몰에서 아이디/비밀번호 찾기를 진행해 주시기 바랍니다.";
                        }else{
                            errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:1";
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                        errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:3";
                    }
                }

                if(!errorMessage.equals("")){
                    CDialog.showDlg(getContext(), errorMessage);
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                Log.e(TAG, response);
                CDialog.showDlg(getContext(), response);
            }
        });
    }

}
