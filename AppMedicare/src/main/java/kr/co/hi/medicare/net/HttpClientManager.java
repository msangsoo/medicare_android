package kr.co.hi.medicare.net;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.JsonUtil;

public class HttpClientManager
{

	private String TAG = HttpClientManager.class.getName();

	private final int CONNECT_TIMEOUT = 15;
	private final int READ_TIMEOUT = 10;
	private final int READ_BUFFER_SIZE = 8192;

	private static HttpClientManager mInstances;

	static public HttpClientManager getInstances()
	{
		return mInstances == null ? mInstances = new HttpClientManager() : mInstances;
	}

	public void request(final Record record)
	{
		Thread thread = new Thread()
		{
			@Override
			public void run()
			{
				try
				{
					String urlString = record.getEServerAPI().getFullUrl();;
					URL url = null;
					HttpURLConnection connection = null;
					switch(record.getEServerAPI().getProtocol())
					{
						case HTTP:
							switch(record.getEServerAPI().getMethod())
							{
								case GET:
									urlString += addQuery(record.getRequestData(), record, record.getCharSet());
									CLog.i("SEND URL : " + urlString);
									url = new URL(urlString);
									connection = (HttpURLConnection) url.openConnection();
									connection.setConnectTimeout(CONNECT_TIMEOUT * 5000);
									connection.setReadTimeout(READ_TIMEOUT * 5000);
									connection.setRequestMethod(EHttpMethod.GET.name());
									connection.setDoInput(true);

									/*
									 * connection.setRequestProperty(
									 * "Cache-Control", "no-cache");
									 * connection.setRequestProperty(
									 * "Content-Type", "application/json");
									 * connection.setRequestProperty("Accept",
									 * "application/json");
									 */

									connection.setRequestProperty("accept-language", "ko");

									break;
								case POST:
									CLog.d("urlString : " + urlString);

									url = new URL(urlString);
									connection = (HttpURLConnection) url.openConnection();
									connection.setConnectTimeout(CONNECT_TIMEOUT * 1000);
									connection.setReadTimeout(READ_TIMEOUT * 1000);
									connection.setRequestMethod(EHttpMethod.POST.name());
									connection.setDoInput(true);
									connection.setDoOutput(true);

									OutputStream outputStream = connection.getOutputStream();
									BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, record.getCharSet()));
									bufferedWriter.write(addQuery(record.getRequestData(), record, record.getCharSet()));
									bufferedWriter.flush();
									bufferedWriter.close();
									outputStream.close();

									connection.connect();
									break;
								case MULTIPART:
									CLog.d("urlString : " + urlString);

									DefaultHttpClient client = new DefaultHttpClient();
									HttpPost post = new HttpPost(urlString);

									MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
									HashMap<String, Object> hashMap = (HashMap<String, Object>)record.getRequestData();
									for( HashMap.Entry<String, Object> entry : hashMap.entrySet() ){
										if (entry.getValue() instanceof File) {
											File file = (File)entry.getValue();
											if( file.exists() ){
												multipartEntityBuilder.addBinaryBody(entry.getKey(), file);
											}
										}else{
											multipartEntityBuilder.addTextBody(entry.getKey(), (String) entry.getValue());
										}
									}

									HttpEntity entity = multipartEntityBuilder.build();
									post.setEntity(entity);
									HttpResponse response = client.execute(post);
									HttpEntity httpEntity = response.getEntity();

									int responseCode = response.getStatusLine().getStatusCode();
									CLog.d("Response Code : " + responseCode);
									CLog.d("Request Query : " + record.getRequestData());
									if(responseCode == HttpURLConnection.HTTP_OK)
									{
										record.setData(EntityUtils.toByteArray(httpEntity));
										record.getNetworkListener().onResponse(record);
									}
									return;
							}
							break;
						case HTTPS:
							trustAllHosts();
                            urlString += addQuery(record.getRequestData(), record, record.getCharSet());
                            url = new URL(urlString);
                            HttpsURLConnection httpsConnection = (HttpsURLConnection) url.openConnection();
							httpsConnection.setHostnameVerifier(new HostnameVerifier()
                            {
                                @Override
                                public boolean verify(String s, SSLSession sslSession)
                                {
                                    return true;
                                }
                            });
							connection = httpsConnection;
							switch(record.getEServerAPI().getMethod())
							{
								case GET:
									urlString += addQuery(record.getRequestData(), record, record.getCharSet());

									url = new URL(urlString);
									connection.setConnectTimeout(CONNECT_TIMEOUT * 1000);
									connection.setReadTimeout(READ_TIMEOUT * 1000);
									connection.setRequestMethod(EHttpMethod.GET.name());
									connection.setDoInput(true);

									int responseCode = connection.getResponseCode();
									CLog.d("\nSending 'GET' request to URL : " + url);
									CLog.d("Response Code : " + responseCode);
									break;
								case POST:

									url = new URL(urlString);
									connection = (HttpURLConnection) url.openConnection();
									connection.setConnectTimeout(CONNECT_TIMEOUT * 1000);
									connection.setReadTimeout(READ_TIMEOUT * 1000);
									connection.setRequestMethod(EHttpMethod.POST.name());
									connection.setDoInput(true);
									connection.setDoOutput(true);

									OutputStream outputStream = connection.getOutputStream();
									BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, record.getCharSet()));
									bufferedWriter.write(addQuery(record.getRequestData(), record, record.getCharSet()));
									bufferedWriter.flush();
									bufferedWriter.close();
									outputStream.close();

									connection.connect();
									break;
							}
							break;
					}

					int responseCode = connection.getResponseCode();
					CLog.d("Response protocol : " +record.getEServerAPI().getProtocol());
					CLog.d("Response Code : " + responseCode);
					CLog.d("Request : " +record.getEServerAPI().getDomain() + "?" +record.getRequestData());
					if(responseCode == HttpURLConnection.HTTP_OK)
					{
						record.stateCode = responseCode;
						InputStream inputStream = connection.getInputStream();
						if(null != inputStream)
						{
							ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
							byte[] byteBuffer = new byte[READ_BUFFER_SIZE];
							byte[] byteData = null;
							int nLength = 0;

							while((nLength = inputStream.read(byteBuffer, 0, byteBuffer.length)) != -1)
							{
								byteArrayOutputStream.write(byteBuffer, 0, nLength);
							}
							record.setData(byteArrayOutputStream.toByteArray());
							CLog.d("Result : " + JsonUtil.getJSONParserData(record.getData()).toString());
						}
						record.getNetworkListener().onResponse(record);
					}
					else
					{
						record.stateCode = responseCode;
						record.getNetworkListener().onNetworkException(record);
					}

				}
				catch(MalformedURLException e1)
				{
					Log.getStackTraceString(e1);
					record.getNetworkListener().onNetworkException(record);
				}
				catch(IOException e)
				{
					Log.getStackTraceString(e);
					record.getNetworkListener().onNetworkException(record);
				}
			}
		};
		thread.start();
	}


	private String addQuery(Object obj, Record rec, String charSet)
	{

		if(null == obj)
		{
			return "";
		}

		if(obj instanceof HashMap)
		{
			HashMap<String, Object> hashMap = (HashMap) obj;
			Iterator<String> iterator = hashMap.keySet().iterator();
			StringBuffer requestUrl = new StringBuffer();
			if(iterator.hasNext())
			{
				if(rec.getEServerAPI().getMethod() == EHttpMethod.GET)
					requestUrl.append('?');
			}

			String name = null;
			String value = null;

			while(iterator.hasNext())
			{
				name = iterator.next();
				try
				{
					if(hashMap.get(name) instanceof String)
					{
						value = URLEncoder.encode((String) hashMap.get(name), charSet).replaceAll("\\+", "%20");
					}
					else
					{
						value = "" + hashMap.get(name);
					}
                    requestUrl.append(name);
                    requestUrl.append('=');
                    requestUrl.append(value);

                    if(iterator.hasNext())
                    {
                        requestUrl.append('&');
                    }
				}
				catch(UnsupportedEncodingException e)
				{
					e.printStackTrace();
				}
			}
			return requestUrl.toString();
		}
		return "";
	}

	private static void trustAllHosts()
	{
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager()
		{
			public java.security.cert.X509Certificate[] getAcceptedIssuers()
			{
				return new java.security.cert.X509Certificate[] {};
			}

			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws java.security.cert.CertificateException
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws java.security.cert.CertificateException
			{
				// TODO Auto-generated method stub

			}
		} };

		// Install the all-trusting trust manager
		try
		{
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		}
		catch(Exception e)
		{
			Log.getStackTraceString(e);
		}
	}

}
