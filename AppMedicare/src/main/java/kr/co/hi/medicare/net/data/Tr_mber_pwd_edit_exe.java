package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;
import kr.co.hi.medicare.utilhw.Logger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 비밀번호 변경
 * Input 값
 * insures_code: 회사코드
 * mber_sn: 정회원/준회원구분
 * mber_id: 회원key
 * bef_mber_pwd: 신장
 * aft_mber_pwd: 체중
 *
 * Output 값
 * api_code: 호출코드명
 * insures_code: 회사코드
 * mber_sn: 포인트알림여부
 * reg_yn: 포인트적립여부
 *
 *
 *
 */
public class Tr_mber_pwd_edit_exe extends BaseData {
    private final String TAG = Tr_mber_pwd_edit_exe.class.getSimpleName();

    public static class RequestData {
        public String mber_sn;
        public String mber_id;
        public String bef_mber_pwd;
        public String aft_mber_pwd;
    }

    public Tr_mber_pwd_edit_exe() {
        super.conn_url = "http://m.shealthcare.co.kr/HL_MED/WebService/MED_MOBILE_CALL.asmx/MED_mobile_Call";
    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof Tr_mber_pwd_edit_exe.RequestData) {
            JSONObject body = new JSONObject();
            Tr_mber_pwd_edit_exe.RequestData data = (Tr_mber_pwd_edit_exe.RequestData) obj;

            body.put("api_code", getApiCode(TAG) ); //
            body.put("insures_code", INSURES_CODE);
            body.put("mber_sn", data.mber_sn); //  1000
            body.put("mber_id", data.mber_id); //  1000
            body.put("bef_mber_pwd", data.bef_mber_pwd); //  1000
            body.put("aft_mber_pwd", data.aft_mber_pwd); //  1000
            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("api_code")
    public String api_code; //
    @SerializedName("insures_code")
    public String insures_code; //
    @SerializedName("mber_sn")
    public String mber_sn; //
    @SerializedName("reg_yn")
    public String reg_yn; //

}
