package kr.co.hi.medicare.fragment.mypage;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.CDatePicker;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_mber_point_use_list;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 * Created by MrsWin on 2017-02-16.
 *
 */

public class MyPointInFoFragment extends BaseFragmentMedi {
    private final String TAG = MyPointInFoFragment.class.getSimpleName();

    private TextView mSelectDateTv;

    private boolean mIsLast = false;
    private int mPageNo = 1;
    private String maxpage;

    private TextView mCurrentPointTv;

    private TextView mTotalPointTv;


    private final int[] btnPeriodIds={R.id.btn_period_month_1,R.id.btn_period_month_3,R.id.btn_period_month_6,R.id.btn_period_month_all};
    private int TYPE_PERIOD=R.id.btn_period_month_1;

//    private TextView mStartTv;
//    private TextView mEndTv;
//    private Button mSearchBtn;
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;

    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;
    int firstVisibleItem=0, visibleItemCount=0, totalItemCount=0;
    private LinearLayoutManager mLayoutManager;



    public static Fragment newInstance() {
        MyPointInFoFragment fragment = new MyPointInFoFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mypage_point_info, container, false);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

    }

//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        //super.loadActionbar(actionBar);
//        actionBar.setActionBarTitle(getString(R.string.my_point));       // 액션바 타이틀
//    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayout layoutmypagehistory = (LinearLayout) view.findViewById(R.id.layout_mypage_history);

        mCurrentPointTv = (TextView) view.findViewById(R.id.current_point_textview);


        mTotalPointTv = (TextView) view.findViewById(R.id.add_point_textview);


        mRecyclerView = view.findViewById(R.id.point_info_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new RecyclerAdapter();
        mRecyclerView.setAdapter(mAdapter);


        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        setMessageBold(  ((TextView)view.findViewById(R.id.title_point_current)), getString(R.string.current_point) , 0 , 2 );
        setMessageBold(  ((TextView)view.findViewById(R.id.title_point_total)), getString(R.string.add_point) , 0 , 2 );


        for(int i=0; i<btnPeriodIds.length;i++){
            view.findViewById(btnPeriodIds[i]).setOnClickListener(mOnClickListener);
        }


//        view.findViewById(R.id.point_start_date_tv).setOnTouchListener(mOnTouchListener);
//        view.findViewById(R.id.point_end_date_tv).setOnTouchListener(mOnTouchListener);
//        view.findViewById(R.id.my_point_submit_btn).setOnClickListener(mOnClickListener);


//        Calendar cal = Calendar.getInstance();
//        int year = cal.get(Calendar.YEAR);
//        int month = cal.get(Calendar.MONTH)+1;
//
//        Calendar monthAgo = Calendar.getInstance();
//        monthAgo.add (monthAgo.MONTH,-1);
//
//        String firtstDay = String.format("%d.%02d.%02d", monthAgo.get(monthAgo.YEAR), monthAgo.get(monthAgo.MONTH)+1, monthAgo.get(monthAgo.DATE));
//        String currentDate = String.format("%d.%02d.%02d", year, month, cal.get(Calendar.DATE));
//
//        mStartTv.setText(firtstDay);
//        mEndTv.setText(currentDate);

        mPageNo = 1;

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = mRecyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                        Log.i("Yaeye!", "end called" + loading + visibleItemCount+"/" + totalItemCount+"/" + firstVisibleItem);
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    // End has been reached

                    Log.i("Yaeye!", "end called" + loading + visibleItemCount+"/" + totalItemCount+"/" + firstVisibleItem);

                    getPointList();

                    loading = true;
                }
            }
        });
        getPointList();
    }

//    View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {
//        @Override
//        public boolean onTouch(View v, MotionEvent event) {
//            if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                switch (v.getId()) {
//                    case R.id.point_start_date_tv:
//                        showDatePicker(mStartTv);
//                        break;
//                    case R.id.point_end_date_tv:
//                        showDatePicker(mEndTv);
//                        break;
//                }
//
//            }
//            return false;
//        }
//    };

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_period_month_1:
                case R.id.btn_period_month_3:
                case R.id.btn_period_month_6:
                case R.id.btn_period_month_all:
                    if(setPeriodBtns(v.getId())){
                        mPageNo = 1;
                        mIsLast = false;;
                        getPointList();
                    }
                    break;
            }

        }
    };


    private void setMessageBold(TextView view,String message, int start, int end){
        SpannableStringBuilder sp = new SpannableStringBuilder(message);
        sp.setSpan( new StyleSpan(Typeface.BOLD),start,end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        view.setText(sp);
    }

    private boolean setPeriodBtns(int viewId){

        boolean isChoice=false;

        if(TYPE_PERIOD==viewId)
            return isChoice;

        for(int i=0; i<btnPeriodIds.length;i++){
            if(btnPeriodIds[i]==viewId){
                TYPE_PERIOD=btnPeriodIds[i];
                ((TextView)getActivity().findViewById(btnPeriodIds[i])).setBackgroundColor(getResources().getColor(R.color.x0_22_85));
                ((TextView)getActivity().findViewById(btnPeriodIds[i])).setTextColor(Color.WHITE);
            }else{
                ((TextView)getActivity().findViewById(btnPeriodIds[i])).setBackgroundColor(getResources().getColor(R.color.x237_237_237));
                ((TextView)getActivity().findViewById(btnPeriodIds[i])).setTextColor(getResources().getColor(R.color.x143_144_158));
            }
        }
        isChoice=true;
        return isChoice;
    }


    private void showDatePicker(View v) {
        mSelectDateTv = (TextView) v;

        GregorianCalendar calendar = new GregorianCalendar();
        String birth = mSelectDateTv.getText().toString().trim();
        String[] birthSpl = birth.split("\\.");

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        if (birthSpl.length == 3) {
            year = Integer.parseInt("".equals(birthSpl[0]) ? "0" : birthSpl[0].trim());
            month = Integer.parseInt("".equals(birthSpl[1]) ? "0" : birthSpl[1].trim()) - 1;
            day = Integer.parseInt("".equals(birthSpl[2]) ? "0" : birthSpl[2].trim());
        }

        DatePickerDialog dlg = new CDatePicker(getContext(), dateSetListener, year, month, day, false);
        DatePicker picker = dlg.getDatePicker();
        dlg.show();
    }

    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String msg = String.format("%d.%02d.%02d", year, monthOfYear + 1, dayOfMonth);
            mSelectDateTv.setText(msg);
        }

    };


    /**
     * 포인트 정보 가져오기
     */
    private void getPointList() {
        if (mIsLast)
            return;

        Tr_mber_point_use_list.RequestData requestData = new Tr_mber_point_use_list.RequestData();

        String startDate = "";
        String endDate = "";

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH)+1;

        Calendar monthAgo = Calendar.getInstance();

        switch (TYPE_PERIOD){
            case R.id.btn_period_month_1:
                monthAgo.add (monthAgo.MONTH,-1);
                startDate = String.format("%d.%02d.%02d", monthAgo.get(monthAgo.YEAR), monthAgo.get(monthAgo.MONTH)+1, monthAgo.get(monthAgo.DATE));
                break;
            case R.id.btn_period_month_3:
                monthAgo.add (monthAgo.MONTH,-3);
                startDate = String.format("%d.%02d.%02d", monthAgo.get(monthAgo.YEAR), monthAgo.get(monthAgo.MONTH)+1, monthAgo.get(monthAgo.DATE));
                break;
            case R.id.btn_period_month_6:
                monthAgo.add (monthAgo.MONTH,-6);
                startDate = String.format("%d.%02d.%02d", monthAgo.get(monthAgo.YEAR), monthAgo.get(monthAgo.MONTH)+1, monthAgo.get(monthAgo.DATE));
                break;
            case R.id.btn_period_month_all:
                break;
        }

        endDate = String.format("%d.%02d.%02d", year, month, cal.get(Calendar.DATE));

//        if (startDate.indexOf(endDate) > 0) {
//            CDialog.showDlg(getContext(), "조회 시작일과 종료일을 확인해주세요.");
//        }

        Tr_login login = UserInfo.getLoginInfo();
        requestData.mber_sn = login.mber_sn;
        requestData.start_de = StringUtil.getIntString(startDate);
        requestData.end_de = StringUtil.getIntString(endDate);
        requestData.pageNumber = "" + (mPageNo);

        MediNewNetworkModule.doApi(getContext(), Tr_mber_point_use_list.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_mber_point_use_list) {
                    Tr_mber_point_use_list data = (Tr_mber_point_use_list) responseData;
                    try {
                        mCurrentPointTv.setText(StringUtil.getFormatPrice(data.point_usr_amt));
                        mTotalPointTv.setText(StringUtil.getFormatPrice(data.point_user_sum_amt));

                        mAdapter.setData(data.point_day_list);

                        maxpage = data.maxpageNumber;

//                        if (Integer.parseInt(data.point_usr_amt) >= 0){
//                            mCurrentPointTv.setTextColor(getResources().getColor(R.color.point_blue));
//                        }else{
//                            mCurrentPointTv.setTextColor(getResources().getColor(R.color.colorMain));
//                        }

                        if ("0".equals(data.maxpageNumber) || StringUtil.getIntVal(data.maxpageNumber) < mPageNo) {
                            mPageNo = 1;
                            mIsLast = true;
                        } else {
                            mPageNo++;
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
            }
        });

    }


    class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
        List<Tr_mber_point_use_list.pointDay> mList = new ArrayList<>();

        @Override
        public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mypage_point_info_item, parent, false);
            return new ViewHolder(itemView);
        }

        public void setData(List<Tr_mber_point_use_list.pointDay> dataList) {
            if (mPageNo == 1)
                mList.clear();

            mList.addAll(dataList);
            notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(final RecyclerAdapter.ViewHolder holder, final int position) {
            final Tr_mber_point_use_list.pointDay data = mList.get(position);

            holder.pointDateTv.setText(CDateUtil.getDateTimeFormatyyyyMMddhhmm(data.input_de));
            holder.pointContentTv.setText(data.point_txt);

            holder.pointLogTv.setText(getString(R.string.point_decimal_remain, StringUtil.getIntValMinus(data.remain_point_amt           )));
            holder.pointRemainTv.setText(getString(R.string.point_decimal, StringUtil.getIntValMinus(data.accml_amt)));


//            if (Integer.parseInt(data.accml_amt) >= 0){
//                holder.pointRemainTv.setTextColor(getResources().getColor(R.color.point_blue));
//            }else{
//                holder.pointRemainTv.setTextColor(getResources().getColor(R.color.colorMain));
//            }
        }

        @Override
        public int getItemCount() {
            return mList == null ? 0 : mList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView pointLogTv;
            TextView pointRemainTv;
            TextView pointContentTv;
            TextView pointDateTv;


            public ViewHolder(View itemView) {
                super(itemView);

                pointLogTv = itemView.findViewById(R.id.point_remain_textview);
                pointRemainTv = itemView.findViewById(R.id.point_info_log_textview);
                pointContentTv = itemView.findViewById(R.id.point_content_textview);
                pointDateTv = itemView.findViewById(R.id.point_date_textview);
            }
        }
    }

    public void reset(int previousTotal, boolean loading) {
        this.previousTotal = previousTotal;
        this.loading = loading;
    }




    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mPageNo = 1;
    }

    @Override
    public void onResume() {
        super.onResume();
//        if ()

        reset(0, true);
    }

}
