package kr.co.hi.medicare.fragment.health.step;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.fragment.IBaseFragment;
import kr.co.hi.medicare.value.Define;
import kr.co.hi.medicare.net.hwNet.ApiData;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.text.DecimalFormat;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperWeight;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.net.hwdata.Tr_mvm_goalqy;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.utilhw.ViewUtil;

/**
 * Created by insystemscompany on 2017. 2. 28..
 */

public class StepInputFragment extends BaseFragmentMedi implements IBaseFragment {

    private EditText mStepEt, mTargetStepEt;
    private Button typeButton;

//    private CheckBox mGoogleFitCb;
//    private CheckBox mBandCb;

    private CheckBox chkCaledit;
    private CheckBox chkStepedit;

    private View mCheckedEditText;

    private InputMethodManager imm;


    public static Fragment newInstance() {
        StepInputFragment fragment = new StepInputFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_walk_input, container, false);

        /**
         * 상단 바 셋팅
         */
        TextView saveBtn = view.findViewById(R.id.common_toolbar_right_btn);
        saveBtn.setText(getString(R.string.text_save));
        saveBtn.setOnClickListener(mClickListener);

        mStepEt = view.findViewById(R.id.step_edittext);
        mTargetStepEt = view.findViewById(R.id.step_target_edittext);

//        mGoogleFitCb = (CheckBox) view.findViewById(R.id.checkbox_google);
//        mBandCb = (CheckBox) view.findViewById(R.id.checkbox_urban);

        chkCaledit = (CheckBox) view.findViewById(R.id.checkbox_caledit);      // 칼로리수정 체크박스
        chkStepedit = (CheckBox) view.findViewById(R.id.checkbox_stepedit); // 걸음수수정 체크박스

        imm = (InputMethodManager) getContext().getSystemService(getContext().INPUT_METHOD_SERVICE);

        mStepEt.setEnabled(false);
        mStepEt.setFocusableInTouchMode(false);
        mTargetStepEt.setEnabled(false);
        mTargetStepEt.setFocusableInTouchMode(false);

//        mGoogleFitCb.setChecked(false);
//        mBandCb.setChecked(false);

        /** font Typeface 적용 */
        typeButton = (Button)view.findViewById(R.id.walk_input_save_btn);
        ViewUtil.setTypefacenotosanskr_bold(getContext(), typeButton);
        typeButton.setOnClickListener(mClickListener);

        Tr_login login = UserInfo.getLoginInfo();
        if(!login.goal_mvm_calory.toString().isEmpty()){
            mStepEt.setHint(makeStringComma(login.goal_mvm_calory.replace(",","")));
        }

        if(!login.goal_mvm_stepcnt.toString().isEmpty()){
            mTargetStepEt.setHint(makeStringComma(login.goal_mvm_stepcnt.replace(",","")));
        }

        int dataSource = SharedPref.getInstance().getPreferences(SharedPref.STEP_DATA_SOURCE_TYPE, Define.STEP_DATA_SOURCE_GOOGLE_FIT);
//        if (dataSource != -1) {
//            if (Define.STEP_DATA_SOURCE_GOOGLE_FIT == dataSource) {
//                // 구글 피트니스 소스
//                mGoogleFitCb.setChecked(true);
//                mBandCb.setChecked(false);
//            } else if (Define.STEP_DATA_SOURCE_BAND == dataSource) {
//                // 스마트 밴드
//                mGoogleFitCb.setChecked(false);
//                mBandCb.setChecked(true);
//            }
//        }
//
//        mGoogleFitCb.setOnClickListener(mCheckClickListener);
//        mBandCb.setOnClickListener(mCheckClickListener);
        chkCaledit.setOnClickListener(mCheckClickListener);
        chkStepedit.setOnClickListener(mCheckClickListener);

        mStepEt.addTextChangedListener(watcher);
        mTargetStepEt.addTextChangedListener(watcher);

        mStepEt.setOnFocusChangeListener(mFocusChangeListener);
        mTargetStepEt.setOnFocusChangeListener(mFocusChangeListener);


        mStepEt.setOnKeyListener( new View.OnKeyListener() {
              public boolean onKey(View v, int keyCode, KeyEvent event) {
                  if(event.getKeyCode()==KeyEvent.KEYCODE_ENTER) {
                      if((StringUtil.getIntVal(mStepEt.getText().toString()) < 150 || StringUtil.getIntVal(mStepEt.getText().toString()) > 1000) && mStepEt.getText().length() > 0){
                          Toast.makeText(getContext(), getContext().getString(R.string.step_input_over), Toast.LENGTH_LONG).show();
                      }
                  }

                  return false;
              }
        });

        mTargetStepEt.setOnKeyListener( new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(event.getKeyCode()==KeyEvent.KEYCODE_ENTER) {
                    if((StringUtil.getIntVal(mStepEt.getText().toString()) < 150 || StringUtil.getIntVal(mStepEt.getText().toString()) > 1000) && mStepEt.getText().length() > 0){
                        Toast.makeText(getContext(), getContext().getString(R.string.step_input_over), Toast.LENGTH_LONG).show();
                    }
                }

                return false;
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        //super.loadActionbar(actionBar);
//        actionBar.setWhiteTheme();
//        actionBar.setActionBarTitle( getString(R.string.text_target_modify));
//    }

    /*
     **문자열 포맷
     */
    public String makeStringComma(String str) {
        if (str.length() == 0)
            return "";
        long value = Long.parseLong(str);
        DecimalFormat format = new DecimalFormat("###,###");
        return format.format(value);
    }

    View.OnFocusChangeListener mFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            mCheckedEditText = v;
            if (v == mStepEt && hasFocus == false) {
                if((StringUtil.getIntVal(mStepEt.getText().toString()) < 150 || StringUtil.getIntVal(mStepEt.getText().toString()) > 1000) && mStepEt.getText().length() > 0){
                    Toast.makeText(getContext(), getContext().getString(R.string.step_input_over), Toast.LENGTH_LONG).show();
                }
            } else if(v == mTargetStepEt && hasFocus == false) {
                if((StringUtil.getIntVal(mStepEt.getText().toString()) < 150 || StringUtil.getIntVal(mStepEt.getText().toString()) > 1000) && mStepEt.getText().length() > 0){
                    Toast.makeText(getContext(), getContext().getString(R.string.step_input_over), Toast.LENGTH_LONG).show();
                }
            }
        }
    };

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            if (vId == R.id.common_toolbar_right_btn) {
                final String mStep = mStepEt.getText().toString();
                if (TextUtils.isEmpty(mStep) || StringUtil.getIntVal(mStep) <= 0) {
                    CDialog.showDlg(getContext(), "하루 소모 칼로리를 정확히 입력해 주세요.");
                    return;
                }
                if (StringUtil.getIntVal(mStepEt.getText().toString()) < 150 || StringUtil.getIntVal(mStepEt.getText().toString()) > 1000) {
                    CDialog.showDlg(getContext(), getContext().getString(R.string.step_input_over));
                    return;
                }
                final String mTargetStep = mTargetStepEt.getText().toString();
                if (TextUtils.isEmpty(mTargetStep) || (StringUtil.getIntVal(mTargetStep) <= 0 || StringUtil.getIntVal(mTargetStep) > 500000)) {
                    CDialog.showDlg(getContext(), "하루 목표 걸음수를 정확히 입력해 주세요.");
                    return;
                }

                doInputStep(mStep, mTargetStep);

//                CDialog.showDlg(getContext(), "하루 목표 소모칼로리 및 걸음 수를 수정하시겠습니까?", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                    }
//                }, null);
            }
        }
    };


    View.OnClickListener mCheckClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            final CheckBox chkBtn = (CheckBox)v;

            if (chkBtn == chkCaledit ) {
                if (chkCaledit.isChecked() == true) {
                    chkCaledit.setChecked(true);
                    chkStepedit.setChecked(false);

                    mTargetStepEt.setEnabled(false);
                    mTargetStepEt.setFocusable(false);
                    mTargetStepEt.setFocusableInTouchMode(false);
                    mTargetStepEt.requestFocus();
//                    imm.hideSoftInputFromInputMethod(mTargetStepEt.getWindowToken(), 0);

                    mStepEt.setEnabled(true);
                    mStepEt.setFocusable(true);
                    mStepEt.setFocusableInTouchMode(true);
                    mStepEt.requestFocus();
//                    imm.showSoftInput(mStepEt, InputMethodManager.SHOW_IMPLICIT);

                }else {
                    chkCaledit.setChecked(false);

                    mStepEt.setEnabled(false);
                    mStepEt.setFocusable(false);
                    mStepEt.setFocusableInTouchMode(false);
                    mStepEt.requestFocus();
//                    imm.hideSoftInputFromInputMethod(mStepEt.getWindowToken(), 0);
                }
            }else if (chkBtn == chkStepedit){
                if (chkStepedit.isChecked() == true) {
                    chkStepedit.setChecked(true);
                    chkCaledit.setChecked(false);

                    mStepEt.setEnabled(false);
                    mStepEt.setFocusable(false);
                    mStepEt.setFocusableInTouchMode(false);
                    mStepEt.requestFocus();
//                    imm.hideSoftInputFromInputMethod(mStepEt.getWindowToken(), 0);

                    mTargetStepEt.setEnabled(true);
                    mTargetStepEt.setFocusable(true);
                    mTargetStepEt.setFocusableInTouchMode(true);
                    mTargetStepEt.requestFocus();
//                    imm.showSoftInput(mTargetStepEt, InputMethodManager.SHOW_IMPLICIT);
                }else {
                    chkStepedit.setChecked(false);

                    mTargetStepEt.setEnabled(false);
                    mTargetStepEt.setFocusable(false);
                    mTargetStepEt.setFocusableInTouchMode(false);
                    mTargetStepEt.requestFocus();
//                    imm.hideSoftInputFromInputMethod(mTargetStepEt.getWindowToken(), 0);
                }
            }
//            else if (chkBtn == mGoogleFitCb || chkBtn== mBandCb) {
//
//                if (chkBtn.isChecked() == false) {
//                    chkBtn.setChecked(true);
//                    return;
//                }
//
//                String message = "";
//                if (chkBtn == mGoogleFitCb) {
//                    mBandCb.setChecked(chkBtn.isChecked() ? false : true);
//                    message = getString(R.string.text_googlefit) + "를 이용하여 운동데이터를 측정하시겠습니까?";
//                } else if (chkBtn == mBandCb) {
//                    mGoogleFitCb.setChecked(chkBtn.isChecked() ? false : true);
//                    message = getString(R.string.text_croise) + "를 이용하여 운동데이터를 측정하시겠습니까?";
//                }
//
//                CDialog.showDlg(getContext(), message, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        int code = mGoogleFitCb.isChecked() ?
//                                Define.STEP_DATA_SOURCE_GOOGLE_FIT
//                                : Define.STEP_DATA_SOURCE_BAND;
//
//                        if (Define.STEP_DATA_SOURCE_BAND == code) {
//                            if (DeviceManager.isRegDevice(getContext(), DeviceManager.FLAG_BLE_DEVICE_BAND)) {
//                                SharedPref.getInstance().savePreferences(SharedPref.STEP_DATA_SOURCE_TYPE, Define.STEP_DATA_SOURCE_BAND);
//                                onBackPressed();
//                            } else {
//                                Bundle bundle = new Bundle();
//                                bundle.putBoolean(DeviceManagementFragment.IS_STEP_DEVICE_REGIST, true);
//                                NewActivity.startActivityForResult(StepInputFragment.this, Define.STEP_DATA_SOURCE_BAND, DeviceManagementFragment.class, bundle);
//                            }
//                        } else {
//                            SharedPref.getInstance().savePreferences(SharedPref.STEP_DATA_SOURCE_TYPE, code);
//                            onBackPressed();
//                        }
//                    }
//                }, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (chkBtn == mGoogleFitCb) {
//                            mGoogleFitCb.setChecked(false);
//                            mBandCb.setChecked(true);
//                        } else if (chkBtn == mBandCb) {
//                            mGoogleFitCb.setChecked(true);
//                            mBandCb.setChecked(false);
//                        }
//                    }
//                });
//
//            }
        }
    };

    private void doInputStep(String mStep , String mTargetStep) {

        Tr_login login = UserInfo.getLoginInfo();

        Tr_mvm_goalqy.RequestData requestData = new Tr_mvm_goalqy.RequestData();
        requestData.mber_sn = login.mber_sn;
        requestData.goal_mvm_calory = mStep;
        requestData.goal_mvm_stepcnt = mTargetStep;

        getData(getContext(), Tr_mvm_goalqy.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_mvm_goalqy) {
                    Tr_mvm_goalqy data = (Tr_mvm_goalqy)obj;
                    if ("Y".equals(data.reg_yn)) {
                        UserInfo user = new UserInfo(getContext());
                        MediNewNetworkModule.doReLoginMediCare(getContext(), new MediNewNetworkHandler() {
                            @Override
                            public void onSuccess(BaseData responseData) {

                                Tr_login login = UserInfo.getLoginInfo();
                                login.goal_mvm_calory = mStepEt.getText().toString();
                                login.goal_mvm_stepcnt = mTargetStepEt.getText().toString();

                                UserInfo user = new UserInfo(getContext());
                                MediNewNetworkModule.doReLoginMediCare(getContext(),new MediNewNetworkHandler() {
                                    @Override
                                    public void onSuccess(BaseData responseData) {
                                        mStepEt.setText("");
                                        mTargetStepEt.setText("");
                                        onBackPressed();
                                    }

                                    @Override
                                    public void onFailure(int statusCode, String response, Throwable error) {
                                        mStepEt.setText("");
                                        mTargetStepEt.setText("");
                                        onBackPressed();
                                    }
                                });
//                                CDialog.showDlg(getContext(), "수정되었습니다.", new CDialog.DismissListener() {
//                                    @Override
//                                    public void onDissmiss() {
//
//
//                                    }
//                                });
                            }

                            @Override
                            public void onFailure(int statusCode, String response, Throwable error) {
                                Tr_login login = UserInfo.getLoginInfo();
                                login.goal_mvm_calory = mStepEt.getText().toString();
                                login.goal_mvm_stepcnt = mTargetStepEt.getText().toString();

                                MediNewNetworkModule.doReLoginMediCare(getContext(), new MediNewNetworkHandler() {
                                    @Override
                                    public void onSuccess(BaseData responseData) {
                                        mStepEt.setText("");
                                        mTargetStepEt.setText("");
                                        onBackPressed();
                                    }

                                    @Override
                                    public void onFailure(int statusCode, String response, Throwable error) {
                                        mStepEt.setText("");
                                        mTargetStepEt.setText("");
                                        onBackPressed();
                                    }
                                });

//                                CDialog.showDlg(getContext(), "수정되었습니다.", new CDialog.DismissListener() {
//                                    @Override
//                                    public void onDissmiss() {
//
//
//                                    }
//                                });
                            }
                        });
                    } else {
                        CDialog.showDlg(getContext(), getString(R.string.text_update_fail));
                    }

                }
            }
        });
    }



    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(s.toString().length() > 0){

                Tr_login login      = UserInfo.getLoginInfo();
                int sex             = StringUtil.getIntVal(login.mber_sex);
                DBHelper helper = new DBHelper(getContext());
                DBHelperWeight weightDb = helper.getWeightDb();
                DBHelperWeight.WeightStaticData bottomData = weightDb.getResultStatic();
                float weight        = 0.f;
                if(!bottomData.getWeight().isEmpty()) {
                    weight          = StringUtil.getFloatVal(login.mber_bdwgh_app);
                }else{
                    weight          = StringUtil.getFloatVal(login.mber_bdwgh);
                }
                float height        = StringUtil.getFloatVal(login.mber_height);
                int sInt            =  StringUtil.getIntVal(s.toString());

                if(chkCaledit.isChecked() == true){
                    if(mStepEt.getText().toString().length() > 0){
                        mTargetStepEt.removeTextChangedListener(this);
                        mTargetStepEt.setText(""+getStepTargetCalulator(sex, height, weight, sInt));
                        mTargetStepEt.addTextChangedListener(this);
                    }

                }else{
                    if(mTargetStepEt.getText().toString().length() > 0) {
                        mStepEt.removeTextChangedListener(this);
                        mStepEt.setText("" + getCalroriTargetCalulator(sex, height, weight, sInt));
                        mStepEt.addTextChangedListener(this);
                    }
                }
            }else{
                if(chkCaledit.isChecked() == true){
                    mTargetStepEt.removeTextChangedListener(this);
                    mTargetStepEt.setText("");
                    mTargetStepEt.addTextChangedListener(this);
                }else{
                    mStepEt.removeTextChangedListener(this);
                    mStepEt.setText("");
                    mStepEt.addTextChangedListener(this);
                }
            }

            if (isValidStep() && isValidTargetStep()) {
                typeButton.setEnabled(true);
            } else {
                typeButton.setEnabled(false);
            }
        }
    };

    private boolean isValidStep() {
        String text = mStepEt.getText().toString();
        return TextUtils.isEmpty(text) == false;
    }

    private boolean isValidTargetStep() {
        String text = mTargetStepEt.getText().toString();
        return TextUtils.isEmpty(text) == false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Define.STEP_DATA_SOURCE_BAND) {
            int typeSource = SharedPref.getInstance().getPreferences(SharedPref.STEP_DATA_SOURCE_TYPE, Define.STEP_DATA_SOURCE_GOOGLE_FIT);
//            if (Define.STEP_DATA_SOURCE_BAND == typeSource) {
//                // 밴드 등록 성공인 경우
//                mGoogleFitCb.setChecked(false);
//                mBandCb.setChecked(true);
//                restartMainActivity();
//            } else {
//                mGoogleFitCb.setChecked(true);
//                mBandCb.setChecked(false);
//            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        imm.hideSoftInputFromWindow(mStepEt.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(mTargetStepEt.getWindowToken(), 0);

    }
}