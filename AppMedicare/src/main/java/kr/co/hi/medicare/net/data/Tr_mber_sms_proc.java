package kr.co.hi.medicare.net.data;

/**
 *
 * 문자인증요청(간편인증)
 *
 Input 값
 insures_code: 회사코드
 token: 정회원/준회원구분
 mber_hp: 휴대폰번호

 Output 값
 api_code: 호출코드명
 insures_code: 회사코드
 mber_hp: 회원번호
 result_key: 인증키
 result_code: 결과코드


 0000: 정회원이지만 회원가입을 안한경우(가입성공)
 1111: 정회원이지만 이미 회원가입이 된경우(가입처리안함)
 4444: 아이디중복
 2222: 준회원이고 회원가입을 안한경우 (가입성공)
 9999: 처리실패(준회원이지만 회원가입이 된경우 / 기타오류)
 */

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;


public class Tr_mber_sms_proc extends BaseData {
    private final String TAG = getClass().getSimpleName();

    public Tr_mber_sms_proc() {
//    super.conn_url ="https://wkd.walkie.co.kr:443/HS_HL/ws.asmx/getJson";
    }

    public static class RequestData {
        public String mber_hp; // 1000
        public String token; // 99
    }


    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {

            JSONObject body = new JSONObject();

            RequestData data = (RequestData) obj;
            body.put("api_code", TAG.replace("Tr_", ""));
            body.put("insures_code", INSURES_CODE);
            body.put("mber_hp", data.mber_hp); // 1000",
            body.put("token", data.token); // 99

            return body;
        }

        return super.makeJson(obj);
    }

/**************************************************************************************************/
/***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("insures_code")
    public String insures_code;
    @SerializedName("api_code")
    public String api_code;

    @SerializedName("mber_hp")
    public String mber_hp;
    @SerializedName("result_code")
    public String result_code;
    @SerializedName("result_key")
    public String result_key;

}
