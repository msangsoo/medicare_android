package kr.co.hi.medicare.fragment.community.adapter;

public interface LoadMoreListener {

    final int VIEW_PROG = 0;
    final int VIEW_ITEM = 1;
    final int VIEW_EMPTY = 2;
    final int VIEW_HEADER = 3;

    final String PROGRESSBAR_TYPE_CENTER="CENTER";
    final String PROGRESSBAR_TYPE_TOP="TOP";
    final String PROGRESSBAR_TYPE_BOTTOM="BOTTOM";
    final String PAGE_SIZE="10";

    void onLoadMore();
}



