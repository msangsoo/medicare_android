package kr.co.hi.medicare.fragment.health.step;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.health.HealthFragment;
import kr.co.hi.medicare.utilhw.DisplayUtil;
import kr.co.hi.medicare.utilhw.SharedPref;

/**
 * Created by insystemscompany on 2017. 2. 28..
 */

public class StepManageFragment extends BaseFragmentMedi {
    private static final String TAG = StepManageFragment.class.getSimpleName();

    private GoogleFitChartView mGoogleFitView;
    private View mView;
    private LinearLayout mLabelayout;

    private ImageView mHealthMessage;
    private RelativeLayout message_lv;

    private View mVisibleView1;
    private View mVisibleView2;
    private View mVisibleView3;
    private View mPerodLayout;
    private View mDateLayout;
    private View mSelectChartLayout;
    private View mChartFrameLayout;
    private ScrollView mContentScrollView;
    private ImageView mChartCloseBtn, mChartZoomBtn;

    private LinearLayout mRadio_btn_calory;

    private ImageButton action_bar_write_step_btn, share_write;

    LinearLayout.LayoutParams params;
    int tempHeight=0;

    public static Fragment newInstance() {
        StepManageFragment fragment = new StepManageFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        message_lv = getActivity().findViewById(R.id.message_lv);
        mHealthMessage = getActivity().findViewById(R.id.common_toolbar_back_btn);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_walk_manage, container, false);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mView = view;

        mChartCloseBtn = view.findViewById(R.id.chart_close_btn);
        mChartZoomBtn = view.findViewById(R.id.landscape_btn);
        mVisibleView1 = view.findViewById(R.id.visible_view1);
        mVisibleView2 = view.findViewById(R.id.calorie_lv);
        mVisibleView3 = view.findViewById(R.id.step_lv);

        share_write = view.findViewById(R.id.share_write);
        action_bar_write_step_btn = view.findViewById(R.id.action_bar_write_step_btn);

        mPerodLayout = view.findViewById(R.id.period_select_layout);
        mDateLayout = view.findViewById(R.id.chart_date_layout);
        mSelectChartLayout = view.findViewById(R.id.chart_select_tab_layout);
        mLabelayout  = view.findViewById(R.id.chart_label_layout);

        mChartFrameLayout = view.findViewById(R.id.chart_frame_layout);
        mContentScrollView = view.findViewById(R.id.layout_chart_scrollview);

        mGoogleFitView = new GoogleFitChartView(StepManageFragment.this, view);   // 차트뷰 세팅
        view.findViewById(R.id.action_bar_write_step_btn).setOnClickListener(mClickListener);
        mChartCloseBtn.setOnClickListener(mClickListener);
        mChartZoomBtn.setOnClickListener(mClickListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, getContext());


        //건강관리
        view.findViewById(R.id.action_bar_write_step_btn).setOnTouchListener(ClickListener);

        //코드부여
        view.findViewById(R.id.action_bar_write_step_btn).setContentDescription(getString(R.string.action_bar_write_step_btn));


        mRadio_btn_calory = view.findViewById(R.id.radio_btn_calory);

        params = (LinearLayout.LayoutParams) mChartFrameLayout.getLayoutParams();
        tempHeight = params.height;


    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            if(vId == R.id.action_bar_write_step_btn){
                NewActivity.startActivityForResult(StepManageFragment.this, 1111, StepInputFragment.class, new Bundle());
            } else if(vId == R.id.landscape_btn) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            } else if(vId == R.id.chart_close_btn) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        }
    };

    public void onChartViewResume(){
        if (mGoogleFitView == null)
            mGoogleFitView = new GoogleFitChartView(StepManageFragment.this, mView);   // 차트뷰 세팅
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getCurrentFragment()  == MainActivityMedicare.HOME_MENU_2) {
            if (getCurrentTopFragment(StepManageFragment.this) == 1) {
                Log.i(TAG, "onResume.mGoogleFitView=" + mGoogleFitView);
                if (mGoogleFitView != null) {
                    mGoogleFitView.onResume();
                }

                if (SharedPref.getInstance().getPreferences(SharedPref.HEALTH_MESSAGE, false)) {
                    mHealthMessage.setImageResource(R.drawable.health_m_btn_01ov);
                    message_lv.setVisibility(View.VISIBLE);
                } else {
                    mHealthMessage.setImageResource(R.drawable.health_m_btn_01);
                    message_lv.setVisibility(View.GONE);
                }
            }
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop.mGoogleFitView="+mGoogleFitView);
        if (mGoogleFitView != null) {
            mGoogleFitView.onStop();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.i(TAG, "onDetach.mGoogleFitView="+mGoogleFitView);
        if (mGoogleFitView != null) {
            mGoogleFitView.onDetach();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.i(TAG, "onConfigurationChanged="+newConfig.orientation);
        if (getCurrentFragment()  == MainActivityMedicare.HOME_MENU_2) {
            if (getCurrentTopFragment(StepManageFragment.this) == 1) {
                setVisibleOrientationLayout(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE);
            }
        }
    }

    /**
     * 가로, 세로모드일때 불필요한 화면 Visible 처리
     */
    public void setVisibleOrientationLayout(final boolean isLandScape) {
        mGoogleFitView.mIslandscape = isLandScape;
        mGoogleFitView.setLabelLayoutVisible(isLandScape);
        mVisibleView1.setVisibility(isLandScape ? View.GONE : View.VISIBLE);

        if(mRadio_btn_calory.isSelected()){
            mVisibleView2.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
            mVisibleView3.setVisibility(isLandScape ? View.GONE : View.GONE);
        } else {
            mVisibleView2.setVisibility(isLandScape ? View.GONE : View.GONE);
            mVisibleView3.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        }
        mSelectChartLayout.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mChartCloseBtn.setVisibility(isLandScape ? View.VISIBLE : View.GONE);
        mChartZoomBtn.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mPerodLayout.setVisibility(isLandScape ? View.GONE : View.VISIBLE);

        share_write.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        action_bar_write_step_btn.setVisibility(isLandScape ? View.GONE : View.VISIBLE);

        DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
        int portHeight = DisplayUtil.getDpToPix(getContext(),tempHeight);    // 세로모드일때 사이즈 250dp(레이아웃에 설정되어 있는 사이즈)
        int landHeight = (int) (dm.heightPixels
                - mDateLayout.getMeasuredHeight()               // 날자 표시
//                - DisplayUtil.getStatusBarHeight(getContext())  // 상태바 높이
        );

        if (mLabelayout.getVisibility() == View.VISIBLE) {
            landHeight -= mLabelayout.getMeasuredHeight()*1.2;                // 라벨 표시
            landHeight -= DisplayUtil.getStatusBarHeight(getContext());     // 상태바 높이
        } else {
            landHeight -= mLabelayout.getMeasuredHeight();                // 라벨 표시
        }


        params.height = isLandScape ? landHeight : portHeight;
        mChartFrameLayout.setLayoutParams(params);

        // 가로모드일때 스크롤뷰 막기
        mContentScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return isLandScape;
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //가로모드 전환 시 스크롤 상단으로 위치
                mContentScrollView.smoothScrollTo(0,0);
            }
        }, 100);
    }
}