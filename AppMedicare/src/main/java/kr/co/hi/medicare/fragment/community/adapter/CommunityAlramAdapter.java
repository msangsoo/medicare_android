package kr.co.hi.medicare.fragment.community.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.community.data.CommunityNoticeData;
import kr.co.hi.medicare.tempfunc.TemporaryFunction;


public class CommunityAlramAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private View.OnClickListener onClickListener;
    private Context context;
    private ArrayList<CommunityNoticeData> data;

    public void deleteItem(String CM_SEQ) {
        for(int i=0; i<data.size();i++){
            if(data.get(i).CM_SEQ.equals(CM_SEQ)){
                data.remove(i);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i,data.size());
                break;
            }
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView profile,profile_teduri;
        TextView msg,regdate;

        public ViewHolder(View itemView) {
            super(itemView);
            profile = itemView.findViewById(R.id.profile);
            profile_teduri = itemView.findViewById(R.id.profile_teduri);//no
            msg = itemView.findViewById(R.id.msg);
            regdate = itemView.findViewById(R.id.regdate);
        }

        public void SetView(CommunityNoticeData data,Context context){
            if(data.MSG.indexOf("@")==0&&data.MSG.contains(" ")){
                setMessageBold(msg, data.MSG,0, data.MSG.indexOf(" ") );
            }else{
                msg.setText(data.MSG);
            }

            regdate.setText(TemporaryFunction.getDateFormat(data.REGDATE));


            if(data.CM_GUBUN.equals("2")){
                Glide
                        .with(context)
                        .load(R.drawable.commu_s_04)
                        .into(profile);

            }else if(data.PROFILE_PIC==null||data.PROFILE_PIC.equals("")){
                Glide
                        .with(context)
                        .load(R.drawable.main_tab_05_1)
                        .into(profile);
            }else{
                Glide
                        .with(context)
                        .load(CommonFunction.getThumbnail(data.PROFILE_PIC))
                        .into(profile);
            }


            if(data.CM_GUBUN.equals("2")){
                Glide
                        .with(context)
                        .load(R.drawable.commu_s_02)
                        .into(profile_teduri);
            }else if(data.MBER_GRAD==null||!data.MBER_GRAD.equals("10")){
                Glide
                        .with(context)
                        .load(R.drawable.commu_s_01)
                        .into(profile_teduri);
            }else{
                Glide
                        .with(context)
                        .load(R.drawable.commu_s_02)
                        .into(profile_teduri);
            }



        }

        private void setMessageBold(TextView view,String message, int start, int end){
            SpannableStringBuilder sp = new SpannableStringBuilder(message);
            sp.setSpan( new StyleSpan(Typeface.BOLD),start,end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            view.setText(sp);
        }
    }



    public CommunityAlramAdapter(Context context, View.OnClickListener onClickListener) {
        this.context = context;
        this.onClickListener = onClickListener;
        data = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_community_alram, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ViewHolder){
            if (data.size() > 0) {
                if (position < data.size()) {
                    //뷰홀더의 자원을 초기화//
                    final ViewHolder commViewHolder = (ViewHolder) holder;
                    commViewHolder.SetView(data.get(position),context);

                    commViewHolder.msg.setTag(R.id.comm_alram,position);
                    commViewHolder.profile.setTag(R.id.comm_alram,position);
                    commViewHolder.msg.setOnClickListener(onClickListener);
                    commViewHolder.profile.setOnClickListener(onClickListener);
                    return;
                }

            }
        }
    }

    @Override
    public int getItemCount() {
        if (data==null) {
            return 0;
        }
        return data.size();
    }

    public CommunityNoticeData getData(int position){

        return data.get(position);
    }


    public void delItemAll() {
        if(data!=null){
            this.data.clear();
            notifyDataSetChanged();
        }
    }

    public void addAllItem(List<CommunityNoticeData> data) {
        if(this.data==null)
            this.data = new ArrayList<>();

        if(data!=null){
            this.data.clear();
            this.data.addAll(data);
            notifyDataSetChanged();
        }
    }

    public void addItemMore(List<CommunityNoticeData> data){
        int sizeInit = this.data.size();
        this.data.addAll(data);
        notifyItemRangeChanged(sizeInit, this.data.size());
    }


    public void addItem(CommunityNoticeData data) {
        if(this.data==null)
            this.data = new ArrayList<>();

        if(data!=null){
            ArrayList<CommunityNoticeData> datas = new ArrayList<>();
            datas.add(data);
            datas.addAll(this.data);
            this.data.clear();
            this.data = datas;
            notifyDataSetChanged();
        }
    }
}
