package kr.co.hi.medicare.fragment.community.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CommunityProfileData implements Serializable {
    @Expose
    @SerializedName("NICKNAME")
    public String NICKNAME;
    @Expose
    @SerializedName("MBER_SN")
    public String MBER_SN;
    @Expose
    @SerializedName("PROFILE_PIC")
    public String PROFILE_PIC;

    public String LV;
    public String MBER_SEX;
    public String DISEASE_NM;
    public String DISEASE_OPEN;
    public String TOT_POINT;
    public String TOT_CNT;
    public String MBER_NM;
    public String MBER_GRAD;
    public String MYHEART;
    public boolean IS_MEMBER;

}
