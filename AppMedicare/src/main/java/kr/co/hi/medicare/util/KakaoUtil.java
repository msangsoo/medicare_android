package kr.co.hi.medicare.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.kakao.kakaolink.v2.KakaoLinkResponse;
import com.kakao.kakaolink.v2.KakaoLinkService;
import com.kakao.message.template.ContentObject;
import com.kakao.message.template.FeedTemplate;
import com.kakao.message.template.LinkObject;
import com.kakao.network.ErrorResult;
import com.kakao.network.callback.ResponseCallback;
import com.kakao.plusfriend.PlusFriendService;
import com.kakao.util.exception.KakaoException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * RequestUtil
 * 서버에 전달할 parameter.
 */
public class KakaoUtil {
    private final String TAG = KakaoUtil.class.getSimpleName();

    String Url = "";


    /**
     * 카카오톡 보내기
     * @param context
     * @param title
     * @param linkParam1
     * @param linkParam2
     * @param btn1title
     */


    public void sendKakaoMessage(Context context, String title, String linkParam1, String linkParam2, String btn1title) {
        createKakaoResponseCallBack(context);


        FeedTemplate params;
        String templateId;

        templateId="15187";

        Map<String, String> templateArgs = new HashMap<String, String>();

        templateArgs.put("title", title);
        templateArgs.put("btTitle", btn1title);
        templateArgs.put("params", "/HL/Share_sns/sns_contents.asp?seq= "+linkParam1+"&cm_seq="+linkParam2);



        KakaoLinkService.getInstance().sendCustom(context, templateId, templateArgs, serverCallbackArgs, callback);

    }


    public void sendLBDMessage(Context context, String imageUrl) {

        createKakaoResponseCallBack(context);
        FeedTemplate params = FeedTemplate
                .newBuilder(ContentObject.newBuilder("",
                        imageUrl,
                        //Url,
                        LinkObject.newBuilder().setWebUrl("https://developers.kakao.com")
                                .setMobileWebUrl("https://developers.kakao.com").build())
                        .setDescrption("")
                        .build())
                .build();

        KakaoLinkService.getInstance().sendDefault(context, params, serverCallbackArgs, callback);
    }

    private Map<String, String> serverCallbackArgs = getServerCallbackArgs();
    private ResponseCallback<KakaoLinkResponse> callback;
    private void createKakaoResponseCallBack(final Context context) {
        callback = new ResponseCallback<KakaoLinkResponse>() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                Toast.makeText(context, errorResult.getErrorMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(KakaoLinkResponse result) {
//                Toast.makeText(context, "Successfully sent KakaoLink v2 message.", Toast.LENGTH_LONG).show();
                android.util.Log.i(TAG,  "Successfully sent KakaoLink v2 message.");
            }
        };
    }

    private Map<String, String> getServerCallbackArgs() {
        Map<String, String> callbackParameters = new HashMap<>();
        callbackParameters.put("user_id", "1234");
        callbackParameters.put("title", "프로방스 자동차 여행 !@#$%");
        return callbackParameters;
    }



    /**
     * 문자메시지 보내기
     * @param context
     * @param smsText
     * @param url
     */

    public static void sendSMS(Context context, String smsText,String url) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");

        List<ResolveInfo> resInfos = context.getPackageManager().queryIntentActivities(intent, 0);

        Log.i("linksms","list: "+resInfos);

        if(resInfos != null || resInfos.size() != 0) {

            for (ResolveInfo info : resInfos) {
                if (info.activityInfo.packageName.toLowerCase().contains("mms") ||
                        info.activityInfo.name.toLowerCase().contains("mms") ||
                        info.activityInfo.packageName.toLowerCase().contains("com.samsung.android.messaging") ||
                        info.activityInfo.name.toLowerCase().contains("com.samsung.android.messaging")) {


                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                    intent.putExtra("sms_body", smsText + "\n\n\n" + url);
                    intent.putExtra(Intent.EXTRA_TEXT, smsText+url);

//                    intent.putExtra(Intent.EXTRA_STREAM,  Uri.parse(url));
                    intent.setPackage(info.activityInfo.packageName);
                    break;
                } else {
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                    intent.putExtra("sms_body", smsText +  "\n\n\n" + url);
                    intent.putExtra(Intent.EXTRA_TEXT, smsText+url);
//                    intent.setPackage(info.activityInfo.packageName);
                }
            }

            Intent openInChooser = Intent.createChooser(intent, "MMS:");
            context.startActivity(openInChooser);
        }
    }

    public static void kakaoAddFriends(Context context){
        try {
            PlusFriendService.getInstance().addFriend(context, "_tyRjj");
        } catch (KakaoException e) {
            // 에러 처리 (앱키 미설정 등등)
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }


    }

    public static void kakaoChatFriends(Context context){
        try {
            PlusFriendService.getInstance().chat(context, "_tyRjj");
        } catch (KakaoException e) {
            // 에러 처리 (앱키 미설정 등등)
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

}
