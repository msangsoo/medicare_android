package kr.co.hi.medicare.fragment.community.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

//// @JsonIgnoreProperties(ignoreUnknown = true)
public class CommunityCommentData implements Serializable {
    @Expose
    @SerializedName("REGDATE")
    public String REGDATE;
    @Expose
    @SerializedName("CM_CONTENT")
    public String CM_CONTENT;
    @Expose
    @SerializedName("PROFILE_PIC")
    public String PROFILE_PIC;
    @Expose
    @SerializedName("NICK")
    public String NICK;
    @Expose
    @SerializedName("OSEQ")
    public String OSEQ;
    @Expose
    @SerializedName("CM_SEQ")
    public String CM_SEQ;
    @Expose
    @SerializedName("CC_SEQ")
    public String CC_SEQ;
    @Expose
    @SerializedName("TPAGE")
    public String TPAGE;
    @Expose
    @SerializedName("MBER_GRAD")
    public String MBER_GRAD;
    @Expose
    @SerializedName("T_MBER_SN")
    public String T_MBER_SN;
}
