package kr.co.hi.medicare.util;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;

import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.SplashActivityMedicare;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.fragment.mypage.MyPageHealthNoticeData;
import kr.co.hi.medicare.push.NotiDummyActivity;

public class NotificationUtil extends BroadcastReceiver {
    private String channelId = "channel";
    private String channelName = "medihealthalram";
    private DBHelper mDbHelper=null;
    private int idx;
    private MyPageHealthNoticeData data=null;

    @Override
    public void onReceive(Context context, Intent intent) {
        mDbHelper = new DBHelper(context);
        if(intent==null){


        }else if (intent.getAction()!=null&&intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            List<MyPageHealthNoticeData> dataList = mDbHelper.getAlramHealth().getResultAll(mDbHelper);
            for(int i=0; i<dataList.size();i++){
                AlramUtil.setAlarm(context,dataList.get(i).idx,dataList.get(i).time_hhmm);
            }

        }else{
            Log.v("checkAlram","checkAlram:start");
            idx = intent.getIntExtra("idx",0);
            data = mDbHelper.getAlramHealth().select(idx);
            Log.v("checkAlram","checkAlram:start idx:"+idx+",dataisactive:"+ (data==null? "null" : data.isactive) );
            if(data!=null&&data.idx>0&&data.isactive.equals("Y")) {
//                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//
//                //오레오 대응
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
//                    notificationChannel.setDescription(context.getString(R.string.app_name));
//                    notificationChannel.enableLights(false);
//                    notificationChannel.enableVibration(false);
//
//
//
//                    notificationManager.createNotificationChannel(notificationChannel);
//
//                }
//
//                NotificationCompat.Builder builder = new NotificationCompat.Builder(context.getApplicationContext(), channelId);
//
//
////                Intent notificationIntent = new Intent(context.getApplicationContext(), NewActivity.class);  // 알림 클릭 시 이동할 액티비티 지정
//                PendingIntent pendingIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, new Intent(context, SplashActivityMedicare.class), PendingIntent.FLAG_UPDATE_CURRENT);
//
//                builder.setContentTitle(data.gubun) //제목
//                        .setContentText(data.message) //내용
//                        .setDefaults(Notification.DEFAULT_ALL) //알림 설정(사운드, 진동)
//                        .setAutoCancel(true) //터치 시 자동으로 삭제할 지 여부
//                        .setPriority(NotificationCompat.PRIORITY_HIGH) // 알림의 중요도
//                        .setSmallIcon(R.mipmap.ic_stat_notify)
//                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
//                        .setContentIntent(pendingIntent);
//
//                notificationManager.notify(0, builder.build());

                int notiId = (int)(System.currentTimeMillis()/1000);
                String menuId = "8";
                switch (data.gubun){
                    case "식사":
                        menuId="8";
                        break;
                    case "체중":
                        menuId="9";
                        break;
                    case "혈압":
                        menuId="10";
                        break;
                    case "혈당":
                        menuId="11";
                        break;
                }
                NotiDummyActivity.showPushMessage(context, notiId, "", data.message, menuId,"0");
            }
            // 비활성화/활성화 상태 관계없이 재등록을 진행
            AlramUtil.setAlarmRepeat(context,idx,data.time_hhmm, AlarmManager.INTERVAL_DAY);
            Log.v("checkAlram","checkAlram:last:"+ AlramUtil.checkAlram(context,idx) +",idx:"+idx);

        }



    }
}

