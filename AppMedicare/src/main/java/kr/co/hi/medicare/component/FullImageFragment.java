package kr.co.hi.medicare.component;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.utilhw.Logger;


/**
 * Created by MrsWin on 2017-03-01.
 */

public class FullImageFragment extends kr.co.hi.medicare.fragment.BaseFragmentMedi {
    private final String TAG = FullImageFragment.class.getSimpleName();
    public static String SAMPLE_BACK_DATA = "SAMPLE_BACK_DATA";
    public static String DRAWABLE_IMG = "drawable_img";

    /**
     * 액션바 세팅
     */
//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        super.loadActionbar(actionBar);
//        actionBar.setWhiteTheme();
//        actionBar.setActionBarTitle(getString(R.string.health_info_menu3));
//    }

    public static Fragment newInstance() {
        FullImageFragment fragment = new FullImageFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.disease_content_layout, container, false);
        return view;
    }
    private ImageView mIv;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String title  = getArguments().getString(CommonToolBar.TOOL_BAR_TITLE);
        int drawImg  = getArguments().getInt(DRAWABLE_IMG);
        getToolBar(view).setTitle(title);

        mIv = view.findViewById(R.id.full_content_imageview);
        Logger.i(TAG, "drawImg"+drawImg);
//        new IvLoadAsyctask().execute(drawImg);


//        Glide.with(getContext()).load(R.drawable.disease_info_01)
//                .apply(new RequestOptions()
//                        .diskCacheStrategy(DiskCacheStrategy.NONE)
//                        .skipMemoryCache(true)).into(mIv);




        mIv.setImageResource(drawImg);
    }


    class IvLoadAsyctask extends AsyncTask<Integer,  Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress();
            showProgress();
        }

        @Override
        protected Bitmap doInBackground(Integer... integers) {
            if (integers.length > 0) {
                BitmapDrawable drawable = (BitmapDrawable) getResources().getDrawable(integers[0]);
                Bitmap bitmap = drawable.getBitmap();
                return bitmap;
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            if (bitmap != null)
                mIv.setImageBitmap(bitmap);


            hideProgress();
//            hideProgress();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // 이전 플래그먼트에서 데이터 받기
        Bundle bundle = BaseFragmentMedi.getBackData();
        String backString = bundle.getString(SAMPLE_BACK_DATA);
        Logger.i("", "backString=" + backString);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
