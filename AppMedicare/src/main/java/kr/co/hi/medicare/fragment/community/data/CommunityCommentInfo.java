package kr.co.hi.medicare.fragment.community.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CommunityCommentInfo {


    @Expose
    @SerializedName("ADDR_MASS")
    public List<CommunityCommentData> ADDR_MASS = new ArrayList<>();
    @Expose
    @SerializedName("AST_LENGTH")
    public String AST_LENGTH;
    @Expose
    @SerializedName("DOCNO")
    public String DOCNO;

}
