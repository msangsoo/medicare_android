package kr.co.hi.medicare.bean;

import java.util.ArrayList;

/**
 * 암커뮤니티 데이터.
 */
//// @JsonIgnoreProperties(ignoreUnknown = true)
public class MealInfo {

    private String AST_LENGTH;
    // 리스트.
    private ArrayList<MealActivity_List> ADDR_MASS;


    // // @JsonProperty(value = "DATA_LENGTH")
    public String getAST_LENGTH() {
        return AST_LENGTH;
    }

    public void setAST_LENGTH(String AST_LENGTH) {
        this.AST_LENGTH = AST_LENGTH;
    }

    // // @JsonProperty(value = "DATA")
    public ArrayList<MealActivity_List> getADDR_MASS() {
        return ADDR_MASS;
    }

    public void setADDR_MASS(ArrayList<MealActivity_List> ADDR_MASS) {
        this.ADDR_MASS = ADDR_MASS;
    }
}
