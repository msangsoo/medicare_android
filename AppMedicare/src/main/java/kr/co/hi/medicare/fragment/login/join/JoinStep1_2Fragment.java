package kr.co.hi.medicare.fragment.login.join;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.co.hi.medicare.net.hwNet.ApiData;

import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.LoginActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.common.DummyWebviewFragment;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.data.Tr_mber_find_yn;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.net.hwdata.Tr_mber_user_call;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 * Created by MrsWin on 2017-02-16.
 * 1. 번호입력되면 인증요청버튼이 활성화
 * 2. 번호를 보내면 인증요청버튼 비활성
 * 3. 인증번호 입력가능
 * 4. 인증번호 6자리 이상입력하면 인증하기 버튼활성화
 * 5. 인증하기 버튼 누르면 재요청버튼 활성화
 * 6. 인증하기 버튼을 눌러서 인증되면.. 재요청버튼이 인증완료로 활성화
 *
 * 테스트 계정
 * 메디1 810101 1 01600010001 Y Y
 * 메디2 810102 1 01600010002 Y Y
 * 메디3 810103 1 01600010003 Y Y
 */

public class JoinStep1_2Fragment extends BaseFragmentMedi implements TextView.OnEditorActionListener {//implements NewActivity.onKeyBackPressedListener {
    private final String TAG = JoinStep1_2Fragment.class.getSimpleName();

    public static String JOIN_DATA = "join_data";   // 회원가입시에 사용할 데이터들
    public static String BUNDLE_KEY_IS_MEMBER_GRAD = "bundle_key_is_member_grad"; // 정회원(10), 준회원(20) 가입 여부

    private String mMemberGrad = "10";    // 정회원(10), 준회원(20)
    protected boolean mIsRealMember = true;    // 정회원(10), 준회원(20)

    private LinearLayout join_step1_agreegroup;
    private CheckBox mStep1Cb;
    private CheckBox mStep2Cb;
    private CheckBox mStep3Cb;
    private CheckBox mStep4Cb;
    public EditText mNameEt;
    public EditText mBirthEt;
    public EditText mPhoneNumEt;


    private RelativeLayout relativeUserEmailGroup;
    private TextView tvUserEmail;
    private TextView tvTitle;

    private View mCheckedEditText;
    public JoinDataVo dataVo;
    private Button next_button;
    public RadioGroup mSexRg;
    public RadioButton mManRb;
    public RadioButton mWomanRb;

    private CheckBox mAllCheckCheckBox;
    private TextView mAllCheckTextView;

    boolean isValid = false;
    private boolean isNameCheck, isBirthCheck, isPhoneCheck;

    private boolean mIsInfoEdit = false;

    public boolean isChildPage=false;

    private InputMethodManager mImm;

    public static Fragment newInstance() {
        JoinStep1_2Fragment fragment = new JoinStep1_2Fragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(this.getClass().getSimpleName(), "onCreate()");

        super.onCreate(savedInstanceState);
        isNameCheck = false;
        isPhoneCheck = false;
        isBirthCheck = false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.join_step1_2fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mImm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);


        tvTitle = (TextView) view.findViewById(R.id.join_step1_title_tv);

        dataVo = new JoinDataVo();

        if (getArguments() != null&&!isChildPage) {
            Bundle bundle = getArguments();
            mMemberGrad = bundle.getString(BUNDLE_KEY_IS_MEMBER_GRAD);
            dataVo.setMberGrad(mMemberGrad);
            mIsRealMember = "10".equals(mMemberGrad);   // 정회원 여부
        }

        if (mIsRealMember) {
            tvTitle.setText(R.string.join_inform_step1_top_info);
        } else {
            tvTitle.setText(R.string.join_inform_step1_top_info_temp_member);
        }

        tvUserEmail = (TextView) view.findViewById(R.id.tvUserEmail);

        relativeUserEmailGroup = (RelativeLayout) view.findViewById(R.id.relativeUserEmailGroup);
        mNameEt = view.findViewById(R.id.join_step1_name_edittext);
        mBirthEt = view.findViewById(R.id.join_step1_birth_edittext);
        mPhoneNumEt = view.findViewById(R.id.join_step1_phone_num_edittext);

        mStep1Cb = (CheckBox) view.findViewById(R.id.join_step1_checkbox1);
        mStep2Cb = (CheckBox) view.findViewById(R.id.join_step1_checkbox2);
        mStep3Cb = (CheckBox) view.findViewById(R.id.join_step1_checkbox3);
        mStep4Cb = (CheckBox) view.findViewById(R.id.join_step1_checkbox4);

        join_step1_agreegroup = (LinearLayout) view.findViewById(R.id.join_step1_agreegroup);
        next_button = (Button) view.findViewById(R.id.next_button);

        mNameEt.setOnFocusChangeListener(mFocusChangeListener);
        mBirthEt.setOnFocusChangeListener(mFocusChangeListener);
        mPhoneNumEt.setOnFocusChangeListener(mFocusChangeListener);
        mPhoneNumEt.setOnEditorActionListener(this);


        mStep1Cb.setOnClickListener(mOnClickListener);
        mStep2Cb.setOnClickListener(mOnClickListener);
        mStep3Cb.setOnClickListener(mOnClickListener);
        mStep4Cb.setOnClickListener(mOnClickListener);

        mStep1Cb.setOnCheckedChangeListener(mCheckedChageListener);
        mStep2Cb.setOnCheckedChangeListener(mCheckedChageListener);
        mStep3Cb.setOnCheckedChangeListener(mCheckedChageListener);
        mStep4Cb.setOnCheckedChangeListener(mCheckedChageListener);

//        mNameEt.addTextChangedListener(mWatcher);
//        mBirthEt.addTextChangedListener(mWatcher);
//        mPhoneNumEt.addTextChangedListener(mWatcher);

        view.findViewById(R.id.join_step1_contract_textview).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.join_step1_personal_info_textview).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.join_step1_info_share_textview).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.next_button).setOnClickListener(mOnClickListener);

        dataVo.setSex("1");
        mSexRg = view.findViewById(R.id.join_step2_sex_radio_group);
        mManRb = view.findViewById(R.id.join_step2_man_radio_button);
        mWomanRb = view.findViewById(R.id.join_step2_woman_radio_button);

        mAllCheckCheckBox = view.findViewById(R.id.join_step_join1_all_checkbox);
        mAllCheckTextView = view.findViewById(R.id.join_step_all_check_textview);

        mAllCheckCheckBox.setOnClickListener(mOnClickListener);
        mAllCheckTextView.setOnClickListener(mOnClickListener);


        // 액션바 타이틀이 있으면 환경설정에서 온것으로 판단
        if (getArguments() != null) {
            String bundleTitle = getArguments().getString(CommonToolBar.TOOL_BAR_TITLE);
            if (TextUtils.isEmpty(bundleTitle) == false) {
                next_button.setEnabled(true);
                join_step1_agreegroup.setVisibility(View.GONE);
                next_button.setText(R.string.join_step3_ecomplete);

                relativeUserEmailGroup.setVisibility(View.GONE);
                tvUserEmail.setVisibility(View.VISIBLE);
                loadPersonalInfo();
            }
        }

        // 마케팅 활용 동의 (준회원일때만보여주기)
        view.findViewById(R.id.join_step1_checkbox4_layout).setVisibility(mIsRealMember == false ? View.VISIBLE : View.GONE);
        mStep4Cb.setVisibility(mIsRealMember == false ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.join_step1_checkbox4_layout).setOnClickListener(mOnClickListener);

    }


//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        super.loadActionbar(actionBar);
//        actionBar.setActionBarTitle(getString(R.string.join_inform_step1_2));       // 액션바 타이틀
//    }
    @Override
    public void onResume() {
        Log.d(this.getClass().getSimpleName(), "onResume()");
        super.onResume();

        BtnEnableCheck();
    }

    private boolean validCheck() {
        boolean isValid = true;
        if (validNameCheck(true) == false) {
            isValid = false;
        }

        if (validBirthCheck(true) == false) {
            isValid = false;
        }

        if (validPhoneNumCheck(true) == false) {
            isValid = false;
        }

        BtnEnableCheck();
        return isValid;
    }

    View.OnFocusChangeListener mFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            mCheckedEditText = v;
            if (v == mNameEt && hasFocus == false) {
                validNameCheck(true);
            } else if (v == mBirthEt && hasFocus == false) {
                validBirthCheck(true);
            } else if (v == mPhoneNumEt && hasFocus == false) {
               validPhoneNumCheck(true);
            }
            BtnEnableCheck();
        }
    };



    CompoundButton.OnCheckedChangeListener mCheckedChageListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            BtnEnableCheck();
        }
    };

    /**
     * 전화번호 체크
     *
     * @return
     */
    private boolean validPhoneNumCheck(boolean isShowDlg) {
        final String phoneNum = mPhoneNumEt.getText().toString();
        if (phoneNum.length() == 0) {
            isPhoneCheck = false;
            return false;
        }

        dataVo.setPhoneNum(phoneNum);

        if (StringUtil.isValidPhoneNumber(phoneNum) == false) {
            if (isShowDlg)
                CDialog.showDlg(getContext(), getString(R.string.join_step1_phone_num_error))
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                mPhoneNumEt.requestFocus();
                                mPhoneNumEt.setSelection(phoneNum.length());
                                next_button.setEnabled(false);
                                showKeyboard();
                            }
                        });
            isPhoneCheck = false;
            return false;
        } else {
            isValid = true;
            isPhoneCheck = true;
        }
        BtnEnableCheck();
        return isValid;
    }

    /**
     * 이름 체크(닉네임)
     *
     * @return
     */
    private boolean validNameCheck(boolean isShowDlg) {
        final String name = mNameEt.getText().toString();

        if (name.length() == 0) {
//            mNameErrTv.setVisibility(View.GONE);
            isNameCheck = false;
            return false;
        }


        if ((name.length() < 2)) {
            if (isShowDlg)
                CDialog.showDlg(getContext(), getString(R.string.join_step1_name_len_error) )
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                mNameEt.requestFocus();
                                mNameEt.setSelection(mNameEt.length());
                                next_button.setEnabled(false);
                                showKeyboard();
                            }
                        });
            isNameCheck = false;
            return false;
        } else if (StringUtil.isSpecialWord(name) == false) {
            if (isShowDlg)
                CDialog.showDlg(getContext(), getString(R.string.error_name_valid) )
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                mNameEt.requestFocus();
                                mNameEt.setSelection(name.length());
                                next_button.setEnabled(false);
                                showKeyboard();
                            }
                        });
            isNameCheck = false;
            isValid = false;
        } else {
            isNameCheck = true;
            isValid = true;

            dataVo.setName(name);
        }

        BtnEnableCheck();
        return isValid;
    }

    TextWatcher mWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            BtnEnableCheck();
        }

        @Override
        public void afterTextChanged(Editable s) { }
    };

    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    /**
     * 생년월일 체크
     * @return
     */
    private boolean validBirthCheck(boolean isShowDlg) {
        final String birth = mBirthEt.getText().toString();
        if (birth.length() == 0) {// && CDateUtil.isValidyyyyMMdd(birth)) {
//            if (mBirthEt.hasSelection())
//                mBirthErrTv.setVisibility(View.GONE);
            return false;
        }

        if (birth.length() > 0) {
            final String today = CDateUtil.getToday_yyyy_MM_dd();
            if (CDateUtil.isValidyyyyMMdd(birth) == false) {
                if (isShowDlg)
                    CDialog.showDlg(getContext(), "생년월일 형식을 확인해주세요.")
                            .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    mBirthEt.requestFocus();
                                    mBirthEt.setSelection(birth.length());
                                    next_button.setEnabled(false);
                                    showKeyboard();
                                }
                            });
                return false;
            }


            if (birth.compareTo(today) > 0) {
                if (isShowDlg)
                    CDialog.showDlg(getContext(), "생년월일은 미래날자를 입력할 수 없습니다." )
                            .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    mBirthEt.requestFocus();
                                    mBirthEt.setSelection(birth.length());
                                    next_button.setEnabled(false);
                                    showKeyboard();
                                }
                            });
                return false;
            }






            isBirthCheck = true;
            dataVo.setBirth(birth);
            BtnEnableCheck();
            return true;
        } else {
            next_button.setEnabled(false);
            dataVo.setBirth("");
            return false;
        }
    }

    /**
     * 약관동의 체크 박스
     *
     * @return
     */
    private boolean validContractCheck() {
        if (!mIsInfoEdit) {
            if (mStep1Cb.isChecked() == false || mStep2Cb.isChecked() == false || mStep3Cb.isChecked() == false) {
                return false;
            } else {
            }
            return true;
        } else {
            return true;
        }
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();

            Bundle bundle = new Bundle();
            String url = "";
            switch (vId) {
                case R.id.next_button :
                if (validCheck()) {
                    isRegistMember();
                }
                break;
            case R.id.join_step_join1_all_checkbox :
                boolean isCheck = mAllCheckCheckBox.isChecked();
                mStep1Cb.setChecked(isCheck);
                mStep2Cb.setChecked(isCheck);
                mStep3Cb.setChecked(isCheck);
                mStep4Cb.setChecked(isCheck);
                break;
            case R.id.join_step_all_check_textview :
                isCheck = !mAllCheckCheckBox.isChecked();
                mAllCheckCheckBox.setChecked(isCheck);
                mStep1Cb.setChecked(isCheck);
                mStep2Cb.setChecked(isCheck);
                mStep3Cb.setChecked(isCheck);
                mStep4Cb.setChecked(isCheck);
                break;
            case R.id.join_step1_checkbox1 :
            case R.id.join_step1_checkbox2 :
            case R.id.join_step1_checkbox3 :
            case R.id.join_step1_checkbox4 :
                boolean isMarketing = true;
                if (mStep4Cb.getVisibility() == View.VISIBLE) {
                    // 준회원일경우 마케팅활용동의 체크박스 있을 경우
                    mAllCheckCheckBox.setChecked(mStep1Cb.isChecked() && mStep2Cb.isChecked() && mStep3Cb.isChecked() && mStep4Cb.isChecked());
                } else {
                    mAllCheckCheckBox.setChecked(mStep1Cb.isChecked() && mStep2Cb.isChecked() && mStep3Cb.isChecked());
                }

                break;
            case R.id.join_step1_contract_textview :
                bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract1_title_1));
//                url = mIsRealMember ? getString(R.string.personal_terms_1_url) : getString(R.string.personal_terms_jun_1_url);
                url = getString(R.string.personal_terms_1_url);
                bundle.putString(DummyWebviewFragment.URL, url);
                NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                break;
            case R.id.join_step1_personal_info_textview :
                bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract2_title_1));
//                url = mIsRealMember ? getString(R.string.personal_terms_2_url) : getString(R.string.personal_terms_jun_2_url);
                url = getString(R.string.personal_terms_2_url);
                bundle.putString(DummyWebviewFragment.URL, url);
                NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                break;
            case R.id.join_step1_info_share_textview:
                bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract3_title_1));
//                url = mIsRealMember ? getString(R.string.personal_terms_3_url) : getString(R.string.personal_terms_jun_3_url);
                url = getString(R.string.personal_terms_3_url);
                bundle.putString(DummyWebviewFragment.URL, url);
                NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                break;
            case R.id.join_step1_checkbox4_layout:
                bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract4_title_1));
                url = getString(R.string.marketing_terms_jun_url);
                bundle.putString(DummyWebviewFragment.URL, url);
                NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                break;
            }


            if (mCheckedEditText != null) {
                mCheckedEditText.clearFocus();
            }
        }
    };


    /**
     * 개인정보 불러오기
     */
    private void loadPersonalInfo() {
//        mIdEditText.setEnabled(false);

        Tr_mber_user_call.RequestData requestData = new Tr_mber_user_call.RequestData();
        Tr_login info = UserInfo.getLoginInfo();
        requestData.mber_sn = info.mber_sn;
        getData(getContext(), Tr_mber_user_call.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_mber_user_call) {
                    Tr_mber_user_call data = (Tr_mber_user_call)obj;

                    Tr_login login = UserInfo.getLoginInfo();
                    tvUserEmail.setText(data.mber_id);
                    mNameEt.setText(data.mber_nm);
                    mPhoneNumEt.setText(data.mber_hp);

                    dataVo.setCity(data.mber_zone);
                }
            }
        });
    }

    /**
     * 정회원 가입 가능 여부 판별
     */
    public void isRegistMember() {
        dataVo.setSex(mManRb.isChecked() ? "1" : "2");

        final String mberNm = mNameEt.getText().toString();
        Tr_mber_find_yn.RequestData requestData = new Tr_mber_find_yn.RequestData();
        requestData.mber_nm = mberNm;
        requestData.mber_lifyea = mBirthEt.getText().toString();
        requestData.mber_hp = mPhoneNumEt.getText().toString();
        requestData.mber_sex = dataVo.getSex();
        requestData.mber_sn = "";

        if (mIsRealMember) {
            // 정회원 가입인 경우
            getData(getContext(), Tr_mber_find_yn.class, requestData, new ApiData.IStep() {
                @Override
                public void next(Object obj) {
                    if (obj instanceof Tr_mber_find_yn) {
                        Tr_mber_find_yn data = (Tr_mber_find_yn) obj;
                        // data_yn : Y:가입가능 / N: 가입불가 / YN:정회원 가입된 아이디 / YY:웹사이트에 아이디가 존재함
                        if ("Y".equals(data.data_yn) || "YY".equals(data.data_yn)) {
                            dataVo.setmber_no(data.mber_no);
                            moveTo2Step();
                        } else if("YN".equals(data.data_yn)) {
                            CDialog.showDlg(getContext(), getString(R.string.already_regist_member));
                        } else{
                            CDialog.showDlg(getContext(), getString(R.string.already_no_service_member));
                        }
                    }
                }
            });
        } else {
            // 준회원 가입인 경우 검증 없이 2/2 로 이동
            if (BuildConfig.DEBUG)
                Toast.makeText(getContext(), "디버그메시지:준회원가입", Toast.LENGTH_SHORT).show();
            moveTo2Step();
        }
    }

    /**
     * Btn enable true / false
     */
    private void BtnEnableCheck(){
        // 약관동의여부
        boolean isContacts = mStep1Cb.isChecked() && mStep2Cb.isChecked() && mStep3Cb.isChecked();
        Log.i(TAG,
                "isNameCheck="+isNameCheck
                +", isBirthCheck="+ isBirthCheck
                +", isPhoneCheck="+isPhoneCheck
                +", isContacts="+isContacts);

        next_button.setEnabled(isNameCheck
                                && isBirthCheck
                                && isPhoneCheck
                                && isContacts);
    }


    /**
     * 가입스텝 2/2 로 이동
     */
    private void moveTo2Step() {
        final String mberNm = mNameEt.getText().toString();
        final String birth = mBirthEt.getText().toString();
        final String phoneNo = mPhoneNumEt.getText().toString();

        dataVo.setName(mberNm);
        dataVo.setBirth(birth);
        dataVo.setPhoneNum(phoneNo);
        dataVo.setSex(mManRb.isChecked() ? "1" : "2");
        if ("20".equals(mMemberGrad))   //준회원일 경우
            dataVo.setMarketingYn(mStep4Cb.isChecked() ? "Y" : "N");

        Bundle bundle = new Bundle();
        bundle.putString(JoinStep2_2FragmentMedi.BUNDLE_KEY_MBER_NM, mberNm);
        bundle.putBinder(JoinStep1_2Fragment.JOIN_DATA, dataVo);
        bundle.putString(JoinStep1_2Fragment.BUNDLE_KEY_IS_MEMBER_GRAD, mMemberGrad);
        replaceFragment(JoinStep2_2FragmentMedi.newInstance(), bundle);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(isChildPage)
            return;

        Intent intent = new Intent(getActivity(), LoginActivityMedicare.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onPause() {
        super.onPause();

        mImm.hideSoftInputFromWindow(mNameEt.getWindowToken(), 0);
        mImm.hideSoftInputFromWindow(mBirthEt.getWindowToken(), 0);
        mImm.hideSoftInputFromWindow(mPhoneNumEt.getWindowToken(), 0);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if(v.getId()==R.id.join_step1_phone_num_edittext && actionId== EditorInfo.IME_ACTION_DONE ){ // 뷰의 id를 식별, 키보드의 완료 키 입력 검출

            validPhoneNumCheck(true);

        }

        return false;
    }
}
