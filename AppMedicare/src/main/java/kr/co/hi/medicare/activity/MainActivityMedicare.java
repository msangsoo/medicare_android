package kr.co.hi.medicare.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.community.CommunityMainFragment;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.community.dialog.DialogCommon;
import kr.co.hi.medicare.fragment.health.HealthFragment;
import kr.co.hi.medicare.fragment.home.HomeFragmentMedicare;
import kr.co.hi.medicare.fragment.medicareService.MedicareServiceFragment;
import kr.co.hi.medicare.fragment.mypage.MyPageMainFragment;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.push.NotiDummyActivity;
import kr.co.hi.medicare.util.AlramUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.SharedPref;

/**
 *
 **/
public class MainActivityMedicare extends BaseActivityMedicare {

    public static final String INTENT_KEY_MOVE_MENU = "intent_key_move_menu"; // 이동할 메뉴
    public static final String INTENT_KEY_MOVE_MENU_PUSH = "intent_key_move_menu_push"; // 푸시 이동할 메뉴
    public static final int HOME_MENU_1 = 0;
    public static final int HOME_MENU_2 = 1;
    public static final int HOME_MENU_3 = 2;
    public static final int HOME_MENU_4 = 3;
    public static final int HOME_MENU_5 = 4;


    private RadioGroup mBottomMenuRadioGroup;

    private LinearLayout[] mMainLayouts;
    private RadioButton[] mBottomBtns;
    private View[] mBottomLines;
//    private Toolbar toolbar1,toolbar2;
    private View mBottomMenuLayout;

    private int TAB_PRE = R.id.main_bottom_menu1;
    private boolean isStopMoveTab=false;

    private LinearLayout[] mConentsLayouts;
    private final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 101;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        registerAlarm();    // 8시 알람서비스 등록

        mBottomMenuLayout = findViewById(R.id.bottom_menu_layout);

        mConentsLayouts = new LinearLayout[]{
                findViewById(R.id.main_fragment_1)
                , findViewById(R.id.main_fragment_2)
                , findViewById(R.id.main_fragment_3)
                , findViewById(R.id.main_fragment_4)
                , findViewById(R.id.main_fragment_5)
        };

        mMainLayouts = new LinearLayout[] {
                findViewById(R.id.main_fragment_1),
                findViewById(R.id.main_fragment_2),
                findViewById(R.id.main_fragment_3),
                findViewById(R.id.main_fragment_4),
                findViewById(R.id.main_fragment_5),
        };
        // 하단 메뉴 버튼
        mBottomBtns = new RadioButton[] {
                findViewById(R.id.main_bottom_menu1),
                findViewById(R.id.main_bottom_menu2),
                findViewById(R.id.main_bottom_menu3),
                findViewById(R.id.main_bottom_menu4),
                findViewById(R.id.main_bottom_menu5),
        };
        // 하단 메뉴 라인
        mBottomLines = new View[] {
                findViewById(R.id.main_bottom_menu_line1),
                findViewById(R.id.main_bottom_menu_line2),
                findViewById(R.id.main_bottom_menu_line3),
                findViewById(R.id.main_bottom_menu_line4),
                findViewById(R.id.main_bottom_menu_line5),
        };
        mFragments = new Fragment[mMainLayouts.length];
        mBottomMenuRadioGroup = findViewById(R.id.main_bottom_menu_radiogroup);
        mBottomMenuRadioGroup.setOnCheckedChangeListener(mCheckedChangeListener);
        ((RadioButton)findViewById(R.id.main_bottom_menu1)).setChecked(true);
//        replaceFragment(mMainFragments[0], true, false, null);


        //click 저장
        OnClickListener ClickListener = new OnClickListener(mOnClickListener, mBottomMenuLayout, MainActivityMedicare.this);


        //홈
        mBottomMenuLayout.findViewById(R.id.main_bottom_menu1).setOnTouchListener(ClickListener);
        mBottomMenuLayout.findViewById(R.id.main_bottom_menu2).setOnTouchListener(ClickListener);
        mBottomMenuLayout.findViewById(R.id.main_bottom_menu3).setOnTouchListener(ClickListener);
        mBottomMenuLayout.findViewById(R.id.main_bottom_menu4).setOnTouchListener(ClickListener);
        mBottomMenuLayout.findViewById(R.id.main_bottom_menu5).setOnTouchListener(ClickListener);

        //코드부여
        mBottomMenuLayout.findViewById(R.id.main_bottom_menu1).setContentDescription(getString(R.string.main_bottom_menu1));
        mBottomMenuLayout.findViewById(R.id.main_bottom_menu2).setContentDescription(getString(R.string.main_bottom_menu2));
        mBottomMenuLayout.findViewById(R.id.main_bottom_menu3).setContentDescription(getString(R.string.main_bottom_menu3));
        mBottomMenuLayout.findViewById(R.id.main_bottom_menu4).setContentDescription(getString(R.string.main_bottom_menu4));
        mBottomMenuLayout.findViewById(R.id.main_bottom_menu5).setContentDescription(getString(R.string.main_bottom_menu5));


    }


    RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            setFragment(checkedId);
        }
    };

    protected void setFragment(int checkedId) {
        Tr_login user = UserInfo.getLoginInfo();

        if(isStopMoveTab) {
            isStopMoveTab=false;
            return;
        }

        switch (checkedId) {
            case R.id.main_bottom_menu1:
                selectFragment(HOME_MENU_1);
                break;
            case R.id.main_bottom_menu2:
                selectFragment(HOME_MENU_2);
                break;
            case R.id.main_bottom_menu3:
                if(TextUtils.isEmpty(user.nickname)){
                    isStopMoveTab=true;
                    ((RadioButton)findViewById(TAB_PRE)).setChecked(true);
                    openDialogRegisterNick();
                    return;
                }
                selectFragment(HOME_MENU_3);
                break;
            case R.id.main_bottom_menu4:
                selectFragment(HOME_MENU_4);
                break;
            case R.id.main_bottom_menu5:
                selectFragment(HOME_MENU_5);
                break;
        }

        TAB_PRE=checkedId;
    }

    private void openDialogRegisterNick() {

        CommonFunction.openDialogRegisterNick(this, new DialogCommon.UpdateProfile() {
            @Override
            public void updateProfile(String NICK, String DISEASE_OPEN,String DISEASE_NM) {
                if(!NICK.equals("")&&!DISEASE_OPEN.equals("")) {
                    final DialogCommon dialogIntro = DialogCommon.showDialogIntro(MainActivityMedicare.this, NICK);
                    dialogIntro.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            ((RadioButton) findViewById(R.id.main_bottom_menu3)).setChecked(true);
                        }
                    });
                }else{
                    openDialogRegisterNick();
                }
            }
        });


//        Tr_login user = UserInfo.getLoginInfo();
//        DialogCommon.showDialog(
//                this,
//                "",
//                "Y",
//                CommonFunction.getDiseaseNM(user.disease_nm,user.disease_txt),
//                user.mber_sn,
//                new DialogCommon.UpdateProfile() {
//                    @Override
//                    public void updateProfile(String NICK, String DISEASE_OPEN,String DISEASE_NM) {
//                        if(!NICK.equals("")&&!DISEASE_OPEN.equals("")) {
//                            final DialogCommon dialogIntro = DialogCommon.showDialogIntro(MainActivityMedicare.this, NICK);
//                            dialogIntro.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                                @Override
//                                public void onDismiss(DialogInterface dialog) {
//                                        ((RadioButton) findViewById(R.id.main_bottom_menu3)).setChecked(true);
//                                }
//                            });
//                        }else{
//                            openDialogRegisterNick();
//                        }
//                    }
//                });
    }

    /**
     * 메뉴 보여주기
     * @param visibleIdx
     */
    private Fragment[] mFragments;
    private int mVisibleIdx = -1;
    private void selectFragment(final int visibleIdx) {
        mVisibleIdx = visibleIdx;
        mBottomBtns[visibleIdx].setChecked(true);

        Log.i(TAG, "selectFragment.visibleIdx="+visibleIdx);
        for (int i = 0; i < mMainLayouts.length; i++) {
            mMainLayouts[i].setVisibility(i == visibleIdx ? View.VISIBLE : View.GONE);
            mBottomLines[i].setVisibility(i == visibleIdx ? View.VISIBLE : View.INVISIBLE);
        }

        Fragment fragment = null;
        if (mMainLayouts[visibleIdx].getChildCount() == 0) {
            switch (visibleIdx) {
                case 0:
                    fragment = HomeFragmentMedicare.newInstance();
                    break;
                case 1:
                    fragment = HealthFragment.newInstance();
                    break;
                case 2:
                    fragment = CommunityMainFragment.newInstance();
                    break;
                case 3:
                    fragment = MedicareServiceFragment.newInstance();
                    break;
                case 4:
                    fragment = MyPageMainFragment.newInstance();
                    break;
            }

            if (fragment != null) {
                mFragments[visibleIdx] = fragment;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        replaceFragment(visibleIdx, mFragments[visibleIdx], true, false, null);
                    }
                }, 100);
            } else {
                Log.e(TAG, "Fragment is Null");
                mCurrentFragment = mFragments[visibleIdx];
                if (mCurrentFragment != null)
                    mCurrentFragment.onResume();
            }

        } else {
            mCurrentFragment = mFragments[visibleIdx];
            if (mCurrentFragment != null)
                mCurrentFragment.onResume();
        }
    }

    private Fragment mCurrentFragment;
    public void replaceFragment(int idx, final Fragment fragment, final boolean isReplace, boolean isAnim, Bundle bundle) {
        mCurrentFragment = fragment;
        for (int i = 0; i < mConentsLayouts.length; i++) {
            mConentsLayouts[i].setVisibility(i == idx ? View.VISIBLE : View.GONE);
        }

        if (!fragment.isAdded()) {
//            transaction.replace(R.id.main_content_layout, fragment, tag);
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            if (isAnim)
                transaction.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_pop_enter, R.anim.fragment_pop_exit);

            if (bundle != null)
                fragment.setArguments(bundle);

            String tag = fragment != null ? fragment.getClass().getSimpleName() : "tag";
            transaction.replace(mConentsLayouts[idx].getId(), fragment, tag);
            if (!isFinishing() && !isDestroyed()) {
//            if (isReplace == false)
                transaction.addToBackStack(null);

                try {
                    transaction.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                    transaction.commitAllowingStateLoss();
                }
            } else {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                    if (isReplace == false)
                        transaction.addToBackStack(null);
                        transaction.commitAllowingStateLoss();
                    }
                }, 100);
            }
            printFragmentLog();
        } else {

//            if (fragment != null)
//                fragment.onResume();
        }
    }

    private void printFragmentLog() {
        if (getSupportFragmentManager().getFragments() != null) {
            Logger.i(TAG, "replaceFragment.size=" + getSupportFragmentManager().getFragments().size());

            for (Fragment fg : getSupportFragmentManager().getFragments()) {
                if (fg != null)
                    Logger.i(TAG, "replaceFragment.name=" + fg.toString());
            }
        }
    }

    public void removeAllFragment() {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStackImmediate();
    }


    public Fragment getVisibleFragment() {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment != null && fragment.isVisible()) {
                return fragment;
            }
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        Log.i(TAG, "onBackPressed.mIsLandscape="+mIsLandscape);
        if (mIsLandscape) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            if (mCurrentFragment instanceof MedicareServiceFragment) {
                ((MedicareServiceFragment) mCurrentFragment).onBackPressed();
            } else {
                finishStep();
            }
        }
    }


    //클릭 이벤트 정의
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int viewId = v.getId();
            Bundle bundle = new Bundle();
            switch (viewId){
                //탭버튼
//                case R.id.common_toolbar_back_btn:
//                    NewActivity.startActivity(MainActivityMedicare.this, HealthMessageFragment.class,null);
//                    break;
//                case R.id.common_toolbar_right_btn:
//                    NewActivity.startActivity(MainActivityMedicare.this, QuestionnaireFragment.class,null);
//                    break;
//                case R.id.close_btn:
////                    SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MESSAGE, false);
////                    message_lv.setVisibility(View.GONE);
//                    break;
//                case R.id.main_title_alimi:
////                    NewActivity.startActivity(MainActivityMedicare.this, AlimiMainFragment.class,null);
//                    NewActivity.startActivity(MainActivityMedicare.this, NotifierFragment.class,null);
//                    break;
//                case R.id.new_message:
//                    NewActivity.startActivity(MainActivityMedicare.this, HealthMessageFragment.class,null);
//                    break;
//                case R.id.main_toolbar_premium:
//                    NewActivity.startActivity(MainActivityMedicare.this, PremiumMainFragment.class, null);
//                    break;
            }
        }
    };

    /**
     * 10시 알람 설정 하기
     */
    public void registerAlarm() {
        AlramUtil.setAlarmDailyMission(this);
//        Intent intent = new Intent(this, GCAlarmService.class);
//        PendingIntent sender = PendingIntent.getBroadcast(this, GCAlarmService.ALRAM_CODE_DAILY_MISSION, intent, 0);
//
//        if (sender != null) {
////            am.cancel(sender);
////            sender.cancel()
//            // TODO: 이미 설정된 알람이 없는 경우
//
//            Calendar calendar = (Calendar) Calendar.getInstance().clone();
////            if (calendar.get(Calendar.HOUR_OF_DAY) > 10) {
////                // 내일 아침 10시 00분에 처음 시작해서, 24시간 마다 실행되게, 10시 이전 세팅
////                calendar.add(Calendar.DATE, 1);
////            }
////            calendar.set(Calendar.HOUR_OF_DAY, 10);
////            calendar.set(Calendar.MINUTE, 0);
////            calendar.set(Calendar.SECOND, 0);
//
//            calendar.add(Calendar.MINUTE, 2);
//            calendar.set(Calendar.SECOND, 0);
////            Date tomorrow = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2012-02-25 08:10:00");
//            AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
//            am.setInexactRepeating(AlarmManager.RTC, calendar.getTimeInMillis(), 24 * 60 * 60 * 1000, sender);
//
//            DateFormat dateFormat = getDateTimeInstance();
//            Log.i(TAG, "registerAlarm::"+dateFormat.format(new Date(calendar.getTimeInMillis())));
//        } else {
//            // TODO: 이미 설정된 알람이 있는 경우
//            Log.e(TAG, "registerAlarm 이미 설정 된 알람 있음");
//        }
    }

    public int getCurrentFragment() {
        return mVisibleIdx;
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPref.getInstance().initContext(this);



        pushmove();
    }

    /**
     * 가로모드 관련 처리
     */
    private boolean mIsLandscape = false;
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        mIsLandscape = Configuration.ORIENTATION_LANDSCAPE == newConfig.orientation;
        int visible = mIsLandscape ? View.GONE : View.VISIBLE;
        Log.i(TAG, "onConfigurationChanged.isPortVisible="+visible);
        mBottomMenuLayout.setVisibility(mIsLandscape ? View.GONE : View.VISIBLE);
    }

    /**
     * 푸시메시지로 온 메뉴 이동 처리
     */
    public void pushmove(){
        String move_idx = SharedPref.getInstance().getPreferences(SharedPref.PUSH_MOVE_INDEX);
        String hist_sn  = SharedPref.getInstance().getPreferences(SharedPref.PUSH_MOVE_INDEX_HIST);
        Bundle bundle = new Bundle();
//        bundle.putString("BACKTITLE","홈");
        if(TextUtils.isEmpty(move_idx) == false){

            NotiDummyActivity.pushmove(this, move_idx,hist_sn);
        }
        SharedPref.getInstance().savePreferences(SharedPref.PUSH_MOVE_INDEX,"");
        SharedPref.getInstance().savePreferences(SharedPref.PUSH_MOVE_INDEX_HIST,"");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult.resultCode="+resultCode+", data="+data);
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                final int menuIdx = data.getIntExtra(INTENT_KEY_MOVE_MENU, -1);
                if (menuIdx != -1) {

                    switch (menuIdx){
                        case HOME_MENU_3:
                            if(TextUtils.isEmpty(UserInfo.getLoginInfo().nickname)){
                                openDialogRegisterNick();
                            }else{
                                selectFragment(menuIdx);
                            }
                            break;
                        default:
                            selectFragment(menuIdx);
                            break;
                    }


                } else {
                    super.onActivityResult(requestCode, resultCode, data);
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if(intent != null){
            final int menuIdx = intent.getIntExtra(INTENT_KEY_MOVE_MENU_PUSH,-1);
            if(menuIdx != -1){
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        selectFragment(menuIdx);
                    }
                }, 500);

            }
        }
    }
}

