package kr.co.hi.medicare.fragment.community.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommunityPostsData {
    @Expose
    @SerializedName("RNUM")
    public String RNUM;
    @Expose
    @SerializedName("RCNT")
    public String RCNT;
    @Expose
    @SerializedName("MYHEART")
    public String MYHEART;
    @Expose
    @SerializedName("HCNT")
    public String HCNT;
    @Expose
    @SerializedName("CM_TAG")
    public List<String> CM_TAG;
    @Expose
    @SerializedName("REGDATE")
    public String REGDATE;
    @Expose
    @SerializedName("CM_IMG3")
    public String CM_IMG3;
    @Expose
    @SerializedName("CM_IMG2")
    public String CM_IMG2;
    @Expose
    @SerializedName("CM_IMG1")
    public String CM_IMG1;
    @Expose
    @SerializedName("CM_CONTENT")
    public String CM_CONTENT;
    @Expose
    @SerializedName("CM_TITLE")
    public String CM_TITLE;
    @Expose
    @SerializedName("PROFILE_PIC")
    public String PROFILE_PIC;
    @Expose
    @SerializedName("NICK")
    public String NICK;
    @Expose
    @SerializedName("CM_SEQ")
    public String CM_SEQ;
    @Expose
    @SerializedName("TPAGE")
    public String TPAGE;
}
