package kr.co.hi.medicare.fragment.premium;

import kr.co.hi.medicare.activity.BaseActivityMedicare;

/**
 * Created by jihoon on 2016-06-13.
 * 식사갤러리 식사 등록
 * @since 0, 1
 */
public class MealWriteActivity extends BaseActivityMedicare {//implements  DatePickerDialog.OnDateSetListener{

//    public static final String MC_IMG1  =   "MC_IMG1";
//    public static final String MC_IMG2  =   "MC_IMG2";
//    public static final String MC_IMG3  =   "MC_IMG3";
//    public static final String MC_IMG4  =   "MC_IMG4";
//    public static final String MC_IMG5  =   "MC_IMG5";
//    public static final String UPFILE1  =   "UPFILE1";
//    public static final String UPFILE2  =   "UPFILE2";
//    public static final String UPFILE3  =   "UPFILE3";
//    public static final String UPFILE4  =   "UPFILE4";
//    public static final String UPFILE5  =   "UPFILE5";
//
//
////    public static final String VIEWSTATE   =   "/wEPDwULLTEwODU1MTkzMTVkGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYBBQZpbWdCdG4bvDOLSJ+aiKn5gbGyGo/deGh6E0BRDQhrkAYawCUz0w==";
////    public static final String EVENTVALIDATION= "/wEWBwKY4afNCwKCpKjbBQLhstSiCgLHmZm4AQL12LO1BgKgy//VBgKwyf20AgJlZeJ5vSoHUkFm9UEiDHyeHgobAjXGGajbltQvwYmu";
////    public static final String UPLOAD_URL   =   "http://www.walkie.co.kr/UPLOAD/HS_HL_M/picture_upload.aspx";
//
//    public String VIEWSTATE   =   "";
//    public String EVENTVALIDATION= "";
//    public static final String UPLOAD_URL   =   "https://wkd.walkie.co.kr:443/HS_HL/UPLOAD/HS_HL_M/picture_upload.aspx";
//
//    public static final int     MEAL_LENGTH =   4;
//
//    private ArrayList<MealItem> mItem = new ArrayList<MealItem>();
//
//    private UserInfo user;
//    private String A_SEQ = null;
//    private Button mRegistration_Btn;
//
//    private String mDelFile = "";
//    private int mPhotoPosition = 0; // 사진 앨범 or 촬영 후 array 에 저장할 위치
//
//
//    private SimpleDateFormat title_Format, data_Format;
//    private Calendar cal = Calendar.getInstance();
//    private String mCurrentDate = "";
//    private boolean mIsMemoChange, mIsPhotoChange = false;
//
//    // 등록날짜
//    private TextView mDateTv;
//
//    // 식사 시간
//    private TextView mMorningTv, mLunchTv, mDinnerTv, mSnackTv;
//
//    // 식사메모
//    private EditText mUpload_Content_Edit;
//
//    private FileManager mFileManager;
//    private Uri mImageCaptureUri;
//    private final static String TEMP_FILE_NAME	= "tmp.jpg";
//    private ArrayList<PhotoItem> mUploadPhotoList; // api 에 넘겨줄 사진 데이터
////    public static ArrayList<Bitmap> mBitmapList;  // 삭제 후 이미지뷰 처리에 필요한 bitmap 저장객체
//
//    public static final int     PHOTO_LIMIT_SIZE    =   5;
//    private ImageView[] mPhotoThum = new ImageView[PHOTO_LIMIT_SIZE];
//    private ImageView[] mPhoto = new ImageView[PHOTO_LIMIT_SIZE];
//    private ImageView[] mPhotoDel =   new ImageView[PHOTO_LIMIT_SIZE];
//
//    private DisplayMetrics metrics;
//    private float mScale = 0;
//    private int mPhotoHeight = 0;
//
//    // intent data
//    private boolean mIsEdit = false;
//    private String mMealWhen = "B";
//    private int mMealIndex = 0;
//
//    private Intent intent =null;
//
//    private boolean mWriteFlag = false;
//
//    private RelativeLayout mProgressLay;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout._activity_meal_write);
//        init();
//        setEvent();
//
//        title_Format = new SimpleDateFormat("yyyy년 MM월 dd일 (E)");
//        data_Format = new SimpleDateFormat("yyyyMMdd");
//
//        intent = getIntent();
//        if(intent != null){
//
//            mIsEdit     =   intent.getBooleanExtra(EXTRA_EDIT, false);
//
//            if(mIsEdit){    // 수정모드라면
//
//                mMealWhen    =   intent.getStringExtra(EXTRA_MEAL_WHEN);
//                mCurrentDate=   intent.getStringExtra(EXTRA_DATE);
//
//                switch (mMealWhen){ // 현재 선택중인 식사타입
//                    case "B":
//                        mMealIndex = 0;
//                        break;
//                    case "L":
//                        mMealIndex = 1;
//                        break;
//                    case "D":
//                        mMealIndex = 2;
//                        break;
//                    case "S":
//                        mMealIndex = 3;
//                        break;
//                }
//
////                Date date = Util.getDateFormat(mCurrentDate, "yyyy년 MM월 dd일 (E)");
////                mDateTv.setText(title_Format.format(date.getTime()));
//                String date = Util.getDateSpecialCharacter(MealWriteActivity.this, mCurrentDate, 5);
//                mDateTv.setText(date);
//
//            }else{
//                mCurrentDate = "" +cal.get(Calendar.YEAR)+ Util.getTwoDateFormat(((cal.get(Calendar.MONTH) + 1))) + Util.getTwoDateFormat(cal.get(Calendar.DATE));
//                CLog.d("mCurrentDate = " + mCurrentDate);
//
//                mDateTv.setText(title_Format.format(cal.getTimeInMillis()));
//
////                setBtnUi(false);
//
//            }
//            setBtnUi(false);
//
//            setMealBtnUi(); // 식사 버튼 ui
//            requestMeal(mCurrentDate);
//
//        }
//
//    }
//
//
//
//
//    private void init() {
//        user = new UserInfo(this);
//
//        mFileManager	=	new FileManager(getApplicationContext(), 0);
//        mUploadPhotoList = new ArrayList<PhotoItem>();
////        mBitmapList     =   new ArrayList<Bitmap>();
//
//        mDateTv         =   (TextView)  findViewById(R.id.upload_date_tv);
//
//        mMorningTv      =   (TextView)  findViewById(R.id.morning_tv);
//        mLunchTv        =   (TextView)  findViewById(R.id.lunch_tv);
//        mDinnerTv       =   (TextView)  findViewById(R.id.dinner_tv);
//        mSnackTv        =   (TextView)  findViewById(R.id.snack_tv);
//
//        mRegistration_Btn = (Button) findViewById(R.id.registration_btn);
//        mUpload_Content_Edit = (EditText) findViewById(R.id.memo_edit);
//
//        mPhoto[0]       =   (ImageView) findViewById(R.id.profile_photo_1);
//        mPhoto[1]       =   (ImageView) findViewById(R.id.profile_photo_2);
//        mPhoto[2]       =   (ImageView) findViewById(R.id.profile_photo_3);
//        mPhoto[3]       =   (ImageView) findViewById(R.id.profile_photo_4);
//        mPhoto[4]       =   (ImageView) findViewById(R.id.profile_photo_5);
//
//        mPhotoThum[0]   =   (ImageView) findViewById(R.id.profile_photo_thum_1);
//        mPhotoThum[1]   =   (ImageView) findViewById(R.id.profile_photo_thum_2);
//        mPhotoThum[2]   =   (ImageView) findViewById(R.id.profile_photo_thum_3);
//        mPhotoThum[3]   =   (ImageView) findViewById(R.id.profile_photo_thum_4);
//        mPhotoThum[4]   =   (ImageView) findViewById(R.id.profile_photo_thum_5);
//
//
//        mPhotoDel[0]    =   (ImageView)findViewById(R.id.profile_photo_del_1);
//        mPhotoDel[1]    =   (ImageView)findViewById(R.id.profile_photo_del_2);
//        mPhotoDel[2]    =   (ImageView)findViewById(R.id.profile_photo_del_3);
//        mPhotoDel[3]    =   (ImageView)findViewById(R.id.profile_photo_del_4);
//        mPhotoDel[4]    =   (ImageView)findViewById(R.id.profile_photo_del_5);
//
//        mProgressLay    =   (RelativeLayout)findViewById(R.id.progressbar_layout);
//
//        mProgressLay.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                return;
//            }
//        });
//
//        mProgressLay.setVisibility(View.INVISIBLE);
//
//
//
//        CommuninySpinner_Adapter adapter = new CommuninySpinner_Adapter(this);
//
//        metrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(metrics);
//        mScale = getResources().getDisplayMetrics().density;
//
//        mPhotoHeight    =   metrics.widthPixels;
//        CLog.i("mPhotoHeight = " + mPhotoHeight);
//
//        mPhotoHeight    =  (int) (mPhotoHeight - 4) / 3;
//        CLog.i("mPhotoHeight = " + mPhotoHeight);
//
//
//        for(int i = 0; i<PHOTO_LIMIT_SIZE; i++){
//            mPhoto[i].getLayoutParams().width = mPhotoHeight;
//            mPhoto[i].getLayoutParams().height = mPhotoHeight;
//
//        }
//
//    }
//
//    /**
//     * 이벤트 연동
//     */
//    public void setEvent(){
//        mUpload_Content_Edit.addTextChangedListener(new MyTextWatcher());
//    }
//
//    /**
//     * 사진 업로드 파라미터
//     * @param params    파라미터
//     * @param upfileArr 사진 배열
//     * @param arrIndex  사진 위치
//     * @return
//     */
//    public RequestParams setPhotoParam(RequestParams params, ArrayList<PhotoItem> upfileArr, int arrIndex){
//
//        try {
//            switch (upfileArr.get(arrIndex).getmPhotoImgStr()) {
//                case MC_IMG1:
//                    params.put(UPFILE1, new File(upfileArr.get(arrIndex).getmPhotoUrl()), MimeTypeMap.getSingleton().getMimeTypeFromExtension("jpg"));
//                    break;
//                case MC_IMG2:
//                    params.put(UPFILE2, new File(upfileArr.get(arrIndex).getmPhotoUrl()), MimeTypeMap.getSingleton().getMimeTypeFromExtension("jpg"));
//                    break;
//                case MC_IMG3:
//                    params.put(UPFILE3, new File(upfileArr.get(arrIndex).getmPhotoUrl()), MimeTypeMap.getSingleton().getMimeTypeFromExtension("jpg"));
//                    break;
//                case MC_IMG4:
//                    params.put(UPFILE4, new File(upfileArr.get(arrIndex).getmPhotoUrl()), MimeTypeMap.getSingleton().getMimeTypeFromExtension("jpg"));
//                    break;
//                case MC_IMG5:
//                    params.put(UPFILE5, new File(upfileArr.get(arrIndex).getmPhotoUrl()), MimeTypeMap.getSingleton().getMimeTypeFromExtension("jpg"));
//                    break;
//            }
//        }catch(Exception e){
//            CLog.e(e.toString());
//        }
//
//        return params;
//    }
//
//    /**
//     * 사진 갱신
//     * @param photoArr  사진 배열
//     * @param photoArrIndex 사진 배열 위치
//     * @param imgViewIndex  이미지 뷰 위치
//     */
//    public void setPhotoUi(ArrayList<PhotoItem> photoArr, int photoArrIndex, int imgViewIndex){
//        CustomImageLoader.displayImage(MealWriteActivity.this, photoArr.get(photoArrIndex).getmPhotoUrl(), mPhoto[imgViewIndex]);
//        mPhoto[imgViewIndex].setVisibility(View.VISIBLE);
//        mPhotoDel[imgViewIndex].setVisibility(View.VISIBLE);
//    }
//
//    /**
//     * 영양코칭 식사등록 api
//     * @param meal_when  식사구분 ( B - 아침, L - 점심, D - 저녁, S - 간식 )
//     * @param mc_content 글내용
//     * @param upfileArr 사진 배열 ( 1~3장 )
//     * @param del_file 삭제할 이미지 배열
//     */
//    public void requestMealSave(final String meal_when, final String mc_content, final ArrayList<PhotoItem> upfileArr, final String del_file){
//
//        if(mProgressLay != null)
//            mProgressLay.setVisibility(View.VISIBLE);
//
//
//        mUpload_Content_Edit.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                try {
//
//                    AsyncHttpClient client = new AsyncHttpClient();
//                    RequestParams params = new RequestParams();
//                    client.setTimeout(10000);
//                    client.addHeader("Accept-Charset", "UTF-8");
//                    client.addHeader("User-Agent", new WebView(getBaseContext()).getSettings().getUserAgentString());
//
//                    if (!mc_content.equals("")) { // 내용
//                        params.put("MC_CONTENT", mc_content);
//                    }
//                    if (!del_file.equals("")) {  // 삭제할 사진
//                        params.put("DEL_FILE", del_file);
//                    }
//
//                    params.put("SEQ", user.getSeq());  // 회원 고유번호
//                    params.put("MEAL_WHEN", meal_when);
//                    params.put("WDATE", mCurrentDate);
//                    params.put("__VIEWSTATE", VIEWSTATE);
//                    params.put("__EVENTVALIDATION", EVENTVALIDATION);
//                    params.put("imgBtn.x", "27");
//                    params.put("imgBtn.y", "22");
//
//                    switch (upfileArr.size()) {
//                        case 1:
//                            if (!upfileArr.get(0).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 0);
//                            }
//
//                            break;
//                        case 2:
//                            if (!upfileArr.get(0).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 0);
//                            }
//                            if (!upfileArr.get(1).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 1);
//                            }
//
//                            break;
//                        case 3:
//                            if (!upfileArr.get(0).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 0);
//                            }
//                            if (!upfileArr.get(1).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 1);
//                            }
//                            if (!upfileArr.get(2).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 2);
//                            }
//                            break;
//                        case 4:
//                            if (!upfileArr.get(0).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 0);
//                            }
//                            if (!upfileArr.get(1).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 1);
//                            }
//                            if (!upfileArr.get(2).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 2);
//                            }
//                            if (!upfileArr.get(3).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 3);
//                            }
//                            break;
//                        case 5:
//                            if (!upfileArr.get(0).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 0);
//                            }
//                            if (!upfileArr.get(1).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 1);
//                            }
//                            if (!upfileArr.get(2).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 2);
//                            }
//                            if (!upfileArr.get(3).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 3);
//                            }
//                            if (!upfileArr.get(4).getmPhotoUrl().startsWith("http")) {
//                                params = setPhotoParam(params, upfileArr, 4);
//                            }
//
//                            break;
//                        default:
//                            CLog.d("photo empty");
//                            break;
//                    }
//
//                    CLog.i("params =  " + params.toString());
//                    client.post(UPLOAD_URL, params, mAsyncHttpHandler);
//
//
//                } catch (Exception e) {
//                    CLog.e(e.toString());
//                }
//            }
//        }, 50);
//    }
//
//    /**
//     * 식사기록 보기 api
//     * @param date 식사기록 조회하려는 날짜 ( yyyyMMdd )
//     */
//    private void requestMeal(String date) {
//        if (mProgressLay != null){
//            mProgressLay.setVisibility(View.VISIBLE);
//        }
//        //서버에 값 전달.
//        JSONObject jObject = new JSONObject();
//        try {
//            jObject.put("SEQ", user.getSeq());
//            jObject.put("WDATE", date);
//            //old
////            request(EServerAPI.API_POST, RequestUtil.getJSONParameter(jObject, "DX003"));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        //new
//        try {
//            AsyncHttpClient client = new AsyncHttpClient();
//            RequestParams params = new RequestParams();
//            client.setTimeout(10000);
//            client.addHeader("Accept-Charset", "UTF-8");
//            client.addHeader("User-Agent", new WebView(getBaseContext()).getSettings().getUserAgentString());
//            jObject.put("DOCNO","DX003");
//            params.put("strJson", jObject.toString());
//            CLog.i("url = " + Defined.BASE_HTTPS_URL +"?" +params.toString());
//            client.post(Defined.BASE_HTTPS_URL, params, mAsyncHttpHandler2);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private AsyncHttpResponseHandler mAsyncHttpHandler2 = new AsyncHttpResponseHandler() {
//        @Override
//        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//            CLog.i("onSuccess");
//
//            try {
//
//                int count = headers.length;
//
//                for (int i = 0; i < count; i++) {
//                    CLog.v(headers[i].getName() + ": " + headers[i].getValue());
//                }
//
//                String response = new String(responseBody, "UTF-8");
//                CLog.i("response = " +response);
//                JSONObject resultData = null;
//
//                if(response.startsWith("<")){   // api result 문자열 시작값이 "<" 로 시작하는 경우는 xml 폼
//                    try {
//                        response =  Util.parseXml(response);
//                    }catch(Exception e){
//                        CLog.e(e.toString());
//                    }
//
//                    resultData = new JSONObject(response);
//
//                }else{
//                    resultData = new JSONObject(response);
//                }
//
//                String resultCode = resultData.getString("DOCNO");
//                switch(resultCode){
//                    case "DX003":
//
//                        int length = Integer.parseInt((String) resultData.get("AST_LENGTH"));
//                        mItem.clear();
//                        mDelFile = "";
//                        mIsMemoChange = false;
//                        mIsPhotoChange = false;
//                        setBtnUi(false);
//
//                        // 아침,점심,저녁,간식 기본 틀 저장
//                        mItem.add(new MealItem("", "B", "", "", "", "", "", "", false));
//                        mItem.add(new MealItem("", "L", "", "", "", "", "", "", false));
//                        mItem.add(new MealItem("", "D", "", "", "", "", "", "", false));
//                        mItem.add(new MealItem("", "S", "", "", "", "", "", "", false));
//
//                        if(length < 1){    // 데이터 없음
//                            UISettingThread();
//
//                        }else{   // 데이터 있음
//                            JSONObject jsonObject = new JSONObject(response);
//                            CLog.d(jsonObject.toString());
//
//                            JSONArray jsonArray = new JSONArray(jsonObject.getString("AST_MASS"));
//                            for(int i=0; i<jsonArray.length(); i++){
//                                JSONObject object = jsonArray.getJSONObject(i);
//
//                                MealItem item = new MealItem(object.getString("WDATE"),
//                                        object.getString("MEAL_WHEN"),
//                                        object.getString("MC_CONTENT"),
//                                        object.getString("MC_IMG1"),
//                                        object.getString("MC_IMG2"),
//                                        object.getString("MC_IMG3"),
//                                        object.getString("MC_IMG4"),
//                                        object.getString("MC_IMG5"),
//                                        true);
//
//                                switch(item.getmMealWhen()){    // 식사 타입에 맞는 array 에 치환
//                                    case "B":   //아침
//                                        mItem.set(0, item);
//                                        break;
//                                    case "L":   //점심
//                                        mItem.set(1, item);
//                                        break;
//                                    case "D":   //저녁
//                                        mItem.set(2, item);
//                                        break;
//                                    case "S":   //간식식
//                                        mItem.set(3, item);
//                                        break;
//                                }
//                            }
//
//                            UISettingThread();
//                        }
//                        break;
//                }
//
//
//            } catch (Exception e) {
//                CLog.e(e.toString());
//            }
//        }
//
//        @Override
//        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//            CLog.i("onFailure");
//            Handler mHandler = new Handler(Looper.getMainLooper());
//            mHandler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    UIThread(MealWriteActivity.this, getString(R.string.confirm), getString(R.string.networkexception));
//                }
//            }, 0);
//        }
//    };
//
//    private AsyncHttpResponseHandler mAsyncHttpHandler = new AsyncHttpResponseHandler() {
//
//        @Override
//        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//            CLog.i("onSuccess");
//
//            try {
//
//                int count = headers.length;
//
//                for (int i = 0; i < count; i++) {
//                    CLog.d(headers[i].getName() + ": " + headers[i].getValue());
//                }
//
//                String resultData = new String(responseBody);
//
//                int noteLastIndex = resultData.indexOf("<");
//                int noteFirstIndex = resultData.lastIndexOf("n");
//
//                int dataFirstIndex = resultData.indexOf(">");
//
//                CLog.d("statusCode = " +statusCode);
//                CLog.d("resultData = " +resultData.toString());
//
//                JSONObject object = new JSONObject(resultData);
//
//                String resultCode = object.getString("RESULT_CODE");
//                switch(resultCode){
//                    case "0000":
//                        AlertDialog.Builder alert = new AlertDialog.Builder(MealWriteActivity.this);
//                        alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                mIsPhotoChange = false;
//                                mIsMemoChange = false;
//                                setResult(RESULT_OK);
//                                mWriteFlag = true;
//                                requestMeal(mCurrentDate);
//                                dialog.dismiss();
//                            }
//                        });
//                        if (mMealIndex == 0){
//                            alert.setMessage(getString(R.string.register_save_1));
//                        }else if (mMealIndex == 1){
//                            alert.setMessage(getString(R.string.register_save_2));
//                        }else if (mMealIndex == 2){
//                            alert.setMessage(getString(R.string.register_save_3));
//                        }else {
//                            alert.setMessage(getString(R.string.register_save_4));
//                        }
//
//                        alert.show();
//                        break;
//                    case "1000":
//                        AlertDialog.Builder alert1 = new AlertDialog.Builder(MealWriteActivity.this);
//                        alert1.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                mIsPhotoChange = false;
//                                mIsMemoChange = false;
//                                setResult(RESULT_OK);
//                                mWriteFlag = true;
//                                requestMeal(mCurrentDate);
//                                dialog.dismiss();
//                            }
//                        });
//                        if (mMealIndex == 0){
//                            alert1.setMessage(getString(R.string.register_change_1));
//                        }else if (mMealIndex == 1){
//                            alert1.setMessage(getString(R.string.register_change_2));
//                        }else if (mMealIndex == 2){
//                            alert1.setMessage(getString(R.string.register_change_3));
//                        }else {
//                            alert1.setMessage(getString(R.string.register_change_4));
//                        }
//                        alert1.show();
//                        break;
//                    case "3333":
//                        UIThread(MealWriteActivity.this, getString(R.string.confirm), getString(R.string.msg_same_post));
//                        break;
//                    case "4444":
//                        UIThread(MealWriteActivity.this, getString(R.string.confirm), getString(R.string.msg_empty_community));
//                        break;
//                    case "8888":
//                        UIThread(MealWriteActivity.this, getString(R.string.confirm), getString(R.string.msg_empty_member));
//                        break;
//                    case "9999":
//                        UIThread(MealWriteActivity.this, getString(R.string.confirm), getString(R.string.msg_error));
//                        break;
//                    default:
//                        UIThread(MealWriteActivity.this, getString(R.string.confirm), getString(R.string.msg_error));
//                        break;
//                }
//
//
//            } catch (Exception e) {
//                CLog.e(e.toString());
//            }
//
//            if(mProgressLay != null)
//                mProgressLay.setVisibility(View.INVISIBLE);
//
//        }
//
//        @Override
//        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//            CLog.i("onFailure");
//            if(responseBody == null)
//                return;
//            String response = new String(responseBody);
//            CLog.i("statusCode = " +statusCode);
//            CLog.i("response = " +response);
//
//        }
//    };
//
//
//    @Override
//    public void request(EServerAPI eServerAPI, Object obj) {
//        Record record = new Record();
//        record.setEServerAPI(eServerAPI);
//        switch (eServerAPI) {
//            case API_GET:
//                record.setRequestData(obj);
//                break;
//            case API_POST:
//                record.setRequestData(obj);
//                break;
//        }
//        sendApi(record);
//    }
//
//    @Override
//    public void response(Record record) throws UnsupportedEncodingException {
//        String json = new String(record.getData());
//        json = json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1);
//        Map<String, Object> result = JsonUtil.getJSONParserData(record.getData());
//        CLog.e(json);
//        if (result.containsKey("DOCNO")) {
//            try {
//                switch ((String) result.get("DOCNO")) {
//                    case "DX003":
//
//                        int length = Integer.parseInt((String) result.get("AST_LENGTH"));
//                        mItem.clear();
//                        mDelFile = "";
//                        mIsMemoChange = false;
//                        mIsPhotoChange = false;
//                        setBtnUi(false);
//
//                        // 아침,점심,저녁,간식 기본 틀 저장
//                        mItem.add(new MealItem("", "B", "", "", "", "", "", "", false));
//                        mItem.add(new MealItem("", "L", "", "", "", "", "", "", false));
//                        mItem.add(new MealItem("", "D", "", "", "", "", "", "", false));
//                        mItem.add(new MealItem("", "S", "", "", "", "", "", "", false));
//
//                        if(length < 1){    // 데이터 없음
//                            UISettingThread();
//
//                        }else{   // 데이터 있음
//                            JSONObject jsonObject = new JSONObject(json);
//                            CLog.d(jsonObject.toString());
//
//                            JSONArray jsonArray = new JSONArray(jsonObject.getString("AST_MASS"));
//                            for(int i=0; i<jsonArray.length(); i++){
//                                JSONObject object = jsonArray.getJSONObject(i);
//
//                                MealItem item = new MealItem(object.getString("WDATE"),
//                                        object.getString("MEAL_WHEN"),
//                                        object.getString("MC_CONTENT"),
//                                        object.getString("MC_IMG1"),
//                                        object.getString("MC_IMG2"),
//                                        object.getString("MC_IMG3"),
//                                        object.getString("MC_IMG4"),
//                                        object.getString("MC_IMG5"),
//                                        true);
//
//                                switch(item.getmMealWhen()){    // 식사 타입에 맞는 array 에 치환
//                                    case "B":   //아침
//                                        mItem.set(0, item);
//                                        break;
//                                    case "L":   //점심
//                                        mItem.set(1, item);
//                                        break;
//                                    case "D":   //저녁
//                                        mItem.set(2, item);
//                                        break;
//                                    case "S":   //간식식
//                                        mItem.set(3, item);
//                                        break;
//                                }
//                            }
//
//                            UISettingThread();
//                        }
//                        break;
//                }
//            } catch (Exception e) {
//                CLog.e(e.toString());
//            }
//        }
//    }
//
//    @Override
//    public void networkException(Record record) {
//        CLog.e("ERROR STATE : " + record.stateCode);
//        UIThread(MealWriteActivity.this, getString(R.string.confirm), getString(R.string.networkexception));
//    }
//
//    /**
//     * 팝업 다이얼로그
//     * @param activity  컨택스트
//     * @param confirm   확인
//     * @param message   내용
//     */
//    private void UIThread(final Activity activity, final String confirm, final String message) {
//        Handler mHandler = new Handler(Looper.getMainLooper());
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                AlertDialogUtil.onAlertDialog(activity, confirm, message);
//            }
//        }, 0);
//    }
//
//    /**
//     * 식사 시간 이동시 수정사항 유무 확인
//     * @param meal_when 식사 타입 ( B - 아침, L - 점심, D - 저녁, S - 간식 )
//     * @param meal_index    식사 index ( 0 - 아침, 1 - 점심, 2 - 저녁, 3 - 간식 )
//     */
//    private void mealChangeDialog(final String meal_when, final int meal_index){
//
//        CLog.i("mUploadPhotoList.size() --> " + mUploadPhotoList.size());
//
//        AlertDialog.Builder alert = new AlertDialog.Builder(this);
//        alert.setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
////                mMealWhen   =   meal_when;
////                mMealIndex  =   meal_index;
////                mIsPhotoChange = false;
////                mIsMemoChange = false;
////                mDelFile = "";
////                setBtnUi(false);
////                setUiSetting();
//                dialog.dismiss();
//            }
////        }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
////            @Override
////            public void onClick(DialogInterface dialog, int which) {
////                dialog.dismiss();
////            }
//        });
//        if (mMealIndex == 0){
//            alert.setMessage(getString(R.string.msg_meal_change_delete_1));
//        }else if (mMealIndex == 1){
//            alert.setMessage(getString(R.string.msg_meal_change_delete_2));
//        }else if (mMealIndex == 2){
//            alert.setMessage(getString(R.string.msg_meal_change_delete_3));
//        }else {
//            alert.setMessage(getString(R.string.msg_meal_change_delete_4));
//        }
//        alert.show();
//    }
//
//    /**
//     * 식사등록 화면 그리기
//     */
//    private void UISettingThread() {
//        Handler mHandler = new Handler(Looper.getMainLooper());
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                setUiSetting();
//
//            }
//        }, 0);
//    }
//
//    String sViewState = "id=\"__VIEWSTATE\" value=\"";
//    String sEventValidation = "id=\"__EVENTVALIDATION\" value=\"";
//    String sEndStr = "\" />";
//
//    private void UIThreadTest() {
//        Handler mHandler = new Handler(Looper.getMainLooper());
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Thread t = new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            URL url = new URL(UPLOAD_URL);
//                            HttpURLConnection conn = (HttpURLConnection)url.openConnection();// 접속
//                            if (conn != null) {
//                                conn.setConnectTimeout(2000);
//                                conn.setUseCaches(false);
//                                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK){
////                                    Log.d("hsh", "HTTP_OK");
//                                    //    데이터 읽기
//                                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(),"euc-kr"));//"utf-8"
//                                    while(true) {
//                                        String line = br.readLine();
//                                        if (line == null) break;
//                                        if (line.contains(sViewState)){
//                                            line = line.substring(line.indexOf(sViewState)+sViewState.length(),line.indexOf(sEndStr));
//                                            VIEWSTATE = line;
////                                            Log.d("hsh", "VIEWSTATE "+VIEWSTATE);
//                                        }else if(line.contains(sEventValidation)){
//                                            line = line.substring(line.indexOf(sEventValidation)+sEventValidation.length(),line.indexOf(sEndStr));
//                                            EVENTVALIDATION = line;
////                                            Log.d("hsh", "EVENTVALIDATION "+EVENTVALIDATION);
//                                        }
//
//                                    }
//                                    br.close(); // 스트림 해제
//                                }
//                                conn.disconnect(); // 연결 끊기
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                        finally {
//                            if(!VIEWSTATE.equals("") && !EVENTVALIDATION.equals("")) {
//                                Handler mHandler = new Handler(Looper.getMainLooper());
//                                mHandler.postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        requestMealSave(mMealWhen, mUpload_Content_Edit.getText().toString(), mUploadPhotoList, mDelFile);
//                                    }
//                                }, 0);
//
//                            }else{
//                                Toast.makeText(MealWriteActivity.this, getString(R.string.networkexception), Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//                });
//                t.start(); // 쓰레드 시작
//            }
//        }, 0);
//    }
//
//    public void click_event(View v) {
//        Intent intent = null;
//        Date date = null;
//        Calendar calendar = null;
//
//        if(getButtonClickEnabled() == false)
//            return;
//
//        setButtonClickEnabled(false);
//
//        switch (v.getId()) {
//            case R.id.activity_actrecord_ImageView_back:
//                if (mWriteFlag){
//                    setResult(RESULT_OK);
//                }
//                finish();
//                break;
//            // 등록
//            case R.id.registration_btn:
//                if (mDelFile.length() > 0)
//                    mDelFile = Util.removeDuplicateStringToken(mDelFile, ",");
//                CLog.i("mDelFile = " +mDelFile);
////                requestMealSave(mMealWhen, mUpload_Content_Edit.getText().toString(), mUploadPhotoList, mDelFile);
//                UIThreadTest();
//                break;
//            case R.id.upload_date_layout:   // 날짜 변경
//                // 달력.
//                date = new Date(System.currentTimeMillis());
//                Calendar calendar2 = Calendar.getInstance();
//                calendar2.setTime(date);
//                date = new Date(cal.getTimeInMillis());
//                calendar = Calendar.getInstance();
//                calendar.setTime(date);
//                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(MealWriteActivity.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
//                datePickerDialog.setMaxDate(calendar2);
//                datePickerDialog.show(getFragmentManager(), null);
//                break;
//            case R.id.morning_tv:   // 아침
//
//                if (mUploadPhotoList.size() == 0 && mUpload_Content_Edit.length() == 0) {
//                    mIsMemoChange = false;
//                    mIsPhotoChange = false;
//                    mMealWhen = "B";
//                    mMealIndex = 0;
//                    setUiSetting();
//                }else {
//                    if (mIsMemoChange || mIsPhotoChange) {
//                        mealChangeDialog("B", 0);
//                    } else {
//                        mMealWhen = "B";
//                        mMealIndex = 0;
//                        setUiSetting();
//                    }
//                }
//
//                break;
//            case R.id.lunch_tv: // 점심
//                if (mUploadPhotoList.size() == 0 && mUpload_Content_Edit.length() == 0) {
//                    mIsMemoChange = false;
//                    mIsPhotoChange = false;
//                    mMealWhen = "L";
//                    mMealIndex = 1;
//                    setUiSetting();
//                } else {
//                    if (mIsMemoChange || mIsPhotoChange) {
//                        mealChangeDialog("L", 1);
//                    } else {
//                        mMealWhen = "L";
//                        mMealIndex = 1;
//                        setUiSetting();
//                    }
//                }
//                break;
//            case R.id.dinner_tv:    //저녁
//                if (mUploadPhotoList.size() == 0 && mUpload_Content_Edit.length() == 0) {
//                    mIsMemoChange = false;
//                    mIsPhotoChange = false;
//                    mMealWhen = "D";
//                    mMealIndex = 2;
//                    setUiSetting();
//                }else {
//                    if (mIsMemoChange || mIsPhotoChange) {
//                        mealChangeDialog("D", 2);
//                    } else {
//                        mMealWhen = "D";
//                        mMealIndex = 2;
//                        setUiSetting();
//                    }
//                }
//                break;
//            case R.id.snack_tv: //간식
//                if (mUploadPhotoList.size() == 0 && mUpload_Content_Edit.length() == 0) {
//                    mIsMemoChange = false;
//                    mIsPhotoChange = false;
//                    mMealWhen = "S";
//                    mMealIndex = 3;
//                    setUiSetting();
//                }else {
//                    if (mIsMemoChange || mIsPhotoChange) {
//                        mealChangeDialog("S", 3);
//                    } else {
//                        mMealWhen = "S";
//                        mMealIndex = 3;
//                        setUiSetting();
//                    }
//                }
//                break;
//            //사진올리기
//            case R.id.profile_add_photo:    // 사진 추가
//                if (mUploadPhotoList.size() == 5 ) {
//                    Toast.makeText(MealWriteActivity.this, getString(R.string.upload_limit_meal), Toast.LENGTH_SHORT).show();
//                } else {
//
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {   // 마시멜로 권한 예외처리
//                        if (PermissionUtils.canAccessStorage(this)) {
////                            setTheme(R.style.ActionSheetStyleiOS9);
//                            showActionSheet();
//                        } else {
//                            ActivityCompat.requestPermissions(this, PermissionUtils.STORAGE_PERMS, 2);
//                        }
//                    }else{
////                        setTheme(R.style.ActionSheetStyleiOS9);
//                        showActionSheet();
//                    }
//
//                }
//                break;
//
////            case R.id.activity_actrecord_TextView_confirm:
////
////                A_SEQ = String.valueOf(activity_actrecord_Spinner_type.getSelectedItemPosition() + 1);
////                if (A_SEQ == null) {
////                    // 종류 선택 안했을때.
////                    AlertDialogUtil.onAlertDialog(_CommunityActivity.this, getString(R.string.confirm), "종류를 선택해 주세요.");
////                } else {
////                    JSONObject jObject = new JSONObject();
////                    JSONObject jsonObject = new JSONObject();
////                    JSONArray jsonArray = new JSONArray();
////                    try {
////                        jsonObject.put("WT", WT);
////                        jsonObject.put("A_SEQ", A_SEQ);
////                        jsonObject.put("WD", WD);
////                        jsonObject.put("ET", ET);
////                        jsonArray.put(jsonObject);
////                        jObject.put("SEQ", user.getSeq());
////                        jObject.put("ACTIVE_MASS", jsonArray);
////                        jObject.put("ACTIVE_LENGTH", "1");
////                    } catch (JSONException e) {
////                        e.printStackTrace();
////                    }
////                    request(EServerAPI.API_POST, RequestUtil.getJSONParameter(jObject, "DA001"));
////                }
////                break;
//
//            case R.id.profile_photo_del_1:  // 첫번째 사진 삭제
//
//                if(mUploadPhotoList.size() > 0) {
//
//                    for (int i = 0; i < mUploadPhotoList.size(); i++) {
//                        if (mUploadPhotoList.get(i).getmPhotoImgStr().equals(MC_IMG1)) {
//                            mUploadPhotoList.remove(i);
//                            mPhoto[0].setImageDrawable(null);
//                            mPhotoDel[0].setVisibility(View.GONE);
//                            if(mDelFile.equals("")){
//                                mDelFile    = MC_IMG1;
//                            }else{
//                                mDelFile    =   mDelFile + "," +MC_IMG1;
//                            }
//
//                            break;
//                        }
//                    }
//
//                    if(mUploadPhotoList.size() < 1){
//                        setBtnUi(false);
//                    }else{
//                        setBtnUi(true);
//                    }
//
//                }
//
//                mIsPhotoChange = true;
//
//                break;
//            case R.id.profile_photo_del_2:  // 두번째 사진 삭제
//
//                if(mUploadPhotoList.size() > 0) {
//
//                    for (int i = 0; i < mUploadPhotoList.size(); i++) {
//                        if (mUploadPhotoList.get(i).getmPhotoImgStr().equals(MC_IMG2)) {
//                            mUploadPhotoList.remove(i);
//                            mPhoto[1].setImageDrawable(null);
//                            mPhotoDel[1].setVisibility(View.GONE);
//                            if (mDelFile.equals("")) {
//                                mDelFile = MC_IMG2;
//                            } else {
//                                mDelFile = mDelFile + "," + MC_IMG2;
//                            }
//
//                            break;
//                        }
//                    }
//
//                    if(mUploadPhotoList.size() < 1){
//                        setBtnUi(false);
//                    }else{
//                        setBtnUi(true);
//                    }
//
//                }
//
//                mIsPhotoChange = true;
//
//                break;
//            case R.id.profile_photo_del_3:  // 세번째 사진 삭제
//
//                if(mUploadPhotoList.size() > 0) {
//
//                    for (int i = 0; i < mUploadPhotoList.size(); i++) {
//                        if (mUploadPhotoList.get(i).getmPhotoImgStr().equals(MC_IMG3)) {
//                            mUploadPhotoList.remove(i);
//                            mPhoto[2].setImageDrawable(null);
//                            mPhotoDel[2].setVisibility(View.GONE);
//                            if (mDelFile.equals("")) {
//                                mDelFile = MC_IMG3;
//                            } else {
//                                mDelFile = mDelFile + "," + MC_IMG3;
//                            }
//
//                            break;
//                        }
//                    }
//                    if(mUploadPhotoList.size() < 1){
//                        setBtnUi(false);
//                    }else{
//                        setBtnUi(true);
//                    }
//                }
//
//                mIsPhotoChange = true;
//
//                break;
//            case R.id.profile_photo_del_4:  // 네번째 사진 삭제
//
//                if(mUploadPhotoList.size() > 0) {
//
//                    for (int i = 0; i < mUploadPhotoList.size(); i++) {
//                        if (mUploadPhotoList.get(i).getmPhotoImgStr().equals(MC_IMG4)) {
//                            mUploadPhotoList.remove(i);
//                            mPhoto[3].setImageDrawable(null);
//                            mPhotoDel[3].setVisibility(View.GONE);
//                            if (mDelFile.equals("")) {
//                                mDelFile = MC_IMG4;
//                            } else {
//                                mDelFile = mDelFile + "," + MC_IMG4;
//                            }
//
//                            break;
//                        }
//                    }
//                    if(mUploadPhotoList.size() < 1){
//                        setBtnUi(false);
//                    }else{
//                        setBtnUi(true);
//                    }
//                }
//
//                mIsPhotoChange = true;
//
//                break;
//            case R.id.profile_photo_del_5:  // 다섯번째 사진 삭제
//
//                if(mUploadPhotoList.size() > 0) {
//
//                    for (int i = 0; i < mUploadPhotoList.size(); i++) {
//                        if (mUploadPhotoList.get(i).getmPhotoImgStr().equals(MC_IMG5)) {
//                            mUploadPhotoList.remove(i);
//                            mPhoto[4].setImageDrawable(null);
//                            mPhotoDel[4].setVisibility(View.GONE);
//                            if (mDelFile.equals("")) {
//                                mDelFile = MC_IMG5;
//                            } else {
//                                mDelFile = mDelFile + "," + MC_IMG5;
//                            }
//
//                            break;
//                        }
//                    }
//                    if(mUploadPhotoList.size() < 1){
//                        setBtnUi(false);
//                    }else{
//                        setBtnUi(true);
//                    }
//                }
//
//                mIsPhotoChange = true;
//
//                break;
//        }
//
//        setButtonClickEnabled(true);
//
//    }
//
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            if (mWriteFlag){
//                setResult(RESULT_OK);
//            }
//            finish();
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//
//    }
//
//    /**
//     * 이미지 뷰 갱신
//     */
//    public void resetImageUi(){
//
//        for(int i=0; i<mPhoto.length; i++){    // 모든 이미지뷰 초기화
//            mPhoto[i].setImageDrawable(null);
//            mPhotoDel[i].setVisibility(View.GONE);
//        }
//
//        switch (mUploadPhotoList.size()){
//            case 1:
//                switch(mUploadPhotoList.get(0).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 0, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 0, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 0, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 0, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 0, 4);
//                        break;
//                }
//                break;
//            case 2:
//                switch(mUploadPhotoList.get(0).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 0, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 0, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 0, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 0, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 0, 4);
//                        break;
//                }
//
//                switch(mUploadPhotoList.get(1).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 1, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 1, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 1, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 1, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 1, 4);
//                        break;
//                }
//
//                break;
//            case 3:
//                switch(mUploadPhotoList.get(0).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 0, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 0, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 0, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 0, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 0, 4);
//                        break;
//                }
//
//                switch(mUploadPhotoList.get(1).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 1, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 1, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 1, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 1, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 1, 4);
//                        break;
//                }
//                switch(mUploadPhotoList.get(2).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 2, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 2, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 2, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 2, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 2, 4);
//                        break;
//                }
//
//                break;
//            case 4:
//                switch(mUploadPhotoList.get(0).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 0, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 0, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 0, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 0, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 0, 4);
//                        break;
//                }
//
//                switch(mUploadPhotoList.get(1).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 1, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 1, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 1, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 1, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 1, 4);
//                        break;
//                }
//                switch(mUploadPhotoList.get(2).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 2, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 2, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 2, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 2, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 2, 4);
//                        break;
//                }
//                switch(mUploadPhotoList.get(3).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 3, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 3, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 3, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 3, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 3, 4);
//                        break;
//                }
//
//                break;
//            case 5:
//                switch(mUploadPhotoList.get(0).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 0, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 0, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 0, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 0, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 0, 4);
//                        break;
//                }
//
//                switch(mUploadPhotoList.get(1).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 1, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 1, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 1, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 1, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 1, 4);
//                        break;
//                }
//                switch(mUploadPhotoList.get(2).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 2, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 2, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 2, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 2, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 2, 4);
//                        break;
//                }
//                switch(mUploadPhotoList.get(3).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 3, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 3, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 3, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 3, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 3, 4);
//                        break;
//                }
//                switch(mUploadPhotoList.get(4).getmPhotoImgStr()){
//                    case MC_IMG1:
//                        setPhotoUi(mUploadPhotoList, 4, 0);
//                        break;
//                    case MC_IMG2:
//                        setPhotoUi(mUploadPhotoList, 4, 1);
//                        break;
//                    case MC_IMG3:
//                        setPhotoUi(mUploadPhotoList, 4, 2);
//                        break;
//                    case MC_IMG4:
//                        setPhotoUi(mUploadPhotoList, 4, 3);
//                        break;
//                    case MC_IMG5:
//                        setPhotoUi(mUploadPhotoList, 4, 4);
//                        break;
//                }
//
//                break;
//        }
//
//    }
//
//    /**
//     * 사진 등록 팝업 설정
//     */
//    public void showActionSheet() {
//
//        if(mPhotoDel[0].getVisibility() == View.GONE){
//            mPhotoPosition  =   0;
//        }else if(mPhotoDel[1].getVisibility() == View.GONE){
//            mPhotoPosition  =   1;
//        }else if(mPhotoDel[2].getVisibility() == View.GONE){
//            mPhotoPosition  =   2;
//        }else if(mPhotoDel[3].getVisibility() == View.GONE){
//            mPhotoPosition  =   3;
//        }else if(mPhotoDel[4].getVisibility() == View.GONE){
//            mPhotoPosition  =   4;
//        }
//
////        ActionSheet.createBuilder(this, getSupportFragmentManager())
////                .setCancelButtonTitle(getString(R.string.cancel))
////                .setOtherButtonTitles(getString(R.string.photo_album), getString(R.string.new_capture))
////                .setCancelableOnTouchOutside(true).setListener(this).show();
//    }
//
//    /**
//     * 갤러리에서 사진 가져오기
//     */
//    private void startGallery() {
//        Intent intent = new Intent();
//        intent.setAction(Intent.ACTION_PICK);
//        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
//        startActivityForResult(intent, 1);
//    }
//
//    /**
//     * 카메라로 사진 찍기
//     */
//    private void startCamera()
//    {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//        String tempFileName = "tmp.jpg";
//
//        File file = new File(mFileManager.getFilePath(), tempFileName);
//
//        // 임시파일을 삭제한다.
//        if ( file.isFile() )
//            file.delete();
//
//        mImageCaptureUri	=	Uri.fromFile(file);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
//        //intent.putExtra("return-data", true);
//        startActivityForResult(intent, 2);
//    }
//
//    /**
//     * 임시파일 삭제
//     */
//    private void tempFileDelete() {
//
//        File file = new File(mFileManager.getFilePath(), TEMP_FILE_NAME);
//
//        // 임시파일을 삭제한다.
//        if ( file.isFile() )
//            file.delete();
//
//    }
//
//    /**
//     * URI 를 실제로 주소로 변환
//     * @param uriPath
//     * @return 파일 실제 주소
//     */
//    public String getRealImagePath (Uri uriPath)
//    {
//        String []proj = {MediaStore.Images.Media.DATA};
//        Cursor cursor = this.managedQuery(uriPath, proj, null, null, null);
//        int index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//
//        cursor.moveToFirst();
//        String path = cursor.getString(index);
//        return path;
//    }
//
//    /**
//     * 식사등록 화면 그리기
//     */
//    public void setUiSetting(){
//
//        mUploadPhotoList.clear();   // 사진 초기화
//        resetImageUi();
//
//        switch(mMealWhen){
//            case "B":   // 아침
//
//                // 사진
//                if(!mItem.get(0).getmUpfile1().equals("")) {    // 첫번째 사진이 있으면 수정모드로 판단
////                    setBtnUi(true);
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(0).getmUpfile1(), MC_IMG1));
//                }else{
////                    setBtnUi(false);
//                }
//
//                if(!mItem.get(0).getmUpfile2().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(0).getmUpfile2(), MC_IMG2));
//                }
//                if(!mItem.get(0).getmUpfile3().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(0).getmUpfile3(), MC_IMG3));
//                }
//                if(!mItem.get(0).getmUpfile4().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(0).getmUpfile4(), MC_IMG4));
//                }
//                if(!mItem.get(0).getmUpfile5().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(0).getmUpfile5(), MC_IMG5));
//                }
//
//                if(!mItem.get(0).getmMcContent().equals("")){
//                    mUpload_Content_Edit.setText(mItem.get(0).getmMcContent());
//                }else{
//                    mUpload_Content_Edit.setText("");
//                }
//
//
//                break;
//            case "L":   // 점심
//                // 사진
//                if(!mItem.get(1).getmUpfile1().equals("")) {    // 첫번째 사진이 있으면 수정모드로 판단
////                    setBtnUi(true);
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(1).getmUpfile1(), MC_IMG1));
//                }else{
////                    setBtnUi(false);
//                }
//
//                if(!mItem.get(1).getmUpfile2().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(1).getmUpfile2(), MC_IMG2));
//                }
//                if(!mItem.get(1).getmUpfile3().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(1).getmUpfile3(), MC_IMG3));
//                }
//                if(!mItem.get(1).getmUpfile4().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(1).getmUpfile4(), MC_IMG4));
//                }
//                if(!mItem.get(1).getmUpfile5().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(1).getmUpfile5(), MC_IMG5));
//                }
//
//                if(!mItem.get(1).getmMcContent().equals("")){
//                    mUpload_Content_Edit.setText(mItem.get(1).getmMcContent());
//                }else{
//                    mUpload_Content_Edit.setText("");
//                }
//                break;
//            case "D":   // 저녁
//                // 사진
//                if(!mItem.get(2).getmUpfile1().equals("")) {    // 첫번째 사진이 있으면 수정모드로 판단
////                    setBtnUi(true);
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(2).getmUpfile1(), MC_IMG1));
//                }else{
////                    setBtnUi(false);
//                }
//
//                if(!mItem.get(2).getmUpfile2().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(2).getmUpfile2(), MC_IMG2));
//                }
//                if(!mItem.get(2).getmUpfile3().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(2).getmUpfile3(), MC_IMG3));
//                }
//                if(!mItem.get(2).getmUpfile4().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(2).getmUpfile4(), MC_IMG4));
//                }
//                if(!mItem.get(2).getmUpfile5().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(2).getmUpfile5(), MC_IMG5));
//                }
//
//                if(!mItem.get(2).getmMcContent().equals("")){
//                    mUpload_Content_Edit.setText(mItem.get(2).getmMcContent());
//                }else{
//                    mUpload_Content_Edit.setText("");
//                }
//                break;
//            case "S":   // 간식
//                // 사진
//                if(!mItem.get(3).getmUpfile1().equals("")) {    // 첫번째 사진이 있으면 수정모드로 판단
////                    setBtnUi(true);
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(3).getmUpfile1(), MC_IMG1));
//                }else{
////                    setBtnUi(false);
//                }
//
//                if(!mItem.get(3).getmUpfile2().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(3).getmUpfile2(), MC_IMG2));
//                }
//                if(!mItem.get(3).getmUpfile3().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(3).getmUpfile3(), MC_IMG3));
//                }
//                if(!mItem.get(3).getmUpfile4().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(3).getmUpfile4(), MC_IMG4));
//                }
//                if(!mItem.get(3).getmUpfile5().equals("")){
//                    mUploadPhotoList.add(new PhotoItem(mItem.get(3).getmUpfile5(), MC_IMG5));
//                }
//
//                if(!mItem.get(3).getmMcContent().equals("")){
//                    mUpload_Content_Edit.setText(mItem.get(3).getmMcContent());
//                }else{
//                    mUpload_Content_Edit.setText("");
//                }
//                break;
//        }
//
//        if(mUploadPhotoList.size() > 0) {   // 사진이 1개 이상이면 이미지 리셋
//            resetImageUi();
//        }
//        setMealBtnUi(); // 식사버튼
//
//    }
//
//    /**
//     * 등록버튼 활성화 유무
//     * @param bool ( true - 활성화, false - 비활성화 )
//     */
//    public void setBtnUi(final boolean bool){
//
//        Handler mHandler = new Handler(Looper.getMainLooper());
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if(bool){
//                    mRegistration_Btn.setEnabled(true);
//
//                    mRegistration_Btn.setBackgroundResource(R.drawable.btn_2_36aa9d);
//                    mRegistration_Btn.setTextColor(ContextCompat.getColorStateList(MealWriteActivity.this, R.color.color_ffffff_btn));
//                }else{
//                    mRegistration_Btn.setEnabled(false);
//
//                    mRegistration_Btn.setBackgroundResource(R.drawable.background_2_36aa9d);
//                    mRegistration_Btn.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_50ffffff));
//                }
//            }
//        }, 0);
//    }
//
//    /**
//     * 식사시간 버튼 설정
//     */
//    public void setMealBtnUi(){
//
//        switch(mMealWhen){
//            case "B":   //아침
//                mMorningTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_00000000));
//                mMorningTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_ffffff));
//                mLunchTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_f2f2f2));
//                mLunchTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.baseAppColor));
//                mDinnerTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_f2f2f2));
//                mDinnerTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.baseAppColor));
//                mSnackTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_f2f2f2));
//                mSnackTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.baseAppColor));
//                break;
//            case "L":   //점심
//                mMorningTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_f2f2f2));
//                mMorningTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.baseAppColor));
//                mLunchTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_00000000));
//                mLunchTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_ffffff));
//                mDinnerTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_f2f2f2));
//                mDinnerTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.baseAppColor));
//                mSnackTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_f2f2f2));
//                mSnackTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.baseAppColor));
//                break;
//            case "D":   //저녁
//                mMorningTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_f2f2f2));
//                mMorningTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.baseAppColor));
//                mLunchTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_f2f2f2));
//                mLunchTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.baseAppColor));
//                mDinnerTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_00000000));
//                mDinnerTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_ffffff));
//                mSnackTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_f2f2f2));
//                mSnackTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.baseAppColor));
//                break;
//            case "S":   //간식
//                mMorningTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_f2f2f2));
//                mMorningTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.baseAppColor));
//                mLunchTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_f2f2f2));
//                mLunchTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.baseAppColor));
//                mDinnerTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_f2f2f2));
//                mDinnerTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.baseAppColor));
//                mSnackTv.setBackgroundColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_00000000));
//                mSnackTv.setTextColor(ContextCompat.getColor(MealWriteActivity.this, R.color.color_ffffff));
//                break;
//        }
//
//        if (mProgressLay != null){
//            mProgressLay.setVisibility(View.INVISIBLE);
//        }
//
//    }
//
////
////    @Override
////    public void onDismiss(ActionSheet actionSheet, boolean b) {
////
////    }
////
////    @Override
////    public void onOtherButtonClick(ActionSheet actionSheet, int i) {
////        switch( i ){
////            case 0: // 사진앨범
////                startGallery();
////                break;
////            case 1: // 새로찍기
////                startCamera();
////                break;
////        }
////    }
//
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        CLog.i("onRequestPermissionsResult");
//        if (requestCode == 2) {
//            if(PermissionUtils.canAccessStorage(this)){
////                setTheme(R.style.ActionSheetStyleiOS9);
//                showActionSheet();
//            }else{
//                Toast.makeText(MealWriteActivity.this, getString(R.string.toast_permission_storage), Toast.LENGTH_LONG).show();
//            }
//        } else {
//            CLog.i("onRequestPermissionsResult else");
//            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        }
//    }
//
//
//
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode , Intent data){
//
//        // TODO Auto-generated method stub
//        CLog.i("resultCode : " + resultCode);
//        CLog.i("requestCode :" + requestCode);
//
//        if(resultCode != Activity.RESULT_OK){
//            CLog.i("resultCode != RESULT_OK");
//            return;
//        }
//        try {
//            switch (requestCode) {
//                case 1: // 갤러리에서 사진 가져오기
//                    CLog.i("갤러리에서 사진 가져오기");
//                    mImageCaptureUri = data.getData();
//
//                    tempFileDelete();
//
//                    FileInputStream in;
//                    FileOutputStream out;
//
//                    File inFile 	= new File( getRealImagePath( mImageCaptureUri ) );
//                    File outFile 	= new File( mFileManager.getFilePath(), TEMP_FILE_NAME );
//
//                    try {
//
//                        in = new FileInputStream( inFile );
//                        out = new FileOutputStream( outFile );
//
//                        try {
//
//                            FileChannel inChannel 	= in.getChannel();
//                            FileChannel outChannel 	= out.getChannel();
//
//                            long size = inChannel.size();
//
//                            inChannel.transferTo(0, size, outChannel);
//
//                            inChannel.close();
//                            outChannel.close();
//
//                            in.close();
//                            out.close();
//
//                        } catch (IOException e) {
//                            // TODO Auto-generated catch block
//                            e.printStackTrace();
//                        }
//
//                    } catch (FileNotFoundException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    }
//
//                    mImageCaptureUri	=	Uri.fromFile( outFile );
////                */
//                case 2:  // 카메라에서 사진 가져오기
////                /* 주석
//                    Intent intent = new Intent("com.android.camera.action.CROP");
//
//                    intent.setDataAndType(mImageCaptureUri, "image/*");
////                 2016-02-18 크롭 자유롭게 하기위해 주석
////                intent.putExtra("aspectX", 1);
////                intent.putExtra("aspectY", 1);
//
//                    intent.putExtra("output", mImageCaptureUri);
//                    intent.putExtra("scale", true);
//
//                    startActivityForResult(intent, 3);
//
//                    break;
////                */
//                case 3:       // 사진 자르기 return
//                    CLog.i("NATIVE_CROP");
////                /* 주석
//                    String path 	= mImageCaptureUri.getPath();
//                    File file 		= new File(path);
//
//                    CLog.i("img path = " +path);
//
//                    BitmapFactory.Options options = new BitmapFactory.Options();
//
//                    try {
//                        options.inJustDecodeBounds = true; // 파일은 읽어서 가지고 오지만 메모리 할당은 하지 않도록
//                        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
//                    }
//                    catch ( Exception e ) {
//
//                        Toast.makeText(MealWriteActivity.this, "photo error = " + e.toString(), Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//
//                    // 현재 크롭된 이미지 사이즈가 MAX SIZE의 2배가 넘으면
//                    // 리사이즈 한다.
//                    int scale = 1;
//
//                    if ( options.outWidth > (640 * 2) ) {
//                        //scale = (int)Math.pow(2, (int)Math.round(Math.log(MAX_IMG_SIZE/(double)Math.max(options.outHeight, options.outWidth)) / Math.log(0.5)));
//                        scale = 2;
//                    }
//
//                    options.inSampleSize				= scale;
//                    options.inJustDecodeBounds 			= false;
//                    options.inPreferredConfig 			= Bitmap.Config.RGB_565;
//                    options.inPreferQualityOverSpeed 	= true;
//                    options.inScaled 					= false;
//
//                    //options.inSampleSize 		= scale;
//
//                    Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
//
//                    // 모든 이미지를 강재로 640x640으로 변경
//                    // 보여주기 편하게 하기 위함
////                myBitmap = Bitmap.createScaledBitmap(myBitmap, MAX_IMG_SIZE, MAX_IMG_SIZE, true);
//
//                    CLog.i("Crop file : " + file.getAbsolutePath());
//                    CLog.i("mUploadPhotoList.size() = " + mUploadPhotoList.size());
//
//                    CLog.i("System.currentTimeMillis() = " + System.currentTimeMillis());
//                    long time = System.currentTimeMillis();
//
//                    mFileManager.SaveBitmapToFileCache(myBitmap, mUploadPhotoList.size() + "_" + time + ".jpg", 70);
//
//
//                    CLog.i("filepath() = " + mFileManager.getFilePath() + mUploadPhotoList.size() + "_" + time + ".jpg");
////                mUploadPhotoList.put(mUploadPhotoList.size() + "", mFileManager.getFilePath() + mUploadPhotoList.size() + ".jpg");
//
//                    String imsi = "";
//                    switch(mPhotoPosition){
//                        case 0:
//                            imsi    =   MC_IMG1;
//                            break;
//                        case 1:
//                            imsi    =   MC_IMG2;
//                            break;
//                        case 2:
//                            imsi    =   MC_IMG3;
//                            break;
//                        case 3:
//                            imsi    =   MC_IMG4;
//                            break;
//                        case 4:
//                            imsi    =   MC_IMG5;
//                            break;
//                    }
//
//                    mUploadPhotoList.add(mPhotoPosition, new PhotoItem(mFileManager.getFilePath() + mUploadPhotoList.size() + "_" + time + ".jpg", imsi));
////                    mBitmapList.add(myBitmap);
//
//                    CLog.i("mFileManager file = " + mFileManager.getFilePath());
//                    CLog.i("uploadPhotolist = " + mUploadPhotoList.get(mUploadPhotoList.size() - 1));
//
//                    CLog.i("mUploadPhotoList.size() = " +mUploadPhotoList.size());
//
//                    CustomImageLoader.displayImage(MealWriteActivity.this, "file://" + mUploadPhotoList.get(mPhotoPosition).getmPhotoUrl(), mPhoto[mPhotoPosition]);
//                    mPhotoDel[mPhotoPosition].setVisibility(View.VISIBLE);
//
//                    /* 2016-07-15 이미지 업로드 수정으로 주석
//                    switch (mUploadPhotoList.size()){
//                        case 1:
//                            CustomImageLoader.displayImage(MealWriteActivity.this, "file://" + mUploadPhotoList.get(0), mPhoto[0]);
//                            mPhotoDel[0].setVisibility(View.VISIBLE);
//                            break;
//                        case 2:
//                            CustomImageLoader.displayImage(MealWriteActivity.this, "file://" + mUploadPhotoList.get(1), mPhoto[1]);
//                            mPhotoDel[1].setVisibility(View.VISIBLE);
//                            break;
//                        case 3:
//                            CustomImageLoader.displayImage(MealWriteActivity.this, "file://" + mUploadPhotoList.get(2), mPhoto[2]);
//                            mPhotoDel[2].setVisibility(View.VISIBLE);
//                            break;
//                        case 4:
//                            CustomImageLoader.displayImage(MealWriteActivity.this, "file://" + mUploadPhotoList.get(3), mPhoto[3]);
//                            mPhotoDel[3].setVisibility(View.VISIBLE);
//                            break;
//                        case 5:
//                            CustomImageLoader.displayImage(MealWriteActivity.this, "file://" + mUploadPhotoList.get(4), mPhoto[4]);
//                            mPhotoDel[4].setVisibility(View.VISIBLE);
//                            break;
//                    }
//                    */
//
//                    mIsPhotoChange = true;
//                    setBtnUi(true);
//
//                    break;
//            }
//
//        }catch(Exception e){
//            CLog.e(e.toString());
//        }
//
//        super.onActivityResult(requestCode, resultCode, data);
//
//    }
//
//    @Override
//    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
//        String strdate = null;
//        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
//        Date date = null;
//        if ((monthOfYear) < 9) {
//            if (dayOfMonth < 10) {
//                strdate = year + "0" + (monthOfYear + 1) + "0" + dayOfMonth;
//            } else {
//                strdate = year + "0" + (monthOfYear + 1) + "" + dayOfMonth;
//            }
//        } else {
//            if (dayOfMonth < 10) {
//                strdate = year + "" + (monthOfYear + 1) + "0" + dayOfMonth;
//            } else {
//                strdate = year + "" + (monthOfYear + 1) + "" + dayOfMonth;
//            }
//
//        }
//        CLog.d("strdate = " + strdate);
//
//        try {
//            date = format.parse(strdate);
//            cal.setTime(date);
//            mCurrentDate = "" +cal.get(Calendar.YEAR)+ Util.getTwoDateFormat(((cal.get(Calendar.MONTH) + 1))) + Util.getTwoDateFormat(cal.get(Calendar.DATE));
//            CLog.d("mCurrentDate = " +mCurrentDate);
//            mDateTv.setText(title_Format.format(cal.getTimeInMillis()));
//            requestMeal(mCurrentDate);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 입력창 리스너
//     */
//    class MyTextWatcher implements TextWatcher
//    {
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count)
//        {
//            if(mItem.get(mMealIndex).getmMcContent().equals(mUpload_Content_Edit.getText().toString())){
//                mIsMemoChange = false;
//            }else{
//                mIsMemoChange = true;
//                if(mUploadPhotoList.size()> 0){
//                    setBtnUi(true);
//                }else{
//                    setBtnUi(false);
//                }
//            }
//
//
//        }
//
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after)
//        {
//        }
//
//        @Override
//        public void afterTextChanged(Editable s)
//        {
//        }
//    }


}
