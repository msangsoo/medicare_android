package kr.co.hi.medicare.bean;

/**
 * 신체활동 내역 리스트.
 */
//// @JsonIgnoreProperties(ignoreUnknown = true)
public class MealActivity_List {
    private String WDATE;
    private String MEAL_WHEN;
    private String MC_CONTENT;
    private String MC_IMG1;
    private String MC_IMG2;
    private String MC_IMG3;
    private String MC_IMG4;
    private String MC_IMG5;
    private String EVAL_YN;
    private String LDATE;
    private String NDATE;
    private String OSEQ;
    private String RESULT_CODE;

    // // @JsonProperty(value = "WDATE")
    public String getWDATE() {
        return WDATE;
    }

    public void setWDATE(String WDATE) {
        this.WDATE = WDATE;
    }


    // // @JsonProperty(value = "MEAL_WHEN")
    public String getMEAL_WHEN() {
        return MEAL_WHEN;
    }

    public void setMEAL_WHEN(String MEAL_WHEN) {
        this.MEAL_WHEN = MEAL_WHEN;
    }

    // // @JsonProperty(value = "MC_CONTENT")
    public String getMC_CONTENT() {
        return MC_CONTENT;
    }

    public void setMC_CONTENT(String MC_CONTENT) {
        this.MC_CONTENT = MC_CONTENT;
    }

    // // @JsonProperty(value = "MC_IMG1")
    public String getMC_IMG1() {
        return MC_IMG1;
    }

    public void setMC_IMG1(String MC_IMG1) {
        this.MC_IMG1 = MC_IMG1;
    }

    // // @JsonProperty(value = "MC_IMG2")
    public String getMC_IMG2() {
        return MC_IMG2;
    }

    public void setMC_IMG2(String MC_IMG2) {
        this.MC_IMG2 = MC_IMG2;
    }

    // // @JsonProperty(value = "MC_IMG3")
    public String getMC_IMG3() {
        return MC_IMG3;
    }

    public void setMC_IMG3(String MC_IMG3) {
        this.MC_IMG3 = MC_IMG3;
    }

    // // @JsonProperty(value = "MC_IMG4")
    public String getMC_IMG4() {
        return MC_IMG4;
    }

    public void setMC_IMG4(String MC_IMG4) {
        this.MC_IMG4 = MC_IMG4;
    }

    // // @JsonProperty(value = "MC_IMG5")
    public String getMC_IMG5() {

        return MC_IMG5;
    }

    public void setMC_IMG5(String MC_IMG5) {
        this.MC_IMG5 = MC_IMG5;
    }

    // // @JsonProperty(value = "EVAL_YN")
    public String getEVAL_YN() {

        return EVAL_YN;
    }

    public void setEVAL_YN(String EVAL_YN) {
        this.EVAL_YN = EVAL_YN;
    }

    // // @JsonProperty(value = "LDATE")
    public String getLDATE() {

        return LDATE;
    }

    public void setLDATE(String LDATE) {
        this.LDATE = LDATE;
    }

    // // @JsonProperty(value = "NDATE")
    public String getNDATE() {

        return NDATE;
    }

    public void setNDATE(String NDATE) {
        this.NDATE = NDATE;
    }


    // // @JsonProperty(value = "OSEQ")
    public String getOSEQ() {

        return OSEQ;
    }

    public void setOSEQ(String OSEQ) {
        this.OSEQ = OSEQ;
    }

    // // @JsonProperty(value = "RESULT_CODE")
    public String getRESULT_CODE() {

        return RESULT_CODE;
    }

    public void setRESULT_CODE(String RESULT_CODE) {
        this.RESULT_CODE = RESULT_CODE;
    }



}


