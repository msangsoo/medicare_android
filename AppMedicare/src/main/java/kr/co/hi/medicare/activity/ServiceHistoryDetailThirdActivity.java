package kr.co.hi.medicare.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.Defined;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.AlertDialogUtil;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.JsonUtil;
import kr.co.hi.medicare.util.Util;
import kr.co.hi.medicare.utilhw.CDateUtil;

/**
 * Created by suwun on 2016-06-16.
 * 서비스 이력보기 (맞춤형 운동&영양관리)
 * @since 0, 1
 */
public class ServiceHistoryDetailThirdActivity extends BaseActivityMedicare {

    private UserInfo user;
    private int mType; // 2 맞춤형 운동
    private Intent intent;

    private TextView mResultTv1 , mResultTv2 , mResultTv3 , mResultTv4 , mResultTv5 , mResultTv6 , mResultTv7 , mResultTv8 , mResultTv9 , mTop_Tv;
    private LinearLayout mGroupLay1 , mGroupLay2 , mTopLay , mTopLay1;
    private String mResult1 , mResult2 , mResult3 , mResult4 , mResult5 , mResult6 , mResult7 , mResult8 , mResult9;
    private ScrollView mAllLay;

    private boolean mAFlag = false , mBFlag = false , mCFlag = false , mTopFlag = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_history_third);
        user = new UserInfo(this);

        init();
    }

    private void init() {

        intent = getIntent();
        if (intent != null){
            mType = intent.getIntExtra(EXTRA_SERVICETYPE , 0);
        }

        CLog.i("mType --> " + mType);

        mTop_Tv     =   (TextView) findViewById(R.id.top_tv2);
        mResultTv1  =   (TextView) findViewById(R.id.result_tv1);
        mResultTv2  =   (TextView) findViewById(R.id.result_tv2);
        mResultTv3  =   (TextView) findViewById(R.id.result_tv3);
        mResultTv4  =   (TextView) findViewById(R.id.result_tv4);
        mResultTv5  =   (TextView) findViewById(R.id.result_tv5);
        mResultTv6  =   (TextView) findViewById(R.id.result_tv6);
        mResultTv7  =   (TextView) findViewById(R.id.result_tv7);
        mResultTv8  =   (TextView) findViewById(R.id.result_tv8);
        mResultTv9  =   (TextView) findViewById(R.id.result_tv9);
        mGroupLay1  =   (LinearLayout) findViewById(R.id.group_lay1);
        mGroupLay2  =   (LinearLayout) findViewById(R.id.group_lay2);
        mTopLay     =   (LinearLayout) findViewById(R.id.top_lay);
        mTopLay1     =   (LinearLayout) findViewById(R.id.top_lay1);
        mAllLay     =   (ScrollView) findViewById(R.id.all_lay);

        mTopLay.setVisibility(View.GONE);
        mAllLay.setVisibility(View.GONE);
        requestHistoryApi(mType);

    }


    /**
     * api 호출
     * @param mtype
     */

    public void requestHistoryApi(int mtype) {

        CLog.i("mtype --> " + mtype);
        /*
        http -> https 변경 160627

        JSONObject jObject = new JSONObject();
        try {
            jObject.put("SEQ", user.getSeq());      // 회원일렬번호
        } catch (JSONException e) {
            e.printStackTrace();
        }
        request(EServerAPI.API_POST, RequestUtil.getJSONParameter(jObject, "DZ004"));
        */

        try {
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            client.setTimeout(10000);
            client.addHeader("Accept-Charset", "UTF-8");
            client.addHeader("User-Agent", new WebView(getBaseContext()).getSettings().getUserAgentString());

            Tr_login login = UserInfo.getLoginInfo();

            JSONObject jObject = new JSONObject();
            if(BuildConfig.DEBUG){
                jObject.put("SEQ", "0001013000015");
            }else{
                jObject.put("SEQ", login.seq);
            }
            jObject.put("DOCNO", "DZ004");

            params.put("strJson", jObject.toString());
            CLog.i("url = " + Defined.COMMUNITY_HTTPS_URL + "?" + params.toString());
            client.post(Defined.COMMUNITY_HTTPS_URL, params, mAsyncHttpHandler);
        } catch (Exception e) {
            CLog.e(e.toString());
        }


    }

    private AsyncHttpResponseHandler mAsyncHttpHandler = new AsyncHttpResponseHandler() {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            CLog.i("onSuccess");

            try {

                int count = headers.length;

                for (int i = 0; i < count; i++) {
                    CLog.v(headers[i].getName() + ": " + headers[i].getValue());
                }

                String response = new String(responseBody, "UTF-8");
                CLog.i("response = " +response);
                JSONObject resultData = null;

                if(response.startsWith("<")){   // api result 문자열 시작값이 "<" 로 시작하는 경우는 xml 폼
                    try {
                        response =  Util.parseXml(response);
                    }catch(Exception e){
                        CLog.e(e.toString());
                    }

                    resultData = new JSONObject(response);

                }else{
                    resultData = new JSONObject(response);
                }

                Intent intent;

                String resultCode = resultData.getString("DOCNO");
                switch(resultCode){
                    case "DZ004": //
                        try {
                            String resultLength = (String) resultData.get("DATA_LENGTH");

                            if (!resultLength.equals("0")) {
                                JSONArray jsonArray = new JSONArray(resultData.getString("DATA"));
                                JSONObject object1 = jsonArray.getJSONObject(0);

                                switch (object1.getString("RESULT_CODE")) {
                                    case "0000":
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject object = jsonArray.getJSONObject(i);

                                            if (object.getString("SGUBUN").equals("A")) {
                                                mAFlag = true;
                                                mResult1 = object.getString("REQDATE");
                                                mResult2 = object.getString("ENDDATE");
                                                mResult3 = object.getString("STATE_NAME");
                                            }

                                            if (object.getString("SGUBUN").equals("B")) {
                                                mBFlag = true;
                                                mResult7 = object.getString("REQDATE");
                                                mResult8 = object.getString("USEDATE");
                                                mResult9 = object.getString("USE_MEMO");
                                            }

                                            if (object.getString("SGUBUN").equals("C")) {
                                                mCFlag = true;
                                                mResult4 = object.getString("REQDATE");
                                                mResult5 = object.getString("USEDATE");
                                                mResult6 = object.getString("USE_MEMO");
                                            }

                                            mTopFlag = true;
                                        }

                                        break;
                                    case "4444":
                                        UIThread(ServiceHistoryDetailThirdActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "6666":
                                        UIThread(ServiceHistoryDetailThirdActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "9999":
                                        UIThread(ServiceHistoryDetailThirdActivity.this, getString(R.string.confirm), getString(R.string.no_user) , true);
                                        break;

                                }
                                UiThread();
                                break;
                            } else {
                                UIThread(ServiceHistoryDetailThirdActivity.this, getString(R.string.confirm), getString(R.string.meal_tok_notdata) , true);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }

            } catch (Exception e) {
                CLog.e(e.toString());
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            CLog.i("onFailure");
            UIThread(ServiceHistoryDetailThirdActivity.this, getString(R.string.confirm), getString(R.string.networkexception) , true);

        }
    };


    public void click_event(View v) {

        switch (v.getId()) {
            // 뒤로
            case R.id.activity_actrecord_ImageView_back:
                finish();
                break;
        }
    }

    @Override
    public void request(EServerAPI eServerAPI, Object obj) {
        Record record = new Record();
        record.setEServerAPI(eServerAPI);
        switch (eServerAPI) {
            case API_GET:
                record.setRequestData(obj);
                break;
            case API_POST:
                record.setRequestData(obj);
                break;
        }
        sendApi(record);
    }

    @Override
    public void response(Record record) throws UnsupportedEncodingException {
        String json = new String(record.getData());
        json = json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1);
        Map<String, Object> result = JsonUtil.getJSONParserData(record.getData());
        if (result != null && result.containsKey("DOCNO")) {
            try {
                switch ((String) result.get("DOCNO")) {
                    case "DZ004": //
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            CLog.d(jsonObject.toString());

                            String resultLength = (String) result.get("DATA_LENGTH");

                            if (!resultLength.equals("0")) {
                                JSONArray jsonArray = new JSONArray(jsonObject.getString("DATA"));
                                JSONObject object1 = jsonArray.getJSONObject(0);

                                switch (object1.getString("RESULT_CODE")) {
                                    case "0000":
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject object = jsonArray.getJSONObject(i);

                                            if (object.getString("SGUBUN").equals("A")) {
                                                mAFlag = true;
                                                mResult1 = object.getString("REQDATE");
                                                mResult2 = object.getString("ENDDATE");
                                                mResult3 = object.getString("STATE_NAME");
                                            }

                                            if (object.getString("SGUBUN").equals("B")) {
                                                mBFlag = true;
                                                mResult7 = object.getString("REQDATE");
                                                mResult8 = object.getString("USEDATE");
                                                mResult9 = object.getString("USE_MEMO");
                                            }

                                            if (object.getString("SGUBUN").equals("C")) {
                                                mCFlag = true;
                                                mResult4 = object.getString("REQDATE");
                                                mResult5 = object.getString("USEDATE");
                                                mResult6 = object.getString("USE_MEMO");
                                            }

                                            mTopFlag = true;
                                        }

                                        break;
                                    case "4444":
                                        UIThread(ServiceHistoryDetailThirdActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "6666":
                                        UIThread(ServiceHistoryDetailThirdActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "9999":
                                        UIThread(ServiceHistoryDetailThirdActivity.this, getString(R.string.confirm), getString(R.string.no_user) , true);
                                        break;

                                }
                                UiThread();
                                break;
                            } else {
                                UIThread(ServiceHistoryDetailThirdActivity.this, getString(R.string.confirm), getString(R.string.meal_tok_notdata) , true);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void networkException(Record record) {
        CLog.e("ERROR STATE : " + record.stateCode);
        UIThread(ServiceHistoryDetailThirdActivity.this, getString(R.string.confirm), getString(R.string.networkexception) , true);
    }

    private void UIThread(final Activity activity, final String confirm, final String message, final boolean asd) {
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertDialogUtil.onAlertDialog(activity, confirm, message , asd);
            }
        }, 0);
    }

    private void UiThread() {
        final Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                CLog.i("UiThread ---> ");

                if (mTopFlag){
                    mTopLay.setVisibility(View.GONE);
                    mAllLay.setVisibility(View.VISIBLE);
                    mTopLay1.setVisibility(View.VISIBLE);
                }

                if (mBFlag){
                    mGroupLay2.setVisibility(View.VISIBLE);
                }

                if (mCFlag){
                    mGroupLay1.setVisibility(View.VISIBLE);
                }

                mResultTv1.setText(CDateUtil.getDateTimeFormatyyyyMMddhhmm(mResult1));
                mResultTv2.setText(CDateUtil.getFormatYYYYMMDD(mResult2));
                mResultTv3.setText(mResult3);
                mResultTv4.setText(CDateUtil.getDateTimeFormatyyyyMMddhhmm(mResult4));
                mResultTv5.setText(CDateUtil.getDateTimeFormatyyyyMMddhhmm(mResult5));
                mResultTv6.setText(mResult6);
                mResultTv7.setText(CDateUtil.getDateTimeFormatyyyyMMddhhmm(mResult7));
                mResultTv8.setText(CDateUtil.getDateTimeFormatyyyyMMddhhmm(mResult8));
                mResultTv9.setText(mResult9);

                double date = Util.getTwoDateCompare(mResult2 , "yyyy-MM-dd");
                long a = Math.abs((long) date);

                String imsi = String.valueOf(a);
                mTop_Tv.setText(imsi + " 일");

            }

        }, 0);


    }
}
