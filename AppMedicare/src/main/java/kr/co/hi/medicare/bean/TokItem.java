package kr.co.hi.medicare.bean;


/**
 * Created by suwun on 2016-06-15.
 * 영양톡 데이터
 * @since 0, 1
 */
public class TokItem {

    private String HABITS_GUBUN;
    private String HABITS_MEMO;
    private String HB_IMG1;
    private String HB_IMG2;
    private String HB_IMG3;
    private String LDATE;
    private String NDATE;

    /**
     *  HABITS_GUBUN	CHAR	1	식습관 구분 (K:유지해야할 식습관 ,R:고쳐야할 식습관)
     *  HABITS_MEMO	VARCHAR	2000	식습관 코칭메모
     *  HB_IMG1	VARCHAR	500	이미지1
     *  HB_IMG2	VARCHAR	500	이미지2
     *  HB_IMG3	VARCHAR	500	이미지3
     *  LDATE	Char	8	코칭 시작일 기준 시작일  ex)20160301
     *  NDATE	Char	8	 코칭 종료일 기준 시작일 ex)20160301
     */


    public TokItem(String habits_gubun,
                   String habits_memo,
                   String hb_img1,
                   String hb_img2,
                   String hb_img3,
                   String ldate,
                   String ndate
                   ){
        this.HABITS_GUBUN =   habits_gubun;
        this.HABITS_MEMO  =   habits_memo;
        this.HB_IMG1   =   hb_img1;
        this.HB_IMG2   =   hb_img2;
        this.HB_IMG3   =   hb_img3;
        this.LDATE   =   ldate;
        this.NDATE   =   ndate;
    }

    public String getHABITS_GUBUN() {
        return HABITS_GUBUN;
    }

    public void setHABITS_GUBUN(String HABITS_GUBUN) {
        this.HABITS_GUBUN = HABITS_GUBUN;
    }

    public String getHABITS_MEMO() {
        return HABITS_MEMO;
    }

    public void setHABITS_MEMO(String HABITS_MEMO) {
        this.HABITS_MEMO = HABITS_MEMO;
    }

    public String getHB_IMG1() {
        return HB_IMG1;
    }

    public void setHB_IMG1(String HB_IMG1) {
        this.HB_IMG1 = HB_IMG1;
    }

    public String getHB_IMG2() {
        return HB_IMG2;
    }

    public void setHB_IMG2(String HB_IMG2) {
        this.HB_IMG2 = HB_IMG2;
    }

    public String getHB_IMG3() {
        return HB_IMG3;
    }

    public void setHB_IMG3(String HB_IMG3) {
        this.HB_IMG3 = HB_IMG3;
    }

    public String getLDATE() {
        return LDATE;
    }

    public void setLDATE(String LDATE) {
        this.LDATE = LDATE;
    }

    public String getNDATE() {
        return NDATE;
    }

    public void setNDATE(String NDATE) {
        this.NDATE = NDATE;
    }
    
}
