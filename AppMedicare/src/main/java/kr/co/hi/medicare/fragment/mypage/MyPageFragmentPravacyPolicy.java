package kr.co.hi.medicare.fragment.mypage;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.LoginActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.common.DummyWebviewFragment;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.data.ReceiveDataCode;
import kr.co.hi.medicare.googleFitness.GoogleFitInstance;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_mber_drop_info;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.value.Define;


public class MyPageFragmentPravacyPolicy extends BaseFragmentMedi {

    public static BaseFragmentMedi newInstance() {
        MyPageFragmentPravacyPolicy fragment = new MyPageFragmentPravacyPolicy();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mypage_privacy_policy, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.setting_join_content1).setOnClickListener(mClickListener);
        view.findViewById(R.id.setting_join_content2).setOnClickListener(mClickListener);
        view.findViewById(R.id.setting_join_content3).setOnClickListener(mClickListener);
        view.findViewById(R.id.btn_withdrawal).setOnClickListener(mClickListener);
//        view.findViewById(R.id.setting_join_content4).setOnClickListener(mClickListener);
//        view.findViewById(R.id.setting_join_content5).setOnClickListener(mClickListener);
//        view.findViewById(R.id.setting_join_content5).setVisibility(UserInfo.getLoginInfo().mber_grad.equals("10") ? View.GONE : View.VISIBLE);
//        view.findViewById(R.id.line_last).setVisibility(UserInfo.getLoginInfo().mber_grad.equals("10") ? View.GONE : View.VISIBLE);
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            Bundle bundle = new Bundle();
            String url = "";
            Tr_login login = UserInfo.getLoginInfo();
            switch (vId) {
                case R.id.setting_join_content1:
                    bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract1_title_1));
//                    url = login.mber_grad.equals("10") ? getString(R.string.personal_terms_1_url) : getString(R.string.personal_terms_jun_1_url);
                    url = getString(R.string.personal_terms_1_url);
                    bundle.putString(DummyWebviewFragment.URL, url);
                    NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                    break;
                case R.id.setting_join_content2:
                    bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract2_title_1));
//                    url = login.mber_grad.equals("10") ? getString(R.string.personal_terms_2_url) : getString(R.string.personal_terms_jun_2_url);
                    url = getString(R.string.personal_terms_2_url);
                    bundle.putString(DummyWebviewFragment.URL, url);
                    NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                    break;
                case R.id.setting_join_content3:
                    bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract3_title_1));
//                    url = login.mber_grad.equals("10") ? getString(R.string.personal_terms_3_url) : getString(R.string.personal_terms_jun_3_url);
                    url = getString(R.string.personal_terms_3_url);
                    bundle.putString(DummyWebviewFragment.URL, url);
                    NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                    break;
                case R.id.btn_withdrawal:
                    CDialog.showDlg(getContext(),getString(R.string.withdrawal_text1))
                            .setOkButton("확인",new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    getWithdrawal();
                                }
                            })
                            .setNoButton("취소",null);
                    break;
//                case R.id.setting_join_content4:
//                    bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract5_title_1));
//                    url = login.mber_grad.equals("10") ? getString(R.string.location_terms_url) : getString(R.string.location_terms_jun_url);
//                    bundle.putString(DummyWebviewFragment.URL, url);
//                    DummyActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
//                    break;
//                case R.id.setting_join_content5:
//                    bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_step1_contract4_title_1));
//                    url = getString(R.string.marketing_terms_jun_url);
//                    bundle.putString(DummyWebviewFragment.URL, url);
//                    DummyActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
//                    break;

            }
        }
    };


    /**
     * 서비스 탈퇴
     */
    private void getWithdrawal() {
        final Tr_mber_drop_info.RequestData requestData = new Tr_mber_drop_info.RequestData();

        requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;

        MediNewNetworkModule.doApi(getContext(), new Tr_mber_drop_info(), requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                if (responseData instanceof Tr_mber_drop_info) {
                    Tr_mber_drop_info data = (Tr_mber_drop_info)responseData;
                    try {
                        switch (data.result_code){
                            case ReceiveDataCode.MBER_LOAD_MYPAGE_SUCCESS:
                                CDialog.showDlg(getContext(),getString(R.string.withdrawal_text2))
                                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {
                                                Logout();
                                            }
                                        });
                                break;

                            case ReceiveDataCode.MBER_LOAD_MYPAGE_ERROR_FAIL:
                                CDialog.showDlg(getContext(),"회원탈퇴에 실패하였습니다.");

                                break;

                            default:

                                break;
                        }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(),getResources().getString(R.string.comm_error_db004));
//                Log.e(TAG, response);
            }
        });
    }

    private void Logout(){

        FragmentManager fm = getActivity().getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }

        SharedPref.getInstance().savePreferences(SharedPref.IS_AUTO_LOGIN, false);
        SharedPref.getInstance().savePreferences(SharedPref.IS_LOGIN_SUCEESS, false);
        Define.getInstance().setWeatherRequestedTime(-1);

        UserInfo user = new UserInfo(getContext());
        user.setIsAutoLogin(false);      // 자동로그인

        moveToLogin();

        GoogleFitInstance.signOutGoogleClient(getContext(), new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                // 로그인 화면으로 이동 처리
//                        moveToLogin(intent);
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                error.printStackTrace();
                // 로그인 화면으로 이동 처리
//                        moveToLogin(intent);
            }
        });

    }

    private void moveToLogin() {
        Intent intent = new Intent(getContext(), LoginActivityMedicare.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        getActivity().startActivity(intent);
        getActivity().finish();
    }


}