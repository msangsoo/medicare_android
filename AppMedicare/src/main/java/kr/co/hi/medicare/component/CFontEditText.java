package kr.co.hi.medicare.component;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import kr.co.hi.medicare.R;

/**
 * Created by mrsohn on 2017. 3. 21..
 */
public class CFontEditText extends EditText {

    public CFontEditText(Context context) {
        super(context);
    }

    public CFontEditText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CFontEditText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void init(AttributeSet attrs) {

//        TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(
//                attrs,
//                R.styleable.TextViewWithFont,
//                0, 0);
//
//        String typeface = typedArray.getString(R.styleable.TextViewWithFont_font);

//        Typeface typeface = getContext().getResources().getFont(R.font.notosanskr_light);
        Typeface tf = ResourcesCompat.getFont(getContext(), R.font.notosanskr_regular);
        setTypeface(tf);
//        if (tf != null) {
//            try {
////                Typeface tf = Typeface.createFromAsset(getContext().getAssets(), typeface);
//                setTypeface(tf);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        } else {
//            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), getContext().getString(R.string.notosanskr_regular));
//            setTypeface(tf);
//        }
    }
}