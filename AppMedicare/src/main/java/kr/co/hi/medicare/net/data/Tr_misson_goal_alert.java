package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *미션별 포인트 멘트
 *
 * input값
 * insures_code : 회사코드
 * app_code : 기기명
 * mber_sn : 회원키값
 * misson_goal_alert_typ : 구분  1 :체중등록 2: 혈압등록 3: 식사일지 등록 4:혈당등록
 *
 * output 값
 * api_code : 기기명
 * insures_code : 회사코드
 * misson_goal_alert_typ" : 구분  1 :체중등록 2: 혈압등록 3: 식사일지 등록 4:혈당등록
 * misson_goal_txt : 내용
 * misson_goal_point : 포인트
 * data_yn : Y / N
 *
 */

public class Tr_misson_goal_alert extends BaseData {
	private final String TAG = Tr_misson_goal_alert.class.getSimpleName();

	public static class RequestData {
		public String mber_sn; //회원 일련번호
		public String misson_goal_alert_typ;
	}


	@Override
	public JSONObject makeJson(Object obj) throws JSONException {
		if (obj instanceof RequestData) {
			JSONObject body = getBaseJsonObj();

			RequestData data = (RequestData) obj;
			body.put("api_code", getApiCode(TAG));
			body.put("insures_code", INSURES_CODE);
			body.put("mber_sn", data.mber_sn);
			body.put("misson_goal_alert_typ", data.misson_goal_alert_typ);
			return body;
		}

		return super.makeJson(obj);
	}

	/**************************************************************************************************/
	/***********************************************RECEIVE********************************************/
	/**************************************************************************************************/
	@SerializedName("api_code")
	public String api_code;
	@SerializedName("insures_code")
	public String insures_code;
	@SerializedName("misson_goal_alert_typ")
	public String insures_misson_goal_alert_typcode;
	@SerializedName("misson_goal_txt")
	public String misson_goal_txt;
	@SerializedName("misson_goal_point")
	public String misson_goal_point;
	@SerializedName("data_yn")
	public String data_yn;

}
