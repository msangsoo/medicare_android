
package kr.co.hi.medicare.chartview.food;

import android.content.Context;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.charting.components.MarkerView;
import kr.co.hi.medicare.charting.data.CEntry;
import kr.co.hi.medicare.charting.highlight.Highlight;
import kr.co.hi.medicare.charting.utils.MPPointF;

import java.text.DecimalFormat;

/**
 * Custom implementation of the MarkerView.
 * 
 * @author Philipp Jahoda
 */
public class RadarMarkerView extends MarkerView {

    private TextView tvContent;
    private DecimalFormat format = new DecimalFormat("##0");

    public RadarMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        tvContent = (TextView) findViewById(R.id.tvContent);
        tvContent.setTypeface(ResourcesCompat.getFont(context, R.font.notosanskr_light));
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(CEntry e, Highlight highlight) {
        tvContent.setText(format.format(e.getY()) + " %");

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight() - 10);
    }
}
