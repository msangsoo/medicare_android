package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import kr.co.hi.medicare.utilhw.Logger;

/**
 * 진료예약, 검진예약(고유키값) 나중에 회원쪽 cmpny_code HB으로 바꿔야 한다 20180313
 */

public class Tr_mber_hospital_booking extends BaseData {
    private final String TAG = Tr_mber_hospital_booking.class.getSimpleName();

    public static class RequestData {

        public String mber_sn;
        public String pageNumber;

    }

    public Tr_mber_hospital_booking() {
        super.conn_url = "http://m.shealthcare.co.kr/HL_MED/WebService/MED_MOBILE_CALL.asmx/MED_mobile_Call";
    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof RequestData) {
            JSONObject body = new JSONObject();
            RequestData data = (RequestData) obj;

            body.put("api_code", getApiCode(TAG));
            body.put("insures_code", INSURES_CODE);
            body.put("mber_sn", data.mber_sn);
            body.put("pageNumber", data.pageNumber); //  1000

            return body;
        }

        return super.makeJson(obj);
    }

    public JSONArray getArray(Tr_get_hedctdata.DataList dataList) {
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        try {
            obj.put("idx", dataList.idx); //170410173713859",
            array.put(obj);
        } catch (JSONException e) {
            Logger.e(e);
        }

        return array;
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/
    @Expose
    @SerializedName("booking_arr")
    public List<Booking_arr> booking_arr;
    @Expose
    @SerializedName("data_lenth")
    public String data_lenth;
    @Expose
    @SerializedName("api_code")
    public String api_code;

    @Expose
    @SerializedName("maxpageNumber")
    public String maxpageNumber;

    public static class Booking_arr {
        @Expose
        @SerializedName("h_name")
        public String h_name;
        @Expose
        @SerializedName("step")
        public String step;
        @Expose
        @SerializedName("reg_date")
        public String reg_date;
        @Expose
        @SerializedName("second_date")
        public String second_date;
        @Expose
        @SerializedName("first_date")
        public String first_date;
    }

}
