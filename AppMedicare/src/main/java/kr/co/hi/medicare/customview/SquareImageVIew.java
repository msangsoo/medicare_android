package kr.co.hi.medicare.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

public class SquareImageVIew extends AppCompatImageView {

    public SquareImageVIew(Context context) {
        super(context);
    }

    public SquareImageVIew(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageVIew(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        setMeasuredDimension(width, width);
    }
}
