package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;


/**
 걸음수 10000보이상

 input값
 insures_code : 회사코드
 mber_sn : 회원고유키값
 api_code : api 키값
 misson_work_typ : 10000보 일때 호출 한다. (1)

 output 값
 api_code : 호출코드명
 insures_code : 회사코드
 misson_work_typ : 10000보 일때 호출 한다. (1)
 misson_goal_txt : 텍스트
 misson_goal_point : 포인트
 data_yn : data 확인여부 (y,n)

 */

public class Tr_mvm_misson_goal_alert extends BaseData {
    private final String TAG = Tr_mvm_misson_goal_alert.class.getSimpleName();

    public static class RequestData {
        public String mber_sn; // 1000
        public String misson_work_typ; // 1000
    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof Tr_mvm_misson_goal_alert.RequestData) {
            JSONObject body = new JSONObject();
            Tr_mvm_misson_goal_alert.RequestData data = (Tr_mvm_misson_goal_alert.RequestData) obj;
            body.put("api_code", getApiCode(TAG));
            body.put("mber_sn", data.mber_sn);
            body.put("insures_code", INSURES_CODE);
            body.put("misson_work_typ",  data.misson_work_typ); //
            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("api_code")
    public String api_code; //
    @SerializedName("insures_code")
    public String insures_code; //
    @SerializedName("data_yn")
    public String data_yn; //



    @SerializedName("misson_work_typ")
    public String misson_work_typ; //
    @SerializedName("misson_goal_txt")
    public String misson_goal_txt; //
    @SerializedName("misson_goal_point")
    public String misson_goal_point; //

}
