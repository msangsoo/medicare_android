package kr.co.hi.medicare.net.data;

/**
 *
 * 문자인증확인(간편인증)
 Input 값
 insures_code: 회사코드
 token: 토큰값
 mber_hp: 휴대폰번호
 result_key: 문자인증코드

 Output 값
 api_code: 호출코드명
 insures_code: 회사코드
 mber_hp: 전화번호
 m_name: 이름
 m_sex: 성별
 m_birth: 생년월일
 result_code: 결과코드(0000: 일치 / 4444: 불일치 / 6666: 가입된 정보가 있음)
 */

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;


public class Tr_mber_sms_check extends BaseData {
    private final String TAG = getClass().getSimpleName();

    public Tr_mber_sms_check() {
//    super.conn_url ="https://wkd.walkie.co.kr:443/HS_HL/ws.asmx/getJson";
    }

    public static class RequestData {
        public String mber_hp; // 1000
        public String token; // 1000
        public String result_key; // 99
    }


    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {

            JSONObject body = new JSONObject();

            RequestData data = (RequestData) obj;
            body.put("api_code", TAG.replace("Tr_", ""));
            body.put("insures_code", INSURES_CODE);
            body.put("mber_hp", data.mber_hp); // 1000",
            body.put("token", data.token); // 1000",
            body.put("result_key", data.result_key); // 99

            return body;
        }

        return super.makeJson(obj);
    }

/**************************************************************************************************/
/***********************************************RECEIVE********************************************/
    /**************************************************************************************************/
    @SerializedName("insures_code")
    public String insures_code;
    @SerializedName("api_code")
    public String api_code;

    @SerializedName("result_code")
    public String result_code;
    @SerializedName("mber_hp")
    public String mber_hp;

    @SerializedName("m_name")
    public String m_name;
    @SerializedName("m_sex")
    public String m_sex;
    @SerializedName("m_birth")
    public String m_birth;

}
