package kr.co.hi.medicare.fragment.login;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.text.DecimalFormat;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.BaseActivityMedicare;
import kr.co.hi.medicare.component.CAlertDialogBuilder;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.login.join.JoinDataVo;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.utilhw.TextWatcherFloatUtil;

/**
 * Created by MrsWin on 2017-02-16.
 */

public class LoginFirstInfoFragment1_2 extends BaseFragmentMedi implements BaseActivityMedicare.onKeyBackCatchListener {
    private final String TAG = LoginFirstInfoFragment1_2.class.getSimpleName();
    public static final String BUNDLE_IS_REGIST = "BUNDLE_IS_REGIST";   // 회원가입후 바로 들어왔는지 여부

    private EditText mHeightTv;
    private EditText mWeightTv;
//    private TextView mSugertypeTv;
//    private TextView mSugerdateTv;
    private EditText mGoalWeightTv;
    private Button  nextbutton;

    private int mSelectNum;
    private String mSugarType;

    private JoinDataVo dataVo;

    boolean isValid = false;

    boolean mIsRegist = false;       // 가입후 바로 들어왔는지 여부

    private InputMethodManager mImm;

    public static Fragment newInstance() {
        LoginFirstInfoFragment1_2 fragment = new LoginFirstInfoFragment1_2();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.join_first_info_1_1_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            mIsRegist = getArguments().getBoolean(BUNDLE_IS_REGIST);
        }

        mImm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        getToolBar(view).getLeftICon().setVisibility(View.GONE);    // 백 버튼 가리기


        mHeightTv =  view.findViewById(R.id.login_first_height_tv);
        mWeightTv =  view.findViewById(R.id.login_first_weight_tv);
        mGoalWeightTv = view.findViewById(R.id.login_first_goalweight_tv);

        nextbutton = (Button) view.findViewById(R.id.next_button);

//        mSugertypeTv.setOnClickListener(mClickListener);
//        mSugerdateTv.setOnClickListener(mClickListener);
        nextbutton.setOnClickListener(mClickListener);


        dataVo = new JoinDataVo();
        dataVo.setSugartype("0");
        dataVo.setSugardate("9999");

        new TextWatcherFloatUtil().setTextWatcher(mHeightTv, 230, 1);
        new TextWatcherFloatUtil().setTextWatcher(mWeightTv, 300, 2);
        new TextWatcherFloatUtil().setTextWatcher(mGoalWeightTv, 300, 2);

        mHeightTv.addTextChangedListener(mWatcher);
        mWeightTv.addTextChangedListener(mWatcher);
        mGoalWeightTv.addTextChangedListener(mWatcher);


    }

    TextWatcher mWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            validCheck(false);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private boolean validCheck(boolean isShowDlg) {
        String heightStr = mHeightTv.getText().toString();
        String weightStr = mWeightTv.getText().toString();
        String goalWeightStr = mGoalWeightTv.getText().toString();
        if (isShowDlg) {
            heightStr = replaceEndDot(mHeightTv);
            weightStr = replaceEndDot(mWeightTv);
            goalWeightStr = replaceEndDot(mGoalWeightTv);
        }

        float height = StringUtil.getFloat(heightStr);
        float weight = StringUtil.getFloat(weightStr);
        float gweight = StringUtil.getFloat(goalWeightStr);

        if (height < 90 ||  height > 230) {
            String msg = "키를 정확히 입력하세요\n입력 범위는 90~230cm입니다.";
            if (isShowDlg)
                CDialog.showDlg(getContext(), msg);
            Log.e(TAG, msg);
            nextbutton.setEnabled(false);
            return false;
        }
        if (weight < 20 ||  weight > 300) {
            String msg = "체중을 정확히 입력하세요\n입력 범위는 20~300kg입니다.";
            if (isShowDlg)
                CDialog.showDlg(getContext(), msg);
            nextbutton.setEnabled(false);
            Log.e(TAG, msg);
            return false;
        }
        if (gweight < 20 || gweight > 300) {
            String msg = "목표체중을 정확히 입력하세요\n입력 범위는 20~300kg입니다.";
            if (isShowDlg)
                CDialog.showDlg(getContext(), msg);
            Log.e(TAG, msg);
            nextbutton.setEnabled(false);
            return false;
        }
        nextbutton.setEnabled(true);
        return true;
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TextView tv = (TextView) v;
            int vId = v.getId();
            int idx = 0;
            int sameIdx = -1;

            if (R.id.next_button == vId) {
                if (validCheck(true) == false)
                    return;

                doInfo();
                return;
            }

//            if (tv == mSugertypeTv) {
//                String[] items = getResources().getStringArray(R.array.sugar_types);
//                String selectText = mSugertypeTv.getText().toString();
//                if("".equals(selectText)){
//                    sameIdx = 0;
//                }
//                for (String item : items) {
//                    if (selectText.equals(item)) {
//                        sameIdx = idx;
//                    }
//                    idx++;
//                }
//                selectDialog(items, sameIdx, tv,"3");
//            } else if (tv == mSugerdateTv) {
//                List<String> height = new ArrayList<>();
//                Calendar cal = Calendar.getInstance();
//                String selectText = mSugerdateTv.getText().toString();
//                height.add(0,"없음");
//
//                if("없음".equals(selectText) || "".equals(selectText)){
//                    sameIdx = 0;
//                }
//
//                for (int i = cal.get(Calendar.YEAR); i >= 1960 ; i--) {
//                    String str = i +"년";
//                    height.add(str);
//                    if (str.equals(selectText)) {
//                        sameIdx = idx+1;
//                    }
//                    idx++;
//                }
//                String[] datas = height.toArray(new String[height.size()]);
//                selectDialog(datas, sameIdx, tv,"4");
//            }

        }
    };

    private String replaceEndDot(EditText editText) {
        String val = editText.getText().toString();
        if (val.endsWith(".")) {
            val = val.replaceAll("\\.", "");
            editText.setText(val);
            return val;
        }
        return val;
    }

    AlertDialog dlg;
    private AlertDialog selectDialog(final String[] items, int checkedItem, final TextView tv, final String type) {
        ListAdapter adapter = new ListAdapter(items);
        CAlertDialogBuilder builder = new CAlertDialogBuilder(getContext());

            String defalutItem = items[checkedItem];
            mSelectNum = checkedItem;
            tv.setTag(defalutItem);

        builder.setSingleChoiceItems(adapter, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String selectItem = items[which];
                mSelectNum = which;
                tv.setTag(selectItem);  // 임시로 tag에 저장

                tv.setText(selectItem);

                /*if (dlg != null)
                    dlg.dismiss();*/
            }
        });
        builder.init();
        builder.setOkButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv.getTag() != null) {
                    tv.setText(tv.getTag().toString()); // tag에 저장된 값 세팅
                }
                String [] mTextValue;
                switch (type) {

                    case "3":
                        mSugarType = String.valueOf(mSelectNum);
                        dataVo.setSugartype(mSugarType);
                        tv.setCompoundDrawablesWithIntrinsicBounds(0,0, 0, 0);
                        break;
                    case "4":
                        if ("없음".equals(tv.getText().toString())) {
                            mTextValue = new String[]{"9999"};
                            dataVo.setSugardate(mTextValue[0]);
                        } else {
                            mTextValue = tv.getText().toString().split("년");
                            dataVo.setSugardate(mTextValue[0]);
                        }
                        tv.setCompoundDrawablesWithIntrinsicBounds(0,0, 0, 0);
                        break;
                }

            }
        });

        dlg = builder.create();
        dlg.show();


        return dlg;
    }


    public View init() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.alertdialog_layout, null);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;

        return view;
    }


    class ListAdapter extends BaseAdapter {

        String[] mStrList;

        public ListAdapter(String[] strList) {
            mStrList = strList;
        }


        public String[] getEmails() {
            return mStrList;
        }

        @Override
        public int getCount() {
            return mStrList.length;
        }

        @Override
        public String getItem(int position) {
            return mStrList[position];
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_single_choice_item, null);
                holder.tv = view.findViewById(R.id.text1);

                WindowManager.LayoutParams params = dlg.getWindow().getAttributes();
                params.height = 720;
                dlg.getWindow().setAttributes(params);

                convertView = view;
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder)convertView.getTag();
            }

            holder.tv.setText(getItem(position));
            return convertView;
        }

        class ViewHolder {
            TextView tv;
        }
    }

    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            monthOfYear = monthOfYear + 1;
            String msg = String.format("%d. %d. %d", year, monthOfYear, dayOfMonth);

            StringBuffer sb = new StringBuffer();
            sb.append(year);
            DecimalFormat df = new DecimalFormat("00");
            sb.append(df.format(monthOfYear));
            sb.append(df.format(dayOfMonth));

        }

    };

    /*private void loadPersonalInfo() {
        Tr_mber_user_call.RequestData requestData = new Tr_mber_user_call.RequestData();
        Tr_login info = UserInfo.getLoginInfo();
        requestData.mber_sn = info.mber_sn;
        getData(getContext(), Tr_mber_user_call.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_mber_user_call) {
                    Tr_mber_user_call data = (Tr_mber_user_call)obj;
                    Tr_login login = UserInfo.getLoginInfo();

                    String birth = CDateUtil.getFormatYYYYMMDD(data.mber_lifyea);
                    mHeightTv.setText(login.mber_height);
//                    mSexWomanRb.setChecked("2".equals(data.mber_sex));

                    DBHelper helper = new DBHelper(getContext());
                    DBHelperWeight WeightDb = helper.getWeightDb();
                    DBHelperWeight.WeightStaticData bottomData = WeightDb.getResultStatic();
                    if(bottomData.getWeight().isEmpty()){
                        mWeightTv.setText(data.mber_bdwgh);
                    }else{
                        mWeightTv.setText(bottomData.getWeight());
                    }
                    int raiseYear = StringUtil.getIntVal(data.sugar_occur_de);

                    mHeightTv.setText(data.mber_height+"cm");
                    mWeightTv.setText(data.mber_bdwgh+"kg");
                    mSugertypeTv.setText("*****당뇨유형 입력해야함");
                    mSugerdateTv.setText(raiseYear == 0 ? "없음" : raiseYear +"년");
                }
            }
        });
    }*/


    private void doInfo() {
        String height = mHeightTv.getText().toString();
        String weight = mWeightTv.getText().toString();
        String gweight = mGoalWeightTv.getText().toString();

        Bundle bundle = new Bundle();
        bundle.putString(LoginFirstInfoFragment2_2.BUNDLE_KEY_HEIGHT, height);
        bundle.putString(LoginFirstInfoFragment2_2.BUNDLE_KEY_WEIGHT, weight);
        bundle.putString(LoginFirstInfoFragment2_2.BUNDLE_KEY_WEIGHT_GOAL, gweight);
        bundle.putBoolean(LoginFirstInfoFragment1_2.BUNDLE_IS_REGIST, mIsRegist);

        replaceFragment(new LoginFirstInfoFragment2_2(), bundle);
    }



//    private void doInfo() {
//
//        String height = mHeightTv.getText().toString();
//        String weight = mWeightTv.getText().toString();
//        String gweight = mGoalWeightTv.getText().toString();
////        if (height.length()==0) height = "170";
////        if (weight.length()==0) weight = "60";
////        if (gweight.length()==0) gweight = "60";
//
//        Tr_login info = UserInfo.getLoginInfo();
//        // 회원수정
//        final Tr_login_add_aft.RequestData requestData = new Tr_login_add_aft.RequestData();
//        requestData.mber_sn =info.mber_sn;
//        requestData.mber_height = height;
//        requestData.mber_bdwgh = weight;
//        requestData.mber_bdwgh_goal = gweight;
//        requestData.sugar_typ = dataVo.getSugartype();
//        requestData.sugar_occur_de = dataVo.getSugardate();
//
//        getData(getContext(), Tr_login_add_aft.class, requestData, new ApiData.IStep() {
//            @Override
//            public void next(Object obj) {
//                if (obj instanceof Tr_login_add_aft) {
//                    Tr_login_add_aft data = (Tr_login_add_aft) obj;
//                    if ("Y".equals(data.reg_yn)) {
//                        // 로그인 싱글턴 정보 갱신 하기.
//                        Tr_login login = UserInfo.getLoginInfo();
//                        login.mber_height = requestData.mber_height;
//                        login.mber_bdwgh = requestData.mber_bdwgh;
//                        login.mber_bdwgh_goal = requestData.mber_bdwgh_goal;
//                        login.sugar_typ = requestData.sugar_typ;
//                        login.sugar_occur_de = requestData.sugar_occur_de;
//                        Define.getInstance().setLoginInfo(login);
//
//                        if("N".equals(data.point_alert_yn)) {
////                            replaceFragment(MainFragment.newInstance());
////                            stratGoogleClient(MainFragment.newInstance());
//                            Intent intent = new Intent(getActivity(), MainActivityMedicare.class);
//                            startActivity(intent);
//                            getActivity().finish();
//                        }
//                        else{
////                            CDialog.showDlg(getContext(),getString(R.string.first_loigin_point, StringUtil.getIntVal(data.point_alert_amt)));
//
//                            CDialog.showDlg(getContext(), getString(R.string.first_loigin_point, StringUtil.getIntVal(data.point_alert_amt)), new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
////                                    replaceFragment(MainFragment.newInstance());
//                                    Intent intent = new Intent(getActivity(), MainActivityMedicare.class);
//                                    startActivity(intent);
//                                    getActivity().finish();
//                                }
//                            });
//                        }
//
//                        /*CDialog.showDlg(getContext(), "정보가 수정 되었습니다.", new CDialog.DismissListener() {
//                            @Override
//                            public void onDissmiss() {
//                                onBackPressed();
//                            }
//                        });*/
//
//                    } else {
//                        CDialog.showDlg(getContext(), "수정에 실패 하였습니다.");
//                    }
//                }
//            }
//        });
//    }

    @Override
    public void onResume() {
        Logger.d(this.getClass().getSimpleName(), "onResume()");
        super.onResume();
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (getActivity() instanceof BaseActivityMedicare) {
            ((BaseActivityMedicare) context).setonKeyBackCatchListener(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        mImm.hideSoftInputFromWindow(mHeightTv.getWindowToken(), 0);
        mImm.hideSoftInputFromWindow(mWeightTv.getWindowToken(), 0);
        mImm.hideSoftInputFromWindow(mGoalWeightTv.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }


    @Override
    public void onCatch() {

    }
}