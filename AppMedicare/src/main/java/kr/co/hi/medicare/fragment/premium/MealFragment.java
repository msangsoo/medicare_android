package kr.co.hi.medicare.fragment.premium;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.Defined;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.BaseActivityMedicare;
import kr.co.hi.medicare.activity.ImgDetailActivity;
import kr.co.hi.medicare.adapter.MealListView_Adapter;
import kr.co.hi.medicare.bean.MealInfo;
import kr.co.hi.medicare.bean.TokItem;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.AlertDialogUtil;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.JsonUtil;
import kr.co.hi.medicare.util.Util;

//import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 영양코칭
 */
public class MealFragment extends BaseFragmentMedi {

    public static final int REQUEST_CODE_EDIT = 177;
    public static final int REQUEST_CODE_DETAIL = 2;

    private UserInfo user;
//    private ObjectMapper mapper;

    private MealInfo mealInfo;


    private String mTotalDate , mToDay;

    private String mType; // GL 갤러리 , TK 영양톡

    private Calendar cal = Calendar.getInstance();
    private Calendar calendar = Calendar.getInstance();

    private LinearLayout mPrevBtn, mNextBtn;
    private ScrollView mTokLay;
    private TextView mDateTv;

    private MealListView_Adapter mAdapter;


    private String mPrev , mNext , mEVAL_YN;

    private TextView mMaintainTv, mPhoteEaTv1 , mRepairTv, mPhoteEaTv2;
    private FrameLayout mPhotoLay1 , mPhotoLay2;
    private ArrayList<TokItem> mItem = new ArrayList<TokItem>();

    private ArrayList<String> mPhotoArr1 = new ArrayList<>();
    private ArrayList<String> mPhotoArr2 = new ArrayList<>();
    private ViewPager mViewPager1 , mViewPager2;
//    private UnderlinePageIndicator mIndicator1 , mIndicator2;

    public static Fragment newInstance() {
        MealFragment fragment = new MealFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meal, container, false);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }


    private void init(View view) {

        int year = calendar.get ( cal.YEAR );
        int month = calendar.get ( cal.MONTH ) + 1 ;
        int day = calendar.get(cal.DATE) ;

        mToDay = String.valueOf(year) + Util.getTwoDateFormat(month) + Util.getTwoDateFormat(day);
        mTotalDate = String.valueOf(year) + Util.getTwoDateFormat(month) + Util.getTwoDateFormat(day);

//        mapper = new ObjectMapper();
        user = new UserInfo(getContext());

        mPrevBtn        =   (LinearLayout) view.findViewById(R.id.prev_btn);
        mNextBtn        =   (LinearLayout) view.findViewById(R.id.next_btn);
        mTokLay         =   (ScrollView) view.findViewById(R.id.tok_lay);
        mDateTv         =   (TextView) view.findViewById(R.id.date_tv);

        mMaintainTv     =   (TextView) view.findViewById(R.id.maintain_tv);
        mRepairTv       =   (TextView) view.findViewById(R.id.repair_tv);
        mPhoteEaTv1     =   (TextView) view.findViewById(R.id.photoea_tv1);
        mPhoteEaTv2     =   (TextView) view.findViewById(R.id.photoea_tv2);

        mPhotoLay1      =   (FrameLayout) view.findViewById(R.id.photo_lay1);
        mPhotoLay2      =   (FrameLayout) view.findViewById(R.id.photo_lay2);

        mViewPager1     =   (ViewPager) view.findViewById(R.id.viewpager1);   // 사진
//        mIndicator1     =   (UnderlinePageIndicator) view.findViewById(R.id.indicator1);
        mViewPager2     =   (ViewPager) view.findViewById(R.id.viewpager2);   // 사진
//        mIndicator2     =   (UnderlinePageIndicator) view.findViewById(R.id.indicator2);


//        mIndicator1.setSelectedColor(ContextCompat.getColor(getContext(), R.color.baseAppColor));
//        mIndicator1.setFades(false);
//        mIndicator2.setSelectedColor(ContextCompat.getColor(getContext(), R.color.baseAppColor));
//        mIndicator2.setFades(false);

        /**
         * 클릭 정의(20190315)
         */
        view.findViewById(R.id.prev_btn).setOnClickListener(mClickListener);
        view.findViewById(R.id.next_btn).setOnClickListener(mClickListener);

        mViewPager1.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                CLog.i("onPageSelected = " + position);
                String imsiEa = String.valueOf(position + 1);
                String imsiTotalEa = String.valueOf(mPhotoArr1.size());
                CLog.i("imsiEa = " + imsiEa);
                CLog.i("imsiTotalEa = " + imsiTotalEa);

                mPhoteEaTv1.setText(imsiEa + "/" + imsiTotalEa);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        mViewPager2.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                CLog.i("onPageSelected = " + position);
                String imsiEa = String.valueOf(position + 1);
                String imsiTotalEa = String.valueOf(mPhotoArr2.size());
                CLog.i("imsiEa = " + imsiEa);
                CLog.i("imsiTotalEa = " + imsiTotalEa);

                mPhoteEaTv2.setText(imsiEa + "/" + imsiTotalEa);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });




        mType = "TK";

//        mListView.setOnItemClickListener(mItemClickListener);

//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                mAdapter.notifyDataSetChanged();
////				mAdapter.notifyDataSetInvalidated();
//            }
//        }, 500);


        requestMealApi(mType);
    }


    /**
     * API 호출
     * @param mtype // ED = 최근게시글 , WB = 주간베스트 , MY = 나의쓴글
     */

    public void requestMealApi(String mtype) {

        CLog.i("mTotalDate -- > " + mTotalDate);
        CLog.i("mtype --> " + mtype);

        JSONObject jObject = new JSONObject();

        String[] monsun = Util.getWeekSunMon(mTotalDate);
        String startday = monsun[0];
        String endday = monsun[6];

        String textstart = Util.getDateSpecialCharacter(getContext() , monsun[0] , 3);
        String textend = Util.getDateSpecialCharacter(getContext(), monsun[6], 3);

        mDateTv.setText(textstart + " ~ " + textend);

        Tr_login login = UserInfo.getLoginInfo();


        try {

                if(BuildConfig.DEBUG){
                    jObject.put("SEQ", "0001013000015");
                }else{
                    jObject.put("SEQ", login.seq);      // 회원일렬번호
                }

//                jObject.put("SEQ", "1234567890123");      // 테스트 회원번호
                jObject.put("SDATE", startday);      // 시작날짜
                jObject.put("EDATE", endday);      // 끝날짜

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String sDocNo = "";
            sDocNo = "DX005";

        //new
        try {
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            client.setTimeout(10000);
            client.addHeader("Accept-Charset", "UTF-8");
            client.addHeader("User-Agent", new WebView(getContext()).getSettings().getUserAgentString());
            jObject.put("DOCNO",sDocNo);
            params.put("strJson", jObject.toString());
            CLog.i("url = " + Defined.COMMUNITY_HTTPS_URL +"?" +params.toString());
            client.post(Defined.COMMUNITY_HTTPS_URL, params, mAsyncHttpHandler);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


//    @Override
    public void request(EServerAPI eServerAPI, Object obj) {
        Record record = new Record();
        record.setEServerAPI(eServerAPI);
        switch (eServerAPI) {
            case API_GET:
                record.setRequestData(obj);
                break;
            case API_POST:
                record.setRequestData(obj);
                break;
        }
        ((BaseActivityMedicare)getActivity()).sendApi(record);
    }

    private AsyncHttpResponseHandler mAsyncHttpHandler = new AsyncHttpResponseHandler() {
        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            CLog.i("onSuccess");

            try {

                int count = headers.length;

                for (int i = 0; i < count; i++) {
                    CLog.v(headers[i].getName() + ": " + headers[i].getValue());
                }

                String response = new String(responseBody, "UTF-8");
                CLog.i("response = " +response);
                JSONObject resultData = null;

                if(response.startsWith("<")){   // api result 문자열 시작값이 "<" 로 시작하는 경우는 xml 폼
                    try {
                        response =  Util.parseXml(response);
                    }catch(Exception e){
                        CLog.e(e.toString());
                    }

                    resultData = new JSONObject(response);

                }else{
                    resultData = new JSONObject(response);
                }

                String resultCode = resultData.getString("DOCNO");
                switch(resultCode){

                    case "DX005":
                        mItem.clear();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            CLog.d(jsonObject.toString());

                            String resultLength = (String) resultData.get("DATA_LENGTH");

                            if (!resultLength.equals("0")) {
                                JSONArray jsonArray = new JSONArray(jsonObject.getString("DATA"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    TokItem item = new TokItem(object.getString("HABITS_GUBUN"),
                                            object.getString("HABITS_MEMO"),
                                            object.getString("HB_IMG1"),
                                            object.getString("HB_IMG2"),
                                            object.getString("HB_IMG3"),
                                            object.getString("LDATE"),
                                            object.getString("NDATE"));

                                    mItem.add(item);
                                    UiTokThread();
                                }
                            } else {
                                UIThread(getActivity(), getString(R.string.confirm), getString(R.string.meal_tok_notdata));

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }


            } catch (Exception e) {
                CLog.e(e.toString());
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            CLog.i("onFailure");
            Handler mHandler = new Handler(Looper.getMainLooper());
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    UIThread(getActivity(), getString(R.string.confirm), getString(R.string.networkexception));
                }
            }, 0);
        }
    };

//    @Override
    public void response(Record record) throws UnsupportedEncodingException {
        String json = new String(record.getData());
        json = json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1);
        Map<String, Object> result = JsonUtil.getJSONParserData(record.getData());
        CLog.e(json);
        if (result.containsKey("DOCNO")) {
            switch ((String) result.get("DOCNO")) {

                case "DX005":
                    mItem.clear();
                    /**
                     *  배열의 길이	AST_LENGTH	INT	4	AST_MASS 배열의 원소 개수
                     *  배열	ADDR_MASS
                     *  1	HABITS_GUBUN	CHAR	1	식습관 구분 (K:유지해야할 식습관 ,R:고쳐야할 식습관)
                     *  2	HABITS_MEMO	VARCHAR	2000	식습관 코칭메모
                     *  3	HB_IMG1	VARCHAR	500	이미지1
                     *  4	HB_IMG2	VARCHAR	500	이미지2
                     *  5	HB_IMG3	VARCHAR	500	이미지3
                     *  5	LDATE	Char	8	코칭 시작일 기준 시작일  ex)20160301
                     *  6	NDATE	Char	8	 코칭 종료일 기준 시작일 ex)20160301
                     */

                    try {
                        JSONObject jsonObject = new JSONObject(json);
                        CLog.d(jsonObject.toString());

                        String resultLength = (String) result.get("AST_LENGTH");

                        if (!resultLength.equals("0")) {
                            JSONArray jsonArray = new JSONArray(jsonObject.getString("ADDR_MASS"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);

                                TokItem item = new TokItem(object.getString("HABITS_GUBUN"),
                                        object.getString("HABITS_MEMO"),
                                        object.getString("HB_IMG1"),
                                        object.getString("HB_IMG2"),
                                        object.getString("HB_IMG3"),
                                        object.getString("LDATE"),
                                        object.getString("NDATE"));

                                mItem.add(item);
                                UiTokThread();
                            }
                        } else {
                            UIThread(getActivity(), getString(R.string.confirm), getString(R.string.meal_tok_notdata));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

//    @Override
    public void networkException(Record record) {
        CLog.e("ERROR STATE : " + record.stateCode);
        UIThread(getActivity(), getString(R.string.confirm), getString(R.string.networkexception));
    }

    private void UIThread(final Activity activity, final String confirm, final String message) {
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertDialogUtil.onAlertDialog(activity, confirm, message);
            }
        }, 0);
    }


    private void UiTokThread() {
        final Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                boolean mPhotoFlag;
                boolean mTypeFlag;

                mTokLay.setVisibility(View.VISIBLE);

                CLog.i("mitem.size --> " + mItem.size());

                // 톡 UI

                if (mItem.size() == 2) {

                    if (mItem.get(0).getHABITS_GUBUN().equals("K")){
                        mMaintainTv.setText(mItem.get(0).getHABITS_MEMO());
                        mRepairTv.setText(mItem.get(1).getHABITS_MEMO());
                        mPrev = mItem.get(0).getLDATE();
                        mNext = mItem.get(0).getNDATE();

                        mPhotoArr1.clear();
                        mPhotoArr2.clear();

                        if (!mItem.get(0).getHB_IMG1().equals("")) {
                            mPhotoArr1.add(mItem.get(0).getHB_IMG1());
                        }
                        if (!mItem.get(0).getHB_IMG2().equals("")) {
                            mPhotoArr1.add(mItem.get(0).getHB_IMG2());
                        }
                        if (!mItem.get(0).getHB_IMG3().equals("")) {
                            mPhotoArr1.add(mItem.get(0).getHB_IMG3());
                        }

                        if (!mItem.get(1).getHB_IMG1().equals("")) {
                            mPhotoArr2.add(mItem.get(1).getHB_IMG1());
                        }
                        if (!mItem.get(1).getHB_IMG2().equals("")) {
                            mPhotoArr2.add(mItem.get(1).getHB_IMG2());
                        }
                        if (!mItem.get(1).getHB_IMG3().equals("")) {
                            mPhotoArr2.add(mItem.get(1).getHB_IMG3());
                        }


                        if (mPhotoArr1.size() == 0){
                            mPhotoLay1.setVisibility(View.GONE);
//                            mIndicator1.setVisibility(View.GONE);
                        }else {
                            mPhotoFlag = true;
                            mPhotoLay1.setVisibility(View.VISIBLE);
//                            mIndicator1.setVisibility(View.VISIBLE);
                            mPhoteEaTv1.setText("1/" + mPhotoArr1.size());
                            mViewPager1.setAdapter(new PagerAdapterClass(getContext(), mPhotoFlag));
//                            mIndicator1.setViewPager(mViewPager1);
                        }

                        if (mPhotoArr2.size() == 0){
                            mPhotoLay2.setVisibility(View.GONE);
//                            mIndicator2.setVisibility(View.GONE);
                        }else {
                            mPhotoFlag = false;
                            mPhotoLay2.setVisibility(View.VISIBLE);
//                            mIndicator2.setVisibility(View.VISIBLE);
                            mPhoteEaTv2.setText("1/" + mPhotoArr2.size());
                            mViewPager2.setAdapter(new PagerAdapterClass(getContext() , mPhotoFlag));
//                            mIndicator2.setViewPager(mViewPager2);
                        }
                    }else {
                        mMaintainTv.setText(mItem.get(1).getHABITS_MEMO());
                        mRepairTv.setText(mItem.get(0).getHABITS_MEMO());
                        mPrev = mItem.get(0).getLDATE();
                        mNext = mItem.get(0).getNDATE();

                        mPhotoArr1.clear();
                        mPhotoArr2.clear();

                        if (!mItem.get(1).getHB_IMG1().equals("")) {
                            mPhotoArr1.add(mItem.get(1).getHB_IMG1());
                        }
                        if (!mItem.get(1).getHB_IMG2().equals("")) {
                            mPhotoArr1.add(mItem.get(1).getHB_IMG2());
                        }
                        if (!mItem.get(1).getHB_IMG3().equals("")) {
                            mPhotoArr1.add(mItem.get(1).getHB_IMG3());
                        }

                        if (!mItem.get(0).getHB_IMG1().equals("")) {
                            mPhotoArr2.add(mItem.get(0).getHB_IMG1());
                        }
                        if (!mItem.get(0).getHB_IMG2().equals("")) {
                            mPhotoArr2.add(mItem.get(0).getHB_IMG2());
                        }
                        if (!mItem.get(0).getHB_IMG3().equals("")) {
                            mPhotoArr2.add(mItem.get(0).getHB_IMG3());
                        }


                        if (mPhotoArr1.size() == 0){
                            mPhotoLay1.setVisibility(View.GONE);
//                            mIndicator1.setVisibility(View.GONE);
                        }else {
                            mPhotoFlag = true;
                            mPhotoLay1.setVisibility(View.VISIBLE);
//                            mIndicator1.setVisibility(View.VISIBLE);
                            mPhoteEaTv1.setText("1/" + mPhotoArr1.size());
                            mViewPager1.setAdapter(new PagerAdapterClass(getContext(), mPhotoFlag));
//                            mIndicator1.setViewPager(mViewPager1);
                        }

                        if (mPhotoArr2.size() == 0){
                            mPhotoLay2.setVisibility(View.GONE);
//                            mIndicator2.setVisibility(View.GONE);
                        }else {
                            mPhotoFlag = false;
                            mPhotoLay2.setVisibility(View.VISIBLE);
//                            mIndicator2.setVisibility(View.VISIBLE);
                            mPhoteEaTv2.setText("1/" + mPhotoArr2.size());
                            mViewPager2.setAdapter(new PagerAdapterClass(getContext() , mPhotoFlag));
//                            mIndicator2.setViewPager(mViewPager2);
                        }
                    }


                }else {

                    if (mItem.get(0).getHABITS_GUBUN().equals("K")){
                        mPhotoFlag = true;
                        mMaintainTv.setText(mItem.get(0).getHABITS_MEMO());
                        mRepairTv.setText(getString(R.string.meal_tok_no));
                        mPhotoLay2.setVisibility(View.GONE);
//                        mIndicator2.setVisibility(View.GONE);
                    }else {

                        mPhotoFlag = false;
                        if(mItem.get(0).getHABITS_MEMO().equals("")){
                            mRepairTv.setText(getString(R.string.meal_tok_no));
                            mPhotoLay2.setVisibility(View.GONE);
                        }else {
                            mRepairTv.setText(mItem.get(0).getHABITS_MEMO());
                        }
                        mMaintainTv.setText(getString(R.string.meal_tok_no));
                        mPhotoLay1.setVisibility(View.GONE);
//                        mIndicator1.setVisibility(View.GONE);
                    }


                    mPrev = mItem.get(0).getLDATE();
                    mNext = mItem.get(0).getNDATE();

                    mPhotoArr1.clear();
                    mPhotoArr2.clear();

                    if (mItem.get(0).getHABITS_GUBUN().equals("K")) {
                        if (!mItem.get(0).getHB_IMG1().equals("")) {
                            mPhotoArr1.add(mItem.get(0).getHB_IMG1());
                        }
                        if (!mItem.get(0).getHB_IMG2().equals("")) {
                            mPhotoArr1.add(mItem.get(0).getHB_IMG2());
                        }
                        if (!mItem.get(0).getHB_IMG3().equals("")) {
                            mPhotoArr1.add(mItem.get(0).getHB_IMG3());
                        }
                    } else {
                        if (!mItem.get(0).getHB_IMG1().equals("")) {
                            mPhotoArr2.add(mItem.get(0).getHB_IMG1());
                        }
                        if (!mItem.get(0).getHB_IMG2().equals("")) {
                            mPhotoArr2.add(mItem.get(0).getHB_IMG2());
                        }
                        if (!mItem.get(0).getHB_IMG3().equals("")) {
                            mPhotoArr2.add(mItem.get(0).getHB_IMG3());
                        }
                    }

                    if (mItem.get(0).getHABITS_GUBUN().equals("K")) {
                        mPhotoLay1.setVisibility(View.VISIBLE);
//                        mIndicator1.setVisibility(View.VISIBLE);
                        mPhoteEaTv1.setText("1/" + mPhotoArr1.size());
                        mViewPager1.setAdapter(new PagerAdapterClass(getContext(), mPhotoFlag));
//                        mIndicator1.setViewPager(mViewPager1);
                    } else {
                        if(mItem.get(0).getHABITS_MEMO().equals("")){
                            mPhotoLay2.setVisibility(View.GONE);
                        }else {
                            mPhotoLay2.setVisibility(View.VISIBLE);
                        }

//                        mIndicator2.setVisibility(View.VISIBLE);
                        mPhoteEaTv2.setText("1/" + mPhotoArr2.size());
                        mViewPager2.setAdapter(new PagerAdapterClass(getContext(), mPhotoFlag));
//                        mIndicator2.setViewPager(mViewPager2);
                    }

                }

                if (mPrev.equals("")) {
                    mPrevBtn.setVisibility(View.INVISIBLE);
                } else {
                    mPrevBtn.setVisibility(View.VISIBLE);
                }

                if (mNext.equals("")){
                    mNextBtn.setVisibility(View.INVISIBLE);
                }else {
                    mNextBtn.setVisibility(View.VISIBLE);
                }

            }

        }, 0);


    }


    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = null;
            SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");
            // GL 갤러리 , TK 영양톡
            switch (v.getId()) {
                // 날짜 이전
                case R.id.prev_btn:
                    mTotalDate = mPrev;
                    requestMealApi(mType);
                    break;

                // 날짜 이후
                case R.id.next_btn:
                    mTotalDate = mNext;
                    requestMealApi(mType);
                    break;
                // 뒤로
                case R.id.activity_actrecord_ImageView_back:
                    onBackPressed();
                    break;

                // 글쓰기
                case R.id.activity_record_ImageView_write:
                    intent = new Intent(getContext(), MealWriteActivity.class);
                    intent.putExtra(BaseActivityMedicare.EXTRA_EDIT, false);
                    startActivityForResult(intent, REQUEST_CODE_EDIT);
                    break;
            }
        }
    };



    public void click_event(View v) {

    }



    /**
     * 해당 메인 사진 뷰페이저 아답터
     */
    private class PagerAdapterClass extends PagerAdapter {

        private LayoutInflater mInflater;
        private boolean mPhotoflag;
        private int mEA;

        public PagerAdapterClass(Context c, boolean flag) {
            super();
            mInflater = LayoutInflater.from(c);
            mPhotoflag = flag;
            if (mPhotoflag) {
                mEA = mPhotoArr1.size();
            } else {
                mEA = mPhotoArr2.size();
            }
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return mEA;
        }

        @Override
        public Object instantiateItem(View pager, final int position) {
            View v = null;

            v = mInflater.inflate(R.layout.img_viewpager, null);
            ImageView pagerImg = (ImageView) v.findViewById(R.id.main_img);
            if (mPhotoflag) {
                Glide.with(getContext()).load(mPhotoArr1.get(position))
                        .apply(new RequestOptions()
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.background_5_506bb0d7)).into(pagerImg);
//                CustomImageLoader.displayImage(getContext(), mPhotoArr1.get(position), pagerImg);
            } else {
                Glide.with(getContext()).load(mPhotoArr2.get(position))
                        .apply(new RequestOptions()
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.background_5_506bb0d7)).into(pagerImg);
//                CustomImageLoader.displayImage(getContext(), mPhotoArr2.get(position), pagerImg);
            }

            pagerImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), ImgDetailActivity.class);
                    if (mPhotoflag) {
                        intent.putExtra(BaseActivityMedicare.EXTRA_PHOTO_LIST, mPhotoArr1);
                    }else {
                        intent.putExtra(BaseActivityMedicare.EXTRA_PHOTO_LIST, mPhotoArr2);
                    }
                    intent.putExtra(BaseActivityMedicare.EXTRA_PHOTO_INDEX, position);
                    startActivity(intent);
                }
            });

//            if (!mPhotoEaFlag) {
//                mPhotoEaTv.setText(position + 1 + "/" + mPhoteArr.size());
//                mPhotoEaFlag = true;
//            }


            ((ViewPager) pager).addView(v, 0);
            return v;
        }

        @Override
        public void destroyItem(View pager, int position, Object view) {
            ((ViewPager) pager).removeView((View) view);
        }

        @Override
        public boolean isViewFromObject(View pager, Object obj) {
            return pager == obj;
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
        }

        @Override
        public void finishUpdate(View arg0) {
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        CLog.i("resultCode : " + resultCode);
        CLog.i("requestCode :" + requestCode);

        if (resultCode != Activity.RESULT_OK) {
            CLog.i("resultCode != RESULT_OK");
            return;

        }
            switch (requestCode) {
                case REQUEST_CODE_EDIT:
                    CLog.i("REQUEST_CODE_EDIT");
                    requestMealApi(mType);
                break;


            }
        }

}
