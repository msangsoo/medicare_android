package kr.co.hi.medicare.sample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.tasks.OnSuccessListener;
import kr.co.hi.medicare.value.model.BandModel;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.googleFitness.noti.FitnessForeGroundService;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.net.hwdata.Tr_mvm_info_input_data;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.SharedPref;

import static java.text.DateFormat.getDateTimeInstance;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Created by MrsWin on 2017-03-01.
 */

public class SampleFragmentGoogleFitness extends BaseFragmentMedi {
    private final String TAG = SampleFragmentGoogleFitness.class.getSimpleName();
    public static String SAMPLE_BACK_DATA = "SAMPLE_BACK_DATA";
    private final int REQUEST_OAUTH_REQUEST_CODE = 115;

    private TextView mStepTv;

    public static Fragment newInstance() {
        SampleFragmentGoogleFitness fragment = new SampleFragmentGoogleFitness();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sample_layout_googlefitness, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // 공통 툴바 커스터마이징 코드
        CommonToolBar toolBar = getToolBar(view);
        toolBar.setTitle("타이틀");
        toolBar.setLeftIcon(R.drawable.arrow_left);

        mStepTv = view.findViewById(R.id.step_count_tv);


        /**
         * 구글 피트니스 걸음 테스트
         */
        view.findViewById(R.id.google_fitness_noti_test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                FitnessForeGroundService.startForegroundService(getActivity());

                FitnessOptions fitnessOptions =
                        FitnessOptions.builder()
                                .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                                .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
                                .build();

                if(!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(getContext()), fitnessOptions)) {
                    GoogleSignIn.requestPermissions(
                            getActivity(),
                            REQUEST_OAUTH_REQUEST_CODE,
                            GoogleSignIn.getLastSignedInAccount(getContext()),
                            fitnessOptions);
                } else {

                    FitnessForeGroundService.startForegroundService(getContext());

//                    Fitness.getRecordingClient(getActivity(),
//                            GoogleSignIn.getLastSignedInAccount(getContext()))
//                            .subscribe(DataType.TYPE_STEP_COUNT_CUMULATIVE)
//                            .addOnCompleteListener(
//                                    new OnCompleteListener<Void>() {
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            if (task.isSuccessful()) {
//                                                Log.i(TAG, "Successfully subscribed!");
//                                                readData();
//                                            } else {
//                                                Log.w(TAG, "There was a problem subscribing.", task.getException());
//                                            }
//                                        }
//                                    });
                }

            }
        });

        view.findViewById(R.id.google_fitness_read_data).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                uploadGoogleStepData();
            }
        });
    }

    private void uploadGoogleStepData() {
        Calendar lastUploadCal = (Calendar) CDateUtil.getCalendar_yyyyMMdd("20190328").clone(); // 마지막 저장된 날자
        Calendar yesterDayCal = (Calendar) Calendar.getInstance().clone();
        yesterDayCal.add(Calendar.DATE, -1);    // 어제

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String yesterDay;// = CDateUtil.getForamtyyyyMMdd(new Date(yesterDayCal.getTimeInMillis()));
        String lastUploadDay;// = CDateUtil.getForamtyyyyMMdd(new Date(lastUploadCal.getTimeInMillis()));

        do {
            yesterDay = sdf.format(new Date(lastUploadCal.getTimeInMillis()));
            lastUploadDay = sdf.format(new Date(yesterDayCal.getTimeInMillis()));
            Log.i(TAG, "yesterDay="+yesterDay+", lastUploadDay="+lastUploadDay+ ", "+yesterDay.compareTo(lastUploadDay));

            final String finalLastUploadDay = lastUploadDay;
            readData(lastUploadCal, new MediNewNetworkHandler() {
                @Override
                public void onSuccess(BaseData responseData) {
                    SharedPref.getInstance().savePreferences(SharedPref.GOOGLE_FITNESS_UPLOAD_DATE2, finalLastUploadDay);
                }

                @Override
                public void onFailure(int statusCode, String response, Throwable error) {

                }
            });

            lastUploadCal.add(Calendar.DATE, 1);    // 하루 증가
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } while (yesterDay.compareTo(lastUploadDay) < 0);
    }

    private void readData(Calendar cal, final MediNewNetworkHandler handler) {
        final DataReadRequest req = queryFitnessDay(cal);

        Fitness.getHistoryClient(getContext(),
                GoogleSignIn.getLastSignedInAccount(getContext()))
                .readData(req)
                .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
                    @Override
                    public void onSuccess(DataReadResponse response) {

                        if (response.getBuckets().size() > 0) {
                            DateFormat dateFormat = getDateTimeInstance();

                            List<BandModel> list = new ArrayList<>();
                            Log.i(TAG, "BucketSize=" + response.getBuckets().size());
                            for (Bucket bucket : response.getBuckets()) {
                                List<DataSet> dataSets = bucket.getDataSets();
                                for (DataSet dataSet : dataSets) {

                                    for (DataPoint dp : dataSet.getDataPoints()) {
                                        Log.i(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(MILLISECONDS)));
                                        Log.i(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(MILLISECONDS)));
                                        for (Field field : dp.getDataType().getFields()) {

                                            int value = dp.getValue(field).asInt();
                                            Log.i(TAG, "readData.value="+value);

                                            BandModel model = new BandModel();
                                            model.setStep(value);
                                            list.add(model);
                                        }
                                    }
                                }
                            }

                            if (list.size() > 0) {
                                uploadGoogleFitStepData(list, handler);
                            }
                        }
                    }
                });

    }

    /**
     * 걸음수 조회 하기
     */
    public DataReadRequest queryFitnessDay(Calendar cal) {
        DataType dataType1 = DataType.TYPE_STEP_COUNT_DELTA;
        DataType dataType2 = DataType.AGGREGATE_STEP_COUNT_DELTA;
//        Calendar cal = (Calendar) Calendar.getInstance().clone();
//        cal.add(Calendar.DATE, day);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
        long startTime = cal.getTimeInMillis();
        String dateStr = CDateUtil.getForamtyyyyMMdd(new Date(startTime));
        Log.i(TAG, "queryFitnessDay.dateStr="+dateStr);


        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
        long endTime = cal.getTimeInMillis();

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(dataType1, dataType2)
                .bucketByTime(1, TimeUnit.HOURS)
//                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, MILLISECONDS)
                .build();


        Log.i(TAG, "GoogleStepSave startDate:"+startTime + " endTime:" + endTime  +  " readRequest:"+readRequest);

        return readRequest;
    }


    /**
     * 구글 걸음 데이터를 서버에 전송 및 Sqlite에 저장하기
     * 일별 조회 일때만 저장하기
     */
    private void uploadGoogleFitStepData(List<BandModel> dataModel, MediNewNetworkHandler handler) {
        Tr_mvm_info_input_data inputData                = new Tr_mvm_info_input_data();
        Tr_login login                                  = UserInfo.getLoginInfo();

        Tr_mvm_info_input_data.RequestData requestData  = new Tr_mvm_info_input_data.RequestData();
        requestData.mber_sn     = login.mber_sn;
        requestData.ast_mass    = inputData.getArray(dataModel, "D");
        MediNewNetworkModule.doApi(getContext(), Tr_mvm_info_input_data.class, requestData, handler);

//        baseFragment.getData(baseFragment.getContext(), inputData.getClass(), requestData, true, new ApiData.IStep() {
//            @Override
//            public void next(Object obj) {
//                if (obj instanceof Tr_mvm_info_input_data) {
//                    Tr_mvm_info_input_data data = (Tr_mvm_info_input_data) obj;
//                    if ("Y".equals(data.reg_yn)) {
//                        registStepByDB(baseFragment, dataModel, true);
//
//                        if (iBluetoothResult != null)
//                            iBluetoothResult.onResult(true);
//                    } else {
//                        CDialog.showDlg(baseFragment.getContext(), baseFragment.getContext().getString(R.string.text_regist_fail));
//                    }
//                }
//            }
//        }, new ApiData.IFailStep() {
//            @Override
//            public void fail() {
//                registStepDB(baseFragment, dataModel, false);
//                if (iBluetoothResult != null)
//                    iBluetoothResult.onResult(false);
//            }
//        });
//        if (mTimeClass.getPeriodType() == TypeDataSet.Period.PERIOD_DAY) {
//            int hour = 0;
//            List<BandModel> dataModelArr = new ArrayList<>();
//
//            for(int key : mGoogleFitDataMap.keySet() ){
//                hour = key;
//                int step = mGoogleFitDataMap.get(key);
//                System.out.println( String.format("key(hour)=%s, value=%s", key, step)+", mDbResultMap.get("+hour+")="+mDbResultMap.get(hour));
//
//                Calendar calendar = Calendar.getInstance();
//                calendar.setTimeInMillis(mTimeClass.getStartTime());
//
//                calendar.set(Calendar.HOUR, hour);
//                calendar.set(Calendar.MINUTE, 0);
//                calendar.set(Calendar.SECOND, 0);
//                calendar.set(Calendar.MILLISECOND, 0);
//
//                BandModel model = new BandModel();
//                model.setStep(step);
//                model.setRegtype("G");  // 구글 피트니스
//                model.setIdx(CDateUtil.getForamtyyMMddHHmmssSS(new Date(calendar.getTimeInMillis())));
//                model.setRegDate(CDateUtil.getForamtyyyyMMddHHmmss(new Date(calendar.getTimeInMillis())));
//
//                Log.i(TAG, "StepGoogle.regDate=" + model.getRegDate() + ", idx=" + model.getIdx() + ", step=" + model.getStep()
//                        +", mDbResultMap.get("+hour+") ="+mDbResultMap.get(hour) );
//
//                // Sqlite에서 조회 했던 결과가 없으면 서버저장 전문에 사용할 데이터와
//                // Sqlite에서에 저장할 데이터를 생성
//                if (mDbResultMap.get(hour) == null) {
//                    if (isToday() && getNowHour() == hour) {
//                        // 현재 시간은 저장하지 않음
//                    } else {
//                        dataModelArr.add(model);
//                    }
//                 }
//            }
//
//            if (dataModelArr.size() <= 0)
//                return;
//
//            DBHelper helper = new DBHelper(mContext);
//            DBHelperStep db = helper.getStepDb();
//            List<BandModel> newModelArr = db.getResultRegistData(dataModelArr);
//
//            if (newModelArr.size() > 0)
//                new DeviceDataUtil().uploadGoogleStepData(mBaseFragment, newModelArr);
//        }
    }



    @Override
    public void onResume() {
        super.onResume();
        // 이전 플래그먼트에서 데이터 받기
        Bundle bundle = getBackData();
        String backString = bundle.getString(SAMPLE_BACK_DATA);
        Logger.i("", "backString=" + backString);
    }

    @Override
    public void onBackPressed() {
        super.finishStep();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // 로그인 성공시
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_OAUTH_REQUEST_CODE) {
//                readData();
            }
        }
    }


}
