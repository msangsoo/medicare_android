package kr.co.hi.medicare.fragment.community.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.community.data.CommunityPostsData;
import kr.co.hi.medicare.fragment.community.holder.ProgressViewHolder;
import kr.co.hi.medicare.tempfunc.TemporaryFunction;


public class CommunityTagAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private View.OnClickListener onClickListener;
    private Context context;
    private ArrayList<CommunityListViewData> data;

    private boolean isMoreLoading = false;
    private String boldTag="";
    private Typeface typeFace_B,typeFace_L;
    private static TagView.OnTagClickListener onTagClickListener;

    private static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView profile,profile_teduri,image,btn_like,btn_comment;
        TextView nick,text,like,comment,regdate;
        TagContainerLayout tag;

        public ViewHolder(View itemView) {
            super(itemView);
            profile = itemView.findViewById(R.id.profile);
            profile_teduri = itemView.findViewById(R.id.profile_teduri);//no
            regdate = itemView.findViewById(R.id.regdate);
            image = itemView.findViewById(R.id.image);
            btn_like = itemView.findViewById(R.id.btn_like);
            btn_comment = itemView.findViewById(R.id.btn_comment);
            nick = itemView.findViewById(R.id.nick);
            text = itemView.findViewById(R.id.text);
            like = itemView.findViewById(R.id.like);
            comment = itemView.findViewById(R.id.comment);
            tag = itemView.findViewById(R.id.tag);
        }

        public void SetView(CommunityListViewData data,Context context,String boldTag,Typeface typeFace_B,Typeface typeFace_L){


            nick.setText(data.NICK);
            regdate.setText(TemporaryFunction.getDateFormat(data.REGDATE)); //파싱필요
            like.setText( data.HCNT );
            comment.setText( data.RCNT );
            text.setText(data.CM_CONTENT); //10글자 이상이면 ...처리

            if(data.CM_IMG1.equals("")){
                image.setVisibility(View.INVISIBLE);
            }else{
                image.setVisibility(View.VISIBLE);
                Glide

                        .with(context)
//                        .applyDefaultRequestOptions(new RequestOptions().override(100,80))
                        .load(CommonFunction.getThumbnail(data.CM_IMG1))
                        .into(image);
                image.setClipToOutline(true);
            }

            CommonFunction.setProfileTeduri(context,data.MBER_GRAD, profile_teduri);
            CommonFunction.setProfile(context, data.PROFILE_PIC, profile);

            if(data.CM_TAG!=null && data.CM_TAG.size()>0){
                tag.setVisibility(View.VISIBLE);
                tag.setTags(data.CM_TAG);
                tag.setTagTextSize(context.getResources().getDimension(R.dimen.px_40));
                tag.setOnTagClickListener(onTagClickListener);

                for(int i=0; i<data.CM_TAG.size();i++){
                    if(tag.getTagText(i).equals(boldTag)){
                        tag.getTagView(i).setTypeface(typeFace_B);
                    }else{
                        tag.getTagView(i).setTypeface(typeFace_L);
                    }
                }
            }else{
                tag.setVisibility(View.GONE);
            }




        }
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position) != null ? LoadMoreListener.VIEW_ITEM : LoadMoreListener.VIEW_PROG;
    }


    public CommunityTagAdapter(Context context, View.OnClickListener onClickListener,String bolTag,TagView.OnTagClickListener onTagClickListener) {
        this.context = context;
        this.onClickListener = onClickListener;
        data = new ArrayList<>();
        this.boldTag = bolTag;
        typeFace_B = ResourcesCompat.getFont(context,R.font.notosanskr_bold);
        typeFace_L = ResourcesCompat.getFont(context,R.font.notosanskr_light);
        this.onTagClickListener = onTagClickListener;

    }
    public void setBoldTag(String boldTag){
        this.boldTag = boldTag;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == LoadMoreListener.VIEW_ITEM) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_community_posts, parent, false));
        }else{
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_progress, parent, false));
        }
    }

    public void setMore(boolean isMore) {
        this.isMoreLoading = isMore;
    }

    public boolean getMore() {
        return isMoreLoading;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ViewHolder){
            if (data.size() > 0) {
                if (position < data.size()) {
                    //뷰홀더의 자원을 초기화//
                    final ViewHolder commViewHolder = (ViewHolder) holder;
                    commViewHolder.SetView(data.get(position),context,boldTag,typeFace_B,typeFace_L);
                    commViewHolder.profile.setTag(R.id.comm_user, position);
                    commViewHolder.profile.setOnClickListener(onClickListener);
                    commViewHolder.text.setTag(R.id.comm_main_text, position);
                    commViewHolder.text.setOnClickListener(onClickListener);


                    return;
                }

//            position -= humanData.getHumanDataList().size();
//              position -= data.getSize();
            }
            throw new IllegalArgumentException("invalid position");
        }


    }

    @Override
    public int getItemCount() {
        if (data==null) {
            return 0;
        }
        return data.size();
    }


    public void delItemAll() {
        if(data!=null){
            this.data.clear();
            notifyDataSetChanged();
        }
    }

    public void addAllItem(List<CommunityListViewData> data) {
        if(this.data==null)
            this.data = new ArrayList<>();

        if(data!=null){
            this.data.clear();
            this.data.addAll(data);
            notifyDataSetChanged();
        }
    }

    public void addItemMore(List<CommunityListViewData> data){
        int sizeInit = this.data.size();
        this.data.addAll(data);
        notifyItemRangeChanged(sizeInit, this.data.size());
    }


    public void addItem(CommunityListViewData data) {
        if(this.data==null)
            this.data = new ArrayList<>();

        if(data!=null){
            ArrayList<CommunityListViewData> datas = new ArrayList<>();
            datas.add(data);
            datas.addAll(this.data);
            this.data.clear();
            this.data = datas;
            notifyDataSetChanged();
        }
    }

    public void setProgressMore(final boolean isProgress) {
        if (isProgress) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    data.add(null);
                    notifyItemInserted(data.size() - 1);
                }
            });
        } else {

            if(data.size()>0) {
                data.remove(data.size() - 1);
                notifyItemRemoved(data.size());
            }
        }
    }

    public CommunityListViewData getData(int position){

        return data.get(position);
    }


    public void updateData(CommunityListViewData comm_data) {
        if(comm_data!=null){
            for(int i=0; i< data.size();i++){
                if(data.get(i).CM_SEQ.equals(comm_data.CM_SEQ)){
                    data.get(i).RNUM = comm_data.RNUM;
                    data.get(i).RCNT = comm_data.RCNT;
                    data.get(i).HCNT = comm_data.HCNT;
                    data.get(i).REGDATE = comm_data.REGDATE;
                    data.get(i).CM_TITLE = comm_data.CM_TITLE;
                    data.get(i).NICK = comm_data.NICK;
                    data.get(i).CM_SEQ = comm_data.CM_SEQ;
                    data.get(i).TPAGE = comm_data.TPAGE;
                    data.get(i).CM_IMG1 = comm_data.CM_IMG1;
                    data.get(i).PROFILE_PIC = comm_data.PROFILE_PIC;
                    data.get(i).CM_CONTENT = comm_data.CM_CONTENT;
                    data.get(i).CM_TAG = comm_data.CM_TAG;
                    data.get(i).MYHEART = comm_data.MYHEART;
                    data.get(i).ISDETAIL = comm_data.ISDETAIL;
                    data.get(i).MBER_SN = comm_data.MBER_SN;
                    data.get(i).CM_GUBUN = comm_data.CM_GUBUN;
                    data.get(i).MBER_GRAD = comm_data.MBER_GRAD;
                    data.get(i).CM_MEAL = comm_data.CM_MEAL;
                    notifyItemChanged(i);
                    break;
                }
            }
        }
    }

    public void deleteItem(String CM_SEQ) {
        for(int i=0; i<data.size();i++){
            if(data.get(i).CM_SEQ.equals(CM_SEQ)){
                data.remove(i);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i,data.size());
                break;
            }
        }
    }
}
