package kr.co.hi.medicare.googleFitness;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInOptionsExtension;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import kr.co.hi.medicare.net.data.MediNewNetworkHandler;


/**
 * Created by MrsWin on 2017-05-02.
 */

public class GoogleFitInstance {
    private final String TAG = GoogleFitInstance.class.getSimpleName();

    public static final int REQUEST_OAUTH_REQUEST_CODE = 0x1001;
    public static final int GOOGLE_PERMISSION_REQ_CODE = 0x567;

    private OnDataPointListener mListener;
    private static GoogleFitInstance instance;

    public static GoogleFitInstance getInstance() {
        if (instance == null)
            instance = new GoogleFitInstance();
        return instance;
    }

    /**
     * 구글 피트니스 인증 여부
     * @param context
     * @return
     */
    public static boolean isFitnessAuth(Context context) {
        return GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(context), getFitnessOptions());
    }

    /**
     * 구글피트니스 계정 등록
     * @param activity
     */
    public static void requestGoogleFitnessAuth(Activity activity) {
        GoogleSignIn.requestPermissions(
                activity,
                GOOGLE_PERMISSION_REQ_CODE,
                GoogleSignIn.getLastSignedInAccount(activity),
                getFitnessOptions());
    }

    public static FitnessOptions getFitnessOptions() {
        FitnessOptions fitnessOptions = FitnessOptions.builder()
                .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
                .addDataType(DataType.TYPE_LOCATION_SAMPLE)
                .build();
        return fitnessOptions;
    }


    /**
     * 구글 피트니스 센서API 리스너 해지
     * @param context
     * @param listener
     */
    public static void unregisterSensorFitnessDataListener(Context context, OnDataPointListener listener) {
        if (GoogleFitInstance.isFitnessAuth(context) == false)
            return;

        GoogleSignInOptionsExtension fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                        .build();

        GoogleSignInAccount gsa = GoogleSignIn.getAccountForExtension(context, fitnessOptions);
//        SensorRequest sensorRequest = new SensorRequest.Builder()
//                .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
//                .setSamplingRate(3, TimeUnit.MILLISECONDS)  // sample once per minute
//                .build();

        Fitness.getSensorsClient(context, gsa).remove(listener);
    }

    /**
     * 구글 인증 SignOut
     * @param context
     * @param handler
     */
    public static void signOutGoogleClient(Context context, final MediNewNetworkHandler handler) {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
//                .requestIdToken("<web client id>")
                .build();

        GoogleSignIn.getClient(context, gso).signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (handler != null)
                    handler.onSuccess(null);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (handler != null)
                    handler.onFailure(0, null, e);
            }
        });
    }

    /**
     * 오늘 걸음수
     */
    public void readDataTodayStep(Context context) {
        Fitness.getHistoryClient(context, GoogleSignIn.getLastSignedInAccount(context))
                .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
                .addOnSuccessListener(
                        new OnSuccessListener<DataSet>() {
                            @Override
                            public void onSuccess(DataSet dataSet) {
                                long total =
                                        dataSet.isEmpty()
                                                ? 0
                                                : dataSet.getDataPoints().get(0).getValue(Field.FIELD_STEPS).asInt();
                                Log.i(TAG, "Total steps: " + total);
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "There was a problem getting the step count.", e);
                            }
                        });
    }






    /** Finds available data sources and attempts to register on a specific {@link DataType}. */
//    private void findFitnessSensorDataSources(final Context context) {
//        // [START find_data_sources]
//        // Note: Fitness.SensorsApi.findDataSources() requires the ACCESS_FINE_LOCATION permission.
//        Fitness.getSensorsClient(context, GoogleSignIn.getLastSignedInAccount(context))
//                .findDataSources(
//                        new DataSourcesRequest.Builder()
//                                .setDataTypes(DataType.TYPE_LOCATION_SAMPLE)
//                                .setDataSourceTypes(DataSource.TYPE_RAW)
//                                .build())
//                .addOnSuccessListener(
//                        new OnSuccessListener<List<DataSource>>() {
//                            @Override
//                            public void onSuccess(List<DataSource> dataSources) {
//                                for (DataSource dataSource : dataSources) {
//                                    Log.i(TAG, "Data source found: " + dataSource.toString());
//                                    Log.i(TAG, "Data Source type: " + dataSource.getDataType().getName());
//
//                                    // Let's register a listener to receive Activity data!
//                                    if (dataSource.getDataType().equals(DataType.TYPE_LOCATION_SAMPLE)
//                                            && mListener == null) {
//                                        Log.i(TAG, "Data source for LOCATION_SAMPLE found!  Registering.");
//                                        registerFitnessDataListener(context, dataSource, DataType.TYPE_LOCATION_SAMPLE);
//                                    }
//                                }
//                            }
//                        })
//                .addOnFailureListener(
//                        new OnFailureListener() {
//                            @Override
//                            public void onFailure(@NonNull Exception e) {
//                                Log.e(TAG, "failed", e);
//                            }
//                        });
//        // [END find_data_sources]
//    }

//    private void registerFitnessDataListener(Context context, DataSource dataSource, DataType dataType) {
//        // [START register_data_listener]
//        mListener =
//                new OnDataPointListener() {
//                    @Override
//                    public void onDataPoint(DataPoint dataPoint) {
//                        for (Field field : dataPoint.getDataType().getFields()) {
//                            Value val = dataPoint.getValue(field);
//                            Log.i(TAG, "Detected DataPoint field: " + field.getName());
//                            Log.i(TAG, "Detected DataPoint value: " + val);
//                        }
//                    }
//                };
//
//        Fitness.getSensorsClient(context, GoogleSignIn.getLastSignedInAccount(context))
//                .add(
//                        new SensorRequest.Builder()
//                                .setDataSource(dataSource) // Optional but recommended for custom data sets.
//                                .setDataType(dataType) // Can't be omitted.
//                                .setSamplingRate(10, TimeUnit.SECONDS)
//                                .build(),
//                        mListener)
//                .addOnCompleteListener(
//                        new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
//                                if (task.isSuccessful()) {
//                                    Log.i(TAG, "Listener registered!");
//                                } else {
//                                    Log.e(TAG, "Listener not registered.", task.getException());
//                                }
//                            }
//                        });
//        // [END register_data_listener]
//    }


//    public void googleSignInPermisson(final  Activity context,  final ApiData.IStep callBack) {
//        FitnessOptions fitnessOptions =
//                FitnessOptions.builder()
//                        .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
//                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
//
//                        .addDataType(DataType.TYPE_STEP_COUNT_CADENCE)
//                        .addDataType(DataType.TYPE_CALORIES_EXPENDED)
//                        .addDataType(DataType.AGGREGATE_CALORIES_EXPENDED)
//                        .build();
//
//        // Check if the user has permissions to talk to Fitness APIs, otherwise authenticate the
//        // user and request required permissions.
//        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(context), fitnessOptions)) {
//            GoogleSignIn.requestPermissions(
//                    context,
//                    REQUEST_OAUTH_REQUEST_CODE,
//                    GoogleSignIn.getLastSignedInAccount(context),
//                    fitnessOptions);
//        } else {
//            subscribe(context, callBack);
//        }
//    }

//    public void findFitnessSensorDataSources(Activity activity, final GoogleApiClient client, final ApiData.IStep callBack) {
//        // [START find_data_sources]
//        // Note: Fitness.SensorsApi.findDataSources() requires the ACCESS_FINE_LOCATION permission.
//        boolean isRegist = SharedPref.getInstance().getPreferences(SharedPref.IS_REGIST_GOOGLE_FITNESS, false);
//
//        if (isRegist) {
//            callBack.next(client);
//        } else {
//            FitnessOptions fitnessOptions =
//                    FitnessOptions.builder()
//                            .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
//                            .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
//
//                            .addDataType(DataType.TYPE_STEP_COUNT_CADENCE)
//                            .addDataType(DataType.TYPE_CALORIES_EXPENDED)
//                            .addDataType(DataType.AGGREGATE_CALORIES_EXPENDED)
//                            .build();
//
//            if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(client.getContext()), fitnessOptions)) {
//                GoogleSignIn.requestPermissions(
//                        activity,
//                        REQUEST_OAUTH_REQUEST_CODE,
//                        GoogleSignIn.getLastSignedInAccount(client.getContext()),
//                        fitnessOptions);
//            } else {
//                subscribe(client.getContext(), callBack);
//            }
//
////            Fitness.SensorsApi.findDataSources(client, new DataSourcesRequest.Builder()
////                    // At least one datatype must be specified.
////                    .setDataTypes(DataType.TYPE_STEP_COUNT_CADENCE)
////                    .setDataTypes(DataType.TYPE_STEP_COUNT_CUMULATIVE)
////                    .setDataTypes(DataType.TYPE_STEP_COUNT_DELTA)
////                    .setDataTypes(DataType.TYPE_CALORIES_EXPENDED)
////                    .setDataTypes(DataType.AGGREGATE_CALORIES_EXPENDED)
////                    // Can specify whether data type is raw or derived.
////                    .setDataSourceTypes(DataSource.TYPE_RAW)
////                    .build())
////                    .setResultCallback(new ResultCallback<DataSourcesResult>() {
////                        @Override
////                        public void onResult(DataSourcesResult dataSourcesResult) {
////                            Log.i(TAG, "Result: " + dataSourcesResult.getStatus().toString());
////                            for (DataSource dataSource : dataSourcesResult.getDataSources()) {
////                                Log.i(TAG, "Data Source found: " + dataSource.toString());
////                                Log.i(TAG, "Data Source type: " + dataSource.getDataType().getName());
////
////                                //Let's register a listener to receive Activity data!
////                                if (dataSource.getDataType().equals(DataType.TYPE_STEP_COUNT_DELTA) && mListener == null) {
////                                    Log.i(TAG, "Data source for LOCATION_SAMPLE found!  Registering.");
////                                }
////                            }
////
////                            registerFitnessDataListener();
////                            subscribeRecordingApi(client);
////
////                            SharedPref.getInstance().savePreferences(SharedPref.IS_REGIST_GOOGLE_FITNESS, true);
////
////                            callBack.next(null);
////
////                        }
////                    });
//        }
//    }

//    public void subscribe(Context context, final ApiData.IStep callBack) {
//        // To create a subscription, invoke the Recording API. As soon as the subscription is
//        // active, fitness data will start recording.
//        Fitness.getRecordingClient(context, GoogleSignIn.getLastSignedInAccount(context))
//                .subscribe(DataType.TYPE_STEP_COUNT_CUMULATIVE)
//                .addOnSuccessListener(new OnSuccessListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//                        Log.i(TAG, "subscribe.onSuccess");
//                        SharedPref.getInstance().savePreferences(SharedPref.IS_REGIST_GOOGLE_FITNESS, true);
//                        callBack.next(null);
//                    }
//                })
//                .addOnCompleteListener(
//                        new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
//                                Log.i(TAG, "subscribe.onComplete task.isSuccessful()="+task.isSuccessful());
//                                if (task.isSuccessful()) {
//                                    Log.i(TAG, "Successfully subscribed!");
//                                    SharedPref.getInstance().savePreferences(SharedPref.IS_REGIST_GOOGLE_FITNESS, true);
//                                } else {
//                                    Log.w(TAG, "There was a problem subscribing.", task.getException());
//                                }
//                                callBack.next(null);
//                            }
//                        })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Log.i(TAG, "subscribe.onFailure");
//                        callBack.next(null);
//                    }
//                });
//    }

//    private void dumpDataSet(DataSet dataSet) {
//        Logger.i(TAG, "Data returned for Data type: " + dataSet.getDataType().getName());
//        for (DataPoint dp : dataSet.getDataPoints()) {
//            DateFormat dateFormat = getTimeInstance();
//            Logger.i(TAG, "Data point:");
//            Logger.i(TAG, "\tType: " + dp.getDataType().getName());
//            Logger.i(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
//            Logger.i(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
//            for(Field field : dp.getDataType().getFields()) {
//                Logger.i(TAG, "\tField: " + field.getName() +
//                        " Value: " + dp.getValue(field));
//            }
//        }
//    }
//
//    private void dumpSession(Session session) {
//        DateFormat dateFormat = getTimeInstance();
//        Logger.i(TAG, "Data returned for Session: " + session.getName()
//                + "\n\tDescription: " + session.getDescription()
//                + "\n\tStart: " + dateFormat.format(session.getStartTime(TimeUnit.MILLISECONDS))
//                + "\n\tEnd: " + dateFormat.format(session.getEndTime(TimeUnit.MILLISECONDS)));
//    }

//    /**
//     * register_data_listener
//     */
//    private void registerFitnessDataListener() {
//        mListener = new OnDataPointListener() {
//            @Override
//            public void onDataPoint(DataPoint dataPoint) {
//                for (Field field : dataPoint.getDataType().getFields()) {
//                    Value val = dataPoint.getValue(field);
//                    Log.i(TAG, "Detected DataPoint field: " + field.getName());
//                    Log.i(TAG, "Detected DataPoint value: " + val);
//                }
//            }
//        };
//    }


//    public void stopClient(BaseFragmentMedi fragment) {
//        if (mClient != null && mClient.isConnected()) {
//            mClient.stopAutoManage(fragment.getActivity());
//            mClient.disconnect();
//        }
//        fragment.hideProgress();
//    }

//    public void buildApiClient(final FragmentActivity activity, final IConnectResult iConnectResult) {
//        if (mClient != null) {
//            Logger.i(TAG, "buildFitnessClientApis.mClient isCreated="+mClient+", success");
//            iConnectResult.success(mClient);
//            return;
//        }
//
//        mClient = new GoogleApiClient.Builder(activity)
//                .addApi(Fitness.RECORDING_API)
//                .addApi(Fitness.HISTORY_API)
//                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
////                .addApi(Fitness.SENSORS_API)
////                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ))
////                .addApi(Fitness.HISTORY_API)
////                .addApi(Fitness.RECORDING_API)
////                .addApi(Fitness.SESSIONS_API)
////                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ_WRITE))
////                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
//                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
//                    @Override
//                    public void onConnected(Bundle bundle) {
//                        Logger.i(TAG, "buildFitnessClientApis Connected!!! "+mClient.isConnected());
//                        iConnectResult.success(mClient);
//                    }
//
//                    @Override
//                    public void onConnectionSuspended(int i) {
//                        if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST) {
//                            Logger.i(TAG, "buildFitnessClientApis Connection lost.  Cause: Network Lost.");
//                            CDialog.showDlg(activity, "Google 서비스 연결에 실패 했습니다.(NETWORK_LOST)");
//
//                        } else if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED) {
//                            Logger.i(TAG, "buildFitnessClientApis Connection lost.  Reason: Service Disconnected");
//                            CDialog.showDlg(activity, "Google 서비스 연결에 실패 했습니다.(Disconnected)");
//                        }
//
//                        iConnectResult.fail();
//                    }
//                })
//                .enableAutoManage(activity, 0, new GoogleApiClient.OnConnectionFailedListener() {
//                    @Override
//                    public void onConnectionFailed(ConnectionResult result) {
//
//                        Logger.e(TAG, "buildFitnessClientApis connection failed. Cause: " + result.toString());
//                        Logger.e(TAG, "buildFitnessClientApis connection failed. getErrorCode: " + result.getErrorCode());
//                        Logger.e(TAG, "buildFitnessClientApis connection failed. getErrorMessage: " + result.getErrorMessage());
//                        Logger.e(TAG, "buildFitnessClientApis connection failed. getResolution: " + result.getResolution());
//                        Logger.e(TAG, "buildFitnessClientApis connection failed. Cause: " + result.isSuccess());
//
//                        if (ConnectionResult.CANCELED == result.getErrorCode()) {
//                            CDialog.showDlg(activity, "계정인증 후 이용 가능합니다.", new CDialog.DismissListener() {
//                                @Override
//                                public void onDissmiss() {
//                                    SharedPref.getInstance().savePreferences(SharedPref.STEP_DATA_SOURCE_TYPE, -1);
////                                    restartMainActivity();
//                                }
//                            });
//                        } else {
//                            CDialog.showDlg(activity, "Google 서비스 연결에 실패 했습니다.(" + result.toString() + ")");
//                        }
//                        iConnectResult.fail();
//                    }
//                })
//                .build();
//    }
//
//    private GoogleApiClient mClient;
//    public void buildApiClient(final BaseFragmentMedi baseFragment, final IConnectResult iConnectResult) {
//        if (mClient != null && mClient.isConnected()) {
//            Logger.i(TAG, "buildFitnessClientApis.mClient isCreated="+mClient+", success");
//            iConnectResult.success(mClient);
//            return;
//        } else {
//            if (BuildConfig.DEBUG) {
//                if (mClient == null) {
//                    Logger.i(TAG, "buildApiClient.isConnect mClient is Null");
//                } else {
//                    Logger.i(TAG, "buildApiClient.isConnected="+mClient.isConnected());
//                }
//            }
//
//            if (mClient != null) {
//                mClient.connect();
//                iConnectResult.success(mClient);
//                return;
//            }
//
//            try {
//                mClient = new GoogleApiClient.Builder(baseFragment.getContext())
//                        .addApi(Fitness.RECORDING_API)
//                        .addApi(Fitness.HISTORY_API)
//                        .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
////                        .addApi(Fitness.SENSORS_API)
////                        .addScope(new Scope(Scopes.FITNESS_LOCATION_READ))
////                        .addApi(Fitness.HISTORY_API)
////                        .addApi(Fitness.RECORDING_API)
////                        .addApi(Fitness.SESSIONS_API)
////                        .addScope(new Scope(Scopes.FITNESS_LOCATION_READ_WRITE))
////                        .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
//                        .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
//                            @Override
//                            public void onConnected(Bundle bundle) {
//                                baseFragment.hideProgress();
//                                Logger.i(TAG, "buildFitnessClientApis Connected!!! "+mClient.isConnected());
//                                iConnectResult.success(mClient);
//                            }
//
//                            @Override
//                            public void onConnectionSuspended(int i) {
//                                baseFragment.hideProgress();
//                                if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST) {
//                                    Logger.i(TAG, "buildFitnessClientApis Connection lost.  Cause: Network Lost.");
//                                    CDialog.showDlg(baseFragment.getContext(), "Google 서비스 연결에 실패 했습니다.(NETWORK_LOST)");
//
//                                } else if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED) {
//                                    Logger.i(TAG, "buildFitnessClientApis Connection lost.  Reason: Service Disconnected");
//                                    CDialog.showDlg(baseFragment.getContext(), "Google 서비스 연결에 실패 했습니다.(Disconnected)");
//                                }
//
//                                iConnectResult.fail();
//                            }
//                        })
//                        .enableAutoManage(baseFragment.getActivity(), 0, new GoogleApiClient.OnConnectionFailedListener() {
//                            @Override
//                            public void onConnectionFailed(ConnectionResult result) {
//                                baseFragment.hideProgress();
//
//                                Logger.e(TAG, "buildFitnessClientApis connection failed. Cause: " + result.toString());
//                                Logger.e(TAG, "buildFitnessClientApis connection failed. getErrorCode: " + result.getErrorCode());
//                                Logger.e(TAG, "buildFitnessClientApis connection failed. getErrorMessage: " + result.getErrorMessage());
//                                Logger.e(TAG, "buildFitnessClientApis connection failed. getResolution: " + result.getResolution());
//                                Logger.e(TAG, "buildFitnessClientApis connection failed. Cause: " + result.isSuccess());
//
//                                if (ConnectionResult.CANCELED == result.getErrorCode()) {
//                                    CDialog.showDlg(baseFragment.getContext(), "계정인증 후 이용 가능합니다.", new CDialog.DismissListener() {
//                                        @Override
//                                        public void onDissmiss() {
//                                            SharedPref.getInstance().savePreferences(SharedPref.STEP_DATA_SOURCE_TYPE, -1);
////                                    restartMainActivity();
//                                        }
//                                    });
//                                } else {
//                                    CDialog.showDlg(baseFragment.getContext(), "Google 서비스 연결에 실패 했습니다.(" + result.toString() + ")");
//                                }
//                                iConnectResult.fail();
//                            }
//                        })
//                        .build();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }

    /**
     * 총걸음수, 칼로리 구하기
     */
//    public void totalReqDataTask(final GoogleApiClient client, DataReadRequest readRequest, final ApiData.IStep callBack) {
//        new TotalValReqDataTask(client, readRequest, callBack).execute();
//    }
//    private class TotalValReqDataTask extends AsyncTask<Object, Object, Long> {
//        private GoogleApiClient client = null;
//        private DataReadRequest readRequest;
//        private ApiData.IStep callBack;
//
//        public TotalValReqDataTask(final GoogleApiClient client, DataReadRequest readRequest, ApiData.IStep callBack) {
//            this.client = client;
//            this.readRequest = readRequest;
//            this.callBack = callBack;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        protected Long doInBackground(Object... params) {
//            long total = 0;
//
//            PendingResult<DataReadResult> result = Fitness.HistoryApi.readData(client, readRequest);
//            DataReadResult totalResult = result.await(30, TimeUnit.SECONDS);
//            if (totalResult.getStatus().isSuccess()) {
//                List<DataSet> totalSet = totalResult.getDataSets();
//                for (DataSet dataSet : totalSet) {
//                    Logger.i(TAG, "dataSet="+dataSet);
//                }
//
//            } else {
//                Logger.w(TAG, "There was a problem getting the step count.");
//            }
//
//            return total;
//        }
//
//        @Override
//        protected void onPostExecute(Long aLong) {
//            super.onPostExecute(aLong);
//            if (callBack != null)
//                callBack.next(aLong);
//        }
//    }

//    public void onStop(FragmentActivity activity) {
//        if (mClient != null) {
//            Logger.i(TAG, "GoogleFitness onStop()");
//            mClient.stopAutoManage(activity);
//            mClient.disconnect();
//        }
//    }
}
