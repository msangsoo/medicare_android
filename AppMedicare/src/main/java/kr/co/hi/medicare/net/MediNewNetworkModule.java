package kr.co.hi.medicare.net;

import android.content.Context;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;

import com.google.gson.Gson;

import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.net.hwNet.BaseData;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Constructor;

import cz.msebera.android.httpclient.Header;
import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.activity.BaseActivityMedicare;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.utilhw.JsonLogPrint;
import kr.co.hi.medicare.utilhw.NetworkUtil;

public class MediNewNetworkModule {
    private final static String TAG = MediNewNetworkModule.class.getSimpleName();

    public static void doApi(Context context, final Class<? extends BaseData> trCls, Object requestData, final MediNewNetworkHandler handler ) {
        doApi(context, trCls, requestData, true,false, handler);
    }

    public static void doApi(Context context, final Class<? extends BaseData> trCls, Object requestData, boolean isShowProgress, final MediNewNetworkHandler handler ) {
        BaseData tr = createTrClass(trCls, context);
        doApi(context, tr, requestData, isShowProgress, false, handler);
    }

    public static void doApi(Context context, final BaseData tr, Object requestData, boolean isShowProgress, final MediNewNetworkHandler handler ) {
        doApi(context, tr, requestData, isShowProgress, false, handler);
    }

    public static void doApi(Context context, final Class<? extends BaseData> trCls, Object requestData, boolean isShowProgress, boolean Agent, final MediNewNetworkHandler handler ) {
        BaseData tr = createTrClass(trCls, context);
        doApi(context, tr, requestData, isShowProgress, Agent, handler);
    }

    public static void doApi(final Context context, final BaseData tr, Object requestData, final MediNewNetworkHandler handler ) {
        doApi(context, tr, requestData, true,false, handler);
    }


    public static void doApi(final Context context, final BaseData tr, final Object requestData, boolean isShowProgress, boolean Agent, final MediNewNetworkHandler handler ) {
        final String trName = tr.getClass().getSimpleName();
        if (NetworkUtil.getConnectivityStatus(context) == false) {
            CDialog.showDlg(context, "네트워크 연결 상태를 확인해주세요.");
            handler.onFailure(1, "네트워크 연결 상태를 확인해주세요.", null);
            return;
        }

        if (isShowProgress) {
            if (context instanceof BaseActivityMedicare) {
                Log.i(TAG, "doApi progress showProgress");
                ((BaseActivityMedicare)context).showProgress();
            } else {
                Log.e(TAG, "doApi progress Context="+context);
            }
        }



        try {
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            client.setTimeout(10000);
            client.addHeader("Accept-Charset", "UTF-8");
            if(!Agent){
                client.addHeader("User-Agent", new WebView(context).getSettings().getUserAgentString());
            }

            if (tr != null)
                tr.setContext(context);

            final JSONObject reqObj = tr.makeJson(requestData);
//            final String trName = tr.getClass().getSimpleName();
            if (reqObj != null) {
                String paramString = reqObj.toString();
                params.put(tr.json_obj_name, paramString);
                Log.i(TAG, "┌-------------------------------- Send::"+trName+" --------------------------------┐");
                JsonLogPrint.printJson(trName, paramString);
                Log.i(TAG, "└-------------------------------- Send::"+trName+" --------------------------------┘");
            } else {
                Log.e(TAG, "############ request data is null "+tr.getClass().getName());
            }

            Log.i(TAG, "url = " + tr.conn_url + "?" + params.toString());
            client.post(tr.conn_url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    if (context instanceof BaseActivityMedicare) {
                        ((BaseActivityMedicare)context).hideProgress();
                    }

                    String response = null;
                    try {
                         response = new String(responseBody);
                         // json 불필요 내용 제거
                        int idx = response.indexOf("{");
                        if (idx != -1)
                            response = response.substring(idx);
                        response = response.replaceAll("</string>", "");
                        Log.i(TAG, "┌-------------------------------- Receive::"+trName+" --------------------------------┐");
//                        Log.i(TAG, "response=\n"+response);
                        JsonLogPrint.printJson(trName, response);
                        Log.i(TAG, "└-------------------------------- Receive::"+trName+" --------------------------------┘");
                        Gson gson = new Gson();
                        BaseData resTr = gson.fromJson(response, tr.getClass());
                        resTr.setContext(context);
                        resTr.setJsonString(context, response);
                        if (handler != null)
                            handler.onSuccess(resTr);
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (handler != null)
                            handler.onFailure(statusCode, response, e);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    if (context instanceof BaseActivityMedicare) {
                        ((BaseActivityMedicare)context).hideProgress();
                    }

                    String response = null;
                    if (responseBody != null) {
                         response = new String(responseBody);
                    }
                    Log.e(TAG, "############## onFailure::"+trName +" ###############");
                    if (tr != null && requestData != null)
                        try {
                            JsonLogPrint.printJson(tr.getClass().getSimpleName(), tr.makeJson(requestData).toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    error.printStackTrace();
                    if (handler != null)
                        handler.onFailure(statusCode, response, error);
                }
            });

        } catch (Exception e) {
            if (context instanceof BaseActivityMedicare)
                ((BaseActivityMedicare)context).hideProgress();

            if (BuildConfig.DEBUG)
                Toast.makeText(context, "데이터 수신에 실패 했습니다.["+trName+"]", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(context, "데이터 수신에 실패 했습니다.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private static BaseData createTrClass(Class<? extends BaseData> cls, Context context) {
        BaseData trClass = null;
        try {
            Constructor<? extends BaseData> co = cls.getConstructor();
            trClass = co.newInstance();
        } catch (Exception e) {
            try {
                Constructor<? extends BaseData> co = cls.getConstructor(Context.class);
                trClass = co.newInstance(context);
            } catch (Exception e2) {
                Log.e(TAG, "createTrClass", e2);
            }
        }

        return trClass;
    }


    /**
     * 아이디 패스워드 없이 사용 재로그인
     * @param context
     * @param handler
     */
    public static void doReLoginMediCare(final Context context, final MediNewNetworkHandler handler) {
        UserInfo userInfo = new UserInfo(context);
        doReLoginMediCare(context, userInfo.getSaveId(), userInfo.getSavedPw(), handler);
    }

    /**
     * 신규 로그인 전문
     * @param id
     * @param pw
     */
    public static void doReLoginMediCare(final Context context, final String id, final String pw, final MediNewNetworkHandler handler) {
        final Tr_login.RequestData requestData = new Tr_login.RequestData();
        requestData.mber_id = id;
        requestData.mber_pwd = pw;

        Log.i(TAG, "doReLoginMediCare requestData.mber_id="+requestData.mber_id+", requestData.mber_pwd="+requestData.mber_pwd);

        MediNewNetworkModule.doApi(context, Tr_login.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_login) {
                    Tr_login data = (Tr_login) responseData;
                    String resultCode = data.log_yn;

                    UserInfo user = new UserInfo(context);
                    if ("Y".equals(resultCode)) {
                        user.setSavedId(id);
                        user.setSavePwd(pw);
                        if (handler != null)
                            handler.onSuccess(responseData);
                    } else {
                        Exception e = new Exception();
                        if (handler != null)
                            handler.onFailure(9999, "로그인 오류 발생", e);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                if (handler != null)
                    handler.onFailure(statusCode, response, error);
            }
        });
    }

}
