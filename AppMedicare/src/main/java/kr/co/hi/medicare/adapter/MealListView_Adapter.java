package kr.co.hi.medicare.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;

import kr.co.hi.medicare.bean.MealActivity_List;

/**
 * 건강정보 리스트뷰 어댑터.
 */
public class MealListView_Adapter extends BaseAdapter  {
    private Activity mActivity;
    private ArrayList<MealActivity_List> mListData = new ArrayList<>();
    private FragmentManager mFm;
    private String EXTRA_EDIT = "EXTRA_EDIT";
    private String EXTRA_MEAL_WHEN = "EXTRA_MEAL_WHEN";
    private String EXTRA_DATE = "EXTRA_DATE";
    private int REQUEST_CODE_EDIT = 177;
    private MealActivity_List mItem;
    private String mEditFlag; // 수정 유무 플래그 Y 가능 , N 불가

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

//    public MealListView_Adapter(Activity activity , FragmentManager fm, String editflag) {
//        mFm = fm;
//        mActivity = activity;
//        mEditFlag = editflag;
//    }
//
//    @Override
//    public int getCount() {
//        return mListData.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return mListData.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        final ViewHolder holder;
//
//        final MealActivity_List mData = mListData.get(position);
//
//
//        if (convertView == null) {
//            holder = new ViewHolder();
//
//            LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            convertView = inflater.inflate(R.layout.item_meal_listview, null);
//
//            holder.mTypeTv = (TextView) convertView.findViewById(R.id.type_tv);
//            holder.mDateTv = (TextView) convertView.findViewById(R.id.date_tv);
//            holder.mContentTv = (TextView) convertView.findViewById(R.id.content_tv);
//
//
//            holder.mViewPager   =   (ViewPager)     convertView.findViewById(R.id.viewpager);
//            holder.mIndicatorLay =   (RelativeLayout)convertView.findViewById(R.id.indicator_lay);
//            holder.mPhotoLay    =   (FrameLayout)   convertView.findViewById(R.id.photo_lay);
//            holder.mIndicator   =   (UnderlinePageIndicator)convertView.findViewById(R.id.indicator);
////            holder.mSlidingPagerAdapter =   new SlidingImagePagerAdapter();
//            holder.mEtcImg      =   (ImageView)  convertView.findViewById(R.id.etc_img);
//            holder.mPhotoEaTv   =   (TextView)  convertView.findViewById(R.id.photeea_tv);
//            holder.PhotoArr = new ArrayList<>();
//
//
//            holder.mIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//                @Override
//                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                }
//
//                @Override
//                public void onPageSelected(int position) {
//                    CLog.i("onPageSelected = " + position);
//                    String imsiEa = String.valueOf(position + 1);
//                    String imsiTotalEa = String.valueOf(holder.PhotoArr.size());
//                    CLog.i("imsiEa = " + imsiEa);
//                    CLog.i("imsiTotalEa = " + imsiTotalEa);
//
//                    holder.mPhotoEaTv.setText(imsiEa + "/" + imsiTotalEa);
//                }
//
//                @Override
//                public void onPageScrollStateChanged(int state) {
//                }
//            });
//
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//
//
//        if (mData != null) {
//            if (mData.getMEAL_WHEN() != null && !mData.getMEAL_WHEN().equals("")) {
//                if (mData.getMEAL_WHEN().equals("B")) {
//                    holder.mTypeTv.setText("아침");
//                } else if (mData.getMEAL_WHEN().equals("L")) {
//                    holder.mTypeTv.setText("점심");
//                } else if (mData.getMEAL_WHEN().equals("D")) {
//                    holder.mTypeTv.setText("저녁");
//                } else {
//                    holder.mTypeTv.setText("간식");
//                }
//            }
//
//            if (mData.getWDATE() != null && !mData.getWDATE().equals("")){
//                String date = Util.getDateSpecialCharacter(mActivity, mData.getWDATE(), 5);
//                holder.mDateTv.setText(date);
//            }
//
//            if (mData.getMC_CONTENT() != null && !mData.getMC_CONTENT().equals("")){
//                holder.mContentTv.setText(mData.getMC_CONTENT());
//            }
//
//            holder.PhotoArr.clear();
//
//            if (!mData.getMC_IMG1().equals("")) {
//                holder.PhotoArr.add(mData.getMC_IMG1());
//            }
//            if (!mData.getMC_IMG2().equals("")) {
//                holder.PhotoArr.add(mData.getMC_IMG2());
//            }
//            if (!mData.getMC_IMG3().equals("")) {
//                holder.PhotoArr.add(mData.getMC_IMG3());
//            }
//            if (!mData.getMC_IMG4().equals("")) {
//                holder.PhotoArr.add(mData.getMC_IMG4());
//            }
//            if (!mData.getMC_IMG5().equals("")) {
//                holder.PhotoArr.add(mData.getMC_IMG5());
//            }
//
//
//            if (holder.PhotoArr.size() > 0){
//                holder.mPhotoEaTv.setText(1 + "/" + holder.PhotoArr.size());
//                holder.mViewPager.setAdapter(new PagerAdapterClass(mActivity , holder.PhotoArr));
//                holder.mIndicator.setViewPager(holder.mViewPager);
//                holder.mIndicator.setSelectedColor(ContextCompat.getColor(mActivity, R.color.baseAppColor));
//                holder.mIndicator.setFades(false);
//            }
//
//            holder.mEtcImg.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) { //
//                    mItem = mData;
////                    mActivity.setTheme(R.style.ActionSheetStyleiOS9);
//                    if (mEditFlag.equals("N")) {
//                        showActionShare();
//                    }else {
//                        UIThread(mActivity, mActivity.getString(R.string.confirm), mActivity.getString(R.string.meal_no_edit));
//                    }
//                }
//            });
//        }
//
//        return convertView;
//    }
//
//    public class ViewHolder {
//        public TextView mTypeTv, mDateTv, mContentTv, mPhotoEaTv;
//        public ViewPager mViewPager;
//        public UnderlinePageIndicator mIndicator;
//        public FrameLayout mPhotoLay;
//        public RelativeLayout mIndicatorLay;
//        public ArrayList<String> PhotoArr;
//        public ImageView mEtcImg;
//    }
//
//    public void addItem(MealInfo mealInfo) {
//        mListData.addAll(mealInfo.getADDR_MASS());
//    }
//
//    public void removeItem(){
//        mListData.clear();
//    }
//
//
//    /**
//     * 수정 삭제 버튼
//     */
//
//    public void showActionShare() {
//
////        ActionSheet.createBuilder(mActivity, mFm)
////                .setCancelButtonTitle(mActivity.getString(R.string.cancel))   // 취소버튼
////                .setOtherButtonTitles(mActivity.getString(R.string.option_tap_my_01), mActivity.getString(R.string.option_tap_my_02))    // 상단, 하단 버튼
////                .setCancelableOnTouchOutside(true).setListener(MealListView_Adapter.this).show();   // 리스너
//    }
//
//
////    @Override
////    public void onDismiss(ActionSheet actionSheet, boolean b) {
////
////    }
////
////    @Override
////    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
////
////        Intent intent = null;
////        switch (index) {
////            case 0:
////                CLog.i("mListData.get(position).getMEAL_WHEN() ---> " + mItem.getMEAL_WHEN());
////                CLog.i("mListData.get(position).getWDATE() ---> " + mItem.getWDATE());
////                CLog.i("position ---> " + mItem);
////
////
////                intent = new Intent(mActivity, MealWriteActivity.class);
////                intent.putExtra(EXTRA_EDIT, true);
////                intent.putExtra(EXTRA_MEAL_WHEN, mItem.getMEAL_WHEN());
////                intent.putExtra(EXTRA_DATE, mItem.getWDATE());
////                mActivity.startActivityForResult(intent, REQUEST_CODE_EDIT);
////                break;
////            case 1:
////                AlertDialog.Builder alert = new AlertDialog.Builder(mActivity);
////                alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
////                    @Override
////                    public void onClick(DialogInterface dialog, int which) {
////                        requestDeleteMeal(mItem.getOSEQ(), mItem.getWDATE(), mItem.getMEAL_WHEN());
////                    }
////                }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
////                    @Override
////                    public void onClick(DialogInterface dialog, int which) {
////                        dialog.dismiss();
////                    }
////                });
////                alert.setMessage("삭제 하시겠습니까?");
////                alert.show();
////
////                break;
////            case 2:
////                break;
////        }
////    }
//
//    /**
//     * 삭제하기
//     * @param qseq 회원번호
//     * @param wdate 등록날짜
//     * @param meal_when 아침 점시 저녁 간식
//     */
//    private void requestDeleteMeal(String qseq, String wdate, String meal_when) {
//        //서버에 값 전달.
//        JSONObject jObject = new JSONObject();
//        try {
//            jObject.put("SEQ", qseq);
//            jObject.put("WDATE", wdate);
//            jObject.put("MEAL_WHEN", meal_when);
//            //old
////            request(EServerAPI.API_POST, RequestUtil.getJSONParameter(jObject, "DX004"));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        //new
//        try {
//            AsyncHttpClient client = new AsyncHttpClient();
//            RequestParams params = new RequestParams();
//            client.setTimeout(10000);
//            client.addHeader("Accept-Charset", "UTF-8");
//            client.addHeader("User-Agent", new WebView(mActivity).getSettings().getUserAgentString());
//            jObject.put("DOCNO","DX004");
//            params.put("strJson", jObject.toString());
//            CLog.i("url = " + Defined.BASE_HTTPS_URL +"?" +params.toString());
//            client.post(Defined.BASE_HTTPS_URL, params, mAsyncHttpHandler);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private AsyncHttpResponseHandler mAsyncHttpHandler = new AsyncHttpResponseHandler() {
//        @Override
//        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//            CLog.i("onSuccess");
//
//            try {
//
//                int count = headers.length;
//
//                for (int i = 0; i < count; i++) {
//                    CLog.v(headers[i].getName() + ": " + headers[i].getValue());
//                }
//
//                String response = new String(responseBody, "UTF-8");
//                CLog.i("response = " +response);
//                JSONObject resultData = null;
//
//                if(response.startsWith("<")){   // api result 문자열 시작값이 "<" 로 시작하는 경우는 xml 폼
//                    try {
//                        response =  Util.parseXml(response);
//                    }catch(Exception e){
//                        CLog.e(e.toString());
//                    }
//
//                    resultData = new JSONObject(response);
//
//                }else{
//                    resultData = new JSONObject(response);
//                }
//
//                Map<String, Object> result = JsonUtil.toMap(resultData);
//                CLog.e(response);
//                if (result.containsKey("RESULT_CODE")) {
//                    try {
//                        switch ((String) result.get("RESULT_CODE")) {
//                            case "0000":
//                                UISettingThread();
//                                break;
//                            case "4444":
//                                UIThread(mActivity, mActivity.getString(R.string.confirm), mActivity.getString(R.string.msg_fail_delete));
//                                break;
//                            case "6666":
//                                UIThread(mActivity, mActivity.getString(R.string.confirm), mActivity.getString(R.string.no_user));
//                                break;
//                            case "9999":
//                                UIThread(mActivity, mActivity.getString(R.string.confirm), mActivity.getString(R.string.msg_error));
//                                break;
//
//                        }
//                    } catch (Exception e) {
//                        CLog.e(e.toString());
//                    }
//                }
//
//
//            } catch (Exception e) {
//                CLog.e(e.toString());
//            }
//        }
//
//        @Override
//        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//            CLog.i("onFailure");
//            Handler mHandler = new Handler(Looper.getMainLooper());
//            mHandler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    UIThread(mActivity, mActivity.getString(R.string.confirm), mActivity.getString(R.string.networkexception));
//                }
//            }, 0);
//        }
//    };
//
//    public void request(EServerAPI eServerAPI, Object obj) {
//        Record record = new Record();
//        record.setEServerAPI(eServerAPI);
//        switch (eServerAPI) {
//            case API_GET:
//                record.setRequestData(obj);
//                break;
//            case API_POST:
//                record.setRequestData(obj);
//                break;
//        }
//        sendApi(record);
//    }
//
//    public void response(Record record) throws UnsupportedEncodingException {
//        String json = new String(record.getData());
//        json = json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1);
//        Map<String, Object> result = JsonUtil.getJSONParserData(record.getData());
//        CLog.e(json);
//        if (result.containsKey("RESULT_CODE")) {
//            try {
//                switch ((String) result.get("RESULT_CODE")) {
//
//                    /*
//                    0000	삭제성공
//                    4444	등록된 식사기록이 없습니다.
//                    6666	회원이 존재하지 않음
//                    9999	기타오류
//                    */
//                    case "0000":
//                        UISettingThread();
//                        break;
//                    case "4444":
//                        UIThread(mActivity, mActivity.getString(R.string.confirm), mActivity.getString(R.string.msg_fail_delete));
//                        break;
//                    case "6666":
//                        UIThread(mActivity, mActivity.getString(R.string.confirm), mActivity.getString(R.string.no_user));
//                        break;
//                    case "9999":
//                        UIThread(mActivity, mActivity.getString(R.string.confirm), mActivity.getString(R.string.msg_error));
//                        break;
//
//                }
//            } catch (Exception e) {
//                CLog.e(e.toString());
//            }
//        }
//    }
//
//
//    private void UISettingThread() {
//        Handler mHandler = new Handler(Looper.getMainLooper());
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                mListData.remove(mItem);
//                notifyDataSetChanged();
//            }
//        }, 0);
//    }
//
//
//
//    public void networkException(Record record) {
//        CLog.e("ERROR STATE : " + record.stateCode);
//        UIThread(mActivity, mActivity.getString(R.string.confirm), mActivity.getString(R.string.networkexception));
//    }
//
//      private void UIThread(final Activity activity, final String confirm, final String message) {
//        Handler mHandler = new Handler(Looper.getMainLooper());
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                AlertDialogUtil.onAlertDialog(activity, confirm, message);
//            }
//        }, 0);
//    }
//
//    /**
//     * 해당 메인 사진 뷰페이저 아답터
//     */
//    private class PagerAdapterClass extends PagerAdapter {
//
//        private LayoutInflater mInflater;
//        private ArrayList<String> mPhotoArr;
//
//        public PagerAdapterClass(Context c , ArrayList<String> arr) {
//            super();
//            mInflater = LayoutInflater.from(c);
//            mPhotoArr = arr;
//        }
//
//        @Override
//        public int getCount() {
//            // TODO Auto-generated method stub
//            return mPhotoArr.size();
//        }
//
//        @Override
//        public Object instantiateItem(View pager, final int position) {
//            View v = null;
//
//            CLog.i("position ---> " + position);
//
//            v = mInflater.inflate(R.layout.img_viewpager, null);
//            ImageView pagerImg = (ImageView) v.findViewById(R.id.main_img);
//            CustomImageLoader.displayImage(mActivity, mPhotoArr.get(position), pagerImg);    // 음식사진 이미지
//
//
//            pagerImg.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent intent = new Intent(mActivity, ImgDetailActivity.class);
//                    intent.putExtra("EXTRA_PHOTO_LIST", mPhotoArr);
//                    intent.putExtra("EXTRA_PHOTO_INDEX", position);
//                    mActivity.startActivity(intent);
//                }
//            });
//
//            ((ViewPager) pager).addView(v, 0);
//            return v;
//        }
//
//        @Override
//        public void destroyItem(View pager, int position, Object view) {
//            ((ViewPager) pager).removeView((View) view);
//        }
//
//        @Override
//        public boolean isViewFromObject(View pager, Object obj) {
//            return pager == obj;
//        }
//
//        @Override
//        public void restoreState(Parcelable arg0, ClassLoader arg1) {
//        }
//
//        @Override
//        public Parcelable saveState() {
//            return null;
//        }
//
//        @Override
//        public void startUpdate(View arg0) {
//        }
//
//        @Override
//        public void finishUpdate(View arg0) {
//        }
//
//    }
//
//
//    private NetworkListener networkListener = new NetworkListener() {
//        @Override
//        public void onResponse(Record a_oRecord) {
//            try
//            {
//                response(a_oRecord);
//            }catch(UnsupportedEncodingException e){
//                CLog.e(Util.getExceptionLog(e));
//                networkException(a_oRecord);
//            }
//        }
//
//        @Override
//        public void onNetworkException(Record a_oRecord) {
//            networkException(a_oRecord);
//        }
//    };
//
////    public abstract void response(Record record) throws UnsupportedEncodingException;
////
////    public abstract void request(EServerAPI eServerAPI, Object obj);
////
////    public abstract void networkException(Record record);
//
//
//    public void sendApi(Record record) {
//        record.setNetworkListener(networkListener);
//        HttpClientManager.getInstances().request(record);
//    }

//    private class SlidingImagePagerAdapter extends PagerAdapter
//    {
//        private Context mContext;
//        private LayoutInflater inflater;
//        private ArrayList<String> mPhotoData;
//
//        public SlidingImagePagerAdapter(){
//
//        }
//
//        public SlidingImagePagerAdapter(Context context, ArrayList<String> arr)
//        {
//            mPhotoData  =   arr;
//            mContext	=	context;
//            inflater = LayoutInflater.from(context);
//        }
//        @Override
//        public int getCount()
//        {
//            return mPhotoData.size();
//        }
//
//        @Override
//        public boolean isViewFromObject(View view, Object object)
//        {
//            return view == object;
//        }
//
//        @Override
//        public Object instantiateItem(ViewGroup container,final int position)
//        {
//
//            View view = inflater.inflate(R.layout.img_viewpager, null);
//
//            CLog.i("PhotoArr.size -- > " + mPhotoData.size());
//
//            view.setLayoutParams(new LinearLayout.LayoutParams(
//                    ViewGroup.LayoutParams.MATCH_PARENT,
//                    ViewGroup.LayoutParams.MATCH_PARENT));
//
//            ImageView photoImage = (ImageView)view.findViewById(R.id.main_img);
//
//            if(mPhotoData.size() > 0 && mPhotoData.get(position) != null) {
//                CustomImageLoader.displayImage(mContext, mPhotoData.get(position), photoImage);
//            }
//
//            photoImage.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    CLog.i("photo onClick");
//
//
//                }
//            });
//
//            ((ViewPager) container).addView(view, 0);
//
//            return view;
//        }
//
//        @Override
//        public void destroyItem(ViewGroup container, int position, Object object)
//        {
//            ((ViewPager) container).removeView((View) object);
//        }
//    }
}
