package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.utilhw.Logger;

/**
 *
  운동 입력
 input값
 insures_code : 회사코드
 mber_sn : 회원고유키값
 api_code : api 키값
 active_seq : 운동 일련번호
 mets : mets 값 칼로리 로직 계산하기 위해서는 반드시 보내야만 함
 active_de : 운동등록일
 active_nm : 운동명
 active_de_stime : 운동 시작일 (ex : 07:00  , 15:60)  반드시 날짜 형식 맞추주세요
 active_de_etime : 운동 끝나는 일 (ex 21:30) 반드시 날짜 형식 맞추주세요

 output 값
 api_code : 호출코드명
 insures_code : 회사코드
 sport_sn : 수동 입력된 제일 고유한 키값( 대표 값으로 삭제하면 --> 시간별 삭제 됨
 sport_list : 배열
 data_sn : 저장된 키값
 active_seq : 운동 고유키값
 active_time : 시간 09 ~ 21
 calory : 칼로리
 reg_de : 등록일
 reg_yn : 저장여부 (Y,N)
 */

public class Tr_mvm_info_input_data_sport extends BaseData {
    private final String TAG = Tr_mvm_info_input_data_sport.class.getSimpleName();

    public static class RequestData {

        public String mber_sn;
        public String active_seq;
        public String mets;
        public String active_de;
        public String active_nm;
        public String active_de_stime;
        public String active_de_etime;
    }

    public Tr_mvm_info_input_data_sport() {
    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof Tr_mvm_info_input_data_sport.RequestData) {
            JSONObject body = new JSONObject();
            Tr_mvm_info_input_data_sport.RequestData data = (Tr_mvm_info_input_data_sport.RequestData) obj;

            body.put("api_code", getApiCode(TAG) );
            body.put("insures_code", INSURES_CODE);
            body.put("mber_sn", data.mber_sn);


            body.put("active_seq", data.active_seq );
            body.put("mets", data.mets);
            body.put("active_de", data.active_de);

            body.put("active_nm", data.active_nm );
            body.put("active_de_stime", data.active_de_stime);
            body.put("active_de_etime", data.active_de_etime);

            return body;
        }

        return super.makeJson(obj);
    }

    public JSONArray getArray(Tr_get_hedctdata.DataList dataList) {
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        try {
            obj.put("idx" , dataList.idx ); //170410173713859",
            array.put(obj);
        } catch (JSONException e) {
            Logger.e(e);
        }

        return array;
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/
    @SerializedName("api_code")
    public String api_code; //
    @SerializedName("insures_code")
    public String insures_code; //
    @SerializedName("sport_sn")
    public String sport_sn; //
    @SerializedName("sport_list")
    public List<sport_list> sport_list = new ArrayList<>(); //

    public class sport_list {
        @SerializedName("data_sn")
        public String data_sn;
        @SerializedName("active_seq")
        public String active_seq;
        @SerializedName("active_time")
        public String active_time;
        @SerializedName("calory")
        public String calory;
        @SerializedName("reg_de")
        public String reg_de;
    }
    @SerializedName("reg_yn")
    public String reg_yn; //

}
