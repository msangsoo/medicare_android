package kr.co.hi.medicare.util;

import android.app.Activity;
import android.graphics.Color;
import android.os.Handler;
import android.widget.TextView;

/**
 * 원형 그래프 색변화.
 */
public class CircularGraphUtil {
    private Activity mActivity;
    private CircleProgressBar mProgressBar;
    private Handler mHandler = new Handler();
    private int mProgressStatus = 0;

    public CircularGraphUtil(Activity activity, CircleProgressBar progressBar) {
        mActivity = activity;
        mProgressBar = progressBar;
    }

    public void dosomething(final int point, final TextView textView) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mProgressStatus = 0;
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (point < 40) {
                            textView.setTextColor(Color.parseColor("#fd6b42"));
                            mProgressBar.setColor(Color.parseColor("#fd6b42"));
                            //mProgressBar.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.circular_graph_red));
                        } else if (point < 70) {
                            textView.setTextColor(Color.parseColor("#f0ba02"));
                            mProgressBar.setColor(Color.parseColor("#f0ba02"));
                            //mProgressBar.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.circular_graph_yellow));
                        } else {
                            textView.setTextColor(Color.parseColor("#36aa9d"));
                            mProgressBar.setColor(Color.parseColor("#36aa9d"));
                           // mProgressBar.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.circular_graph_green));
                        }
                    }
                });
                while (mProgressStatus < point) {
                    mProgressStatus += 1;
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBar.setProgress(mProgressStatus);
                        }
                    });
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }).start();
    }
    /*
    public void dosomething(final int point) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (mProgressStatus < point) {
                    mProgressStatus += 1;
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (mProgressStatus < 40) {
                                mProgressBar.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.circular_graph_red));
                            } else if (mProgressStatus < 70) {
                                mProgressBar.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.circular_graph_yellow));
                            } else {
                                mProgressBar.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.circular_graph_green));
                            }
                            mProgressBar.setProgress(mProgressStatus);
                        }
                    });
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
    */
}
